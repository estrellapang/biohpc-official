## Notes on CompTIA Security+ Certification 

### Network Devices

#### layer 2 switches

  - packet forwarding via MAC addresses

  - managed, vs. non-managed --> latter allow for individual ports' securities to be configured

  - non-routable, only communicate with local network

  - uses "ASIC", or application-specific integrated circuit chip to recognize connected devices based on ports

\

#### WAP (wireless access point)

  -  layer 2, bridges 802.3 Ethernet segments with that of 802.11 wireless segments

  -  all utilize one form of encryption or another

  - only communicate with local networks (non-routable)

  - **WAP vs. Wi-Fi Routers EXPLAINED**

```
# WAP                                           |             # Wi-Fi Router
------------------------------------------------|-----------------------------------------|
- connects to existing LAN                      | - can create LANs
- needs PoE switch to route/connect to gateway  | - routable, connects directly w/h gateway
- cannot mamange its connected LAN              | - can manage the LANs it creates
- an extender of Ethernet to wireless proto     | - a WAP, Ethernet switch, and a firewall
```

- [great explanation here](https://community.fs.com/blog/wireless-access-point-vs-router-what-are-the-differences.html)

\

#### layer 3 switches/"Multilayer Layer Switch"(MLS)

- also has ASIC chip but programmed to to ROUTE
 
  - meaning it CAN connect to NON-LOCAL networks

- can implement security at layer 2 and above (3)

- not as common in smaller local networks b.c. of higher cost

\

#### Router

- functions at layer 3

- DIFFERENCE from Switches

  - uses **SOFTWARE programming** RATHER than hardware-based ASIC chip for decision making

  - (more hackable?)

- can implement Access Control Lists (ACLs) and its own Firewalls

\

### Proxy Servers

- relays requests ON BEHALF of its internal clients

  - hides the identity of its clients

- can filter external traffic based on IP/domain/website name, protocols, etc.

- can accelerate network performance by caching commonly accessed web pages (even though that's done on the Browser's side as well)

\

### Load-Balancers

> also called "content switch/filter"

- limit/filter (for example only the FTPS protocol) and redirect incoming traffic

- relays or spreads the traffic across multiple nodes to even out workload for efficiency


\
\
\

### Firewalls

#### properties

- blocks packets from ENTERING -OR- LEAVING networks

- Stateless

  - checks every packet against rule set for actions

- Stateful

  - ONLY checks the state of connection between ntwks

  - checks on Established connections, Internal --> External; inbound packets from external site not checked

  - blocks NEW connections initiated from External sites (only allows Internal to initiate connections to External)


#### Layers

- implemented at layers 2,3,4, and 7

- On Routers and Servers/Hosts --> software based!

- on dedicated appliance --> non-software!





\
\
\


















 

## Basic Biology 1 

### Characteristics of Life 

1. Regulation (Homeostasis) 

2. Metabolism (heterotroph vs. autotroph) 

3. Reproduction (conjugation, sexual reproduction) 

4. Growth (carrying out genetic of instructions, repair) 

5. Response (to internal & external stimuli)

6. Evolution (adapation, specialization) 

7. Movement

 



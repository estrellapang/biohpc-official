## OpenMPI Installation & Commands


### Installations 

```
sudo yum install openmpi
[sudo] password for zxp8244: 
Loaded plugins: fastestmirror
Determining fastest mirrors
epel/x86_64/metalink                                                                                                 |  17 kB  00:00:00     
 * base: centos.mirror.lstn.net
 * epel: fedora-epel.mirror.lstn.net
 * extras: centos.mirror.lstn.net
 * updates: centos.mirror.lstn.net
OpenHPC                                                                                                              | 1.6 kB  00:00:00     
OpenHPC-updates                                                                                                      | 1.2 kB  00:00:00     
base                                                                                                                 | 3.6 kB  00:00:00     
epel                                                                                                                 | 4.7 kB  00:00:00     
extras                                                                                                               | 3.4 kB  00:00:00     
updates                                                                                                              | 3.4 kB  00:00:00     
(1/5): OpenHPC-updates/primary                                                                                       | 318 kB  00:00:00     
(2/5): epel/x86_64/updateinfo                                                                                        | 987 kB  00:00:01     
(3/5): extras/7/x86_64/primary_db                                                                                    | 187 kB  00:00:00     
(4/5): updates/7/x86_64/primary_db                                                                                   | 3.4 MB  00:00:00     
(5/5): epel/x86_64/primary_db                                                                                        | 6.7 MB  00:00:01     
OpenHPC-updates                                                                                                                   1764/1764
Resolving Dependencies
--> Running transaction check
---> Package openmpi.x86_64 0:1.10.7-2.el7 will be installed
--> Processing Dependency: librdmacm.so.1(RDMACM_1.0)(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: libpsm2.so.2(PSM2_1.0)(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: libibverbs.so.1(IBVERBS_1.1)(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: libibverbs.so.1(IBVERBS_1.0)(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: libfabric.so.1(FABRIC_1.1)(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: libfabric.so.1(FABRIC_1.0)(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: environment-modules for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: librdmacm.so.1()(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: libquadmath.so.0()(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: libpsm_infinipath.so.1()(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: libpsm2.so.2()(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: libosmcomp.so.3()(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: libibverbs.so.1()(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: libgfortran.so.3()(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: libfabric.so.1()(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Running transaction check
---> Package environment-modules.x86_64 0:3.2.10-10.el7 will be installed
---> Package infinipath-psm.x86_64 0:3.3-26_g604758e_open.2.el7 will be installed
---> Package libfabric.x86_64 0:1.6.1-2.el7 will be installed
---> Package libgfortran.x86_64 0:4.8.5-36.el7_6.1 will be installed
---> Package libibverbs.x86_64 0:17.2-3.el7 will be installed
--> Processing Dependency: rdma-core(x86-64) = 17.2-3.el7 for package: libibverbs-17.2-3.el7.x86_64
---> Package libpsm2.x86_64 0:10.3.58-1.el7 will be installed
---> Package libquadmath.x86_64 0:4.8.5-36.el7_6.1 will be installed
---> Package librdmacm.x86_64 0:17.2-3.el7 will be installed
---> Package opensm-libs.x86_64 0:3.3.20-3.el7 will be installed
--> Processing Dependency: libibumad.so.3(IBUMAD_1.0)(64bit) for package: opensm-libs-3.3.20-3.el7.x86_64
--> Processing Dependency: libibumad.so.3()(64bit) for package: opensm-libs-3.3.20-3.el7.x86_64
--> Running transaction check
---> Package libibumad.x86_64 0:17.2-3.el7 will be installed
---> Package rdma-core.x86_64 0:17.2-3.el7 will be installed
--> Processing Dependency: pciutils for package: rdma-core-17.2-3.el7.x86_64
--> Running transaction check
---> Package pciutils.x86_64 0:3.5.1-3.el7 will be installed
--> Processing Conflict: lmod-ohpc-7.8.1-5.1.ohpc.1.3.6.x86_64 conflicts environment-modules
--> Restarting Dependency Resolution with new changes.
--> Running transaction check
---> Package lmod-ohpc.x86_64 0:7.8.1-5.1.ohpc.1.3.6 will be updated
---> Package lmod-ohpc.x86_64 0:7.8.15-4.1.ohpc.1.3.67 will be an update
--> Processing Conflict: lmod-ohpc-7.8.15-4.1.ohpc.1.3.67.x86_64 conflicts environment-modules
--> Finished Dependency Resolution
Error: lmod-ohpc conflicts with environment-modules-3.2.10-10.el7.x86_64
 You could try using --skip-broken to work around the problem
 You could try running: rpm -Va --nofiles --nodigest

```

#### Workaround (Does not work) 

```
[zxp8244@head ~]$ sudo yum --skip-broken install openmpi
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: centos.mirror.lstn.net
 * epel: fedora-epel.mirror.lstn.net
 * extras: centos.mirror.lstn.net
 * updates: centos.mirror.lstn.net
Resolving Dependencies
--> Running transaction check
---> Package openmpi.x86_64 0:1.10.7-2.el7 will be installed
--> Processing Dependency: librdmacm.so.1(RDMACM_1.0)(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: libpsm2.so.2(PSM2_1.0)(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: libibverbs.so.1(IBVERBS_1.1)(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: libibverbs.so.1(IBVERBS_1.0)(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: libfabric.so.1(FABRIC_1.1)(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: libfabric.so.1(FABRIC_1.0)(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: environment-modules for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: librdmacm.so.1()(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: libquadmath.so.0()(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: libpsm_infinipath.so.1()(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: libpsm2.so.2()(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: libosmcomp.so.3()(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: libibverbs.so.1()(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: libgfortran.so.3()(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Processing Dependency: libfabric.so.1()(64bit) for package: openmpi-1.10.7-2.el7.x86_64
--> Running transaction check
---> Package environment-modules.x86_64 0:3.2.10-10.el7 will be installed
---> Package infinipath-psm.x86_64 0:3.3-26_g604758e_open.2.el7 will be installed
---> Package libfabric.x86_64 0:1.6.1-2.el7 will be installed
---> Package libgfortran.x86_64 0:4.8.5-36.el7_6.1 will be installed
---> Package libibverbs.x86_64 0:17.2-3.el7 will be installed
--> Processing Dependency: rdma-core(x86-64) = 17.2-3.el7 for package: libibverbs-17.2-3.el7.x86_64
---> Package libpsm2.x86_64 0:10.3.58-1.el7 will be installed
---> Package libquadmath.x86_64 0:4.8.5-36.el7_6.1 will be installed
---> Package librdmacm.x86_64 0:17.2-3.el7 will be installed
---> Package opensm-libs.x86_64 0:3.3.20-3.el7 will be installed
--> Processing Dependency: libibumad.so.3(IBUMAD_1.0)(64bit) for package: opensm-libs-3.3.20-3.el7.x86_64
--> Processing Dependency: libibumad.so.3()(64bit) for package: opensm-libs-3.3.20-3.el7.x86_64
--> Running transaction check
---> Package libibumad.x86_64 0:17.2-3.el7 will be installed
---> Package rdma-core.x86_64 0:17.2-3.el7 will be installed
--> Processing Dependency: pciutils for package: rdma-core-17.2-3.el7.x86_64
--> Running transaction check
---> Package pciutils.x86_64 0:3.5.1-3.el7 will be installed
--> Processing Conflict: lmod-ohpc-7.8.1-5.1.ohpc.1.3.6.x86_64 conflicts environment-modules
--> Restarting Dependency Resolution with new changes.
--> Running transaction check
---> Package lmod-ohpc.x86_64 0:7.8.1-5.1.ohpc.1.3.6 will be updated
---> Package lmod-ohpc.x86_64 0:7.8.15-4.1.ohpc.1.3.67 will be an update
--> Processing Conflict: lmod-ohpc-7.8.15-4.1.ohpc.1.3.67.x86_64 conflicts environment-modules
OpenHPC-updates/filelists                                                                                            | 5.1 MB  00:00:00     
--> Running transaction check
---> Package lmod-ohpc.x86_64 0:7.8.1-5.1.ohpc.1.3.6 will be updated
---> Package lmod-ohpc.x86_64 0:7.8.1-5.1.ohpc.1.3.6 will be updated
---> Package lmod-ohpc.x86_64 0:7.8.15-4.1.ohpc.1.3.67 will be an update
--> Processing Conflict: lmod-ohpc-7.8.1-5.1.ohpc.1.3.6.x86_64 conflicts environment-modules
base/7/x86_64/filelists_db                                                                                           | 7.1 MB  00:00:00     
updates/7/x86_64/filelists_db                                                                                        | 2.7 MB  00:00:00     

Packages skipped because of dependency problems:
    environment-modules-3.2.10-10.el7.x86_64 from base
    infinipath-psm-3.3-26_g604758e_open.2.el7.x86_64 from base
    libfabric-1.6.1-2.el7.x86_64 from base
    libgfortran-4.8.5-36.el7_6.1.x86_64 from updates
    libibumad-17.2-3.el7.x86_64 from base
    libibverbs-17.2-3.el7.x86_64 from base
    libpsm2-10.3.58-1.el7.x86_64 from base
    libquadmath-4.8.5-36.el7_6.1.x86_64 from updates
    librdmacm-17.2-3.el7.x86_64 from base
    lmod-ohpc-7.8.15-4.1.ohpc.1.3.67.x86_64 from OpenHPC-updates
    openmpi-1.10.7-2.el7.x86_64 from base
    opensm-libs-3.3.20-3.el7.x86_64 from base
    pciutils-3.5.1-3.el7.x86_64 from base
    rdma-core-17.2-3.el7.x86_64 from base
```

#### Package Installation Fix!

`yum group list` 


```
[zxp8244@head ~]$ yum group list
Loaded plugins: fastestmirror
There is no installed groups file.
Maybe run: yum groups mark convert (see man yum)
Loading mirror speeds from cached hostfile
 * base: centos.mirror.lstn.net
 * epel: fedora-epel.mirror.lstn.net
 * extras: centos.mirror.lstn.net
 * updates: centos.mirror.lstn.net
Available Environment Groups:
   Minimal Install
   Compute Node
   Infrastructure Server
   File and Print Server
   Cinnamon Desktop
   MATE Desktop
   Basic Web Server
   Virtualization Host
   Server with GUI
   GNOME Desktop
   KDE Plasma Workspaces
   Development and Creative Workstation
Available Groups:
   Cinnamon
   Compatibility Libraries
   Console Internet Tools
   Development Tools
   Educational Software
   Electronic Lab
   Fedora Packager
   General Purpose Desktop
   Graphical Administration Tools
   Haskell
   Legacy UNIX Compatibility
   MATE
   Milkymist
   Scientific Support
   Security Tools
   Smart Card Support
   System Administration Tools
   System Management
   TurboGears application framework
   Xfce
   ohpc-autotools
   ohpc-base
   ohpc-base-compute
   ohpc-ganglia
   ohpc-io-libs-gnu
   ohpc-io-libs-intel
   ohpc-nagios
   ohpc-parallel-libs-gnu
   ohpc-parallel-libs-gnu-mpich
   ohpc-parallel-libs-gnu-mvapich2
   ohpc-parallel-libs-gnu-openmpi
   ohpc-parallel-libs-intel-impi
   ohpc-parallel-libs-intel-mpich
   ohpc-parallel-libs-intel-mvapich2
   ohpc-parallel-libs-intel-openmpi
   ohpc-perf-tools-gnu
   ohpc-perf-tools-intel
   ohpc-python-libs-gnu
   ohpc-python-libs-intel
   ohpc-runtimes-gnu
   ohpc-runtimes-intel
   ohpc-serial-libs-gnu
   ohpc-serial-libs-intel
   ohpc-slurm-client
   ohpc-slurm-server
   ohpc-warewulf
Done

```

&nbsp;


### Install MPI Libraries 
(use `groupinstall`, since the package was listed as an "Anvailable Group") 

&nbsp;


```
sudo yum groupinstall "ohpc-parallel-libs-gnu-openmpi" 


...
...
...


Installed:
  boost-gnu-openmpi-ohpc.x86_64 0:1.66.0-3.7                            fftw-gnu-openmpi-ohpc.x86_64 0:3.3.7-3.7                            
  hypre-gnu-openmpi-ohpc.x86_64 0:2.13.0-3.8                            mumps-gnu-openmpi-ohpc.x86_64 0:5.1.2-5.1                           
  petsc-gnu-openmpi-ohpc.x86_64 0:3.8.3-3.9                             scalapack-gnu-openmpi-ohpc.x86_64 0:2.0.2-26.4                      
  superlu_dist-gnu-openmpi-ohpc.x86_64 0:4.2-84.4                       trilinos-gnu-openmpi-ohpc.x86_64 0:12.12.1-4.4                      

Dependency Installed:
  gnu-compilers-ohpc.x86_64 0:5.4.0-21.1   hdf5-gnu-ohpc.x86_64 0:1.10.1-3.2            infinipath-psm.x86_64 0:3.3-26_g604758e_open.2.el7  
  ksh.x86_64 0:20120801-139.el7            libibumad.x86_64 0:17.2-3.el7                libibverbs.x86_64 0:17.2-3.el7                      
  libicu.x86_64 0:50.1.2-17.el7            librdmacm.x86_64 0:17.2-3.el7                metis-gnu-ohpc.x86_64 0:5.1.0-16.2                  
  openblas-gnu-ohpc.x86_64 0:0.2.20-3.2    openmpi-gnu-ohpc.x86_64 0:1.10.7-4.6         opensm-libs.x86_64 0:3.3.20-3.el7                   
  pciutils.x86_64 0:3.5.1-3.el7            phdf5-gnu-openmpi-ohpc.x86_64 0:1.10.1-3.4   prun-ohpc.noarch 0:1.3-4.1.ohpc.1.3.7               
  rdma-core.x86_64 0:17.2-3.el7            superlu-gnu-ohpc.x86_64 0:5.2.1-38.2        

Complete!

```


**VERIFY INSTALLATION & Module Environments**

`yum group list` --> see Available & Installed package groups; under "Installled Groups," one should see: 

```
Development Tools   #this is also a must, as it includes the GCC compiler
ohpc-parallel-libs-gnu-openmpi

```

- To load OpenMPI Module

`module avail` -- `cmake,gnu,pmix,prun` should be available: the moduke `gnu/` contains the openmpi module. 

1.`module load gnu` 

2.`module avail` --> now more modules will be available: 

```
------------------------------------------------------- /opt/ohpc/pub/moduledeps/gnu -------------------------------------------------------
   hdf5/1.10.1    metis/5.1.0    openblas/0.2.20    openmpi/1.10.7    superlu/5.2.1

-------------------------------------------------------- /opt/ohpc/pub/modulefiles ---------------------------------------------------------
   cmake/3.12.2    gnu/5.4.0 (L)    pmix/2.1.4    prun/1.3

  Where:
   L:  Module is loaded

```
3.`module add openmpi`   # "add" is synonymous with "load"

4.Verify: `module list` 

```
Currently Loaded Modules:
  1) gnu/5.4.0   2) openmpi/1.10.7

```

5.`which mpirun` 

`/opt/ohpc/pub/mpi/openmpi-gnu/1.10.7/bin/mpirun   # now all things are installed`

6.**Additional Module Commands**: 

`module unload [packagename]` 

`module switch [current module] [desired module version]` 

`module purge` --> unload all of the loaded modules

`module spider` --> list all possible modules that can be installed

   `module load [uninstalled module]` --> will tell you which packages/modules needed in order to load such module 


- **Compiling MPI Codes & Submitting them via SLURM**

  - use `mpicc` to compile C code (Not `gcc`, or this error will return `fatal error: mpi.h:no such file or directory`) 

  - similarly, use `mpiCC`, `mpicxx`, or `mpic++` to compile C++ MPI code

    - these commands are simply MPI compiler wrappers, whose jobs are to include the appropriate compiler flags, libraries (including their directories) will be included


- first C MPI Code: "Hello!" 

```
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

int main(int argc, char *argv[], char *envp[]) {
  int rank, namelen;
  char processor_name[MPI_MAX_PROCESSOR_NAME];

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Get_processor_name(processor_name, &namelen);

  printf("Hello from rank %d, this is %s!\n", rank, processor_name);

  MPI_Finalize();
}

```

- corresponding bash script: 

``` 
#!/bin/bash

#SBATCH --job-name="2nd MPI"
#SBATCH --partition=normal
#SBATCH --nodes=3 
#SBATCH --ntasks=12   #number of instances the code/commands will be executed; this also corresponds to the # of MPI ranks
#SBATCH -o heympi.out
#SBATCH -e heympi.err

module load gnu 

module load openmpi

mpirun ~/test_scripts/heympi

```                             

**NOTE on Batchscript Above**: when running MPI scripts using a scheduler i.e. SLURM, one does not have to worry about specifying how many processes/instances of the instructions that will be run via `mpirun -n [insert number here] ./[insert compiled script here]` 
 
- SLURM Batch Script: an alternative to `--ntasks` is `--ntasks-per-node`

  - former lets SLURM utilize any available cores--good for faster scheduling, but job run time might be a bit slower 

  - latter arranges and waits for resources to become available across each node before running the job 

  - note that the two CANNOT be used together [more info](https://hpcrcf.atlassian.net/wiki/spaces/TCP/pages/7287338/How-to+Submit+a+MPI+Job) 


- `srun` --> a SLURM command, can be utilized to launch MPI code directly without the need of a `.sh` file (this is convenient for small jobs) 

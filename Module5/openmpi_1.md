## Introduction to OpenMPI: Part I

### MPI Implementations 

- OpenMPI (opensource) 

- MPICH (more Enterprise platform users) 

### Terminology  

- **Initialization & Finalization**: the MPI library/standard/API must ALWAYS be initialized & finalized 

- Rank: process ID among the total number processes 

### Commands

# OpenMPI module is not available on workstations, but it is available on the cluster

```
# remote into nucleus login node 

ssh zpang1@nucleus

# load the implementation of MPI 

module load openmpi

# check if the load was successful 

[zpang1@biohpcwsc037 elementary_source]$ module list
Currently Loaded Modulefiles:
  1) shared                   2) slurm/16.05.8            3) openmpi [see actual output]

# check if the C & C++ compiler wrappers are available 

[zpang1@biohpcwsc037 elementary_source]$ which mpicc   # for compiling MPI C scripts
/cm/shared/apps/mpich/ge/gcc/64/3.2rc2/bin/mpicc


[zpang1@biohpcwsc037 elementary_source]$ which mpicxx   # for compiling MPI C++ sciprts
/cm/shared/apps/mpich/ge/gcc/64/3.2rc2/bin/mpicxx

``` 



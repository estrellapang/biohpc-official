## Broad Concepts on Parallel Computing 

> You can spend a lifetime getting 95% of your code to be parallell and never achieve> better than 20x speedup no matter how many processors you throw at it!

[see why, under "Sub-section: Limits & Costs of Parallel Programming](https://computing.llnl.gov/tutorials/parallel_comp/#SPMD-MPMD) 

### Flynn's Categories: From Traditional Computers to Parallel

**SISD (Single Instruction, Single Data)** 

- Oldest type of non-parallel computer, one instruction stream (from memory) is handled by the CPU per clock cycle

**SIMD (Single Instruction, Multiple Data)** 

- Beginning of parallelism: each CPU may work on its own data block, however, all CPUs carry out identical instructions

  - this requires synchronous & deterministic execution

  - most GPU's operate under this principle, for tasks that benefit from this type of parallelism are makred by high degrees of regularity


**MIMD (Multiple Instruction, Multiple Data)** 

- each CPU is allowed to operate on a separate instructions using different data 

- flexibility in execution: asynchronous or synchronous, deterministic or non-deterministic

  - SIMD sub-components (e.g. GPU cards) 

- Most modern cluster and computers fall into this category 

### From the Algorithm & Software Perspective

- Due to having multiple instruction streams, parallel codes are more complex than that of serial ones 

- Parallel applications rely on APIs & Libraries to be portable, i.e. OpenMPI, OpenMP, POSIX threads, etc.

- Scaling with resources used (mainly # of processors), the time to solution vs. quantity of resources ought to scale as linearly as possible 

  - in most cases, the linear relationship will reach a threshold cease to induce performance increase 
 
&nbsp; 

### Memory Architectures for Parallel Computing 

**Shared Memory**

- all cores/processors to access the same global address space---falls under two sub-categories

  - Uniform Memory Access (UMA)---deployed in Symmetric Multiprocessor (SMP) computers

    - identical processors: equal access & access times to memory

  - Non-Uniform Memory Access (NUMA)---characteristic of most modern computer 

    - composed of 2 or more inter-linked SMPs (via internal bus interconnect) 

    - unequal memory access

- OpenMP operates with this memory utilization architecture 

- Strengths (OpenMP-Shared Memory Architecture) 

  - fast data transfer

  - parallelization from serial to parallel is fairly easy in terms of coding

    - more programmer-friendly
  
    - directives can be added incrementally 

  - program can still be run serially (if desired) 

- Weaknesses

  - poor scalability b/w memory & CPUs: more CPU leads to more traffic on memory-CPU interconnect, leading to busier memory cache management


**Distributed Memory** 

- cores/processors each access their own LOCAL memory

- network communication needed to link INTER-processor memory 

- cache-coherency not a factor as it does in shared memory systems


- Strengths 

  - applicable to a wider range of problems

  - each process has local variable 

  - Memory scalable with processors --> as number of processors increase, so does the size of the memory!!! 

  - reduced overhead from maintaining a global cache coherency

  - cost-effective: non-heterogenous nodes can compute in parallel--no need for expensive single units


- Weaknesses: 

  - programming complexity, more modification from serial to parallel

  - bottlenecked by network communications 


**Hybrid Memory Model** 

- Memory shared locally between CPUs/GPUs (which use their own parallel programming language e.g. OpenCL,Cuda) 

- Characteristic of most modern super-computers

- Increased programming complexity as a downside


### Threads in Parallel Computing

- single process, multiple threads (P(POSIX)threads & OpenMP)

  - main process/program loads all of the system & user resources

  - the main program will do partial serial work typically at the beginning and the end of its duration 

    - in the middle: multiple threads are assigned to carry out various tasks, **all utilizing the memory space of the main program**

    - this reduces the overhead resulting from replicating the entire program's resources for sub-routines within the program 

    - the threads can execute their tasks cocurrently or at different times

    - threads communicate with each other via global memory (to update address locations)---requires synchronization, so that not more than one thread is updating the same global address at the same time



 

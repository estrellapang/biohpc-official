## Essential Configurations & Requirements for Headnode


### Very Base Setup for Master Host/Headnode


1. add a proxy in both `/etc/profile.d`(source it) and `/etc/yum.conf` (for UTSW users) 

2. add the EPEL repository to your repolist:

`sudo yum install http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm`
 
3. `sudo yum install net-tools` (install the `ifconfig` command)

4. add DNS server

- check `/etc/resolv.conf` --> typically, if the Ethernet was enabled prior to installation, the Network Manager will automatically registerthe IP addr of the local nameserver. You may also add additional i.e. google DNS 8.8.8.8

5. `sudo yum update`
 
6. setup NTP service

`sudo yum install ntp`

add line `server 192.168.54.1` to `/etc/ntp.conf` --> this is the UTSW/BioHPC NTP 

- allow firewalld to accept UDP packets for ntpd 

`sudo firewall-cmd --add-service=ntp --permanent`

`sudo firewall-cmd --reload` 

`sudo systemctl status ntpd`

`sudo systemctl enable ntpd`

`sudo systemctl restart ntpd`

- to check timekeeping w/r to your NTP servers, execute: `ntpq -p` 

7. assign static IP + hostname 

`sudo vi /etc/hostname` --> insert your machine/domain name --> this will take effect upon restart

- necessary elements in network configuration files:

`sudo vi /etc/sysconfig/network-scripts/ifcfg-enp0s#`

``` 
TYPE=Ethernet
BOOTPROTO=static
IPADDR=192.168.56.#
ONBOOT=yes
GATEWAY=192.168.56.1
NETMASK=255.255.255.0
HWADDR="" 

```
-don't forget to `sudo systemctl restart network`'

7. check distro release version `cat /etc/redhat-release` -OR- `~ ~ /centos-release` 


-**disable SELinux**: use `selinuxenabled` to check whether it is currently enabled

`sudo sestatus` --> check the current status of SElinux 

```
$ sudo sestatus

SELinux status:                 enabled
SELinuxfs mount:                /sys/fs/selinux
SELinux root directory:         /etc/selinux
Loaded policy name:             targeted
Current mode:                   enforcing
Mode from config file:          enforcing
Policy MLS status:              enabled
Policy deny_unknown status:     allowed
Max kernel policy version:      31

```

`sudo vi /etc/selinux/config` 

--> change the entry `SELINUX=enforcing` to `SELINUX=disabled` --> restart the host for effect to take place

`sudo getenforce` --> check status after rebooting 

-**disable firewall**:

`sudo systemctl disable firewalld` 

`sudo systemctl stop firewalld` 

&nbsp;

### Install OpenHPC-related Components

-**adding the OpenHPC repository** (the EPEL repository is a prerequisite) 

`sudo yum install ohpc-release`

-OR- 

`sudo yum install https://github.com/openhpc/ohpc/releases/download/v1.3.GA/ohpc-release-1.3-1.el7.x86_64.rpm` 

`yum repolist` --> check installed repos: 

```
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
epel/x86_64/metalink                                                                                                 |  16 kB  00:00:00     
 * base: centos.host-engine.com
 * epel: archive.linux.duke.edu
 * extras: centos.mirror.lstn.net
 * updates: centos.mirror.lstn.net
OpenHPC                                                                                                              | 1.6 kB  00:00:00     
OpenHPC-updates                                                                                                      | 1.2 kB  00:00:00     
base                                                                                                                 | 3.6 kB  00:00:00     
epel                                                                                                                 | 4.7 kB  00:00:00     
extras                                                                                                               | 3.4 kB  00:00:00     
updates                                                                                                              | 3.4 kB  00:00:00     
(1/5): OpenHPC/group_gz                                                                                              | 1.7 kB  00:00:00     
(2/5): OpenHPC-updates/primary                                                                                       | 316 kB  00:00:00     
(3/5): OpenHPC/primary                                                                                               | 155 kB  00:00:00     
(4/5): epel/x86_64/updateinfo                                                                                        | 960 kB  00:00:00     
(5/5): epel/x86_64/primary_db                                                                                        | 6.6 MB  00:00:01     
OpenHPC                                                                                                                             821/821
OpenHPC-updates                                                                                                                   1769/1769
repo id                                             repo name                                                                         status
OpenHPC                                             OpenHPC-1.3 - Base                                                                   821
OpenHPC-updates                                     OpenHPC-1.3 - Updates                                                              1,769
base/7/x86_64                                       CentOS-7 - Base                                                                   10,019
epel/x86_64                                         Extra Packages for Enterprise Linux 7 - x86_64                                    12,909
extras/7/x86_64                                     CentOS-7 - Extras                                                                    371
updates/7/x86_64                                    CentOS-7 - Updates                                                                 1,103
repolist: 26,992

``` 

-**install base OpenHPC package & Warewulf provisioning system** 

`sudo yum install ohpc-base ohpc-warewulf`

-**install SLURM workload management**

`sudo yum install ohpc-slurm-server`

`sudo perl -pi -e "s/ControlMachine=\S+/ControlMachine=head.cluster/" /etc/slurm/slurm.conf` 

--> this was to identify the resource manager hostname on the master host

--> VERIFY: `sudo cat /etc/slurm/slurm.conf | grep -i 'ControlMachine'` 


-**additional Warewulf setups** 

  - define the network interface that you wish for Warewulf to utilize for provisioning; choose the network interface connecting to the internal/management network (by default, Warewulf is configured to provision over "eth1") 

`sudo perl -pi -e "s/device = eth1/device = enp0s8/" /etc/warewulf/provision.conf`  

--> VERIFY `sudo cat /etc/warewulf/provision.conf | grep -i 'network device'`

  - enable tftp service to distribute image to compute node 

`sudo perl -pi -e "s/^\s+disable\s+= yes/ disable = no/" /etc/xinetd.d/tftp` 

  - enable the internal interface specified above 

--> VERIFY: `sudo cat /etc/xinetd.d/tftp | grep -i 'disable'`

`sudo ifconfig enp0s8 192.168.56.7 netmask 255.255.255.0 up` --> make sure your NIC is up


### for DHCP & PXE boot setup, refer to `pxe.md` within the same directory

## Setting Up the Basic Image for Compute Nodes


### Build Minimal Image via Warewulf


- make a directory location in which a directory structure representation for the compute nodes will be generated: 

`sudo mkdir -p /opt/ohpc/admin/images/centos7_6`

- define the chroot location: 

`export CHROOT=/opt/ohpc/admin/images/centos7_6` --> repeat same command starting with 'sudo'

**CAUTION**:

- if the system is rebooted, one will have to redefine CHROOT! (`export CHROOT=...`) 

- similarly, if you are working on the machine remotely via SSH, upon reconnecting to your remote server after exiting your initial session, REDEFINE CHROOT! 

  - Upon logging in via SSH or rebooting, it is best to FIRST VERIFY  `echo $CHROOT`

- build initial chroot jail/environment via Warewulf

`sudo wwmkchroot centos-7 $CHROOT`

  - this needs access to external repositories e.g. `mirror.centos.org`; if headnode cannot access external repos, then a local cached mirror has to be defined via `export YUM_MIRROR=${your desired local repo}` 

### Dress Up your Minial Image

`sudo cp -p /etc/resolv.conf $CHROOT/etc/resolv.conf` --enable the chroot environment with DNS resolution in order to **access remote repositories** 

`-p` --> `--preserve` option: retain the same ownership & timestamps

- include headnode IP in the $CHROOT /etc/hosts file (**ignore adding the hostname step**)

`cd $CHROOT/etc` 

`sudo vi hosts` 

--> add `192.168.56.7 headnode head.cluster`

- **install `ohpc-slurm-client` in the CHROOT environment**:

`sudo yum --installroot=$CHROOT install ohpc-slurm-client`

`--installroot` allows yum to install the package ONLY in the CHROOT environment

- [more on --installroot option](https://www.computerhope.com/unix/yum.htm)  

```

============================================================================================================================================
 Package                                  Arch                 Version                                  Repository                     Size
============================================================================================================================================
Installing:
 ohpc-slurm-client                        x86_64               1.3.6-14.1.ohpc.1.3.6                    OpenHPC-updates               2.3 k
Installing for dependencies:
 hwloc-libs                               x86_64               1.11.8-4.el7                             base                          1.6 M
 libtool-ltdl                             x86_64               2.4.2-22.el7_3                           base                           49 k
 mariadb-libs                             x86_64               1:5.5.60-1.el7_5                         base                          758 k
 munge-libs-ohpc                          x86_64               0.5.13-6.1.ohpc.1.3.6                    OpenHPC-updates                50 k
 munge-ohpc                               x86_64               0.5.13-6.1.ohpc.1.3.6                    OpenHPC-updates               115 k
 numactl-libs                             x86_64               2.0.9-7.el7                              base                           29 k
 ohpc-filesystem                          noarch               1.3-26.1.ohpc.1.3.6                      OpenHPC-updates               3.4 k
 perl                                     x86_64               4:5.16.3-294.el7_6                       updates                       8.0 M
 perl-Carp                                noarch               1.26-244.el7                             base                           19 k
 perl-Encode                              x86_64               2.51-7.el7                               base                          1.5 M
 perl-Exporter                            noarch               5.68-3.el7                               base                           28 k
 perl-File-Path                           noarch               2.09-2.el7                               base                           26 k
 perl-File-Temp                           noarch               0.23.01-3.el7                            base                           56 k
 perl-Filter                              x86_64               1.49-3.el7                               base                           76 k
 perl-Getopt-Long                         noarch               2.40-3.el7                               base                           56 k
 perl-HTTP-Tiny                           noarch               0.033-3.el7                              base                           38 k
 perl-PathTools                           x86_64               3.40-5.el7                               base                           82 k
 perl-Pod-Escapes                         noarch               1:1.04-294.el7_6                         updates                        51 k
 perl-Pod-Perldoc                         noarch               3.20-4.el7                               base                           87 k
 perl-Pod-Simple                          noarch               1:3.28-4.el7                             base                          216 k
 perl-Pod-Usage                           noarch               1.63-3.el7                               base                           27 k
 perl-Scalar-List-Utils                   x86_64               1.27-248.el7                             base                           36 k
 perl-Socket                              x86_64               2.010-4.el7                              base                           49 k
 perl-Storable                            x86_64               2.45-3.el7                               base                           77 k
 perl-Text-ParseWords                     noarch               3.29-4.el7                               base                           14 k
 perl-Time-HiRes                          x86_64               4:1.9725-3.el7                           base                           45 k
 perl-Time-Local                          noarch               1.2300-2.el7                             base                           24 k
 perl-constant                            noarch               1.27-2.el7                               base                           19 k
 perl-libs                                x86_64               4:5.16.3-294.el7_6                       updates                       688 k
 perl-macros                              x86_64               4:5.16.3-294.el7_6                       updates                        44 k
 perl-parent                              noarch               1:0.225-244.el7                          base                           12 k
 perl-podlators                           noarch               2.5.1-3.el7                              base                          112 k
 perl-threads                             x86_64               1.87-4.el7                               base                           49 k
 perl-threads-shared                      x86_64               1.43-6.el7                               base                           39 k
 pmix-ohpc                                x86_64               2.1.4-2.1.ohpc.1.3.6                     OpenHPC-updates               3.3 M
 slurm-contribs-ohpc                      x86_64               17.11.10-6.2.ohpc.1.3.6                  OpenHPC-updates                17 k
 slurm-example-configs-ohpc               x86_64               17.11.10-6.2.ohpc.1.3.6                  OpenHPC-updates               187 k
 slurm-ohpc                               x86_64               17.11.10-6.2.ohpc.1.3.6                  OpenHPC-updates                12 M
 slurm-pam_slurm-ohpc                     x86_64               17.11.10-6.2.ohpc.1.3.6                  OpenHPC-updates               141 k
 slurm-perlapi-ohpc                       x86_64               17.11.10-6.2.ohpc.1.3.6                  OpenHPC-updates               798 k
 slurm-slurmd-ohpc                        x86_64               17.11.10-6.2.ohpc.1.3.6                  OpenHPC-updates               617 k

Transaction Summary
============================================================================================================================================
Install  1 Package (+41 Dependent packages)

Total download size: 31 M
Installed size: 115 M

```

- VERIFY your install: 

`sudo yum --installroot=$CHROOT list installed | grep -i 'ohpc-slurm-client'` --> if installation successful, you should get the following output:

> ohpc-slurm-client.x86_64           1.3.6-14.1.ohpc.1.3.6    @OpenHPC-updates 

- VERIFY outside your CHROOT environment: the package should not be in there: 

`yum list installed | grep -i 'ohpc-slurm-client'` --> you should not receive any results


- Install `ntp` on $CHROOT environment

`sudo yum --installroot=$CHROOT install ntp`

```
============================================================================================================================================
 Package                             Arch                       Version                                      Repository                Size
============================================================================================================================================
Installing:
 ntp                                 x86_64                     4.2.6p5-28.el7.centos                        base                     549 k
Installing for dependencies:
 autogen-libopts                     x86_64                     5.18-5.el7                                   base                      66 k
 ntpdate                             x86_64                     4.2.6p5-28.el7.centos                        base                      86 k

Transaction Summary
============================================================================================================================================
Install  1 Package (+2 Dependent packages)

```

- Add kernel drivers, & modules user environment to $CHROOT 

`sudo yum --installroot=$CHROOT install kernel`

```

============================================================================================================================================
 Package                           Arch                      Version                                       Repository                  Size
============================================================================================================================================
Installing:
 kernel                            x86_64                    3.10.0-957.5.1.el7                            updates                     48 M
Installing for dependencies:
 grubby                            x86_64                    8.28-25.el7                                   base                        70 k
 linux-firmware                    noarch                    20180911-69.git85c5d90.el7                    base                        49 M

Transaction Summary
============================================================================================================================================

```

- the following messages returned during the installation:

```

grep: /proc/cmdline: No such file or directory
No '/dev/log' or 'logger' included for syslog logging
Turning off host-only mode: '/sys' is not mounted!
Turning off host-only mode: '/proc' is not mounted!
Turning off host-only mode: '/run' is not mounted!
Turning off host-only mode: '/dev' is not mounted!

```

- install additional OpenHPC packages to $CHROOT

`sudo yum --installroot=$CHROOT install lmod-ohpc`

```

============================================================================================================================================
 Package                              Arch                    Version                                Repository                        Size
============================================================================================================================================
Installing:
 lmod-ohpc                            x86_64                  7.8.1-5.1.ohpc.1.3.6                   OpenHPC-updates                  219 k
Installing for dependencies:
 lua-bit-ohpc                         x86_64                  1.0.2-1.1                              OpenHPC                          6.9 k
 lua-filesystem-ohpc                  x86_64                  1.6.3-4.1                              OpenHPC                           19 k
 lua-posix-ohpc                       x86_64                  33.2.1-4.1                             OpenHPC                          248 k
 tcl                                  x86_64                  1:8.5.13-8.el7                         base                             1.9 M
 tcsh                                 x86_64                  6.18.01-15.el7                         base                             338 k

Transaction Summary
============================================================================================================================================

```

&nbsp;

- VERIFY Installations are in the Correct Location! 

`sudo yum --installroot=$CHROOT list installed | grep -i 'ntp'`

`sudo yum --installroot=$CHROOT list installed | grep -i 'kernel'`

`sudo yum --installroot=$CHROOT list installed | grep -i 'lmod-ohpc'`


### SSH keys, Databases, File Shares, & NTP 

- Initialize Warewulf database & ssh_keys 

`sudo wwinit database`

```
[zxp8244@head .ssh]$ sudo wwinit database
[sudo] password for zxp8244: 
database:     Checking to see if RPM 'mysql-server' is installed             NO
database:     Checking to see if RPM 'mariadb-server' is installed           OK
database:     Activating Systemd unit: mariadb
database:      + /bin/systemctl -q enable mariadb.service                    OK
database:      + /bin/systemctl -q restart mariadb.service                   OK
database:      + mysqladmin --defaults-extra-file=/tmp/0.ZwSL8VHGF0FQ/my.cnf OK
database:     Database version: UNDEF (need to create database)
database:      + mysql --defaults-extra-file=/tmp/0.ZwSL8VHGF0FQ/my.cnf ware OK
database:      + mysql --defaults-extra-file=/tmp/0.ZwSL8VHGF0FQ/my.cnf ware OK
database:      + mysql --defaults-extra-file=/tmp/0.ZwSL8VHGF0FQ/my.cnf ware OK
database:     Checking binstore kind                                    SUCCESS

```

`wwinit ssh_keys`  --> the ssh keys are generated in `/etc/warewulf/vnfs/ssh/ssh_host_rsa and ~/ssh_host_dsa` 

```
ssh_keys:     Checking ssh keys for root                                     OK
ssh_keys:     Checking root's ssh config                                     OK
ssh_keys:     Checking for default RSA host key for nodes                    NO
ssh_keys:     Creating default node ssh_host_rsa_key:
ssh_keys:      + ssh-keygen -q -t rsa -f /etc/warewulf/vnfs/ssh/ssh_host_rsa OK
ssh_keys:     Checking for default DSA host key for nodes                    NO
ssh_keys:     Creating default node ssh_host_dsa_key:
ssh_keys:      + ssh-keygen -q -t dsa -f /etc/warewulf/vnfs/ssh/ssh_host_dsa OK
ssh_keys:     Checking for default ECDSA host key for nodes                  NO
ssh_keys:     Creating default node ssh_host_ecdsa_key:
                                                                             OK
ssh_keys:     Checking for default Ed25519 host key for nodes                NO
ssh_keys:     Creating default node ssh_host_ed25519_key:
                                                                             OK'
```

- **Add NFS client and mount Shared Filesystems to base image** 

`su -` --> switch to rootuser, b.c. of the following occurence: 

```
[zxp8244@head ~]$ sudo echo "headnode:/home /home nfs nfsvers=3,nodev,nosuid,noatime 0 0" >> $CHROOT/etc/fstab
-bash: /opt/ohpc/admin/images/centos7_6/etc/fstab: Permission denied

```

**IMPORTANT** If you were previously working from a user account with sudo rights, you MUST define $CHROOT once more! 
(I did not know that variable declarations, unless specified as global, are exclusive to their respective accounts---which is a good thing) 

`export CHROOT=/opt/ohpc/admin/images/centos7_6`

`echo "head.cluster:/home /home nfs nfsvers=3,nodev,nosuid,noatime 0 0" >> $CHROOT/etc/fstab` 
 
`echo "head.cluster:/opt/ohpc/pub /opt/ohpc/pub nfs nfsvers=3,nodev,nosuid,noatime 0 0" >> $CHROOT/etc/fstab`

- more info on export file options

`nodev` option --> specifies that the filesystem cannot contain special devices, which can expose clients to access random device hardware

`nosiud` option --> filesystem cannot contain set userid files--this prevents potential risk of root escalation  

`noatime` option --> instructs filesystem to not record the last accessed date of files--this is mainly to increase read and write time

--> VERIFY: `cat $CHROOT/etc/fstab`

- Export `/home` and `/opt/ohpc/pub` directories from the headnode to chroot environment 

`sudo echo "/home *(rw,no_subtree_check,fsid=10,no_root_squash)" >> /etc/exports`

`sudo echo "/opt/ohpc/pub  *(ro,no_subtree_check,fsid=11)" >> /etc/exports`

`exportfs -a`

- export options: `no_subtree_check`--> prevents problems often resulting from the default subtree_check setting for when a requested files are renamed while the clients have the files open. `fsid` is simply an additional effort to help the the NFS server how to identify an exported file system, each of which should have a unique fsid; **fsid's range from 0 - 255**'`fsid=0` has special meaning in NFS version 4, whichrepresents the top/pseudo root directory of the exported hierarchy. 

[exports man page to review all these options' meanins'](https://linux.die.net/man/5/exports)

`CTRL + D` (NOTE: if you log out of the superuser, upon re-logging in as root, the $CHROOT value will be WIPED and require re-definition; it is honestly better to just stay as root user the entire time during the configuration process..the same applies to regular users only upon reboot or existing SSH session) 

- stay as the root user and doublecheck $CHROOT value

`systemctl status nfs-server` 

`systemctl enable nfs-server` 

- **also enable nfs-server in the chroot environment!** 

`sudo chroot $CHROOT systemctl enable nfs-server`

- **Appoint the Head Node as the local NTP server**: 

`chroot $CHROOT systemctl enable ntpd` 

`echo "server head.cluster" >> $CHROOT/etc/ntp.conf` 

- **note**: if your compute nodes are not meant to connect to the external network; then you should delete the entries listed by default in `ntp.conf` & only leave the headnode IP/hostname in the file 

  - VERIFY: `cat $CHROOT/etc/ntp.conf`

- Import Credential Files and Global Slurm Configurations: 

```

sudo wwsh file import /etc/passwd
sudo wwsh file import /etc/group
sudo wwsh file import /etc/shadow
sudo wwsh file import /etc/munge/munge.key
sudo wwsh file import /etc/slurm/slurm.conf

```

**NOTICE**: every time the contents of the following files are modified, the following files need to be re-imported

&nbsp;

### Finalizing Base Image 

- Bootstrap Image

`su -` ; `export CHROOT=/opt/ohpc/admin/images/centos7_6` 

`sudo echo "drivers += updates/kernel/" >> /etc/warewulf/bootstrap.conf` 

`sudo echo "drivers += overlay" >> /etc/warewulf/bootstrap.conf`--> include drivers from kernel updates & overlayfs drivers 

- Build Bootstrap Image 

`wwbootstrap 'uname -r'` --> NOTE! The marks around "uname -r" are NOT SINGLE QUOTES, instead, they are **back ticks!**  

```
[root@head ~]# wwbootstrap `uname -r`
Number of drivers included in bootstrap: 540
Number of firmware images included in bootstrap: 96
Building and compressing bootstrap
Integrating the Warewulf bootstrap: 3.10.0-957.5.1.el7.x86_64
Including capability: provision-adhoc
Including capability: provision-files
Including capability: provision-selinux
Including capability: provision-vnfs
Including capability: setup-filesystems
Including capability: setup-ipmi
Including capability: transport-http
Compressing the initramfs
Locating the kernel object
Bootstrap image '3.10.0-957.5.1.el7.x86_64' is ready
Done.

```

- Assemble Virtual Node File System (VNFS) image 

`wwvnfs --chroot $CHROOT` 

> what are VNFS & overlayfs? 

```
[root@head ~]# wwvnfs --chroot $CHROOT
Using 'centos7_6' as the VNFS name
Creating VNFS image from centos7_6
Compiling hybridization link tree                           : 0.24 s
Building file list                                          : 0.57 s
Compiling and compressing VNFS                              : 83.48 s
Adding image to datastore                                   : 11.20 s
Wrote a new configuration file at: /etc/warewulf/vnfs/centos7_6.conf
Total elapsed time                                          : 95.48 s 

``` 


### Additional Provisioning configurations are listed in `image2.md`


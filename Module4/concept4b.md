## More Concepts that Originate from Computer Clusters

### Chroot 

> a major concept behind containers & how multiple web-servers w/h multiple domains can be hostd> on the same machine.
**what it does**:

- change root directory of the current process (as well as its children) being run

  - programs run in the chroot environment cannot access files outside it

**How it works**: 

- chroot creates the aforementioned directory by copying all of the system files needed for the a process to run 

  -  start a process at the base of the chroot environment, so that it cannot reference paths outside i.e. reading or writing to such locations

- all processes inside chroot environment/jail are user-level processes in the host OS, but are isolated by the namespaces provided by the kernel 

**How is it different from virtual machines**:

- a chroot jail is a OS/Kernel-level virtualization, and it is often used to create multiple isolated instances of the host OS. 

- unlike Virtual Machines, chroot jails have no overhead

- VMs are a software/application layer virtualization, and they utilize elements of the Hardware virtualization to procure virtual images of working operating systems 

	look up components of operating systems 

[explanation 1 w/h guide on adding commands to the chroot jail](https://www.geeksforgeeks.org/linux-virtualization-using-chroot-jail/) 

**Bottom Line**

- `chroot` changes root directory for a process and its children process; but it alone does not 
provide an entirely isolated environment for testing; relative paths within the chroot can still refer to locations outside of the new root

### Cgroups 

> "Control Groups"--a Linux kernel feature that controls & isolates resources (e.g. CPU, memory, disk I/O, network, etc.) for specific processes. 

**Summary of Functions** 

- Overview: control resources for single processes to OS-level virtualization via the following ways:

  - Resource Capping: e.g. groups cannot exceed specified memory & file system cache 

  - Prioritization: which groups have prioritized access or larger shares of CPU or disk I/O throughput 

  - Summarization: measuring each group's usage

  - Direct Management: start/stop, checkpointing. 

**Usage** 

- Manually

  - via tools i.e. cgcreate, cgexec, & cgclassify 

- Indirectly/Automatically 

  - rules engine daemon, Docker, Linux Containers Virtualization (LXC), libvirt, systemd, etc. 

[example of cgroup usage here](https://www.geeksforgeeks.org/linux-virtualization-resource-throttling-using-cgroups/)

### Linux Namespaces

> a kernel feature that has been around since the early 2000's (see timeline in Lynda course on > Kubernetes?), but has recently been capitalized extensively by the craze over containerization> . Essentially, namespaces allow isolation of processes from the rest of the system (or from each other), which is v> ery powerful in faciliating operating-system-level virtualization.   

[detailed history & description here](https://lwn.net/Articles/531114/) 

[shorter explanation w/h the unshare command](https://medium.com/@teddyking/linux-namespaces-850489d3ccf) 

[another relatively good overview w/h more command ops](https://prefetch.net/blog/2018/02/22/making-sense-of-linux-namespaces/) 

[last but not the least, the official man page](http://man7.org/linux/man-pages/man7/namespaces.7.html) 

**Primary Function**

- Process isolation 

- OS-level virtualization 

**7 Current Namespaces in Linux** 

       Namespace   Constant          Isolates
       Cgroup      CLONE_NEWCGROUP   Cgroup root directory
       IPC         CLONE_NEWIPC      System V IPC, POSIX message queues
       Network     CLONE_NEWNET      Network devices, stacks, ports, etc.
       Mount       CLONE_NEWNS       Mount points
       PID         CLONE_NEWPID      Process IDs
       User        CLONE_NEWUSER     User and group IDs
       UTS         CLONE_NEWUTS      Hostname and NIS domain name

- Examples of what can be achieved with the use of each one: 

  - PID namespace allows two or more processes running on the same host to have the same PID

  - Mount namespace allows mounting/unmounting filesystems without affecting those mounted on the host (even if they are named identically outside the containers)

  - Networking namespaces allow programs to be run on any port without having to share or conflict with those that are using the same ports outside the namespace

  - [more details on RHEL 7 deployment guide](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux_atomic_host/7/html/overview_of_containers_in_red_hat_systems/introduction_to_linux_containers)

 

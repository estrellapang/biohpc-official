## Info on Slurm Commands & Scripts

> Important Daemons: Controller-**slurmctld**; Compute Nodes-**slurmd**

### Modify `slurm.conf` on Headnode

`sudo cp /etc/slurm/slurm.conf /etc/slurm/slurm.conf.bkup` # in case you mess up, have a back up file

- at the bottom of the page, under "Compute Nodes" make the following changes:

```
NodeName=replicant[1-2]  # correct your compute nodes name

Nodes=c[1-2] # formerly "Nodes=c[1-4]" # restrict nodes 1-2 as compute nodes

```

`sudo chroot $CHROOT systemctl enable slurmd` enable it in the **"image"**, or the chroot environment

`sudo systemctl enable slurmctld` enable it on the **headnode**

```
[zxp8244@head ~]$ sudo wwsh file import /etc/slurm/slurm.conf
Overwrite existing file object "slurm.conf" in the data store?
Yes/No [no]> yes

```

`sudo wwnfs --chroot $CHROOT` --> assemble(or Reassemble) image

> **REFER to file "provisioning1.md" to complete the image set-up** 

&nbsp; 

### Batch Scripts 

	Comprise of two main sectors: 1. Directives (starting with #SBATCH) 2. Commands 


-**Crucial Directive Entries**: 

  - `--job-name` 

  - `--partition` / `-p` 

  - `--nodes` / `-N` # number of nodes 

  - `-n` # number of cores 

  - `--time` / `-t` # time limit 

  - `--mem` # memory allocation 

    - e.g. `--mem=1G` #the use of equal sign is not mandatory e.g. `--mem 1G` 

  - `--output` \ `-o` # STDOUT/output file 

  - `--error` \ `-e` #STDERR/errot file 

    - e.g. `--output slurm.%N.%j.out` 

  - `--mail-user` #put user E-mail address

  - `--mail-type` #options: "BEGIN, END, FAIL" 

    - e.g. `--mail-type BEGIN,END,FAIL` #no need to include all three 


-**example of my first slurm job** 


```
#!/bin/bash
BATCH --job-name="first ever"
#SBATCH --partition=normal
#SBATCH --nodes=1
#SBATCH --mem= 500M
#SBATCH -time=00:01:50 # time (D-HH:MM)
#SBATCH -o slurm.%N.%j.out # STDOUT
#SBATCH -e slurm.%N.%j.err # STDERR
for i in {1..100}; do
echo $RANDOM >> /home/zxp8244/SomeRandomNumbers.txt
done;
sort -n -o /home/zxp8244/SomeSortedNumbers.txt /home/zxp8244/SomeRandomNumbers.txt
~                                                                                       

```

### Slurm Commands 

#### Controlling Nodes 

`sinfo` --> list status of all nodes & partitions 

`sudo scontrol update NodeName=replicant[1-2] state=[idle/resume/drain]` 

- `state=idle` --> when no jobs are currently running on the node(s) 

- `state=resume` --> when THERE ARE jobs currently running on the nodes(s) 

- `state=drain` --> this option requires a reason, e.g. `state=drain reason="testing y'all"`

  - e.g. `sudo scontrol update nodename=replicant[1-2] state=resume` 

  - check the status of nodes frequently; they tend to get drained after few job runs `sinfo`

- `scontrol show node` - show detailed info on compute nodes 

#### List Information on Jobs 

`sudo sbatch [script file]` --> submit your jobs 

  - **estimate run time** --> `sbatch --test-only [script file]` 

`squeue -u [username]` --> list all current jobs for user 

  - `squeue -u [username] -t RUNNING` --> ~ running 

  - `squeue -u [username] -t PENDING` --> ~ pending 

`showq-slurm -o -u -q [partition name]` --> list jobs for specific partitions; in the order of priority  

`squeue -u [username] -p [partition name]` --> list jobs for ~ & specific users 

`scontrol show jobid -dd [job id]` --> show details on jobs 

`sstat --format=AveCPU,AvePages,AveRSS,AveVMSize,JobID -j [jobid] --allsteps` --> status info for any job 

#### Post-Job Information 

`sacct -j [jobid] --format=JobID,JobName,MaxRSS,Elapsed` --> actual run time, memory utilization, blah blah 

`sacct -u [username] --format=JobID,JobName,MaxRSS,Elapse` --> ~ for all jobs from a user

### Controlling Jobs 

`sudo scancel <jobid>` --> cancel a job 

`scancel -u <username>` --> cancel all jobs for a user 

`scancel -t PENDING -u <username>` --> cancel all pending jobs for a user

`scancel --name [myJobName]` --> cancel a job by name

`scontrol hold <jobid>` --> hold a job in the queue from being scheduled to run 

`scontrol release <jobid>` --> release the hold

`scontrol requeue <jobid>` --> cancel & rerun a job 

[more detail i.e. listing/controlling jobs as groups/arrays here](https://www.rc.fas.harvard.edu/resources/documentation/convenient-slurm-commands/)


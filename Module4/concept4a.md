## Introduction to HPC Clusters

![alt text](https://www.hpc.iastate.edu/sites/default/files/uploads/HPCHowTo/HPCCluster.JPG)

![alt text](https://cdn-images-1.medium.com/max/1600/1*cxO0ysn8US__38hbO60tCQ.jpeg) 

### Categories & Definitions

**High Performance Cluster** 

- utilize different resources towards COMMON goal 

- concurrent calculations 

- also called "grid computing" --- because the nodes do not have to belong toone specific location 

- focus is on high I/O 

**Storage Cluster** 

- hosts on the cluster all access the same file system 

- read & write concurrentlyonto the storage (requires parallel storage) 

- applications, & authentications all located on one system 

**High Availability Cluster** 

- overall goal to reduce the possibility of down-time 

- does not have SINGLE-POINT FAILURES

- failover service that transfer role to another node, and also preserve the data 

**Load Balancing Cluster**

- can balance load across many nodes 

- requires Load Balancing software (similar to job scheduler?) 

- also has failover attribute 

 

**Fencing** -- the technique through which the head node can disconnect a faild node from the cluster's central processing and shared storage via fencing agent/devices (sometimes this is through power control--called "power fencing; fibre channel fencing; the connection will be blocked from the switch, and therefore will not connect to the headnode, etc)

 

	what is Lock Management? 





- importance of parallel file systems on HPC clusters
- Symmetrical MultiProcessing (SMP)--how memory on a single node is shared among different threads of a job (a parallelism component) 
- Message Passing Interface (MPI)--how single jobs are split into multiple nodes (a parallelism component)

  - MPI is a protocol --> based on the message passing model

  - MPI is performance oriented: - utilizes the fastest transport available when sending messages (TCP/IP typically the last resort)

  - includes both point to point, broadcast, and reduction communications 

  - good for carrying out a SINGLE hefty task split among several nodes of similar specs 

  - bindings fro Fortran, C, Python, R, etc. 

  - [good video explaining MPI](https://www.youtube.com/watch?v=kHV6wmG35po) 

 
- scheduling systems & queues (keeping track of how many processors are active/inactive, # of tasks left in queue, how much resources to dedicate to each job)

- network: latency (amount of time taken for messages to transfer from one computer to another on the cluster (measured in micro or nano secs, preferrably low) bandwidth --- how much data a network can move/time unit 

- what is failover clustering? 

[a bit of DHCP failover here](https://blogs.technet.microsoft.com/teamdhcp/2012/06/28/ensuring-high-availability-of-dhcp-using-windows-server-2012-dhcp-failover/)  

- high availability primarily a networking concept? 

  - if BioHPC manually assign static IPs, then is there a need for DHCP service?

- softwares/protocols: DNS, HTTP, DHCP, TFTP, NFS, IPMI, Puppet  

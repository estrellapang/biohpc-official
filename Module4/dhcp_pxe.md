## Configuring Master Node to act as PXE (Network Boot) Installation Server on CentOS/RHEL 7 

> PXE (Preboox eXecution Environment -- allows for automated OS installation over the network. This eliminates the need for manual, bootable> drives and allows for efficient provisioning of large numbers of hosts. PXE operates in a server-client architecture; therefore, in order > for the head node to provision compute nodes in a cluster, it will have to be configured to become a PXE host/server 

### General Steps 

1. Install required packages 

2. Configure DHCP Server  

3. Config PXE/tftp server (xinetd.d?)

4. Mount CentOS/RHEL ISO file into /mnt/ & copy syslink contents (no need for OpenHPC deployment), as well as vmlinuz and initrd,img into the tftp server 

5. Create kickStart (no need for it for OpenHPC deployment) 

6. Enable xinetd, dhcp, vsftpd.

### Details 

**1** 

`sudo yum install dhcp` --> DHCP server 

`sudo yum install tftp` 

`sudo yum install tftp-server` 

`sudo yum install vsftpd` --> the three commands above are a part of the "very secure FTP daemon" package, which includes the FTP server 

`sudo yum install syslinux` --> install SYSLINUX  bootloader (might not be needed? included in the PXE boot resides a bootloader, I think?) 

`sudo yum install xinetd` --> (Extended Internet Service Daemon) -- primarily listens for requests over specific ports and launches services based on request; commonly used to manage FTP & Telnet


**2**

- copy the example configuration file (to use as template)  to the `/etc/dhcp` directory 
`sudo cp /usr/share/doc/dhcp-4.2.5/dhcpd.conf.example /etc/dhcp/dhcpd.conf`

`su -` 

`vi /etc/dhcp/dhcpd.conf`

```

# dhcpd.conf
# 
# basic configurations 

authoritative;
allow booting;
allow bootp;
allow unknown-clients; 
ddns-update-style interim;
ignore client-updates;

# network settings 

subnet 192.168.56.0 subnet 255.255.255.0 {
option domain-name-servers 192.168.54.1; 
option domain-name ".cluster"; #this option specifies the domain name that **clients** should use
option broadcast-address 192.168.56.255;
option routers 192.168.56.1;
default-lease-time 600;
max-lease-time 7200;
range 192.168.56.10 192.168.56.20;
next-server 192.168.56.7;
filename "pxelinux.0"; # this file will be copied to the appropriate location in the upcoming step 
} 

``` 

[list of dhcpd options](https://linux.die.net/man/5/dhcpd-options)

**3** 

- check tftp server configurations: TFTP (Trivial File Transfer Protocol ) is used to transfer files from data server to its clients without any kind of authentication. In case of PXE server setup tftp is used for bootstrap loading 

`sudo vi /etc/xinetd.d/tftp` --->  the only parameter that needs to be changed is the entry `disable = [ ]`: --->  change it to `disable = no` 

- **move `pxelinux.0`, the PXE boot file to `var/lib/tftpboot` 

`sudo cp /usr/share/syslinux/pxelinux.0 /var/lib/tftpboot` 

	PXE booting process would not work without `pxelinux.0` and the **PXE configuration file** located within **/var/lib/tftpboot/pxelinux.cfg** !!! 

- **PXE Booting Rundown** 

1. The pxelinux boot file pxelinux.0 is loaded

2. The tftp root pxelinux.cfg directory is scanned for a configuration file 

3. If a configuration file is located then the commands within it will be executed (e.g. a boot menu will be displayed; default option executed; etc).

4. If a configuration file is not found the boot process will halt with error    

- **PXE configurations** 

 - make the `pxelinux.cfg` directory `sudo mkdir /var/lib/tftpboot/pxelinux.cfg` 

 - touch a configuration file `sudo touch /var/lib/tftpboot/pxelinux.cfg/default` 

  - there are in general 3 naming options for this PXE configuration file, however, naming it `default` is the lest complicated method--refer to links below for more info

  - [PXE configuration from vmware](https://pubs.vmware.com/vsphere-4-esx-vcenter/index.jsp?topic=/com.vmware.vsphere.installclassic.doc_41/install/boot_esx_install/c_about_pxe_config_files.html) 

  - [pxe configuration from RHEL 6 deployment guide](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/installation_guide/s1-netboot-pxe-config) 


  - [explanation on vmlinuz](https://askubuntu.com/questions/452070/what-is-vmlinuz-file-on-live-ubuntu) 

  - [info on initrd](https://www.ibm.com/developerworks/library/l-initrd/) 

  - [info on vmlinuz 1](https://www.linuxquestions.org/questions/linux-software-2/what-is-vmlinuz-and-what-does-it-do-136823/)



 - modify `default`:  

```
vi /var/lib/tftpboot/pxelinux.cfg/default

[this portion of the documentation has NOT BEEN COMPLETED]


- start services

`sudo systemctl restart xinetd` 
`sudo systemctl restart mariadb` 
`sudo systemctl restart dhcpd.service`
`sudo systemctl restart vsftpd`

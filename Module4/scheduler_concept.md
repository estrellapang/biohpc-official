## Understanding of Cluster Layout/Infrastructure 

### Login Nodes 

- connected to network outside the cluster

- compile & edit files 

- submit jobs 

### Compute Nodes 

- normally no direct access

- scheduler utilizes them to run jobs 

### Admin & Storage Nodes 

### Scheduler 

> collect parameters of jobs & prioritizes them 

- Cycle through jobs once/min

- Pre-Job Actions: scanning nodes for resource availability;decides if jobs can be run given the current availability

- On-Job Actions: utilizes scheduler agent that is installed on each compute node for monitoring the running processes:

  - memory usage 

  - time taken 

  - whether to kill 

- Prioritization will prefer smaller, shorter jobs 

  - avoid submitting a series of long jobs 

    - big jobs are "picky" and tend to get delayed 

  - jobs may change priority as more are coming in; new schedules are made constantly 

### What's Needed to run A Job? 

- Commands
 
- In/Out/Error Files 

- Runtime & Resource Limits
 
- CPU cores
 
- GPU cores

### Issues Affecting Jobs 

- running out of time (hard to estimate; good to give it a little extra cushion, but not so much that it gets in the way of other jobs) 

  - jobs back-filled? 

- out of memory 

  - `rlimitas`--> most commonly system program to set the memory limit 

  - could happen at process startup (many processes have larger startup memories) `signal 9 segmentation fault` --> typically a memory issue 

- Executable/Library Issues 

  - `Sqsub` propagates environment variables to the job, but sometimes if certain files/libraries/directories are renamed/missing, it interferes with the job 

- I/O 

  - typically compute nodes shouldn't be mounted with `/archive/` or any directory associated with cold storage; but they should be mounted with directories containing necessary files for data & jobs 

OVERALL TIPS: 

- Try serial prior to MPI/threaded submissions 
- Avoid Overestimating Resource Usage 
- Be generous but not-too-Generous with time limits 
- Shun lining up picky jobs in a row 
- Submit jobs early rather than later; queued is better than not 

## Configuring Headnode for Provisioning

> Work as **root**


echo "GATEWAYDEV=enp0s8" > /tmp/network.$$` # identify appropriate network card that will be used for provisioning

`wwsh -y file import /tmp/network.$$ --name network`

`wwsh -y file set network --path /etc/sysconfig/network --mode=0644 --uid=0`

```
[root@head ~]# wwsh -y file set network --path /etc/sysconfig/network --mode=0644 --uid=0
About to apply 3 action(s) to 1 file(s):

     SET: PATH                 = /etc/sysconfig/network
     SET: MODE                 = 0644
     SET: UID                  = 0

Proceed?

# enter "yes" 

```

### Register Compute Nodes

> On the **compute node themselves**, connect them either virtually or physically to the **local switch/host-only network** that which the headnode is connected to; also configure the **boot order** to PXE first and harddrive second---**also note down the MAC address** for each compute node to manually register them on the headnode see below:

`wwsh -y node new replicant1 --ipaddr=192.168.56.10 --hwaddr=08:00:27:a8:dc:f4 -D enp0s8   #enp0s8 = port that headnode provisions through`

`wwsh -y node new replicant2 --ipaddr=192.168.56.11 --hwaddr=08:00:27:b4:ff:2e -D enp0s8`

`wwsh -y node new replicant3 --ipaddr=192.168.56.12 --hwaddr=08:00:27:b3:7c:6e -D enp0s8`

```
[root@head ~]# wwsh provision set "replicant*" --kargs "net.ifnames=1,biosdevname=1"
Are you sure you want to make the following changes to 5 node(s):

     SET: KARGS                = "net.ifnames=1,biosdevname=1"

Yes/No> yes

```

> "5 Nodes? the commands above clearly registered 3---perhaps there is a default setting somewhere in ohpc-warewulf package?" 

```

[root@head ~]# wwsh provision set --postnetdown=1 "replicant*"
Are you sure you want to make the following changes to 4 node(s):

     SET: POSTNETDOWN          = 1

Yes/No> yes

```

```
[root@head ~]# wwsh provision set "replicant*" --vnfs=centos7_6 --bootstrap=`uname -r` --files=dynamic_hosts,passwd,group,shadow,slurm.conf,munge.key,network
Are you sure you want to make the following changes to 4 node(s):

     SET: BOOTSTRAP            = 3.10.0-957.5.1.el7.x86_64
     SET: VNFS                 = centos7_6
     SET: FILES                = dynamic_hosts,passwd,group,shadow,slurm.conf,munge.key,network

Yes/No> yes

```

- restart above services & daemons 

```
systemctl enable xinetd
systemctl enable httpd.service
systemctl enable vsftpd
systemctl enable mariadb.service
systemctl enable dhcpd.service

```

`wwsh pxe update` --> let Warewulf configure the PXE server

**if a compute node was to be added after the initial setup, repeat the following commands: 

`wwsh -y node new replicant[] --ipaddr=192.168.56.[] --hwaddr=[] -D enp0s8`

`wwsh provision set "replicant*" --vnfs=centos7_6 --bootstrap=`uname -r` --files=dynamic_hosts,passwd,group,shadow,slurm.conf,munge.key,network`   

> note: if new nodes are intended for job processing via SLURM (essentially being a compute node), then the **slurm.conf** will need to be modified to include the new nodes hostname---afterwards, execute `wwsh file import /etc/slurm/slurm.conf` to update the configuration file in the database, **No need to rebuild the image**, as all of the necessary configuration files are stored in the Warewulf database (either remotely or ideally on a remote server). see example of an update in block code below:

```
[zxp8244@head ~]$ sudo wwsh file import /etc/slurm/slurm.conf
[sudo] password for zxp8244: 
Overwrite existing file object "slurm.conf" in the data store?
```

### Configure Headnode for Stateful Provisioning 

`yum --installroot=$CHROOT install grub2` --> install GRUB2 bootloader to the chroot environment

```

============================================================================================================================================
 Package                               Arch                     Version                                     Repository                 Size
============================================================================================================================================
Installing:
 grub2                                 x86_64                   1:2.02-0.76.el7.centos.1                    updates                    31 k
Installing for dependencies:
 file                                  x86_64                   5.11-35.el7                                 base                       57 k
 file-libs                             x86_64                   5.11-35.el7                                 base                      340 k
 freetype                              x86_64                   2.8-12.el7_6.1                              updates                   380 k
 gettext                               x86_64                   0.19.8.1-2.el7                              base                      1.0 M
 gettext-libs                          x86_64                   0.19.8.1-2.el7                              base                      501 k
 grub2-common                          noarch                   1:2.02-0.76.el7.centos.1                    updates                   728 k
 grub2-pc                              x86_64                   1:2.02-0.76.el7.centos.1                    updates                    31 k
 grub2-pc-modules                      noarch                   1:2.02-0.76.el7.centos.1                    updates                   846 k
 grub2-tools                           x86_64                   1:2.02-0.76.el7.centos.1                    updates                   1.8 M
 grub2-tools-extra                     x86_64                   1:2.02-0.76.el7.centos.1                    updates                   995 k
 grub2-tools-minimal                   x86_64                   1:2.02-0.76.el7.centos.1                    updates                   172 k
 libcroco                              x86_64                   0.6.12-4.el7                                base                      105 k
 libgomp                               x86_64                   4.8.5-36.el7                                base                      157 k
 libpng                                x86_64                   2:1.5.13-7.el7_2                            base                      213 k
 libunistring                          x86_64                   0.9.3-9.el7                                 base                      293 k
 os-prober                             x86_64                   1.58-9.el7                                  base                       42 k

Transaction Summary
============================================================================================================================================

```

- Implement partition table parameters to Warewulf configuration: 

```
cp /etc/warewulf/filesystem/examples/gpt_example.cmds /etc/warewulf/filesystem/gpt.cmds

[root@head ~]# wwsh provision set --filesystem=gpt "replicant*"

Are you sure you want to make the following changes to 4 node(s):

       SET: FS                   = select /dev/sda,mklabel gpt,mkpart primary 1MiB 3MiB,mkpart primary ext4 3MiB 513MiB,mkpart primary linux-swap 513MiB 50%,mkpart primary ext4 50% 100%,name 1 grub,name 2 boot,name 3 swap,name 4 root,set 1 bios_grub on,set 2 boot on,mkfs 2 ext4 -L boot,mkfs 3 swap,mkfs 4 ext4 -L root,fstab 4 / ext4 defaults 0 0,fstab 2 /boot ext4 defaults 0 0,fstab 3 swap swap defaults 0 0

Yes/No> yes
[root@head ~]# 
[root@head ~]# 
[root@head ~]# 
[root@head ~]# wwsh provision set --bootloader=sda "replicant*"

Are you sure you want to make the following changes to 4 node(s):

       SET: BOOTLOADER           = sda

Yes/No> yes

```

`wwvnfs --chroot $CHROOT` --> reassemble image

**at this point, you may boot up your compute nodes (pre-configure to PXE boot) and let them be provisioned by the headnode (make sure itself is up & running)** 


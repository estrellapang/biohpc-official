## Configuring Compute Nodes for Additional Services 

### On Samba/NFS server (not local)  

-Add entry to `/etc/exports`-->  `/home2 192.168.56.0/24(rw,no_subtree_check,fsid=12)` --> export the directory to the entire subnet 56.** 

`sudo systemctl restart nfs-server`

`id zxp8244` 

output:

`uid=1000(zxp8244) gid=1003(Common_Branch) groups=1003(Common_Branch),1000(zxp8244),1014(wheel),1015(sharers)`

- use the following information as reference for editing credentials in the headnode

### On the Headnode:

**Configuring NFS Mount**

- grant local headnode user group permission to access the fileshare

`sudo groupadd --gid 1015 sharers` --> create the group that has permission to access NFS share (w/h identical gid) 

`sudo usermod -g sharers zxp8244` --> add user "zxp8244" to "sharers" as its **PRIMARY group** (this is important)

`id zxp8244` --> verify if zxp8244's uid & gid match up with that seen in the NFS/Samba server'

`sudo vi /etc/group` --> change `wheels` gid from "10" to "1014" as seen in the NFS server; reboot for changes to take effect

- add NFS/Samba & LDAP server (see next heading for LDAP client) into `/etc/hosts` (both on master node and it chroot jail) 

`sudo vi /etc/hosts` 

- insert the following: 

```
192.168.56.5 sambanfs.vms.train
192.168.56.6 ldap.vms.train

``` 

  - to add the new hosts to the image; DO NOT directly add them to the $CHROOT/etc/hosts file, instead, rerun the following command:

```
sudo wwsh provision set "replicant*" --vnfs=centos7_6 --bootstrap=`uname -r` --files=dynamic_hosts,passwd,group,shadow,slurm.conf,munge.key,network

# enter "yes" to confirm 

``` 

- **echo new fstab into $CHROOT jail** 

`su -` 

`export CHROOT=/opt/ohpc/admin/images/centos7_6` 

`echo $CHROOT` 

`echo "sambanfs.vms.train:/home2 /home2 nfs nodev,nosuid,noatime 0 0" >> $CHROOT/etc/fstab` 

```
[root@head ~]# cat $CHROOT/etc/fstab
#GENERATED_ENTRIES#
tmpfs /dev/shm tmpfs defaults 0 0
devpts /dev/pts devpts gid=5,mode=620 0 0
sysfs /sys sysfs defaults 0 0
proc /proc proc defaults 0 0
head.cluster:/home /home nfs nfsvers=3,nodev,nosuid,noatime 0 0
head.cluster:/opt/ohpc/pub /opt/ohpc/pub nfs nfsvers=3,nodev,nosuid,noatime 0 0
sambanfs.vms.train:/home2 /home2 nfs nodev,nosuid,noatime 0 0

# to mount nfs share from newer nfs servers, DO NOT SPECIFY nfsvers! Only do this for older nfs servers 

```

**Install LDAP Client in Chroot Jail** 

`sudo yum --installroot=$CHROOT install openldap-clients nss-pam-ldapd`

```
============================================================================================================================================
 Package                               Arch                        Version                               Repository                    Size
============================================================================================================================================
Installing:
 nss-pam-ldapd                         x86_64                      0.8.13-16.el7                         base                         160 k
 openldap-clients                      x86_64                      2.4.44-21.el7_6                       updates                      190 k
Installing for dependencies:
 nscd                                  x86_64                      2.17-260.el7_6.3                      updates                      281 k
Updating for dependencies:
 glibc                                 x86_64                      2.17-260.el7_6.3                      updates                      3.7 M
 glibc-common                          x86_64                      2.17-260.el7_6.3                      updates                       12 M
 openldap                              x86_64                      2.4.44-21.el7_6                       updates                      356 k

Transaction Summary
============================================================================================================================================
Install  2 Packages (+1 Dependent package)
Upgrade             ( 3 Dependent packages)

```

- Install `authconfig` tool, which is needed to enable the LDAP connection 

`sudo yum --installroot=$CHROOT install authconfig`

```
============================================================================================================================================
 Package                              Arch                       Version                                  Repository                   Size
============================================================================================================================================
Installing:
 authconfig                           x86_64                     6.2.8-30.el7                             base                        424 k
Installing for dependencies:
 libselinux-utils                     x86_64                     2.5-14.1.el7                             base                        151 k
 make                                 x86_64                     1:3.82-23.el7                            base                        420 k
 newt                                 x86_64                     0.52.15-4.el7                            base                        105 k
 newt-python                          x86_64                     0.52.15-4.el7                            base                         54 k
 openssl                              x86_64                     1:1.0.2k-16.el7_6.1                      updates                     493 k
 policycoreutils                      x86_64                     2.5-29.el7_6.1                           updates                     916 k
 slang                                x86_64                     2.2.4-11.el7                             base                        512 k
Updating for dependencies:
 openssl-libs                         x86_64                     1:1.0.2k-16.el7_6.1                      updates                     1.2 M

Transaction Summary
============================================================================================================================================
Install  1 Package  (+7 Dependent packages)
Upgrade             ( 1 Dependent package)

```

- VERIFY:

`sudo chroot $CHROOT which authconfig` 

`sudo yum --installroot=$CHROOT list installed | grep -i 'openldap-clients'` 

`sudo yum --installroot=$CHROOT list installed | grep -i 'nss-pam-ldapd'`

`sudo yum list installed | grep -i [both packages above]` --> they should not be installed on the host file system! 

- Use `chroot` command to configure ldap client installed in $CHROOT 
udo groupadd --gid 1015 sharers
`su -` (remember to **re-declare** $CHROOT path)

`chroot $CHROOT authconfig --enableldap --enableldapauth --ldapserver=ldap.vms.train --ldapbasedn="dc=ldap,dc=vms,dc=train" --enablemkhomedir --update` 

`chroot $CHROOT systemctl enable nslcd`

**TROUBLESHOOTING** 

- if nslcd fails to start in the compute node w/h the following error shown by `journalctl -xe`: 

`nslcd[2998]: nslcd: /etc/nslcd.conf:8:uid: not a valid uid: 'nslcd'`

> solution: make the user `nslcd`(uid=65) and the group `ldap`(gid=55), and add the former to the latter

`sudo groupadd --gid 55 ldap` 

`sudo useradd -u 65 nslcd` 

`sudo useradd -g ldap nslcd`

`sudo vi /etc/group` --> modify nslcd's default generated gid to '65'' 

- VERIFY:

`cat /etc/group`

`sudo lid nslcd` --> the output should list `ldap(gid=55)` 

**Wrapping Up**
 
- re-import local credentials: 

```
sudo wwsh file import /etc/passwd
sudo wwsh file import /etc/group
sudo wwsh file import /etc/shadow

Overwrite existing file object "passwd" "group" "shadow" in the data store?
Yes/No [no]> yes

> **IMPORTANT OBSERVATION**: When modifying the image, while `/etc/fstab` can be modified directly in the $CHROOT and have its changes imple> mented upon rebuild; **HOWEVER**, files having to do credentials/identification are pulled from the provisioning **Warewulf database** (st> ored either locally or remotely) in a snapshot-like fashion. As such, everytime the following files are mod> ified:
> 
> /etc/passwd
> /etc/hosts 
> /etc/shadow 
> /etc/slurm/slurm.conf 
> /etc/munge/munge.key 
> /etc/group 
> 
> the command `sudo wwsh file import /etc/[changed file]` need to be entered, b.c. the 

sudo wwsh provision set "replicant*" --vnfs=centos7_6 --bootstrap=`uname -r` --files=dynamic_hosts,passwd,group,shadow,slurm.conf,munge.key,network	#"yes" to confirm

```

- rebuild image 

`sudo wwvnfs --chroot $CHROOT`


### On the COMPUTE Nodes 

**Verify LDAP** 

`sudo ldapsearch -x uid=* -b dc=ldap,dc=vms,dc=train` 


**Verify NFS** 

`df -Th` 


### Refer to `schlr_cmds_sh.md` to further configure Slurm prior to provisioning


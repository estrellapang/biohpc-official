## Useful Commands for OpenHPC, Slurm, Warewulf, & More Cluster Related Tasks 

### Executing in the chroot environment 

-**yum**

`sudo yum --installroot=$CHROOT [any yum flag/option] [arguments]` 

e.g.

1. Install a package: 

`sudo yum --installroot=&CHROOT install 'ohpc-slurm-client'

2. Check if a package has been installed in $CHROO: 

`sudo yum --installroot=&CHROOT list installed | grep -i 'ohpc-slurm-client'` 

-**chroot** 

  - `chroot ()` sets the root directory somewhere else for processes/commands:

`sudo chroot $CHROOT systemctl enable nfs-server`


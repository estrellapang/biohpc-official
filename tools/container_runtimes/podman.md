## Podman as Container Runtime

### Intro

- Officially supported by RHEL

- Improvement on Docker

  - ability to run Docker containers without root permissions

  - can alias Docker runtime commands

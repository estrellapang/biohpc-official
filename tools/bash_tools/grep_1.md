## Grep Usage by Examples

### General Uses:

- **search for keyword**

- `grep -nr "<search term>" <target directory>` 

  - `-n`: line number; `-r` recursive search

- **match EXACT string/term**

  - `grep -w '<symbol/string/word>'

- **output ONLY the matched portion/pattern**

  - `grep -o <>`

\
\

- **search for multiple words**

-  "OR" operator

  - `grep 'word1\|word2\|word3' /path/to/file`

  - e.g. `grep -ri 'error\|fail' /endosome/.backup/backup_services2/log/`

- "AND" operator

  - `grep 'PATTERN1.*PATTERN2' <file>`

- [grep AND OR operators](https://www.shellhacks.com/grep-or-grep-and-grep-not-match-multiple-patterns/)

\
\

- **exclude strings and terms**

  - `grep -v 'term'`

  - `grep -Eiv "exclude term"`

  - -OR- `grep -Fv "exclude term"` 

  - [reference](https://superuser.com/questions/537619/grep-for-term-and-exclude-another-term)

\
\

- show N # of lines of text under the matched term

  - `grep -A[N] 'term'`

- show N # of lines of text above the matched term

  - `grep -B<#> '<search term>'

\
\

#### Regular Expressions & Special Characters

- **match for character at the beginning of the lines** `grep "^<string>"` 

- **Search for number characters only:** `grep '[[:digit:]]*'`

- **Search for SPACE characters:**   `grep '\s' <text file> 

  - SPACE + other characters `'\s\+\<search term>' <text file>

- **search for symbolic links**

  - `ls -lah <directory> | grep -i "\->"   # the escape character "\" is necessart for "-"

- **search for backslashes**: `grep '\\'

  - this is bc `\` character has special meaning under GNU definitions

  - [here](https://www.gnu.org/software/grep/manual/html_node/The-Backslash-Character-and-Special-Expressions.html)

\

- **REFERENCES**

- [reference 01](https://askubuntu.com/questions/949326/how-to-include-a-space-character-with-grep)

- [reference 02](https://stackoverflow.com/questions/4233159/grep-regex-whitespace-behavior)

- [grep regular expression guide](https://linuxize.com/post/regular-expressions-in-grep/)

- [grep for numbers only](https://askubuntu.com/questions/184204/how-do-i-fetch-only-numbers-in-grep)

- [example of grep finding directories only](https://stackoverflow.com/questions/14352290/listing-only-directories-using-ls-in-bash)

- [grep for backslashes](https://stackoverflow.com/questions/44034124/escaping-slash-in-grep)

\
\

#### short case studies

- **e.g. grep for ">" and "|" characters in output**

```
# `diff` command uses ">" to denote NEW and "|" to denote modified files

diff -y -W 170 /etc/exports /tmp/exports | grep -i '>\||'
									 >	/work          198.215.51.3(rw,fsid=69,async)
#/work          198.215.51.8(rw,fsid=69,async)				 |	#/work          198.215.51.8(rw,fsid=69,async---fsid taken by 198.215.51.3)
#WebDataBan for Zhiyu Zhao/Morrison Lab.				 |	#WebDataBank for Zhiyu Zhao/Morrison Lab.
/work/CRI/WebDataBank          198.215.54.61(rw,fsid=628,async)     	 |	/work/CRI/WebDataBank          198.215.54.61(rw,fsid=628,async)
									 >	/home1          198.215.51.3(rw,fsid=2090,async)
#/home1          198.215.51.91(rw,fsid=2090,async)			 |	#/home1          198.215.51.91(rw,fsid=2090,async---fsid taken by 198.215.51.3)

```

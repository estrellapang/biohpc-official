## Archiving & Compression CLI Utilities

### tar

```
### Create Tar Archives

tar -czf <archive file>.gz <target files/directories>
tar -cjf <archive file>.bz2 <target files/directories>

### UNTAR Archives based on TYPE

  tar xvf  <.tar file>
  tar xzvf <.tar.gz file>
  tar xjvf <.tar.bz2 file>
```

- [tar tutorial](https://linuxize.com/post/how-to-create-and-extract-archives-using-the-tar-command-in-linux/)

- [tar C option](https://unix.stackexchange.com/questions/449703/tar-ignores-directory-option)

\
\

### gzip & gunzip

- gunzip

```
# unzip file

gunzip <filename>.gz

# unzip and redirect to new file

gunzip -c <filename>.gz > /<directory>/<new file name>

```

\
\

### Resources

- [options for Windows 10](https://wiki.haskell.org/How_to_unpack_a_tar_file_in_Windows)

## Concatenation and Output Trimming

### cat

- show line numbers:

`cat -n` or `cat --number`

\
\
\

### tail

- trim the output starting the the first nth line

`tail -n +<line number>`

\
\

### echo

- append to the SAME line (do not start new lines)

  - `echo -n <txt> > <file>`

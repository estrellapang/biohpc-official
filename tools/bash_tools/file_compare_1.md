## Commands & Guides for File & Directory Comparisons

### Compare Differences in Directories

- **diff command**

  - compare only the fileNAMES, not the contents: `diff --brief -r dir1/ dir2/`

- [reference 1](https://serverfault.com/questions/177001/find-files-in-one-directory-not-in-another)


\
\

- **rsync**

  - dryrun compare ALL contents of two directories `rsync -avn --delete <source> <destination>` 

  - `-avn` --> recursive, preserve metadata, verbose, and --dry-run, respectively

  - `--delete` --> delete anything that's extra on the destination that's not in the source



## How to use `curl` CLI Utility

### Fetching Info On Websites

#### Get Headers Only

`curl -I <URL>`

- [reference](https://alvinalexander.com/linux/how-to-use-curl-get-headers-from-url/)

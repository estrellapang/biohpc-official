## how to use "cut" command

### Basic usage

- trim based on field number and specify delimiter

  - `cut -f <field#,field#,field#,...> -d ':' --complement`

  - `--complement` = trim the fields specified and output the remaining 

  - e.g. `cut -f 1,2,3 -d ':' --complement`

- print a range of fields (syntax more concise than awk) --- by cutting all fields except specific fields/columns

  - `cut -d ' ' -f2-6`  --> this would print fields 2 to 6 and trim out all else  

  - `-d' '` --> uses space character as delimiter; `d';'` `d,` are examples of other common delimiters

\

- trim first few fields (or however-many)

  - simpler than awk syntax

```
# e.g. print all fields except 1st field
----------------------------------------
cut -f 2- -d ' '
----------------------------------------

# alternative approach
----------------------------------------
cut -f 1 -d ' ' --complement
----------------------------------------
```

\
\
  
### Sources

- [stasckoverflow print a sequence of columns](https://stackoverflow.com/questions/13690461/using-cut-command-to-remove-multiple-columns)

- [geek stuff overview](https://www.thegeekstuff.com/2013/06/cut-command-examples/)

- [computer hope overview](https://www.computerhope.com/unix/ucut.htm)

- [cut --complement](https://stackoverflow.com/questions/4198138/printing-everything-except-the-first-field-with-awk)

- [cut specify fields](https://stackoverflow.com/questions/4198138/printing-everything-except-the-first-field-with-awk)

### ENCRYPT DECRYPT

#### via password ONLY

```
# encrypt a file

  # outputs a .gpg file

gpg -c <file> 

\

# decrypt a file

gpg -o <decrypted plain file> -d <file to be decrypted> 

```

#### via keys

- `gpg -es <filename>`

  - enter your FULL NAME, then ENTER again with empty line

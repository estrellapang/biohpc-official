### Sort

#### Common Handles 

- `-n` --> arrange by numerical value

- `-r` --> reverse arrangement order (large > small)

#### remove duplicates

- `sort -u`

  - COMMON COMBO: `sort <> | uniq -c` 

#### arrange numerical large to small

- `sort -V`

\

- **references**

  - [mange page](https://www.computerhope.com/unix/usort.htm)

  - [sort_head_numerical_arragement](https://stackoverflow.com/questions/16212410/finding-the-max-and-min-values-and-printing-the-line-from-a-file)

\
\
\

## uniq

- **REQUIRES list to be ORDERED**

  - [reference here](https://unix.stackexchange.com/questions/52534/how-to-print-only-the-duplicate-values-from-a-text-file)

- print how many times duplicates occur

  - `uniq -c`

- print ONLY duplicates

  - `uniq -d`

- **references**

  - [mange page](https://www.computerhope.com/unix/uuniq.htm)

## How to Customize and BASH shell's Behaviour

### To SAVE ALL History

- add the following to `.bashrc`

```
# CentOS/RedHat

HISTSIZE=
HISTFILESIZE=

# Alternatives

HISTSIZE=-1
HISTFILESIZE=-1

```

- [reference 1](http://jesrui.sdf-eu.org/remember-all-your-bash-history-forever.html)

- [reference 2](https://stackoverflow.com/questions/9457233/unlimited-bash-history)

- [reference 3](https://www.linuxjournal.com/content/using-bash-history-more-efficiently-histcontrol)

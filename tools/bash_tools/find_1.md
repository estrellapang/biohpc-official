## find command usage

### Sort File Types

- **find all files**

```
# become root to see all files

# find files and COUNT TOTAL
-----------------------------------------------
find . -type f | wc -l

  # one can also specify what directory as well 
-----------------------------------------------

# find files and dir based on group and user 
-----------------------------------------------
find <dir> -type d -user <uid>

find <dir> -type f -group <groupname> 

# FIND PATTERNS that DO NOT MATCH

find <dir> -type <f/d> -not -name "<name>"

# FIND PATTERNS based on NAME

find <dir> -type <f/d> -name "*<PATTERN>*"

  # without the wildcards, utility cannot properly match the pattern
-----------------------------------------------


```

- **more granular find subdirectories** 

```
# find DEPTH levels
-----------------------------------------------
## stay within the directory specified
find <dir> -maxdepth 1

## stay within the directory specified, exclude outputting the topmost directory
find <dir> -mindepth 1 -maxdepth 1
-----------------------------------------------
```

- **find all directories and COUNT TOTAL**

```
-----------------------------------------------
find . -type d | wc -l
-----------------------------------------------

```

\

- ** find files/directories by owner/group

```
find . -type <f/d> -<user/group> <input>
```

\

- **find symbolic links**

```
-------------------------------------------------------------------------------
# find recursively and list all links 
find . -type l -ls

# find symbolic links ONLY in current directory
find . -maxdepth 1 -type l -ls

# if cannot see where sym link is pointing to, use `readlink -f <link>` 
[https://serverfault.com/questions/76042/find-out-symbolic-link-target-via-command-line]

# find symbolic links owned by a user and change permission to new user via `chown -h` flag
find /project/MRL/MRL_Core/shared/ -type l -user s166458 -exec chown -h carana {} \;
-------------------------------------------------------------------------------
``` 

- [reference 1](https://ostechnix.com/quick-tip-how-to-list-symlinks-on-linux/)

\
\
\

### Combine Commands: "if found, then execute this"

#### Find file/dir then delete it

```
sudo lfs find <target_directory> -type <f/d> -<user/group/name/etc> <attribute> | sudo xargs rm -v | tee -a <log_file>.log

  ## note that here we couldn have used `-exec` handle but piping into `xargs` is a lot quicker

  ## if unsure, do `xargs ls` first

```

- **delete files older than a certain modified time**

  - for DRY-RUNs, do `-exec echo rm {} \;`

```
-------------------------------------------------------------------------------
find <directory> -maxdepth 1 -mtime +30 -exec rm -v {} \;

  ## "+30" --> older than 30 days

  ## "-exec" --> execute `rm` on every matched file

  ## "-mtime 0" --> modified within the LAST 24 hours
-------------------------------------------------------------------------------

```

\


- **find files and change their permissions**

```
-------------------------------------------------------------------------------
nohup sudo find /endosome/archive/bioinformatics/Danuser_lab/melanoma/raw -group live_cell -exec chgrp -hv Danuser_lab {} \; >> melanoma_raw_live_cell_chgrp.out
-------------------------------------------------------------------------------
```

\


- **find files modified since n minutes ago**

```
[link](https://stackoverflow.com/questions/543946/find-mtime-files-older-than-1-hour)

```

\


- **exclude CURRENT/PARENT dir with `prune` "handle/switch"**

```

# helpful in excluding the CURRENT or "PARENT" directory under which you are seaching

# NOTE: place AFTER other options e.g. "maxdepth" "type" etc. 

e.g. find directories and get individual size:
-------------------------------------------------------------------------------
sudo find . -maxdepth 1 -type d ! -name . -prune -exec du -sh {} \;
-------------------------------------------------------------------------------

```

\

- **exclude any subdir of your choosing**

```
-------------------------------------------------------------------------------
sudo find <search dir> -not -path <absolute path to subdir> -<group/name/etc.> 
-------------------------------------------------------------------------------

# e.g., exclude `melanoma` from `/endosome/archive/bioinformatics/Danuser_lab/`
-------------------------------------------------------------------------------
sudo find /endosome/archive/bioinformatics/Danuser_lab/ -maxdepth 1 -not -path /endosome/archive/bioinformatics/Danuser_lab/melanoma -group Danuser_lab
-------------------------------------------------------------------------------
```

\

- **launch parallel find procs**

```
e.g. assign 5 separate processes to concurrently search for criterion
-------------------------------------------------------------------------------
sudo nohup ls /endosome/archive/bioinformatics/Danuser_lab/ | grep -v 'melanoma' | sudo xargs -n1 -I% -P5 find /endosome/archive/bioinformatics/Danuser_lab/% -group live_cell > live_cell_files_Danuser_lab.raw

  # NOTE: having sudo before `nohup` and `xargs` is crucial if you are running as a sudo user, or else you'll run into permission denied
-------------------------------------------------------------------------------
```

\

- **find hidden files and move to new director**

  - `mv` CLI utility alone cannot move hidden files easily in scripting

```
-------------------------------------------------------------------------------
find /archive/bioinformatics/Danuser_lab/melanoma/analysis/Vasanth/Cell\ Profiler\ Pipeline/Cell\ Profiler\ Pipeline/ -name '.*' -exec echo mv {} /archive/bioinformatics/Danuser_lab/melanoma/analysis/Vasanth//Cell\ Profiler\ Pipeline/ \;
-------------------------------------------------------------------------------
```
\
\

#### Useful Links

- [comprehensive find tutorial](https://alvinalexander.com/unix/edu/examples/find.shtml)

- [find exec tutorial](https://www.cyberciti.biz/faq/linux-change-user-group-uid-gid-for-all-owned-files/)

- [find prune good guide](http://www.theunixschool.com/2012/07/find-command-15-examples-to-exclude.html)

- [hint for sorting files/directories by size](https://unix.stackexchange.com/questions/53737/how-to-list-all-files-ordered-by-size)

- [prune exclude directories](https://stackoverflow.com/questions/4210042/how-to-exclude-a-directory-in-find-command)


\
\

### Troubleshooting

- Error `find: failed to restore initial working directory: Permission denied`

  - tends to occur when user does not have login shell or home dir

  - workaround: `cd /tmp`, then execute `find` command

  - [reference](https://stackoverflow.com/questions/5791651/find-is-returning-find-permission-denied-but-i-am-not-searching-in)

\
\

### Resources

- [find case insensitive, user and group](https://www.cyberciti.biz/faq/how-do-i-find-all-the-files-owned-by-a-particular-user-or-group/)

- [techmint good basics](https://www.tecmint.com/35-practical-examples-of-linux-find-command/)

- [diff ways of countint files, basics](https://linuxhandbook.com/count-files-directory-linux/)

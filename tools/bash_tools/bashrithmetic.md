## How to Conduct Arithmetic Operations in BASH

### `bc` CLI Utility - perform advanced arithmetic operations

#### BEHAVIOR

  - does not handled CAPITAL Lettered variables!

```
echo "totalm = totalm + bcKing; totalm" | bc
(standard_in) 1: illegal character: K
(standard_in) 1: syntax error
```

- [examples with variable substitutions](https://www.geeksforgeeks.org/bc-command-linux-examples/)

- [more examples](https://stackoverflow.com/questions/38751037/add-two-decimal-number-in-bash-script)

\

#### Case Study - seek sum of all Megabyte sized files from a `du` command output

> the challege: not being able to easily operate on floating point/decimals when dealing bashscripting 

```
# insert all 'M' file sizes into a neat array

dislocatedM=( $(cat BICF_dislocated_files_final.txt | awk '{print $1}' | grep 'M' | sed 's/M//g') )

\

# arrange the file sizes into a input equation format, e.g. addition

bcKing=$(for i in ${dislocatedM[@]}; do echo -n "+ $i "; done)

\

# insert last variable into 
```

## Xarg Command Use Cases

### Parallelism in One Shell

- remove all filenames that match a regular expression search

  - `find /var/log/samba/ -mindepth 1 -maxdepth 1 -name *old* | xargs rm -v | tee -a rm_log.out`

\

- remove 1000+ subdirs efficiently under a parent dir

  - `nohup ls | xargs -n1 -P5 -I% rm -rfv % > <log_name>.log`


\
\

### Parallel SSH

#### Get Most Recent Nodelist (using SLURM controller)

- **get raw node list from SLURM**

  - `sinfo --Node >> slurmRaw.txt`

- **use xargs to check SSH connection into each node**

  - `sudo xargs -a slurmRaw.txt -I"NODE" -n1 sh -c "sudo ssh -o ConnectTimeout=5 NODE hostname -s || true" | tee nodelistRaw.txt` 

- **filter out non-responsive and duplicate node names**

  - `cat nodelistRaw.txt | sort -u > nodelistFinal.txt` 


\

- **execute remote ssh commands for multiple nodes**

  - protip: to see effects of varying thread "-P" values, prepend xargs command with `time`

```
------------------------------------------------------------------------------
xargs -a <node list> -I"NODE" -P <# of processes> -n1 sh -c "<command/script>"
------------------------------------------------------------------------------

e.g.

xargs -a /tmp/nodelist_ns -I"NODE" -P 24 -n1 sh -c "sudo ssh NODE bash /home/tools/bin/lazyunmount-script.sh" >> out.txt

```

- [xargs-ssh github reference](https://github.com/Lihuanghe/xargs-ssh)

- **using SSH timeout & ignore errors**

  - e.g. `xargs -a nodelist_08242020.txt -I"NODE" -P 16 -n1 sh -c "sudo ssh -o ConnectTimeout=3 NODE bash /home/tools/bin/disk_prefail_chk.sh || true" > disk_prefail_real01.out`

  - `ConnectTimeout=3` force SSH timeout after 3 seconds (prevents long packet drop hangs)

  - AVOID this if your parallel SSH script is quitting MID-WAY upon timeout!

  - ` .. || true` do not quit xargs upon shell errors, e.g. "xargs: sh: exited with status 255; aborting" would prevent xargs from completely going through all nodes on nodelist


- **CAUTION**: if combining remote SSH with xargs, use single quote after -c 'ssh ...' and double quote for the remote command

  - Piping and passing outputs could encounter issues using xargs & remote SSH!

  - `xargs ... -c 'ssh "<remote_cmd>"'`

\
\

#### Parallel File Transfer

- **Parallel Rsync**

```
ls <directory> | xargs -n1 -P<processes> -I% rsync -aP % <remote server>:<dest dir>
```
- [source & explanation](https://stackoverflow.com/questions/24058544/speed-up-rsync-with-simultaneous-concurrent-file-transfers)

\
\

#### Piping within xargs shells

- With `awk`, we need to escape the `$`

- e.g. `ls *.csv | xargs -I@ bash -c "cat my.log | grep @|tail -n 1|awk '{print \$3}'"`

- [USEFUL thread on piping into awk inside xargs initiated shells](https://unix.stackexchange.com/questions/537827/filter-columns-in-string-with-awk-piped-with-xargs)

\
\

#### Special Characters

-**problematic characters**

  - spaces --> use `-0` option to instruct xargs to NOT interpret spaces as a line break

  - single qoutes

- **benign characters**

  - "#", "-", "@"

\
\

### Let xargs wait for USER PERMISSION

- `xargs -p`

- [useful tutorial](https://www.tecmint.com/xargs-command-examples/)

\
\

#### Debugging

- [xargs exit error codes](https://www.gnu.org/software/findutils/manual/html_node/find_html/Error-Messages-From-xargs.html)

- [instruct xargs to ignore error exit codes](https://serverfault.com/questions/289094/prevent-xargs-from-quitting-on-error/289106)


\
\

#### General Guides

- [xargs and other parallel cli options](https://www.linuxjournal.com/content/parallel-shells-xargs-utilize-all-your-cpu-cores-unix-and-windows)

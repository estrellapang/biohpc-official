## Our Dear Dependable Friend SSH

\
\

### Overview

- [ssh auth, how it works](https://www.geeksforgeeks.org/introduction-to-sshsecure-shell-keys/)

- [key signature algorithms](https://www.ssh.com/manuals/server-zos-admin/65/ConfiguringHostKeyAlgorithms.html)

- [RHEL sysadmin guide to SSH](https://www.redhat.com/sysadmin/passwordless-ssh)

\
\

### SSH client settings

#### Prevent client-side timeouts

- [link 1](https://www.looklinux.com/how-to-fix-ssh-connection-timeout-in-linux/)

- [link 2](https://www.thegeekdiary.com/how-to-stop-ssh-session-from-getting-timed-out/)

\
\

### Guides and Tips on Remote SSH Commands

### Resources:

- [basics 1](https://www.cyberciti.biz/faq/unix-linux-execute-command-using-ssh/)



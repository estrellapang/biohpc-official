## Manual Various Email CLI Utilities

### Good Ol' "mail"

#### specify "FROM" address and forward content from file

```
mail -s '<subject>' -r <from addr> <recipient addr> < file.txt

  # e.g. mail -s 'cluster test email' -r biohpc-help@utsouthwestern.edu zengxing.pang
@utsouthwestern.edu < /tmp/test_mail.txt

  # explanation: send contents of "test_mail.txt" FROM biohpc-help, to zengxing.pang

```

\

#### email the stdOUT of commands

```
mail -s 'Lustre Quota Report' biohpc-help@utsouthwestern.edu < <( bash /project/biohpcadmin/shared/scripts/quota_breach_detection.sh )
```

- [tutorial link 1](https://www.poftut.com/linux-mail-mailx-commands-tutorial-examples-send-email-command-line/)

- [tutorial link 2](https://www.interserver.net/tips/kb/linux-mail-command-usage-examples/)

- [tutorial link 3](https://linuxhint.com/bash_script_send_email/)

- [tutorial link 4](https://www.cyberciti.biz/faq/linux-unix-bash-ksh-csh-sendingfiles-mail-attachments/)

- [how to mail command/pipe outputs](https://unix.stackexchange.com/questions/534520/sending-output-of-script-by-mail)

## Essential Operations via `Sed`

### Text Operations: Lines

#### Print Specific Lines from StdOUT:

- [link 1](https://www.linuxquestions.org/questions/programming-9/get-only-first-line-using-sed-awk-946567/)

- [link for command nth line of STDOUT](https://stackoverflow.com/questions/1429556/command-to-get-nth-line-of-stdout)

```
sed -n <#>p

 `#` --> represent number of lines

## awk can also be used:

awk 'NR == #' 

```

\
\

### Text Operations: Content Manipulation

#### Search and Replace

```
### acting on an input file

sed -i 's/SEARCH_REGEX/REPLACEMENT/g' <input file>

### acting on output text--no '-i' handle

<cmd> | sed 's/SEARCH_REGEX/REPLACEMENT/g'

### replace +1 patterns in one command

sed 's/<search>/<replace>g;s/<2nd_search>/<2nd_replace>/g'

### search and replace special characters

sed 's|<search>|<replace>|g'

### search and replace with '\' backslashes

  # enter `\\` so shell interpret it as a literal `\` character

  # e.g. replace spaces with `\ ` for to escape space characters
----------------
sed 's| |\\ |g'
----------------
  
  # EXCEPTION, if attempting to replace special character along with a space, one `\` suffices

  # e.g. `sed 's|-|-\ |g'` --> here, the replacement character '-\' does not need '\\'

### replace Nth pattern
----------------
sed 's/<pattern>//N'

  # 'N' = Nth pattern detected (sequentially)

  # e.g.  `sed 's/ //1'` --> replace the 1st space character
----------------

[reference here](https://unix.stackexchange.com/questions/339266/removing-the-first-space-in-a-line)
```

- [stackoverflow sed multiple patterns](https://stackoverflow.com/questions/26568952/how-to-replace-multiple-patterns-at-once-with-sed)


- [stackoverflow sed escape special characters](https://stackoverflow.com/questions/53718574/replace-special-character-using-sed-command)

\

#### Delete all empty lines in a file

```
sed -i '/^$/d' <file>

```

- [resource](https://www.cyberciti.biz/faq/using-sed-to-delete-empty-lines/)

\
\

### References

- [gen tutorial 1](https://linuxize.com/post/how-to-use-sed-to-find-and-replace-string-in-files/)

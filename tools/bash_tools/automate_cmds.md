## Commands Used for Auto-Start/Termination Purposes

### timeout

- kill command after N secs

  - `timeout 7 ping host`

- force kill command

  - by default the stop signal is SIGTERM (polite way)

  - `timeout -s SIGKILL 7 <cmd>`

- [tutorial 1](https://www.putorius.net/linux-timeout-command.html)

\
\

### Run cmds in Background

- [reference](https://serverfault.com/questions/41959/how-to-send-jobs-to-background-without-stopping-them)

\
\

### Watch Live Logs

#### watch command

- `watch <cmd>`

- monitor a pipe of commands: `watch '<cmd> | ... | <cmd>'` 

  - could also use double quotation marks

  - [hints here](https://unix.stackexchange.com/questions/318859/how-to-use-watch-command-with-a-piped-chain-of-commands-programs)

\

#### Follow Logs Live

- `tail -f <log name>`

  - e.g. `tail -f /var/log/messages`

- follow service-specific logs

  - `journalctl -u <service name> -f`

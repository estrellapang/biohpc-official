## Notes on scp/cp/mv commands

### cp/scp

#### They do not work with regular expressions (?)

- Bash shell works with "glob patterns" but not regular expressions:

  - [link 1](https://unix.stackexchange.com/questions/24242/using-regular-expressions-with-cp) 

  - [link 2](https://superuser.com/questions/441422/how-do-you-use-regular-expressions-with-the-cp-command-in-linux)


#### COPY MULTIPLE FILES AT ONCE:

- [bracket expansions!](https://stackoverflow.com/questions/9915822/how-to-copy-multiple-files-from-a-different-directory-using-cp)

\
\

#### Copy and Preserve Symbolinks

- [source](https://www.golinuxhub.com/2016/09/how-to-preserve-symbolic-link-while/)

\
\

#### Scopy Performance Tuning

- use simplist encryption algorithm, `arcfour`

  - e.g. `scp -o Cipher=arcfour`

\
\

#### Troubleshooting

- "ambigugous target" --> SOLUTION: enclose destination address with double-quotes ""

  - [reference](https://superuser.com/questions/1022976/scp-copy-has-error-ambiguous-target)

\
\
\

### `mv` command

#### General Gotcha's

- **regular expressions don't work well with** `sudo`

  - e.g. `sudo mv /etc/*` --> the * is interpretted literally because of sudo using a separate shell

- `mv` doesn't move hidden files!

  - combine with `find` --> `find <dir>  -name ".*" -exec mv -v {} <dest_dir>`

\

#### exclude certain subdirs

- [stack overflow reference](https://stackoverflow.com/questions/4612157/how-to-use-mv-command-to-move-files-except-those-in-a-specific-directory)

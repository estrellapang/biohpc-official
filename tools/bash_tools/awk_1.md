## Notes on AWK 

### Spliting/Parsing

- print first field and pass to text file 

  - `awk '//{print $1}' Desktop/WS_Packpage_Debug.md | tee Desktop/WS_Packpage_Debug.md` 

- print a set of fields: `awk '//{print $2,$3,$4}'` 
 

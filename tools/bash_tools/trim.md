## Commands Useful for Trimming Text Outputs

### `tr` command

#### Basics

- Eliminate empty lines

`tr -s '\n'`

  - e.g. get clean rsnapshot config, no comments included

```
cat /endosome/.backup/backup_services2/config/rsnapshot-cloud.conf | grep -v '#' | tr -s '\n'
```

\

- Eliminate blank spaces

``tr -s '[:blank:]'``

\

- Match and Replace

  - `tr '<match_term>' '<replacement_term>'`

  - e.g. replace newline characters with spaces: `tr '\n' ' '`

\

- Delete/Trim

  - `tr -d '<term>'  -OR- `tr --delete '\n'`

- [Reference](https://stackoverflow.com/questions/1251999/how-can-i-replace-a-newline-n-using-sed)

- [how to eliminate white spaces, empty lines](https://stackoverflow.com/questions/9953448/how-to-remove-all-white-spaces-from-a-given-text-file)

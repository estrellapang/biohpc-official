##

### ls command

#### list only the directories

`ls -l | grep "^d"`

- [source 1](https://stackoverflow.com/questions/14352290/listing-only-directories-using-ls-in-bash)

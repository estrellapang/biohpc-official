## Rsync Usage by Exmaples

### SYNTAX

- `rsync [options/flags] [SRC] [USR]@[HOST]:[DEST]`

- Pay attention to the **Trailing** `/` on SOURCE directories!!!

  - e.g. copy **only the contents** inside source directory

    - `rsync -a /home/mydocs/ user@test.com:/data/`

  - e.g. copy** entire source directory** inside dest directory

    - `rsync -a /home/mydocs user@test.com:/data/`

    - here the trailing `/` is omiited

  - **same rule applies to** `scp` and `cp`

\
\

### Transfer Basics + Gotcha's:

- `-a` --> RECURSIVE copy + PRESERVE metadata & symbolic links

- `-n` / `--dry-run` --> DRY-RUN: will list files to be copied/deleted

  - good to use with `-v` if there aren't too many files 

- `--append` --> Keep incomplete copies; good for RESUMING 

- `--delete` --> MIRROR: no extra files on either side

- `--remove-source-files` --> rid of source files once complete 

- `--progress` --> show transfer progress

- `-h` --> reports in human readable format

- `-P` --> --partial + --progress

- `-A` --> preserve ACLs

- `-H` --> preserve hard links

  - hardlinks have different file locations but the SAME innode #

  - hardlinks reference the original file but smaller in size

  - deleting the hardlink or the original file does not affect the existence of the other

- `--exclude "<item combinable with regular expressions>"` --> exclude folders and directories from rsync

  - NOTE: the **trailing slash** following the include/exclude dir name is very important! If no trailing slash, rsync will match ANY subdir with the specified name, and with the trailing slash added, ONLY the specific subdir underneathe the main copy directory is marked

- `--include <directory> --include <directory>`   #transfer multiple drectories at once

  - reference usage from Ticket#2020020710010471

- `--update` --> copy from source files that do not exist on dest, and ONLY update existing files on dest when source has version with newer timestamps

  - prevents overwriting new changes already made on dest

- `-s` or `--protect-args` --> does not tell the remote shell interpret filenames; allows transferring of names with SPECIAL characters such as spaces in between without having to manually escape them

- `--info=progress2` --> displays total progress by % and total size of files transferred

  - e.g. `rsync -avhP --info=progress2 <srce> <dest>`  # -h = human-readable is helpful

- `--size-only` --> check file differences based solely on filesizes

- `--ignore-existing` --> only copy the files that DO NOT EXIST on dest

\

#### Safest Rsync Transfers

- `rsync -AavhPs` 

  - `-A` --> preserve ACLs

\

- e.g. **rsync directories, and escape SPACE chracters and '( )' characters**

```
# ESCAPE SPACES

rsync -Aavhs /project/bioinformatics/Danuser_lab/melanoma/raw/Olfactory\ Receptors/ /endosome/archive/bioinformatics/Danuser_lab/melanoma/raw/Olfactory\ Receptors/ 2>&1 > ${HOSTNAME}_danuser_OlfactoryReceptors.log &


# ESCAPE '()'

rsync -Aavhs /project/bioinformatics/Danuser_lab/melanoma/raw/hiRes3D\(cheap\ knockoff\)/ /endosome/archive/bioinformatics/Danuser_lab/melanoma/raw/hiRes3D\(cheap\ knockoff\)/ 2>&1 > ${HOSTNAME}_danuser_hiRes3D_check.log &
```

\
\

### file comparison handles

- Default Behavior: checks size and timestamp to determine differences 

- VERIFY Transfers:

  - `rsync -nri <source> <dest>`

  - `-ri` --> recursive and list or "itemize" changes

- Comparison Handles

  - `--size-only` --> compare size of source and destination files; if different, copies over

  - `--ignore-times` -->  no comparisons whatsoever; ALWAYS copies new files or changes over

  - `--checksum` --> size and static checksums of source and destination are compared; no timestamp comparisons (this option REQUIRE READING THE ENTIRE FILE)

  - `--update` --> only transfer files that do not exist or has newer mod times on destination's side 

- COMPARE ONLT CONTENTS (not mod/access times)

  - `rsync -nvc <source> <dest>`

  - [checksum and update options explained](https://unix.stackexchange.com/questions/67539/how-to-rsync-only-new-files)

\
\
\

### Optimizations

#### Granularize Transfer Targets Ahead of Time

- [using dry-run and include option](https://unix.stackexchange.com/questions/260616/reuse-rsync-dry-run-output-to-speed-up-the-actual-transfer-later-on)

\
\
\
### Trouble-shooting

- **"files/attrs were not transferred"**

- re-do the rsync with DRY-RUN (-n) and catch for errors, most notably those arising from permission restrictions

```
# e.g. error at the end of Lamella backup script
-------------------------------------------------------------
rsync error: some files/attrs were not transferred (see previous errors) (code 23) at main.c(1178) [sender=3.1.2]
-------------------------------------------------------------

# dry-run found the source of errors
-------------------------------------------------------------
rsync: readlink_stat("/project_ro/shared/.cloud_data/server_configs/vsftpd_config/vsftpd/ftpusers") failed: Permission denied (13)
rsync: readlink_stat("/project_ro/shared/.cloud_data/server_configs/vsftpd_config/vsftpd/vsftpd.conf") failed: Permission denied (13)
rsync: readlink_stat("/project_ro/shared/.cloud_data/server_configs/vsftpd_config/vsftpd/vsftpd_conf_migrate.sh") failed: Permission denied (13)
rsync: readlink_stat("/project_ro/shared/.cloud_data/server_configs/vsftpd_config/vsftpd/user_list") failed: Permission denied (13)
rsync: readlink_stat("/project_ro/shared/.cloud_data/server_configs/vsftpd_config/vsftpd/vsftpd.conf.bak") failed: Permission denied (13)
-------------------------------------------------------------
```

### References:

- [rsync parallel transfer techniques](https://stackoverflow.com/questions/24058544/speed-up-rsync-with-simultaneous-concurrent-file-transfers) 

- [rsync include option](https://stackoverflow.com/questions/15687755/how-to-use-rsync-to-copy-only-specific-subdirectories-same-names-in-several-dir)

- [rsync include option 2](https://stackoverflow.com/questions/9952000/using-rsync-include-and-exclude-options-to-include-directory-and-file-by-pattern)

- [all exclude methods, inline, via file listing, and via curly braces](https://askubuntu.com/questions/320458/how-to-exclude-multiple-directories-with-rsync)

- [rsync exclude include filter rules](https://unix.stackexchange.com/questions/5774/rsync-excluding-a-particular-subdirectory-and-its-children-where-the-subdirect)

- [rsync -P option--progress & partial](https://www.digitalocean.com/community/tutorials/how-to-use-rsync-to-sync-local-and-remote-directories-on-a-vps)

- [rsync general 1](https://www.tecmint.com/rsync-local-remote-file-synchronization-commands/) 

- [run rsync in the background](https://stackoverflow.com/questions/6141710/running-rsync-in-background)

- [monitoring rsync with pv option](https://www.cyberciti.biz/faq/show-progress-during-file-transfer/)

- [pass multiple rsync source/dest arguments](https://unix.stackexchange.com/questions/368210/how-to-rsync-multiple-source-folders)

- [rsync overcome space escape characters](https://unix.stackexchange.com/questions/104618/how-to-rsync-over-ssh-when-directory-names-have-spaces) 

- [rsync comparison handles explained](https://stackoverflow.com/questions/13778889/rsync-difference-between-size-only-and-ignore-times)

- [how rsync compares files, conceptual](http://tutorials.jenkov.com/rsync/detecting-file-differences.html)

- [show overall transfer progress methods](https://serverfault.com/questions/219013/showing-total-progress-in-rsync-is-it-possible) 

- [rsync from file list and bandwidth limit](https://www.complexsql.com/rsync-files-from/)

- [rsync link behaviors 1](https://superuser.com/questions/799354/rsync-and-symbolic-links)

- [rsync link behaviors 2](https://unix.stackexchange.com/questions/230334/rsync-complains-about-symlinks)


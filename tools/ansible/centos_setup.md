## Ansible installation on CentOS 7 

### On Headnode

```
Installed:
  ansible.noarch 0:2.8.1-1.el7

Dependency Installed:
  PyYAML.x86_64 0:3.10-11.el7                                   libyaml.x86_64 0:0.1.4-11.el7_0              python-babel.noarch 0:0.9.6-8.el7        python-backports.x86_64 0:1.0-8.el7
  python-backports-ssl_match_hostname.noarch 0:3.5.0.1-1.el7    python-cffi.x86_64 0:1.6.0-5.el7             python-enum34.noarch 0:1.0.4-1.el7       python-httplib2.noarch 0:0.9.2-1.el7
  python-idna.noarch 0:2.4-1.el7                                python-ipaddress.noarch 0:1.0.16-2.el7       python-jinja2.noarch 0:2.7.2-3.el7_6     python-markupsafe.x86_64 0:0.11-10.el7
  python-paramiko.noarch 0:2.1.1-9.el7                          python-ply.noarch 0:3.4-11.el7               python-pycparser.noarch 0:2.14-1.el7     python-setuptools.noarch 0:0.9.8-7.el7
  python-six.noarch 0:1.9.0-2.el7                               python2-cryptography.x86_64 0:1.7.2-2.el7    python2-jmespath.noarch 0:0.9.0-3.el7    python2-pyasn1.noarch 0:0.1.9-7.el7
  sshpass.x86_64 0:1.06-2.el7

[estrella@headnode ~]$ which ansible
/usr/bin/ansible

[estrella@headnode ~]$ whereis ansible
ansible: /usr/bin/ansible /etc/ansible /usr/share/ansible /usr/share/man/man1/ansible.1.gz

[estrella@headnode ~]$ ansible --version
ansible 2.8.1
  config file = /etc/ansible/ansible.cfg
  configured module search path = [u'/home/estrella/.ansible/plugins/modules', u'/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/lib/python2.7/site-packages/ansible
  executable location = /usr/bin/ansible
  python version = 2.7.5 (default, Oct 30 2018, 23:45:53) [GCC 4.8.5 20150623 (Red Hat 4.8.5-36)]
```

- **Generate SSH Key** 

```
### NOTE: DO NOT CHANGE key name & location - NOR - ADD PASSPHRASE

  ## Non-default settings tend to disrupt the no-password login for SSH connections

[estrella@headnode ~]$ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/home/estrella/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/estrella/.ssh/id_rsa.
Your public key has been saved in /home/estrella/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:cxs0N94f8/2HAJe2GjNhSNLXHi+ETE8sW2itUVRWj6I estrella@headnode.biohpc.new
The key's randomart image is:
+---[RSA 2048]----+
|       . o.O+.o..|
|      . o OoB. ..|
|       o +oO=+. .|
|        ..*=*+.  |
|        S.E=.o.o |
|         o+oo  .=|
|          .= . .+|
|          .   . o|
|                o|
+----[SHA256]-----+

```

- **Copy Key to Managed Node(s)'s** 

```
[estrella@headnode ~]$ scp -pr ~/.ssh/id_rsa.pub estrella@worker001:/tmp
estrella@worker001's password:
id_rsa.pub

```

### On Managed Node

- **pass headnode's public key into `authorized_keys` 

[estrella@headnode ~]$ ssh worker001

[estrella@worker001 ~]$ cat /tmp/id_rsa.pub >> .ssh/authorized_keys 

  ## make sure the file exists in the first place

```

- **check if correct packages/libraries have been installed**

```

[estrella@worker001 ~]$ sudo yum install libselinux-python 

Package libselinux-python-2.5-14.1.el7.x86_64 already installed and latest version

```


### Test Ansible 

```

[estrella@headnode ~]$ ansible all -u estrella -m ping
198.215.56.3 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    },
    "changed": false,
    "ping": "pong"
}

# success!

```

# Adding a `root` key to managed nodes 

- we want to cast root user's SSH key to a vast group managed nodes, who did not previously have any user's pub key stored in them 
           

- initial state 

```
[root@headnode ~]# ssh root@worker1
The authenticity of host 'worker1 (23.202.231.166)' can't be established.
ECDSA key fingerprint is SHA256:HnzBy7BAfkMCT4uIcdLrpoWiOrnhHhN8k7XMbbB2Epk.
ECDSA key fingerprint is MD5:d1:23:b1:96:eb:66:5f:5f:09:64:b9:27:0e:9e:c5:e4.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'worker1,23.202.231.166' (ECDSA) to the list of known hosts.
Permission denied (publickey).
```

- 

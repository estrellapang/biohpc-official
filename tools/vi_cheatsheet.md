## Vi/Vim Editor Manual

### Outside the Editor  

- if your session crashes and a `.swp` file is created: 

`vim -r [original, unrecovered file]` --> this will migrate the unsaved contents of the .swp file to the original, essentially recovering it 

### While in Command Mode 

`SHIFT + D` --> delete a line's content but not the line itself

`dd` --> delete a line

`yy` --> copy a line

`d --> w` --> delete a word 

`v` --> enter visual mode (highlight by lines) 

	- UP & DOWN Arrow keys to highlight   
	- `yy` to copy highlighted text 
	- `d` to delete ~ 
	- `ESC` to exit 

  - visual mode grab chunks of text:

```
"v" --> "G" + "end line number" = select from cursor location to specified line number

  e.g. v --> G + 76 = highlight text from cursor to line 76

[link here](https://stackoverflow.com/questions/7406949/vim-faster-way-to-select-blocks-of-text-in-visual-mode)

## SELECT ALL

"ggVG" 

[link here](https://superuser.com/questions/227385/how-do-i-select-all-text-in-vi-vim)

```

`u` --> undo last action 

`CTRL + r` --> redo action

`SHIFT + a` --> insert at the end of a body of text

`SHIFT + h` --> top of current page

`SHIFT + l` --> bottom of current page

`[[` && ` ]]` --> beginning & end of the entire text

`b` --> move back a word

`e` --> move forward a word 

`CTRL + b` --> move up one page

`CTRL + f` --> move down one page


\
\

- [moving around in Vim](https://vim.fandom.com/wiki/Moving_around)

### Commands

\

`:set hlsearch` --> enable search highlighting

`:noh` --> remove highlighting in text

`:syn on` --> enable **syntax highlighting** (great for writing code) 

[how to permanently enable this](https://apple.stackexchange.com/questions/295459/color-code-for-vi-on-terminal-on-mac) 

- Note, `.vimrc` has to be MANUALLY created in `~/` (basically your home directory)

  - sample `.vimrc` entries: 

```
  1 
  2 
  3 " Set colorscheme to Ron 
  4 
  5 colo ron
  6 
  7 " Turn on line numbering
  8 
  9 set nu

``` 

`ls /usr/share/vim/vim74/colors/` --> list color options for vim 

```
blue.vim      delek.vim    evening.vim  murphy.vim     README.txt  slate.vim
darkblue.vim  desert.vim   koehler.vim  pablo.vim      ron.vim     torte.vim
default.vim   elflord.vim  morning.vim  peachpuff.vim  shine.vim   zellner.vim

```

- to choose a colorscheme: `:colorscheme [ ]`  

e.g. `:colorscheme elflord` --> `:colorscheme default` (go back to original colors) 

- shorthand for above: `:colo [desired scheme]`  

e.g. `:colo ron` [more info here](https://alvinalexander.com/linux/vi-vim-editor-color-scheme-colorscheme) 

[more details on vim & different color schemes to pick](https://www.cyberciti.biz/faq/turn-on-or-off-color-syntax-highlighting-in-vi-or-vim/) 

[extra colorschemes for download](http://www.vimninjas.com/2012/08/26/10-vim-color-schemes-you-need-to-own/) 

`:set nu` --> enable line numbers 

`:set nu!` --> disable line numbers [more info here](https://www.cyberciti.biz/faq/vi-show-line-numbers/)


**Wanna use the Mouse?**---`:set mouse=a`

- disable mouse `:set mouse=`

`:f` or `:file` --> display the current file being edited

\

### TEXT MANIPULATIONS: Search & Replace

- `%s/search/replace/gc` 

  - `g` --> global  & `c` --> confirm

  - [good link for vim search n replace](https://www.linux.com/tutorials/vim-tips-basics-search-and-replace/)

\

### TEXT SEARCH:

- `set ic` --> "case insensitive" 

- `set noic` --> "case-sensitive" 

- or we can used the "\c" escape sequence after the search term, e.g. `/<search term>\c` = case-insensitive; use `\C` for case sensitive search

  - [link here](https://stackoverflow.com/questions/2287440/how-to-do-case-insensitive-search-in-vim)


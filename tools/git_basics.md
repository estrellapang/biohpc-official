## This Markdown file contains some of the basic operations in using git

[look here for essential git commands](https://orga.cat/posts/most-useful-git-commands)

- for BioHPC git repository; when generating a ssh key, **DO NOT ADD A PASSPHRASE**, otherwise during the authentication process, the repository will ask for the git account password, which you do not have--> after several failed attempts, your local host will literally be **black-listed** by the remote server, thus not being able to access the repository at all!!!

#### In RHEL/ CentOS, check if git is installed: 

`which git` 

`git --version`
 
`yum install git` 

#### Let git know that you are working on a new machine (only do this once) 

`git config --global user.name "Zengxing Pang"` 

`git config --global user.email "zengxing.pang@utsouthwestern.edu"` 

Check your git credentials with `git config -l` 

#### Initialize a git repo

`git init` 

Besure to first go to a desired directory first! 

> we don't want to modify any of its contents! So `chmod 700 .git` is a good idea :) 

To confirm the connection to your credentials, try 

`ssh -T git@git.biohpc.swmed.edu` (or wherever your remote  gitlab/github server is located)' 


#### Adding a SSH key: 

- if you don't have a SSH key:'

`ssh-keygen` --> this creates a key 


you will be prompted to decide upon the directory of the key pair (one public, one private) 

	- Enter file in which to save the key (/~/user/.ssh/id_rsa) 

press ENTER to accept the default, or enter your own .ssh directory e.g." /~/Users/.ssh/my_keys "

CAUTION: if no directories are specified, ONLY a FILENAME, then your keys will be created in the ~/ folder! 

	- Enter passphrase (empty for no passphrase):

press ENTER for no passphrase (and again to confirm)  

It is generally a good idea to have a decent passphrase 

Once this is done: 

	- Your public key has been saved in [keyfile name].pub 

	- The key fingerprint is (randomly generated characters and random art image)

&nbsp;

- if you already have a SSH key pair, add your .pub key fingerprint to your remote server: 

`ssh-copy-id -i ~/.ssh/[keyname].pub  git@git.biohpc.swmed.edu`

This logs into the server directly and copies the public key into it  

- The second approach: 

Go to your server account's web page --> go to "Setttings" --> "SSH Keys" --> Copy and Paste your .pub fingerprint there 

- To Verify: 

`ssh -i ~/.ssh/[keyname] [user]@[host I.P/name/]` --> this tries to let the ssh server authenticate your credentials with a specific file

`ssh -i ~/.ssh/id_rsa.pub zpang1@198.162.54.1` --> ssh into zpang1 via the pubkey you've copied

&nbsp;

NOTE: for git, especially if you are using a public network; things might be a little different: 

to AUTHENTICATE your ssh key to the remote git repo; ALWAYS use "git" as the [user]@[host], e.g. `ssh -T git@github.com` 

if CONNECTION successful, you should get the following output, and no password prompted for the [user@host]  

	- Welcme to to GitLab, [your name]!  

NOTE: to verify connection, for [keyname], we DON'T HAVE to SPECIFY ".pub"!!! We also don't have to use the " -i " option; simply "ssh -T" 

&nbsp;
&nbsp;

#### Associate a Name to your Remote

- Add remote repo to local repo

`git remote add [desired_name] [repo https/ssh URL]` 

e.g. `git remote add origin https://github.com/estrellapang/Learning_Linux.git` 

\

- Change remote URL name

  - below example, change to SSH url

`git remote set-url origin git@github.com:estrellapang/HPC_Journey.git`


\

- LOCALLY rename a remote repo

`git remote rename [oldname] [newname]` 

- Change REMOTE repo name

  - log in to github, change repo name under settings

  - determin new URL of renamed remote repo

  - set new URL for local repo that pulls from the remote

  - `git remote set-url origin git@github.com:User/project-new.git`

  - [stack overflow reference](https://stackoverflow.com/questions/2041993/how-do-i-rename-a-git-repository)

\
\

#### Setting Up a Proxy to A Remote: 

`git config --add remote.[your remote nickname].proxy "[proxy URL with port #]"` 

example: `git config --add remote.origin.proxy "proxy.swmed.edu:3128`

&nbsp;

### Begin Contributing 

#### Associate "Nickname" to your Remote Server

`git remote add [desired nickname] [remote actual URL]` 

#### Download from remote  & Initiate a Local Repo

`git clone [remote repository/project path]` download everything existing in the remote server to your working directory 

#### Add, Commit, Push 

1. make a change: create/edit files
2. add the change: `git add [filename]` --You can also add the name of the directory in which you made a change so long as you are within the overall git repodirector
3. commit the change: `git commit -m "description/comment"` --it's better to add the -m comment option--SAVES TIME! 

NOTE--> do not include any file/directory names inside the comment; things tend to break 

4. push the modification: `git push [remote name/URL] [brancename]`  e.g. `git push origin master` 

#### Fetch, Pull 

`git pull [remote name] [branch]` -- e.g. `git pull origin master`  

***explain the difference between fetch & pull***

**show difference on a branch between local & remote repositories** 

1. do a fetch: `git fetch [remote name] [name of branch]` 

2. `git diff --name-only [branch] [remote name]/[branch]` 

e.g. `git diff --name-only master education/master` 


### Repo File Mangement

#### Delete a File

`git rm [filename]` --> automatically sets this deletion for commit

#### Moving files into New Directories 

1. Move the file

2. Add the new directory folder onto stage

3. cd out of the new directory, `git rm [moved file]` --> to let git know that the moved file will be deleted from its previous directory 

#### Renaming Files 

1. `mv [old name] [new name]` 

2. `git add [new name]` 

3. `git rm [old name]` 

4. VERIFY: `git status` --> the bash prompt should return "RENAMED: [name of your file]"

#### Troubleshooting 

- **recover files** 

  - if deleted and NO COMMITS and PUSHES has been done: `git reset --hard`

  - if **EDITED** files but did not commit changes, however on the branch significant upstream changes have occured

    - Revert to most recent commit prior to your own changes [link here](https://stackoverflow.com/questions/215718/how-can-i-reset-or-revert-a-file-to-a-specific-revision)

\
\

### Version Examine

- show commits

  - `git log` 

- see changes of each commit

  - `git show <commit #>` 

\
\

### Setting Up SSH Keys for WSL

- [link here](https://peteoshea.co.uk/setup-git-in-wsl/)

#### Prevent git from ALWAYS asking for login and password

- set remote URL to SSH-based

- configure `.git/config`

```
### exmaple from ThankPad01's WSL

[user]
        name = estrellapang
        email = heres.pang@gmail.com
[core]
        repositoryformatversion = 0
        filemode = true
        bare = false
        logallrefupdates = true
        sshCommand = "ssh -i ~/.ssh/id_rsa"   #NOTE: don't specify pub key !!!
[remote "origin"]
        url = git@github.com:estrellapang/HPC_Journey.git
        fetch = +refs/heads/*:refs/remotes/origin/*
[branch "master"]
        remote = origin
        merge = refs/heads/master

```

- test your ssh key

```
ssh -T git@github.com

# sample output
-------------------------------------------------------
The authenticity of host 'github.com (140.82.113.4)' can't be established.
RSA key fingerprint is SHA256:nThbg6kXUpJWGl7E1IGOCspRomTxdCARLviKw6E5SY8.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'github.com,140.82.113.4' (RSA) to the list of known hosts.
Enter passphrase for key '/home/estrella/.ssh/id_rsa':
Hi estrellapang! You've successfully authenticated, but GitHub does not provide shell access.
-------------------------------------------------------
```

- [reference 1](https://dev.to/web3coach/how-to-configure-a-local-git-repository-to-use-a-specific-ssh-key-4aml)

- [reference 2 - is git store credentials out-dated?](https://www.freecodecamp.org/news/how-to-fix-git-always-asking-for-user-credentials/)



## When Git isn't Gitty

- **ISSUE**: SSH key works, but Permission Denied to Pull/Push repo

  - solution: change Git HTTP URL to that of SSH URL

  - `git remote set-url origin <URL>`

  - [reference01](https://stackoverflow.com/questions/14762034/push-to-github-without-a-password-using-ssh-key)


\
\

- **ISSUE**: SSH key does not meet Server's encryption algorithm standards

  - solution: generate a key with ed25519 hashing algorithm/cryptography standard 

  - `ssh-keygen -t ed25519`

  - [reference talks about ssh-keygen -C option](https://www.unixtutorial.org/how-to-generate-ed25519-ssh-key/)


\
\
\

- ### Further Reading

- [git deploy key, RHEL Openshift](https://www.openshift.com/blog/private-git-repositories-part-2b-repository-ssh-keys)



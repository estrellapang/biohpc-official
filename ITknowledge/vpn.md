## Virtual Private Networks(VPNS) 

> what allows hosts on the internet to connect securely over encrypted tunnel router routes and authenticate as into secure LAN networks from external locations

- has nothing to do with Active Directory or Group Policies

### VPN Attributes 

- **Tunnel Protocol** that checks against interceptions; if detected, will look re-establish tunnels over different route

- **Encryption**: data being transferred wihin the tunnel protocol are also encrypted  

- **Server-Client based interaction**:

  - client connect with server to authenticate, and then a session is established; the VPN server will let you keep the same credentials, but all your requests will be first sent over a secure tunnel to a remote server, that will in turn relay your data to the intended destination 

  - server-client solutions are specific 

### Disadvantages: 

- UPLOAD connection speed could be reduced to that of the slowest connection over the tunnel connection to the VPN server, including that of your own upload link e.g. if internal connections are 10G/s, but the connection to the external network speed is 1Mb/s, then the speed of data transfer doneby you via a VPN server will not be 10G but be reduced to 1Mb/s 

 
- Infrastructure shortcomings: old wiring tend to drop packets, and VPN will mistake it as a hacking attempt, and will therefore continually drop tunnels and re-establish them. 

  - older routers (15+ yrs old) do not allow VPN passthroughs, and could stop your VPN connection.  

### Protocols 

	Internet Protocol Security (IPSec) 

- 2 modes: Transport & Tunnel mode 

	L2TP (Layer 2 Transport Protocol)

- auto-built in 

- uses IPSec Encryption

- UDP Port 500 (has trouble getting around firewalls) 

	PPTP (Point to Point Transport Protocol) 

- made by MS for dial-up connections 

- clients auto-built in most OS's 

- TCP port 1723  

- vulnerable 

	SSL/TLS (Secure Socket Layer/Transport Layer Security) 

- used commonly on web browsers (e.g. https) 

  - e.g. SSTP (proprietary protocol by MS; uses SSL 3.0 Encryption; TCP port 443, can bypass most firewalls) --> more secure than PPTP & L2TP (however only works on Windows)
	OpenVPN 

- needs third party client, most auto-installed

- one of the more secure encryption protocols (AES) 

- TCP port 443 

- more versatile & secure

	SSH 

 
 

## The Importance of Time-Keeping & NTP Servers 

- **useful links** 

[link1](https://insights.sei.cmu.edu/sei_blog/2017/04/best-practices-for-ntp-services.html) 

[link2](https://timetoolsltd.com/time-sync/network-time-sync/) 

[link3](https://www.masterclock.com/company/masterclock-inc-blog/network-time-synchronization-why-you-need-an-ntp-server)

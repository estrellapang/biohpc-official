## Fundamental Concepts in Operating Systems 

> An operating system takes the road of reducing the barrier of managing tasks and> their resources, providing the interfaces for various hardware and software comp> o nents. Let's see the scenario when no OS is used. Programmatically, it's possi> ble to develop a program, up and running, without an OS.

### OS Characteristics

- Resource & Power  Management 

  - how much memory & space an OS takes (ideally, less)

- Portability & Modularity (support various devices & platforms) 

  - how many additional features, frameworks, & libraries can it support? 

- Support for a range of network connectivity options 

- Security & Stability 

- Product development (tool sets, licensing (open-source licenses i.e. permissive & copyleft & copyright), support) 


### General Purpose vs. Embedded OS's 

- General Purpose OS 

  - full-fledged e.g. Linux, Windows, Mac OS 

- Embedded OS 

  - stable & efficient e.g. uses low power microprocessors within constrained devices i.e. smart sensors, controllers, general appliances (refridgerators) 

  - OS's for Embedded Devices: FreeRTOS, Mbed OS, Mynewt OS, Zephyr, RIOT, Contiki OS, TinyOS (most of these are under permissive or copyleft licenses) 

    - FreeRTOS: uses the RTOS kernel, which is popular for supporting small embedded systems i.e. microcontrollers & microprocessors (under the MIT permissive license) 
      - supported by processors e.g. Arm, MIPS, AVR, PIC, PowerPC, & MSP430 -- from 8 bit to 32 bit processors 

    - Mbed OS 

      - Produced by Arm co under the Apache License 2.0 

      - RTOS kernel; good for quickly building applications/prototyping 

      - supports Arm (Cortex-M Processor) 

    - Mynewt OS 

      - RTOS kernel;includes network protocols needed for most applications 

      - under Apache License 2.0; Processors: Arm, MIPS, RISC-V 

    - Zephyr 

      - Apache License 2.0; built by Linux Foundation & Wind River Systems 

      - RTOS kernel & common networking protocols 

      - Processors: x86, Arm, ARC, Altera NIOS II, Xtensa Cadence 

    - RIOT 

      - released by LGPLv2 license 

      - microkernel architecture; partially compliant with POSIX API's; network protocols 

      - **real-time capability**, processors: AVR, MSP430; MIPS; PIC; x86; Arm 

    - Contiki OS 

      - under the BSD license; research based; development & network protocol statcks 

      - processors: Arm, AVR, MSP430 

    - TinyOS 

      - BSD license; research based; wireless networking protocols 

      - supports nesC programming 

      - processor: Atmel AVR, MSP430, MSP432 (Arm) 


### The Linux Kernel 

- Full/generic Linux OS's: Debian, Raspbian, Ubuntu, & Red Hat, etc. 

- Embedded Linux OS's: lack memory management units; the component that sits between processors and RAM memory 

  - uClinux: 

    - linux kernel & uses cClibc library a C library specifically designed for embedded Linux (full Linux OS's use glibc) 

  - Yocto Project: 

    - provides platform for creating custom embedded Linux distributions  

    - processors: arm, PowerPC, x86, x86-64 

  - OpenWrt 

    - Linux distribution for wifi routers 

    - supports Wifi, Ethernet, & cryptographics 

    - processors: Arm, MIPS, AVR32, PowerPC, x86, x86-64, etc. 

  - Android Things 

    - developed by Google; linux kernel, Java API framework & its libraries

    - good array of network protocols

    - contains the Android development environment 

    - security enhanced 



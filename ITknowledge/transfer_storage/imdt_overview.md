# Intel Memory Drive Technology (IMDT)

> a means of expanding system memory beyond that of DRAM by making SSD into byte addressable memory---fat nodes made cheaper?

	IMDT implements software-defined/virtual memory + Intel Optane low-latency PCIe-SSDs 

## Highlights

- Operates on the 'hypervisor layer', which is atop the hardware & below the operating system 

## Hardware Components

### Intel Optane NVMe SSD

- utilizing of 3D XPoint memory cell (14 + of them are on the backside and underneath the heatsink): wrapped in a NVMe SSD card

  - lower latency (under 10 micro-seconds) 

  - higher IOPS (input/output operations per second) 

  - nearly equal read/write speeds 

> The Optane SSD DC P4800X utilizes seven channels with four dies per channel---what are "dies" in "channels"? 
&nbsp;

## Software Components 

	**IMDT**--- virtual memory software that executes at the "hypervisor" layer--- below the OS and directly on the hardware 

- Overview of Action: 

  - monitorts memory access patterns to cache actively used data in DRAM & prefetch from the Optane SSDs 

    - duplicating the memory **paging** system that already exists? 
    
    - similar to managing SWAP space, but more efficient due to Optane SSD performance 

    - Prefetching: IMDT utilizes  machine learning (to prevent cache misses)

- Takes account of non-uniform memory access effects (NUMA) and attempts to optimize data placement to either DRAM or Optane SSDs

  - IMDT will by default present to OS a NUMA topology similar to that from the DRAM, but does not staically allocate specific address ranges to DRAM or Optane SSD---this allows OS & applications to function without modifications or specific optimizations 


## Number Rundown 

- IMDT Memory Expansion overhead: 320GB for a  375GB drive = 15% space lost due to overhead

  - Intel's recommended Optane SSDs to DRAM ratio: 3 vs. 1 or 8 vs. 1

- good for large sequential reads, parallel processes

- caveat: much smaller bandwidth than DRAM



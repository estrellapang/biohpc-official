{\rtf1\ansi\ansicpg1252\cocoartf1671\cocoasubrtf200
{\fonttbl\f0\fswiss\fcharset0 Helvetica;}
{\colortbl;\red255\green255\blue255;}
{\*\expandedcolortbl;;}
\margl1440\margr1440\vieww14340\viewh15360\viewkind0
\pard\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\pardirnatural\partightenfactor0

\f0\fs24 \cf0 ## Data Transfer & Storage Overview \
\
### SSD vs. HDD \
\
#### SSD (Solid State Drives) \
\
- utilizes NAND (non\'97volatile) based flash memory\
\
- performance dictated by controller \
\
- - facilitates faster boot & shutdown speeds compared to HDDs \
\
- connects to SATA & miniPCIe slots (both have acted as bottlenecks in the past)  \
\
  - now SSD\'92s can utilize PCIe slots thanks to NVMe protocols\
 \
\
####HDD (Hard Disk  Drives) \
\
- utilizes magnetism to store data \
\
- performance dictated by RPMs\
\
- more affordable \
\
- utilizes SATA \
\
\
### Data Transfer Protocols \
\
- AHCI: Advanced Host Controller Interface (supports SATA & IDE) \
\
- **NVMe** \
\
  - has nothing to do with form factors \
\
  - describes how bus component communicates with the motherboard \
\
  - NVMe SSD\'92s \
\
> NVMe allows flash memory to act as a SSD through PCIe interface as opposed to the traditional SATA interfaces \
\
  - NVMe SSD\'92s utilizes either **M.2** or **PCIe** connectors to go into **PCIe** slots in the computer \
\
  - allows parallel I/O \
\
  - has way more queues: 64k queues with 64k commands in each queue \
  \
    - AHCI only offers 1 queue with 32 commands within\
\
\
### Transfer Interfaces/Form Factors \
\
- **SATA**  - unidirectional: can only either read or write, not simultaneously \
\
- **PCIe** (offers faster transfer rates & lower latency due to having more channels connected to the CPU 25 as compared to 10 seen in SATA interfaces) \
  \
  - bi-directional: simultaneous read & write \
\
  - connection is divided up into lanes; each lane has 2 wires: a sending and a receiving wire \
\
    - each lane makes an independent connection b/w PCI controller & expansion card/slot \
\
    - # of lanes vs. resultant bandwidth \'97> **Linear Scaling**  e.g. 8-lane = 2x 4-lane bandwidth \
\
  - Current configuration of PCIe slots: x1, x2, x4, x16 slots \
\
- PCIe Transfer Rates: \
\
- PCIe 1.0 & 2.0 \'97> utilizes **8b/10b encoding**\
\
  - in this methodology, for every **8 bits that are transferred, 2 bits are lost** to physical overhead\
\
  - per PCIe 1.0 lane = 2.5 **GT/s** (Gigatransfers/second) = (2.5 Gigabits/second) * 0.8 (due to overhead) / 8 (bits/byte) = **250 Megabytes/second** \
\
  - per PCIe 2.0 lane = 5 GT/s = **500MB/s**\
\
  - PCIe 3.0 utilizes **128b/130 encoding** \'97 this means way less loss due to overhead!  \
 \
  - per PCIe 3.0 lane = 8 GT/s = 8 (Gigabits/second) * (128/130) / 8 (bits/byte) = **985MB/s** \'97> nearly twice the speed of PCIe 2.0 accomplished with only 60% increase in throughout/transfer rate! \
\
**NOTE** \'97> if your CPU chipset does not have enough PCIe lanes\'97> then bulking up on expansion PCIe slots could only bring down the transfer rate for each expansion device. [really good info here](https://www.tested.com/tech/457440-theoretical-vs-actual-bandwidth-pci-express-and-thunderbolt/)  \
\
- **M.2** (Mini PCIe v2) \
\
  - compact & low power\'97intended for use within Laptops & Tablets\
\
  - comprised of 4 PCIe lanes\
\
  - a smaller form factor designed to replace mSATA interfaces \
\
  - can interface with SATA III, PCIe III (obviously offers the fastest speed), and USB III  \
\
  - 3 major types of slots: B, M, & B+M [more info & pictures here](https://www.howtogeek.com/320421/what-is-the-m.2-expansion-slot/) \
\
\
\
}
## Computer Science Base Knowledge 001 


### Computing History 

#### Beginning: Preserving the State of Calculations 

- computing tools reduced the entry barrier for advanced/laborious calculations 

- e.g. abacus, Step Reckoner (first machine to do all 4 basic arithmic operatios, Difference Engine, Analytical Engine--remember, multiplication & division are just adding and subtractng)) 

- before devices, "computers" were a job title 

- Ada Lovelace: the first programmer who wrote a language for the Analytical Machine 

- Hollerith's machine fastracking the census process--eventually leading to the formation of IBM, the international Business Machines corporation 






## Introduction to Boolean Logics

### Why Binary Representation is Used?

> In short, to represent True & False conditions

#### Underlying Mechanics 

- **transistors**: the most advanced form of non-mechanical/non-electromechanical relays available that can control the flow of electricity at rapid speeds

  - current flow --> ON state (TRUE - "1")

  - no current flow --> OFF state (FALSE - "0")

  - CAN MORE THAN 2 STATES BE REPRESENTED BY TRANSISTORS? Yes, early computing flirted with Ternary & Quinary systems, only to find that these systems do not respond well to environmental interference, as it was difficult to consistently keep current flows at the exact discrete rates that represent different states---it became more difficult to keep consistent current flows when state changes has to occur at thousands of times/second, which is critical for computing performance

#### Underlying Mathematics

- **Boolean Algebra!** -- invented by George Boole, a **self-taught** English mathematician from the nineteenth century, whose approach allowed truth to be proven through systematic logic equations, which consisted of the following fundamental principles:

- **Values Range from TRUE & FALSE** 

- **Logical Operations**(contrary to arithmetic operations)

  - _**AND**_: both input has to be true in order for output to be true: e.g. In: True + True --> Out: True; e.g. In: True + False --> OUt: False; e.g. In: False + False --> Out: False---representable by a **AND Gate** transistor circuit

  - _**NOT**_: turns input into opposite value e.g. In: True --> Out: False, which is physically representatable by a **NOT Gate** transistor circuit

  - _**OR**_: only 1 true input has to be true for outout to be true. e.g. In: True + False --> Out: True --- needs a **parallel transistor** circuit

  - **_XOR_**: or "exclusive OR" --> just like OR logic, but inputting the SAME values returns FALSE outputs e.g. In: True + True --> Out: False; e.g. In: False + False --> Out: False---this can also be represented by a transistor gate circuit



## Soft Intro to Crytography, and details on Feistel & Blowfish Ciphers 

### Two Main Types of Cryptosystems 

1.Symmetric: 

- Same process/key (however, they may be reversed for decryption) utilized for encryption and decryption 

  - e.g. Shift Cipher, AES encryption, Feistel cipher/function 

2. Asymmetric or Public-Private Key Encryption: 

- Encryption Key --> Public: anyone could encrypt the messages send them; but not everyone can decipher the ciphertext 

- Decryption Key --> Private: only the holder of this key can decrypt 

- e.g. SSH encryption 

### Block Ciphers 

	Contrary to stream ciphers (encrypts plaintext one bit at a time), Block ciphers break plaintext into chunks with preset lengths, the bits within which are encrypted together: 


- **Feistel Cipher**--a 16 round cipher that encrypts input in 64-bit blocks: an essential symmetrici cipher **framework** used in most, if not all modern block ciphers

  - e.g. of  Block Ciphers that utilizes the  Feistel network: DES, AES, Blowfish, Twofish, RC5, etc.

  - Features of the Feistel Cipher Network 

    - 1. Round Keys (pre-defined in segments of fixed bits, or generated via some key expansion function) 

    - 2. Round Function (defined by the implementer) 

    - 3. Logical Operations per Round: exclusive OR, substitution & permutation (repeated a total of 16 times) 
  
      - **XOR** ("exclusive OR")---outputs the true statement between two arguments, see the "truth table" below 

![alt-text](https://morf.lv/images//blowfish/xor_truth_table.PNG) 

  - Structure of the Feistel Network: ![alt-text](https://morf.lv/images//blowfish/03fig03.jpg)

  
  - Sequence of Encryption Operations per Round: 

  a.64-bit input block is split into 2X32-bit segments: a Left Block_i & a Right Block_i (L_i & R_i) 

  b.**R_i goes untouched**, and moves to last step of the round

  c.**L_i** is -XOR'ed- with Output of Round Function [ F(R_i, K_i) ], whose inputs are R_i & 1st Round Key (K_i) ---this is the substitution step'

  d.Output of step c. & R_i are permutated so that former = R_i+1 & latter = L_i+1 

  e.The next round begins with the next round key, K_i+1  

  - at the end of the 16th round, the R_i+15 & L_i+15 are simply concatenated together to form the encrypted ciphertext 

  - Decryption operates in the same fashion as encryption, except that the round keys are used in reverse order 

    - this is as result of a convenient property of the Exclusive OR operation (XOR), [see proof here](https://morf.lv/introduction-to-data-encryption)---in essence, the communicative property of XOR. 


### The Blowfish Cipher 

> a cipher utilizing the a mofidied Feistel network, with the addition of P-arrays fo> r sub-key generation and for s-boxes embedded within the Feistel/Round function of > the cipher algorithm 

#### Components of Blowfish Cipher 

1. 16 round functions with 18 sub-keys (generated from 18 entries of the P-array) 

  - 2 out of the 18 sub-keys are used for the extra "output operation" that is charactertistic of  the Blowfish

2. 4x 256-bit S-boxes--all of which are inside Blowfish's round function: 

  - structure of the round-function:

![alt-text](https://morf.lv/images//blowfish/blowfish_feistel_diagram.png) 

- as seen above, the 32-bit input is further divided into 4 8-bit segments to be eachincorporated into a s-box (substitution boxes whose generation depends on the value of the round key)---which perform an 8-bit to 32-bit mapping of the inputs 

  - **operations within le round function**: 1. outputs of S-1 & S-2 are added modulo2^32 2. result of step one is XOR'ed with output of S-3. 3. output of step two is added to that of S-4 modulo 2^32

3. 18 32-bit P-arrays/boxes (mentioned in 1.)

#### Characteristics of Blowfish Cipher 

- symmetrical, block cipher that takes 64-bit input blocks 

- modified Feistel network: the **Right** 32-bit block is XOR'ed with the output of the round function, whose input is Left block XOR'ed with round sub-key: 

	R_i XOR F(L_i XOR K_i)

- **variable** key length: from 32 to 448 bits 

- utilizes random hexadecimal sequence/generator to produce P-array (in 8-character portions, which equal to 32-bits each b.c. each hexadecimal character = 4 bits), which is used to generate the s-boxes & subkeys 

- Key generation actions summarized: 

![alt-text](https://morf.lv/images//blowfish/parray_init.png) 

  - [.........] **HOW ARE THE SUBKEYS GENERATED??!** 

#### Actions in the Blowfish Cipher 

![alt-text](https://upload.wikimedia.org/wikipedia/commons/5/5e/Blowfish_diagram.png)

**Key Differences Compared to Original Feistel Network**: key-whitening, **right** block is XOR'ed with round function output each time (rather than the left), the inclusion of an output round, and the utilization of P-arrays to generate round keys & S-boxes (pre-defined in many Feistel networks)  

a. L_i XOR K_i (key whitening, which is an additional step compared to the traditional Feitstel network)---result goes straight to the end of the round, as well as separately to the next step 
 
b. F (L_i XOR K_i) 

c. F (L_i XOR K_i) XOR R_i

d. output of step c (becomes L_i+1) is permutated with L_i XOR K_i (which becomes R_i+1) 

e. steps a-e will repeat 15 more times, each round with newly generated S-boxes & round/sub-keys

f. after the 16th round, the left block & the right block are XOR'ed with the round keys K_i+16 & K_i+17 (17th & 18th key), respectively 

g. result from previous step is permutated once more and concatenated as the cipheredtext 

#### Useful Links: 

[a bit on S-boxes](https://crypto.stackexchange.com/questions/30919/s-boxes-in-blowfish/30934#30934) 

[more on key-initialization](https://crypto.stackexchange.com/questions/24961/how-does-the-blowfish-algorithm-key-initialization-work) 

 

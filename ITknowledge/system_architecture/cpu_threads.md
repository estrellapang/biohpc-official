## CPUs & Threads 


### Overview of Threads 

> Also known as "Lighweight Processes," which are the highest level of code executed by the processor. Every process has **at least one thre> ad** 

- Difference from Processes: threads use a **shared** memory space when contributing to the same process 

  - this means: threads **share** with other threads their **code section, data section, and OS resources** 

  - processes run in **separate** memory spaces, and are independent of each other

  - where they are similar: each has its own program counter, register set, and stack space (**research what these are**) 


- What Determines the Number of Threads?

  - **# of CPU Cores**---**1 core = 2 threads** 
  
    - CPU Cores are essentially individual processors, technically labeled "PU" in chip architecture lingo

    - e.g. 4th gen. Intel Xeon processors have 18 physical cores, and after hyper threading, it can attain 36 threads

  - more than memory, # of threads determine how many tasks the system can perform at a time, but since the CPU acts upon the memory, the latter's size dictates performance just as crucially 

### A Bit of Reviewing on CPUs 

> proceesses data in a three-part, cyclic loop---**fetch, decode, and execute** 

- Fetching: CPU accesses instruction from memory, the address of which is stored in an internal(CPU) register named **program counter (PC)**---it does so incrementally and sequentially along the PC, and the memory will respond with the specific instructions accordingly 

- Decoding: CPU stores info returned by memory into another internal register named **Instruction register (IR)**---this information is stored as binary, which the processor decodes to determine what operation to perform. The role of the IRis to **hold the instruction code** while it is being decoded/executed 

- Executing: this step often require the processor to perform further fetching: 

  - e.g. initial instruction: direct processor to fetch two operands to add them and store the result in a different location 

  - Instructions for the CPU to execute breaks down into 3 categories 

    - 1.arithmetic & logic: addition, multiplicatiom, logical AND & OR & XOR, etc. The operands can either be stored in memory or be transferred to the CPU's internal register itself 

    - 2.data transfer: move data between internal registers, from memory to an internal register, or between two memory locations. This type of instructions arealso responsible for initiating I/O operations 

    - 3.control: arrange/modify the sequence in which the instructions are executed---constructing loops, if-else constructs, and do loops 

  - CPU's have separate components dedicated to each of the tasks listed above; [refer to this site for detailed description](https://www.bottomupcs.com/chapter02.xhtml), or see a simpler diagram below: 

![alt-text](http://www.cs.uwm.edu/classes/cs315/Bacon/Lecture/HTML/Images/cpu_block.png) 

[description of diagram](http://www.cs.uwm.edu/classes/cs315/Bacon/Lecture/HTML/ch05s02.html)

- Modern CPU's have Memory caches (L1,L2,L3) as well, which are essentially SRAM units that are faster than the system memory (DRAM) 

- clock cycle & clock speed: governs how quickly each of the 3-part loop is carried out [a bit more details](https://www.phy.ornl.gov/csep/ca/node3.html)


### More Details on Threads 

- The threads themselves are not physical attributes! They are more like lines of code

- To create a thread(s), a process must be first created. When a command is performed and a process is initiated, the associated thread (s) is/are the **sequence(s) of instructions** that inform the CPU what it has to do to complete it 

  - there is no limit on the # of threads that a process can use: for certain processes, multi-threading allows carrying out multiple components of it simultaneously 

#### hyper-threading

  - interesting caveat of hyper-threading: it splits the CPU utilization and slows down the response time for each thread [a case study](https://www.percona.com/blog/2015/01/15/hyper-threading-double-cpu-throughput/) 

- processes that benefit from multi/hyper-threading:

  - video editing/encoding 

  - mas? 

  - mas? 


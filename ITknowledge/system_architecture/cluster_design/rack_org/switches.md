## Tips for Problem-Preventative Network Switch Arrangement

### NEW Racks w/h Front-End Switches  

- Leave 3 rows/slots open from the top:

  - 1 slot for 1G Management Switch 

  - 1 slot for Infiniband inter-SANS(storage area network) comms.

  - 1 empty slot for backup switch (switches can malfunction too!) 

  - **if possible**: install the backupp switch installed, configured, & updated in the 3rd slot from the get-go 

- Leave 3 open slots from the middle of the rack--for the same purposes


### Cabling: 

- **Guiding Principles** 

  1. **Layout**

    - Install Severs **BOTTOM UP** (higher positions are more physically demanding & risky) 

	- Align with respect to power outlet locations 

	- think balancing phases--on avg. 3 phases/PDU

   - Connect cablees **BOTTOM UP**

	- assign swtich slots from **left to right**

    - **Separate** Ethernet & IB bundles (do one type at a time)

        - **power cables** first

	- **1G cables** second

	- **IB cables** third

	- **inter-rack connections** last 
  

  2. **Inventory** 

    - Estimate the **quatntity** & **length** of cables needed 

       - Power cables: pairs of 15 & 18-inch/dual PDU  machine
 
       - Network Cables (1-switch-on-top configuration)

	- 1-1.5 meters for rows 40-23 (top to middle); 2-3 meters for rows 23and below 
 

    - **Label** your cables from **BOTH ENDS**

       - label cables **one at a time** while they are being installed

       - orient cable labels so they are easily visible to line of sight 

\
\
\



### Notes on 10G Connections 

- 10G drops contact sysops

  - 10G I.P.'s contact Network Services

- if setting up 10G on a new campus switch

  - contact IR to make sure that jumbo frame is configured on the local switch

  - if not, reduce your interface's MTU to 1500 before jumbo frames are fully configured

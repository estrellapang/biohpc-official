## Demilitarized Zones (DMZs) 

> "Zones where Firewall Protection is Forbidden" 
>
> In networking, this is used to improve security of organizational networks by placing servicing hosts/servers IN FRONT of the organization's firewall

- could include 2 firewalls, one in front of the servicing hosts/servers(DMZ servers), and one in front of the internal hosts and servers

- in home network setups

  - are not traditional DMZ configurations, with separate computer/device acting as a :DMZ host" i.e. a separate computer, playstation, etc. 

    - the DMZ host will have all ports forwarded to it, with a **static IP** 


## Info on IPMI & BMC 

> more acronyms?! 

[link on BMC](https://www.servethehome.com/explaining-the-baseboard-management-controller-or-bmc-in-servers/) 

[more links on BMC](https://searchnetworking.techtarget.com/definition/baseboard-management-controller)

[link on IPMI](https://www.itworld.com/article/2708437/ipmi--the-most-dangerous-protocol-you-ve-never-heard-of.html)

### What is IPMI (Intelligent Platform Management Interface) 

> a set of protocols that give administrators almost total control over remotely deployed servers.
> And now-standard hardware called a Baseboard Management Controller (BMC) - let remote administrators
> monitor the health of servers, deploy (or remove) software, manage hardware peripherals like the
> keyboard and mouse, reboot the system and update software on it. 

### What is BMC (Baseboard Management Controllers)

	**A  Hardware Component of the IPMI protocol**
 
> The BMC is usually an Arm-based SoC with graphics and control logic built-in.
> It  can utilize either shared or dedicated NICs for remote access. They also have multiple connections to the
> host system providing the ability to monitor hardware, flash BIOS/ UEFI firmware, give console access via serial
> physical/ virtual KVM, power cycle the servers, and log events.

- The analogy that I typically use when describing a BMC is that it is similar to a small Raspberry Pi type device that sits in a server in order to control the larger server.

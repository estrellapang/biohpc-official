## LW Training on Network and Filesystem Namespace

### Network

#### IPMI

- `192.168.[0-4].###` are virtual interfaces that only a few admin nodes have

  - for each subnet, virtual nics are created in order to access the ipmi network

  - the ipmi addrs are assigned through the BIOS/BMC interface via shared mode

\

- **BMC console**

  - SPECIFIC GPU nodes, the web consoles are ported to run on the GPU card, specified in xorg.conf (maybe)


#### Images

- `cd /cm/images/GPU-images-7.6-A100/`

- **install**: On Nucleus003

  - e.g. copy rpm packages in there, `chroot to /cm/images/GPU-images-7.6-A100/`

  - `cd /root` && `yum localinstall <transferred_rpm package>`

- in general when images are generated via chroot jail, it creates many symlinks to the host's kernel modules

  - how containers work as well

- one can map /proc & driver directories into image

  - `mount -t proc proc`

  - mount /sys/fs/ to image

  - **the TWO are CRITICAL** to have for installing certain clients like gpfs and lustre to images, bc certain rpms need to scan kernel module information in order to set up installation scripts, kind of like how compiling software from scratch, needing to generate a make file prior to rpms are created

  - check 003 history

  - one must remember to **umount it**, or else when the compute node reboots, they'll inherit the image with proc info

\
\

- on headnodes's `../tftp/image` --> where the compute images are actually located

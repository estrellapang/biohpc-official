## Intel Showcase with PCIe v.4 Product Briefing

### Contacts

> Brian Dietrich - Intel Sales Representative for the UT System
>
> Quote: "Intel is just as much of a Software company as it is a hardware company"

### One-API project

- get more info?

\

### Networking

- what is Intel OmniPath?

  - "Cornelius" --> new company taking over OmniPath (40% shares still held by Intel)
\

### Next-Gen Zeon

- 8xCH DDR4/3200 RAM lanes

  - previous version has 6 channels

- 64x PCIe4.0 lanes

- "Whitley 2-socket platform"

  - 2 CPUs, each connected to Intel Optane Persistent Memory, then DDR4 RAM

  - each socket has 3xUPI links that are dedicated for inter-socket comms - NUMA networking based

  - each Ice Lake-SP has a MESHED design (Sunny Cove architecture)

- "Speed Select Technology" -- "Intel SST"

  - users, as opposed to previously only BIOs/admins, can now tweak the CPU frequency

  - Intel primarily targets the Cloud users

  - Slurm plugins

  - requires kernel module support

- more power consumption per socket than before

  - most high performing SKUs are 205W - 270Watts/socket

  - Brian recommends `63##` series

- scalable up to 2,4,8 and above sockets at once

- "Security Enclave" feature

  - isolates processes from all else on the machine

\

### What is Intel doing with storage?


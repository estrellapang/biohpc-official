## Upcoming Trends in Storage

> Contacts
> 
> Brian Sherman - Distinguished Engineer (spoke on IBM VM & Container Storage & Flash NVMe)
> bsherman@ca.ibm.com (416-347-8462)
> IBM WSC(Washington Systems Center) --> conducts tons of workshops has YT channels 
> GET UPDATES on IBM WSC Webinars --> accelerate-join@hursley.ibm.com
> AJ(Andrew?) Casamento - Broadcom - BSN Solutioneer
> Andrew Greenfield - Global Storage & Network Engineer
>
>
>
>
>
>
>

### Gen. Future Trends

- software defined storage is becoming more prevalent

  - efficiency; compatibility across most platforms/hardware/container runtimes/hypervisor

- spinning disks are becoming outdated

  - NVMe skips the SCSI protocol (too chatty)

  - flash supports micro-second latency

  - flash provides more storage/volume unit of space

  - cost/TB is reducing for flash (2020; 7x SSD/HDD --> 2030 x0.6 SSD/HDD)

- due to WFH trends (covid) -- cloud security, end-point-security, and identity security technologies are becoming more relevant

  - Cyber-Resiliency (institutions have been paying ransom to hackers; millions of dollars)

  - Network-Resiliency (increase availability and bandwidth) 

- Moving away from SCSI protocol to FC-based e.g. FCP

\
\

### Terms to Know

- hybrid-cloud

  - half on-premise and half uploaded to cloud; depends on use case and saves expenses

- CSI (Container Storage Interface) 

 - has separate operators/plugins/driver for either file-based (Spectrumscale) or block based operators 

 - the operators/drivers are a lower level version of the client

\
\

### Check-out

- what is spectrum protect?

- what is Spectrum Connect?

  - plugins for VMware to manage its Spectrum Virtualize/On-Cloud IBM Storage resources (expand volumes,do backups and replications,etc)

  - the filebased operator with scale clients is more "persistent" data

- what is Spectrum Virtualize

  - IBM's partnership with VMware to support its platforms

- Powersystems Openstack? Cinder Driver

\
\

### Automation

- **ANSIBLE**

- CHECKOUT "Automation Hub"

- VMware use case

  - ansible modules installed on VMWare & on attached IBM storage, allows managed nodes to create and assign/provision new LUNs to ESXI servers (see set of 3 screenshots)

\
\

### Networking

- iSER

  - "iSCSI Extensions for RDMA"

  - lower latency alternative to iSCI protocol adapters

  - runs on top of Ethernet networks; 10/25G switching

  - works on 25GB iWARP/RoCE capable adapters

\
\

#### Benchmarking Attributes

  - "OLTP Transaction Per Minute"

  - "OLTP TPM per PCPU Util"

\
\

### Hardware Storage

#### NVMe (Non-Volatile Memory Express)

- a higher performing version of SAS/SATA, exept it utilizes the PCIe bus and has less overhead from drivers, OS and applications

- NVMe Performance?

  - Parallelism on CPU Queues/Core (with SCSI protocol it was serial) 

  - each CPU core has dedicated queues per SSD

- NVMe over Fabrics (NVMeF/NVMe-oF) --- main types of NVMe fabric transports: Fibre channel (via its dedicated protocol, "FCP" -- purported to be faster/lower latency than FC/SCSI protoc), Direct Memory Access (RDMA - InfiniBand, RoCE, iWARP), and TCP

  - NVMe started out as DAS/Direct connect, but NVMe-oF is in implementation
  

- Features & Developments

  - NVMe-oF TCP enabled (TP 8000)

    - not RDMA-based

  - supports multi-path now

  - ESXi 7 does NVMe-oF support

\

#### Benchmarking Attributes

  - "Transactions Per Minute"

  - "New Orders Per Minute"

  - "IO CPU"/"%SYS"

  - "TPM per CPU"

  - "CPU %IOWAIT"




### Fun Info

- Alibaba Cloud?

- Broadcom

- ESXi 7 provides NVMe-oF support

- menti.com? (survey page)

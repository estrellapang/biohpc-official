## Spectrum Scale Problem Determination

> Contacts: Madhav Ponamgi
> mzp@us.ibm.com

### General Questions

- is the system out of innodes?

- do we have deadlocks and waiters?

- daemon not running? 

- check quorum/cluster manager/filesystem manager

- check `-A` to `mmcrcluster` for autoload when nodes do not auto-mount

- GNR compatibility?

\


`mmgetstate`

`mmlsnsd`

`mmlsdisk bassdata` 

  - if without FS, but daemon active, do `fdisk -l` on NSD servers

#### Health Checks

`mmdiag --iohist` --> checks io operations!

`mmdiag --network` 

`mmdiag --memory`

`mmdiag --stats`

`mmdiag --rpc` --> rpc traffic

- by default these shows the local node's info

- this cmd is lighter weight than mmfsdump

- `mmhealth` 

  - `mmhealth node show -N <node name>`

  - checks network, filesystem, perfmon, and threshold by default

  - e.g. `mmheatlh <> -v`

  - e.g. `mmhealth node show --unhealthy`  # show ONLY unhealthy nodes


#### I/O monitoring

`dstat --gpfs` --> specific GPFS I/O

- `mmpmon` 

- can check I/O based on I/O Blocksize SIZE and LATENCY range

  - need "histogram files"

  - needs to turn on while debugging via these files above

  - e.g. `mmpmon -i ron/roff.txt`

  - can run on any node or multiple nodes --- be aware this affects performance

  - `mmperfmon` ?

- `nsdperf` --> checks bandwidth without occupying real I/O traffic

  - could overcrowd network traffic

  - only works on the network layer! not on the filesystem?

  - `cd /usr/lpp/mmfs/samples/net; make` --> this compiles the nsdperf program on local node

  - can test for latency via histogram 

  - works in server-client relationship

  - specify parallel threads

- `mmnetverify`

- `gpfsperf` --> what's the difference vs. nsdperf?

\


### Errors to Watch For

- `MMFS_SYSTEM_UNMOUNT`/ `MMFS_DISKFAIL`

- "Expel node" messages

  - typically due to networking issues/connection issues --> check `.latest.log` and `messages`


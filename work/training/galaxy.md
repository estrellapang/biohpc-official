## Galaxy Internal Training by DM on 06192020

- check config on galaxy7 on Docs

### What is it

- can chain tools together into pipelines using  

- most tools are installed as conda environments, which can isolate dependencies from each other, downsides, dependencies can be installed many times 

\
\


### Structure

- postgreSQL can help free up python's global interpreter lock/transactional lock

- requires sudo rights for certain tools

  - in order for shared storage integration, needs its own dedicated directory

  - nfs mount for /project

  - /project/apps/galaxy7

- has user `galaxy7` as admin/application user 

- python installed inside the galaxy7's home folder to work around python being on a lower version on the server

  - how to make install on specific locations?


- `tool_conf.xml` --> create tool-set folders

- every new tool --> set to slurm as scheduler and assign ID in xml config file

- ACL applies to each user's incoming folder so that galaxy7 user can access it 

- users can upload data via ftp or via smb mount on local machine

- /cm/shared/apps/16.05.8 --> can access directly from cluster?

- needs to add DRMAA as slurm pluging and needs exporting a variable in galaxy7's .bashrc 


\
\

###


\
\

###


\
\

### tips

- `idle_timelimit 90` set this in /etc/nslcd.conf if server is complaining about timeouts

- `sudo -E` --> preserve sudoer user's environment 
\
\

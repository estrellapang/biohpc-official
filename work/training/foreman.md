## Foreman/Puppet Training: MA-03-19-2021

### Configuration Files

- can do config file checks

  - e.g. `fstab.pp` && `hosts.pp`

- package settings

  - the repos are hosted on the foreman server

  - `software_rhel.pp` --> main

  - can have separate puppet configs like `software_biohpc.pp`

  - can specify versions, exclude packages


- `init.pp`

  - controls proxy and more base configs

  - here we must list all other .pp files
 

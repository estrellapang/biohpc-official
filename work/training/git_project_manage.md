## 08-07-2020 Git Issue Board/Project Management

- Analogous to MS Azure DevOps

- Tracks Open Issues

  - can assign to members of group

  - can see who's working on who

  - drag and drop

- Check Milestones (associated with Git merge requests)

- can create new boards

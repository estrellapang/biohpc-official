## "THe Cockroach Hour: Containers, Openshift and Kubernetes with RHEL"

- Cockroach Labs: serves Cloud platforms and databases

### Contacts/Hosts/Resources

- Jim Walker (host, marketer - Cockroach Labs)

- Scott McCarty - RHEL containerization (RHEL speaker)

  - "podman" service

  - 9 yrs of experience at RHEL

- Tim Veil (Opensource Solutions Engineer at Cockroach)

- Who are CoreOS? (What's Rocket? a container Runtime Environment)

- Openstack?

- KubeCon


### Overview Containers

- They are simply "fancy" processes

  - "file registries" ("fancy file servers")

  - "Can we run a database in a container?" = "can we run a database in a proces?!"

  - essentially overlay of filesystem layers; only topmost dir can be written into

  - google "containers internals labs" for topology

- What is "podman"?

- Main components

  - container images ("serverless" & "distro-less" is an abstraction; still needs OS just without the package manager, they still rely on the dependency trees of their native OSs)

  - container hosts

  - "stateful sets" --> tries to retain statefulness, but not useful for absolute uptime

- What are operators?

  - an app/component "robot admin" in the openshift cluster

  - get the status of other components in the cluster

  - checks the states

\

### Openshift 4 

- Openshift Use Cases

  - pre-built applications and solutions

  - OS indepence; portability for running anywhere; restart services thousand times

  - automating the updates that were really complex in Kubernetes

- has its own dedicated, centralized database

- Openshift is better at changing the "States?"

- Essentially an RHEL reinforced Kubernetes distro

- NEW:

  - a single restAPI can manage the whole openshift cluster

  - takes away the need to write yaml

\

### Ideas

- Standardize Automation; Deployment

  - Selling point for Openshift & Ansible

- ppl are using multi-clouds without knowing it

\
\
\

### 03-12-2021: Peng Liang - Vagrant Test Environment

- [openshift single node instructions](https://www.youtube.com/watch?v=uBsilb4cuaI)

- clone "Openshift Development Environment"

  - includes centOS vm plus vnc setup instructions

  - `vagrant up` to deploy it

  - check documentation for requirements(KVM,vagrant version,networking, CPU settings)

  - needs an Redhat Developer account for pulling secrets


- can log into console from cli

  - `oc login -u kubeadmin -p <url>:<port>`



##

### Intel Persistent Memory

- uses a dedicated chip to map M2 (usual disk drive form factor uses "U.2" form factor) connection to memory address, so applications can understand the mem address of the persistent mem module

- application mode

  - mount as fast filesystem; good for serial applications i.e. python that doesn't benefit from large memory, python has problem with memory access---with help of cuda, maybe. Simpliest way is to provide a fast /tmp directory via persistent memory for python performance boost

- memory mode  

  - memory address mapping

  - doesn't benefit from random memory access

\

### NVMe over Fabric

- the distance of fabric affects latency performance

\

### Intel Optane/3D Cross Point

- Stacked, 3D architecture

\

### News

- new intel chips with additional memory lanes --> increases persistent memory performance!

- U.2 will be phased out with U.3 to access PCIe v.4

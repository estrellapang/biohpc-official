## Hossein F. to discuss MS Azure HPC Cache

> June 12th, 2020 

### Purpose

- Reduce IO latency: cache staying at the cloud servers' side ( where virtual nodes are installed)

- 



/
/

### Limitations

- support only NFS3 protocol

- max throughput: 2-8 GB/s (internally)

- cache size: 3 - 48 TBs

  - each increase in specification costs more

- has several mount modes similar to AFM (ro, rw, etc) 

- **COST**: ~ $7000/month 


/
/

### Benchmarking

- refer to pictures

- ![chart & graph](location of picture file)


/
/


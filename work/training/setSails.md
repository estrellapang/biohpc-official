##

> Top Guides, References

```

```

### Networking

#### Create virtual subnets --> check Nuc003/Nuc006

#### Wireshark


\
\
\

### Cluster Management

#### pdsh ('syncfile')

#### ansible

#### puppet - foreman

#### Centralized logging

#### Security

- email admin everytime someone logs in

- script bash session and save to centralized location



\
\
\

### Containerization

#### Packages

- EasyBuild

- download container image to use as software

  - performance/passthrough(?)

- create a docker/singularity/podman image

- Openshift training/Kubernetes training

\
\


### Scripting

#### System-checks

- hardware failures

- log cleaning

- rsync using `parallel`

- rsnapshot backups

- SLURM job scheduler

\
\

### Storage

#### NFS

- thread optimization

- packet size - jumbo frames

- HuaiWei NFS configuration


#### Parallel Filesystems

- GPFS IPoIB bond crreation

- Lustre Multi-rail

  - controller/RAID architecture


#### IBM

##### IBM TCT (Cloud-tiering)

- any data that is cold/untouched, will be synced to cloud

\
\

#### Hardware

#### Nodelists

#### IPMI/iDRAC/ILO/IPMI


### Special Projects

#### Lamella


#### LamellaSE


#### DWDM


#### AFM-Migration


#### Openshift/LFS integration

  - CSI Driver implementation

#### Liqid SLURM integration

\
\
\

### Original

## Exit Strategy

\
\

### Big Projects

#### Lamella Upgrade

#### DDN Lustre

- set up account for Murat and Xiaochu

#### GPFS

- copy documentation

#### Document Network Testing 

\
\

### Skills to Learn

#### Scripting

- copy scripts (portal, project scripts, cron scripts, rsnapshot scripts)

#### Package Installation

- copy module installation

#### Containerization

\
\

### Showcase

#### Search Past Resumes
 
#### Search interview questions


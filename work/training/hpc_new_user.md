## Notes on BioHPC New User Training

> Presentation version 082020


### Power Transition Words

- "Now--!"

- "Okay?!"

- "Great!"

### Candy Topics

- Scientific Python Libraries Ananconda

- MATlab, R

- ImageJ, ParaView

- Git --> share code-based projects, whether it is data analysis scripts, pipelines, or applications that you or other researchers have developed; public

- Astrocyte, for those of you who conduct genomic sequencing work; RNASeq pipelines --> direct access to our

- Exposure to services and a platform where you may consult with the expertise of our user base community

### Speech by the page

- Cover page; greetings

```
Gooooooooooood Morning everyone, thank you for coming to our New User Training! Whether you are offically admitted as a BioHPC User or are here for interest,
We aim to provide you with useful and exciting information on BioHPC, specifically in terms of Who We Are, What We Do, and most importantly, What Services and Privileges
You as a BioHPC user gain when your lab or department become our patron
Now, My name is Zengxing, please feel free to call me Zeng. I am one of the guys working in the backend of BioHPC's services and infrastructure,
And today I will introduce to you the main components and policies of BioHPC, and to briefly demonstrate for you on how to access our wonderfull services

```


- Team page;

```
So let us begin---
First, who make up the BioHPC Team? In short, a select group of highly multi-talented professionals with various scientific and computing backgorunds, who all come together to form an incredibly cross-functional team, 
all dedicated towards the same goal of offering you the powerful HPC tools for research. We are officially under the Lyda Hill Department of Bioinformatics, who are currently headquartered in south campus' E building

On another note; we are constantly acquiring new talent, and in fact, our team will welcome 3 additional members who will onoboard this fall and to soon contribute to our organization and UTSW
```

- HPC Concept;

```
Now what do we do, in short we run a HPC center, and HPC stands for
High Performance Computing, and as the definition goes, it is
<read line>

okay, the what challenges require HPC?

These often are calculations relying on large datasets, like genomic sequencing and high resolution image processing.
or computations using complex algorithms, such as those utilized in deep learning and molecular, protein structure modeling, where potentially millions of calculations are needed per data set
and lastly there are certain software tools whose full functionalities cannot be well leveraged using the limited power of standalone lab computers---

So you run into these problems, what do you do? You use a super computer to perform the above mentioned tasks!

And this is more precisely what BioHPC offers its users, indiviualized access into the powerful labyrinth of a super computer---and on the user end, you may submit jobs or processes to run through the terminal/or the command line interface, for those of you who are already Linux or HPC savvy, or you may perform your research tasks using remote desktop GUI sessions, or through web the interfaces we built to use our broad list of software tools, or to submit your jobs to be run in the background on our cluster 

Alright, before we proceed further, now that we've overviewed what BioHPC can do, 
we need to realize, before we begin to form misconceptions, what we are not intended to do,

First, although our system is equipped with tons of storage space, we are not a commercial data center that rigorously backups your data incrementall, meaning we don't by default keep for example, 13 month old version of a file you've uploaded and have extensively modified over time; we also do not perform minute, high-frequency backups, meaning if you upload a file at 10 AM in the morning, and accidentally delete it an hour later at 11---that file is as good as gone forever. 

That said we do conduct weekly and monthly backups, but do not entirely count on this as a safety net against one's own human errors. Your data is safe with us 100%, but it's not always safe from accidental deletions that occur from the users end. Another word of caution is that we prohibit the use of BioHPC for personal gain; this is a UTSW instutionalized tool, and it should only be used towards legitmized research, not for watching YouTube videos, laughing at memes, or to mine bitcoins. We have a lot of security and monitoring systems in place for this type of stuff, so please respect this standard policy.

Now that's out of the way---let's talk about how the HPC or supercomputing infrastructure works, is via the clustering of numerous individual computing units called nodes, which are essentially servers beefed up with high specs. And these nodes together wait to receive jobs, or processes submitted over the internet, through a centralized launching pad server which we call the login node. Through this server, our users' jobs get sent to be processed by individual, high-powered compute nodes.

To peel back a layer of abstraction, we can look at the machinery behind the BioHPC Supercomputing cluster---we call it "Nucleus", this picture is a small corner of it, in total it is comprised of approximiately half a thousand servers of varying classes, we divide them by into two large groups: CPU nodes and GPU nodes---the CPU nodes are classed by the RAM size, and GPU nodes are categorized by the type of GPU cards they carry; our GPU nodes carry high-end, Nvidia Tesla GPU cards designed for deeplearning and general GPU computing, each hold video memories from 8 to 32 GB, and each GPU server carry 256GB of RAM (expect +300 nodes)

So of course, the BioHPC users get their slices of this pie---at any give time, each user may reserve up to 4 GPU nodes, 16 CPU nodes with balanced or large RAM pools, and 32 light weight CPU nodes with 32GB of memory each

As for the cluster's internal environment, the whole cluster uniformly runs on an enterprise-scale linux operating system, called RedHat Enterprise Linux--this is the defacto standard OS for not only a big majority of the supercomputers across the world, but also for industry giants like IBM, Google, Facebook, etc. RedHat linux is known for its stability, simplicity, and high performance. The default command line environment on our cluster is the BASH, or Bourne-again shell, for those of you who aren't familiar with the command line interface, installed on RHEL linux7 by default is a GUI interface called Gnome3 --> it's very straightforward to use and intuitive; you will not find it any drastically different than the Windows, MacOS  in terms of operations

Next, we also provide the optional, individualized computing units that are directly linked to the cluster's storage directories an software stacks. These needed to be purchased from us separately by request and are not included with default BioHPC memberships; the neat thing about having BioHPC Thin clients and workstations is that it gives you access to our cluster at all times, and sometimes when the cluster is busy and there are no free nodes to run your jobs, you may still get work done via your local thin clients and workstations

Okay let's look at the storage systems on Nucleus; at BioHPC, we are hosting a number of high-end, multi-million dollar parallel storage systems that yield a total capacity of 25 PB, so that's nearly 25000TBs of total storage space, give or take. We are serious about bringing big data to the UTSW research community, and we've deployed the best tools for it. Our storage systems are built by us and purchased from the biggest industry leaders like DDN and IBM. Our 3 main storage directories, /project, /work & /archive, anmd /home2, are each built on distinct physical storage arrays, which are all fault tolerant and can sustain the loss of multiple drives prior to data loss

And once again, let's look at what each user is allotted by default on storage space:

 - each user has 50GB of in their personal home folder under /home2, as well as 5TBs of individual storage space under /work/
 - then each lab or group is assigned 2 5TBs under /project and /archive, remember for these two folders, the capacity is shared among all of your lab 
 - moreover, we host two web-based storage servers, cloud.biohpc.swmed.edu is intended for data access and sharing external to the campus network, and lamella.biohpc.swmed.edu does the same but for internal use; each user has a reserve of 50 and 100GBs for these two web-based storage, respectively
 
```

- software & PI

```
Okay moving onto the subject of software tools, the BioHPC cluster holds a pandora's box of 800+ scientific applications and libraries, of course this includes some of the most commonly used tools like MATlab, Rstudio, Python-Ananconda scientific libraries, etc. We also keep different versions of each.

The software stack is installed in a modular fashion, where each user decides what software stack they make available to them, depending on their need; this is also to avoid the dependency and conflicts that our huge collection of softwares can create if they were simultaneously loaded to the user's environment--I will briefly demonstrate how this works soon

Note that we are open to install more software applications onto our cluster upon request, if you have specific apps that you wish to run on our cluster, just point us to where to download the source packages, or if it requires a license, please provide that for us. and we'll make sure it can be loaded as a software module on our environment

\

Now, on the subject of getting BioHPC memebership---our business model dictates that in order for users' to gain access, whether you are a PI or a graduate student work under a PI, your department has to have a prior agreement/affliation with our Bioinformatics department. The heads of your and my department directly negotiate over the numbers, and you or your PI will communicate with your chair over the specific terms over how much storage and compute resources you will acquire. Now if your department is not already affiliated with us, please send us an email, and we'll get our Director to start a discussion with your department chair. Currently we have all over campus 20 affliated departments and nearly 1000 registered users, scale.

```

- segway

```
So let's digress from all of this static information, I will do some demos on how to access our storage, webservices for you guys.

```

- portal

```

I will start with storage

storage lamella

network mount

  - storage recommend browsing, not editing, viewing


storage quota

training

guides

Web Services

Gitlab

Astrocyte

R

JobSubmission

  - absolute path; cmd to execute

  - time limit

  - job location; 


webGUI

  - stable network connection; high bandwidth and low latency; I am using 10Mbps

  - spec

  - MATlab 


SSH

  - run python

  - users, and count them

  - load a different version of python


I will stop the demo at this point and give our a few pointers before we conclude this training session.

``` 


- For those of you who will soon be granted access--this is a shared resource dedicated to the advancement of research and innovation for our institution. So please consider others and use it efficiently. Reserver only the amount and nodes you need and avoid hogging them for extended periods of time, there are others in line to perform their computations as well, set a realistic limit on your jobs--if it's too short, you can always email us for us to extend it. Delete the old and irrevelant files that you no longer need, as our storage space is fairly pricey and we want data processing to take place as opposed to files sitting out there catching dust, if possbile.

- Run some small scripts on login node---if you anticiapte them to run longer than a day, submit it as a job to the cluster

- And finally remember, we are here for you by offerring you additional resources and tools, so don't hesitatate to email our online help task @, try to be as specific as you can; and it will help us diagnose the issue a lot quicker.

- Before we leave, I just want to remind you once more to submit a register form on portal.biohpc.swmed.edu --> and we will review your form manually and determine your eligibiity. If you have already registered, the attendance of this training is required for us to grant you access, so shortly after this we will activate your acconts and email you guys a notice. Once again I'm really glad you all could make it this morning, and we look forward to working with you all! 



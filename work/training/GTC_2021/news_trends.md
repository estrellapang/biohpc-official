## 

### Organizations to Know

- CSCS/Los Alamos

- ALPs/Grace-based supercomputer

\
\

### Terms

- "Edge servers"

- what is a "DPU?"

\
\

### Projects/Products

- "Grace"

  - enhanced GPU-Memory bandwidth

- "Aerial A100"

  - 5G based edge datacenter/server

- "Morpheus" AI project

  - neural network to check security checks against key compromises

  - tied into the Aerial A100 5G

- Check out Nvidia's AI networks/projects

  - most of these are opensource, pre-trained

- "NGC Pre-Trained Models"

  - delivers from central cloud edge devices

  - has sample data and code

  - "Nvidia Tao" --> manufacturing

- "Nvidia Jarvis" --> emotional-based conversation AI

  - already used by T-mobile

- "Orin" --> mini computer for Vehicles and autopiloting

- "Nvidia Omniverse"

-  what is "Metropolis" Network


\
\



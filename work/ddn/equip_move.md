## DDN Meeting to Arrange Equipment Move

> contacts
>
> James Riddles
> Craig Vershon
> Morris Skupinksy

### Overview/Requirements

- This team has conducted relocation (within datahall before)

  - Craig primarily

\

- Reinstallation

  - graceful shutdown

  - disk remove & protect

  - rack/server based shipping

- Requirements

  - receiving end has to be ready

  - temporary data backup; if we have "landing side" available (dataflow - OS independent-requires upgrading to the latest Exascalerv5) 

  - namespace will be duplicated, IPADDR and all

  - Data Retention

  - Time

    - replication time?

    - 2-3 hrs/site - physical work

    - expect 2 days of downtime

- Challenges

  - BioHPC is at Exascaler v2 but DataFlow require Exascaler v5(18k)

  - would is be worth buying Dataflow vs buying an entire new firmware hardware that comes with Dataflow?

  - 2023 is end of official support for DDN SFA12K, but LW is attempting to extend support plan for current system

  - confirmed 2023 extension

\
\

- To-Do

  - submit ticket for upgrade plan

  - normal health-check contract

  - remote upgrade

- Future Plans

  - if new storage system purchased --> old system becomes backup for /archive and /home1 and service-based storage e.g. galaxy, thunder, etc.


\
\

### 12-10-2020: Migration Proposal

- Our environment

  - 200-300 labs

  - 2000-3000 individual user dirs

  - 2-3 months of hauling; quick cutover downtime

- Morris has experience moving 3PB of data to GPFS

#### Datamover

- one department at a time

- using data mover nodes (not their dedicated nodes)

  - 3-4 nodes, avoid bandwidth occupation

  - has GUI, and tweak the bandwidth/throughput

  - Data-Mover is compatible across multiple filesystems

\

- limitations

  - SFA OS; lustre version too old

\

- Rsync

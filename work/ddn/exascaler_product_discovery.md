## DDN Exascaler Storage Showcase

### 7-23-2020 Meeting

- Contacts:

  - Morris Skupinsky

  - James Riddles

```
### Hardware

- all modular controllers and servers can be swapped in and out of different boxes



## ES200NVX no JBODs

## ES400NVX

  - support NVMe slots & SSD

  - 4 VMs

## Hot Pools in Controller 

  - currently 18K controllers; corosync & pacemaker to do failover

    - also virtualized in boxes; uses custom proprietary KVM for SFA18k OS

  - SAS for disks

  - IB

  - OSS & MDS & MGS are virtualized 

    - 150 GB ram, 10-12 cores

    - OSS VMs are PCIe passthrough

    - HDR/EDR max 16 connections passthrough to each VM

    - has proprietary PCIe switch between boxes

    - latest cascade lake

  -
\
\

## Software 

- EXAScaler V5.1

  - requires 2.12.3 or later

  - Stratagem Data Management Engine

  - hybrid spinning disk (colder storage) & ssd storage

    - same filesystem

    - all I/O lands on flash

    - data that's no longer being operated gets replicated to spinning disk (innode & pointers in backend moves)

    - when flash gets full (hot pools); it removes the flash

    - can be manually and disabled or enabled via striping (?); in V5.2 visualization will
 
  - snapshots

     - file sync and cloud sync  ---> to other DDN appliances & cloud platform fs

  - local persistent cache

  - IML GUI / ESUI  --> GUI management not performance but single filesystems

\
\

- multi-rail

  - self-recovery?

\
\

- Nvidia GPUDirect 

  - AI-100 new GPU on market

  - increase BW between Storage & GPU

  - upcoming feature


\
\

- data replication & syncing

```

- PACKAGES & QUOTES

```
- can expand JBODs 

\
\
\

### DDN Show-Case: Feb. 25th, 2021

> Contacts:
> Morris Skupinksy (systems enginner, DDN) 
> Holly Newman (Mark III)


#### Questions

- In the past, for example, we have had 2 mds/mgs servers, but they are only in active-passive mode

  - and we've had outtages where the backup mds is supposed to mount the metadata target, but some hiccups occur and in the end we still have to manually re-mount the metadata target---so do you guys have more of an active-active implementation of the metadata servers?


- metadata bottle-neck, what we've noticed that during production, when we have many users doing reading and writing, simultaneously, how does meta-data array look like, and and how is it data linked up to the rest of the oss nodes? 


- data migration; how well is it supported? IBM gpfs filesystem, 5 Petabytes


\

#### Presentation/Features

- "Xscale" and "enterprise" divisions (one for HPC, and the other for NAS, more small scale like VMWare)

\

#### Small Flash - "IntelliFlash"

- NAS, competitor to Isilon

- ndmp backups/snapshots

- nfs, samba - ZFS running in the backend (Software RAID)

  - will support parallel samba, nfs in future

- uses NVMe Flash caching: SAS-Flash hybrid

  - caches writes until it's full stripe, then writes to SAS

- one controller box--two servers, each dual-socket, 20 core--> they access the storage blocks in a HA-fashion, but the software storage server itself(nfs) is standalone...

  - supports "tens of thousands of clients" 

  - 4x10 Gbit Ethernet ports

- 18Gbit/sec performance benchmark

- supports iSCI and fibrechannel for block storage

\
\

#### EXAScaler

- PCIe4 Storage Nodes (SFA200NVX2E)

  - 2 u

  - 4x Ice Lake CPUs

  - 32x DDR4 Memory channels

  - 8x HDR ports

  - chosen Intel versus AMD because of AMD with L2 Cache causes Numa issues with current code; they'd have to re-write code to use L3 cache (vendor says AMD is not good for heavy IO, parity checks, raid calculations.., although it is good for compute)

  - designs around stability

- controller firmware is now containerized --> easy update

- cloud-based log upload, uses algorithm to auto-analyze potential bugs --> will open up to user in the future

  - collects telemtry data only, not actual data

  - we may not use this, UTSW cannot forward data to external sites

\

- upcoming self-encrypting NVMe and SAS drives!

\

- DDN-exclusive features (not community lustre)

- emf (GUI, replacement for iml) --> will run as a container on SFA controller nodes(easier upgrades)

- metadata-balancing (available on lustre v2.14)

  - removes need to manually restrip mdts

- "HotPools"

  - migration from Flash to SAS depending on usage frequency

- "HotNodes"
  
  - uses clients' NVMes as a local OST, as INCLUDED in the overall filesystem namespace (accssible by other clients, only that it's locally, the FASTEST)

  - works with NVIDIA DGX 

  - 76 Gbit/sec datatransfer rate into a single container; can pass RDMA to GPU memory(!)

  - difference between hot pools is that they actually keep the data, not exactly cached then deleted like hot pools

- Cloud-sync; uses rsnapshots to easily deploy to remote locations

  - has to be full-filesystem snapshots (ONLY works with FLASH, right now)

  - use stratagem and parallelized datamover (Available EXASCaler v6)

  - still in infancy

  - caching in testing

  - sync data in cloud, then sync it back

- DDN "Red" 

  - 3 different I/O engines --> upcoming

  - log structured filesystem

\

- Lustre Components

  - per controller node: 4x Sockets (ES400NVX), 4 VMs, each one is a OSS/MDS(!) -- has dedicated PCIe lanes,cpu, and memory

    - 20x cores, 150GB/VM

  - SFA controllers have ALWAYS BEEN software raid---it's installed on baremetal, and the raid engine runs on a different layer than the OS/lustre software storage layer; SFA controllers have dedicated resources like OSSs/VMs

  - 8 flash osses, 2 flash osses to each vm

  - 1x45 pool to each vm oss vm 

  - corosync, pacemaker, each mds, mounts a mdt, and its failover-peer is the local neighbor vm

    - can also cross-node vm failover

    - still, only one server mounts one set of metatargets at once

  - Nvidia has 56 servers

  - dedicated metada server is also available (they will easily hit the metadata limit)

    - more cores, doesn't mean better performance, NUMA latencies

    - e.g. of 2x ES200NVX dedicated metadata that hit performance issue, drop cores actually increased performance

\

- Openshift/Lustre

  - developing CSI drivers to make compatiple with Openshift/Kubernetes


\
\

### 05-06-2021 Meeting

- lustre 2.14 for newest DDN environments

- standard 4 MDSs

  - embedded (400 thousands IOPs)

  - stand alone, more, 400/box

  - automated mdts capacities

- HDR2000 8 max ports/metadata server

  - 150 GM RAM/VM --> dedicated, separated by NUMA

- GPU-direct IO

  - different libraries are used

  - Nvidia for beta testing

- Hot nodes?

  - Read-only cache, NVMe on local server

  - PFL/uses lfs mirror to the actual filesystem

- Hot Pools

  - benefits only from READING, not WRITING

  - Mostly for Nvidia DGX servers

- DDN insight (old IML)

  - few months

- DDN Lustre CSI driver documentation

- Asked for OnDemand training, reluctant

- Asked for Metadata server configurations visual

  - 4 sockets per node--> 4 metadata VM servers -OR-

    - cross-mounting targets, failover is ONLY within the node (dangerous, remember lost one side of power, and the DDN SFA12K controllers they failed over to the peer for paths to enclosures)

  - multiple nodes, each node is a single metadata server (vendor says it's no IO benefit, but I'd like to take a closer look)


\
\

### 05-28-2021

#### Metadata connectivity

- What kind of disk array

  -

- What kind of connection from the DISK array to the MDS servers

  - 

- How are you facilitating the active-active mounting of MDT targets

  - even though the disks are sitting in the same array, how are the LUNs cross-mounted to the HBA's on the metadata nodes?

- Management target (MGT) --> both clients and OSTs need to retrieve info from here

  - I'm a bit vague on how the management targets are arranged, or whether there's any fault-tolerance measures being applied to them

  - or are they grouped together with the Metadata targets in the same disk array?

- 4x sockets each MDS node

  - if we do bare metal, how will we be mounting the metadata disk array?

  - much more more bandwidth?

  - cross-box failove vs. intra-node, VM failover---just to confirm, there's no way for the VMs to failover to the VMs on the another box right? this would apply both for the OSSs' and the Metadata VMs on embedded systems?

  - what's the hypervisor/KVM?

    - modified KVM, with Virt-IO, so resources are pinned, SFAOS are pinned cores and memories

    - each VM has 10 cores and 150GB of RAM, NUMA nodes dedicated, total 40 cores/600GB per box (ONLY FOR DUAL-socket configs!---single socket, single VM config will still have 10 cores), it will come with single socket box

\
\

#### Education

- Intermediate to advanced trouble-shooting and debugging

  - specific client cranking up inundating the metadata bandwidth, how do we track it down?

  - OSTs getting full, how do we identify the large files, let's say files larger than 1GB spread across all OSTs, and mark them for offl-loading or deletion?

  - manual intervention when auto fail over fails

\
\

#### Notes

- (where as GPFS the box handles all filesystem and backend RAID operations)

  - GPFS does cross-mounting over client network, cache mirroring uses client network

  - GPFS does baremetal, with single hosts handling both filesystem and RAID layer

  - Lustre does PCIe, dedicated cache mirroing bandwidth

  - 7600-lane PCIe switch handling all SAS connections --> then to the boxes w/h controllers (internal) via PCIe; 

  - QUESTION: IS IT REALLY A SWITCH? Daisy-chaining Intel blades between two peer-servers (ask Morris to show actual product)

- each box is a controller (dual-socket), and 8 lanes of PCIe switch connection to peer
 
  - SFAOS (a Debian-derivative kernel)  --> controller handles the baremetal RAID operations (so is Isolon), software RAID; VIRT-IO
 
  - VMs exclusively handle the filesystem, both the metadata and oss boxes have the SFAOSs to handle

  - only one controller is active to access/talk to OSS at a given time

  - disks are dual-ported?


- VMs can failover locally via corosync, but they can also failover to the 2nd controller?

  - controller will kill the first one, then take over other, still faciliated by corosync


- max-MDS --> 4 / box, max bandwidth

  - 400000 metadata IOPs total w/h 4 in a box

  - DNE auto mnanage meta allocation 

  - SFA v6 uses Lustre v2.14

- compact config

  - mds and oss VMs in a single boxx2--> lower performance than dedicated metadata server 

  - A3I, compact but all NVMe OSTs/MDTs

  - each appliance hosts a MDS and a OSS, with a peer appliance

    - two MDTs/MGS VM has better performanc than one

  - max-80 appliances done in a single deployment


\
\

- MDT failures

  - if MDT00 fails, entire filesystem goes down

  - if other targets fail, then the filesystem will be partially available


\
\
\

### DDN Technical Brief Cont'd

#### HA MDS

- In the possible deployment where we have multiple metadata servers spread across multiple boxes, let's say 4 appliances

  - where each appliance is accessing an unique set of targets

  - on the filesystem level, will we have more than 1 metadata server active at any given time?(if not, how does failover work??)

  - how is the real storage devices arranged? Do we have a dedicated disk array?

  - and just to confirm (if storage array) each MDS will be accessing the metadata target block devices through the embedded PCIe/SAS module? I'm just wondering through which interface are we connecting to the metadata blocks

  - 4 appliances, where you have a set of two failover 

\

#### Answers

- each unique MDT has active dedicated MDS (DNE)

  - 2xcontroller/appliance

  - VM failover/controller

  - controller failover/appiance

  - per appliance/flash array

  - only connection is the IB to IB switch

\

- MDT storage array

 - inside each appliance

 - mixed-media --> for OSTs

-  

\
\

#### PCIe Lanes / PCIe Switch

- PCIev4 --> does DDN have an estimated timeframe on when this feature is included in in its appliances?

- On this PCIe bridge concept, where you mentioned that between failover appliances, we have a PCIe bridge or link established. I am wondering how that's actually done in terms of hardware. I anticipate that it'll require a cable connection

\

#### Answers

\
\

#### Multi-Rail

- 200-400 NVs

  - 4-8 ports, 2 ports/VM

- 18k

  - 16 ports, 4 ports/VM

- EACH VM controls 1 CARD

  - clients can use two nids/server

  - no LACP like with bonding via ethernet

\

#### Answers

\

- everything passes through VirIO

- On baremetal, we have configure Lustre servers to go through mutliple interfaces over a single LNET network, for example, you may have ib0,ib1,ib2, etc., and ib0 is assigned primary, and the others are set as peers

  - can we have this primary-peer relationship setup across the VM servers? So per VM, it has a primary IB NIC, but it's also configured to talk through the other IB ports if needed.

- What we want is for each OSS or MGS to utilize as many routes available to the client network as possible, but with the virtualization and passing through each VM to a dedicated interface, how could that possible?

\

#### Answers

\
\
\

### Training

- Primarily interested in having some workshops, perferrably intermediate to advanced level

\

#### Answers

\

#### Info

- GPFS: GNR vs ZFS

- Exascaler 5 limited release vs GA release

- Grace-Faster Texas A&M

- Long distance clients

  - LNET router in-between

  - persistent-client side caching, local storage (NVMe) to cache read, and write back to filesytem

  - file-level replication, FLR caching (different appliance on remote) --> metadata server sits on remote

  - filesync-strategem, sync across WAN (similar to AFM)


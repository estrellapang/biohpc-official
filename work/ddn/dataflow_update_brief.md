## Dataflow & DDN Exascaler Showccase

### Dataflow

- fastscan engine

  - works with snapshots & create changelists based on it

  - works with DDN Stratagem

  - could break large files into small chunks and move in parallel

     - more data movers + more throughput

     - no license (included in capacity license) for data movers, just pay for hardware (needs pay support; includes perpetual license)

     - can have movers mount on both sides: e.g. movers mounting cache - movers mounting home---movers mount both storage like AFM works as well

  - supports lustre, gpfs, s3

    - moving from lustre to lustre can also be done using Filesync with Exascaler5 engine

    - will not work with old DDN lustre at BioHPC, need Dataflow

  - user & admin GUIs

  - metadata database; categorization labeling like discover; similar to SQL

  - **catelog servers**  

- dataflow comes with 3 months of service during migration


 

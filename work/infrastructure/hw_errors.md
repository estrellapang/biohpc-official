## Hardware Error Detection & Fixes for Cluster Nodes

### Force Shutdowns

```
echo 1 > /proc/sys/kernel/sysrq
echo b > /proc/sysrq-trigger
```

- [reference 1](https://www.recitalsoftware.com/blogs/17-howto-force-a-immediate-reboot-of-a-remote-linux-machine)

- [reference 2](https://matoski.com/article/emergency-reboot-shutdown-linux-sysrq/)

\

### disk erors

- **Gotcha's**

  - Some DISKs are NOT SMART-capable

  - SAS drives return different outputs than ATA-based drives

- Established Work Flow

```
# pull raw node list from SLURM
--------------------------------------------------
sinfo --Node | awk '{print $1}' | grep -vi 'nodelist' >> nodelist_01.txt
--------------------------------------------------

# narrow down to nodes that are responsive (via SSH in return hostname)
--------------------------------------------------
sudo xargs -a nodelist01.txt -I"NODE" -n1 sh -c "sudo ssh -o ConnectTimeout=5 NODE hostname -s || true" >> nodelist_02.txt

cat nodelist02.txt | grep -v 'ssh\|timed out' >> nodelist03.txt

cat nodelist03.txt | sort -u >> nodelistFinal.txt
--------------------------------------------------

# launch smartctl scan
--------------------------------------------------
sudo xargs -a nodelistFinal.txt -I"NODE" -n1 sh -c "echo NODE; sudo ssh -o ConnectTimeout=7 NODE 'smartctl -d auto -a /dev/sda' | grep 'Reallocated_Sector_Ct\|Current_Pending_Sector\|Non-medium error count'" >> diskHealthReport_06282021_detailed.log
--------------------------------------------------

# filter out nodes with '0' values
--------------------------------------------------
grep -vw '0' clusterOps/diskHealthReport_06282021_detailed.log | less
--------------------------------------------------

### CRITERIA for SELECTION ###
--------------------------------------------------
Non-medium error count > 100  -OR-
Reallocated_Sector_Ct > 10   -OR-
Current_Pending_Sector > 1
FAILING_NOW
--------------------------------------------------

\
\

# QUICK SCAN: only detect CRITICAL FAILURES
--------------------------------------------------
sudo xargs -a clusterOps/nodelistFinal.txt -I"NODE" -n1 sh -c "echo NODE; sudo ssh -o ConnectTimeout=7 NODE 'smartctl -d auto -H /dev/sda' | grep 'Health Status\|test result'" | tee
 clusterOps/diskHealthReport_06282021.log

# e.g. of detected failure
------------------------
NucleusA201
SMART overall-health self-assessment test result: FAILED!
------------------------
--------------------------------------------------
```


- [smartctl cmd](https://www.tecmint.com/check-linux-hard-disk-bad-sectors-bad-blocks/)

- [smartctl disk failure prediction 1](https://www.cyberciti.biz/tips/linux-find-out-if-harddisk-failing.html)

- [smartctl disk failure prediction 2](https://www.suse.com/support/kb/doc/?id=000017146)

- [smartctl disk failure prediction 3](https://www.reddit.com/r/techsupport/comments/f8b8uc/hard_drive_smart_nonmedium_error_count_what_is_it/)

- [smartctl disk failure prediction - GOOD List of STATS](https://www.computerworld.com/article/2846009/the-5-smart-stats-that-actually-predict-hard-drive-failure.html)

- [smartctl disk failure prediction 5](https://www.truenas.com/community/threads/a-lot-of-non-medium-errors.60350/)

- [smartctl disk failure prediction 6](https://serverdiary.com/linux/how-to-check-disk-health-using-smartmontools-on-linux/)

- [smartctl disk failure prediction 7](https://unix.stackexchange.com/questions/303486/non-medium-error-in-smartctl-output)

\

#### Single SSD Recovery Methods

- [good ref 1](https://www.cleverfiles.com/howto/ssd-data-recovery.html)

\

### BioHPC Cluster Node Check

- nodes killed by jobs

  - `sudo grep -i 'failure\|not responding' /cm/log/slurmctld` 

- nodes kernel panics 

  - `sudo grep -i 'panic' /cm/log/messages`

- lustre errors

  - `sudo grep LBUG /cm/log/messages`

  - `sudo grep -i 'lustre error' /cm/log/messages`

\
\

### Use `mcelog` to detect

- **resources**

  - [overview 1](https://www.cyberciti.biz/tips/linux-server-predicting-hardware-failure.html)

\
\

### Check Boot and dmesg logs

## CMDs & Operations on DDN Lustre

### Resources

#### Manuals

- [lustre wiki home](http://wiki.lustre.org/Main_Page)

- [lustre gen architecture](http://wiki.lustre.org/images/6/64/LustreArchitecture-v4.pdf)

- [whamcloud community home](https://wiki.whamcloud.com/display/PUB/Whamcloud+Community+Portal)

- [whamcloud issue tracker](https://jira.whamcloud.com/projects/LU/issues/LU-11989?filter=allopenissues)

- [lustre release 2.x operation manual](http://doc.lustre.org/lustre_manual.xhtml#idm140539601488368)

- [lustre_manual_2.x_markdown_manual!](https://github.com/DDNStorage/lustre_manual_markdown)

- [lustre_maintenance_bp_guide](https://lustre.ornl.gov/ecosystem/documents/tutorials/Hall-BP-12Step.pdf)

- [whamCloudon IML](https://whamcloud.github.io/Online-Help/docs/Introduction_1_0.html)


#### Architecture

- [striping and server layout](https://www.nics.tennessee.edu/computing-resources/file-systems/io-lustre-tips)

- [IBM Multi-Rail](https://www.spectrumscaleug.org/wp-content/uploads/2020/03/SSSD20DE-Spectrum-Scale-multi-socket-support-and-related-network-flows.pdf)

- [Multi-Rail with LNet](https://wiki.lustre.org/Multi-Rail_LNet)

- [old guide on how to enable multi-rail](https://edc.intel.com/content/www/kr/ko/design/products-and-solutions/networking-and-io/fabric-products/omni-path/intel-omni-path-performance-tuning-user-guide/lustre-multi-rail-support-with-intel-opa/)


\
\

### Monitoring

#### Identify Nodes Incurring Metadata Load

```
# from mds00/mds01

watch sudo bash /scratch/scripts/mdt_busy_nodes.sh

```

\
\

### Essentials

```
# Check Controller Version:

cat /etc/es_install_version

cat /etc/ddn/lustre-2.5_definition.xml

# Check Lustre Version (requires lustre kernel module to be loaded)

cat /proc/fs/lustre/version

lctl get_param version

# Check Deployment Settings:

cat /etc/ddn/exascaler.conf

  # here you will see underlying filesystem e.g. ext4

# see connection paths to controllers

multipath -l
```

### Troubleshooting CMDs

```
# location of `es` cmds
--------------------------------------------
/opt/ddn/es/tools
--------------------------------------------

# there are no man pages? inquire --help for es_<cmds>
--------------------------------------------
es_mount --help
--------------------------------------------

# check recovery status

  # very important while failover-mounting
--------------------------------------------
/opt/ddn/es/tools/lustre_recovery_status.sh
--------------------------------------------
```

### Failover CMDs

```
# see which OSTs are assigned to oss node
--------------------------------------------
ex_mount --status
--------------------------------------------

# see which OSTs are currently mounted
--------------------------------------------
df | grep -i project_ost
--------------------------------------------

# perform failover mount
--------------------------------------------
es_mount --failover --dry-run

  # first perform a --dry-run

# perform actual failover mount

es_mount --failover

# check status

lustre_recovery_status.sh
--------------------------------------------


### e.g.

oss01 failover mounted OSTs originally mounted by oss0:
--------------------------------------------------------------------------------------
oss01 ~]# es_mount --status
+-----------------+---------+
|  Resource name  |  Status |
+-----------------+---------+
| ost0010_project | Mounted |
| ost0011_project | Mounted |
| ost0012_project | Mounted |
| ost0013_project | Mounted |
| ost0014_project | Mounted |
| ost0015_project | Mounted |
| ost0016_project | Mounted |
| ost0017_project | Mounted |
| ost0018_project | Mounted |
| ost0019_project | Mounted |
| ost0050_project | Mounted |
| ost0051_project | Mounted |
| ost0052_project | Mounted |
| ost0053_project | Mounted |
| ost0054_project | Mounted |
| ost0055_project | Mounted |
| ost0056_project | Mounted |
| ost0057_project | Mounted |
| ost0058_project | Mounted |
| ost0059_project | Mounted |
+-----------------+---------+

   # what it's supposed to mount(20 OSTs)

df -Th | grep -i project_ost
/dev/mapper/project_ost0056
/dev/mapper/project_ost0050
/dev/mapper/project_ost0016
/dev/mapper/project_ost0019
/dev/mapper/project_ost0052
/dev/mapper/project_ost0055
/dev/mapper/project_ost0014
/dev/mapper/project_ost0010
/dev/mapper/project_ost0057
/dev/mapper/project_ost0053
/dev/mapper/project_ost0054
/dev/mapper/project_ost0011
/dev/mapper/project_ost0017
/dev/mapper/project_ost0013
/dev/mapper/project_ost0051
/dev/mapper/project_ost0058
/dev/mapper/project_ost0059
/dev/mapper/project_ost0012
/dev/mapper/project_ost0018
/dev/mapper/project_ost0015
/dev/mapper/project_ost0042
/dev/mapper/project_ost0007
/dev/mapper/project_ost0005
/dev/mapper/project_ost0001
/dev/mapper/project_ost0045
/dev/mapper/project_ost0006
/dev/mapper/project_ost0002
/dev/mapper/project_ost0049
/dev/mapper/project_ost0003
/dev/mapper/project_ost0040
/dev/mapper/project_ost0009
/dev/mapper/project_ost0000
/dev/mapper/project_ost0008
/dev/mapper/project_ost0047
/dev/mapper/project_ost0043
/dev/mapper/project_ost0048
/dev/mapper/project_ost0004
/dev/mapper/project_ost0041
/dev/mapper/project_ost0046

  # what it actually mounts(40 OSTs)

oss00 ~]$ es_mount --status
+-----------------+-------------+
|  Resource name  |    Status   |
+-----------------+-------------+
| ost0000_project | Not mounted |
| ost0001_project | Not mounted |
| ost0002_project | Not mounted |
| ost0003_project | Not mounted |
| ost0004_project | Not mounted |
| ost0005_project | Not mounted |
| ost0006_project | Not mounted |
| ost0007_project | Not mounted |
| ost0008_project | Not mounted |
| ost0009_project | Not mounted |
| ost0040_project | Not mounted |
| ost0041_project | Not mounted |
| ost0042_project | Not mounted |
| ost0043_project | Not mounted |
| ost0044_project | Not mounted |
| ost0045_project | Not mounted |
| ost0046_project | Not mounted |
| ost0047_project | Not mounted |
| ost0048_project | Not mounted |
| ost0049_project | Not mounted |
+-----------------+-------------+

  # what oss00 is supposed to mount--> should match with oss01's current mounts above
--------------------------------------------------------------------------------------
```

\
\
\

### Fail-back Mounts

```
# umount from failed over oss server
--------------------------------------------
es_mount --failover --umount --dry-run

  ## check if the umounted targets match that of fail-back server's mounts (es_mount --status)
--------------------------------------------

# fail back mount on another oss server
--------------------------------------------
  # restart multipathd & flush it

service multipathd stop

multipath -F 

service multipathd start

  # begin mounting back OSTs

es_mount --dry-run

es_mount

lustre_recovery_status.sh
--------------------------------------------

```

\
\
\

### Safe Shutdown Procedure

- [lustre wiki notes](http://wiki.lustre.org/Starting_and_Stopping_Lustre_Services)

- drop file cache? lustre_rmmod? 


\
\
\

### Check System Attributes

- check whether qouta is enabled on OSTs

```
cat /proc/fs/lustre/osd-ldiskfs/project-OST00##/quota_slave/enabled
```

\
\
\

### Dealing w/h Full OSTs !!!

```
# list capacity of OSTs

lfs df -h

### USEFUL: scan for large files WITHIN SPECIFIC OSTS
-------------------------------------------------------------------------------------------------
sudo lfs find /project --ost project-OST003f_UUID -t f -maxdepth <1/2/3/4/5/6/..> -size +1<M/G/T>

  # generally start getting meaningful from -maxdepth 4-6

  # use OST UIDs to for `--ost` flag; can attain from `lfs df -h`

# POST-PROCESSING: sort files from small to largest in size, given a output file 

cat <raw_output>.txt | grep -v "'" | sudo xargs -n1 -I% du -sh % | sort -V

  # exclude "'" because xargs cannot process it, be sure to isolate file names with single quote character
-------------------------------------------------------------------------------------------------

# output file creation limit on a OST

lctl get_param osp.project-OST003f*.max_create_count

  # the OST numbers are hexadecimal, use ost uids from `lfs df -h`
  
  # e.g. output

osp.project-OST003f-osc-MDT0000.max_create_count=20000

# CAUTION: changing `max_create_count` is ONLY recommended for lustre releases 2.9.* & later!
```

- **relevant resources**

- {stop file creation on a OST](https://wiki.lustre.org/Handling_Full_OSTs)


\
\

### Filesystem Scanning via `lfs`

#### Find large files for specific Group

```
lfs find /project/BICF/shared/ -t f -g BICF -size +100M
```

#### Find files not belonging to its designated location

```
nohup ls /project | grep -v BICF | sudo xargs -P7 -n1 -I% lfs find /project/% -t f -g BICF >> BICF_dislocated_files.txt
```

\
\
\

### File Striping across OSTs

#### get stipe info on directories

`lfs getstripe -d <dir>`

#### get stripe info on directories and its files

`lfs getstripe -r <dir>`

#### get stripe info on files

`lfs getstripe -y <file>   # in YAML`

#### set stripe on directories

lfs setstripe -c <stripe count> <dir>

#### how to stripe existing files

```
# Set striping on the directory
lfs setstripe -c 4 /project/department/myuser/bigfiles
cd /project/department/myuser/bigfiles
# Copy the existing file to create a striped version
cp myfile myfile.striped
# Replace the original file with its striped copy
mv myfile.striped myfile
```


- [reference 1](https://portal.biohpc.swmed.edu/content/guides/storage-cheat-cheet/)

- [reference 2](https://www.nics.utk.edu/computing-resources/file-systems/lustre-striping-guide)

- [reference 3](https://www.nics.tennessee.edu/faq/general-how-do-i-change-striping-lustre)

- [reference 4](https://www.navydsrc.hpc.mil/docs/lustreUserGuide.html)

- [reference 5](https://www.nas.nasa.gov/hecc/support/kb/Lustre-Basics_224.html)

- [reference 6](https://www.nas.nasa.gov/hecc/support/kb/lustre-best-practices_226.html)

\
\
\

### To-Do's

- clean-up rootsquash list, add 003, remove old, non-existing IB hosts

- start Graphana

- identify IPMI admin root pass on all servers 

- make /scratch a script location --> add I/O check scripts to every node, and to $PATH

  - Git, `lustre_utils`

    - pull lustre_report

    - list top Groups

    - list top Users

    - quota breach

    - check client mount status

    - unmount/remount across all clients

    - check total # of files, inode usage

\

  - `lustre_admin_cmds`

    - sync-file

    - mount_status

    - fail_over

    - lfs find: files larger than X MBytes; files/directories belonging to group/user

    - check OST individual usage, lfs find large individual files inside each OST

\

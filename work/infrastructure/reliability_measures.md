## Methods to Monitor and Ensure Security

### script all login sessions

```
# add this to `/etc/profile`
------------------------------------------------------------------------------
script -qf --timing=$HOME/$(date +%F_%H:%M:%S).$(hostname -s).$USER.timing.log $HOME/$(date +%F_%H:%M:%S).$(hostname -s).$USER.session.log

  # `-q` --> quiet, no output
------------------------------------------------------------------------------

# replay via the following command
------------------------------------------------------------------------------
scriptreplay -d 3 2021-06-25_17\:16\:09.cloud-test.estrella.timing.log 2021-06-25_17\:16\:09.cloud-test.estrella.session.log

  # `-d` --> playback speedup multiplier
------------------------------------------------------------------------------

# prevent big script files from idle sessions, enable auto-logout
------------------------------------------------------------------------------
##NOTE##
# this needs to be implemented at 2 places, `/etc/profile` for login shell, and
# as a script under `/etc/profile.d/auto_logout.sh` for all interactive shells, 
# i.e. when an admin user switch to root user

# for `/etc/profile`
_________________________
TMOUT=198   # 3.3 minutes
_________________________

# for `/etc/profile.d/auto_logout.sh`
_________________________
export TMOUT=198
_________________________
------------------------------------------------------------------------------

# add crontab under root to clean script recordings older than a month
------------------------------------------------------------------------------
# clean old session script files once a week on Sundays at 11:11 A.M.
11 11 * * 0 /bin/find /var/log/.recordings/ -type f -mtime +28 -exec rm -f {} \;
------------------------------------------------------------------------------
```

\

- [reference 1](https://linoxide.com/script-command-recorder/)
- [reference 2](https://unix.stackexchange.com/questions/25639/how-to-automatically-record-all-your-terminal-sessions-with-script-utility)
- [reference 3](https://www.redhat.com/sysadmin/linux-script-command)
- [session auto logout and SSH autolog out](https://linuxhandbook.com/auto-logout-linux/)
- [start-up files where environment variables are located generally](https://unix.stackexchange.com/questions/813/how-to-determine-where-an-environment-variable-came-from)

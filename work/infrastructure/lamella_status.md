## Lamella Debug from Summer 2019 to Spring 2020 

### `lamella02.biohpc.swmed.edu` 

- LV `centos-root` has reached > 73% usage 

```
[live][root@lamella02 smbd]# df -Th
Filesystem                                     Type      Size  Used Avail Use% Mounted on
/dev/mapper/centos-root                        xfs        50G   37G   14G  73% /

```

- Cause: core dumps in `/var/log/samba/cores/smbd` 

```

# as Daniel noted, the system has been accumulating core dumps since Feb 2019

# space utilzation by this directory 

[live][root@lamella02 smbd]# du -sh
25G	.

  # July 30th, 2019 update: size has grown 1G in a week! 

[live][root@lamella02 ~]# du -sh /var/log/samba/cores/smbd
26G	/var/log/samba/cores/smbd


# ~50 core.* files are generated each day; each file measures amounts to ~3.5 MB

[live][root@lamella02 smbd]# ls -lah core.59518
-rw------- 1 root root 3.5M Jul 25 09:41 core.59518

[live][root@lamella02 smbd]# ls | wc -l
5749

``` 

- Solution: temporarily begin deleting old core.* files

  - Long-term: figure out WHY samba/ctdb is actively creating core dumps




### August 21st, 2019: LAMELLA02--Cron job to Remove Core Dumps 

- SSH: `ssh biohpcadmin@198.215.54.119`

- **testing** crontjob to remove `core.*` dumps older than 15 days 

```
## test find files modified older than 180 days

  # first list timestamps on the located files

[live][root@lamella02 smbd]# find . -mtime +180 -exec ls -lt {} \;

-rw------- 1 root root 3969024 Feb 12  2019 ./core.132844
-rw------- 1 root root 3993600 Feb 12  2019 ./core.141793
-rw------- 1 root root 3969024 Feb 12  2019 ./core.152626
-rw------- 1 root root 3969024 Feb 12  2019 ./core.169440
-rw------- 1 root root 21020672 Feb 12  2019 ./core.227917
-rw------- 1 root root 3969024 Feb 12  2019 ./core.211842
-rw------- 1 root root 3969024 Feb 12  2019 ./core.217480
-rw------- 1 root root 3993600 Feb 12  2019 ./core.220241
-rw------- 1 root root 3969024 Feb 12  2019 ./core.221636
-rw------- 1 root root 3969024 Feb 12  2019 ./core.203042
-rw------- 1 root root 3969024 Feb 12  2019 ./core.11603
-rw------- 1 root root 4100096 Feb 12  2019 ./core.11719
-rw------- 1 root root 3969024 Feb 12  2019 ./core.106822
-rw------- 1 root root 3969024 Feb 12  2019 ./core.78009
-rw------- 1 root root 3969024 Feb 12  2019 ./core.160765
-rw------- 1 root root 3969024 Feb 12  2019 ./core.189065
-rw------- 1 root root 3969024 Feb 12  2019 ./core.185831
-rw------- 1 root root 3969024 Feb 12  2019 ./core.10891
-rw------- 1 root root 3969024 Feb 12  2019 ./core.26358
-rw------- 1 root root 3969024 Feb 12  2019 ./core.218913
-rw------- 1 root root 3969024 Feb 12  2019 ./core.41289
-rw------- 1 root root 3969024 Feb 12  2019 ./core.45673
-rw------- 1 root root 3969024 Feb 12  2019 ./core.49714
-rw------- 1 root root 3969024 Feb 12  2019 ./core.53818
-rw------- 1 root root 3969024 Feb 12  2019 ./core.63723
-rw------- 1 root root 37953536 Feb 12  2019 ./core.209721
-rw------- 1 root root 3969024 Feb 12  2019 ./core.69034
-rw------- 1 root root 3969024 Feb 12  2019 ./core.64007
-rw------- 1 root root 3969024 Feb 12  2019 ./core.81580
-rw------- 1 root root 3969024 Feb 12  2019 ./core.98007
-rw------- 1 root root 46383104 Feb 12  2019 ./core.113488
-rw------- 1 root root 3969024 Feb 12  2019 ./core.132650
-rw------- 1 root root 3969024 Feb 12  2019 ./core.141750
-rw------- 1 root root 38080512 Feb 12  2019 ./core.137029
-rw------- 1 root root 3993600 Feb 12  2019 ./core.105730
-rw------- 1 root root 3969024 Feb 12  2019 ./core.166522
-rw------- 1 root root 3969024 Feb 12  2019 ./core.175707
-rw------- 1 root root 3969024 Feb 12  2019 ./core.181536
-rw------- 1 root root 3969024 Feb 12  2019 ./core.57161
-rw------- 1 root root 3969024 Feb 12  2019 ./core.206657
-rw------- 1 root root 3969024 Feb 12  2019 ./core.29980
-rw------- 1 root root 3969024 Feb 13  2019 ./core.38582
-rw------- 1 root root 3969024 Feb 13  2019 ./core.49456
-rw------- 1 root root 3969024 Feb 13  2019 ./core.87750
-rw------- 1 root root 3969024 Feb 13  2019 ./core.107882
-rw------- 1 root root 39460864 Feb 13  2019 ./core.226110
-rw------- 1 root root 3993600 Feb 13  2019 ./core.91890
-rw------- 1 root root 3764224 Feb 13  2019 ./core.90103
-rw------- 1 root root 3977216 Feb 13  2019 ./core.113342
-rw------- 1 root root 39456768 Feb 13  2019 ./core.115426
-rw------- 1 root root 3977216 Feb 13  2019 ./core.128387
-rw------- 1 root root 3977216 Feb 13  2019 ./core.129634
-rw------- 1 root root 21012480 Feb 13  2019 ./core.134211
-rw------- 1 root root 4001792 Feb 13  2019 ./core.127006
-rw------- 1 root root 4001792 Feb 13  2019 ./core.143340
-rw------- 1 root root 4358144 Feb 13  2019 ./core.161670
-rw------- 1 root root 38526976 Feb 13  2019 ./core.162406
-rw------- 1 root root 3969024 Feb 13  2019 ./core.204485
-rw------- 1 root root 38494208 Feb 13  2019 ./core.217049
-rw------- 1 root root 3969024 Feb 13  2019 ./core.222948
-rw------- 1 root root 3969024 Feb 13  2019 ./core.18981
-rw------- 1 root root 3969024 Feb 13  2019 ./core.26962
-rw------- 1 root root 40734720 Feb 13  2019 ./core.28040
-rw------- 1 root root 3969024 Feb 13  2019 ./core.51599
-rw------- 1 root root 22110208 Feb 13  2019 ./core.155936
-rw------- 1 root root 3969024 Feb 13  2019 ./core.44946
-rw------- 1 root root 3969024 Feb 13  2019 ./core.50988
-rw------- 1 root root 3969024 Feb 13  2019 ./core.70883
-rw------- 1 root root 49819648 Feb 13  2019 ./core.74377
-rw------- 1 root root 3969024 Feb 13  2019 ./core.104310
-rw------- 1 root root 3969024 Feb 13  2019 ./core.113776
-rw------- 1 root root 3969024 Feb 13  2019 ./core.129692
-rw------- 1 root root 21016576 Feb 13  2019 ./core.143353
-rw------- 1 root root 3969024 Feb 13  2019 ./core.149958
-rw------- 1 root root 3969024 Feb 13  2019 ./core.169829
-rw------- 1 root root 48902144 Feb 13  2019 ./core.135919
-rw------- 1 root root 3969024 Feb 13  2019 ./core.199546
-rw------- 1 root root 3969024 Feb 13  2019 ./core.208007
-rw------- 1 root root 3969024 Feb 13  2019 ./core.1772
-rw------- 1 root root 3764224 Feb 13  2019 ./core.24148
-rw------- 1 root root 15556608 Feb 14  2019 ./core.212607
-rw------- 1 root root 3764224 Feb 14  2019 ./core.39887
-rw------- 1 root root 3969024 Feb 14  2019 ./core.62460
-rw------- 1 root root 4313088 Feb 14  2019 ./core.89628
-rw------- 1 root root 3969024 Feb 14  2019 ./core.83100
-rw------- 1 root root 3993600 Feb 14  2019 ./core.100979
-rw------- 1 root root 3993600 Feb 14  2019 ./core.102765
-rw------- 1 root root 3993600 Feb 14  2019 ./core.118050
-rw------- 1 root root 3969024 Feb 14  2019 ./core.119132
-rw------- 1 root root 39202816 Feb 14  2019 ./core.127086
-rw------- 1 root root 3993600 Feb 14  2019 ./core.129713
-rw------- 1 root root 3969024 Feb 14  2019 ./core.146519
-rw------- 1 root root 21016576 Feb 14  2019 ./core.207916
-rw------- 1 root root 3969024 Feb 14  2019 ./core.220021
-rw------- 1 root root 3969024 Feb 14  2019 ./core.26106
-rw------- 1 root root 3969024 Feb 14  2019 ./core.8214
-rw------- 1 root root 3969024 Feb 14  2019 ./core.68440
-rw------- 1 root root 3969024 Feb 14  2019 ./core.87739
-rw------- 1 root root 3969024 Feb 14  2019 ./core.94819
-rw------- 1 root root 44326912 Feb 14  2019 ./core.180556
-rw------- 1 root root 3969024 Feb 14  2019 ./core.115863
-rw------- 1 root root 3969024 Feb 14  2019 ./core.149846
-rw------- 1 root root 15757312 Feb 14  2019 ./core.121396
-rw------- 1 root root 3969024 Feb 14  2019 ./core.14499
-rw------- 1 root root 3969024 Feb 14  2019 ./core.175959
-rw------- 1 root root 3764224 Feb 14  2019 ./core.183972
-rw------- 1 root root 3993600 Feb 14  2019 ./core.195309
-rw------- 1 root root 3969024 Feb 14  2019 ./core.198352
-rw------- 1 root root 3969024 Feb 14  2019 ./core.217064
-rw------- 1 root root 3969024 Feb 14  2019 ./core.14706
-rw------- 1 root root 3969024 Feb 14  2019 ./core.42954
-rw------- 1 root root 21237760 Feb 14  2019 ./core.99301
-rw------- 1 root root 3969024 Feb 15  2019 ./core.148444
-rw------- 1 root root 7016448 Feb 15  2019 ./core.122209
-rw------- 1 root root 6230016 Feb 15  2019 ./core.174559
-rw------- 1 root root 3969024 Feb 15  2019 ./core.177232
-rw------- 1 root root 12623872 Feb 15  2019 ./core.13179
-rw------- 1 root root 3969024 Feb 15  2019 ./core.219580
-rw------- 1 root root 3969024 Feb 15  2019 ./core.128187
-rw------- 1 root root 3969024 Feb 15  2019 ./core.66231
-rw------- 1 root root 5226496 Feb 15  2019 ./core.55632
-rw------- 1 root root 3969024 Feb 15  2019 ./core.66390
-rw------- 1 root root 39219200 Feb 15  2019 ./core.76116
-rw------- 1 root root 5234688 Feb 15  2019 ./core.111095
-rw------- 1 root root 3969024 Feb 15  2019 ./core.119434
-rw------- 1 root root 3993600 Feb 15  2019 ./core.161735
-rw------- 1 root root 3969024 Feb 15  2019 ./core.159643
-rw------- 1 root root 5156864 Feb 15  2019 ./core.183560
-rw------- 1 root root 3969024 Feb 15  2019 ./core.196797
-rw------- 1 root root 3969024 Feb 15  2019 ./core.196188
-rw------- 1 root root 3764224 Feb 15  2019 ./core.204991
-rw------- 1 root root 3969024 Feb 15  2019 ./core.220251
-rw------- 1 root root 3969024 Feb 15  2019 ./core.225323
-rw------- 1 root root 3969024 Feb 15  2019 ./core.226332
-rw------- 1 root root 3969024 Feb 15  2019 ./core.228120
-rw------- 1 root root 3969024 Feb 15  2019 ./core.1833
-rw------- 1 root root 3969024 Feb 15  2019 ./core.82779
-rw------- 1 root root 3969024 Feb 15  2019 ./core.82777
-rw------- 1 root root 4493312 Feb 15  2019 ./core.82885
-rw------- 1 root root 4231168 Feb 15  2019 ./core.82888
-rw------- 1 root root 3969024 Feb 15  2019 ./core.41565
-rw------- 1 root root 3969024 Feb 15  2019 ./core.46740
-rw------- 1 root root 3969024 Feb 15  2019 ./core.65618
-rw------- 1 root root 3764224 Feb 15  2019 ./core.74102
-rw------- 1 root root 12886016 Feb 15  2019 ./core.92739
-rw------- 1 root root 3764224 Feb 15  2019 ./core.107099
-rw------- 1 root root 3969024 Feb 15  2019 ./core.98075
-rw------- 1 root root 3969024 Feb 15  2019 ./core.126122
-rw------- 1 root root 3969024 Feb 15  2019 ./core.169625
-rw------- 1 root root 3969024 Feb 15  2019 ./core.171172
-rw------- 1 root root 3969024 Feb 15  2019 ./core.170938
-rw------- 1 root root 16211968 Feb 15  2019 ./core.95167
-rw------- 1 root root 3969024 Feb 15  2019 ./core.206853
-rw------- 1 root root 3969024 Feb 15  2019 ./core.209630
-rw------- 1 root root 3969024 Feb 15  2019 ./core.221440
-rw------- 1 root root 3969024 Feb 15  2019 ./core.33933
-rw------- 1 root root 3969024 Feb 15  2019 ./core.58555
-rw------- 1 root root 3969024 Feb 16  2019 ./core.121702
-rw------- 1 root root 3969024 Feb 16  2019 ./core.152718
-rw------- 1 root root 3969024 Feb 16  2019 ./core.188686
-rw------- 1 root root 3969024 Feb 16  2019 ./core.185032
-rw------- 1 root root 3969024 Feb 16  2019 ./core.44183
-rw------- 1 root root 3969024 Feb 16  2019 ./core.15797
-rw------- 1 root root 3969024 Feb 16  2019 ./core.75053
-rw------- 1 root root 3969024 Feb 16  2019 ./core.91967
-rw------- 1 root root 3969024 Feb 16  2019 ./core.151445
-rw------- 1 root root 3969024 Feb 16  2019 ./core.140810
-rw------- 1 root root 3969024 Feb 16  2019 ./core.19488
-rw------- 1 root root 3969024 Feb 16  2019 ./core.37109
-rw------- 1 root root 3969024 Feb 16  2019 ./core.47122
-rw------- 1 root root 3969024 Feb 16  2019 ./core.67960
-rw------- 1 root root 3993600 Feb 16  2019 ./core.95222
-rw------- 1 root root 3969024 Feb 16  2019 ./core.103586
-rw------- 1 root root 3969024 Feb 16  2019 ./core.74010
-rw------- 1 root root 3969024 Feb 16  2019 ./core.161872
-rw------- 1 root root 3969024 Feb 16  2019 ./core.173317
-rw------- 1 root root 38154240 Feb 16  2019 ./core.205829
-rw------- 1 root root 3993600 Feb 16  2019 ./core.48732
-rw------- 1 root root 3969024 Feb 16  2019 ./core.74757
-rw------- 1 root root 3969024 Feb 17  2019 ./core.83181
-rw------- 1 root root 3969024 Feb 17  2019 ./core.107758
-rw------- 1 root root 3969024 Feb 17  2019 ./core.191454
-rw------- 1 root root 3969024 Feb 17  2019 ./core.86074
-rw------- 1 root root 3969024 Feb 17  2019 ./core.16792
-rw------- 1 root root 3969024 Feb 17  2019 ./core.211514
-rw------- 1 root root 3969024 Feb 17  2019 ./core.38891
-rw------- 1 root root 3969024 Feb 17  2019 ./core.78498
-rw------- 1 root root 3993600 Feb 17  2019 ./core.88156
-rw------- 1 root root 3969024 Feb 17  2019 ./core.138449
-rw------- 1 root root 3969024 Feb 18  2019 ./core.223233
-rw------- 1 root root 15564800 Feb 18  2019 ./core.33150
-rw------- 1 root root 39190528 Feb 18  2019 ./core.214515
-rw------- 1 root root 3993600 Feb 18  2019 ./core.14073
-rw------- 1 root root 3993600 Feb 18  2019 ./core.51849
-rw------- 1 root root 44109824 Feb 18  2019 ./core.29658
-rw------- 1 root root 3969024 Feb 18  2019 ./core.73303
-rw------- 1 root root 21803008 Feb 18  2019 ./core.133404
-rw------- 1 root root 3969024 Feb 18  2019 ./core.81464
-rw------- 1 root root 3993600 Feb 18  2019 ./core.74651
-rw------- 1 root root 3969024 Feb 18  2019 ./core.86433
-rw------- 1 root root 3969024 Feb 18  2019 ./core.160643
-rw------- 1 root root 3969024 Feb 18  2019 ./core.166790
-rw------- 1 root root 3969024 Feb 18  2019 ./core.167316
-rw------- 1 root root 3969024 Feb 18  2019 ./core.107338
-rw------- 1 root root 3969024 Feb 18  2019 ./core.209395
-rw------- 1 root root 3969024 Feb 18  2019 ./core.142663
-rw------- 1 root root 3969024 Feb 18  2019 ./core.146618
-rw------- 1 root root 3969024 Feb 18  2019 ./core.4672
-rw------- 1 root root 13414400 Feb 18  2019 ./core.4673
-rw------- 1 root root 3993600 Feb 18  2019 ./core.173339
-rw------- 1 root root 3969024 Feb 18  2019 ./core.175089
-rw------- 1 root root 39776256 Feb 18  2019 ./core.87712
-rw------- 1 root root 3969024 Feb 18  2019 ./core.199971
-rw------- 1 root root 3969024 Feb 18  2019 ./core.203404
-rw------- 1 root root 4509696 Feb 18  2019 ./core.88107
-rw------- 1 root root 3969024 Feb 18  2019 ./core.206350
-rw------- 1 root root 3969024 Feb 18  2019 ./core.5068
-rw------- 1 root root 3969024 Feb 18  2019 ./core.8743
-rw------- 1 root root 3969024 Feb 18  2019 ./core.8611
-rw------- 1 root root 3969024 Feb 18  2019 ./core.5912
-rw------- 1 root root 3969024 Feb 18  2019 ./core.46577
-rw------- 1 root root 3969024 Feb 18  2019 ./core.47579
-rw------- 1 root root 3969024 Feb 18  2019 ./core.54985
-rw------- 1 root root 3969024 Feb 18  2019 ./core.57511
-rw------- 1 root root 29634560 Feb 18  2019 ./core.37668
-rw------- 1 root root 22843392 Feb 18  2019 ./core.172354
-rw------- 1 root root 3993600 Feb 18  2019 ./core.49100
-rw------- 1 root root 3969024 Feb 18  2019 ./core.125098
-rw------- 1 root root 3969024 Feb 18  2019 ./core.132497
-rw------- 1 root root 3969024 Feb 18  2019 ./core.137573
-rw------- 1 root root 3969024 Feb 18  2019 ./core.199949
-rw------- 1 root root 3969024 Feb 18  2019 ./core.145549
-rw------- 1 root root 3969024 Feb 18  2019 ./core.161436
-rw------- 1 root root 3969024 Feb 19  2019 ./core.193667
-rw------- 1 root root 3969024 Feb 19  2019 ./core.200109
-rw------- 1 root root 3969024 Feb 19  2019 ./core.207656
-rw------- 1 root root 3969024 Feb 19  2019 ./core.13587
-rw------- 1 root root 3969024 Feb 19  2019 ./core.32035
-rw------- 1 root root 3969024 Feb 19  2019 ./core.48099
-rw------- 1 root root 3969024 Feb 19  2019 ./core.151310
-rw------- 1 root root 3969024 Feb 19  2019 ./core.151472
-rw------- 1 root root 3969024 Feb 19  2019 ./core.23150
-rw------- 1 root root 3969024 Feb 19  2019 ./core.213107
-rw------- 1 root root 3969024 Feb 19  2019 ./core.199093
-rw------- 1 root root 3993600 Feb 19  2019 ./core.4735
-rw------- 1 root root 4562944 Feb 19  2019 ./core.6081
-rw------- 1 root root 3969024 Feb 19  2019 ./core.10471
-rw------- 1 root root 3969024 Feb 19  2019 ./core.20997
-rw------- 1 root root 3969024 Feb 19  2019 ./core.23290
-rw------- 1 root root 43270144 Feb 19  2019 ./core.4466
-rw------- 1 root root 3969024 Feb 19  2019 ./core.25452
-rw------- 1 root root 3969024 Feb 19  2019 ./core.45758
-rw------- 1 root root 3969024 Feb 19  2019 ./core.46784
-rw------- 1 root root 3993600 Feb 19  2019 ./core.55667
-rw------- 1 root root 3969024 Feb 19  2019 ./core.60491
-rw------- 1 root root 3969024 Feb 19  2019 ./core.70419
-rw------- 1 root root 3969024 Feb 19  2019 ./core.74436
-rw------- 1 root root 3969024 Feb 19  2019 ./core.84123
-rw------- 1 root root 3969024 Feb 19  2019 ./core.86465
-rw------- 1 root root 3993600 Feb 19  2019 ./core.93206
-rw------- 1 root root 3969024 Feb 19  2019 ./core.118002
-rw------- 1 root root 3969024 Feb 19  2019 ./core.94601
-rw------- 1 root root 3969024 Feb 19  2019 ./core.201130
-rw------- 1 root root 3993600 Feb 19  2019 ./core.139323
-rw------- 1 root root 3969024 Feb 19  2019 ./core.172174
-rw------- 1 root root 26484736 Feb 19  2019 ./core.85257
-rw------- 1 root root 38477824 Feb 19  2019 ./core.224496
-rw------- 1 root root 20754432 Feb 19  2019 ./core.115090
-rw------- 1 root root 37801984 Feb 19  2019 ./core.109801
-rw------- 1 root root 3969024 Feb 19  2019 ./core.1870
-rw------- 1 root root 3969024 Feb 19  2019 ./core.37344
-rw------- 1 root root 3969024 Feb 19  2019 ./core.38691
-rw------- 1 root root 3977216 Feb 19  2019 ./core.46509
-rw------- 1 root root 12361728 Feb 19  2019 ./core.39726
-rw------- 1 root root 3977216 Feb 19  2019 ./core.58474
-rw------- 1 root root 3977216 Feb 19  2019 ./core.63555
-rw------- 1 root root 5570560 Feb 19  2019 ./core.79867
-rw------- 1 root root 3969024 Feb 19  2019 ./core.202438
-rw------- 1 root root 3977216 Feb 19  2019 ./core.128433
-rw------- 1 root root 3977216 Feb 20 00:03 ./core.138098
-rw------- 1 root root 3977216 Feb 20 00:26 ./core.138703
-rw------- 1 root root 3977216 Feb 20 02:08 ./core.190056
-rw------- 1 root root 3977216 Feb 20 02:20 ./core.195034
-rw------- 1 root root 3977216 Feb 20 05:11 ./core.35997
-rw------- 1 root root 3977216 Feb 20 06:21 ./core.64779
-rw------- 1 root root 3977216 Feb 20 08:20 ./core.113891
-rw------- 1 root root 3977216 Feb 20 09:13 ./core.135635
-rw------- 1 root root 21364736 Feb 20 10:16 ./core.161345
-rw------- 1 root root 3977216 Feb 20 10:21 ./core.163470
-rw------- 1 root root 3977216 Feb 20 10:45 ./core.171599
-rw------- 1 root root 3977216 Feb 20 10:49 ./core.175610
-rw------- 1 root root 3977216 Feb 20 11:09 ./core.175878
-rw------- 1 root root 3977216 Feb 20 11:22 ./core.59229
-rw------- 1 root root 3977216 Feb 20 12:11 ./core.701
-rw------- 1 root root 3977216 Feb 20 12:19 ./core.5782
-rw------- 1 root root 4001792 Feb 20 12:25 ./core.6861
-rw------- 1 root root 3977216 Feb 20 12:36 ./core.12386
-rw------- 1 root root 3977216 Feb 20 12:59 ./core.31148
-rw------- 1 root root 3977216 Feb 20 13:30 ./core.55905
-rw------- 1 root root 33353728 Feb 20 13:59 ./core.72797
-rw------- 1 root root 3977216 Feb 20 14:00 ./core.74125
-rw------- 1 root root 3977216 Feb 20 14:19 ./core.85548
-rw------- 1 root root 3977216 Feb 20 14:22 ./core.76793
-rw------- 1 root root 4464640 Feb 20 14:31 ./core.84342
-rw------- 1 root root 3977216 Feb 20 14:52 ./core.97796
-rw------- 1 root root 12369920 Feb 20 15:07 ./core.110907
-rw------- 1 root root 3977216 Feb 20 15:30 ./core.127804
-rw------- 1 root root 3977216 Feb 20 15:33 ./core.127510
-rw------- 1 root root 3977216 Feb 20 15:38 ./core.130954
-rw------- 1 root root 3977216 Feb 20 15:40 ./core.132996
-rw------- 1 root root 3977216 Feb 20 16:44 ./core.175909
-rw------- 1 root root 3977216 Feb 20 16:56 ./core.181577
-rw------- 1 root root 12562432 Feb 20 17:34 ./core.157314
-rw------- 1 root root 3977216 Feb 20 17:40 ./core.203099
-rw------- 1 root root 3977216 Feb 20 17:54 ./core.216168
-rw------- 1 root root 3977216 Feb 20 18:16 ./core.7048
-rw------- 1 root root 12853248 Feb 20 18:56 ./core.123245
-rw------- 1 root root 40796160 Feb 20 19:16 ./core.36842
-rw------- 1 root root 3977216 Feb 20 19:28 ./core.46194
-rw------- 1 root root 12369920 Feb 20 20:02 ./core.61404
-rw------- 1 root root 3977216 Feb 20 21:11 ./core.92342
-rw------- 1 root root 38281216 Feb 20 21:26 ./core.143123
-rw------- 1 root root 3977216 Feb 20 22:17 ./core.120243
-rw------- 1 root root 12369920 Feb 21 01:04 ./core.190953
-rw------- 1 root root 12369920 Feb 21 02:59 ./core.20705
-rw------- 1 root root 3977216 Feb 21 05:19 ./core.77835
-rw------- 1 root root 3977216 Feb 21 05:50 ./core.90546
-rw------- 1 root root 3977216 Feb 21 06:33 ./core.104428
-rw------- 1 root root 3977216 Feb 21 09:25 ./core.179291
-rw------- 1 root root 3977216 Feb 21 10:26 ./core.198340
-rw------- 1 root root 3977216 Feb 21 10:50 ./core.217276
-rw------- 1 root root 40947712 Feb 21 11:54 ./core.228258
-rw------- 1 root root 38010880 Feb 21 13:03 ./core.43250
-rw------- 1 root root 3977216 Feb 21 13:16 ./core.71257
-rw------- 1 root root 3977216 Feb 21 13:27 ./core.82579
-rw------- 1 root root 3977216 Feb 21 13:48 ./core.95275
-rw------- 1 root root 4001792 Feb 21 13:59 ./core.73786
-rw------- 1 root root 13803520 Feb 21 14:10 ./core.425
-rw------- 1 root root 12369920 Feb 21 14:31 ./core.122235


# now remove the located files and remove them; verify

[live][root@lamella02 smbd]# find . -mtime +180 -exec rm -rf {} \;

# verify

[live][root@lamella02 smbd]# find . -mtime +180 -exec ls -lt {} \;
[live][root@lamella02 smbd]# 
[live][root@lamella02 smbd]#		# no output, files older than 180 days have all been deleted 

### NOTE on `find`!!! ###

-------------------------------------------------------------------------------

`-exec ls -lt {} \;` for found targets is risky!!! 

# if the located targets were DIRECTORIES, then the output would be the contents
WITHIHN the directories, and not the directores themselves 

# BETTER option: `-exec echo {} \;` --> this doesn't traverse down directories

  # this only gets confusing when there are directories involved; if one is solely working
with files, then the `ls` as an executed command isn't too bad 

  # when dealing with folders, the handles `-maxdepth` & `-type d` are redundant in their purpose
# ADDITIONAL security measures: 

`-type d` --> if we are looking for just folders

`-maxdepth 1` --> do not allow deeper directory traversal other than the directory
specified

`-mtime +` is generally more straight forward than `-mtime -` (for now at least)


-------------------------------------------------------------------------------

# current status 

[live][root@lamella02 smbd]# du -sh /var/log/samba/cores/smbd
28G	/var/log/samba/cores/smbd


## Implement a cronjob

[live][root@lamella02 smbd]# crontab -l
no crontab for root


[live][root@lamella02 smbd]# crontab -e

-------------------------------------------------------------------------------

# Remove core dumps likely triggered by Samba/CTDB that are older than 1 month 

0 5 * * sun /bin/find /var/log/samba/cores/smbd -mtime +30 -exec rm -rf {} \;

-------------------------------------------------------------------------------


[live][root@lamella02 smbd]# crontab -l
# Remove core dumps likely triggered by Samba/CTDB that are older than 1 month 

0 5 * * sun /bin/find /var/log/samba/cores/smbd -mtime +30 -exec rm -rf {} \;


# number of files: to be checked again this upcoming Sunday


### Crontab EDIT!!! ###

-------------------------------------------------------------------------------

0 5 * * sun /bin/find /var/log/samba/cores/smbd -maxdepth 1 -mtime +30 -exec rm {} \;

 # `rm -rf` isn't necessary as we are dealing solely with files & not directories

-------------------------------------------------------------------------------

[live][root@lamella02 smbd]# ls | wc -l
6386

[live][root@lamella02 smbd]# find . -mtime +30 -exec ls {} \; | wc -l
5240

# projected number of files by next Monday

1146

  # verify:

 [live][root@lamella02 biohpcadmin]# ls /var/log/samba/cores/smbd | wc -l
1174        # core dumps deleted; more than projected b.c. of dump accumulations

```

### 09-24-19: LAMELLA01-Cronjob to auto delete "remnant" users

```
[live][root@lamella01 nextcloud]# crontab -l
no crontab for root 

# implement new crontab

crontab -e 

-------------------------------------------------------------------------------
# executing script to remove remnant users on Tue,Thr,&Sat @ 9 P.M. 

0 21 * * 2,4,6 /bin/bash /var/www/nextcloud/orphan_dump.sh
-------------------------------------------------------------------------------

# see script contents 

-------------------------------------------------------------------------------
#!/bin/bash

for i in $(sudo -u apache php occ ldap:show-remnants | awk -F '|' '{print $2}'); do

	echo "Deleting Remnant User: $i"
	sudo -u apache php occ user:delete $i

done 
-------------------------------------------------------------------------------

```

- [AWESOME Crontab Interpretor!](https://crontab.guru/)


### 09-24-19: enabled "LDAP USer Cleanup" & "APCu CLI Memcaching"

```
vi /var/www/nextcloud/config/config.php
------------------------------------------------------------------------
'ldapUserCleanupInterval' => 9,   # PREVIOUSLY set to "0" ... 

  ## checks 50 user entries every 9 minutes
------------------------------------------------------------------------ 


vi /etc/php.d/40-apcu.ini 
------------------------------------------------------------------------ 
apc.enable_cli=1.  #  # PREVIOUSLY set to "0" ...
------------------------------------------------------------------------

```

### 10-21-2019 

- Issue with External Storage Plugin on Yingfei's Account

- Resources: 

  - [ls all symlinks](https://stackoverflow.com/questions/8513133/how-do-i-find-all-of-the-symlinks-in-a-directory-tree)


- Debug: 

  - solutions already attempted: 1.delete Nextcloud account 2.remnant user check 3.check bashrc & bashprofile

  - log entries on user:

```
{"reqId":"Xa3UU2F@0QRS8T45-SAQLwAAAAk","level":1,"time":"2019-10-21T10:52:51-05:00","remoteAddr":"198.215.54.120","user":"65ae6ecd-6efb-4dc0-a01f-4a083f85b28b_2023","app":"no app in context","method":"PUT","url":"\/index.php\/apps\/files_external\/userstorages\/1114","message":"External storage not available: stat() failed","userAgent":"Mozilla\/5.0 (X11; Linux x86_64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/77.0.3865.120 Safari\/537.36","version":"16.0.1.1"}


{"reqId":"Xa3URPcBWYS15Jyi64oXygAAAAg","level":1,"time":"2019-10-21T10:52:36-05:00","remoteAddr":"198.215.54.120","user":"65ae6ecd-6efb-4dc0-a01f-4a083f85b28b_2023","app":"no app in context","method":"GET","url":"\/index.php\/apps\/files_external\/userstorages\/1115?testOnly=true","message":"External storage not available: stat() failed","userAgent":"Mozilla\/5.0 (X11; Linux x86_64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/77.0.3865.120 Safari\/537.36","version":"16.0.1.1"}

{"reqId":"Xa3UOBMRASbn-6x-rYG8DwAAAAA","level":1,"time":"2019-10-21T10:52:24-05:00","remoteAddr":"198.215.54.120","user":"65ae6ecd-6efb-4dc0-a01f-4a083f85b28b_2023","app":"no app in context","method":"GET","url":"\/index.php\/apps\/files_external\/userstorages\/1114?testOnly=false","message":"External storage not available: stat() failed","userAgent":"Mozilla\/5.0 (X11; Linux x86_64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/77.0.3865.120 Safari\/537.36","version":"16.0.1.1"}

{"reqId":"Xa3UOA9Ik9VPkR7UXMV7NwAAAAQ","level":1,"time":"2019-10-21T10:52:24-05:00","remoteAddr":"198.215.54.120","user":"65ae6ecd-6efb-4dc0-a01f-4a083f85b28b_2023","app":"no app in context","method":"GET","url":"\/index.php\/apps\/files_external\/userstorages\/1115?testOnly=false","message":"External storage not available: stat() failed","userAgent":"Mozilla\/5.0 (X11; Linux x86_64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/77.0.3865.120 Safari\/537.36","version":"16.0.1.1"}

{"reqId":"XaZGgykXVg2Jd@fZ1WUS-AAAAAk","level":1,"time":"2019-10-15T17:21:56-05:00","remoteAddr":"198.215.54.120","user":"65ae6ecd-6efb-4dc0-a01f-4a083f85b28b_2023","app":"no app in context","method":"GET","url":"\/index.php\/apps\/recommendations\/api\/recommendations","message":"External storage not available: stat() failed","userAgent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/77.0.3865.90 Safari\/537.36","version":"16.0.1.1"}

{"reqId":"XaZGgn9-FBm26F6UsZpB8AAAABE","level":1,"time":"2019-10-15T17:21:55-05:00","remoteAddr":"198.215.54.120","user":"65ae6ecd-6efb-4dc0-a01f-4a083f85b28b_2023","app":"no app in context","method":"PROPFIND","url":"\/remote.php\/dav\/files\/65ae6ecd-6efb-4dc0-a01f-4a083f85b28b_2023\/","message":"External storage not available: stat() failed","userAgent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/77.0.3865.90 Safari\/537.36","version":"16.0.1.1"}

{"reqId":"XaTsr0s2uzDtdPpMXJByEwAAAAo","level":4,"time":"2019-10-14T16:46:23-05:00","remoteAddr":"198.215.54.120","user":"65ae6ecd-6efb-4dc0-a01f-4a083f85b28b_2023","app":"webdav","method":"PROPFIND","url":"\/remote.php\/dav\/files\/65ae6ecd-6efb-4dc0-a01f-4a083f85b28b_2023\/work","message":{"Exception":"Sabre\\DAV\\Exception\\ServiceUnavailable","Message":"","Code":0,"Trace":[{"file":"\/var\/www\/nextcloud\/3rdparty\/sabre\/dav\/lib\/DAV\/Tree.php","line":76,"function":"getChild","class":"OCA\\DAV\\Connector\\Sabre\\Directory","type":"->","args":["work"]},{"file":"\/var\/www\/nextcloud\/3rdparty\/sabre\/dav\/lib\/DAV\/Server.php","line":967,"function":"getNodeForPath","class":"Sabre\\DAV\\Tree","type":"->","args":["files\/65ae6ecd-6efb-4dc0-a01f-4a083f85b28b_2023\/work"]},{"file":"\/var\/www\/nextcloud\/3rdparty\/sabre\/dav\/lib\/DAV\/Server.php","line":1666,"function":"getPropertiesIteratorForPath","class":"Sabre\\DAV\\Server","type":"->","args":["files\/65ae6ecd-6efb-4dc0-a01f-4a083f85b28b_2023\/work",["{DAV:}getlastmodified","{DAV:}getetag","{DAV:}getcontenttype","{DAV:}resourcetype","{http:\/\/owncloud.org\/ns}fileid","{http:\/\/owncloud.org\/ns}permissions","{http:\/\/owncloud.org\/ns}size","{DAV:}getcontentlength","{http:\/\/nextcloud.org\/ns}has-preview","{http:\/\/nextcloud.org\/ns}mount-type","{http:\/\/nextcloud.org\/ns}is-encrypted","{http:\/\/open-collaboration-services.org\/ns}share-permissions","{http:\/\/owncloud.org\/ns}tags","{http:\/\/owncloud.org\/ns}favorite","{http:\/\/owncloud.org\/ns}comments-unread","{http:\/\/owncloud.org\/ns}owner-id","{http:\/\/owncloud.org\/ns}owner-display-name","{http:\/\/owncloud.org\/ns}share-types"],1]},{"file":"\/var\/www\/nextcloud\/3rdparty\/sabre\/dav\/lib\/DAV\/CorePlugin.php","line":355,"function":"generateMultiStatus","class":"Sabre\\DAV\\Server","type":"->","args":[{"__class__":"Generator"},false]},{"function":"httpPropFind","class":"Sabre\\DAV\\CorePlugin","type":"->","args":[{"absoluteUrl":"https:\/\/lamella.biohpc.swmed.edu\/remote.php\/dav\/files\/65ae6ecd-6efb-4dc0-a01f-4a083f85b28b_2023\/work","__class__":"Sabre\\HTTP\\Request"},{"__class__":"Sabre\\HTTP\\Response"}]},{"file":"\/var\/www\/nextcloud\/3rdparty\/sabre\/event\/lib\/EventEmitterTrait.php","line":105,"function":"call_user_func_array","args":[[{"__class__":"Sabre\\DAV\\CorePlugin"},"httpPropFind"],[{"absoluteUrl":"https:\/\/lamella.biohpc.swmed.edu\/remote.php\/dav\/files\/65ae6ecd-6efb-4dc0-a01f-4a083f85b28b_2023\/work","__class__":"Sabre\\HTTP\\Request"},{"__class__":"Sabre\\HTTP\\Response"}]]},{"file":"\/var\/www\/nextcloud\/3rdparty\/sabre\/dav\/lib\/DAV\/Server.php","line":479,"function":"emit","class":"Sabre\\Event\\EventEmitter","type":"->","args":["method:PROPFIND",[{"absoluteUrl":"https:\/\/lamella.biohpc.swmed.edu\/remote.php\/dav\/files\/65ae6ecd-6efb-4dc0-a01f-4a083f85b28b_2023\/work","__class__":"Sabre\\HTTP\\Request"},{"__class__":"Sabre\\HTTP\\Response"}]]},{"file":"\/var\/www\/nextcloud\/3rdparty\/sabre\/dav\/lib\/DAV\/Server.php","line":254,"function":"invokeMethod","class":"Sabre\\DAV\\Server","type":"->","args":[{"absoluteUrl":"https:\/\/lamella.biohpc.swmed.edu\/remote.php\/dav\/files\/65ae6ecd-6efb-4dc0-a01f-4a083f85b28b_2023\/work","__class__":"Sabre\\HTTP\\Request"},{"__class__":"Sabre\\HTTP\\Response"}]},{"file":"\/var\/www\/nextcloud\/apps\/dav\/lib\/Server.php","line":316,"function":"exec","class":"Sabre\\DAV\\Server","type":"->","args":[]},{"file":"\/var\/www\/nextcloud\/apps\/dav\/appinfo\/v2\/remote.php","line":35,"function":"exec","class":"OCA\\DAV\\Server","type":"->","args":[]},{"file":"\/var\/www\/nextcloud\/remote.php","line":163,"args":["\/var\/www\/nextcloud\/apps\/dav\/appinfo\/v2\/remote.php"],"function":"require_once"}],"File":"\/var\/www\/nextcloud\/apps\/dav\/lib\/Connector\/Sabre\/Directory.php","Line":225,"CustomMessage":"--"},"userAgent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/77.0.3865.90 Safari\/537.36","version":"16.0.1.1"}

{"reqId":"XaTpkO91GToybS2zQ8WyLQAAAAM","level":0,"time":"2019-10-14T16:33:04-05:00","remoteAddr":"198.215.54.120","user":"65ae6ecd-6efb-4dc0-a01f-4a083f85b28b_2023","app":"core","method":"GET","url":"\/index.php\/logout?requesttoken=QBEdrtJFfI2HKkshnPHFti3dxtv%2FlidF03Tg6ch%2FdKk%3D%3AL3d73L99Odm0HnIQr5WdxhvrnOuq2R4Iuhutj5hUQs8%3D","message":{"Exception":"OC\\AppFramework\\Middleware\\Security\\Exceptions\\CrossSiteRequestForgeryException","Message":"CSRF check failed","Code":412,"Trace":[{"file":"\/var\/www\/nextcloud\/lib\/private\/AppFramework\/Middleware\/MiddlewareDispatcher.php","line":95,"function":"beforeController","class":"OC\\AppFramework\\Middleware\\Security\\SecurityMiddleware","type":"->","args":[{"__class__":"OC\\Core\\Controller\\LoginController"},"logout"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/AppFramework\/Http\/Dispatcher.php","line":98,"function":"beforeController","class":"OC\\AppFramework\\Middleware\\MiddlewareDispatcher","type":"->","args":[{"__class__":"OC\\Core\\Controller\\LoginController"},"logout"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/AppFramework\/App.php","line":126,"function":"dispatch","class":"OC\\AppFramework\\Http\\Dispatcher","type":"->","args":[{"__class__":"OC\\Core\\Controller\\LoginController"},"logout"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/AppFramework\/Routing\/RouteActionHandler.php","line":47,"function":"main","class":"OC\\AppFramework\\App","type":"::","args":["OC\\Core\\Controller\\LoginController","logout",{"__class__":"OC\\AppFramework\\DependencyInjection\\DIContainer"},{"_route":"core.login.logout"}]},{"function":"__invoke","class":"OC\\AppFramework\\Routing\\RouteActionHandler","type":"->","args":[{"_route":"core.login.logout"}]},{"file":"\/var\/www\/nextcloud\/lib\/private\/Route\/Router.php","line":297,"function":"call_user_func","args":[{"__class__":"OC\\AppFramework\\Routing\\RouteActionHandler"},{"_route":"core.login.logout"}]},{"file":"\/var\/www\/nextcloud\/lib\/base.php","line":975,"function":"match","class":"OC\\Route\\Router","type":"->","args":["\/logout"]},{"file":"\/var\/www\/nextcloud\/index.php","line":42,"function":"handleRequest","class":"OC","type":"::","args":[]}],"File":"\/var\/www\/nextcloud\/lib\/private\/AppFramework\/Middleware\/Security\/SecurityMiddleware.php","Line":174,"CustomMessage":"--"},"userAgent":"Mozilla\/5.0 (X11; Linux x86_64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/77.0.3865.120 Safari\/537.36","version":"16.0.1.1"}

{"reqId":"XaTYF3Y28enxDFZ7-Vua2gAAAA8","level":3,"time":"2019-10-14T15:18:32-05:00","remoteAddr":"198.215.54.120","user":"65ae6ecd-6efb-4dc0-a01f-4a083f85b28b_2023","app":"core","method":"POST","url":"\/index.php\/login?redirect_url=\/index.php\/logout%3Frequesttoken%3DYTMziIkmjPKICi3Qpd9VMnOMTIjcUiFzn9XQkrnyO5U%253D%253AD1VluuYX4rHSeh2%252FxugjWzXtCfi0Pxcc2LCc3OGnU8U%253D","message":"Following symlinks is not allowed ('\/cloud_data\/65ae6ecd-6efb-4dc0-a01f-4a083f85b28b_2023\/files_external' -> '\/project\/shared\/.cloud_data\/65ae6ecd-6efb-4dc0-a01f-4a083f85b28b_2023\/files_external\/' not inside '\/cloud_data\/65ae6ecd-6efb-4dc0-a01f-4a083f85b28b_2023\/')","userAgent":"Mozilla\/5.0 (X11; Linux x86_64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/77.0.3865.120 Safari\/537.36","version":"16.0.1.1"}


{"reqId":"XaTYF3Y28enxDFZ7-Vua2gAAAA8","level":2,"time":"2019-10-14T15:18:32-05:00","remoteAddr":"198.215.54.120","user":"65ae6ecd-6efb-4dc0-a01f-4a083f85b28b_2023","app":"core","method":"POST","url":"\/index.php\/login?redirect_url=\/index.php\/logout%3Frequesttoken%3DYTMziIkmjPKICi3Qpd9VMnOMTIjcUiFzn9XQkrnyO5U%253D%253AD1VluuYX4rHSeh2%252FxugjWzXtCfi0Pxcc2LCc3OGnU8U%253D","message":{"Exception":"OCP\\Files\\ForbiddenException","Message":"Following symlinks is not allowed","Code":0,"Trace":[{"file":"\/var\/www\/nextcloud\/lib\/private\/Files\/Storage\/Local.php","line":125,"function":"getSourcePath","class":"OC\\Files\\Storage\\Local","type":"->","args":["cache"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/Files\/Storage\/Wrapper\/Wrapper.php","line":102,"function":"opendir","class":"OC\\Files\\Storage\\Local","type":"->","args":["cache"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/Files\/Storage\/Wrapper\/Wrapper.php","line":102,"function":"opendir","class":"OC\\Files\\Storage\\Wrapper\\Wrapper","type":"->","args":["cache"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/Files\/View.php","line":1154,"function":"opendir","class":"OC\\Files\\Storage\\Wrapper\\Wrapper","type":"->","args":["cache"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/Files\/View.php","line":366,"function":"basicOperation","class":"OC\\Files\\View","type":"->","args":["opendir","\/",["read"]]},{"file":"\/var\/www\/nextcloud\/lib\/private\/Cache\/File.php","line":181,"function":"opendir","class":"OC\\Files\\View","type":"->","args":["\/"]},{"file":"\/var\/www\/nextcloud\/lib\/base.php","line":812,"function":"gc","class":"OC\\Cache\\File","type":"->","args":[]},{"function":"{closure}","class":"OC","type":"::","args":["*** sensitive parameters replaced ***"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/Hooks\/EmitterTrait.php","line":99,"function":"call_user_func_array","args":[{"__class__":"Closure"},["*** sensitive parameter replaced ***","*** sensitive parameter replaced ***","*** sensitive parameter replaced ***"]]},{"file":"\/var\/www\/nextcloud\/lib\/private\/Hooks\/PublicEmitter.php","line":36,"function":"emit","class":"OC\\Hooks\\BasicEmitter","type":"->","args":["\\OC\\User","postLogin",["*** sensitive parameter replaced ***","*** sensitive parameter replaced ***","*** sensitive parameter replaced ***"]]},{"file":"\/var\/www\/nextcloud\/lib\/private\/User\/Session.php","line":375,"function":"emit","class":"OC\\Hooks\\PublicEmitter","type":"->","args":["\\OC\\User","postLogin",["*** sensitive parameter replaced ***","*** sensitive parameter replaced ***","*** sensitive parameter replaced ***"]]},{"file":"\/var\/www\/nextcloud\/core\/Controller\/LoginController.php","line":336,"function":"completeLogin","class":"OC\\User\\Session","type":"->","args":["*** sensitive parameters replaced ***"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/AppFramework\/Http\/Dispatcher.php","line":166,"function":"tryLogin","class":"OC\\Core\\Controller\\LoginController","type":"->","args":["*** sensitive parameters replaced ***"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/AppFramework\/Http\/Dispatcher.php","line":99,"function":"executeController","class":"OC\\AppFramework\\Http\\Dispatcher","type":"->","args":[{"__class__":"OC\\Core\\Controller\\LoginController"},"tryLogin"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/AppFramework\/App.php","line":126,"function":"dispatch","class":"OC\\AppFramework\\Http\\Dispatcher","type":"->","args":[{"__class__":"OC\\Core\\Controller\\LoginController"},"tryLogin"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/AppFramework\/Routing\/RouteActionHandler.php","line":47,"function":"main","class":"OC\\AppFramework\\App","type":"::","args":["OC\\Core\\Controller\\LoginController","tryLogin",{"__class__":"OC\\AppFramework\\DependencyInjection\\DIContainer"},{"_route":"core.login.tryLogin"}]},{"function":"__invoke","class":"OC\\AppFramework\\Routing\\RouteActionHandler","type":"->","args":[{"_route":"core.login.tryLogin"}]},{"file":"\/var\/www\/nextcloud\/lib\/private\/Route\/Router.php","line":297,"function":"call_user_func","args":[{"__class__":"OC\\AppFramework\\Routing\\RouteActionHandler"},{"_route":"core.login.tryLogin"}]},{"file":"\/var\/www\/nextcloud\/lib\/base.php","line":975,"function":"match","class":"OC\\Route\\Router","type":"->","args":["\/login"]},{"file":"\/var\/www\/nextcloud\/index.php","line":42,"function":"handleRequest","class":"OC","type":"::","args":[]}],"File":"\/var\/www\/nextcloud\/lib\/private\/Files\/Storage\/Local.php","Line":387,"CustomMessage":"Exception when running cache gc."},"userAgent":"Mozilla\/5.0 (X11; Linux x86_64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/77.0.3865.120 Safari\/537.36","version":"16.0.1.1"}

 
```

### 10-22-2019: Further Debug on Yingfei's External Storage

```
### list "Admin Mounts"

sudo -u apache php occ files_external:list
+----------+-------------+-----------------+--------------------------------------+------------------------------------------------------------------+----------------------+------------------+-------------------+
| Mount ID | Mount Point | Storage         | Authentication Type                  | Configuration                                                    | Options              | Applicable Users | Applicable Groups |
+----------+-------------+-----------------+--------------------------------------+------------------------------------------------------------------+----------------------+------------------+-------------------+
| 1        | /Cloud      | Cloud           | Log-in credentials, save in database | host: "https:\/\/cloud.biohpc.swmed.edu", root: "", secure: true | enable_sharing: true | All              |                   |
| 946      | /home       | BioHPC/Lysosome | Log-in credentials, save in database | root: "", share: "homes"                                         | enable_sharing: true | All              |                   |
+----------+-------------+-----------------+--------------------------------------+------------------------------------------------------------------+----------------------+------------------+-------------------+

### Export "User-specific Mounts" Options

sudo -u apache php occ files_external:export [user ID] 


### Show User Settings

sudo -u apache php occ user:setting <nextcloud user ID> 

e.g.

sudo -u apache php occ user:setting 65ae6ecd-6efb-4dc0-a01f-4a083f85b28b_2023
  - avatar:
    - generated: true
    - version: 12
  - core:
    - lang: en
    - timezone: America/Chicago
  - files:
    - file_sorting: name
    - file_sorting_direction: desc
    - show_sharing_menu: 0
  - files_external:
    - config_version: 0.5.0
  - firstrunwizard:
    - show: 0
  - login:
    - lastLogin: 1571766269
  - login_token:
    - 83X9mHQQ4kh7Cm13m1VOVA/MDFFPoI1s: 1571766269
    - pCCa2S+/hoJXUI+ZzGzWrb9h5R8kizZX: 1571673143
  - settings:
    - email: yingfei.chen@utsouthwestern.edu
  - user_ldap:
    - displayName: Yingfei Chen (s173217)
    - firstLoginAccomplished: 1
    - homePath: 
    - lastFeatureRefresh: 1571766269
    - uid: s173217


### Delete User Settings

sudo -u apache php occ user:setting --delete <nextcloud user ID> <category> <item> "<value>" 

e.g. deleted last login_token

sudo -u apache php occ user:setting --delete 65adelete 65ae6ecd-6efb-4dc0-a01f-4a083f85b28b_2023 login_token RqMol2Fn9IDlEEiEZi2JmFA/rDAxvD0J


### Add User Settings

sudo -u apache php occ user:setting <nextcloud user ID> <category> <item> "<value>"

e.g. add files options:

----------------------------------------------------------------------------------------
sudo -u apache php occ user:setting 65ae6ecd-6efb-4dc0-a01f-4a083f85b28b_2023 files show_hidden "0"

sudo -u apache php occ user:setting 65ae6ecd-6efb-4dc0-a01f-4a083f85b28b_2023 files show_grid "0"

sudo -u apache php occ user:setting 65ae6ecd-6efb-4dc0-a01f-4a083f85b28b_2023 core enabled "true"
----------------------------------------------------------------------------------------

```

### SAMBA Debug

- Another Fix: Check Samba Logs in `/var/log/samba/<IP/hostname>` 

  - look into the more recent entries/get user to do a login first 

- `smbpasswd -e <uid>` --> check use's entry in Sambda Database 

- `sambpasswd -a <uid>` --> re-add user to Samba database 

- **ISSUE FOUND**: `ldap passwd sync` option turned to `no` in `smb.conf`!!! (lamella01)

### Additional Debugging:

- check `.bash_profile` 

- check `.bashrc`

- check for symlinks! 

### Successful Fix from Ticket#2019123010008084

- **Issue**: user logs in to lamella web portal, but no files exists

- **diangosis**: 

```
# Do SMB mount from client --> user can see files --> user exists in database

# if SMB mount does NOT work --> `smbpasswd -e <uid>` (check user in Samba database) --> `smbpasswd -a <uid>` (make sure password SAME as LDAP) 

# if SMB password reset does not work --> backup NC data in `/home/backup` --> disable LDAP account --> occ ldap:check-user --> occ ldap:remove-remnants --> check if user data folder still exists (should be removed) 

# re-enable LDAP user account --> `occ ldap:search <user display name>` --> test user login

```

\
\

### 04-24-2020: added logrotate to Lamella01

```
# CONFIGURATION

cat /etc/logrotate.d/nextcloud1.conf
--------------------------------
/cloud_data/nexcloud1.log {
    su apache apache
    weekly
    rotate 4
    size 100M
    missingok
    notifempty
    create 644 apache apache
}
--------------------------------

# MANUALLY FORCE A ROTATION

--------------------------------
sudo logrotate -vf /etc/logrotate.d/nextcloud1.conf
reading config file /etc/logrotate.d/nextcloud1.conf
Allocating hash table for state file, size 15360 B

Handling 1 logs

rotating pattern: /cloud_data/nexcloud1.log  forced from command line (4 rotations)
empty log files are not rotated, old logs are removed
switching euid to 48 and egid to 48
considering log /cloud_data/nexcloud1.log
  log needs rotating
rotating log /cloud_data/nexcloud1.log, log->rotateCount is 4
dateext suffix '-20200424'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
renaming /cloud_data/nexcloud1.log.4 to /cloud_data/nexcloud1.log.5 (rotatecount 4, logstart 1, i 4), 
old log /cloud_data/nexcloud1.log.4 does not exist
renaming /cloud_data/nexcloud1.log.3 to /cloud_data/nexcloud1.log.4 (rotatecount 4, logstart 1, i 3), 
old log /cloud_data/nexcloud1.log.3 does not exist
renaming /cloud_data/nexcloud1.log.2 to /cloud_data/nexcloud1.log.3 (rotatecount 4, logstart 1, i 2), 
old log /cloud_data/nexcloud1.log.2 does not exist
renaming /cloud_data/nexcloud1.log.1 to /cloud_data/nexcloud1.log.2 (rotatecount 4, logstart 1, i 1), 
renaming /cloud_data/nexcloud1.log.0 to /cloud_data/nexcloud1.log.1 (rotatecount 4, logstart 1, i 0), 
old log /cloud_data/nexcloud1.log.0 does not exist
log /cloud_data/nexcloud1.log.5 doesn't exist -- won't try to dispose of it
renaming /cloud_data/nexcloud1.log to /cloud_data/nexcloud1.log.1
creating new /cloud_data/nexcloud1.log mode = 0644 uid = 48 gid = 48
switching euid to 0 and egid to 0
Removing /var/adm/ras/mmsysmonitor.log from state file, because it does not exist and has not been rotated for one year
Removing /var/log/wtmp from state file, because it does not exist and has not been rotated for one year
Removing /var/adm/ras/mmsysmonitor.lamella01.log from state file, because it does not exist and has not been rotated for one year
--------------------------------

```

- [NC SupportReference](https://help.nextcloud.com/t/nextcloud-logfile-not-in-var-log-nextcloud-log/2986/9)

\
\
\

### Do Samba/CIFS Mounts on Linux Machines

- [basic info](https://www.looklinux.com/how-to-mount-samba-share-smbfs-in-linux/)

- [link with correct syntax](https://www.cyberciti.biz/faq/linux-mount-cifs-windows-share/) 

```
# check mount shares:

smbclient -L 198.215.54.119 -U zpang1
mkdir failed on directory /var/lib/samba/lock/msg.lock: Permission denied
Unable to initialize messaging context
Enter SAMBA\zpang1's password: 

	Sharename       Type      Comment
	---------       ----      -------
	home            Disk      
	project         Disk      
	work            Disk      
	archive         Disk      
	homes           Disk      
	ansir_ext       Disk      
	IPC$            IPC       IPC Service (BioHPC Samba Server Version 4.7.1)
	zpang1          Disk      Home directory of zpang1
Reconnecting with SMB1 for workgroup listing.

	Server               Comment
	---------            -------

	Workgroup            Master
	---------            -------

# mount /home2 via lamella02

sudo mount -t cifs //198.215.54.119/zpang1 /smb_home/ -o user=zpang1
Password for zpang1@//198.215.54.119/zpang1

df -Th | grep -i smb_home
//198.215.54.119/zpang1              cifs       50G   32G   19G  63% /smb_home

```


\
\
\

### Pending Tasks

- **Cron**

  - clean old samba logs

  - rename orphan script to `orphan_relief.sh`  --> see if need to implement on cloud

- **Git**

  - store current version

  - notable config files

\

- **Cloud**

   - store current version

   - notable config files

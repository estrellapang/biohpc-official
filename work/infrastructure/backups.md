## Notes on General Backups

[documentation](https://docs.biohpc.swmed.edu/dokuwiki/doku.php?id=project_rsnapshot_backup#current_departments_schedule)

### /project & /archive backups

> Performed by NucleusS004 via user rsnapshot (who does not exist as an unix user) 

- **location of cronjobs** 

`/etc/cron.d/`

- **location of rsnapshot configurations** (example) 

`/work/.backup/backup_project/.config/rsnapshot-radiology.conf`

  - lockfiles prevent two backup scripts of the same PID to run simultaneously, which is why we need to very careful about selecting the time slots for backups

``` 
# If enabled, rsnapshot will write a lockfile to prevent two instances
# from running simultaneously (and messing up the snapshot_root).
# If you enable this, make sure the lockfile directory is not world
# writable. Otherwise anyone can prevent the program from running.
#
lockfile        /var/run/rsnapshot.radiology.pid

```

\
\
\

### Tickets

- Ticket#2020040110009711 --> monthly and weekly backups conflict; weekly could not initiate bc of lockfile while monthly was taking place

- Ticket#2020062110010785

  - issue: `ERROR: /usr/bin/rsync returned 255 while processing`

  - cause: SSH issue, `backup_services` user cannot ssh into server of concern

  - [source](https://community.netgear.com/t5/Using-your-ReadyNAS-in-Business/rsnapshot-error-255/td-p/898267)

  - resolve: make homedirectory for backup user, add backup001's pub key in `authorized_keys`; allowUsers backupservices in `/etc/ssh/sshd_config`

  

\
\
\

### MANUALLY Run RSNAPSHOTS

- example of ticket reporting erroneous rsnapshot:

```

echo 47563 > /var/run/rsnapshot.radiology.pid
/bin/rm -rf /work/.backup/backup_project/radiology/weekly.3/
/bin/rm: cannot remove
‘/work/.backup/backup_project/radiology/weekly.3/ANSIR_lab/XNAT/EXTERNAL/XNAT_DATA/archive/BRP/arc001/BRP_YT329_20160727/SCANS/26/DICOM/Brp_yt329.MR.head_iTAKL_2016.26.2.20160727.161237.1l9a7hg.dcm’:
Stale file handle
.
.
.
.
.
----------------------------------------------------------------------------
rsnapshot encountered an error! The program was invoked with these options:
/usr/local/bin/rsnapshot -c \
    /work/.backup/backup_project/.config/rsnapshot-radiology.conf weekly
----------------------------------------------------------------------------
ERROR: Warning! /bin/rm failed.
Could not open logfile /work/.backup/backup_project/.log/rsnapshot_radiology.log for
writing

```

- **COMMAND**

```
-------------------------------------------------------------------------------
/usr/local/bin/rsnapshot -c /work/.backup/backup_project/.config/rsnapshot-radiology.conf weekly
-------------------------------------------------------------------------------
```


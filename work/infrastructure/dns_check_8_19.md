## Service Hostanames Un-Resolvable by BioHPC DNS

------------------------------------------------------

clairityai				198.215.54.106

answer-test.biohpc.swmed.edu		198.215.54.71

nuclia-test2				198.215.54.76

singularity-build.biohpc.swmed.edu	198.215.54.108 

pops.biohpc.swmed.edu			129.112.9.10

lce.biohpc.swmed.edu			129.112.9.39

lce-test.biohpc.swmed.edu		198.215.54.66

debias.biohpc.swmed.edu			129.112.9.21

pecan.biohpc.swmed.edu			198.215.54.39

bicf-redmine.biohpc.swmed.edu		198.215.54.39

kce.biohpc.swmed.edu			198.215.56.32 

sap.biohpc.swmed.edu			129.112.9.2
	
------------------------------------------------------

## Customizations Made to Workstation wsc037:

### 10-31-2019 

- Updated Gnome-Tweak-Tool:

```
  gnome-shell.x86_64 0:3.28.3-11.el7                                                                   gnome-tweak-tool.noarch 0:3.28.1-2.el7.2                                                                  

Dependency Installed:
  evolution-data-server-langpacks.noarch 0:3.28.5-3.el7               libinput.x86_64 0:1.10.7-2.el7               libwayland-egl.x86_64 0:1.15.0-1.el7               mozjs52.x86_64 0:52.9.0-1.el7              

Updated:
  gdm.x86_64 1:3.28.2-16.el7                                   gnome-session.x86_64 0:3.28.1-7.el7                                   xorg-x11-server-Xorg.x86_64 0:1.20.4-7.el7                                  

Dependency Updated:
  bluez.x86_64 0:5.44-5.el7                        evolution-data-server.x86_64 0:3.28.5-3.el7      folks.x86_64 1:0.11.4-1.el7                              gjs.x86_64 0:1.52.5-1.el7_6                        
  glib2.x86_64 0:2.56.1-5.el7                      glib2-devel.x86_64 0:2.56.1-5.el7                gnome-clocks.x86_64 0:3.28.0-1.el7                       gnome-contacts.x86_64 0:3.28.2-1.el7               
  gnome-desktop3.x86_64 0:3.28.2-2.el7             gnome-session-xsession.x86_64 0:3.28.1-7.el7     gnome-settings-daemon.x86_64 0:3.28.1-5.el7              gsettings-desktop-schemas.x86_64 0:3.28.0-2.el7    
  libgweather.x86_64 0:3.28.2-2.el7                libical.x86_64 0:3.0.3-2.el7                     mutter.x86_64 0:3.28.3-10.el7                            xorg-x11-drv-ati.x86_64 0:19.0.1-2.el7             
  xorg-x11-drv-dummy.x86_64 0:0.3.7-1.el7.1        xorg-x11-drv-fbdev.x86_64 0:0.5.0-1.el7          xorg-x11-drv-intel.x86_64 0:2.99.917-28.20180530.el7     xorg-x11-drv-nouveau.x86_64 1:1.0.15-1.el7         
  xorg-x11-drv-qxl.x86_64 0:0.1.5-5.el7            xorg-x11-drv-v4l.x86_64 0:0.2.0-49.el7           xorg-x11-drv-vesa.x86_64 0:2.4.0-3.el7                   xorg-x11-drv-vmware.x86_64 0:13.2.1-1.el7.1        
  xorg-x11-server-common.x86_64 0:1.20.4-7.el7    

Replaced:
  caribou.x86_64 0:0.4.21-1.el7    caribou-gtk2-module.x86_64 0:0.4.21-1.el7    caribou-gtk3-module.x86_64 0:0.4.21-1.el7    gnome-tweak-tool.noarch 0:3.22.0-1.el7    python2-caribou.noarch 0:0.4.21-1.el7   

```

- Went to `tweak` & added # of workspaces to 7


### 11-05-2019: WS Crashed: No Display

#### Troubleshoot/Symptoms

```
Tue Nov  5 10:52:39 2019       
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 390.30                 Driver Version: 390.30                    |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|===============================+======================+======================|
|   0  Quadro P2000        Off  | 00000000:01:00.0 Off |                  N/A |
|  0%   37C    P0    19W /  75W |      0MiB /  5053MiB |      0%      Default |
+-------------------------------+----------------------+----------------------+
                                                                               
+-----------------------------------------------------------------------------+
| Processes:                                                       GPU Memory |
|  GPU       PID   Type   Process name                             Usage      |
|=============================================================================|
|  No running processes found                                                 |
+-----------------------------------------------------------------------------+

# Graphic Card Off?

# restart display manager will not do

#  dmesg | grep -i NVRM
---------------------------------------------------------------------------------------------------------
[    8.875454] NVRM: loading NVIDIA UNIX x86_64 Kernel Module  390.30  Wed Jan 31 22:08:49 PST 2018 (using threaded interrupts)
[  292.267050] NVRM: Your system is not currently configured to drive a VGA console
---------------------------------------------------------------------------------------------------------

# check PCI bus connections:

---------------------------------------------------------------------------------------------------------
lspci -nnv | grep -i nvidia
01:00.0 VGA compatible controller [0300]: NVIDIA Corporation GP106GL [Quadro P2000] [10de:1c30] (rev a1) (prog-if 00 [VGA controller])
	Subsystem: NVIDIA Corporation Device [10de:11b3]
	Kernel driver in use: nvidia
	Kernel modules: nouveau, nvidia_drm, nvidia
01:00.1 Audio device [0403]: NVIDIA Corporation GP106 High Definition Audio Controller [10de:10f1] (rev a1)
	Subsystem: NVIDIA Corporation Device [10de:11b3]
---------------------------------------------------------------------------------------------------------

  ## COMMENT ## looks like the graphics card is getting picked up by the system---more likely the issue is arising out of driver software rather than hardware

# Feedback from `/var/log/Xorg.0.log`

---------------------------------------------------------------------------------------------------------
[ 64446.312] (II) Loading /usr/lib64/nvidia/xorg/libglx.so
[ 64446.316] (II) Module glx: vendor="NVIDIA Corporation"
[ 64446.316]    compiled for 4.0.2, module version = 1.0.0
[ 64446.316]    Module class: X.Org Server Extension
[ 64446.316] (II) NVIDIA GLX Module  390.30  Wed Jan 31 21:27:29 PST 2018
[ 64446.316] (II) LoadModule: "nvidia"
[ 64446.316] (II) Loading /usr/lib64/xorg/modules/drivers/nvidia_drv.so
[ 64446.317] (II) Module nvidia: vendor="NVIDIA Corporation"
[ 64446.317]    compiled for 4.0.2, module version = 1.0.0
[ 64446.317]    Module class: X.Org Video Driver
[ 64446.317] ================ WARNING WARNING WARNING WARNING ================
[ 64446.317] This server has a video driver ABI version of 24.0 that this
driver does not officially support.  Please check
http://www.nvidia.com/ for driver updates or downgrade to an X
server with a supported driver ABI.
[ 64446.317] =================================================================
[ 64446.317] (EE) NVIDIA: Use the -ignoreABI option to override this check.
[ 64446.317] (II) UnloadModule: "nvidia"
[ 64446.317] (II) Unloading nvidia
[ 64446.317] (EE) Failed to load module "nvidia" (unknown error, 0)
[ 64446.317] (EE) No drivers available.
[ 64446.317] (EE)
Fatal server error:
[ 64446.317] (EE) no screens found(EE)
[ 64446.317] (EE)
---------------------------------------------------------------------------------------------------------

```

### Fix One: Update nvidia-kmod one minor version:

```
yumdownloader nvidia-kmod.x86_64 1:390.116-2.el7

yum localinstall nvidia-kmod-390.116-2.el7.x86_64.rpm

---------------------------------------------------------------------------------------------------------

Dependencies Resolved

===================================================================================================================================================
 Package                      Arch                    Version                             Repository                                          Size
===================================================================================================================================================
Updating:
 nvidia-kmod                  x86_64                  1:390.116-2.el7                     /nvidia-kmod-390.116-2.el7.x86_64                   24 M

Transaction Summary
===================================================================================================================================================
Upgrade  1 Package

Total size: 24 M
Is this ok [y/d/N]: y
Downloading packages:
Running transaction check
Running transaction test
Transaction test succeeded
Running transaction
  Updating   : 1:nvidia-kmod-390.116-2.el7.x86_64                                                                                              1/2 

Creating symlink /var/lib/dkms/nvidia/390.116/source ->
                 /usr/src/nvidia-390.116

DKMS: add completed.

Kernel preparation unnecessary for this kernel.  Skipping...

Building module:
cleaning build area...
'make' -j8 NV_EXCLUDE_BUILD_MODULES='' KERNEL_UNAME=3.10.0-693.el7.x86_64 modules........
cleaning build area...

DKMS: build completed.

nvidia.ko.xz:
Running module version sanity check.
 - Original module
   - This kernel never originally had a module by this name
 - Installation
   - Installing to /lib/modules/3.10.0-693.el7.x86_64/extra/

nvidia-uvm.ko.xz:
Running module version sanity check.
 - Original module
   - This kernel never originally had a module by this name
 - Installation
   - Installing to /lib/modules/3.10.0-693.el7.x86_64/extra/

nvidia-modeset.ko.xz:
Running module version sanity check.
 - Original module
   - This kernel never originally had a module by this name
 - Installation
   - Installing to /lib/modules/3.10.0-693.el7.x86_64/extra/

nvidia-drm.ko.xz:
Running module version sanity check.
 - Original module
   - This kernel never originally had a module by this name
 - Installation
   - Installing to /lib/modules/3.10.0-693.el7.x86_64/extra/
Adding any weak-modules

depmod...

DKMS: install completed.

-------- Uninstall Beginning --------
Module:  nvidia
Version: 390.30
Kernel:  3.10.0-693.el7.x86_64 (x86_64)
-------------------------------------

Status: This module version was INACTIVE for this kernel.
depmod...

DKMS: uninstall completed.

------------------------------
Deleting module version: 390.30
completely from the DKMS tree.
------------------------------
Done.
  Cleanup    : 1:nvidia-kmod-390.30-2.el7.x86_64                                                                                               2/2 
Excluding 1 update due to versionlock (use "yum versionlock status" to show it)
  Verifying  : 1:nvidia-kmod-390.116-2.el7.x86_64                                                                                              1/2 
  Verifying  : 1:nvidia-kmod-390.30-2.el7.x86_64                                                                                               2/2 

Updated:
  nvidia-kmod.x86_64 1:390.116-2.el7                            
---------------------------------------------------------------------------------------------------------

```

### Issue Continued After Reboot: 

```
[biohpcadmin@biohpcwsc037 ~]$ dmesg | grep -i NVRM
[    5.839031] NVRM: loading NVIDIA UNIX x86_64 Kernel Module  390.116  Sun Jan 27 07:21:36 PST 2019 (using threaded interrupts)
[  127.559991] NVRM: API mismatch: the client has the version 390.30, but
NVRM: this kernel module has the version 390.116.  Please
NVRM: make sure that this kernel module and all NVIDIA driver
NVRM: components have the same version.
[  136.007772] NVRM: API mismatch: the client has the version 390.30, but
NVRM: this kernel module has the version 390.116.  Please
NVRM: make sure that this kernel module and all NVIDIA driver
NVRM: components have the same version.

### ISSUE FOUND ###

[biohpcadmin@biohpcwsc037 ~]$ rpm -qa | grep -i nvidia
xorg-x11-drv-nvidia-390.30-1.el7.x86_64
xorg-x11-drv-nvidia-devel-390.30-1.el7.x86_64
xorg-x11-drv-nvidia-gl-390.30-1.el7.x86_64
nvidia-kmod-390.116-2.el7.x86_64
xorg-x11-drv-nvidia-libs-390.30-1.el7.x86_64 


### Download & Update Packages Above

yumdownloader xorg-x11-drv-nvidia-libs-390.116-1.el7.x86_64
yumdownloader xorg-x11-drv-nvidia-gl-390.116-1.el7.x86_64
yumdownloader xorg-x11-drv-nvidia-devel-390.116-1.el7.x86_64 
yumdownloader xorg-x11-drv-nvidia-390.116-1.el7.x86_64

yum localinstall xorg-x11-drv-nvidia-390.116-1.el7.x86_64.rpm xorg-x11-drv-nvidia-devel-390.116-1.el7.x86_64.rpm xorg-x11-drv-nvidia-gl-390.116-1.el7.x86_64.rpm xorg-x11-drv-nvidia-libs-390.116-1.el7.x86_64.rpm

---------------------------------------------------------------------------------------------------------
Examining xorg-x11-drv-nvidia-390.116-1.el7.x86_64.rpm: 1:xorg-x11-drv-nvidia-390.116-1.el7.x86_64
Marking xorg-x11-drv-nvidia-390.116-1.el7.x86_64.rpm as an update to 1:xorg-x11-drv-nvidia-390.30-1.el7.x86_64
Examining xorg-x11-drv-nvidia-devel-390.116-1.el7.x86_64.rpm: 1:xorg-x11-drv-nvidia-devel-390.116-1.el7.x86_64
Marking xorg-x11-drv-nvidia-devel-390.116-1.el7.x86_64.rpm as an update to 1:xorg-x11-drv-nvidia-devel-390.30-1.el7.x86_64
Examining xorg-x11-drv-nvidia-gl-390.116-1.el7.x86_64.rpm: 1:xorg-x11-drv-nvidia-gl-390.116-1.el7.x86_64
Marking xorg-x11-drv-nvidia-gl-390.116-1.el7.x86_64.rpm as an update to 1:xorg-x11-drv-nvidia-gl-390.30-1.el7.x86_64
Examining xorg-x11-drv-nvidia-libs-390.116-1.el7.x86_64.rpm: 1:xorg-x11-drv-nvidia-libs-390.116-1.el7.x86_64
Marking xorg-x11-drv-nvidia-libs-390.116-1.el7.x86_64.rpm as an update to 1:xorg-x11-drv-nvidia-libs-390.30-1.el7.x86_64
Resolving Dependencies
--> Running transaction check
---> Package xorg-x11-drv-nvidia.x86_64 1:390.30-1.el7 will be updated
---> Package xorg-x11-drv-nvidia.x86_64 1:390.116-1.el7 will be an update
---> Package xorg-x11-drv-nvidia-devel.x86_64 1:390.30-1.el7 will be updated
---> Package xorg-x11-drv-nvidia-devel.x86_64 1:390.116-1.el7 will be an update
---> Package xorg-x11-drv-nvidia-gl.x86_64 1:390.30-1.el7 will be updated
---> Package xorg-x11-drv-nvidia-gl.x86_64 1:390.116-1.el7 will be an update
---> Package xorg-x11-drv-nvidia-libs.x86_64 1:390.30-1.el7 will be updated
---> Package xorg-x11-drv-nvidia-libs.x86_64 1:390.116-1.el7 will be an update
--> Finished Dependency Resolution

Dependencies Resolved

===================================================================================================================================================
 Package                             Arch             Version                      Repository                                                 Size
===================================================================================================================================================
Updating:
 xorg-x11-drv-nvidia                 x86_64           1:390.116-1.el7              /xorg-x11-drv-nvidia-390.116-1.el7.x86_64                  10 M
 xorg-x11-drv-nvidia-devel           x86_64           1:390.116-1.el7              /xorg-x11-drv-nvidia-devel-390.116-1.el7.x86_64           579 k
 xorg-x11-drv-nvidia-gl              x86_64           1:390.116-1.el7              /xorg-x11-drv-nvidia-gl-390.116-1.el7.x86_64               72 M
 xorg-x11-drv-nvidia-libs            x86_64           1:390.116-1.el7              /xorg-x11-drv-nvidia-libs-390.116-1.el7.x86_64             88 M

Transaction Summary
===================================================================================================================================================
Upgrade  4 Packages

Total size: 172 M
Is this ok [y/d/N]: y
Downloading packages:
Running transaction check
Running transaction test
Transaction test succeeded
Running transaction
  Updating   : 1:xorg-x11-drv-nvidia-390.116-1.el7.x86_64                                                                                      1/8 
  Updating   : 1:xorg-x11-drv-nvidia-libs-390.116-1.el7.x86_64                                                                                 2/8 
  Updating   : 1:xorg-x11-drv-nvidia-gl-390.116-1.el7.x86_64                                                                                   3/8 
  Updating   : 1:xorg-x11-drv-nvidia-devel-390.116-1.el7.x86_64                                                                                4/8 
  Cleanup    : 1:xorg-x11-drv-nvidia-devel-390.30-1.el7.x86_64                                                                                 5/8 
  Cleanup    : 1:xorg-x11-drv-nvidia-gl-390.30-1.el7.x86_64                                                                                    6/8 
  Cleanup    : 1:xorg-x11-drv-nvidia-libs-390.30-1.el7.x86_64                                                                                  7/8 
  Cleanup    : 1:xorg-x11-drv-nvidia-390.30-1.el7.x86_64                                                                                       8/8 
Excluding 1 update due to versionlock (use "yum versionlock status" to show it)
  Verifying  : 1:xorg-x11-drv-nvidia-gl-390.116-1.el7.x86_64                                                                                   1/8 
  Verifying  : 1:xorg-x11-drv-nvidia-libs-390.116-1.el7.x86_64                                                                                 2/8 
  Verifying  : 1:xorg-x11-drv-nvidia-devel-390.116-1.el7.x86_64                                                                                3/8 
  Verifying  : 1:xorg-x11-drv-nvidia-390.116-1.el7.x86_64                                                                                      4/8 
  Verifying  : 1:xorg-x11-drv-nvidia-libs-390.30-1.el7.x86_64                                                                                  5/8 
  Verifying  : 1:xorg-x11-drv-nvidia-390.30-1.el7.x86_64                                                                                       6/8 
  Verifying  : 1:xorg-x11-drv-nvidia-devel-390.30-1.el7.x86_64                                                                                 7/8 
  Verifying  : 1:xorg-x11-drv-nvidia-gl-390.30-1.el7.x86_64                                                                                    8/8 

Updated:
  xorg-x11-drv-nvidia.x86_64 1:390.116-1.el7       xorg-x11-drv-nvidia-devel.x86_64 1:390.116-1.el7  xorg-x11-drv-nvidia-gl.x86_64 1:390.116-1.el7 
  xorg-x11-drv-nvidia-libs.x86_64 1:390.116-1.el7 

---------------------------------------------------------------------------------------------------------


### UPON REBOOT, STILL NO DISPLAY: although NVRM Warnings Gone ### 

# /var/log/messages:

--------------------------------------------------------------------------------------------------------

biohpcwsc037 ModemManager[1206]: <warn>  Couldn't create modem for device at '/sys/devices/pci0000:00/0000:00:16.3': Failed to find primary AT port

Nov  5 13:59:58 biohpcwsc037 gnome-session: gnome-session-binary[2847]: WARNING: Application 'org.gnome.Shell.desktop' killed by signal 11
Nov  5 13:59:58 biohpcwsc037 gnome-session-binary[2847]: WARNING: Application 'org.gnome.Shell.desktop' killed by signal 11
Nov  5 13:59:58 biohpcwsc037 abrtd: Size of '/var/spool/abrt' >= 1000 MB (MaxCrashReportsSize), deleting old directory 'ccpp-2019-08-05-10:54:30-16143'
Nov  5 13:59:58 biohpcwsc037 kernel: gnome-shell[2926]: segfault at 0 ip 00007f92b7078ef4 sp 00007ffec6724800 error 4 in libgdk-3.so.0.2200.10[7f92b704e000+b1000]
Nov  5 13:59:58 biohpcwsc037 abrt-hook-ccpp: Process 2926 (gnome-shell) of user 42 killed by SIGSEGV - ignoring (repeated crash)
Nov  5 13:59:58 biohpcwsc037 gnome-session: gnome-session-binary[2847]: WARNING: Application 'org.gnome.Shell.desktop' killed by signal 11
Nov  5 13:59:58 biohpcwsc037 gnome-session-binary[2847]: WARNING: Application 'org.gnome.Shell.desktop' killed by signal 11
Nov  5 13:59:58 biohpcwsc037 gnome-session-binary: Unrecoverable failure in required component org.gnome.Shell.desktop
Nov  5 13:59:58 biohpcwsc037 gnome-session: gnome-session-binary[2847]: WARNING: App 'org.gnome.Shell.desktop' respawning too quickly
Nov  5 13:59:58 biohpcwsc037 gnome-session-binary[2847]: WARNING: App 'org.gnome.Shell.desktop' respawning too quickly
Nov  5 13:59:58 biohpcwsc037 spice-vdagent[2939]: Cannot access vdagent virtio channel /dev/virtio-ports/com.redhat.spice.0
Nov  5 13:59:58 biohpcwsc037 gnome-session: gnome-session-binary[2847]: WARNING: App 'spice-vdagent.desktop' exited with code 1
Nov  5 13:59:58 biohpcwsc037 gnome-session-binary[2847]: WARNING: App 'spice-vdagent.desktop' exited with code 1


Nov  5 14:01:21 biohpcwsc037 gdm: GLib: g_hash_table_find: assertion 'version == hash_table->version' failed
Nov  5 14:01:21 biohpcwsc037 systemd: Starting GNOME Display Manager...
Nov  5 14:01:21 biohpcwsc037 systemd: Started GNOME Display Manager.
Nov  5 14:01:22 biohpcwsc037 systemd-logind: New session c2 of user gdm.
Nov  5 14:01:22 biohpcwsc037 systemd: Started Session c2 of user gdm.
Nov  5 14:01:22 biohpcwsc037 systemd: Starting Session c2 of user gdm.
Nov  5 14:01:22 biohpcwsc037 org.a11y.Bus: Activating service name='org.a11y.atspi.Registry'
Nov  5 14:01:22 biohpcwsc037 org.a11y.Bus: Successfully activated service 'org.a11y.atspi.Registry'
Nov  5 14:01:22 biohpcwsc037 org.a11y.atspi.Registry: SpiRegistry daemon is running with well-known name - org.a11y.atspi.Registry
Nov  5 14:01:22 biohpcwsc037 gnome-session: generating cookie with syscall
Nov  5 14:01:22 biohpcwsc037 gnome-session: generating cookie with syscall
Nov  5 14:01:22 biohpcwsc037 gnome-session: generating cookie with syscall
Nov  5 14:01:22 biohpcwsc037 gnome-session: generating cookie with syscall
Nov  5 14:01:22 biohpcwsc037 dbus[1249]: [system] Activating service name='org.fedoraproject.Setroubleshootd' (using servicehelper)
Nov  5 14:01:22 biohpcwsc037 kernel: gnome-shell[4542]: segfault at 0 ip 00007f9d5a8f7ef4 sp 00007ffe62806700 error 4 in libgdk-3.so.0.2200.10[7f9d5a8cd000+b1000]
Nov  5 14:01:22 biohpcwsc037 abrt-hook-ccpp: Process 4542 (gnome-shell) of user 42 killed by SIGSEGV - dumping core
Nov  5 14:01:23 biohpcwsc037 gnome-session: gnome-session-binary[4504]: WARNING: Application 'org.gnome.Shell.desktop' killed by signal 11
Nov  5 14:01:23 biohpcwsc037 gnome-session-binary[4504]: WARNING: Application 'org.gnome.Shell.desktop' killed by signal 11
Nov  5 14:01:23 biohpcwsc037 dbus[1249]: [system] Successfully activated service 'org.fedoraproject.Setroubleshootd'
Nov  5 14:01:23 biohpcwsc037 kernel: gnome-shell[4557]: segfault at 0 ip 00007f514b140ef4 sp 00007ffcbefc2c50 error 4 in libgdk-3.so.0.2200.10[7f514b116000+b1000]
Nov  5 14:01:23 biohpcwsc037 abrt-hook-ccpp: Process 4557 (gnome-shell) of user 42 killed by SIGSEGV - ignoring (repeated crash)
Nov  5 14:01:23 biohpcwsc037 gnome-session: gnome-session-binary[4504]: WARNING: Application 'org.gnome.Shell.desktop' killed by signal 11
Nov  5 14:01:23 biohpcwsc037 gnome-session-binary[4504]: WARNING: Application 'org.gnome.Shell.desktop' killed by signal 11
Nov  5 14:01:23 biohpcwsc037 gnome-session: gnome-session-binary[4504]: WARNING: App 'org.gnome.Shell.desktop' respawning too quickly
Nov  5 14:01:23 biohpcwsc037 gnome-session-binary[4504]: WARNING: App 'org.gnome.Shell.desktop' respawning too quickly
Nov  5 14:01:23 biohpcwsc037 gnome-session-binary: Unrecoverable failure in required component org.gnome.Shell.desktop
Nov  5 14:01:23 biohpcwsc037 spice-vdagent[4569]: Cannot access vdagent virtio channel /dev/virtio-ports/com.redhat.spice.0
Nov  5 14:01:23 biohpcwsc037 gnome-session: gnome-session-binary[4504]: WARNING: App 'spice-vdagent.desktop' exited with code 1
Nov  5 14:01:23 biohpcwsc037 gnome-session-binary[4504]: WARNING: App 'spice-vdagent.desktop' exited with code 1
Nov  5 14:01:23 biohpcwsc037 journal: Failed to get current display configuration state: GDBus.Error:org.freedesktop.DBus.Error.NameHasNoOwner: Name "org.gnome.Mutter.DisplayConfig" does not exist

--------------------------------------------------------------------------------------------------------


### Try Running minimal update (not security) 

--------------------------------------------------------------------------------------------------------
### Multi-lib conflicts:

Error:  Multilib version problems found. This often means that the root
       cause is something else and multilib version checking is just
       pointing out that there is a problem. Eg.:
       
         1. You have an upgrade for zlib which is missing some
            dependency that another package requires. Yum is trying to
            solve this by installing an older version of zlib of the
            different architecture. If you exclude the bad architecture
            yum will tell you what the root cause is (which package
            requires what). You can try redoing the upgrade with
            --exclude zlib.otherarch ... this should give you an error
            message showing the root cause of the problem.
       
         2. You have multiple architectures of zlib installed, but
            yum can only see an upgrade for one of those architectures.
            If you don't want/need both architectures anymore then you
            can remove the one with the missing update and everything
            will work.
       
         3. You have duplicate versions of zlib installed already.
            You can use "yum check" to get yum show these errors.
       
       ...you can also use --setopt=protected_multilib=false to remove
       this checking, however this is almost never the correct thing to
       do as something else is very likely to go wrong (often causing
       much more problems).
       
       Protected multilib versions: zlib-1.2.7-18.el7.x86_64 != zlib-1.2.7-17.el7.i686
[root@biohpcwsc037 biohpcadmin]# yum list installed | grep -i zlib
perl-Compress-Raw-Zlib.x86_64      1:2.061-4.el7           @anaconda/7.4        
perl-IO-Zlib.noarch                1:1.10-292.el7          @anaconda/7.4        
zlib.x86_64                        1.2.7-17.el7            @anaconda/7.4        
zlib-devel.x86_64                  1.2.7-17.el7            @rhel-x86_64-server-7
[root@biohpcwsc037 biohpcadmin]# 

### TRACE THE DEPENDENCIES ###

---> Package gstreamer1-plugins-ugly-free.x86_64 0:1.10.4-3.el7 will be installed
--> Processing Dependency: libmpg123.so.0()(64bit) for package: gstreamer1-plugins-ugly-free-1.10.4-3.el7.x86_64
---> Package gtk3.x86_64 0:3.22.10-4.el7 will be updated
---> Package gtk3.x86_64 0:3.22.30-3.el7 will be an update
---> Package gtk3-immodule-xim.x86_64 0:3.22.10-4.el7 will be updated
---> Package gtk3-immodule-xim.x86_64 0:3.22.30-3.el7 will be an update
---> Package libblockdev.x86_64 0:2.18-4.el7 will be installed
---> Package libblockdev-crypto.x86_64 0:2.18-4.el7 will be installed
--> Processing Dependency: libcryptsetup.so.12(CRYPTSETUP_2.0)(64bit) for package: libblockdev-crypto-2.18-4.el7.x86_64
--> Processing Dependency: libcryptsetup.so.12()(64bit) for package: libblockdev-crypto-2.18-4.el7.x86_64
--> Processing Dependency: libvolume_key.so.1()(64bit) for package: libblockdev-crypto-2.18-4.el7.x86_64
---> Package libblockdev-fs.x86_64 0:2.18-4.el7 will be installed
---> Package libblockdev-loop.x86_64 0:2.18-4.el7 will be installed
---> Package libblockdev-mdraid.x86_64 0:2.18-4.el7 will be installed
--> Processing Dependency: libbytesize.so.1()(64bit) for package: libblockdev-mdraid-2.18-4.el7.x86_64
---> Package libblockdev-part.x86_64 0:2.18-4.el7 will be installed
---> Package libblockdev-swap.x86_64 0:2.18-4.el7 will be installed
---> Package libblockdev-utils.x86_64 0:2.18-4.el7 will be installed
---> Package libglvnd-devel.x86_64 1:1.0.1-0.8.git5baa1e5.el7 will be installed
--> Processing Dependency: libglvnd-core-devel(x86-64) = 1:1.0.1-0.8.git5baa1e5.el7 for package: 1:libglvnd-devel-1.0.1-0.8.git5baa1e5.el7.x86_64
--> Processing Dependency: libglvnd-opengl(x86-64) = 1:1.0.1-0.8.git5baa1e5.el7 for package: 1:libglvnd-devel-1.0.1-0.8.git5baa1e5.el7.x86_64
--> Processing Dependency: libglvnd(x86-64) = 1:1.0.1-0.8.git5baa1e5.el7 for package: 1:libglvnd-devel-1.0.1-0.8.git5baa1e5.el7.x86_64
---> Package libglvnd-egl.x86_64 1:1.0.1-0.8.git5baa1e5.el7 will be installed
---> Package libglvnd-gles.x86_64 1:1.0.1-0.8.git5baa1e5.el7 will be installed
---> Package libglvnd-glx.x86_64 1:1.0.1-0.8.git5baa1e5.el7 will be installed
---> Package libuuid-devel.x86_64 0:2.23.2-61.el7 will be installed
--> Processing Dependency: libuuid = 2.23.2-61.el7 for package: libuuid-devel-2.23.2-61.el7.x86_64
---> Package libwayland-cursor.x86_64 0:1.15.0-1.el7 will be installed
---> Package llvm-private.x86_64 0:5.0.0-3.el7 will be installed
---> Package nss-util.x86_64 0:3.36.0-1.el7_5 will be updated
---> Package nss-util.x86_64 0:3.44.0-3.el7 will be an update
---> Package perl-Mozilla-CA.noarch 0:20130114-5.el7 will be installed
---> Package unbound-libs.x86_64 0:1.6.6-1.el7 will be installed
---> Package zlib.i686 0:1.2.7-17.el7 will be installed
--> Processing Dependency: libc.so.6(GLIBC_2.1.3) for package: zlib-1.2.7-17.el7.i686
--> Processing Dependency: libc.so.6(GLIBC_2.4) for package: zlib-1.2.7-17.el7.i686
--> Processing Dependency: libc.so.6(GLIBC_2.1) for package: zlib-1.2.7-17.el7.i686
--> Processing Dependency: libc.so.6 for package: zlib-1.2.7-17.el7.i686
--> Processing Dependency: libc.so.6(GLIBC_2.0) for package: zlib-1.2.7-17.el7.i686
--> Processing Dependency: libc.so.6(GLIBC_2.3.4) for package: zlib-1.2.7-17.el7.i686
---> Package zlib.x86_64 0:1.2.7-17.el7 will be updated



### Solution: Exclude it..? 

yum --exclude=virtualbox*,puppet*,zlib* update-minimal

--------------------------------------------------------------------------------------------------------

### Update RESULT: 

gnome-dictionary.x86_64 0:3.26.1-2.el7                                         grub2.x86_64 1:2.02-0.65.el7_4.2                                 
  grub2-tools.x86_64 1:2.02-0.65.el7_4.2                                         grub2-tools-extra.x86_64 1:2.02-0.65.el7_4.2                     
  grub2-tools-minimal.x86_64 1:2.02-0.65.el7_4.2                                 subscription-manager-rhsm.x86_64 0:1.24.13-3.el7_7               
  subscription-manager-rhsm-certificates.x86_64 0:1.24.13-3.el7_7               

Dependency Installed:
  bind-export-libs.x86_64 32:9.11.4-9.P2.el7                             boost-iostreams.x86_64 0:1.53.0-27.el7                                   
  boost-random.x86_64 0:1.53.0-27.el7                                    fribidi.x86_64 0:1.0.2-1.el7                                             
  fribidi-devel.x86_64 0:1.0.2-1.el7                                     gnome-shell-extension-top-icons.noarch 0:3.26.2-3.el7                    
  gstreamer1-plugins-ugly-free.x86_64 0:1.10.4-3.el7                     libblockdev.x86_64 0:2.18-4.el7                                          
  libblockdev-crypto.x86_64 0:2.18-4.el7                                 libblockdev-fs.x86_64 0:2.18-4.el7                                       
  libblockdev-loop.x86_64 0:2.18-4.el7                                   libblockdev-mdraid.x86_64 0:2.18-4.el7                                   
  libblockdev-part.x86_64 0:2.18-4.el7                                   libblockdev-swap.x86_64 0:2.18-4.el7                                     
  libblockdev-utils.x86_64 0:2.18-4.el7                                  libbytesize.x86_64 0:1.2-1.el7                                           
  libglvnd.x86_64 1:1.0.1-0.8.git5baa1e5.el7                             libglvnd-core-devel.x86_64 1:1.0.1-0.8.git5baa1e5.el7                    
  libglvnd-devel.x86_64 1:1.0.1-0.8.git5baa1e5.el7                       libglvnd-egl.x86_64 1:1.0.1-0.8.git5baa1e5.el7                           
  libglvnd-gles.x86_64 1:1.0.1-0.8.git5baa1e5.el7                        libglvnd-glx.x86_64 1:1.0.1-0.8.git5baa1e5.el7                           
  libglvnd-opengl.x86_64 1:1.0.1-0.8.git5baa1e5.el7                      libsmartcols.x86_64 0:2.23.2-61.el7                                      
  libuuid-devel.x86_64 0:2.23.2-61.el7                                   libwayland-cursor.x86_64 0:1.15.0-1.el7                                  
  llvm-private.x86_64 0:5.0.0-3.el7                                      mpg123-libs.x86_64 0:1.25.6-1.el7                                        
  perl-Mozilla-CA.noarch 0:20130114-5.el7                                unbound-libs.x86_64 0:1.6.6-1.el7                                        
  volume_key-libs.x86_64 0:0.3.9-9.el7                                  

Updated:
  GeoIP.x86_64 0:1.5.0-13.el7                                            ImageMagick.x86_64 0:6.7.8.9-16.el7_6                                   
  ModemManager.x86_64 0:1.6.10-1.el7                                     ModemManager-glib.x86_64 0:1.6.10-1.el7                                 
  NetworkManager.x86_64 1:1.8.0-11.el7_4                                 NetworkManager-adsl.x86_64 1:1.8.0-11.el7_4                             
  NetworkManager-config-server.noarch 1:1.8.0-11.el7_4                   NetworkManager-glib.x86_64 1:1.8.0-11.el7_4                             
  NetworkManager-libnm.x86_64 1:1.8.0-11.el7_4                           NetworkManager-ppp.x86_64 1:1.8.0-11.el7_4                              
  NetworkManager-team.x86_64 1:1.8.0-11.el7_4                            NetworkManager-tui.x86_64 1:1.8.0-11.el7_4                              
  NetworkManager-wifi.x86_64 1:1.8.0-11.el7_4                            PackageKit.x86_64 0:1.1.10-1.el7                                        
  PackageKit-command-not-found.x86_64 0:1.1.10-1.el7                     PackageKit-glib.x86_64 0:1.1.10-1.el7                                   
  PackageKit-gstreamer-plugin.x86_64 0:1.1.10-1.el7                      PackageKit-gtk3-module.x86_64 0:1.1.10-1.el7                            
  PackageKit-yum.x86_64 0:1.1.10-1.el7                                   abrt.x86_64 0:2.1.11-50.el7                                             
  abrt-addon-ccpp.x86_64 0:2.1.11-50.el7                                 abrt-addon-kerneloops.x86_64 0:2.1.11-50.el7                            
  abrt-addon-pstoreoops.x86_64 0:2.1.11-50.el7                           abrt-addon-python.x86_64 0:2.1.11-50.el7                                
  abrt-addon-vmcore.x86_64 0:2.1.11-50.el7                               abrt-addon-xorg.x86_64 0:2.1.11-50.el7                                  
  abrt-cli.x86_64 0:2.1.11-50.el7                                        abrt-console-notification.x86_64 0:2.1.11-50.el7                        
  abrt-dbus.x86_64 0:2.1.11-50.el7                                       abrt-desktop.x86_64 0:2.1.11-50.el7                                     
  abrt-gui.x86_64 0:2.1.11-50.el7                                        abrt-gui-libs.x86_64 0:2.1.11-50.el7                                    
  abrt-libs.x86_64 0:2.1.11-50.el7                                       abrt-python.x86_64 0:2.1.11-50.el7                                      
  abrt-tui.x86_64 0:2.1.11-50.el7                                        accountsservice.x86_64 0:0.6.45-3.el7_4.1                               
  accountsservice-libs.x86_64 0:0.6.45-3.el7_4.1                         acl.x86_64 0:2.2.51-14.el7                                              
  adcli.x86_64 0:0.8.1-4.el7                                             adwaita-cursor-theme.noarch 0:3.26.0-1.el7                              
  adwaita-gtk2-theme.x86_64 0:3.22.2-2.el7_5                             adwaita-icon-theme.noarch 0:3.26.0-1.el7                                
  alsa-plugins-pulseaudio.x86_64 0:1.1.6-1.el7                           alsa-utils.x86_64 0:1.1.6-1.el7                                         
  anaconda-core.x86_64 0:21.48.22.134-1.el7                              anaconda-gui.x86_64 0:21.48.22.134-1.el7                                
  anaconda-tui.x86_64 0:21.48.22.134-1.el7                               anaconda-user-help.noarch 1:7.5.3-1.el7                                 
  anaconda-widgets.x86_64 0:21.48.22.134-1.el7                           appstream-data.noarch 0:7-20180614.el7                                  
  apr.x86_64 0:1.4.8-5.el7                                               at.x86_64 0:3.1.13-22.el7_4.2                                           
  at-spi2-atk.x86_64 0:2.26.2-1.el7                                      at-spi2-core.x86_64 0:2.28.0-1.el7                                      
  atk.x86_64 0:2.28.1-1.el7                                              atk-devel.x86_64 0:2.28.1-1.el7                                         
  attr.x86_64 0:2.4.46-13.el7                                            audit.x86_64 0:2.8.1-3.el7                                              
  audit-libs.x86_64 0:2.8.1-3.el7                                        audit-libs-python.x86_64 0:2.8.1-3.el7                                  
  augeas-libs.x86_64 0:1.4.0-5.el7_5.1                                   autofs.x86_64 1:5.0.7-70.el7_4                                          
  avahi.x86_64 0:0.6.31-19.el7                                           avahi-glib.x86_64 0:0.6.31-19.el7                                       
  avahi-gobject.x86_64 0:0.6.31-19.el7                                   avahi-libs.x86_64 0:0.6.31-19.el7                                       
  avahi-ui-gtk3.x86_64 0:0.6.31-19.el7                                   baobab.x86_64 0:3.28.0-2.el7                                            
  bash.x86_64 0:4.2.46-29.el7_4                                          bind-libs.x86_64 32:9.9.4-72.el7                                        
  bind-libs-lite.x86_64 32:9.9.4-72.el7                                  bind-license.noarch 32:9.9.4-72.el7                                     
  bind-utils.x86_64 32:9.9.4-72.el7                                      binutils.x86_64 0:2.25.1-32.base.el7_4.1                                
  biosdevname.x86_64 0:0.7.3-1.el7                                       bison.x86_64 0:3.0.4-2.el7                                              
  blktrace.x86_64 0:1.0.5-9.el7                                          brlapi.x86_64 0:0.6.0-16.el7                                            
  brltty.x86_64 0:4.5-16.el7                                             ca-certificates.noarch 0:2017.2.20-71.el7                               
  cairo.x86_64 0:1.15.12-3.el7                                           cairo-devel.x86_64 0:1.15.12-3.el7                                      
  cairo-gobject.x86_64 0:1.15.12-3.el7                                   certmonger.x86_64 0:0.78.4-3.el7_5.1                                    
  checkpolicy.x86_64 0:2.5-6.el7                                         cheese.x86_64 2:3.22.1-2.el7                                            
  cheese-libs.x86_64 2:3.22.1-2.el7                                      chromium.x86_64 0:77.0.3865.120-1.el7                                   
  chromium-common.x86_64 0:77.0.3865.120-1.el7                           chromium-libs.x86_64 0:77.0.3865.120-1.el7                              
  chromium-libs-media.x86_64 0:77.0.3865.120-1.el7                       chrony.x86_64 0:3.2-2.el7                                               
  clutter.x86_64 0:1.26.2-2.el7                                          clutter-gst3.x86_64 0:3.0.26-1.el7                                      
  clutter-gtk.x86_64 0:1.8.4-1.el7                                       cogl.x86_64 0:1.22.2-2.el7                                              
  compat-libtiff3.x86_64 0:3.9.4-12.el7                                  control-center.x86_64 1:3.26.2-8.el7                                    
  control-center-filesystem.x86_64 1:3.26.2-8.el7                        coreutils.x86_64 0:8.22-21.el7                                          
  cpio.x86_64 0:2.11-25.el7_4                                            cpp.x86_64 0:4.8.5-28.el7_5.1                                           
  crash.x86_64 0:7.2.0-6.el7                                             crash-trace-command.x86_64 0:2.0-13.el7                                 
  crda.x86_64 0:3.18_2018.05.31-4.el7                                    cronie.x86_64 0:1.4.11-19.el7                                           
  cronie-anacron.x86_64 0:1.4.11-19.el7                                  cryptsetup.x86_64 0:2.0.3-5.el7                                         
  cryptsetup-python.x86_64 0:2.0.3-5.el7                                 cups.x86_64 1:1.6.3-35.el7                                              
  cups-client.x86_64 1:1.6.3-35.el7                                      cups-filesystem.noarch 1:1.6.3-35.el7                                   
  cups-filters.x86_64 0:1.0.35-26.el7                                    cups-filters-libs.x86_64 0:1.0.35-26.el7                                
  cups-libs.x86_64 1:1.6.3-35.el7                                        curl.x86_64 0:7.29.0-51.el7                                             
  cyrus-sasl.x86_64 0:2.1.26-23.el7                                      cyrus-sasl-gssapi.x86_64 0:2.1.26-23.el7                                
  cyrus-sasl-lib.x86_64 0:2.1.26-23.el7                                  cyrus-sasl-md5.x86_64 0:2.1.26-23.el7                                   
  cyrus-sasl-plain.x86_64 0:2.1.26-23.el7                                cyrus-sasl-scram.x86_64 0:2.1.26-23.el7                                 
  dbus.x86_64 1:1.10.24-12.el7                                           dbus-devel.x86_64 1:1.10.24-12.el7                                      
  dbus-libs.x86_64 1:1.10.24-12.el7                                      dbus-x11.x86_64 1:1.10.24-12.el7                                        
  dconf.x86_64 0:0.26.0-3.el7_5.1                                        desktop-file-utils.x86_64 0:0.23-2.el7                                  
  device-mapper.x86_64 7:1.02.146-4.el7                                  device-mapper-event.x86_64 7:1.02.146-4.el7                             
  device-mapper-event-libs.x86_64 7:1.02.146-4.el7                       device-mapper-libs.x86_64 7:1.02.146-4.el7                              
  device-mapper-multipath.x86_64 0:0.4.9-111.el7_4.2                     device-mapper-multipath-libs.x86_64 0:0.4.9-111.el7_4.2                 
  device-mapper-persistent-data.x86_64 0:0.7.0-0.1.rc6.el7_4.1           dhclient.x86_64 12:4.2.5-77.el7                                         
  dhcp-common.x86_64 12:4.2.5-77.el7                                     dhcp-libs.x86_64 12:4.2.5-77.el7                                        
  diffutils.x86_64 0:3.3-5.el7                                           dkms.noarch 0:2.7.1-1.el7                                               
  dleyna-server.x86_64 0:0.5.0-2.el7_4                                   dmidecode.x86_64 1:3.1-2.el7                                            
  dnsmasq.x86_64 0:2.76-7.el7                                            dosfstools.x86_64 0:3.0.20-10.el7                                       
  dracut.x86_64 0:033-502.el7_4.1                                        dracut-config-rescue.x86_64 0:033-502.el7_4.1                           
  dracut-network.x86_64 0:033-502.el7_4.1                                dyninst.x86_64 0:9.3.1-2.el7                                            
  e2fsprogs.x86_64 0:1.42.9-11.el7                                       e2fsprogs-libs.x86_64 0:1.42.9-11.el7                                   
  ebtables.x86_64 0:2.0.10-16.el7                                        efibootmgr.x86_64 0:17-2.el7                                            
  efivar-libs.x86_64 0:36-11.el7                                         elfutils.x86_64 0:0.172-2.el7                                           
  elfutils-default-yama-scope.noarch 0:0.170-4.el7                       elfutils-libelf.x86_64 0:0.172-2.el7                                    
  elfutils-libelf-devel.x86_64 0:0.172-2.el7                             elfutils-libs.x86_64 0:0.172-2.el7                                      
  emacs.x86_64 1:24.3-22.el7                                             emacs-common.x86_64 1:24.3-22.el7                                       
  emacs-filesystem.noarch 1:24.3-22.el7                                  empathy.x86_64 0:3.12.13-1.el7                                          
  enscript.x86_64 0:1.6.6-7.el7                                          eog.x86_64 0:3.28.3-1.el7                                               
  epel-release.noarch 0:7-12                                             ethtool.x86_64 2:4.8-7.el7                                              
  evince.x86_64 0:3.28.2-5.el7                                           evince-libs.x86_64 0:3.28.2-5.el7                                       
  evince-nautilus.x86_64 0:3.28.2-5.el7                                  exempi.x86_64 0:2.2.0-9.el7                                             
  exiv2-libs.x86_64 0:0.26-3.el7                                         fcoe-utils.x86_64 0:1.0.32-2.el7                                        
  file.x86_64 0:5.11-35.el7                                              file-libs.x86_64 0:5.11-35.el7                                          
  file-roller.x86_64 0:3.28.1-2.el7                                      file-roller-nautilus.x86_64 0:3.28.1-2.el7                              
  filesystem.x86_64 0:3.2-25.el7                                         findutils.x86_64 1:4.5.11-6.el7                                         
  firefox.x86_64 0:60.2.2-2.el7_5                                        firewall-config.noarch 0:0.4.4.4-14.el7                                 
  firewalld.noarch 0:0.4.4.4-14.el7                                      firewalld-filesystem.noarch 0:0.4.4.4-14.el7                            
  flatpak-libs.x86_64 0:1.0.2-7.el7                                      flex.x86_64 0:2.5.37-6.el7                                              
  fontconfig.x86_64 0:2.13.0-4.3.el7                                     fontconfig-devel.x86_64 0:2.13.0-4.3.el7                                
  foomatic.x86_64 0:4.0.9-8.el7_6.1                                      foomatic-filters.x86_64 0:4.0.9-8.el7_6.1                               
  fprintd.x86_64 0:0.8.1-2.el7                                           fprintd-pam.x86_64 0:0.8.1-2.el7                                        
  freerdp-libs.x86_64 0:1.0.2-15.el7                                     freetype.x86_64 0:2.8-12.el7                                            
  freetype-devel.x86_64 0:2.8-12.el7                                     fros.noarch 0:1.0-5.el7                                                 
  fuse.x86_64 0:2.9.2-10.el7                                             fuse-libs.x86_64 0:2.9.2-10.el7                                         
  gcc.x86_64 0:4.8.5-28.el7_5.1                                          gcc-c++.x86_64 0:4.8.5-28.el7_5.1                                       
  gcc-gfortran.x86_64 0:4.8.5-28.el7_5.1                                 gcr.x86_64 0:3.28.0-1.el7                                               
  gdb.x86_64 0:7.6.1-100.el7_4.1                                         gdisk.x86_64 0:0.8.10-2.el7                                             
  gdk-pixbuf2.x86_64 0:2.36.12-3.el7                                     gdk-pixbuf2-devel.x86_64 0:2.36.12-3.el7                                
  gedit.x86_64 2:3.28.1-1.el7                                            genisoimage.x86_64 0:1.1.11-25.el7                                      
  geoclue2.x86_64 0:2.4.8-1.el7                                          geoclue2-libs.x86_64 0:2.4.8-1.el7                                      
  geocode-glib.x86_64 0:3.26.0-2.el7                                     ghostscript.x86_64 0:9.07-28.el7_4.2                                    
  ghostscript-cups.x86_64 0:9.07-28.el7_4.2                              git.x86_64 0:1.8.3.1-19.el7                                             
  glade-libs.x86_64 0:3.22.1-1.el7                                       glib-networking.x86_64 0:2.56.1-1.el7                                   
  glibc.x86_64 0:2.17-260.el7                                            glibc-common.x86_64 0:2.17-260.el7                                      
  glibc-devel.x86_64 0:2.17-260.el7                                      glibc-headers.x86_64 0:2.17-260.el7                                     
  glibmm24.x86_64 0:2.56.0-1.el7                                         glusterfs.x86_64 0:3.8.4-53.el7                                         
  glusterfs-api.x86_64 0:3.8.4-53.el7                                    glusterfs-cli.x86_64 0:3.8.4-53.el7                                     
  glusterfs-client-xlators.x86_64 0:3.8.4-53.el7                         glusterfs-fuse.x86_64 0:3.8.4-53.el7                                    
  glusterfs-libs.x86_64 0:3.8.4-53.el7                                   glx-utils.x86_64 0:8.3.0-10.el7                                         
  gnome-bluetooth.x86_64 1:3.28.2-1.el7                                  gnome-bluetooth-libs.x86_64 1:3.28.2-1.el7                              
  gnome-boxes.x86_64 0:3.28.5-2.el7                                      gnome-calculator.x86_64 0:3.28.2-1.el7                                  
  gnome-classic-session.noarch 0:3.26.2-3.el7                            gnome-color-manager.x86_64 0:3.22.2-2.el7                               
  gnome-disk-utility.x86_64 0:3.28.3-1.el7                               gnome-font-viewer.x86_64 0:3.28.0-1.el7                                 
  gnome-getting-started-docs.noarch 0:3.28.2-1.el7                       gnome-keyring.x86_64 0:3.28.2-1.el7                                     
  gnome-keyring-pam.x86_64 0:3.28.2-1.el7                                gnome-online-accounts.x86_64 0:3.26.2-1.el7                             
  gnome-packagekit.x86_64 0:3.28.0-1.el7                                 gnome-packagekit-common.x86_64 0:3.28.0-1.el7                           
  gnome-packagekit-installer.x86_64 0:3.28.0-1.el7                       gnome-packagekit-updater.x86_64 0:3.28.0-1.el7                          
  gnome-screenshot.x86_64 0:3.26.0-1.el7                                 gnome-shell-extension-alternate-tab.noarch 0:3.26.2-3.el7               
  gnome-shell-extension-apps-menu.noarch 0:3.26.2-3.el7                  gnome-shell-extension-common.noarch 0:3.26.2-3.el7                      
  gnome-shell-extension-launch-new-instance.noarch 0:3.26.2-3.el7        gnome-shell-extension-places-menu.noarch 0:3.26.2-3.el7                 
  gnome-shell-extension-user-theme.noarch 0:3.26.2-3.el7                 gnome-shell-extension-window-list.noarch 0:3.26.2-3.el7                 
  gnome-software.x86_64 0:3.22.7-5.el7                                   gnome-system-monitor.x86_64 0:3.22.2-3.el7                              
  gnome-terminal.x86_64 0:3.28.2-2.el7                                   gnome-terminal-nautilus.x86_64 0:3.28.2-2.el7                           
  gnome-themes-standard.x86_64 0:3.22.2-2.el7_5                          gnome-user-docs.noarch 0:3.28.2-1.el7                                   
  gnome-weather.noarch 0:3.26.0-1.el7                                    gnutls.x86_64 0:3.3.29-8.el7                                            
  gobject-introspection.x86_64 0:1.56.1-1.el7                            gom.x86_64 0:0.3.3-1.el7                                                
  gperftools-libs.x86_64 0:2.6.1-1.el7                                   gpm-libs.x86_64 0:1.20.7-6.el7                                          
  grilo.x86_64 0:0.3.6-1.el7                                             grilo-plugins.x86_64 0:0.3.4-3.el7                                      
  grub2-common.noarch 1:2.02-0.65.el7_4.2                                grub2-efi-x64.x86_64 1:2.02-0.65.el7_4.2                                
  grub2-pc.x86_64 1:2.02-0.65.el7_4.2                                    grub2-pc-modules.noarch 1:2.02-0.65.el7_4.2                             
  grubby.x86_64 0:8.28-25.el7                                            gspell.x86_64 0:1.6.1-1.el7                                             
  gssdp.x86_64 0:1.0.2-1.el7                                             gssproxy.x86_64 0:0.7.0-17.el7                                          
  gstreamer1-plugins-bad-free.x86_64 0:1.10.4-3.el7                      gstreamer1-plugins-base.x86_64 0:1.10.4-2.el7                           
  gtk-update-icon-cache.x86_64 0:3.22.10-5.el7_4                         gtk-vnc2.x86_64 0:0.7.0-3.el7                                           
  gtk3-immodule-xim.x86_64 0:3.22.30-3.el7                               gtkmm30.x86_64 0:3.22.2-1.el7                                           
  gtksourceview3.x86_64 0:3.24.8-1.el7                                   gucharmap.x86_64 0:10.0.4-1.el7                                         
  gucharmap-libs.x86_64 0:10.0.4-1.el7                                   gupnp.x86_64 0:1.0.2-5.el7                                              
  gupnp-igd.x86_64 0:0.2.5-2.el7                                         gvfs.x86_64 0:1.30.4-5.el7                                              
  gvfs-afc.x86_64 0:1.30.4-5.el7                                         gvfs-afp.x86_64 0:1.30.4-5.el7                                          
  gvfs-archive.x86_64 0:1.30.4-5.el7                                     gvfs-client.x86_64 0:1.30.4-5.el7                                       
  gvfs-fuse.x86_64 0:1.30.4-5.el7                                        gvfs-goa.x86_64 0:1.30.4-5.el7                                          
  gvfs-gphoto2.x86_64 0:1.30.4-5.el7                                     gvfs-mtp.x86_64 0:1.30.4-5.el7                                          
  gvfs-smb.x86_64 0:1.30.4-5.el7                                         gvnc.x86_64 0:0.7.0-3.el7                                               
  gzip.x86_64 0:1.5-10.el7                                               harfbuzz.x86_64 0:1.7.5-2.el7                                           
  harfbuzz-devel.x86_64 0:1.7.5-2.el7                                    harfbuzz-icu.x86_64 0:1.7.5-2.el7                                       
  http-parser.x86_64 0:2.7.1-5.el7_4                                     hwdata.x86_64 0:0.252-8.8.el7                                           
  hwloc.x86_64 0:1.11.8-4.el7                                            hwloc-libs.x86_64 0:1.11.8-4.el7                                        
  hyperv-daemons.x86_64 0:0-0.32.20161211git.el7                         hyperv-daemons-license.noarch 0:0-0.32.20161211git.el7                  
  hypervfcopyd.x86_64 0:0-0.32.20161211git.el7                           hypervkvpd.x86_64 0:0-0.32.20161211git.el7                              
  hypervvssd.x86_64 0:0-0.32.20161211git.el7                             ibus.x86_64 0:1.5.17-2.el7                                              
  ibus-gtk2.x86_64 0:1.5.17-2.el7                                        ibus-gtk3.x86_64 0:1.5.17-2.el7                                         
  ibus-hangul.x86_64 0:1.4.2-11.el7                                      ibus-libs.x86_64 0:1.5.17-2.el7                                         
  ibus-setup.noarch 0:1.5.17-2.el7                                       icedtea-web.x86_64 0:1.7.1-1.el7                                        
  imsettings.x86_64 0:1.6.3-10.el7                                       imsettings-gsettings.x86_64 0:1.6.3-10.el7                              
  imsettings-libs.x86_64 0:1.6.3-10.el7                                  info.x86_64 0:5.1-5.el7                                                 
  initial-setup.x86_64 0:0.3.9.43-1.el7                                  initial-setup-gui.x86_64 0:0.3.9.43-1.el7                               
  initscripts.x86_64 0:9.49.39-1.el7_4.1                                 iotop.noarch 0:0.6-4.el7                                                
  ipa-client-common.noarch 0:4.5.0-21.el7                                ipa-common.noarch 0:4.5.0-21.el7                                        
  iproute.x86_64 0:4.11.0-14.el7                                         iprutils.x86_64 0:2.4.15.1-1.el7                                        
  ipset.x86_64 0:6.38-2.el7                                              ipset-libs.x86_64 0:6.38-2.el7                                          
  iptables.x86_64 0:1.4.21-18.2.el7_4                                    ipxe-roms-qemu.noarch 0:20170123-1.git4e85b27.el7_4.1                   
  irqbalance.x86_64 3:1.0.7-10.el7_4.1                                   iscsi-initiator-utils.x86_64 0:6.2.0.874-7.el7                          
  iscsi-initiator-utils-iscsiuio.x86_64 0:6.2.0.874-7.el7                iw.x86_64 0:4.3-2.el7                                                   
  iwl100-firmware.noarch 0:39.31.5.1-62.1.el7_5                          iwl1000-firmware.noarch 1:39.31.5.1-62.1.el7_5                          
  iwl105-firmware.noarch 0:18.168.6.1-62.1.el7_5                         iwl135-firmware.noarch 0:18.168.6.1-62.1.el7_5                          
  iwl2000-firmware.noarch 0:18.168.6.1-62.1.el7_5                        iwl2030-firmware.noarch 0:18.168.6.1-62.1.el7_5                         
  iwl3160-firmware.noarch 0:22.0.7.0-62.1.el7_5                          iwl3945-firmware.noarch 0:15.32.2.9-62.1.el7_5                          
  iwl4965-firmware.noarch 0:228.61.2.24-62.1.el7_5                       iwl5000-firmware.noarch 0:8.83.5.1_1-62.1.el7_5                         
  iwl5150-firmware.noarch 0:8.24.2.2-62.1.el7_5                          iwl6000-firmware.noarch 0:9.221.4.1-62.1.el7_5                          
  iwl6000g2a-firmware.noarch 0:17.168.5.3-62.1.el7_5                     iwl6000g2b-firmware.noarch 0:17.168.5.2-62.1.el7_5                      
  iwl6050-firmware.noarch 0:41.28.5.1-62.1.el7_5                         iwl7260-firmware.noarch 0:22.0.7.0-62.1.el7_5                           
  iwl7265-firmware.noarch 0:22.0.7.0-62.1.el7_5                          jasper-libs.x86_64 0:1.900.1-33.el7                                     
  java-1.7.0-openjdk.x86_64 1:1.7.0.191-2.6.15.5.el7                     java-1.7.0-openjdk-headless.x86_64 1:1.7.0.191-2.6.15.5.el7             
  java-1.8.0-openjdk.x86_64 1:1.8.0.191.b12-1.el7_6                      java-1.8.0-openjdk-headless.x86_64 1:1.8.0.191.b12-1.el7_6              
  json-glib.x86_64 0:1.4.2-2.el7                                         kbd.x86_64 0:1.15.5-15.el7                                              
  kbd-legacy.noarch 0:1.15.5-15.el7                                      kbd-misc.noarch 0:1.15.5-15.el7                                         
  kexec-tools.x86_64 0:2.0.14-17.2.el7_4                                 kmod.x86_64 0:20-15.el7_4.1                                             
  kmod-libs.x86_64 0:20-15.el7_4.1                                       kpartx.x86_64 0:0.4.9-111.el7_4.2                                       
  kpatch.noarch 0:0.4.0-2.el7_4                                          krb5-libs.x86_64 0:1.15.1-34.el7                                        
  krb5-workstation.x86_64 0:1.15.1-34.el7                                langtable.noarch 0:0.0.31-4.el7                                         
  langtable-data.noarch 0:0.0.31-4.el7                                   langtable-python.noarch 0:0.0.31-4.el7                                  
  ledmon.x86_64 0:0.90-1.el7                                             libX11.x86_64 0:1.6.5-2.el7                                             
  libX11-common.noarch 0:1.6.5-2.el7                                     libX11-devel.x86_64 0:1.6.5-2.el7                                       
  libXcursor.x86_64 0:1.1.15-1.el7                                       libXcursor-devel.x86_64 0:1.1.15-1.el7                                  
  libXfont.x86_64 0:1.5.4-1.el7                                          libXfont2.x86_64 0:2.0.3-1.el7                                          
  libXres.x86_64 0:1.2.0-1.el7                                           libacl.x86_64 0:2.2.51-14.el7                                           
  libappstream-glib.x86_64 0:0.7.8-2.el7                                 libarchive.x86_64 0:3.1.2-12.el7                                        
  libattr.x86_64 0:2.4.46-13.el7                                         libbasicobjects.x86_64 0:0.1.1-29.el7                                   
  libblkid.x86_64 0:2.23.2-61.el7                                        libcap.x86_64 0:2.22-10.el7                                             
  libcdio.x86_64 0:0.92-3.el7                                            libcgroup.x86_64 0:0.41-15.el7                                          
  libcgroup-tools.x86_64 0:0.41-15.el7                                   libchamplain.x86_64 0:0.12.16-2.el7                                     
  libchamplain-gtk.x86_64 0:0.12.16-2.el7                                libcollection.x86_64 0:0.7.0-29.el7                                     
  libcom_err.x86_64 0:1.42.9-11.el7                                      libcroco.x86_64 0:0.6.12-4.el7                                          
  libcurl.x86_64 0:7.29.0-51.el7                                         libcurl-devel.x86_64 0:7.29.0-51.el7                                    
  libdb.x86_64 0:5.3.21-21.el7_4                                         libdb-devel.x86_64 0:5.3.21-21.el7_4                                    
  libdb-utils.x86_64 0:5.3.21-21.el7_4                                   libdhash.x86_64 0:0.5.0-29.el7                                          
  libdrm.x86_64 0:2.4.91-3.el7                                           libdrm-devel.x86_64 0:2.4.91-3.el7                                      
  libepoxy.x86_64 0:1.3.1-2.el7_5                                        libfastjson.x86_64 0:0.99.4-3.el7                                       
  libfprint.x86_64 0:0.8.2-1.el7                                         libgcc.x86_64 0:4.8.5-28.el7_5.1                                        
  libgdata.x86_64 0:0.17.9-1.el7                                         libgee.x86_64 0:0.20.1-1.el7                                            
  libgfortran.x86_64 0:4.8.5-28.el7_5.1                                  libgnomekbd.x86_64 0:3.26.0-1.el7                                       
  libgomp.x86_64 0:4.8.5-28.el7_5.1                                      libgovirt.x86_64 0:0.3.3-6.el7                                          
  libgphoto2.x86_64 0:2.5.15-1.el7                                       libgtop2.x86_64 0:2.34.2-2.el7                                          
  libgudev1.x86_64 0:219-57.el7_5.1                                      libgxps.x86_64 0:0.3.0-4.el7                                            
  libhugetlbfs.x86_64 0:2.16-13.el7                                      libhugetlbfs-utils.x86_64 0:2.16-13.el7                                 
  libibverbs.x86_64 0:15-6.el7                                           libicu.x86_64 0:50.1.2-17.el7                                           
  libicu-devel.x86_64 0:50.1.2-17.el7                                    libini_config.x86_64 0:1.3.1-29.el7                                     
  libipa_hbac.x86_64 0:1.16.0-19.el7_5.5                                 libjpeg-turbo.x86_64 0:1.2.90-6.el7                                     
  libjpeg-turbo-devel.x86_64 0:1.2.90-6.el7                              libkadm5.x86_64 0:1.15.1-34.el7                                         
  libldb.x86_64 0:1.3.4-1.el7                                            libmbim.x86_64 0:1.14.2-1.el7                                           
  libmbim-utils.x86_64 0:1.14.2-1.el7                                    libmediaart.x86_64 0:1.9.4-1.el7                                        
  libmount.x86_64 0:2.23.2-61.el7                                        libmspack.x86_64 0:0.5-0.6.alpha.el7                                    
  libmtp.x86_64 0:1.1.14-1.el7                                           libndp.x86_64 0:1.2-9.el7                                               
  libnfsidmap.x86_64 0:0.25-19.el7                                       libnm-gtk.x86_64 0:1.8.6-2.el7                                          
  libnma.x86_64 0:1.8.6-2.el7                                            libosinfo.x86_64 0:1.1.0-2.el7                                          
  libpath_utils.x86_64 0:0.2.1-29.el7                                    libpcap.x86_64 14:1.5.3-11.el7                                          
  libpciaccess.x86_64 0:0.13.4-3.1.el7_4                                 libpeas.x86_64 0:1.22.0-1.el7                                           
  libpeas-gtk.x86_64 0:1.22.0-1.el7                                      libpeas-loader-python.x86_64 0:1.22.0-1.el7                             
  libproxy.x86_64 0:0.4.11-11.el7                                        libproxy-mozjs.x86_64 0:0.4.11-11.el7                                   
  libpurple.x86_64 0:2.10.11-7.el7                                       libpwquality.x86_64 0:1.2.3-5.el7                                       
  libqmi.x86_64 0:1.18.0-2.el7                                           libqmi-utils.x86_64 0:1.18.0-2.el7                                      
  libquadmath.x86_64 0:4.8.5-28.el7_5.1                                  libquadmath-devel.x86_64 0:4.8.5-28.el7_5.1                             
  librados2.x86_64 1:10.2.5-4.el7                                        librbd1.x86_64 1:10.2.5-4.el7                                           
  librdmacm.x86_64 0:15-6.el7                                            libref_array.x86_64 0:0.1.5-29.el7                                      
  libreport.x86_64 0:2.1.11-40.el7                                       libreport-anaconda.x86_64 0:2.1.11-40.el7                               
  libreport-cli.x86_64 0:2.1.11-40.el7                                   libreport-filesystem.x86_64 0:2.1.11-40.el7                             
  libreport-gtk.x86_64 0:2.1.11-40.el7                                   libreport-plugin-bugzilla.x86_64 0:2.1.11-40.el7                        
  libreport-plugin-mailx.x86_64 0:2.1.11-40.el7                          libreport-plugin-reportuploader.x86_64 0:2.1.11-40.el7                  
  libreport-plugin-rhtsupport.x86_64 0:2.1.11-40.el7                     libreport-plugin-ureport.x86_64 0:2.1.11-40.el7                         
  libreport-python.x86_64 0:2.1.11-40.el7                                libreport-rhel.x86_64 0:2.1.11-40.el7                                   
  libreport-rhel-anaconda-bugzilla.x86_64 0:2.1.11-40.el7                libreport-web.x86_64 0:2.1.11-40.el7                                    
  libreswan.x86_64 0:3.20-5.el7_4                                        librsvg2.x86_64 0:2.40.20-1.el7                                         
  librsvg2-tools.x86_64 0:2.40.20-1.el7                                  libsecret.x86_64 0:0.18.6-1.el7                                         
  libselinux.x86_64 0:2.5-14.1.el7                                       libselinux-devel.x86_64 0:2.5-14.1.el7                                  
  libselinux-python.x86_64 0:2.5-14.1.el7                                libselinux-ruby.x86_64 0:2.5-14.1.el7                                   
  libselinux-utils.x86_64 0:2.5-14.1.el7                                 libsemanage.x86_64 0:2.5-14.el7                                         
  libsemanage-python.x86_64 0:2.5-14.el7                                 libsepol.x86_64 0:2.5-10.el7                                            
  libsepol-devel.x86_64 0:2.5-10.el7                                     libsmbclient.x86_64 0:4.7.1-9.el7_5                                     
  libsoup.x86_64 0:2.62.2-2.el7                                          libss.x86_64 0:1.42.9-11.el7                                            
  libssh2.x86_64 0:1.4.3-12.el7                                          libsss_autofs.x86_64 0:1.16.0-19.el7_5.5                                
  libsss_certmap.x86_64 0:1.16.0-19.el7_5.5                              libsss_idmap.x86_64 0:1.16.0-19.el7_5.5                                 
  libsss_nss_idmap.x86_64 0:1.16.0-19.el7_5.5                            libsss_sudo.x86_64 0:1.16.0-19.el7_5.5                                  
  libstdc++.x86_64 0:4.8.5-28.el7_5.1                                    libstdc++-devel.x86_64 0:4.8.5-28.el7_5.1                               
  libstoragemgmt.x86_64 0:1.4.0-5.el7_4                                  libstoragemgmt-python.noarch 0:1.4.0-5.el7_4                            
  libstoragemgmt-python-clibs.x86_64 0:1.4.0-5.el7_4                     libtalloc.x86_64 0:2.1.10-1.el7                                         
  libtdb.x86_64 0:1.3.15-1.el7                                           libteam.x86_64 0:1.25-6.el7_4.3                                         
  libtevent.x86_64 0:0.9.31-2.el7_4                                      libtiff.x86_64 0:4.0.3-32.el7                                           
  libtiff-devel.x86_64 0:4.0.3-32.el7                                    libtirpc.x86_64 0:0.2.4-0.15.el7                                        
  libudisks2.x86_64 0:2.7.3-6.el7                                        libusal.x86_64 0:1.1.11-25.el7                                          
  libusbx.x86_64 0:1.0.21-1.el7                                          libuser.x86_64 0:0.60-9.el7                                             
  libuser-python.x86_64 0:0.60-9.el7                                     libvirt-daemon.x86_64 0:3.9.0-14.el7_5.7                                
  libvirt-daemon-config-network.x86_64 0:3.9.0-14.el7_5.7                libvirt-daemon-driver-interface.x86_64 0:3.9.0-14.el7_5.7               
  libvirt-daemon-driver-network.x86_64 0:3.9.0-14.el7_5.7                libvirt-daemon-driver-nodedev.x86_64 0:3.9.0-14.el7_5.7                 
  libvirt-daemon-driver-nwfilter.x86_64 0:3.9.0-14.el7_5.7               libvirt-daemon-driver-qemu.x86_64 0:3.9.0-14.el7_5.7                    
  libvirt-daemon-driver-secret.x86_64 0:3.9.0-14.el7_5.7                 libvirt-daemon-driver-storage.x86_64 0:3.9.0-14.el7_5.7                 
  libvirt-daemon-driver-storage-core.x86_64 0:3.9.0-14.el7_5.7           libvirt-daemon-driver-storage-disk.x86_64 0:3.9.0-14.el7_5.7            
  libvirt-daemon-driver-storage-gluster.x86_64 0:3.9.0-14.el7_5.7        libvirt-daemon-driver-storage-iscsi.x86_64 0:3.9.0-14.el7_5.7           
  libvirt-daemon-driver-storage-logical.x86_64 0:3.9.0-14.el7_5.7        libvirt-daemon-driver-storage-mpath.x86_64 0:3.9.0-14.el7_5.7           
  libvirt-daemon-driver-storage-rbd.x86_64 0:3.9.0-14.el7_5.7            libvirt-daemon-driver-storage-scsi.x86_64 0:3.9.0-14.el7_5.7            
  libvirt-daemon-kvm.x86_64 0:3.9.0-14.el7_5.7                           libvirt-libs.x86_64 0:3.9.0-14.el7_5.7                                  
  libwacom.x86_64 0:0.24-3.el7                                           libwacom-data.noarch 0:0.24-3.el7                                       
  libwayland-client.x86_64 0:1.15.0-1.el7                                libwayland-server.x86_64 0:1.15.0-1.el7                                 
  libwbclient.x86_64 0:4.7.1-9.el7_5                                     libwnck3.x86_64 0:3.24.1-2.el7                                          
  libxcb.x86_64 0:1.13-1.el7                                             libxcb-devel.x86_64 0:1.13-1.el7                                        
  libxkbcommon.x86_64 0:0.7.1-3.el7                                      libxkbcommon-x11.x86_64 0:0.7.1-3.el7                                   
  linux-firmware.noarch 0:20180220-62.1.git6d51311.el7_5                 lldpad.x86_64 0:1.0.1-5.git036e314.el7                                  
  lm_sensors-libs.x86_64 0:3.4.0-6.20160601gitf9185e5.el7                logrotate.x86_64 0:3.8.6-15.el7                                         
  lsof.x86_64 0:4.87-5.el7                                               ltrace.x86_64 0:0.7.91-15.el7                                           
  lvm2.x86_64 7:2.02.177-4.el7                                           lvm2-libs.x86_64 7:2.02.177-4.el7                                       
  lz4.x86_64 0:1.7.5-3.el7                                               m17n-db.noarch 0:1.6.4-4.el7                                            
  mailx.x86_64 0:12.5-19.el7                                             make.x86_64 1:3.82-24.el7                                               
  man-db.x86_64 0:2.6.3-11.el7                                           man-pages-overrides.x86_64 0:7.5.2-1.el7                                
  mariadb-libs.x86_64 1:5.5.64-1.el7                                     mcelog.x86_64 3:144-8.94d853b2ea81.el7                                  
  mdadm.x86_64 0:4.0-13.el7                                              mesa-dri-drivers.x86_64 0:17.2.3-8.20171019.el7                         
  mesa-filesystem.x86_64 0:17.2.3-8.20171019.el7                         mesa-libEGL.x86_64 0:18.0.5-3.el7                                       
  mesa-libEGL-devel.x86_64 0:18.0.5-3.el7                                mesa-libGL.x86_64 0:18.0.5-3.el7                                        
  mesa-libGL-devel.x86_64 0:18.0.5-3.el7                                 mesa-libGLES.x86_64 0:18.0.5-3.el7                                      
  mesa-libgbm.x86_64 0:18.0.5-3.el7                                      mesa-libglapi.x86_64 0:18.0.5-3.el7                                     
  mesa-libxatracker.x86_64 0:17.2.3-8.20171019.el7                       microcode_ctl.x86_64 2:2.1-29.2.el7_5                                   
  mlocate.x86_64 0:0.26-8.el7                                            mokutil.x86_64 0:15-1.el7                                               
  mozjs17.x86_64 0:17.0.0-20.el7                                         nautilus.x86_64 0:3.26.3.1-2.el7                                        
  nautilus-extensions.x86_64 0:3.26.3.1-2.el7                            nautilus-sendto.x86_64 1:3.8.6-1.el7                                    
  neon.x86_64 0:0.30.0-4.el7                                             net-snmp-libs.x86_64 1:5.7.2-28.el7_4.1                                 
  net-tools.x86_64 0:2.0-0.24.20131004git.el7                            nfs-utils.x86_64 1:1.3.0-0.48.el7_4                                     
  nfs4-acl-tools.x86_64 0:0.3.3-17.el7                                   nm-connection-editor.x86_64 0:1.8.6-2.el7                               
  nmap-ncat.x86_64 2:6.40-13.el7                                         nscd.x86_64 0:2.17-260.el7                                              
  nspr.x86_64 0:4.21.0-1.el7                                             nss.x86_64 0:3.36.0-7.1.el7_6                                           
  nss-pam-ldapd.x86_64 0:0.8.13-16.el7                                   nss-pem.x86_64 0:1.0.3-5.el7                                            
  nss-softokn.x86_64 0:3.44.0-5.el7                                      nss-softokn-freebl.x86_64 0:3.44.0-5.el7                                
  nss-sysinit.x86_64 0:3.36.0-7.1.el7_6                                  nss-tools.x86_64 0:3.36.0-7.1.el7_6                                     
  ntp.x86_64 0:4.2.6p5-29.el7                                            ntpdate.x86_64 0:4.2.6p5-29.el7                                         
  numactl.x86_64 0:2.0.9-7.el7                                           numactl-libs.x86_64 0:2.0.9-7.el7                                       
  numad.x86_64 0:0.5-18.20150602git.el7                                  open-vm-tools.x86_64 0:10.1.10-3.el7                                    
  open-vm-tools-desktop.x86_64 0:10.1.10-3.el7                           openjpeg-libs.x86_64 0:1.5.1-18.el7                                     
  openldap.x86_64 0:2.4.44-13.el7                                        openldap-clients.x86_64 0:2.4.44-13.el7                                 
  openssh.x86_64 0:7.4p1-21.el7                                          openssh-clients.x86_64 0:7.4p1-21.el7                                   
  openssh-server.x86_64 0:7.4p1-21.el7                                   openssl.x86_64 1:1.0.2k-16.el7                                          
  openssl-libs.x86_64 1:1.0.2k-16.el7                                    oprofile.x86_64 0:0.9.9-25.el7                                          
  osinfo-db.noarch 0:20170813-6.el7                                      pam.x86_64 0:1.1.8-22.el7                                               
  pango.x86_64 0:1.42.4-1.el7                                            pango-devel.x86_64 0:1.42.4-1.el7                                       
  parted.x86_64 0:3.1-29.el7                                             passwd.x86_64 0:0.79-5.el7                                              
  patch.x86_64 0:2.7.1-11.el7                                            pciutils.x86_64 0:3.5.1-3.el7                                           
  pciutils-libs.x86_64 0:3.5.1-3.el7                                     pcp.x86_64 0:3.12.2-5.el7                                               
  pcp-conf.x86_64 0:3.12.2-5.el7                                         pcp-libs.x86_64 0:3.12.2-5.el7                                          
  pcp-selinux.x86_64 0:3.12.2-5.el7                                      pcsc-lite-libs.x86_64 0:1.8.8-7.el7                                     
  perf.x86_64 0:3.10.0-862.9.1.el7                                       perl.x86_64 4:5.16.3-293.el7                                            
  perl-Archive-Tar.noarch 0:1.92-3.el7                                   perl-CPAN.noarch 0:1.9800-293.el7                                       
  perl-ExtUtils-CBuilder.noarch 1:0.28.2.6-293.el7                       perl-ExtUtils-Embed.noarch 0:1.30-293.el7                               
  perl-ExtUtils-Install.noarch 0:1.58-293.el7                            perl-Getopt-Long.noarch 0:2.40-3.el7                                    
  perl-Git.noarch 0:1.8.3.1-19.el7                                       perl-HTTP-Daemon.noarch 0:6.01-7.el7                                    
  perl-IO-Socket-IP.noarch 0:0.21-5.el7                                  perl-IO-Socket-SSL.noarch 0:1.94-7.el7                                  
  perl-IO-Zlib.noarch 1:1.10-293.el7                                     perl-Locale-Maketext-Simple.noarch 1:0.21-293.el7                       
  perl-Module-CoreList.noarch 1:2.76.02-293.el7                          perl-Module-Loaded.noarch 1:0.08-293.el7                                
  perl-Object-Accessor.noarch 1:0.42-293.el7                             perl-Package-Constants.noarch 1:0.02-293.el7                            
  perl-Pod-Escapes.noarch 1:1.04-293.el7                                 perl-Time-Piece.x86_64 0:1.20.1-293.el7                                 
  perl-core.x86_64 0:5.16.3-293.el7                                      perl-devel.x86_64 4:5.16.3-293.el7                                      
  perl-libs.x86_64 4:5.16.3-293.el7                                      perl-macros.x86_64 4:5.16.3-293.el7                                     
  perl-version.x86_64 3:0.99.07-3.el7                                    plymouth.x86_64 0:0.8.9-0.31.20140113.el7                               
  plymouth-core-libs.x86_64 0:0.8.9-0.31.20140113.el7                    plymouth-graphics-libs.x86_64 0:0.8.9-0.31.20140113.el7                 
  plymouth-plugin-label.x86_64 0:0.8.9-0.31.20140113.el7                 plymouth-plugin-two-step.x86_64 0:0.8.9-0.31.20140113.el7               
  plymouth-scripts.x86_64 0:0.8.9-0.31.20140113.el7                      plymouth-system-theme.x86_64 0:0.8.9-0.31.20140113.el7                  
  plymouth-theme-charge.x86_64 0:0.8.9-0.31.20140113.el7                 policycoreutils.x86_64 0:2.5-29.el7                                     
  policycoreutils-python.x86_64 0:2.5-29.el7                             polkit.x86_64 0:0.112-14.el7                                            
  poppler.x86_64 0:0.26.5-20.el7                                         poppler-glib.x86_64 0:0.26.5-20.el7                                     
  poppler-utils.x86_64 0:0.26.5-20.el7                                   postfix.x86_64 2:2.10.1-7.el7                                           
  powertop.x86_64 0:2.9-1.el7                                            procps-ng.x86_64 0:3.3.10-23.el7                                        
  psmisc.x86_64 0:22.20-16.el7                                           pulseaudio.x86_64 0:10.0-5.el7                                          
  pulseaudio-gdm-hooks.x86_64 0:10.0-5.el7                               pulseaudio-libs.x86_64 0:10.0-5.el7                                     
  pulseaudio-libs-glib2.x86_64 0:10.0-5.el7                              pulseaudio-module-bluetooth.x86_64 0:10.0-5.el7                         
  pulseaudio-module-x11.x86_64 0:10.0-5.el7                              pulseaudio-utils.x86_64 0:10.0-5.el7                                    
  pyOpenSSL.x86_64 0:0.13.1-4.el7                                        pykickstart.noarch 0:1.99.66.18-1.el7                                   
  pyparted.x86_64 1:3.9-15.el7                                           pytalloc.x86_64 0:2.1.10-1.el7                                          
  python.x86_64 0:2.7.5-76.el7                                           python-backports-ssl_match_hostname.noarch 0:3.5.0.1-1.el7              
  python-blivet.noarch 1:0.61.15.69-1.el7                                python-brlapi.x86_64 0:0.6.0-16.el7                                     
  python-chardet.noarch 0:2.2.1-3.el7                                    python-configshell.noarch 1:1.1.fb23-4.el7_5                            
  python-dmidecode.x86_64 0:3.12.2-1.1.el7                               python-ethtool.x86_64 0:0.8-7.el7                                       
  python-firewall.noarch 0:0.4.4.4-14.el7                                python-gobject.x86_64 0:3.22.0-1.el7_4.1                                
  python-gobject-base.x86_64 0:3.22.0-1.el7_4.1                          python-jwcrypto.noarch 0:0.4.2-1.el7                                    
  python-libipa_hbac.x86_64 0:1.16.0-19.el7_5.5                          python-libs.x86_64 0:2.7.5-76.el7                                       
  python-linux-procfs.noarch 0:0.4.9-4.el7                               python-magic.noarch 0:5.11-35.el7                                       
  python-meh.noarch 0:0.25.3-1.el7                                       python-meh-gui.noarch 0:0.25.3-1.el7                                    
  python-netaddr.noarch 0:0.7.5-9.el7                                    python-perf.x86_64 0:3.10.0-862.9.1.el7                                 
  python-pwquality.x86_64 0:1.2.3-5.el7                                  python-requests.noarch 0:2.6.0-5.el7                                    
  python-rtslib.noarch 0:2.1.fb63-5.el7                                  python-slip.noarch 0:0.4.0-4.el7                                        
  python-slip-dbus.noarch 0:0.4.0-4.el7                                  python-smbc.x86_64 0:1.0.13-8.el7                                       
  python-sss-murmur.x86_64 0:1.16.0-19.el7_5.5                           python-sssdconfig.noarch 0:1.16.0-19.el7_5.5                            
  python-urlgrabber.noarch 0:3.10-9.el7                                  python-urllib3.noarch 0:1.10.2-5.el7                                    
  python2-cryptography.x86_64 0:1.7.2-1.el7_4.1                          python2-ipaclient.noarch 0:4.5.0-21.el7                                 
  python2-ipalib.noarch 0:4.5.0-21.el7                                   qemu-guest-agent.x86_64 10:2.8.0-2.el7_5.1                              
  qemu-img.x86_64 10:1.5.3-160.el7                                       qemu-kvm.x86_64 10:1.5.3-160.el7                                        
  qemu-kvm-common.x86_64 10:1.5.3-160.el7                                quota.x86_64 1:4.01-17.el7                                              
  quota-nls.noarch 1:4.01-17.el7                                         radvd.x86_64 0:1.9.2-9.el7_5.4                                          
  rasdaemon.x86_64 0:0.4.1-32.el7                                        rdma-core.x86_64 0:15-6.el7                                             
  readline.x86_64 0:6.2-11.el7                                           realmd.x86_64 0:0.16.1-11.el7                                           
  redhat-indexhtml.noarch 0:7-13.el7                                     redhat-logos.noarch 0:70.0.3-7.el7                                      
  redhat-rpm-config.noarch 0:9.1.0-87.el7                                redhat-support-tool.noarch 0:0.9.10-1.el7                               
  rest.x86_64 0:0.8.0-2.el7                                              rfkill.x86_64 0:0.4-10.el7                                              
  rhn-check.noarch 0:2.0.2-21.el7                                        rhn-client-tools.noarch 0:2.0.2-21.el7                                  
  rhn-setup.noarch 0:2.0.2-21.el7                                        rhn-setup-gnome.noarch 0:2.0.2-21.el7                                   
  rhnlib.noarch 0:2.5.65-7.el7                                           rhnsd.x86_64 0:5.0.13-7.1.el7_4                                         
  rng-tools.x86_64 0:5-13.el7                                            rpcbind.x86_64 0:0.2.0-44.el7                                           
  rpm.x86_64 0:4.11.3-35.el7                                             rpm-build.x86_64 0:4.11.3-35.el7                                        
  rpm-build-libs.x86_64 0:4.11.3-35.el7                                  rpm-libs.x86_64 0:4.11.3-35.el7                                         
  rpm-python.x86_64 0:4.11.3-35.el7                                      rpm-sign.x86_64 0:4.11.3-35.el7                                         
  rsync.x86_64 0:3.1.2-4.el7                                             rsyslog.x86_64 0:8.24.0-16.el7                                          
  ruby.x86_64 0:2.0.0.648-34.el7_6                                       ruby-irb.noarch 0:2.0.0.648-34.el7_6                                    
  ruby-libs.x86_64 0:2.0.0.648-34.el7_6                                  rubygem-bigdecimal.x86_64 0:1.2.0-34.el7_6                              
  rubygem-io-console.x86_64 0:0.4.2-34.el7_6                             rubygem-json.x86_64 0:1.7.7-34.el7_6                                    
  rubygem-psych.x86_64 0:2.0.0-34.el7_6                                  rubygem-rdoc.noarch 0:4.0.0-34.el7_6                                    
  rubygems.noarch 0:2.0.14.1-34.el7_6                                    samba-client.x86_64 0:4.7.1-9.el7_5                                     
  samba-client-libs.x86_64 0:4.7.1-9.el7_5                               samba-common.noarch 0:4.7.1-9.el7_5                                     
  samba-common-libs.x86_64 0:4.7.1-9.el7_5                               samba-common-tools.x86_64 0:4.7.1-9.el7_5                               
  samba-libs.x86_64 0:4.7.1-9.el7_5                                      samba-winbind.x86_64 0:4.7.1-9.el7_5                                    
  samba-winbind-modules.x86_64 0:4.7.1-9.el7_5                           sane-backends.x86_64 0:1.0.24-11.el7                                    
  sane-backends-drivers-scanners.x86_64 0:1.0.24-11.el7                  sane-backends-libs.x86_64 0:1.0.24-11.el7                               
  satyr.x86_64 0:0.13-15.el7                                             scl-utils.x86_64 0:20130529-18.el7_4                                    
  seabios-bin.noarch 0:1.10.2-3.el7_4.1                                  seavgabios-bin.noarch 0:1.10.2-3.el7_4.1                                
  selinux-policy.noarch 0:3.13.1-166.el7_4.4                             selinux-policy-targeted.noarch 0:3.13.1-166.el7_4.4                     
  setools-libs.x86_64 0:3.3.8-4.el7                                      setroubleshoot.x86_64 0:3.2.29-3.el7                                    
  setroubleshoot-plugins.noarch 0:3.0.66-2.1.el7                         setroubleshoot-server.x86_64 0:3.2.29-3.el7                             
  setup.noarch 0:2.8.71-9.el7                                            sg3_utils-libs.x86_64 0:1.37-17.el7                                     
  shadow-utils.x86_64 2:4.1.5.1-25.el7                                   shared-mime-info.x86_64 0:1.8-4.el7                                     
  shim-x64.x86_64 0:15-1.el7                                             smartmontools.x86_64 1:6.5-1.el7                                        
  sos.noarch 0:3.4-6.el7                                                 sox.x86_64 0:14.4.1-7.el7                                               
  spice-glib.x86_64 0:0.35-2.el7                                         spice-gtk3.x86_64 0:0.35-2.el7                                          
  spice-server.x86_64 0:0.14.0-6.el7                                     spice-vdagent.x86_64 0:0.14.0-15.el7                                    
  sssd-ad.x86_64 0:1.16.0-19.el7_5.5                                     sssd-client.x86_64 0:1.16.0-19.el7_5.5                                  
  sssd-common.x86_64 0:1.16.0-19.el7_5.5                                 sssd-common-pac.x86_64 0:1.16.0-19.el7_5.5                              
  sssd-ipa.x86_64 0:1.16.0-19.el7_5.5                                    sssd-krb5.x86_64 0:1.16.0-19.el7_5.5                                    
  sssd-krb5-common.x86_64 0:1.16.0-19.el7_5.5                            sssd-proxy.x86_64 0:1.16.0-19.el7_5.5                                   
  strace.x86_64 0:4.12-6.el7                                             subscription-manager.x86_64 0:1.19.23-1.el7_4                           
  subscription-manager-gui.x86_64 0:1.19.23-1.el7_4                      subscription-manager-initial-setup-addon.x86_64 0:1.19.23-1.el7_4       
  sudo.x86_64 0:1.8.19p2-11.el7_4                                        sushi.x86_64 0:3.28.3-1.el7                                             
  sysstat.x86_64 0:10.1.5-13.el7                                         system-config-printer.x86_64 0:1.4.1-21.el7                             
  system-config-printer-libs.noarch 0:1.4.1-21.el7                       system-config-printer-udev.x86_64 0:1.4.1-21.el7                        
  systemd.x86_64 0:219-57.el7_5.1                                        systemd-libs.x86_64 0:219-57.el7_5.1                                    
  systemd-python.x86_64 0:219-57.el7_5.1                                 systemd-sysv.x86_64 0:219-57.el7_5.1                                    
  systemtap.x86_64 0:3.2-8.el7_5                                         systemtap-client.x86_64 0:3.2-8.el7_5                                   
  systemtap-devel.x86_64 0:3.2-8.el7_5                                   systemtap-runtime.x86_64 0:3.2-8.el7_5                                  
  systemtap-sdt-devel.x86_64 0:3.1-4.el7_4                               tar.x86_64 2:1.26-34.el7                                                
  targetcli.noarch 0:2.1.fb46-4.el7_5                                    tcpdump.x86_64 14:4.9.2-3.el7                                           
  teamd.x86_64 0:1.25-6.el7_4.3                                          telepathy-glib.x86_64 0:0.24.1-1.el7                                    
  tigervnc.x86_64 0:1.8.0-13.el7                                         tigervnc-icons.noarch 0:1.8.0-13.el7                                    
  tigervnc-license.noarch 0:1.8.0-2.el7_4                                tigervnc-server-minimal.x86_64 0:1.8.0-2.el7_4                          
  totem.x86_64 1:3.26.2-1.el7                                            totem-nautilus.x86_64 1:3.26.2-1.el7                                    
  totem-pl-parser.x86_64 0:3.26.1-1.el7                                  trace-cmd.x86_64 0:2.6.0-10.el7                                         
  tracker.x86_64 0:1.10.5-6.el7                                          tuned.noarch 0:2.8.0-5.el7_4.2                                          
  tzdata.noarch 0:2017c-1.el7                                            tzdata-java.noarch 0:2017c-1.el7                                        
  udisks2.x86_64 0:2.7.3-6.el7                                           unzip.x86_64 0:6.0-19.el7                                               
  upower.x86_64 0:0.99.7-1.el7                                           usb_modeswitch.x86_64 0:2.5.1-1.el7                                     
  usb_modeswitch-data.noarch 0:20170806-1.el7                            usbredir.x86_64 0:0.7.1-3.el7                                           
  usermode.x86_64 0:1.111-6.el7                                          usermode-gtk.x86_64 0:1.111-6.el7                                       
  util-linux.x86_64 0:2.23.2-61.el7                                      valgrind.x86_64 1:3.12.0-9.el7_4                                        
  vim-common.x86_64 2:7.4.160-4.el7                                      vim-enhanced.x86_64 2:7.4.160-4.el7                                     
  vim-filesystem.x86_64 2:7.4.160-4.el7                                  vim-minimal.x86_64 2:7.4.160-4.el7                                      
  vinagre.x86_64 0:3.22.0-9.el7                                          vino.x86_64 0:3.22.0-7.el7                                              
  virt-what.x86_64 0:1.18-4.el7                                          vte-profile.x86_64 0:0.52.2-2.el7                                       
  vte291.x86_64 0:0.52.2-2.el7                                           webkitgtk4.x86_64 0:2.14.7-3.el7                                        
  webkitgtk4-jsc.x86_64 0:2.14.7-3.el7                                   webkitgtk4-plugin-process-gtk2.x86_64 0:2.14.7-3.el7                    
  wget.x86_64 0:1.14-18.el7                                              wodim.x86_64 0:1.1.11-25.el7                                            
  wpa_supplicant.x86_64 1:2.6-12.el7                                     xdg-desktop-portal.x86_64 0:1.0.2-1.el7                                 
  xdg-desktop-portal-gtk.x86_64 0:1.0.2-1.el7                            xfsdump.x86_64 0:3.1.7-1.el7                                            
  xfsprogs.x86_64 0:4.5.0-15.el7                                         xkeyboard-config.noarch 0:2.24-1.el7                                    
  xorg-x11-drv-evdev.x86_64 0:2.10.6-1.el7                               xorg-x11-drv-synaptics.x86_64 0:1.9.0-2.el7                             
  xorg-x11-drv-vmmouse.x86_64 0:13.1.0-1.el7.1                           xorg-x11-drv-void.x86_64 0:1.4.1-2.el7.1                                
  xorg-x11-drv-wacom.x86_64 0:0.34.2-4.el7                               xorg-x11-font-utils.x86_64 1:7.5-21.el7                                 
  xorg-x11-proto-devel.noarch 0:2018.4-1.el7                             xorg-x11-utils.x86_64 0:7.5-23.el7                                      
  xorg-x11-xinit.x86_64 0:1.3.4-2.el7                                    xorg-x11-xkb-utils.x86_64 0:7.7-14.el7                                  
  yelp.x86_64 2:3.28.1-1.el7                                             yelp-libs.x86_64 2:3.28.1-1.el7                                         
  yelp-xsl.noarch 0:3.28.0-1.el7                                         yum.noarch 0:3.4.3-158.el7                                              
  yum-rhn-plugin.noarch 0:2.0.1-10.el7                                   yum-utils.noarch 0:1.1.31-50.el7                                        
  zenity.x86_64 0:3.28.1-1.el7                                          

Dependency Updated:
  alsa-lib.x86_64 0:1.1.8-1.el7       cryptsetup-libs.x86_64 0:2.0.3-5.el7      flatpak.x86_64 0:1.0.2-7.el7      gtk3.x86_64 0:3.22.30-3.el7     
  libuuid.x86_64 0:2.23.2-61.el7      nss-util.x86_64 0:3.44.0-3.el7           

Replaced:
  gnome-dictionary-libs.x86_64 0:3.20.0-1.el7       grub2.x86_64 1:2.02-0.64.el7                         grub2-tools.x86_64 1:2.02-0.64.el7      
  python-rhsm.x86_64 0:1.19.9-1.el7                 python-rhsm-certificates.x86_64 0:1.19.9-1.el7 

-------------------------------------------------------------------------------------------------------------------------------------------

``` 



### Further DEBUG: 11-07-2019 


```
### Show Duplicate Messages:

###  yum --showduplicates list gnome-session | expand  ### 

Plugin "product-id" can't be imported
Plugin "search-disabled-repos" can't be imported
Plugin "subscription-manager" can't be imported
Loaded plugins: langpacks, rhnplugin, versionlock
This system is receiving updates from RHN Classic or Red Hat Satellite.
Excluding 1 update due to versionlock (use "yum versionlock status" to show it)
Installed Packages
gnome-session.x86_64             3.28.1-7.el7              @rhel-x86_64-server-7
Available Packages
gnome-session.x86_64             3.8.4-11.el7              rhel-x86_64-server-7 
gnome-session.x86_64             3.14.0-4.el7              rhel-x86_64-server-7 
gnome-session.x86_64             3.14.0-5.el7              rhel-x86_64-server-7 
gnome-session.x86_64             3.22.3-4.el7              rhel-x86_64-server-7 
gnome-session.x86_64             3.26.1-11.el7             rhel-x86_64-server-7 
gnome-session.x86_64             3.28.1-5.el7              rhel-x86_64-server-7 
gnome-session.x86_64             3.28.1-6.el7              rhel-x86_64-server-7 
gnome-session.x86_64             3.28.1-7.el7              rhel-x86_64-server-7 


### See Which Version Installed: 

yum info gnome-session-3.28.1-7.el7.x86_64
Plugin "product-id" can't be imported
Plugin "search-disabled-repos" can't be imported
Plugin "subscription-manager" can't be imported
Loaded plugins: langpacks, rhnplugin, versionlock
This system is receiving updates from RHN Classic or Red Hat Satellite.
Excluding 1 update due to versionlock (use "yum versionlock status" to show it)
Installed Packages
Name        : gnome-session
Arch        : x86_64
Version     : 3.28.1
Release     : 7.el7                 ### OOOPS...Initial Update Messed Things Up ####
Size        : 1.5 M
Repo        : installed
From repo   : rhel-x86_64-server-7
Summary     : GNOME session manager
URL         : http://www.gnome.org
License     : GPLv2+
Description : gnome-session manages a GNOME desktop or GDM login session. It starts up
            : the other core GNOME components and handles logout and saving the session.



### SSH'ed into Murat's 7.4 WS & Did Searching on Web: CORRECT VERSION SHOULD BE:

----------------------------
gnome-session-3.22.3-4.el7.x86_64
----------------------------


### DOWNGRADE!!! 

------------------------------------------------------------------------------------------
yum downgrade gnome-session-3.22.3-4.el7.x86_64
Plugin "product-id" can't be imported
Plugin "search-disabled-repos" can't be imported
Plugin "subscription-manager" can't be imported
Loaded plugins: langpacks, rhnplugin, versionlock
This system is receiving updates from RHN Classic or Red Hat Satellite.
biohpc_el7                                                                                                                                             | 2.1 kB  00:00:00     
Excluding 1 update due to versionlock (use "yum versionlock status" to show it)
Resolving Dependencies
--> Running transaction check
---> Package gnome-session.x86_64 0:3.22.3-4.el7 will be a downgrade
---> Package gnome-session.x86_64 0:3.28.1-7.el7 will be erased
--> Processing Conflict: gnome-settings-daemon-3.28.1-5.el7.x86_64 conflicts gnome-session < 3.27.90



yum remove gnome-settings-daemon-3.28.1-5.el7.x86_64
==============================================================================================================================================================================
 Package                                                       Arch                       Version                             Repository                                 Size
==============================================================================================================================================================================
Removing:
 gnome-settings-daemon                                         x86_64                     3.28.1-5.el7                        @rhel-x86_64-server-7                     5.1 M
Removing for dependencies:
 control-center                                                x86_64                     1:3.26.2-8.el7                      @rhel-x86_64-server-7                      18 M
 gdm                                                           x86_64                     1:3.28.2-16.el7                     @rhel-x86_64-server-7                     2.2 M
 gnome-classic-session                                         noarch                     3.26.2-3.el7                        @rhel-x86_64-server-7                     199 k
 gnome-shell                                                   x86_64                     3.28.3-11.el7                       @rhel-x86_64-server-7                      11 M
 gnome-shell-extension-alternate-tab                           noarch                     3.26.2-3.el7                        @rhel-x86_64-server-7                     9.5 k
 gnome-shell-extension-apps-menu                               noarch                     3.26.2-3.el7                        @rhel-x86_64-server-7                      32 k
 gnome-shell-extension-common                                  noarch                     3.26.2-3.el7                        @rhel-x86_64-server-7                     590 k
 gnome-shell-extension-launch-new-instance                     noarch                     3.26.2-3.el7                        @rhel-x86_64-server-7                     4.9 k
 gnome-shell-extension-places-menu                             noarch                     3.26.2-3.el7                        @rhel-x86_64-server-7                      25 k
 gnome-shell-extension-top-icons                               noarch                     3.26.2-3.el7                        @rhel-x86_64-server-7                      12 k
 gnome-shell-extension-user-theme                              noarch                     3.26.2-3.el7                        @rhel-x86_64-server-7                     7.0 k
 gnome-shell-extension-window-list                             noarch                     3.26.2-3.el7                        @rhel-x86_64-server-7                      57 k
 gnome-tweak-tool                                              noarch                     3.28.1-2.el7.2                      @rhel-x86_64-server-7                     1.3 M
 orca                                                          x86_64                     3.6.3-4.el7                         @anaconda/7.4                              13 M
 pulseaudio-gdm-hooks                                          x86_64                     10.0-5.el7                          @rhel-x86_64-server-7                     354  
 system-config-printer                                         x86_64                     1.4.1-21.el7                        @rhel-x86_64-server-7                     1.3 M

Transaction Summary
==============================================================================================================================================================================
Remove  1 Package (+16 Dependent packages)
------------------------------------------------------------------------------------------



### Lock Down on Version-Specific Packages 

------------------------------------------------------------------------------------------
yum versionlock gnome-shell-3.22.3-17.el7.x86_64
yum versionlock gnome-session-3.22.3-4.el7.x86_64

 yum versionlock list
Plugin "product-id" can't be imported
Plugin "search-disabled-repos" can't be imported
Plugin "subscription-manager" can't be imported
Loaded plugins: langpacks, rhnplugin, versionlock
This system is receiving updates from RHN Classic or Red Hat Satellite.
1:nvidia-kmod-390.116-2.el7.*
0:gnome-shell-3.22.3-17.el7.*
0:gnome-session-3.22.3-4.el7.*

yum versionlock gnome-settings-daemon-3.22.2-5.el7.x86_64
yum versionlock control-center-3.22.2-5.el7.x86_64

yum downgrade  mutter-3.22.3-12.el7_4.x86_64
Removed:
  mutter.x86_64 0:3.28.3-10.el7                                                                                                                                               

Installed:
  mutter.x86_64 0:3.22.3-12.el7_4


yum downgrade control-center-filesystem-3.22.2-5.el7.x86_64 
Removed:
  control-center-filesystem.x86_64 1:3.26.2-8.el7                                                                                                                             

Installed:
  control-center-filesystem.x86_64 1:3.22.2-5.el7

yum versionlock gdm-3.22.3-13.el7_4.x86_64


--------------------------------------------------------------------------------------------------------------------------------
yum remove libgweather-3.28.2-2.el7.x86_64

Removed:

  libgweather.x86_64 0:3.28.2-2.el7                                                                                                                                           
Dependency Removed:
  empathy.x86_64 0:3.12.13-1.el7        evolution-data-server.x86_64 0:3.28.5-3.el7    evolution-data-server-langpacks.noarch 0:3.28.5-3.el7    folks.x86_64 1:0.11.4-1.el7   
  gnome-clocks.x86_64 0:3.28.0-1.el7    gnome-contacts.x86_64 0:3.28.2-1.el7           gnome-weather.noarch 0:3.26.0-1.el7 

--------------------------------------------------------------------------------------------------------------------------------
yum install libgweather-3.20.4-1.el7.x86_64 evolution-data-server-3.22.7-6.el7.x86_64

yum install gnome-shell

--------------------------------------------------------------------------------------------------------------------------------
Installed:
  gnome-shell.x86_64 0:3.22.3-17.el7                                                                                                                                          

Dependency Installed:
  caribou0.x86_64 0:0.4.21-12.1.el7                       caribou0-gtk2-module.x86_64 0:0.4.21-12.1.el7             caribou0-gtk3-module.x86_64 0:0.4.21-12.1.el7            
  control-center.x86_64 1:3.22.2-5.el7                    gdm.x86_64 1:3.22.3-13.el7_4                              gnome-session.x86_64 0:3.22.3-4.el7                      
  gnome-settings-daemon.x86_64 0:3.22.2-5.el7             pulseaudio-gdm-hooks.x86_64 0:10.0-5.el7                  python2-caribou0.noarch 0:0.4.21-12.1.el7   


yum install gnome-clocks-3.22.1-1.el7.x86_64 gnome-weather-3.20.2-1.el7.noarch

yum install system-config-printer

yum install gnome-shell-extension-common-3.22.2-10.el7.noarch

yum install gnome-classic-session-3.22.2-10.el7.noarch

Dependency Installed:
  gnome-shell-extension-alternate-tab.noarch 0:3.22.2-10.el7                                                
  gnome-shell-extension-apps-menu.noarch 0:3.22.2-10.el7                                                    
  gnome-shell-extension-launch-new-instance.noarch 0:3.22.2-10.el7                                          
  gnome-shell-extension-places-menu.noarch 0:3.22.2-10.el7                                                  
  gnome-shell-extension-window-list.noarch 0:3.22.2-10.el7  


yum install gnome-shell-extension-user-theme-3.22.2-10.el7.noarch

yum localinstall gnome-tweak-tool-3.22.0-1.el7.noarch.rpm

yum install folks-0.11.3-1.el7.x86_64

yum install empathy-3.12.12-4.el7.x86_64

yum install gnome-session-xsession-3.22.3-4.el7.x86_64

--------------------------------------------------------------------------------------------------------------------------------

### Reboot but to no avail: aligning more packages

------------------------------------------------------------------------------------------
yum downgrade gnome-bluetooth-3.20.1-1.el7.x86_64 gnome-bluetooth-libs-3.20.1-1.el7.x86_64

Removed:
  gnome-bluetooth.x86_64 1:3.28.2-1.el7                                                gnome-bluetooth-libs.x86_64 1:3.28.2-1.el7                                               

Installed:
  gnome-bluetooth.x86_64 1:3.20.1-1.el7                                                gnome-bluetooth-libs.x86_64 1:3.20.1-1.el7     


 yum downgrade gnome-boxes-3.22.4-4.el7.x86_64

Removed:
  gnome-boxes.x86_64 0:3.28.5-2.el7                                                                                                                                             

Installed:
  gnome-boxes.x86_64 0:3.22.4-4.el7


yum downgrade gnome-calculator-3.22.3-1.el7.x86_64

Removed:
  gnome-calculator.x86_64 0:3.28.2-1.el7                                                                                                                                        

Installed:
  gnome-calculator.x86_64 0:3.22.3-1.el7                                                                                                                                        

########CLUES###########

[root@biohpcwsc037 biohpcadmin]# yum downgrade gnome-desktop3-3.22.2-2.el7.x86_64
Plugin "product-id" can't be imported
Plugin "search-disabled-repos" can't be imported
Plugin "subscription-manager" can't be imported
Loaded plugins: langpacks, rhnplugin, versionlock
This system is receiving updates from RHN Classic or Red Hat Satellite.
biohpc_el7                                                                                                                                               | 2.1 kB  00:00:00     
Excluding 7 updates due to versionlock (use "yum versionlock status" to show them)
Resolving Dependencies
--> Running transaction check
---> Package gnome-desktop3.x86_64 0:3.22.2-2.el7 will be a downgrade
---> Package gnome-desktop3.x86_64 0:3.28.2-2.el7 will be erased
--> Finished Dependency Resolution
Error: Package: nautilus-3.26.3.1-2.el7.x86_64 (@rhel-x86_64-server-7)
           Requires: libgnome-desktop-3.so.17()(64bit)
           Removing: gnome-desktop3-3.28.2-2.el7.x86_64 (@rhel-x86_64-server-7)
               libgnome-desktop-3.so.17()(64bit)
           Downgraded By: gnome-desktop3-3.22.2-2.el7.x86_64 (rhel-x86_64-server-7)
              ~libgnome-desktop-3.so.12()(64bit)
Error: Package: 1:totem-3.26.2-1.el7.x86_64 (@rhel-x86_64-server-7)
           Requires: libgnome-desktop-3.so.17()(64bit)
           Removing: gnome-desktop3-3.28.2-2.el7.x86_64 (@rhel-x86_64-server-7)
               libgnome-desktop-3.so.17()(64bit)
           Downgraded By: gnome-desktop3-3.22.2-2.el7.x86_64 (rhel-x86_64-server-7)
              ~libgnome-desktop-3.so.12()(64bit)
Error: Package: evince-3.28.2-5.el7.x86_64 (@rhel-x86_64-server-7)
           Requires: libgnome-desktop-3.so.17()(64bit)
           Removing: gnome-desktop3-3.28.2-2.el7.x86_64 (@rhel-x86_64-server-7)
               libgnome-desktop-3.so.17()(64bit)
           Downgraded By: gnome-desktop3-3.22.2-2.el7.x86_64 (rhel-x86_64-server-7)
              ~libgnome-desktop-3.so.12()(64bit)
Error: Package: eog-3.28.3-1.el7.x86_64 (@rhel-x86_64-server-7)
           Requires: libgnome-desktop-3.so.17()(64bit)
           Removing: gnome-desktop3-3.28.2-2.el7.x86_64 (@rhel-x86_64-server-7)
               libgnome-desktop-3.so.17()(64bit)
           Downgraded By: gnome-desktop3-3.22.2-2.el7.x86_64 (rhel-x86_64-server-7)
              ~libgnome-desktop-3.so.12()(64bit)
Error: Package: gnome-font-viewer-3.28.0-1.el7.x86_64 (@rhel-x86_64-server-7)
           Requires: libgnome-desktop-3.so.17()(64bit)
           Removing: gnome-desktop3-3.28.2-2.el7.x86_64 (@rhel-x86_64-server-7)
               libgnome-desktop-3.so.17()(64bit)
           Downgraded By: gnome-desktop3-3.22.2-2.el7.x86_64 (rhel-x86_64-server-7)
              ~libgnome-desktop-3.so.12()(64bit)

########CLUES###########

yum remove gnome-dictionary-3.26.1-2.el7.x86_64

yum versionlock gnome-dictionary-3.20.0-1.el7.x86_64

yum install gnome-dictionary-libs-3.20.0-1.el7.x86_64

yum install gnome-dictionary-3.20.0-1.el7.x86_64


yum downgrade gnome-disk-utility-3.22.1-1.el7.x86_64 gnome-font-viewer-3.22.0-1.el7.x86_64

Removed:
  gnome-disk-utility.x86_64 0:3.28.3-1.el7                                                gnome-font-viewer.x86_64 0:3.28.0-1.el7                                               

Installed:
  gnome-disk-utility.x86_64 0:3.22.1-1.el7                                                gnome-font-viewer.x86_64 0:3.22.0-1.el7                                               



yum downgrade gnome-keyring-3.20.0-3.el7.x86_64 gnome-keyring-pam-3.20.0-3.el7.x86_64 

Removed:
  gnome-keyring.x86_64 0:3.28.2-1.el7                                                  gnome-keyring-pam.x86_64 0:3.28.2-1.el7                                                 

Installed:
  gnome-keyring.x86_64 0:3.20.0-3.el7                                                  gnome-keyring-pam.x86_64 0:3.20.0-3.el7                                                 



yum downgrade gnome-online-accounts-3.22.5-1.el7.x86_64 gnome-packagekit-3.22.1-2.el7.x86_64

Removed:
  gnome-online-accounts.x86_64 0:3.26.2-1.el7                                               gnome-packagekit.x86_64 0:3.28.0-1.el7                                              

Installed:
  gnome-online-accounts.x86_64 0:3.22.5-1.el7                                               gnome-packagekit.x86_64 0:3.22.1-2.el7

yum downgrade gnome-packagekit-common-3.22.1-2.el7.x86_64 gnome-packagekit-installer-3.22.1-2.el7.x86_64 gnome-packagekit-updater-3.22.1-2.el7.x86_64

Removed:
  gnome-packagekit-common.x86_64 0:3.28.0-1.el7            gnome-packagekit-installer.x86_64 0:3.28.0-1.el7            gnome-packagekit-updater.x86_64 0:3.28.0-1.el7           

Installed:
  gnome-packagekit-common.x86_64 0:3.22.1-2.el7            gnome-packagekit-installer.x86_64 0:3.22.1-2.el7            gnome-packagekit-updater.x86_64 0:3.22.1-2.el7           



yum downgrade gnome-screenshot-3.22.0-1.el7.x86_64 gnome-software-3.22.7-1.el7.x86_64

Removed:
  gnome-screenshot.x86_64 0:3.26.0-1.el7                                                  gnome-software.x86_64 0:3.22.7-5.el7                                                 

Installed:
  gnome-screenshot.x86_64 0:3.22.0-1.el7                                                  gnome-software.x86_64 0:3.22.7-1.el7   


yum downgrade gnome-system-monitor-3.22.2-2.el7.x86_64

Removed:
  gnome-system-monitor.x86_64 0:3.22.2-3.el7                                                                                                                                    

Installed:
  gnome-system-monitor.x86_64 0:3.22.2-2.el7         


yum downgrade gnome-terminal-nautilus-3.22.1-2.el7.x86_64 gnome-terminal-3.22.1-2.el7.x86_64

Removed:
  gnome-terminal.x86_64 0:3.28.2-2.el7                                               gnome-terminal-nautilus.x86_64 0:3.28.2-2.el7                                              

Installed:
  gnome-terminal.x86_64 0:3.22.1-2.el7                                               gnome-terminal-nautilus.x86_64 0:3.22.1-2.el7                                             



########### CLUES #############

yum remove gnome-themes-standard-3.22.2-2.el7_5.x86_64

REMOVE:

 gnome-themes-standard                                         x86_64                     3.22.2-2.el7_5                        @rhel-x86_64-server-7                     4.3 M
Removing for dependencies:
 cheese                                                        x86_64                     2:3.22.1-2.el7                        @rhel-x86_64-server-7                     407 k
 cheese-libs                                                   x86_64                     2:3.22.1-2.el7                        @rhel-x86_64-server-7                     2.4 M
 compat-cheese314                                              x86_64                     3.14.2-1.el7                          @anaconda/7.4                             148 k
 control-center                                                x86_64                     1:3.22.2-5.el7                        @rhel-x86_64-server-7                      18 M
 empathy                                                       x86_64                     3.12.12-4.el7                         @rhel-x86_64-server-7                      14 M
 eog                                                           x86_64                     3.28.3-1.el7                          @rhel-x86_64-server-7                      10 M
 evince                                                        x86_64                     3.28.2-5.el7                          @rhel-x86_64-server-7                     9.7 M
 evince-nautilus                                               x86_64                     3.28.2-5.el7                          @rhel-x86_64-server-7                      24 k
 gdm                                                           x86_64                     1:3.22.3-13.el7_4                     @rhel-x86_64-server-7                     2.1 M
 gnome-boxes                                                   x86_64                     3.22.4-4.el7                          @rhel-x86_64-server-7                     5.0 M
 gnome-classic-session                                         noarch                     3.22.2-10.el7                         @rhel-x86_64-server-7                     196 k
 gnome-clocks                                                  x86_64                     3.22.1-1.el7                          @rhel-x86_64-server-7                     1.3 M
 gnome-desktop3                                                x86_64                     3.28.2-2.el7                          @rhel-x86_64-server-7                     2.8 M
 gnome-font-viewer                                             x86_64                     3.22.0-1.el7                          @rhel-x86_64-server-7                     411 k
 gnome-session                                                 x86_64                     3.22.3-4.el7                          @rhel-x86_64-server-7                     1.5 M
 gnome-session-xsession                                        x86_64                     3.22.3-4.el7                          @rhel-x86_64-server-7                      16 k
 gnome-settings-daemon                                         x86_64                     3.22.2-5.el7                          @rhel-x86_64-server-7                     5.5 M
 gnome-shell                                                   x86_64                     3.22.3-17.el7                         @rhel-x86_64-server-7                     9.7 M
 gnome-shell-extension-alternate-tab                           noarch                     3.22.2-10.el7                         @rhel-x86_64-server-7                     9.9 k
 gnome-shell-extension-apps-menu                               noarch                     3.22.2-10.el7                         @rhel-x86_64-server-7                      32 k
 gnome-shell-extension-common                                  noarch                     3.22.2-10.el7                         @rhel-x86_64-server-7                     556 k
 gnome-shell-extension-launch-new-instance                     noarch                     3.22.2-10.el7                         @rhel-x86_64-server-7                     4.9 k
 gnome-shell-extension-places-menu                             noarch                     3.22.2-10.el7                         @rhel-x86_64-server-7                      23 k
 gnome-shell-extension-user-theme                              noarch                     3.22.2-10.el7                         @rhel-x86_64-server-7                     7.0 k
 gnome-shell-extension-window-list                             noarch                     3.22.2-10.el7                         @rhel-x86_64-server-7                      57 k
 gnome-software                                                x86_64                     3.22.7-1.el7                          @rhel-x86_64-server-7                     6.8 M
 gnome-tweak-tool                                              noarch                     3.22.0-1.el7                          installed                                 1.0 M
 mutter                                                        x86_64                     3.22.3-12.el7_4                       @rhel-x86_64-server-7                      11 M
 nautilus                                                      x86_64                     3.26.3.1-2.el7                        @rhel-x86_64-server-7                      15 M
 orca                                                          x86_64                     3.6.3-4.el7                           @rhel-x86_64-server-7                      13 M
 pulseaudio-gdm-hooks                                          x86_64                     10.0-5.el7                            @rhel-x86_64-server-7                     354  
 system-config-printer                                         x86_64                     1.4.1-21.el7                          @rhel-x86_64-server-7                     1.3 M
 totem                                                         x86_64                     1:3.26.2-1.el7                        @rhel-x86_64-server-7                     7.5 M
 totem-nautilus                                                x86_64                     1:3.26.2-1.el7                        @rhel-x86_64-server-7                      32 k

Transaction Summary
================================================================================================================================================================================
Remove  1 Package (+34 Dependent packages)

###NOTE### zlip update required for > 7.4 gnome packages 
########### CLUES #############


yum versionlock gnome-themes-standard-3.22.2-1.el7.x86_64


###############BREAK THROUGH: Remove minizip#####################

yum remove minizip-1.2.7-17.el7.x86_64
yum downgrade adwaita-gtk2-theme-3.22.2-1.el7.x86_64

yum install gnome-themes-standard-3.22.2-1.el7.x86_64

yum versionlock mutter-3.22.3-12.el7_4.x86_64

yum versionlock cheese-2:3.22.1-2.el7.x86_64 

yum versionlock cheese-libs-2:3.22.1-2.el7.x86_64

yum versionlock gnome-desktop3-3.22.2-2.el7.x86_64 

yum install gnome-shell gnome-software gnome-settings-daemon

Installed:
  gnome-settings-daemon.x86_64 0:3.22.2-5.el7          gnome-shell.x86_64 0:3.22.3-17.el7         
  gnome-software.x86_64 0:3.22.7-1.el7                

Dependency Installed:
  cheese-libs.x86_64 2:3.22.1-2.el7                  control-center.x86_64 1:3.22.2-5.el7          
  gdm.x86_64 1:3.22.3-13.el7_4                       gnome-desktop3.x86_64 0:3.22.2-2.el7          
  gnome-session.x86_64 0:3.22.3-4.el7                mutter.x86_64 0:3.22.3-12.el7_4               
  pulseaudio-gdm-hooks.x86_64 0:10.0-5.el7 

yum install eog-3.20.5-2.el7.x86_64

yum install empathy-3.12.12-4.el7.x86_64

yum downgrade nautilus-extensions-3.22.3-4.el7_4

yum install nautilus-3.22.3-4.el7_4.x86_64

yum install gnome-boxes-3.22.4-4.el7.x86_64

yum install gnome-classic-session-3.22.2-10.el7.noarch

  gnome-shell-extension-alternate-tab.noarch 0:3.22.2-10.el7                                       
  gnome-shell-extension-apps-menu.noarch 0:3.22.2-10.el7                                           
  gnome-shell-extension-common.noarch 0:3.22.2-10.el7                                              
  gnome-shell-extension-launch-new-instance.noarch 0:3.22.2-10.el7                                 
  gnome-shell-extension-places-menu.noarch 0:3.22.2-10.el7                                         
  gnome-shell-extension-window-list.noarch 0:3.22.2-10.el7                                         

yum install gnome-shell-extension-user-theme-3.22.2-10.el7.noarch
yum install gnome-tweak-tool
yum install gnome-session-xsession-3.22.3-4.el7.x86_64
yum downgrade evince-libs-3.22.1-5.2.el7_4.x86_64
yum install evince-3.22.1-5.2.el7_4.x86_64
yum install evince-nautilus-3.22.1-5.2.el7_4.x86_64
yum install gnome-clocks-3.22.1-1.el7.x86_64
yum install gnome-font-viewer-3.22.0-1.el7..x86_64
yum install orca
yum install system-config-printer
yum install totem-3.22.1-1.el7.x86_64
yum install totem-nautilus-3.22.1-1.el7.x86_64
yum downgrade libgnomekbd-3.22.0.1-1.el7.x86_64
yum install gnome-contacts-3.22.1-1.el7.x86_64
yum downgrade gnome-getting-started-docs-3.22.0-1.el7.noarch


### STILL DOESN'T WORK!!! ###
------------------------------------------------------------------------------------------
```

### Last Resort ### 

- reimage virtualbox: old version---VirtualBox: 5.1.22_115126_el7-1
 

## Work Flow for Checking Zombie Nodes

### Copy ticket content from "Systemcheck - Killed idle jobs on Nucleus"

```
vim dirtyTicket.txt

  # copy raw content into it

# parse out only node names
------------------------------------------------------------------------
grep 'not able' dirtyTicket.txt | awk '{print $1}' | uniq > dirtyRaw.txt
------------------------------------------------------------------------

# declare node list into an array
------------------------------------------------------------------------
dirtyNodesRaw=( $(cat dirtyRaw.txt) )
------------------------------------------------------------------------

# check array content, as well as # of elements
------------------------------------------------------------------------
for i in ${dirtyNodesRaw[@]}; do echo $i; done

 # for number of elements

echo ${#dirtyNodesRaw[@]}
------------------------------------------------------------------------

# isolate nodes with real zombie processes
------------------------------------------------------------------------
for i in ${dirtyNodesRaw[@]}; do echo $i; sudo ssh $i 'ps -aux | grep Z' ; done | awk '{print $1,$8,$11}' | grep -B5 Z | grep -i Nucleus

  ## to populate output into an array

dirtyNodesReal=( $(for i in ${dirtyNodesRaw[@]}; do echo $i; sudo ssh $i 'ps -aux | grep Z' ; done | awk '{print $1,$8,$11}' | grep -B5 Z | grep -i Nucleus) )

# verify the zombies on the real nodes

for i in ${dirtyNodesReal[@]}; do echo $i; sudo ssh $i 'ps -aux | grep Z'; done
------------------------------------------------------------------------

# check slurm state of each node
------------------------------------------------------------------------
for i in ${dirtyNodesReal[@]}; do echo $i; scontrol show node $i | grep -w State; done
------------------------------------------------------------------------

# check which zombie nodes are idle are draining and setting 
------------------------------------------------------------------------
for i in ${dirtyNodesReal[@]}; do echo $i; scontrol show node $i | grep -w 'State'; done | grep -iB 1 idle | grep -i nucleus

  # also sort out `alloc` nodes
------------------------------------------------------------------------

# further parse into last IP digits for quick reboot, by partition
------------------------------------------------------------------------
...<previous pipe> | grep NucleusA  | sort -V | grep -o '[[:digit:]]*'
------------------------------------------------------------------------
```

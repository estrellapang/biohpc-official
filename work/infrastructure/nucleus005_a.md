## Status & Debug On Nucleus005

### Memory: 10/10/2019

- Inquiry: high usage of SWAP space 

- Diagnosis: `vmstat 1` to watch live whether there are `si` & `so`'s happening 

  - if there are, then we **cannot** `swapoff -a` / `swapon -a` without losing processes 


- links: 

  - [forum tips](https://askubuntu.com/questions/1357/how-to-empty-swap-if-there-is-free-ram) 

  - [swaponoff basics 1](https://www.itzgeek.com/how-tos/mini-howtos/how-to-clear-swap-memory-in-linux-centos-rhel.html)

  - [swaponff basics + bashscript](https://www.tecmint.com/clear-ram-memory-cache-buffer-and-swap-space-on-linux/) 


### 02/04/2020 Increased Number of Opened File Descriptors

- changed per-user to 50000 (previously 130000 for all users) 

- [BioHPC Documentation](https://docs.biohpc.swmed.edu/dokuwiki/doku.php?id=nucleus_change_log)

- [resource](https://www.tecmint.com/increase-set-open-file-limits-in-linux/)

- [resource](https://easyengine.io/tutorials/linux/increase-open-files-limit/)

- [resource](https://www.cyberciti.biz/faq/linux-unix-nginx-too-many-open-files/)

\
\

### 5-27-2020: RHEL 7.6 Reset

- `/project/biohpcadmin/shared/Nucleus-DontTouch/Nucleus005-backup`

- restore /etc/profile  cron (directory?),  check /usr/local/bin/

- check motd, check sudoers, /etc/profile.d/module*

- restore xymon

  - /project/biohpcadmin/shared/rpms/xymon/rhel7/installit
 
  - yum locallinstall <xymon>.rpm

  - xymonclient.cfg /etc/xymon-client/



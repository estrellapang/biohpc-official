## IB Switch Initialization Configs & Update Procedures

### Switch Setup 

- **Essentials**

  - equipment: 

    - Serial Cable Connecting to Dedicated Port on Switch
    - USB to Serial Adapter to Laptop 

- Serial Terminal Connections on MAC:

  - [tutorial](https://software.intel.com/en-us/setting-up-serial-terminal-on-system-with-mac-os-x)

- Serial Connections on PC (PuTTY)

  - on SX60## switches:

```

9600 baud
8 data bits
1 stop bit
No parity
No flow control

```

  - on SB7### switches:

```
115200 baud
8 data bits
1 stop bit
No parity
XON/XOFF flow conrtol

```  

- **Check Available Network Hostnames**

  - via Nucleus003 & Nucleus006 

  - ###.###.53.### subnet

  - **DECIDE on HOSTNAME & I.P. AHEAD of TIME!**


- **Follow Docs Instructions on Initial Setup**

  - Go into configure mode:

    - u: admin, p: admin 

    - `enable` --> `configure terminal`

    - configure **Hostname**, **I.P.**
\
\
\

- **Follow Up Configuration**

  - test connection via Nucleus003/Nucleus006

  - **DISABLE SUBNETMANAGER**

```
# in config mode

show ib sm

no ib sm

show ib sm

```

### Switch Update Outlines

> Mellanox OS Updates are INCREMENTAL!

- `show version` --> check current OS version 
 
- `imagefetch ###.###.53.252` (fetch a update image from nucleus003)

- `show image(s?)` --> show available images`  

- image install --> Install image in current partition

  - Rotate to Second Partition, Reboot, and Install same new image on second partition

- Run script in `/project/biohpcadmin/shared/scripts/ib-check/ib-checl###` to see newly built connections

- for in in $(seq 046 - 057); do ssh ###.###.###.$i hostname &(do in background); done

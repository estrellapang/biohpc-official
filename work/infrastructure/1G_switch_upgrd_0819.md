## Notes on 1G Switch Upgrade on Aug. 25th, 2019

### Diagnositc Protocol

```
ssh into nucleus006 --> become root

# examine slurmctld log timestamped to date of operation, cat contents and grep for 'failed' 

cat /cm/log/slurmctld-20190826 | grep -i 'failed'

# note down associated job ID and grep it in the log

cat /cm/log/slurmctld-20190826 | grep -i 1289638

# go through grepped contents of each failed job, see if the assigned node fall within range of operated nodes


### `sacct` Commmand to bring out more details about job status ###

-------------------------------------------------------------------------------

# return job details during specified time internvals  

sacct -S <yr-mo-d>T<hr:min:sec> -E <yr-mo-d>T<hr:min:sec> 

# e.g. see failed/cancelled jobs 

sacct -S 2019-11-16T23:30:00 -E 2019-11-17T02:50:00 | grep -i FAILED 
sacct -S 2019-11-16T23:30:00 -E 2019-11-17T02:50:00 | grep -i CANCELLED

# see job fails since specified time

sacct -S <year>-<month>-<day>T<hr:min:sec>
sacct -S 2020-01-09T11:00:00 | grep -i FAILED
-------------------------------------------------------------------------------

### Disclose All Jobs on a Partition:

----------------------------------------------------------------------
# see how many nodes per user
`sacct -r <partition> -s running —format=User,nnodes | sort | uniq -c`

# see what nodes each user is using
`squeue -p <partition>` 
----------------------------------------------------------------------

```

### Incidents 

- switches on BF10, BI08 Timeout > 5 minutes

  - caused by faulty configuration on the replacement switch--despite the LED indicators indicating connections being up, all of the IPs could not join biohpc's VLAN (431) 

  - **nodes affected**: nfs001-002, backup002, Nucleus190-195, NucleusA239, NucleusB002-027, NucleusC02-028

### Users Affected 

Yanhe.Zhao@utsouthwestern.edu

- job IDs: 

 - 1287559 

 - 1287548 

 - 1287562 

 - 1287561 

Sahil.Nalawade@UTSouthwestern.edu 

- job IDs:

  - 1270504

  - 1270503

RUICHEN.RONG@UTSouthwestern.edu 

- job IDs: 

  - 1281649 
 
Erica.Villa@UTSouthwestern.edu
 
- job IDs: 

  - 1290603

  - 1292284


### Conclusion 

- **notify users of possible down time** 

- **have drained nodes standing by**

  - inside each rack receiving a switch upgrade

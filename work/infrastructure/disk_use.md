## Common Commands Usesd in Filesystems & Disk Usage, Performance

### File sizes

#### "du" command

- summarize size of one file/directory 

  - `du -sh <file>`

- summarize sizes of all sub-directories, specify depth level:

  - `du -h --max-depth=1 <file>`

#### Find files and determine their size

- `sudo lfs find <dir> -type f -user <uid> | xargs sudo du -sh`

  - `lfs find` is only for lustre filesystems 

  - alternatively, `-type d` OR `-group <gid>`

  - restrict to a specific depth with du (or else it will assign threads to sum the total size of the parent dir in addition to the individual subdirs) 

  - `sudo find <dir> - mindepth 1 -maxdepth 1 -type <f/d> -<group/user> | sudo xargs du -sh`

  - [find | xargs tip](https://stackoverflow.com/questions/18312935/find-file-in-linux-then-report-the-size-of-file-searched)

  - e.g

```
### find all files belonging "Chiang_lab" under galaxy data directories

sudo lfs find /project/apps/galaxy/ -type f -group Chiang_lab | grep -vi 'incoming' | sudo xargs du -sh > Chiang_lab_old_galaxy.list

# add comma as delimiter in place of spaces or tab characters --> staging for importing into Excel parseable formats i.e. .csv
-----------------------------------------------------------------------------------
cat Chiang_lab_old_galaxy.list |  tr -s '[:blank:]' ',' > Chiang_lab_old_galaxy.txt
-----------------------------------------------------------------------------------
```

- [how-to tr link](https://stackoverflow.com/questions/37405865/convert-txt-to-csv-in-shell)

\
\

- **Sort Output by MB,GB, and TB --> Large to Small**

```

  # Matches letter "G" and isolate only lines containing integers/numerical characters

  # `sort -V` sort small to large ONLY by the fields containing numbers (very nice shortcut)

  # grep for 'T' for TB-sized files 

--------------------------------------------------------
cat <outfile> | grep 'G' | grep '[[:digit:]]*' | sort -V
--------------------------------------------------------

```

\
\

### File & Filesystem Attributes

#### inodes

- locate inode's file name and directory

  - [link 1](https://unix.stackexchange.com/questions/35292/quickly-find-which-files-belongs-to-a-specific-inode-number)


\

#### STAT command

- `stat <file/dir>`

- Output explained

```
    File - The name of the file.
    Size - The Ssize of the file in bytes.
    Blocks - The number of allocated blocks the file takes.
    IO Block - The size in bytes of every block.
    File type - (ex. regular file, directory, symbolic link …)
    Device - Device number in hex and decimal.
    Inode - Inode number.
    Links - Number of hard links.
    Access - File permissions in the numeric and symbolic methods.
    Uid - User ID and name of the owner .
    Gid - Group ID and name of the owner.
    Context - The SELinux security context.
    Access - The last time the file was accessed.
    Modify - The last time the file’s content was modified.
    Change - The last time the file’s attribute or content was changed.
    Birth - File creation time (not supported in Linux) 

```

\

- `stat -f <file/dir>`
 
  - pull info on filesystem

- Output explained

```
    File - The name of the file.
    ID - File system ID in hex.
    Namelen - Maximum length of file names.
    Fundamental block size - The size of each block on the file system.
    Blocks:
        Total - Number of total blocks in file system.
        Free - Number of free blocks in file system.
        Available - Number of free blocks available to non-root users.
    Inodes:
        Total - Number of total inodes in file system.
        Free - Number of free inodes in file system.

```

\

- `stat -L <symlinks>

  - pull info on files that symlinks are pointing to:

  - if just stat <symlink>, we will only get info on the symlink itself


\

- `stat -t <file/dir>`

  - get terse info on output; good for scripting and parsing

\

- **references**

  - https://linuxize.com/post/stat-command-in-linux/


\
\
\

### Efficient File Removals

#### Parallel Deletion

- via `xargs`

  - e.g.

```
ls /endosome/.backup/backup_project/BICF/weekly.3/BICF/BICF_Core/shared/bicf_collaborations/ | xargs -n1 -P6 -I% rm -rfv /endosome/.backup/backup_project/BICF/weekly.3/BICF/BICF_Core/shared/bicf_collaborations/% > /project/biohpcadmin/zpang1/BICF_backup_accounting/delete_logs/weekly_collaborations_3.log &&
```


- links

  - [no such file or directory error](https://askubuntu.com/questions/623577/no-such-file-or-directory-when-trying-to-remove-a-file-but-the-file-exists)

\
\
\

### Disk Partitions

#### `parted` utility

- **resources**

  - [link 1](https://linoxide.com/linux-command/parted-commands-manage-disk-partition/)

  - [link 2](https://stackoverflow.com/questions/12313384/how-to-view-unallocated-free-space-on-a-hard-disk-through-terminal)

  - [link 3](https://www.eassos.com/blog/how-to-resize-ubuntu-partition-without-data-loss-in-ubuntu-windows/)

  - [link 4](https://linoxide.com/storage/mbr-vs-gpt-things-know-when-partitioning/)


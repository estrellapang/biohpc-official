## Status Updates & Change Proposals for BioHPC Gitlab: 2019 

### Ticket#2019082310008079 

> git's / directory running low as a result of old log files

- **added cronjob** 

-------------------------------------------------------------------------------
# delete git's LDAP & Nginx logs older than 1 month; Sunday afternoons circa 6 P.M. 
30 18 * * sun /bin/find /var/log/gitlab/gitlab-rails -type f -name "*.log*.gz" -mtime +28 -exec rm {} \;
51 18 * * sun /bin/find /var/log/gitlab/nginx -type f -name "*.log*.gz" -mtime +28 -exec rm {} \;
-------------------------------------------------------------------------------

- **proposal to change logrotation** 

Aside from cronjobs clearing out old compressed log files, I've noticed that the actual log files themselves, e.g. `/var/log/gitlab/nginx/access.log` actually grows to a substantial size:

------------------------------------------------------------------------------------------------------------------
[root@git nginx]# du -sh gitlab_access.log
1.6G    gitlab_access.log

# when the compressed log file is 20x times smaller

[root@git nginx]# du -sh gitlab_access.log-20190801.gz
67M    gitlab_access.log-20190801.gz
------------------------------------------------------------------------------------------------------------------

PROPOSAL:  configure gitlab to up the frequency of logrotation

`vi /etc/gitlab/gitlab.rb`

----------------------------------------------------------------------------------------------------------------
# Logrotate
logging['logrotate_frequency'] = "monthly"       # change this to bi-monthly or even weekly!
logging['logrotate_rotate'] = 300 
logging['logrotate_compress'] = "compress"
logging['logrotate_method'] = "copytruncate"
logging['logrotate_dateformat'] = "-%Y%m%d"
----------------------------------------------------------------------------------------------------------------


### relevant links 

- [link 1](https://docs.biohpc.swmed.edu/dokuwiki/doku.php?id=biohpc_git_change_log#upgrade_procedure_from_682_to_8110) 

- [link 2: ctime vs. mtime](https://www.quora.com/What-is-the-difference-between-mtime-atime-and-ctime)

- [link 3: crontab options](https://www.pantz.org/software/cron/croninfo.html)


## `/var/log/messages` grew past 1GB on Nucleus007 due to pulseaudio messages 

	Ticket#2019082010008021--> pulseaudio daemon keeps respawning

### Intial Status

```
[root@Nucleus007 ~]# du -sh /var/log/messages
1.8G	/var/log/messages

# opened file, full of pulseaudio "failed to start messages" 

# stopped pulseaudio daemon from loading as a module and respwaning:

[root@Nucleus007 ~]# vi /etc/pulse/daemon.conf 

## changed the following entries
--------------------------------------------------------------------

allow-module-loading = no  * previously `yes`

--------------------------------------------------------------------


[root@Nucleus007 ~]# vi /etc/pulse/client.conf

--------------------------------------------------------------------

autospawn = no  * previously `yes` 

--------------------------------------------------------------------

## no effect unless the server reboots...

# tried to kill the pulseaudio processes, but they would keep re-spawning...

```

### TEMPORARY Solution: force logrotate 


```
[root@Nucleus007 ~]# logrotate -vdf /etc/logrotate.conf  --> did a dry run to see what would happen 

[root@Nucleus007 ~]# logrotate -vf /etc/logrotate.conf  # real run


---------------------------------------------------------------------

reading config file /etc/logrotate.conf
including /etc/logrotate.d
reading config file bootlog
reading config file cesdr-log
reading config file chrony
reading config file cmdaemon
reading config file cups
reading config file iscsiuiolog
reading config file libvirtd
reading config file libvirtd.qemu
reading config file mmprotocoltrace
reading config file mmsysmonitor
reading config file named
reading config file numad
reading config file opensm
reading config file ppp
reading config file psacct
reading config file puppet
reading config file samba
olddir is now /var/log/samba/old
reading config file slurm
reading config file srp_daemon
reading config file subscription-manager
reading config file syslog
reading config file up2date
reading config file wpa_supplicant
reading config file xymon-client
reading config file yum
Allocating hash table for state file, size 15360 B

Handling 34 logs

rotating pattern: /var/log/boot.log
 forced from command line (7 rotations)
empty log files are rotated, old logs are removed
considering log /var/log/boot.log
  log needs rotating
rotating log /var/log/boot.log, log->rotateCount is 7
dateext suffix '-20190822'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
destination /var/log/boot.log-20190822 already exists, skipping rotation

rotating pattern: /var/adm/ras/mmcesdr.log  forced from command line (7 rotations)
empty log files are not rotated, log files >= 10485760 are rotated earlier, old logs are removed
considering log /var/adm/ras/mmcesdr.log
  log /var/adm/ras/mmcesdr.log does not exist -- skipping

rotating pattern: /var/log/chrony/*.log  forced from command line (4 rotations)
empty log files are rotated, old logs are removed
considering log /var/log/chrony/*.log
  log /var/log/chrony/*.log does not exist -- skipping
not running postrotate script, since no logs were rotated

rotating pattern: /var/log/cmdaemon /var/log/node-installer  forced from command line (4 rotations)
empty log files are rotated, old logs are removed
considering log /var/log/cmdaemon
  log needs rotating
considering log /var/log/node-installer
  log needs rotating
rotating log /var/log/cmdaemon, log->rotateCount is 4
dateext suffix '-20190822'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
rotating log /var/log/node-installer, log->rotateCount is 4
dateext suffix '-20190822'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
renaming /var/log/cmdaemon to /var/log/cmdaemon-20190822
creating new /var/log/cmdaemon mode = 0600 uid = 0 gid = 0
renaming /var/log/node-installer to /var/log/node-installer-20190822
creating new /var/log/node-installer mode = 0600 uid = 0 gid = 0
running postrotate script
removing old log /var/log/cmdaemon-20190729
removing old log /var/log/node-installer-20190729

rotating pattern: /var/spool/cmd/audit.log  forced from command line (4 rotations)
empty log files are rotated, old logs are removed
considering log /var/spool/cmd/audit.log
  log /var/spool/cmd/audit.log does not exist -- skipping

rotating pattern: /var/spool/cmd/actions.log  forced from command line (4 rotations)
empty log files are rotated, old logs are removed
considering log /var/spool/cmd/actions.log
  log /var/spool/cmd/actions.log does not exist -- skipping

rotating pattern: /var/spool/cmd/event.log  forced from command line (4 rotations)
empty log files are rotated, old logs are removed
considering log /var/spool/cmd/event.log
  log /var/spool/cmd/event.log does not exist -- skipping

rotating pattern: /var/spool/cmd/ec2.log  forced from command line (4 rotations)
empty log files are rotated, old logs are removed
considering log /var/spool/cmd/ec2.log
  log /var/spool/cmd/ec2.log does not exist -- skipping

rotating pattern: /var/log/openstack  forced from command line (3 rotations)
empty log files are rotated, old logs are removed
considering log /var/log/openstack
  log needs rotating
rotating log /var/log/openstack, log->rotateCount is 3
dateext suffix '-20190822'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
renaming /var/log/openstack to /var/log/openstack-20190822
creating new /var/log/openstack mode = 0600 uid = 0 gid = 0
removing old log /var/log/openstack-20190804

rotating pattern: /var/spool/cmd/openstack.log  forced from command line (3 rotations)
empty log files are rotated, old logs are removed
considering log /var/spool/cmd/openstack.log
  log /var/spool/cmd/openstack.log does not exist -- skipping

rotating pattern: /var/log/haproxy  forced from command line (3 rotations)
empty log files are rotated, old logs are removed
considering log /var/log/haproxy
  log needs rotating
rotating log /var/log/haproxy, log->rotateCount is 3
dateext suffix '-20190822'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
renaming /var/log/haproxy to /var/log/haproxy-20190822
creating new /var/log/haproxy mode = 0600 uid = 0 gid = 0
removing old log /var/log/haproxy-20190804

rotating pattern: /var/log/cups/*_log  forced from command line (4 rotations)
empty log files are not rotated, old logs are removed
considering log /var/log/cups/*_log
  log /var/log/cups/*_log does not exist -- skipping

rotating pattern: /var/log/iscsiuio.log  forced from command line (4 rotations)
empty log files are not rotated, old logs are removed
considering log /var/log/iscsiuio.log
  log /var/log/iscsiuio.log does not exist -- skipping
not running postrotate script, since no logs were rotated

rotating pattern: /var/log/libvirt/libvirtd.log  forced from command line (4 rotations)
empty log files are rotated, only log files >= 102400 bytes are rotated, old logs are removed
considering log /var/log/libvirt/libvirtd.log
  log /var/log/libvirt/libvirtd.log does not exist -- skipping

rotating pattern: /var/log/libvirt/qemu/*.log  forced from command line (4 rotations)
empty log files are rotated, only log files >= 102400 bytes are rotated, old logs are removed
considering log /var/log/libvirt/qemu/*.log
  log /var/log/libvirt/qemu/*.log does not exist -- skipping

rotating pattern: /var/adm/ras/mmprotocoltrace.log  forced from command line (30 rotations)
empty log files are not rotated, old logs are removed
considering log /var/adm/ras/mmprotocoltrace.log
  log /var/adm/ras/mmprotocoltrace.log does not exist -- skipping

rotating pattern: /var/adm/ras/mmsysmonitor*.log  forced from command line (30 rotations)
empty log files are not rotated, old logs are removed
considering log /var/adm/ras/mmsysmonitor.log
  log /var/adm/ras/mmsysmonitor.log is symbolic link. Rotation of symbolic links is not allowed to avoid security issues -- skipping.
considering log /var/adm/ras/mmsysmonitor.Nucleus007.log
  log needs rotating
rotating log /var/adm/ras/mmsysmonitor.Nucleus007.log, log->rotateCount is 30
dateext suffix '-20190822'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
destination /var/adm/ras/mmsysmonitor.Nucleus007.log-20190822.gz already exists, skipping rotation

rotating pattern: /var/named/data/named.run  forced from command line (4 rotations)
empty log files are rotated, old logs are removed
switching euid to 25 and egid to 25
considering log /var/named/data/named.run
  log /var/named/data/named.run does not exist -- skipping
switching euid to 0 and egid to 0

rotating pattern: /var/log/numad.log  forced from command line (5 rotations)
empty log files are rotated, old logs are removed
considering log /var/log/numad.log
  log /var/log/numad.log does not exist -- skipping

rotating pattern: /var/log/opensm.log  forced from command line (4 rotations)
empty log files are not rotated, old logs are removed
considering log /var/log/opensm.log
  log /var/log/opensm.log does not exist -- skipping

rotating pattern: /var/log/ppp/connect-errors  forced from command line (5 rotations)
empty log files are not rotated, old logs are removed
considering log /var/log/ppp/connect-errors
  log /var/log/ppp/connect-errors does not exist -- skipping

rotating pattern: /var/account/pacct  forced from command line (31 rotations)
empty log files are not rotated, old logs are removed
considering log /var/account/pacct
  log does not need rotating (log is empty)
rotating pattern: /var/log/puppet/*log  forced from command line (4 rotations)
empty log files are not rotated, old logs are removed
considering log /var/log/puppet/*log
  log /var/log/puppet/*log does not exist -- skipping
not running postrotate script, since no logs were rotated

rotating pattern: /var/log/samba/*  forced from command line (4 rotations)
olddir is /var/log/samba/old, empty log files are not rotated, old logs are removed
No logs found. Rotation not needed.

rotating pattern: /var/log/slurmd /var/log/slurmctld  forced from command line (5 rotations)
empty log files are not rotated, old logs are removed
considering log /var/log/slurmd
  log needs rotating
considering log /var/log/slurmctld
  log /var/log/slurmctld does not exist -- skipping
rotating log /var/log/slurmd, log->rotateCount is 5
dateext suffix '-20190822'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
glob finding logs to compress failed
glob finding old rotated logs failed
renaming /var/log/slurmd to /var/log/slurmd-20190822
creating new /var/log/slurmd mode = 0640 uid = 450 gid = 0
running postrotate script

rotating pattern: /var/log/srp_daemon  forced from command line (4 rotations)
empty log files are not rotated, old logs are removed
considering log /var/log/srp_daemon
  log /var/log/srp_daemon does not exist -- skipping

rotating pattern: /var/log/rhsm/*.log  forced from command line (4 rotations)
empty log files are not rotated, old logs are removed
considering log /var/log/rhsm/rhsm.log
  log needs rotating
rotating log /var/log/rhsm/rhsm.log, log->rotateCount is 4
dateext suffix '-20190822'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
renaming /var/log/rhsm/rhsm.log to /var/log/rhsm/rhsm.log-20190822
creating new /var/log/rhsm/rhsm.log mode = 0644 uid = 0 gid = 0
removing old log /var/log/rhsm/rhsm.log-20190729

rotating pattern: /var/log/cron
/var/log/maillog
/var/log/messages
/var/log/secure
/var/log/spooler
 forced from command line (4 rotations)
empty log files are rotated, old logs are removed
considering log /var/log/cron
  log needs rotating
considering log /var/log/maillog
  log needs rotating
considering log /var/log/messages
  log needs rotating
considering log /var/log/secure
  log needs rotating
considering log /var/log/spooler
  log needs rotating
rotating log /var/log/cron, log->rotateCount is 4
dateext suffix '-20190822'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
rotating log /var/log/maillog, log->rotateCount is 4
dateext suffix '-20190822'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
rotating log /var/log/messages, log->rotateCount is 4
dateext suffix '-20190822'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
rotating log /var/log/secure, log->rotateCount is 4
dateext suffix '-20190822'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
rotating log /var/log/spooler, log->rotateCount is 4
dateext suffix '-20190822'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
renaming /var/log/cron to /var/log/cron-20190822
creating new /var/log/cron mode = 0600 uid = 0 gid = 0
renaming /var/log/maillog to /var/log/maillog-20190822
creating new /var/log/maillog mode = 0600 uid = 0 gid = 0
renaming /var/log/messages to /var/log/messages-20190822
creating new /var/log/messages mode = 0640 uid = 0 gid = 4
renaming /var/log/secure to /var/log/secure-20190822
creating new /var/log/secure mode = 0600 uid = 0 gid = 0
renaming /var/log/spooler to /var/log/spooler-20190822
creating new /var/log/spooler mode = 0600 uid = 0 gid = 0
running postrotate script
removing old log /var/log/cron-20190729
removing old log /var/log/maillog-20190729
removing old log /var/log/messages-20190729
removing old log /var/log/secure-20190729
removing old log /var/log/spooler-20190729

rotating pattern: /var/log/up2date  forced from command line (4 rotations)
empty log files are rotated, old logs are removed
considering log /var/log/up2date
  log /var/log/up2date does not exist -- skipping

rotating pattern: /var/log/wpa_supplicant.log  forced from command line (4 rotations)
empty log files are not rotated, old logs are removed
considering log /var/log/wpa_supplicant.log
  log needs rotating
rotating log /var/log/wpa_supplicant.log, log->rotateCount is 4
dateext suffix '-20190822'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
glob finding old rotated logs failed
renaming /var/log/wpa_supplicant.log to /var/log/wpa_supplicant.log-20190822
creating new /var/log/wpa_supplicant.log mode = 0600 uid = 0 gid = 0

rotating pattern: /var/log/xymon/*.log  forced from command line (5 rotations)
empty log files are rotated, old logs are removed
considering log /var/log/xymon/xymonclient.log
  log needs rotating
considering log /var/log/xymon/xymonlaunch.log
  log needs rotating
rotating log /var/log/xymon/xymonclient.log, log->rotateCount is 5
dateext suffix '-20190822'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
compressing log with: /bin/gzip
rotating log /var/log/xymon/xymonlaunch.log, log->rotateCount is 5
dateext suffix '-20190822'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
compressing log with: /bin/gzip
renaming /var/log/xymon/xymonclient.log to /var/log/xymon/xymonclient.log-20190822
renaming /var/log/xymon/xymonlaunch.log to /var/log/xymon/xymonlaunch.log-20190822
running postrotate script
removing old log /var/log/xymon/xymonclient.log-20190721.gz
removing old log /var/log/xymon/xymonlaunch.log-20190721.gz

rotating pattern: /var/log/yum.log  forced from command line (4 rotations)
empty log files are not rotated, old logs are removed
considering log /var/log/yum.log
  log does not need rotating (log is empty)
rotating pattern: /var/log/wtmp  forced from command line (1 rotations)
empty log files are rotated, only log files >= 1048576 bytes are rotated, old logs are removed
considering log /var/log/wtmp
  log needs rotating
rotating log /var/log/wtmp, log->rotateCount is 1
dateext suffix '-20190822'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
renaming /var/log/wtmp to /var/log/wtmp-20190822
creating new /var/log/wtmp mode = 0664 uid = 0 gid = 22
removing old log /var/log/wtmp-20190411

rotating pattern: /var/log/btmp  forced from command line (1 rotations)
empty log files are rotated, old logs are removed
considering log /var/log/btmp
  log needs rotating
rotating log /var/log/btmp, log->rotateCount is 1
dateext suffix '-20190822'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
renaming /var/log/btmp to /var/log/btmp-20190822
creating new /var/log/btmp mode = 0600 uid = 0 gid = 22
removing old log /var/log/btmp-20190801

---------------------------------------------------------------------


# `messages` backed up 

[root@Nucleus007 ~]# du -sh /var/log/messages
192K	/var/log/messages

[root@Nucleus007 ~]# cd /var/log
[root@Nucleus007 log]# ll
total 2279600
drwxr-xr-x  2 root   root            6 Jun 26  2017 anaconda
drwx------  2 root   root           22 Jun 12  2017 audit
-rw-------  1 root   root            0 Aug 22 03:23 boot.log
-rw-------  1 root   root            0 Aug 16 03:32 boot.log-20190816
-rw-------  1 root   root            0 Aug 17 03:23 boot.log-20190817
-rw-------  1 root   root            0 Aug 18 03:24 boot.log-20190818
-rw-------  1 root   root            0 Aug 19 03:39 boot.log-20190819
-rw-------  1 root   root            0 Aug 20 03:42 boot.log-20190820
-rw-------  1 root   root            0 Aug 21 03:34 boot.log-20190821
-rw-------  1 root   root            0 Aug 22 03:23 boot.log-20190822
-rw-------  1 root   utmp            0 Aug 22 17:48 btmp
-rw-------  1 root   utmp        13056 Aug 22 16:05 btmp-20190822
drwxr-xr-x  2 chrony chrony          6 Apr 24  2017 chrony
-rw-------  1 root   root            0 Aug 22 17:48 cmdaemon
-rw-------  1 root   root       907580 Aug  4 03:06 cmdaemon-20190804
-rw-------  1 root   root      1077552 Aug 11 03:10 cmdaemon-20190811
-rw-------  1 root   root      1076723 Aug 18 03:24 cmdaemon-20190818
-rw-------  1 root   root       692145 Aug 22 17:48 cmdaemon-20190822
-rw-------  1 root   root            0 Aug 22 17:48 cron
-rw-------  1 root   root       134993 Aug  4 03:07 cron-20190804
-rw-------  1 root   root       158166 Aug 11 03:11 cron-20190811
-rw-------  1 root   root       158429 Aug 18 03:24 cron-20190818
-rw-------  1 root   root       103626 Aug 22 17:40 cron-20190822
drwxr-xr-x  2 lp     sys             6 Apr 10  2017 cups
-rw-r--r--  1 root   root       127124 May  1 10:26 dmesg
-rw-r--r--  1 root   root       122068 Dec  3  2018 dmesg.old
drwx--x--x  2 root   gdm          4096 May  1 10:28 gdm
drwxr-xr-x  2 root   root            6 May 30  2017 glusterfs
-rw-------  1 root   root        15023 Aug 20  2018 grubby
-rw-r--r--  1 root   root          592 Sep 28  2017 grubby_prune_debug
-rw-------  1 root   root            0 Aug 22 17:48 haproxy
-rw-------  1 root   root            0 Aug  4 03:07 haproxy-20190811
-rw-------  1 root   root            0 Aug 11 03:11 haproxy-20190818
-rw-------  1 root   root            0 Aug 18 03:24 haproxy-20190822
-rw-r--r--  1 root   root    123125304 Aug 22 16:46 lastlog
drwx------  3 root   root           17 Aug 22  2017 libvirt
-rw-------  1 root   root            0 Aug 22 17:48 maillog
-rw-------  1 root   root            0 Jul 29 03:42 maillog-20190804
-rw-------  1 root   root         2412 Aug  7 09:50 maillog-20190811
-rw-------  1 root   root        10860 Aug 13 18:10 maillog-20190818
-rw-------  1 root   root         1209 Aug 20 11:09 maillog-20190822
-rw-r-----+ 1 root   adm        227804 Aug 22 17:48 messages
-rw-r-----+ 1 root   adm      11700332 Aug  4 03:07 messages-20190804
-rw-r-----+ 1 root   adm      40141684 Aug 11 03:11 messages-20190811
-rw-r-----+ 1 root   adm     371963347 Aug 18 03:24 messages-20190818
-rw-r-----+ 1 root   adm    1783614489 Aug 22 17:48 messages-20190822 #HERE!!!#
-rw-r--r--  1 root   root          800 May  1 10:26 modified-by-node-installer.log
drwx------  2 daemon root           23 Apr 14  2017 munge
-rw-------  1 root   root            0 Aug 22 17:48 node-installer
-rw-------  1 root   root            0 Jul 29 03:42 node-installer-20190804
-rw-------  1 root   root            0 Aug  4 03:07 node-installer-20190811
-rw-------  1 root   root            0 Aug 11 03:11 node-installer-20190818
-rw-------  1 root   root            0 Aug 18 03:24 node-installer-20190822
-rw-r--r--  1 root   root        10097 May 16 12:02 ntp
drwxr-xr-x  2 ntp    ntp             6 Mar  1  2017 ntpstats
-rw-r--r--  1 root   root        26814 Nov  6  2018 nvidia-installer.log
-rw-------  1 root   root            0 Aug 22 17:48 openstack
-rw-------  1 root   root            0 Aug  4 03:07 openstack-20190811
-rw-------  1 root   root            0 Aug 11 03:11 openstack-20190818
-rw-------  1 root   root            0 Aug 18 03:24 openstack-20190822
-rw-rw-rw-  1 root   root       396337 Aug 12 16:21 pan_manage_routes.log
drwxr-xr-x  3 root   root           17 Jun 26  2017 pluto
drwx------  2 root   root            6 Jan 26  2014 ppp
drwxr-x---  2 puppet puppet          6 Apr 25  2016 puppet
drwxr-xr-x  2 root   root            6 May 19  2017 qemu-ga
drwxr-xr-x  2 root   root          117 Aug 22 17:48 rhsm
-rw-r--r--  1 root   root       531118 May  1 10:26 rsyncd.log
drwxr-xr-x  2 root   root         4096 Aug 22 00:00 sa
drwx------  3 root   root           16 Sep 14  2017 samba
-rw-------  1 root   root            0 Aug 22 17:48 secure
-rw-------  1 root   root       520591 Aug  4 03:05 secure-20190804
-rw-------  1 root   root       802944 Aug 11 03:06 secure-20190811
-rw-------  1 root   root       757173 Aug 18 03:22 secure-20190818
-rw-------  1 root   root       397441 Aug 22 17:43 secure-20190822
-rw-r-----  1 slurm  root          154 Aug 22 17:48 slurmd
-rw-------  1 root   root        26158 Aug 21 03:32 slurmd-20190822
drwx------  2 root   root            6 Jan 27  2014 speech-dispatcher
-rw-------  1 root   root            0 Aug 22 17:48 spooler
-rw-------  1 root   root            0 Jul 29 03:42 spooler-20190804
-rw-------  1 root   root            0 Aug  4 03:07 spooler-20190811
-rw-------  1 root   root            0 Aug 11 03:11 spooler-20190818
-rw-------  1 root   root            0 Aug 18 03:24 spooler-20190822
drwxr-xr-x  2 root   root            6 Jun 12  2017 tuned
-rw-------  1 root   root            0 Aug 22 17:48 wpa_supplicant.log
-rw-r--r--  1 root   root          120 May  1 11:13 wpa_supplicant.log-20190822
-rw-rw-r--  1 root   utmp            0 Aug 22 17:48 wtmp
-rw-rw-r--  1 root   utmp       465024 Aug 22 16:18 wtmp-20190822
-rw-r--r--  1 root   root       201896 Aug 22 17:01 Xorg.0.log
-rw-r--r--  1 root   root       150651 Apr 22 17:08 Xorg.0.log.old
-rw-r--r--  1 root   root         5116 Sep 13  2018 Xorg.1.log
-rw-r--r--  1 root   root         5116 Sep 13  2018 Xorg.2.log
-rw-r--r--  1 root   root         5116 Sep 13  2018 Xorg.3.log
-rw-r--r--  1 root   root         5116 Sep 13  2018 Xorg.4.log
-rw-r--r--  1 root   root         5116 Sep 13  2018 Xorg.5.log
drwxr-xr-x  2 xymon  xymon        4096 Aug 22 17:48 xymon
-rw-------  1 root   root            0 May 11 03:46 yum.log
-rw-------  1 root   root           57 Sep 16  2018 yum.log-20180917
-rw-------  1 root   root          568 Nov 28  2018 yum.log-20181204
-rw-------  1 root   root          110 Dec 14  2018 yum.log-20190101
-rw-------  1 root   root          122 May 10 17:29 yum.log-20190511

---------------------------------------------------------------------


### Update: Aug. 31st, 2019 ### 

- kernel messages filled `/var/log/messages` past 1G once more 

```
[root@Nucleus007 ~]# du -sh /var/log/messages
1.3G	/var/log/messages

```

# as of August 31st, 2019 @ 7:43 P.M. 

[root@Nucleus007 ~]# ls -lht /var/log/ | grep -i messages
-rw-r-----+ 1 root   adm    1.1G Aug 31 19:42 messages
-rw-r-----+ 1 root   adm    670M Aug 25 03:46 messages-20190825
-rw-r-----+ 1 root   adm    1.7G Aug 22 17:48 messages-20190822
-rw-r-----+ 1 root   adm    355M Aug 18 03:24 messages-20190818
-rw-r-----+ 1 root   adm     39M Aug 11 03:11 messages-20190811

### FORCE logrotate once more ###


[root@Nucleus007 ~]# logrotate -vf /etc/logrotate.conf

---------------------------------------------------------------------

[root@Nucleus007 ~]# [root@Nucleus007 ~]# ls -lht /var/log/ | grep -i messages
bash: [root@Nucleus007: command not found...
[root@Nucleus007 ~]# -rw-r-----+ 1 root   adm    1.1G Aug 31 19:42 messages
bash: -rw-r-----+: command not found...
[root@Nucleus007 ~]# -rw-r-----+ 1 root   adm    670M Aug 25 03:46 messages-20190825
bash: -rw-r-----+: command not found...
[root@Nucleus007 ~]# -rw-r-----+ 1 root   adm    1.7G Aug 22 17:48 messages-20190822
bash: -rw-r-----+: command not found...
[root@Nucleus007 ~]# -rw-r-----+ 1 root   adm    355M Aug 18 03:24 messages-20190818
bash: -rw-r-----+: command not found...
[root@Nucleus007 ~]# -rw-r-----+ 1 root   adm     39M Aug 11 03:11 messages-20190811^C
[root@Nucleus007 ~]# 
[root@Nucleus007 ~]# 
[root@Nucleus007 ~]# 
[root@Nucleus007 ~]# 
[root@Nucleus007 ~]# logrotate -vf /etc/logrotate.conf
reading config file /etc/logrotate.conf
including /etc/logrotate.d
reading config file bootlog
reading config file cesdr-log
reading config file chrony
reading config file cmdaemon
reading config file cups
reading config file iscsiuiolog
reading config file libvirtd
reading config file libvirtd.qemu
reading config file mmprotocoltrace
reading config file mmsysmonitor
reading config file named
reading config file numad
reading config file opensm
reading config file ppp
reading config file psacct
reading config file puppet
reading config file samba
olddir is now /var/log/samba/old
reading config file slurm
reading config file srp_daemon
reading config file subscription-manager
reading config file syslog
reading config file up2date
reading config file wpa_supplicant
reading config file xymon-client
reading config file yum
Allocating hash table for state file, size 15360 B

Handling 34 logs

rotating pattern: /var/log/boot.log
 forced from command line (7 rotations)
empty log files are rotated, old logs are removed
considering log /var/log/boot.log
  log needs rotating
rotating log /var/log/boot.log, log->rotateCount is 7
dateext suffix '-20190831'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
destination /var/log/boot.log-20190831 already exists, skipping rotation

rotating pattern: /var/adm/ras/mmcesdr.log  forced from command line (7 rotations)
empty log files are not rotated, log files >= 10485760 are rotated earlier, old logs are removed
considering log /var/adm/ras/mmcesdr.log
  log /var/adm/ras/mmcesdr.log does not exist -- skipping

rotating pattern: /var/log/chrony/*.log  forced from command line (4 rotations)
empty log files are rotated, old logs are removed
considering log /var/log/chrony/*.log
  log /var/log/chrony/*.log does not exist -- skipping
not running postrotate script, since no logs were rotated

rotating pattern: /var/log/cmdaemon /var/log/node-installer  forced from command line (4 rotations)
empty log files are rotated, old logs are removed
considering log /var/log/cmdaemon
  log needs rotating
considering log /var/log/node-installer
  log needs rotating
rotating log /var/log/cmdaemon, log->rotateCount is 4
dateext suffix '-20190831'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
rotating log /var/log/node-installer, log->rotateCount is 4
dateext suffix '-20190831'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
renaming /var/log/cmdaemon to /var/log/cmdaemon-20190831
creating new /var/log/cmdaemon mode = 0600 uid = 0 gid = 0
renaming /var/log/node-installer to /var/log/node-installer-20190831
creating new /var/log/node-installer mode = 0600 uid = 0 gid = 0
running postrotate script
removing old log /var/log/cmdaemon-20190811
removing old log /var/log/node-installer-20190811

rotating pattern: /var/spool/cmd/audit.log  forced from command line (4 rotations)
empty log files are rotated, old logs are removed
considering log /var/spool/cmd/audit.log
  log /var/spool/cmd/audit.log does not exist -- skipping

rotating pattern: /var/spool/cmd/actions.log  forced from command line (4 rotations)
empty log files are rotated, old logs are removed
considering log /var/spool/cmd/actions.log
  log /var/spool/cmd/actions.log does not exist -- skipping

rotating pattern: /var/spool/cmd/event.log  forced from command line (4 rotations)
empty log files are rotated, old logs are removed
considering log /var/spool/cmd/event.log
  log /var/spool/cmd/event.log does not exist -- skipping

rotating pattern: /var/spool/cmd/ec2.log  forced from command line (4 rotations)
empty log files are rotated, old logs are removed
considering log /var/spool/cmd/ec2.log
  log /var/spool/cmd/ec2.log does not exist -- skipping

rotating pattern: /var/log/openstack  forced from command line (3 rotations)
empty log files are rotated, old logs are removed
considering log /var/log/openstack
  log needs rotating
rotating log /var/log/openstack, log->rotateCount is 3
dateext suffix '-20190831'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
renaming /var/log/openstack to /var/log/openstack-20190831
creating new /var/log/openstack mode = 0600 uid = 0 gid = 0
removing old log /var/log/openstack-20190818

rotating pattern: /var/spool/cmd/openstack.log  forced from command line (3 rotations)
empty log files are rotated, old logs are removed
considering log /var/spool/cmd/openstack.log
  log /var/spool/cmd/openstack.log does not exist -- skipping

rotating pattern: /var/log/haproxy  forced from command line (3 rotations)
empty log files are rotated, old logs are removed
considering log /var/log/haproxy
  log needs rotating
rotating log /var/log/haproxy, log->rotateCount is 3
dateext suffix '-20190831'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
renaming /var/log/haproxy to /var/log/haproxy-20190831
creating new /var/log/haproxy mode = 0600 uid = 0 gid = 0
removing old log /var/log/haproxy-20190818

rotating pattern: /var/log/cups/*_log  forced from command line (4 rotations)
empty log files are not rotated, old logs are removed
considering log /var/log/cups/*_log
  log /var/log/cups/*_log does not exist -- skipping

rotating pattern: /var/log/iscsiuio.log  forced from command line (4 rotations)
empty log files are not rotated, old logs are removed
considering log /var/log/iscsiuio.log
  log /var/log/iscsiuio.log does not exist -- skipping
not running postrotate script, since no logs were rotated

rotating pattern: /var/log/libvirt/libvirtd.log  forced from command line (4 rotations)
empty log files are rotated, only log files >= 102400 bytes are rotated, old logs are removed
considering log /var/log/libvirt/libvirtd.log
  log /var/log/libvirt/libvirtd.log does not exist -- skipping

rotating pattern: /var/log/libvirt/qemu/*.log  forced from command line (4 rotations)
empty log files are rotated, only log files >= 102400 bytes are rotated, old logs are removed
considering log /var/log/libvirt/qemu/*.log
  log /var/log/libvirt/qemu/*.log does not exist -- skipping

rotating pattern: /var/adm/ras/mmprotocoltrace.log  forced from command line (30 rotations)
empty log files are not rotated, old logs are removed
considering log /var/adm/ras/mmprotocoltrace.log
  log /var/adm/ras/mmprotocoltrace.log does not exist -- skipping

rotating pattern: /var/adm/ras/mmsysmonitor*.log  forced from command line (30 rotations)
empty log files are not rotated, old logs are removed
considering log /var/adm/ras/mmsysmonitor.log
  log /var/adm/ras/mmsysmonitor.log is symbolic link. Rotation of symbolic links is not allowed to avoid security issues -- skipping.
considering log /var/adm/ras/mmsysmonitor.Nucleus007.log
  log needs rotating
rotating log /var/adm/ras/mmsysmonitor.Nucleus007.log, log->rotateCount is 30
dateext suffix '-20190831'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
destination /var/adm/ras/mmsysmonitor.Nucleus007.log-20190831.gz already exists, skipping rotation

rotating pattern: /var/named/data/named.run  forced from command line (4 rotations)
empty log files are rotated, old logs are removed
switching euid to 25 and egid to 25
considering log /var/named/data/named.run
  log /var/named/data/named.run does not exist -- skipping
switching euid to 0 and egid to 0

rotating pattern: /var/log/numad.log  forced from command line (5 rotations)
empty log files are rotated, old logs are removed
considering log /var/log/numad.log
  log /var/log/numad.log does not exist -- skipping

rotating pattern: /var/log/opensm.log  forced from command line (4 rotations)
empty log files are not rotated, old logs are removed
considering log /var/log/opensm.log
  log /var/log/opensm.log does not exist -- skipping

rotating pattern: /var/log/ppp/connect-errors  forced from command line (5 rotations)
empty log files are not rotated, old logs are removed
considering log /var/log/ppp/connect-errors
  log /var/log/ppp/connect-errors does not exist -- skipping

rotating pattern: /var/account/pacct  forced from command line (31 rotations)
empty log files are not rotated, old logs are removed
considering log /var/account/pacct
  log does not need rotating (log is empty)
rotating pattern: /var/log/puppet/*log  forced from command line (4 rotations)
empty log files are not rotated, old logs are removed
considering log /var/log/puppet/*log
  log /var/log/puppet/*log does not exist -- skipping
not running postrotate script, since no logs were rotated

rotating pattern: /var/log/samba/*  forced from command line (4 rotations)
olddir is /var/log/samba/old, empty log files are not rotated, old logs are removed
No logs found. Rotation not needed.

rotating pattern: /var/log/slurmd /var/log/slurmctld  forced from command line (5 rotations)
empty log files are not rotated, old logs are removed
considering log /var/log/slurmd
  log needs rotating
considering log /var/log/slurmctld
  log /var/log/slurmctld does not exist -- skipping
rotating log /var/log/slurmd, log->rotateCount is 5
dateext suffix '-20190831'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
compressing log with: /bin/gzip
renaming /var/log/slurmd to /var/log/slurmd-20190831
creating new /var/log/slurmd mode = 0640 uid = 450 gid = 0
running postrotate script

rotating pattern: /var/log/srp_daemon  forced from command line (4 rotations)
empty log files are not rotated, old logs are removed
considering log /var/log/srp_daemon
  log /var/log/srp_daemon does not exist -- skipping

rotating pattern: /var/log/rhsm/*.log  forced from command line (4 rotations)
empty log files are not rotated, old logs are removed
considering log /var/log/rhsm/rhsm.log
  log needs rotating
rotating log /var/log/rhsm/rhsm.log, log->rotateCount is 4
dateext suffix '-20190831'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
renaming /var/log/rhsm/rhsm.log to /var/log/rhsm/rhsm.log-20190831
creating new /var/log/rhsm/rhsm.log mode = 0644 uid = 0 gid = 0
removing old log /var/log/rhsm/rhsm.log-20190811

rotating pattern: /var/log/cron
/var/log/maillog
/var/log/messages
/var/log/secure
/var/log/spooler
 forced from command line (4 rotations)
empty log files are rotated, old logs are removed
considering log /var/log/cron
  log needs rotating
considering log /var/log/maillog
  log needs rotating
considering log /var/log/messages
  log needs rotating
considering log /var/log/secure
  log needs rotating
considering log /var/log/spooler
  log needs rotating
rotating log /var/log/cron, log->rotateCount is 4
dateext suffix '-20190831'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
rotating log /var/log/maillog, log->rotateCount is 4
dateext suffix '-20190831'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
rotating log /var/log/messages, log->rotateCount is 4
dateext suffix '-20190831'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
rotating log /var/log/secure, log->rotateCount is 4
dateext suffix '-20190831'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
rotating log /var/log/spooler, log->rotateCount is 4
dateext suffix '-20190831'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
renaming /var/log/cron to /var/log/cron-20190831
creating new /var/log/cron mode = 0600 uid = 0 gid = 0
renaming /var/log/maillog to /var/log/maillog-20190831
creating new /var/log/maillog mode = 0600 uid = 0 gid = 0
renaming /var/log/messages to /var/log/messages-20190831
creating new /var/log/messages mode = 0640 uid = 0 gid = 4
renaming /var/log/secure to /var/log/secure-20190831
creating new /var/log/secure mode = 0600 uid = 0 gid = 0
renaming /var/log/spooler to /var/log/spooler-20190831
creating new /var/log/spooler mode = 0600 uid = 0 gid = 0
running postrotate script
removing old log /var/log/cron-20190811
removing old log /var/log/maillog-20190811
removing old log /var/log/messages-20190811
removing old log /var/log/secure-20190811
removing old log /var/log/spooler-20190811

rotating pattern: /var/log/up2date  forced from command line (4 rotations)
empty log files are rotated, old logs are removed
considering log /var/log/up2date
  log /var/log/up2date does not exist -- skipping

rotating pattern: /var/log/wpa_supplicant.log  forced from command line (4 rotations)
empty log files are not rotated, old logs are removed
considering log /var/log/wpa_supplicant.log
  log does not need rotating (log is empty)
rotating pattern: /var/log/xymon/*.log  forced from command line (5 rotations)
empty log files are rotated, old logs are removed
considering log /var/log/xymon/xymonclient.log
  log needs rotating
considering log /var/log/xymon/xymonlaunch.log
  log needs rotating
rotating log /var/log/xymon/xymonclient.log, log->rotateCount is 5
dateext suffix '-20190831'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
compressing log with: /bin/gzip
rotating log /var/log/xymon/xymonlaunch.log, log->rotateCount is 5
dateext suffix '-20190831'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
compressing log with: /bin/gzip
renaming /var/log/xymon/xymonclient.log to /var/log/xymon/xymonclient.log-20190831
renaming /var/log/xymon/xymonlaunch.log to /var/log/xymon/xymonlaunch.log-20190831
running postrotate script
removing old log /var/log/xymon/xymonclient.log-20190804.gz
removing old log /var/log/xymon/xymonlaunch.log-20190804.gz

rotating pattern: /var/log/yum.log  forced from command line (4 rotations)
empty log files are not rotated, old logs are removed
considering log /var/log/yum.log
  log needs rotating
rotating log /var/log/yum.log, log->rotateCount is 4
dateext suffix '-20190831'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
renaming /var/log/yum.log to /var/log/yum.log-20190831
creating new /var/log/yum.log mode = 0600 uid = 0 gid = 0
removing old log /var/log/yum.log-20180917

rotating pattern: /var/log/wtmp  forced from command line (1 rotations)
empty log files are rotated, only log files >= 1048576 bytes are rotated, old logs are removed
considering log /var/log/wtmp
  log needs rotating
rotating log /var/log/wtmp, log->rotateCount is 1
dateext suffix '-20190831'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
renaming /var/log/wtmp to /var/log/wtmp-20190831
creating new /var/log/wtmp mode = 0664 uid = 0 gid = 22
removing old log /var/log/wtmp-20190822

rotating pattern: /var/log/btmp  forced from command line (1 rotations)
empty log files are rotated, old logs are removed
considering log /var/log/btmp
  log needs rotating
rotating log /var/log/btmp, log->rotateCount is 1
dateext suffix '-20190831'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
renaming /var/log/btmp to /var/log/btmp-20190831
creating new /var/log/btmp mode = 0600 uid = 0 gid = 22
removing old log /var/log/btmp-20190822

---------------------------------------------------------------------

# AFTER Rotation: 

---------------------------------------------------------------------
[root@Nucleus007 ~]# ls -lht /var/log/ | grep -i messages
-rw-r-----+ 1 root   adm    213K Aug 31 19:46 messages
-rw-r-----+ 1 root   adm    1.1G Aug 31 19:45 messages-20190831
-rw-r-----+ 1 root   adm    670M Aug 25 03:46 messages-20190825
-rw-r-----+ 1 root   adm    1.7G Aug 22 17:48 messages-20190822
-rw-r-----+ 1 root   adm    355M Aug 18 03:24 messages-20190818
---------------------------------------------------------------------

## Lustre Quota Management

\
\

### System Wide Quota Configs

#### Check if Quota is enabled

```
# On both MDS and OSS

lctl get_param osd-*.*.quota_slave.info | grep enabled
```

#### Enable both User and Group Quota


```
lctl conf_param project.quota.mdt=ug
lctl conf_param project.quota.ost=ug
```

#### Resources

- [enabling disk quota 1](https://programmersought.com/article/12125256836/)


\
\


### Query Quota for Every Group

#### parse out all gids

  - `sudo ldapsearch -x cn=* | grep -i gidNumber | sort -V | uniq`

  - -OR- `sudo ldapsearch -x cn=* | grep -i gidNumber | sort -V | sort -u`

  - 1-11-2021: 409 groups as it stands

```
@Nucleus006 ~]$ sudo ldapsearch -x cn=* | grep -i gidNumber | sort -V | sort -u | wc -l
409
```

\

#### query quota for all gids

- **RAW Quota for all Groups**

  - `sudo ldapsearch -x cn=* | grep -i gidNumber | sort -V | sort -u | awk -F: '{ print $2 }' | xargs -n1 -I% sh -c 'echo "GID=%";sudo lfs quota -g % -h /project'`

  - **FROM RAW OUTPUT**: parse out only filecount into a separate txt file: `cat <rawOutput> | awk '{print $6}'| grep -w '[[:digit:]]*' | grep -v gid | grep -v GID | tr -s '\n' >> <parsed_file>.txt`

  - **FROM RAW OUTPUT**: parse out a specific storage usage amount e.g. groups with ~1TB: `cat <output file> | awk '{print $2}' | grep "^1" | grep T`

  
\

- **Grep for only group space usage**

  - `sudo ldapsearch -x cn=* | grep -i gidNumber | sort -V | sort -u | awk -F: '{ print $2 }' | xargs -n1 -I{} sh -c "echo 'GID={}' ; sudo lfs quota -g {} -h /project | awk '{print \$2}' | grep -w '[[:digit:]]*'"`

  - for DEBUGGING, insert `| head -n 3` before piping into `xargs`

  - to quickly parse out the top usage, simply append `| grep -vi 'GID' | sort -V` to the end of the pipe above

  - **FROM RAW OUTPUT**: list TOP **ANY NUM** groups with most space usage: `cat <output file> | awk '{ print $2 }' | grep -w '[[:digit:]]*' | tr -s '\n' | grep T | sort -rV | head | xargs -n1 -I% grep -B3 %  <output file>`

    - **format into readable format**

```
# store top users into array, in e.g., top 17

top17=( $(cat <OUTPUT FILE> | awk '{ print $2 }' | grep -w '[[:digit:]]*' | tr -s '\n' | grep T | sort -rV | head -n 17 | xargs -n1 -I% grep -B3 %  lab_quota_03182021.txt | grep GID | awk -F '=' '{print $2}' | xargs getent group | awk -F: '{print $1}') )

# output stats into a placeholder file

cat lab_quota_03182021.txt | awk '{ print $2 }' | grep -w '[[:digit:]]*' | tr -s '\n' | grep T | sort -rV | head -n 17 | xargs -n1 -I% grep -B3 %  lab_quota_03182021.txt | grep T | awk '{ print $1,$2,$3,$4,$5,$6 }' > top_17_group_stats_raw.txt

  # e.g. of content
--------------------
/project 265.1T 0k 0k - 6481816
/project 227.7T 500T 510T - 43572523
/project 193.6T 240T 250T - 224947692
/project 167.4T 200T 220T - 5081264
/project 117.8T 300T 320T - 23080477
/project 106.7T 107T 120T - 8027132
/project 102.8T 100T 120T - 8509024
/project 95.54T 100T 110T - 1455696
/project 87.16T 180T 200T - 453364
/project 79.26T 80T 90T - 15366923
/project 76.06T 100T 120T - 2979488
/project 62.38T 100T 120T - 8534726
...
--------------------

# iterate through array and concatenate group name with corresponding stats:
i=0; while read line; do echo ${top17[i]} $line; ((++i)); done < top_17_group_stats_raw.txt | awk '{print $1,$3,$7}'

  # e.g. of content - Mar. 2021

--------------------
astrocyte 265.1T 6481816
Danuser_lab 227.7T 43572523
DLLab 193.6T 224947692
BICF_Core 167.4T 5081264
Li_lab 117.8T 23080477
Nicastro_lab 106.7T 8027132
Cantarel_lab 102.8T 8509024
BICF_Kidney 95.54T 1455696
CryoEM_Transfer 87.16T 453364
Hon_lab 79.26T 15366923
PCDC_Core 76.06T 2979488
Xiao_lab 62.38T 8534726
Rajaram_lab 62.03T 12117930
Lee_lab 55.79T 2511160
PHG_Clinical 51.05T 2559785
trivedi_ansir 49.24T 11339510
Wakeland_lab 48.96T 16632677
-------------------
```

\

- **Grep for files per group**

- each GID and its corresponding # of files:

  - `sudo ldapsearch -x cn=* | grep -i gidNumber | sort -V | sort -u | awk -F: '{ print $2 }' | head -n 3 | xargs -n1 -I{} sh -c "echo 'GID={}' ; sudo lfs quota -g {} -h /project | awk '{print \$6}' | grep -w '[[:digit:]]*'" | grep -v gid`

- get only numerical column for calculating sum, append `| grep -v GID` to end of pipe above

- **FROM RAW OUTPUT FILE**: list TOP 10 groups with highest number of files: `cat <output file> | awk '{print $6}'| grep -w '[[:digit:]]*' | grep -v gid | grep -v GID | tr -s '\n' | sort -rV | head -n 10 | xargs -n1 -I% grep -B3 % <output file>`   

\

- **Calculate Total # of Files in LustreFS**

```
module load python/3.7.x-anaconda

# plug in the appropriate data file into the .py script
python3.7 /home2/zpang1/lustre_gauge_datamove/lustreFileCount.py

# sample output
----------------------------------------------------
python3.7 lustreFileCount.py
# of rows in dataframe: 409
Total number of Files on /project 942110641
----------------------------------------------------

----------------------------------------------------
python3.7

941527239 - 941133251
----------------------------------------------------
``` 
 
\
\
\

### Top Usage by Lab 

#### Concise Method: Updated June 2021

- **Generate Quota Report for ALL GIDS**
----------------------------------------------------
sudo ldapsearch -x cn=* | grep -i gidNumber | sort -V | sort -u | awk -F: '{ print $2 }' | xargs -n1 -I% sh -c 'echo "GID=%";sudo lfs quota -g % -h /project' >> <output_file>.txt
----------------------------------------------------


- **Fetch Top Groups by Data Usage**

----------------------------------------------------
cat <output_file>.txt | awk '{ print $2 }' | tr -s '\n' | grep T | sort -rV | head -n <ANY INTEGER> | xargs -n1 -I% grep -B3 % <output_file>.txt | grep T | awk '{ print $1,$2,$3,$4,$5,$6 }' > <top_group_raw>.txt
----------------------------------------------------

- **Populate Top Group GIDs into an Array**

----------------------------------------------------
topGroups=( $(cat <OUTPUT_FILE> | awk '{ print $2 }' | tr -s '\n' | grep T | sort -rV | head -n <ANY_INTEGER> | xargs -n1 -I% grep -B3 %  <output_file>.txt | grep GID | awk -F '=' '{print $2}' | xargs getent group | awk -F: '{print $1}') )
----------------------------------------------------

- **Substitute '<top_group_raw>'.txt into concise format**

----------------------------------------------------
i=0; while read line; do echo ${topGroups[i]} $line; ((++i)); done < <top_group_raw.txt> | awk '{print $1,$3,$7}'
----------------------------------------------------

\
\

### Groups Exceeded Quota Limit

- **identify quota breaching groups**

----------------------------------------------------
breachGroups=( $(grep '*' <output_file>.txt  | awk '{print $2}' | awk -F* '{print $1}' | xargs -n1 -I% grep -B3 % <output_file>.txt | grep GID | awk -F '=' '{print $2}' | xargs getent group | awk -F: '{print $1}') )
----------------------------------------------------

- **output each group's current quota**

----------------------------------------------------
for i in ${breachGroups[@]}; do echo "=======$i======="; sudo lfs quota -g $i -h /project; done | grep -v 'Disk' | awk '{print $1,$2,$6}' | sed 's|Filesystem||g;s|/project||g' | sed 's/ //1' | sed 's/used/| storage |/g; s/files/file_count |/g'
----------------------------------------------------

- **output each group's individual quota**

----------------------------------------------------
for i in ${breachGroups[@]}; do echo "=======$i======="; getent group $i | cut -f 1,2,3 -d ':' --complement | tr ',' '\n' | xargs -n1 -I% sh -c 'getent passwd % | cut -f 5 -d : | cut -f 1 -d , && lfs quota -hu % /project 2> /dev/null'; done |  grep -v Disk | awk '{print $1,$2,$6}' | sed 's|Filesystem||g;s|/project||g' | sed 's/ //1' | sed 's/used/| storage |/g; s/files/file_count |/g'
----------------------------------------------------

- Full script at: `../../scripts/bash/lustre_quota_check.sh`

\
\

### Account for Files OUTSIDE of dedicated locations

#### NOTES

- general locations where users' files may be found outside of their lab directories

  - `/project/apps/astrocyte/astrocyte_<incoming/outgoing>/<uid>`

  - `/project/flash_ftp/<uid>`

  - `/project/thunder_ftp/<uid>`

  - `/project/shared/<dirs>`

\
\

#### Methodology


```
# Launch `lfs find` query

nohup ls /project | grep -v BICF | sudo xargs -P7 -n1 -I% lfs find /project/% -t f -g BICF >> BICF_dislocated_files.txt

signficantDislocated=$( (cat BICF_dislocated_files.txt | sudo xargs du -sh | awk '{print $1}' | grep 'M\|G') )

for i in ${signficantDislocated[@]}; do grep $i BICF_dislocated_files.txt >> BICF_dislocated_files_final.txt; done

\

# Populate Array for MegaByte and GigaByte-sized files

dislocatedM=( $(cat BICF_dislocated_files_final.txt | awk '{print $1}' | grep 'M' | sed 's/M//g') )

\

# Populate a string variable with file sizes for `bc` command next 

  # NO CAPITAL LETTERS

bcmlist=$(for i in ${dislocatedM[@]}; do echo -n "+ $i "; done)

  # the operations stored in variable
---------------------------------------------------------------
echo $bcmlist
+ 12 + 885 + 149 + 882 + 7.8 + 238 + 1.4 + 1.5 + 7.7 + 342 + 3.1 + 3.1 + 2.4 + 15 + 975 + 814 + 888 + 320 + 463 + 2.5 + 329 + 2.7 + 16 + 2.2 + 149 + 1.9 + 1.9 + 28 + 28 + 1.6 + 2.0 + 2.0 + 2.0 + 2.0 + 149 + 1.9 + 1.9 + 3.1 + 3.1 + 28 + 28 + 329 + 3.9 + 3.9 + 329 + 3.9 + 3.9 + 975 + 75 + 75 + 975 + 75 + 75 + 23 + 342 + 3.2 + 238 + 2.8 + 27 + 18 + 317 + 731 + 3.5 + 326 + 320 + 3.0
---------------------------------------------------------------

\

# Calculate SUM (in GB) via `bc` command

  # for Megabyte-sized files
echo "nicesum = 0  $bcmlist; nicesum/=1000; nicesum" | bc

  # for Gigabyte-sized files
echo "nicesum = 0 $bcglist; nicesum" | bc
```

\
\
\

### Statistics

> as of 07-29-2020

```
Danuser_lab 235.3T
DLLab 207.8T
BICF_Core 193T
Li_lab 108T
Nicastro_lab 98.49T
PDCD_Core 92.62T
Madabhushi_lab 77.3T
Hon_lab 76.86T
Xiao_lab 62.17T
Rajaram_lab 53.45T
```

> as of 06-09-2020


```
Danuser_lab 229.1T
DLLab 206.5T
BICF_Kidney 173.9T*
Koduru_lab 173.9G
BICF_Core 161T
CryoEM_Transfer 156.2T
Li_lab 117.8T
Nicastro_lab 109.6T
Cantarel_lab 106.2T
Rajaram_lab 94.44T
astrocyte 86.39T
Hon_lab 85.95T
Xiao_lab 64.11T
PCDC_Core 59.46T
Lee_lab 56.86T
trivedi_ansir 49.24T
Wakeland_lab 49.4T
castrillon_bicf 46.5T
Madabhushi_lab 41.29T
PHG_Clinical 38.08T
Chahrour_lab 37.78T*
```

\
\

### References

- [markdown doc on quota](https://github.com/DDNStorage/lustre_manual_markdown/blob/master/03.14-Configuring%20and%20Managing%20Quotas.md)

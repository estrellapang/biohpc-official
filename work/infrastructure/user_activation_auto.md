## Notes on BioHPC User Activation

> GOAL: to script & automate as much of the process as possible

### Post-Training & Registration

- triage through list of attendees

- I.D. users who can be activated right away (existing labs & dept. affiliations)

  - note down the following: 

    - uid (for users without "s" numbers, note down their lettered username)

    - appropriate gid, based on PI & Lab name

    - move user to appropriate dept in ApacheDirStudio

      - **ISSUE**: LDAP client base set to `base <ou=users,dc=biohpc,dc=swmed,dc=edu>`  ---> causing us to be unable to query the `newborn` ou!!!

\
\


### Account Creations

#### Individual User Accounts

- Step 1. Verify under `newborn` if their dept has affiliation 

- Step 2. Move their account under their corresponding department ou's

  - NOTE: add them to BOTH the department cn AND the lab group cn!

- Step 3. run `userFolder.sh` script to automatically create user directories

  - script location: `~/newUserActivations/.`

#### New Labs

```
# create lab GID in LDAP Apache Directory Studio

# GID formula

- start with dept GID as prefix, and take next sequentially available GID

 - e.g. dept GID=9000, labs have taken GIDs 9001-9006 --> new lab's GID = 9007!

 - ASSIGN SAMBA SID to the new lab group as well!

 - FORMULA: SambaID = GID*2 + 1001

- On ApacheDirStudio

 - select existing group -->  create new entry --> make new entry from template --> enter correct GID and sambaSID --> delete existing memberUid's from template entry --> finish; verify by `getent group <gid>` on cluster

```

\
\

- **AUTOMATION**:

  - input uid and gid to be changed to --> LDIF file to change GID

    - LDIF: change GID of user dn & add new `memberUID` to to lab cn 

    - (if `ou=newborn` was accessible; we can also script via LDIF to change ou=newborn to ou=<appropriate dept>) 

  - combine with directory generating script?

- LDIF Script Snippets:

```
# CHANGE USER from `newborn` to appropriate department

dn: uid=s132708,ou=newborn,dc=biohpc,dc=swmed,dc=edu
changetype: moddn
newrdn: uid=s132708
deleteoldrdn: 1
newsuperior: ou=CRI,ou=users,dc=biohpc,dc=swmed,dc=edu

# CHANGE USER's GID

dn: uid=s132708,ou=CRI,ou=users,dc=biohpc,dc=swmed,dc=edu
changetype: modify
replace: gidNumber
gidNumber: 5005

# ADD USER's memberUid ENTRY to GID

dn: cn=Xu_lab,ou=Group,ou=CRI,ou=users,dc=biohpc,dc=swmed,dc=edu
changetype: modify
add: memberUid
memberUid: s132708
```

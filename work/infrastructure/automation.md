## Automation Tools and Tactics

### Cron job

#### Carry Out Cron as Specific User 

```
# in this case, `backup_services`

30    3       1       *       *        backup_services    /usr/local/bin/rsnapshot -c /endosome/.backup/backup_services2/config/rsnapshot-docs.conf monthly
```

\

#### Scan for Older Files, Delete Them

```
# delete files older than 28 days

11 11 * * 0 /bin/find /var/log/.recordings/ -type f -mtime +28 -exec rm -f {} \;
```
\

#### Quirks n' Gotcha's

- [error with `date` command in cron jobs](https://unix.stackexchange.com/questions/29578/how-can-i-execute-date-inside-of-a-cron-tab-job)

## Instructions on Exporting Fileshares in BioHPC's Storage Clusters

### NFS exporting `/home2` 

- **Servers on NFS Cluster** 

  - ssh ONLY into `lysosome1` (either 198.215.54.8/111)
    - IB interfaces (10.100.164.3/4) -- no access?
    - **NOTE**: only biohpcadmin allowed, and only connections allowed to this node(check ssh config) 

  - `lyosome0` --> (198.215.54.8?/110) 
    - **Currently no direct access & no ssh out into lysosome1** 
    - IB interfaces (10.100.164.2/4) 

  - controllers:
    - IPMI Web Interfaces: 192.168.0.233:8088
    - IPMI Web Interfaces: 192.168.0.234:8088
    - authenticate via `biohpcadmin` & old root password

- **documentations**
  
  - [link 1](https://docs.biohpc.swmed.edu/dokuwiki/doku.php?id=lysosome_-_nfs_vs_beegfs_vs_gpfs) 

  - [link 2](https://docs.biohpc.swmed.edu/dokuwiki/doku.php?id=drbd-gfs2-nfs) 


- **DEBUG OPERATIONS** 

- show current exports

```
-------------------------------------------------------------------------------
showmount -a lysosome1.biohpc.swmed.edu		# (or "0")
-------------------------------------------------------------------------------
```

- edit `/etc/exports` 

- also exporting `/home1`(needs fsid) & `/home2`

- e.g. of changes made on Aug. 20, 2019 for Hon's Lab(s)

```
/home1          198.215.56.130(rw,fsid=470,async)
.
.
.
/home2          198.215.56.129(rw,async) # Hon's Lab
/home2          198.215.56.130(rw,async) # Kraus Lab
/home2          198.215.56.131(rw,async) # Kraus Lab
/home2          198.215.56.132(rw,async) # Kraus Lab
/home2          198.215.56.133(rw,async) # Hon's Lab

```

- export new entries in the `exports` file:

`exportfs -r` 

- **note changes in** `lysosome_change_log`


\
\
\

### LamellaSE Exports

- nfs export options

```
### e.g. of /project export to thunder_ftp server
-------------------------------------------------------------------------------
/project/thunder_ftp   198.215.54.125(rw,sync,no_root_squash,fsid=501)
-------------------------------------------------------------------------------
``` 

\
\
\

### NFS exporting GPFS `/work` & `/home1`

- open & edit file: `/project/biohpcadmin/shared/Nucleus-DontTouch/exports`

  - **THIS IS OBSOLETE! Now being exported from S002 and S003**

- append new IPs and assign unqie filesystem IDs

- e.g. changes made on Aug. 27, 2019 for Gary Hon's Lab(s) 

```
/work          198.215.56.130(rw,fsid=619,async)
/work          198.215.56.131(rw,fsid=620,async)
/work          198.215.56.132(rw,fsid=621,async)
/work          198.215.56.133(rw,fsid=622,async)
/work          198.215.56.134(rw,fsid=623,async)
/work          198.215.56.135(rw,fsid=624,async)
/work          198.215.56.136(rw,fsid=625,async)
/work          198.215.56.137(rw,fsid=626,async)
/work          198.215.56.138(rw,fsid=627,async)
.
.
.
/home1          198.215.56.129(rw,fsid=1469,async)
/home1          198.215.56.130(rw,fsid=1470,async)
/home1          198.215.56.131(rw,fsid=1480,async)
/home1          198.215.56.132(rw,fsid=1485,async)
/home1          198.215.56.133(rw,fsid=1486,async)
/home1          198.215.56.134(rw,fsid=1487,async)
/home1          198.215.56.135(rw,fsid=1488,async)
/home1          198.215.56.136(rw,fsid=1489,async)
/home1          198.215.56.137(rw,fsid=1491,async)
/home1          198.215.56.138(rw,fsid=1492,async)

```

- **NEW Export Protocol** 

  - modify `/etc/exports` in NucleusS002,S003 

```
# copy a back up

cp -p /etc/exports /etc/exports_bak_<%date> 

# Assign UNIQUE fsid to "/work" and "/home1" to new client 

  # search newly assigned fsid in file for duplicates

# SYNC with other NFS server

cp -p /etc/exports nucleuss003:/tmp 

# check & verify differences between the files

e.g. ">" and "|" denote changes
------------------------------------------------------------------------------------------------------------------------------------
diff -y -W 170 /etc/exports /tmp/exports | grep -i '>\||'
										    >	/work          198.215.51.3(rw,fsid=69,async)
#/work          198.215.51.8(rw,fsid=69,async)					    |	#/work          198.215.51.8(rw,fsid=69,async---fsid taken by 198.215.51.3)
#WebDataBan for Zhiyu Zhao/Morrison Lab.					    |	#WebDataBank for Zhiyu Zhao/Morrison Lab.
/work/CRI/WebDataBank          198.215.54.61(rw,fsid=628,async)     		    |	/work/CRI/WebDataBank          198.215.54.61(rw,fsid=628,async)
										    >	/home1          198.215.51.3(rw,fsid=2090,async)
#/home1          198.215.51.91(rw,fsid=2090,async)				    |	#/home1          198.215.51.91(rw,fsid=2090,async---fsid
------------------------------------------------------------------------------------------------------------------------------------

# make another backup on the second server

# overwrite old file

@nucleuss003 cp -p /tmp/exports /etc/exports

# use `diff` once more ---> no changes should appear now

# on both servers

exportfs -r  

# add client to foreman and to correct environment (based on RHEL release version)

```

\
\

#### Quickly Unexport & Re-export Dirs - e.g. /home1

```
# comment out directory
sed 's|/home1|#/home1|g' -i /etc/exports

# undo commenting, by appending; too much effort to undo '#' from original list
cat /etc/exports | grep -i home1 | sed 's|#||g' | tee -a /etc/exports
``` 

\
\

#### Check Duplicate Fsids for Specific Dirs

```
### Using /home1 as example:

-------------------------------------------------------------------------------
cat /etc/exports | grep -i home1 | grep -w fsid | awk -F, '{print $2}' | sort -V | uniq -d
fsid=2090

  # in example above, hundreds of "home1" exports were checked for duplicate fsids, "fsid=2090" was returned as a duplicate!

  # example, run cron job to check this
-------------------------------------------------------------------------------
```

\
\

#### Client AutoFS Configurations

- `/etc/auto.misc`

```
/work   -rw,hard,intr,nfsvers=3   198.215.54.1,198.215.54.2:/work
/home1   -rw,hard,intr,nfsvers=3   198.215.54.1,198.215.54.2:/home1

  # we use `nfsvers=3` because the newer protocols cannot follow the symlinks back to the NFS servers real path, e.g. `/work` is symlinked to `/endosome/work` on the servers

/project   -rw,hard,intr,nfsvers=4.0   198.215.54.1,198.215.54.2:/project
/archive   -rw,hard,intr,nfsvers=4.0   198.215.54.1,198.215.54.2:/endosome/archive

  # alternatively: `/archive   -rw,hard,intr,nfsvers=3  198.215.54.1,198.215.54.2:/archive`
```

\
\

- `/etc/auto.master`

```
/-   /etc/auto.misc --timeout 1200
+auto.master
```

\
\
\

### Lustre exporting `/project` 

- log in to DDN lustre cluster's controller node: `192.168.54.190`

  - become root

- add new rules to iptables-services' config file 

  - `vi /etc/sysconfig/iptables` 

- e.g. changes added on Aug. 27th, 2019 

```
-A INPUT -p tcp -s 198.215.56.131 --dport 988 -j ACCEPT
-A INPUT -p tcp -s 198.215.56.132 --dport 988 -j ACCEPT
-A INPUT -p tcp -s 198.215.56.133 --dport 988 -j ACCEPT
-A INPUT -p tcp -s 198.215.56.134 --dport 988 -j ACCEPT
-A INPUT -p tcp -s 198.215.56.135 --dport 988 -j ACCEPT
-A INPUT -p tcp -s 198.215.56.136 --dport 988 -j ACCEPT
-A INPUT -p tcp -s 198.215.56.137 --dport 988 -j ACCEPT
-A INPUT -p tcp -s 198.215.56.138 --dport 988 -j ACCEPT

```

- sync all cluster nodes' iptables file 

  - `sync-file /etc/sysconfig/iptables`

- push command to restart iptables services on all lustre nodes

  - `pdsh -a 'service iptables restart'`

- sample output below:

```
[root@mds00 biohpcadmin]# sudo pdsh -a 'service iptables restart'
mds00: iptables: Setting chains to policy ACCEPT: filter [  OK  ]
mds00: iptables: Flushing firewall rules: [  OK  ]
mds01: iptables: Setting chains to policy ACCEPT: filter [  OK  ]
oss00: iptables: Setting chains to policy ACCEPT: filter [  OK  ]
oss00: iptables: Flushing firewall rules: [  OK  ]
mds01: iptables: Flushing firewall rules: [  OK  ]
oss01: iptables: Setting chains to policy ACCEPT: filter [  OK  ]
oss01: iptables: Flushing firewall rules: [  OK  ]
mds00: iptables: Unloading modules: [  OK  ]
mds00: WARNING: All config files need .conf: /etc/modprobe.d/lustre.old, it will be ignored in a future release.
mds00: iptables: Applying firewall rules: [  OK  ]
oss02: iptables: Setting chains to policy ACCEPT: filter [  OK  ]
mds01: iptables: Unloading modules: [  OK  ]
oss02: iptables: Flushing firewall rules: [  OK  ]
oss00: iptables: Unloading modules: [  OK  ]
mds01: iptables: Applying firewall rules: [  OK  ]
oss00: iptables: Applying firewall rules: [  OK  ]
oss03: iptables: Setting chains to policy ACCEPT: filter [  OK  ]
oss03: iptables: Flushing firewall rules: [  OK  ]
oss01: iptables: Unloading modules: [  OK  ]
oss01: iptables: Applying firewall rules: [  OK  ]
oss02: iptables: Unloading modules: [  OK  ]
oss02: iptables: Applying firewall rules: [  OK  ]
oss03: iptables: Unloading modules: [  OK  ]
oss03: iptables: Applying firewall rules: [  OK  ]

```

- Verfy new rule iptable ruleset for EACH node:

```
lustreClutz=(mds00 mds01 oss00 oss01 oss02 oss03);for i in "${lustreClutz[@]}"; do ssh $i "hostname;iptables -L | grep -w '198.215.51.3'"; done

```

- VERIFY: `service iptables status` on all nodes above

  - **reflect changes** on `ddn_lustre_change_log` 

### Allow New IPs to ldap002's Iptables 

- log in to Nucleus005/006 

- check current allowed IPs

  - `sudo service iptables status`

- edit iptables reference file

  - `sudo vi /etc/sysconfig/iptables` 

- add new lines, e.g. from Aug. 20th, 2019 

```
-A INPUT -p tcp -s 198.215.56.131 --dport 636 -j ACCEPT
-A INPUT -p tcp -s 198.215.60.132 --dport 636 -j ACCEPT
-A INPUT -p tcp -s 198.215.60.133 --dport 636 -j ACCEPT

```

- restart & verify status

```
sudo service iptables restart
sudo service iptables status

```

\
\

### LUN MAPPINGS

- [HOW TO RESCAN for NEW LUNS](https://www.2daygeek.com/scan-detect-luns-scsi-disks-on-redhat-centos-oracle-linux/)

\
\

### Samba Exporting

- **Server Side Administration**

```
# list samba shares:
--------------------------------------------
smbstatus --shares
--------------------------------------------

# sources:
--------------------------------------------
https://askubuntu.com/questions/102924/list-samba-shares-and-current-users 
--------------------------------------------

```

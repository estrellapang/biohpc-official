## Commands for Monitoring Node Services & Processes

### Resources

- [comperehensive Techmint tutorial](https://www.tecmint.com/ps-command-examples-for-linux-process-monitoring/)

- [explanation of ps aux fields](https://www.computernetworkingnotes.com/linux-tutorials/ps-aux-command-and-ps-command-explained.html)

- [other process commands](https://www.howtogeek.com/107217/how-to-manage-processes-from-the-linux-terminal-10-commands-you-need-to-know/)

- [read more on PPID, TID, and TGIDs!](https://stackoverflow.com/questions/9305992/if-threads-share-the-same-pid-how-can-they-be-identified)

- [all the "id's"](https://stackoverflow.com/questions/30493424/what-is-the-difference-between-a-process-pid-ppid-uid-euid-gid-and-egid) 

- [loadavg column labels](https://stackoverflow.com/questions/28581333/fifth-column-of-proc-loadavg)

- [redhat_6_and_under](https://www.cyberciti.biz/faq/check-running-services-in-rhel-redhat-fedora-centoslinux/)

- [explanation of PROCESS STATES!](https://linuxjourney.com/lesson/process-states)

- [RHEL explanation of process D state](https://access.redhat.com/solutions/59989)

- [process D state cause in terms of network filesystems](https://unix.stackexchange.com/questions/16738/when-a-process-will-go-to-d-state)

- [all LINUX PROCESS STATES -  USEFUL MANUAL](https://linuxjourney.com/lesson/process-states#)

- [RHEL's explanation on process states](https://access.redhat.com/sites/default/files/attachments/processstates_20120831.pdf)

- [how to pause and continute processes](https://ostechnix.com/suspend-process-resume-later-linux/)

\
\

### System Load Average

```
### show load in the last 5,10,15 mins

cat /proc/loadavg

uptime

```

\
\

### Most Versatile: `ps` 

```
### display all processes
-------------------------------------------------------------------------------
ps -e 

  ## -OR- 

ps -A

  ## -OR-

ps aux
-------------------------------------------------------------------------------

### show how long a process has elapsed
-------------------------------------------------------------------------------
ps -p <pid> -o etime
-------------------------------------------------------------------------------

### show all of a process's child pids
-------------------------------------------------------------------------------
pgrep -P <pid>
-------------------------------------------------------------------------------

### show all processes spawned from the same program
-------------------------------------------------------------------------------
pgrep -a <cmd>

  # this gives PIDs and cmds; without `-a`, only PIDs are outputted
-------------------------------------------------------------------------------


### find process by command and use "top" to monitor
-------------------------------------------------------------------------------
pgrep <command name>
  
  # this should show PID(s) of the program

top -p <PID>

# monitor multiple PIDs

top -p <pid1>,<pid2>,...

[reference](https://unix.stackexchange.com/questions/165214/how-to-view-a-specific-process-in-top)
-------------------------------------------------------------------------------

### pgrep all processes for a program & kill them all
-------------------------------------------------------------------------------
# first confirm we are parsing out the PIDs

pgrep -a <cmd> | awk '{print $1}' | xargs echo

\

# kill all

pgrep -a <cmd> | awk '{print $1}' | xargs kill -9
-------------------------------------------------------------------------------


### show all instances/processes of a program
-------------------------------------------------------------------------------
ps -C <command/program name>

## e.g. show all instances of headless virtualbox vms

ps -C VBoxHeadless -o uid,user,%mem,%cpu,etime
-------------------------------------------------------------------------------


### show all of one user's process
-------------------------------------------------------------------------------
ps -u <uid> -o cmd,pid,ppid,%mem,%cpu,etime

  ## `ppid` --> parent PID 
-------------------------------------------------------------------------------

### kill all of users' processes
-------------------------------------------------------------------------------
pkill -9 -u <user>,<user>,<user>
-------------------------------------------------------------------------------

- [pkill guide](https://linuxize.com/post/pkill-command-in-linux/)

### watch a user's processes
-------------------------------------------------------------------------------
sudo watch -n 1 'ps -u <uid> -o cmd,pid,ppid,%mem,%cpu,etime'

  ## the `%mem\%cpu` can be sorted in ascending or descending order (see first link in "Resources")

  ## `ps` cmd can be piped into `top` to output only the top processes
-------------------------------------------------------------------------------

### check for user's TTY sessions & kick out users
-------------------------------------------------------------------------------
# check PID for users sessions

w 

  # check current sessions

ps -ft <tty>  

  # e.g. `ps -ft pts/15`

kill -9 <PID>

[reference](https://serverfault.com/questions/177001/find-files-in-one-directory-not-in-another)
-------------------------------------------------------------------------------

### check zombie processes
-------------------------------------------------------------------------------
ps aux |grep "defunct"

ps aux |grep Z

[resource](https://www.servernoobs.com/how-to-find-and-kill-all-zombie-processes/)
-------------------------------------------------------------------------------

### pause processes
-------------------------------------------------------------------------------
kill -STOP <pid> <pid> ..

  # to resume:

kill -CONT <pid> <pid> ..
-------------------------------------------------------------------------------
```

\

#### SCRIPTING

- **check if PID is alive**

  - `if [ "$(ps -p <PID>)" ]; then echo "yes"; else echo "no"; fi`

  - useful for setting up crons to periodically check PID

\
\

### pstree

```
# `pstree` command

# e.g. output
-------------------------------------------------------------------------------
pstree
systemd─┬─ModemManager───2*[{ModemManager}]
        ├─abrt-dbus───2*[{abrt-dbus}]
        ├─abrt-watch-log
        ├─abrtd
        ├─accounts-daemon───2*[{accounts-daemon}]
        ├─alsactl
        ├─cm-nfs-checker───{cm-nfs-checker}
        ├─colord───2*[{colord}]
        ├─crond
        ├─dbus-daemon
        ├─gssproxy───5*[{gssproxy}]
        ├─irqbalance
        ├─ksmtuned───sleep
        ├─libvirtd───15*[{libvirtd}]
        ├─lsmd
        ├─lvmetad
        ├─master─┬─pickup
        │        └─qmgr
        ├─mcelog
        ├─mdadm
        ├─mmccrmonitor───mmccrmonitor
        ├─munged───3*[{munged}]
        ├─nslcd───5*[{nslcd}]
        ├─ntpd
        ├─packagekitd───2*[{packagekitd}]
        ├─pan_manage_rout
        ├─polkitd─┬─pkla-check-auth
        │         └─5*[{polkitd}]
        ├─python───30*[{python}]
        ├─rngd
        ├─rpc.statd
        ├─rpcbind
        ├─rsyslogd───2*[{rsyslogd}]
        ├─rtkit-daemon───2*[{rtkit-daemon}]
        ├─runmmfs───mmfsd───413*[{mmfsd}]
        ├─safe_cmd───cmd───12*[{cmd}]
        ├─slurmd
        ├─slurmstepd─┬─slurm_script───awk
        │            └─3*[{slurmstepd}]
        ├─smartd
        ├─sshd───sshd───bash───pstree
        ├─systemd-journal
        ├─systemd-logind
        ├─systemd-udevd
        ├─udisksd───4*[{udisksd}]
        ├─upowerd───2*[{upowerd}]
        └─wpa_supplicant
-------------------------------------------------------------------------------


### lsof: see # of open files 

- see current #
-------------------------------------------
lsof | wc -l
-------------------------------------------

- **Links: Set User & System-Wide Limits for # of Open Files**

  - [link 1](https://easyengine.io/tutorials/linux/increase-open-files-limit/)

  - [link 1](https://linoxide.com/linux-how-to/03-methods-change-number-open-file-limit-linux/)

  - [lsof command tutorial](https://alvinalexander.com/blog/post/linux-unix/linux-lsof-command)

  - [link 1](https://access.redhat.com/solutions/61334)

  - [link 1](https://www.cyberciti.biz/faq/linux-unix-nginx-too-many-open-files/)

  - [best link for limit.conf](https://www.poftut.com/limits-conf-file-limit-users-process-linux-examples/)

### Set User-BASED Limitations

- [limit processes per user](https://www.tecmint.com/set-limits-on-user-processes-using-ulimit-in-linux/)

\
\
\

## Service Checks

- **Networking**

```
# services' open ports
-------------------------
netstat -tulpn
-------------------------
```

\
\
\

### Managing Processes in Foreground,Background 

#### nohup

- [nohup a process w/o .out file](https://stackoverflow.com/questions/10408816/how-do-i-use-the-nohup-command-without-getting-nohup-out)

\

#### manage background processes

- see background jobs in detail `jobs -l`

- kill jobs by job ID: `kill %<ID>`

- [reference on StackExchange](https://unix.stackexchange.com/questions/104821/how-to-terminate-a-background-process)

- [job control on How-to Geek](https://www.howtogeek.com/440848/how-to-run-and-control-background-processes-on-linux/)

- [using while loops for process control](https://stackoverflow.com/questions/7485776/start-script-after-another-one-already-running-finishes)

\

#### Prevent Auto-Spawning Daemons/Processes

- Two steps

  - 1. use `type` to determine path to the file defining the daemon/process

  - e.g. `type -p gnome-keyring-daemon`

  - 2. revoke execute permission from it `chmod -x <path to file>`

  - shortcut example: `chmod -x $(type -p gnome-keyring-daemon)` 

- [unix stack exchange reference](https://unix.stackexchange.com/questions/271661/disable-gnome-keyring-daemon)

- [type command tutorial](https://linuxize.com/post/linux-type-command/)

- [redhat tickets reference](https://bugzilla.redhat.com/show_bug.cgi?id=1205558)

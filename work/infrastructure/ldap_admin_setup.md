## BioHPC LDAP Essentials: Apache Directory Studio, etc. 

### Apache Directory Studio Setup 

- **Setup Page 1**

- Hostname: `192.168.54.1` --> LDAP001

- Port: `636`

- Encryption method: `SSL` --> ldaps://

- **Setup Page 2** 

- Bind DN: `uid=<>,ou=<>,ou=users,dc=biohpc,dc=swmed,dc=edu` 

  - user has to be first added to `1500(ldapadmin)` 

- Bind Password: `<biohpc pass>`

- **do not click**: "Check Authentication"

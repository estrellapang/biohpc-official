### Ansible Ops: Operations SUMMARIZED 

#### ansible options

- `--become-method=su` --> meaning become ROOT`

  - default behavior: use `sudo` privilege

### Apply Security Updates


#### Determine Workstation Model:

```
# FOR LENOVO MACHINES:
ansible <host group> -k -b -K -u biohpcadmin --become-method=su -a "cat /sys/devices/virtual/dmi/id/product_version"

# FOR INTEL NUCs: 
ansible <host group> -k -b -K -u biohpcadmin --become-method=su -a "cat /sys/devices/virtual/dmi/id/board_name"

```



#### Determine package presence/if duplicate packages are installed: 

```
### Good because it is not as verbose as yum info

ansible TCse -k -b -K -u biohpcadmin --become-method=su -a "rpm -q <package name>" 

```



#### Pass Console Output to log (while still printing them out) 

```
<ansible command> | tee -a <absolute path to file>

```



#### Yum minimal security update with minimal

```
ansible WSs -k -b -K -u biohpcadmin --become-method=su -a "yum --disablerepo=virtualbox*,puppet* --exclude=kernel*,redhat-release*,libupnp*,libebml*,libmodplug*,libmatroska*

```

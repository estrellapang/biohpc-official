## Details On Job Submissions

### Node Limits

- **4** GPU

- **16** 128GB, 256GB, 256GBv1, 384GB 

- **64** 32GB

- Details:

```
- every user can allocate up to 16 CPU nodes in the regular partitions (not 32G)

- for the 32G partition, each user can allocate up to 64 CPU nodes, however:

     --  this is dependent on the # of regular CPU nodes already allocated by the user, where each one takes away 4 CPU nodes available from the 32G partition.

- In essence:

     <# of 32G nodes available> = 64 - 4*<# of regular nodes used>

```



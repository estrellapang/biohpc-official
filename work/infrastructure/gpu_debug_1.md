## Incidents with individual servers

### Aug 9th: Nucleus162--detects only ONE GPU modules (vs. 2)

```
[root@Nucleus162 ~]# nvidia-smi 
Fri Aug  9 09:00:16 2019       
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 418.67       Driver Version: 418.67       CUDA Version: 10.1     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|===============================+======================+======================|
|   0  Tesla P100-PCIE...  Off  | 00000000:03:00.0 Off |                    0 |
| N/A   19C    P0    24W / 250W |    133MiB / 16280MiB |      0%      Default |
+-------------------------------+----------------------+----------------------+
                                                                               
+-----------------------------------------------------------------------------+
| Processes:                                                       GPU Memory |
|  GPU       PID   Type   Process name                             Usage      |
|=============================================================================|
|    0     76877      G   /usr/bin/X                                    65MiB |
|    0     76928      G   /usr/bin/gnome-shell                          67MiB |
+-----------------------------------------------------------------------------+
[root@Nucleus162 ~]# dmesg | grep -i nvrm
[  131.929893] NVRM: loading NVIDIA UNIX x86_64 Kernel Module  418.67  Sat Apr  6 03:07:24 CDT 2019
[  191.435274] NVRM: GPU at PCI:0000:04:00: GPU-0f5dfc16-369b-9885-dfa2-f761230e6184
[  191.435294] NVRM: GPU Board Serial Number: 0321917112550
[  191.435297] NVRM: Xid (PCI:0000:04:00): 62, 0a4b(259c) 8400e2b7 00288a6e
[  195.428952] NVRM: Xid (PCI:0000:04:00): 48, An uncorrectable double bit error (DBE) has been detected on GPU in the framebuffer at partition 5, subpartition 0.
[  200.243492] NVRM: Xid (PCI:0000:04:00): 64, Dynamic Page Retirement: Fatal Error, unable to retire page (0x00000000003fdf77).
[  212.266304] NVRM: Xid (PCI:0000:04:00): 64, Dynamic Page Retirement: Fatal Error, unable to retire page (0x00000000003fdf77).
[  212.266308] NVRM: Xid (PCI:0000:04:00): 64, Dynamic Page Retirement: Fatal Error, unable to retire page (0x00000000003fd3b8).
[35378.010726] NVRM: GPU at PCI:0000:04:00: GPU-0f5dfc16-369b-9885-dfa2-f761230e6184
[35378.010733] NVRM: Xid (PCI:0000:04:00): 48, An uncorrectable double bit error (DBE) has been detected on GPU in the framebuffer at partition 5, subpartition 0.
[35378.134886] NVRM: Xid (PCI:0000:04:00): 61, 0a4b(259c) 00000000 00000000
[35389.603226] NVRM: GPU at PCI:0000:04:00: GPU-0f5dfc16-369b-9885-dfa2-f761230e6184
[35389.603242] NVRM: GPU Board Serial Number: 0321917112550
[35389.603244] NVRM: Xid (PCI:0000:04:00): 62, 00ca(4700) 00000000 00000000
[35393.601163] NVRM: Xid (PCI:0000:04:00): 48, An uncorrectable double bit error (DBE) has been detected on GPU in the framebuffer at partition 5, subpartition 0.
[35407.863834] NVRM: Xid (PCI:0000:04:00): 31, Ch 00000001, intr 10000000. MMU Fault: ENGINE GRAPHICS GPCCLIENT_GPCCS faulted @ 0x0_00033000. Fault is of type FAULT_PDE ACCESS_TYPE_READ
[35421.476241] NVRM: RmInitAdapter failed! (0x26:0xffff:1106)
[35421.476258] NVRM: rm_init_adapter failed for device bearing minor number 1
[35421.694977] NVRM: RmInitAdapter failed! (0x26:0xffff:1106)
[35421.694993] NVRM: rm_init_adapter failed for device bearing minor number 1
[82067.594719] NVRM: RmInitAdapter failed! (0x26:0xffff:1106)
[82067.594749] NVRM: rm_init_adapter failed for device bearing minor number 1
[82067.867447] NVRM: RmInitAdapter failed! (0x26:0xffff:1106)
[82067.867473] NVRM: rm_init_adapter failed for device bearing minor number 1
[82068.788106] NVRM: RmInitAdapter failed! (0x26:0xffff:1106)
[82068.788124] NVRM: rm_init_adapter failed for device bearing minor number 1
[82069.435913] NVRM: RmInitAdapter failed! (0x26:0xffff:1106)
[82069.435927] NVRM: rm_init_adapter failed for device bearing minor number 1
[82070.287244] NVRM: RmInitAdapter failed! (0x26:0xffff:1106)
[82070.287259] NVRM: rm_init_adapter failed for device bearing minor number 1
[82070.503136] NVRM: RmInitAdapter failed! (0x26:0xffff:1106)
[82070.503152] NVRM: rm_init_adapter failed for device bearing minor number 1
[82171.657973] NVRM: RmInitAdapter failed! (0x26:0xffff:1106)
[82171.658007] NVRM: rm_init_adapter failed for device bearing minor number 1
[82221.018562] NVRM: RmInitAdapter failed! (0x26:0xffff:1106)
[82221.018576] NVRM: rm_init_adapter failed for device bearing minor number 1
[82221.258196] NVRM: RmInitAdapter failed! (0x26:0xffff:1106)
[82221.258208] NVRM: rm_init_adapter failed for device bearing minor number 1
[82222.242083] NVRM: RmInitAdapter failed! (0x26:0xffff:1106)
[82222.242101] NVRM: rm_init_adapter failed for device bearing minor number 1
[82222.988157] NVRM: RmInitAdapter failed! (0x26:0xffff:1106)
[82222.988173] NVRM: rm_init_adapter failed for device bearing minor number 1
[82223.887256] NVRM: RmInitAdapter failed! (0x26:0xffff:1106)
[82223.887283] NVRM: rm_init_adapter failed for device bearing minor number 1
[82224.136493] NVRM: RmInitAdapter failed! (0x26:0xffff:1106)
[82224.136511] NVRM: rm_init_adapter failed for device bearing minor number 1
[82375.594791] NVRM: RmInitAdapter failed! (0x26:0xffff:1106)
[82375.594808] NVRM: rm_init_adapter failed for device bearing minor number 1
[82394.563999] NVRM: RmInitAdapter failed! (0x26:0xffff:1106)
[82394.564014] NVRM: rm_init_adapter failed for device bearing minor number 1
[82422.244979] NVRM: RmInitAdapter failed! (0x26:0xffff:1106)
[82422.244994] NVRM: rm_init_adapter failed for device bearing minor number 1
[82422.477187] NVRM: RmInitAdapter failed! (0x26:0xffff:1106)
[82422.477204] NVRM: rm_init_adapter failed for device bearing minor number 1
[82423.427439] NVRM: RmInitAdapter failed! (0x26:0xffff:1106)
[82423.427454] NVRM: rm_init_adapter failed for device bearing minor number 1
[82424.107126] NVRM: RmInitAdapter failed! (0x26:0xffff:1106)
[82424.107141] NVRM: rm_init_adapter failed for device bearing minor number 1
[82424.990664] NVRM: RmInitAdapter failed! (0x26:0xffff:1106)
[82424.990679] NVRM: rm_init_adapter failed for device bearing minor number 1
[82425.209897] NVRM: RmInitAdapter failed! (0x26:0xffff:1106)
[82425.209921] NVRM: rm_init_adapter failed for device bearing minor number 1
[82470.897951] NVRM: RmInitAdapter failed! (0x26:0xffff:1106)
[82470.897971] NVRM: rm_init_adapter failed for device bearing minor number 1
[82581.028712] NVRM: RmInitAdapter failed! (0x26:0xffff:1106)
[82581.028744] NVRM: rm_init_adapter failed for device bearing minor number 1
[83415.183014] NVRM: RmInitAdapter failed! (0x26:0xffff:1106)
[83415.183035] NVRM: rm_init_adapter failed for device bearing minor number 1


# see its neighbor `Nucleus163` 

[root@Nucleus163 ~]# nvidia-smi
Fri Aug  9 08:59:18 2019       
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 418.67       Driver Version: 418.67       CUDA Version: 10.1     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|===============================+======================+======================|
|   0  Tesla P100-PCIE...  Off  | 00000000:02:00.0 Off |                    0 |
| N/A   59C    P0   172W / 250W |  15671MiB / 16280MiB |     96%      Default |
+-------------------------------+----------------------+----------------------+
|   1  Tesla P100-PCIE...  Off  | 00000000:82:00.0 Off |                    0 |
| N/A   26C    P0    28W / 250W |     24MiB / 16280MiB |      0%      Default |
+-------------------------------+----------------------+----------------------+
                                                                               
+-----------------------------------------------------------------------------+
| Processes:                                                       GPU Memory |
|  GPU       PID   Type   Process name                             Usage      |
|=============================================================================|
|    0     77542      G   /usr/bin/X                                    65MiB |
|    0     77590      G   /usr/bin/gnome-shell                          66MiB |
|    0    164799      C   python                                     15529MiB |
|    1     77542      G   /usr/bin/X                                    24MiB |
+-----------------------------------------------------------------------------+
[root@Nucleus163 ~]# 
[root@Nucleus163 ~]# 
[root@Nucleus163 ~]# dmesg | grep -i nvrm
[  134.727524] NVRM: loading NVIDIA UNIX x86_64 Kernel Module  418.67  Sat Apr  6 03:07:24 CDT 2019

  # Healthy 



[ref link 1](https://devtalk.nvidia.com/default/topic/1055438/detecting-1-of-2-gpus-nvrm-rminitadapter-failed) 

[ref link 2](https://devtalk.nvidia.com/default/topic/1046475/unable-to-detect-second-gpu-ubuntu-16-04-18-04/)

```

\
\
\

### General Steps to Resolve Blackscreens due to GPU Module Failures

```
killall -9 X
# Now stop gdm - if you do this first it will hang, as X is hard stuck
systemctl stop gdm
# Now reset the GPU card
nvidia-smi -r
# Now start gdm
systemctl start gdm
```

\
\
\

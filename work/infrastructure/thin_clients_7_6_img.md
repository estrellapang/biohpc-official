## Upgrading to RHEL 7.6 on for P330 ThinClients

> while BioHPC's older gen. P320 machines were compatible with RHEL 7.2 and up, the new order of P330's are only compatible with RHEL 7.5 or higher. As of Aug 14, 2019, the BioHPC's thin client image is one minor version behind: 7.4. 

[lenovo linux compatibility site](https://support.lenovo.com/us/en/solutions/pd031426)


### Previous State & Troubleshooting

- pulled RHEL7.4 image into 2 P330's, and both could not recognize the NIC port 

 - also present were ACPI errors in dmesg:

```
ACPI Error: [DSSP] Namespace lookup failure, AE_NOT_FOUND (20110623/psargs-359)
ACPI Error: Method parse/execution failed [\_SB_.PCI0.SAT0.SPT1._GTF] (Node ffff88021484f848), AE_NOT_FOUND (20110623/psparse-536)

# implicate a network card driver issue? 

```

- Network Card Models b/w P320 & P330: 

  - P320: Intel I219LM [reference1](https://psref.lenovo.com/syspool/Sys/PDF/ThinkStation/ThinkStation%20P320%20Tiny/P320Tiny.pdf) 

  - P330: Intel I219LM! [reference2](https://psref.lenovo.com/syspool/Sys/PDF/ThinkStation/ThinkStation%20P330%20Tiny/ThinkStation%20P330%20Tiny.pdf) 

  - **Issue NOT NIC card?** 

  - on-system driver info: 

```
lspci -nn | grep -i net  # yields Ethernet card info 

ethtool -i bootnet  # yields which system specific driver is operating for which card/port 
 
### THERE ARE 3 BASE LINUX NETWORK DRIVERS ### [link](https://www.intel.com/content/www/us/en/support/articles/000005480/network-and-i-o/ethernet-products.html) 
  
  # check via `modinfo be2net, igb, tg3` --> RHEL release & version will be displayed 

  # to determine which one is being used for which port, try `ethtool -i` 

```

## Implementation 

- since network drivers isn't the issue --> resolve for updating to compatible RHEL version: 7.5 or above 

- first register the workstation: 

- docs.biohpc.swmed.edu --> "start" --> "linux" --> execute the two commands under "Activate" 

- either independently install `vulkan-filesystem` for nvidia driver updates, or go to Official RedHat Satellite to add `redhat optional repo` to the registered machine 

- ` yum update`

- comment out lustre mount in `/etc/fstab`  

- `reboot`

- follw Docs page, "lustre_client_config" to build RPMS(locations listed in the documentation)  and reinstall/update lustre client  

- `mount -a` 

- `ethtool -i bootnet` (e1000e, version 3.2.6-k) 

- `uname -r` (3.10.0-1062.el7. x86_64) 


### Push Image to Cluster via Clonezilla

############# Aug 15th 2019 Update ################


- **kernel module driver responsible for Ethernet port is e1000e, and between the 7.4 and 7.7 kernels (-693 vs. -1062), the driver version DOES NOT CHANGE...only license changed from GPL to GPL.2** 

- additional commands for detecting Ethernet Cards

`lspci -nn | grep -i ethernet` --> for ETHERNET! 

`lspci -nn | grep -i network` --> for WIFI controller!


## Advanced Unix Permission Topics 

### UNIX Standards for Filesystem Atrributes

- [understand difference unix timestamps](https://www.unixtutorial.org/atime-ctime-mtime-in-unix-filesystems/)

### `chown` & `chmod`

#### acting on symlinks

- default behavior: change permission of the file deferenced by the symlink

- to change the permission of symlink itself:

  - `chown -h`

- [reference here](https://unix.stackexchange.com/questions/87200/change-permissions-for-a-symbolic-link)

\

### Set ACLs

- Set Default ACLs

  - [reference 1](https://superuser.com/questions/264383/how-to-set-file-permissions-so-that-new-files-inherit-same-permissions)

\

- Remove all ACLs

  - `setfacl -bn <dir>`
\

### Good Ticket Exmaples

```
#2019121610008084

> currently "advanced" for me

### Prevent Deletion of Files by Group Members:

- Approach 1: Add Sticky Bit to Directory: `chmod -R +t <directory>` 

  - Approach 1.1: combined with changing OWNER to `root`, so that even original owners cannot accidentally delete files 

- Approach 2: Set individual files with `+i` or `+a` via `chattr`


### All new files inherit parent directory's GID, add setgid:

- via modebits:

  - `chmod -R 2### <directory>`

- via literal syntax:

  - `chmod -R g+s <directory>`


[link 1](https://linuxconfig.org/how-to-use-special-permissions-the-setuid-setgid-and-sticky-bits)

```

\

### FS Info Fetch

#### `ls -l` and fetch only user and group owners, in addition to the file names:

`sudo ls -l <directory> | grep -i <search term> | awk '//{print $3, $4, $9 ;}'`

#### use `stat` cmd to get ONLY file user:

` stat -c %U <file/directory>`

`stat -f <file/dir>`

#### use `file` to determine type of data standard

\
\

### UNIX Users and Group Management 

- create user and assign to specific uid and gid

`useradd --uid <uid> --gid <gid> <username>`

- create group and assign specific gid

`groupadd -g <gid> <group name>`

- modify user uid and gid

`usermod -u <new_uid> -g <new_gid> <username>`

- modify group gid

`groupmod -g <new_gid> <groupname>`

\

#### References

- [comprehensive user creation guide](https://linuxize.com/post/how-to-create-users-in-linux-using-the-useradd-command/)

- [comprehensive group creation guide](https://linuxize.com/post/how-to-create-groups-in-linux/)

- [user and group mod guide](https://www.thegeekdiary.com/how-to-correctly-change-the-uid-and-gid-of-a-user-group-in-linux/)



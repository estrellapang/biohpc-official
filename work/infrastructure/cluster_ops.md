## Info & Tips on Services on the Cluster


### Modules 

#### July 29th, 2019

- difference between modules on the cluster & modules on the WS 

  - `module avail` lists different apps than on Nucleus005 

```
## on workstation

[zpang1@biohpcwsc037 Learning_Linux]$ module avail

## locations indicate that the modules are locally installed? ##

---------------------------------------------------------- /cm/local/modulefiles -----------------------------------------------------------
dot         module-git  module-info modules     null        shared      use.own

---------------------------------------------------------- /cm/shared/modulefiles ----------------------------------------------------------


## on login node

[zpang1@Nucleus005 ~]$ module avail

## even though the namespace is the same, there are additional features & modules 

---------------------------------------------------------- /cm/local/modulefiles -----------------------------------------------------------
cluster-tools/7.3 dot               gcc/6.1.0         module-git        null              shared
cmd               freeipmi/1.5.2    ipmitool/1.8.17   module-info       openldap          use.own

---------------------------------------------------------- /cm/shared/modulefiles ----------------------------------------------------------

```

### SLURM

#### Aug 7th, 2019 

- **Drain TACC Nodes**

```
# Check which ones are available in TACC Rack

sinfo -p 32GB


PARTITION AVAIL  TIMELIMIT  NODES  STATE NODELIST
32GB         up   infinite      1  down* NucleusA176
32GB         up   infinite    225  alloc NucleusA[002-054,056-072,074-080,082-092,094-096,098-109,116,122-123,126-127,132-133,138-139,141,144-149,151,153,155-174,177-204,209-213,215-234,236-241],NucleusB[002-017,019-027]
32GB         up   infinite     40   idle NucleusA[055,073,081,093,097,110-115,117-121,124-125,128-131,134-137,140,142-143,150,152,154,175,205-208,214,235],NucleusB018


# Chose NucleusA118-121 & NucleusA110-113

scontrol update Nodename=NucleusA[110-113,118-121] state=drain reason="Xeon Phi Removal" 



# Confirm

sinfo -p 32GB
PARTITION AVAIL  TIMELIMIT  NODES  STATE NODELIST
32GB         up   infinite      2 alloc* NucleusA230,NucleusB027
32GB         up   infinite      8  drain NucleusA[110-113,118-121]   # Yep

# Post Operation: 

scontrol update Nodename=NucleusA[110-113,118-121] state=resume


```

### Aug 1st, 2019

- **Extend Job Duration**

```
#  check which job characteristics 

 scontrol show job 1232643 


# locate current end time

RunTime=19:11:57 TimeLimit=1-00:00:00 TimeMin=N/A
   SubmitTime=2019-07-30T14:51:31 EligibleTime=2019-07-30T14:51:31
   StartTime=2019-07-30T14:51:32 EndTime=2019-07-31T14:51:32 Deadline=N/A 

# extend job duration

scontrol update jobid=1232643 EndTime=2019-08-10T19:00:00

  # extended by 9 days


# verify

scontrol show job 1232643 

RunTime=19:13:34 TimeLimit=11-04:08:00 TimeMin=N/A
SubmitTime=2019-07-30T14:51:31 EligibleTime=2019-07-30T14:51:31
StartTime=2019-07-30T14:51:32 EndTime=2019-08-10T19:00:00 Deadline=N/A

```

## Notes on Mellanox IB Network Training

> 1/10 Sessions

> Contact: Ori Lapid, NVIDIA Networking Division, Mellanox Global Services 

> Trainer: Oded Paz: odedp@mellanox.com

> Topic: ibdiagnet

> 05/06/2020

### What is IBDiagnet/Overview

- script that acts upon the Mellanox network drivers

  - check each port & each link

  - checks for duplicate LID addresses

  - checks for credit loops (?)

- run ibdiagnet daily as a cron? 

- firmware on adapters & switches need to be compliant

-

### Prepare Cluster

- 

### IBDiagnet Debug Initialization

- `ibidiagnent -pc` 

  - flags: clear all performance counters --> allows starting fresh page without old errors

  - let it run for 30 - 60 

- `ibdiagnent -ls 25 -lw 4x -P all=1

  - `ls` --> link speed: in 25Gbits for each line/port; check for links that do NOT meet this requirement

  - `lw` --> link-widths: e.g. "4x" 4 links aggregated

    - checks for link degradation: could lead to 1 links as opposed to 4 synchronously 

  - `-P all=1` --> performance counters gather info + errors, and this handle filters for counter value of 1

    - we can adjust to higher values, 1 gathers all errors

- common root causes: cabling,adapter,switch ports, misconfiguration

- fix issue --> repeat ibadiagnet with same parameters

- if fabric is heterogenous in speed, then `-ls` option, setting a speed threshold, may not be necessary
 
### Script Functions

- fabric discovery

- detection of 

  - duplicated GUIDs,node names (common in unmanaged switches)

  - duplicated LIDs (each link & each port is assigned one -- no LID should be 0) 

### Diagnosis 

  - if "0" --> issue communicating with subnet manager 

- checks link operational state (up/down)

- checks Subnet Manager 

  - ONE master at all times

  - others are standby masters

- Port counters checks

- BER test:

  - Bit Error Rate: ratio between error bits/unit time 

  - determines the link layer performance 

- Firmware check:

- Speed/Width Checks

- OUTPUT:

  - /var/tmp/ibdiagnet2/ibdiagnet2.log  --> more details than stdOUT

  - additional files:

    - `.lst` --> topology description file, lists all nodes, ports, and links

    - `.fdbs` --> unicast forwarding tables

    - `.mcfdbs` --> multicast

    - `.sm` --> definite all SMs & their state & priorities

    - `.pm` --> all PM counters

    - `.pkey` --> all partition info & their member host ports

    - `.db_csv` --> database 


- reading MAIN log:

  - I -> "informative"

  - W -> "Warning" (first column)

  - E -> "Errors" (2nd column)

 - when erros are found, e.g. 2 under "Speed/Width" --> 

 - "Discovery" --> # of nodes, swtiches, & adapters

   - "CA-s" --> channel adapters

- e.g. of E on "Link" 

  --> source & destination: <switchGUID>/<>/<port #> --> <switchGUID>/<link>/<port #> 

  -->  "Unexpected actual link speed 2.5" --> 2.5 * 4 = 10Gbit/sec; "enabled_speed2='2.5 or 5 or 10" --> indicating the environment is capable for 5x4 or 10x4


- additional cmd handles:

`ibdiagnent -h` 

  - `--get_cable_info` 

`ibdiagnent -i mlx5_1 --get_cable_info`

  --> could combine with `--cable_info_disconnected` --> even if one side is NOT connected, we can still get info

  --> running tet on a specifc connection (-i)

  --> outputs an additional `.cables` file

  - `--pm_pause_time <seconds>\` (stops within a certain amount of time)

 - cotent of .cables file: 

    - for each port and switch, provides a body of cable info

 - get switch/port specific cable info:`cat <>.cables | grep -A 9(or 7?--> # of lines) <switch name> 


### Diagnosis 

- .sm file content

  - shows master & standby nodes

  - +1 standby typically not recommended 

  - `saquery <LID>` --> gets detailed info on SM node i.e. hostname, etc

- Port Counters Errors

  - `max_retransmission_rate` overflow -->  buffer accumulating past info has exceeded threshold

  - `symbol_error_counter` --> generally indicates bad cables 

- Routing 

  - `ibdiagnet -r` --> outputs additional file

  - the info ONLY FOUND in LOG file!

  - checks for "credit loops" --> causes traffic halting, due to routing loops causing full buffers --> no traffic flow 

  - grep for `credit loops report` 

  - actions/implications: deployment/design flaw or cabling/topology

  - `locate opensm.conf` in switch: most important config file

  - locate `routing_engine` --> change from `minihop` to `updn` --> advanced fix for credit loop


 
#### ibdiagnet as cron job

- NOTE: could affect load on fabric


### EXTRA INFO 

- create ISLANDS for heterogenous fabirc topologies



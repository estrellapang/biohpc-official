## Troubleshooting HTTPD Servers and Components

### Server General Info

- `curl --head -n <server IP/hostname>`

```
e.g.

curl --head -n https://cloud.biohpc.swmed.edu
HTTP/1.1 200 OK
Date: Mon, 22 Jun 2020 21:44:54 GMT
Server: Apache/2.4.6 (Red Hat Enterprise Linux) OpenSSL/1.0.2k-fips PHP/7.3.11
Strict-Transport-Security: max-age=15552000
Last-Modified: Wed, 11 Mar 2020 14:31:14 GMT
ETag: "9c-5a09516a47080"
Accept-Ranges: bytes
Content-Length: 156
Content-Type: text/html; charset=UTF-8
```

\
\

### OpenSSL Libraries

#### How to Install/Update `openssl` library packages

- [link 1](https://www.howtoforge.com/tutorial/how-to-install-openssl-from-source-on-linux/#step-testing)

- [link 2](https://serverfault.com/questions/721867/can-i-upgrade-openssl-version-used-by-apache-without-recompiling-the-server-but)


\
\


### SSL/TLS Cert Verification

- `openssl s_client -showcerts -connect <IP/hostname>:80/443` 

  - this shows the ROOT cert (typically issued to your server by a CA) 

  - AND the intermediate certs in the bundle/chain 

- `openssl x509 -in <cert> -text` --> this only outputs cert issued by root CA

\
\
\

### Common Issues

- External CA Certificates Expiring

  - reference Ticket#2020062210010872

  - solution: delete expired cert in the cert bundle; if no other CA certs available, try downloading an alternative CAcert that will work in chain with the CA issuing your server's cert


### Resources:

- [openssl check remote certs](https://www.shellhacks.com/openssl-check-ssl-certificate-expiration-date/)

- [openssl decode local certs](https://www.shellhacks.com/decode-ssl-certificate/)


```
SSL/TLS Certs:
https://www.codesd.com/item/curl-says-the-certificate-has-expired-firefox-does-not-agree.html 
External CA Common:https://documentation.its.umich.edu/node/1988 
External CA Common:https://calnetweb.berkeley.edu/calnet-technologists/incommon-sectigo-certificate-service/addtrust-external-root-expiration-may-2020 
External CA Common: https://it.ucsf.edu/status/2020-05-14/addtrust-external-ca-root-certificate-will-expire-may-30th-2020 
External CA Common:https://support.sectigo.com/articles/Knowledge/Sectigo-AddTrust-External-CA-Root-Expiring-May-30-2020 
External CA Common:https://www.ssl.com/blogs/addtrust-external-ca-root-expired-may-30-2020/ 
https://serverfault.com/questions/394815/how-to-update-curl-ca-bundle-on-redhat (CA default bundle)
What’s my Chain Cert Test?https://whatsmychaincert.com/?nuclia.biohpc.swmed.edu 
Reset CAstore: https://access.redhat.com/solutions/1549003 
update-ca-trust https://stackoverflow.com/questions/37043442/how-to-add-certificate-authority-file-in-centos-7 
update-ca-trust https://techjourney.net/update-add-ca-certificates-bundle-in-redhat-centos/ 
Configure ca-certhttps://www.centlinux.com/2019/01/configure-certificate-authority-ca-centos-7.html 
Display remote certshttps://serverfault.com/questions/661978/displaying-a-remote-ssl-certificate-details-using-cli-tools 
What is ssl cert chain https://support.dnsimple.com/articles/what-is-ssl-certificate-chain/ 
What is ssl cross sign https://security.stackexchange.com/questions/14043/what-is-the-use-of-cross-signing-certificates-in-x-509 
https://its.umich.edu/computing/web-mobile/web-application-hosting/ssl-server-certificates (best cert chain practices)
```

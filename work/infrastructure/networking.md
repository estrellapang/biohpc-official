## Network Addresses & Configuration Critical to BioHPC Ops

### IPMI 

- log in to **biohpcadmin** 

  - **requires Windows Remote Desktop Client** 

  - u: `SWMED\zpang1` 

  - p: [UTSW pass] 

  - **only** `Ethernet 2` **can access various IPMI networks!** 

  - [IPMI network notations found here](https://docs.biohpc.swmed.edu/dokuwiki/doku.php?id=2017_ip_and_naming_changes) 

\
\

#### HP's IPMI Interface: iLO

- remote power on/off references: 

  - [official reference](https://community.hpe.com/t5/ProLiant-Servers-ML-DL-SL/Simple-iLO-question/td-p/4837375 ) 

  - [booting 1](https://www.webopedia.com/TERM/W/warm_boot.html) 

  - [booting 2](https://kb.netgear.com/1001/Defining-Terms-Power-Cycle-Boot-Reboot-Restart-Reset-and-Hard-Reset)

\
\

### DMZ DNS Addresses:

nameserver 199.242.239.11
nameserver 199.242.239.12

\
\

### Network Operations and Diagnostics

- **Network Gateways**

  - find default gateway: `ip r | grep default`

  - set default gateway: `route add default gw <IP> dev <NIC>`

    - e.g. set default gateway on 172 network: `route add default gw 172.18.239.254 dev em1`

- [reference 1](https://www.cyberciti.biz/faq/how-to-find-gateway-ip-address/)

- [reference 2](https://www.cyberciti.biz/faq/linux-setup-default-gateway-with-route-command/)


\

- **check default routes**

  - `ip route` vs. `route`

- **Sniff Out Duplicate IPs**

  - `arping`

\

- **network port connectivity**

  - `tracepath` is more fail-proof than telnet/ncat/nmap, which rely on applications to be listening for ports in order to show as "opened/connected"


  - `tracepath -p <port> <destination_IP>`

\
\
 
#### Add Static Routing

- `mtr` check routing

- `ip route add 198.215.54.0/24 via 198.215.56.254 dev enp0s8` --> route packets from one subnets to the interface connected to another subnet (56.0/24)

\
\

### iptables

- [how to enable logging](https://tecadmin.net/enable-logging-in-iptables-on-linux/)


\

### General Linux Networking Tips

- **references** 

  - [check which NIC is cabled-connected](https://www.ostechnix.com/how-to-find-out-the-connected-state-of-a-network-cable-in-linux/)


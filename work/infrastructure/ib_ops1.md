## FIRMWARE, BUS Control, & Attached Devices Info

### IB Hardware

- **check Mellanox adapter type**

```
lspci | grep -i mellanox
81:00.0 Network controller: Mellanox Technologies MT27500 Family [ConnectX-3]

  # "81:00.0" is the bus address of card---good for FW updates


# check device name via `mst` command

mst status
------------------------------------------------------------------
MST modules:
------------
    MST PCI module is not loaded
    MST PCI configuration module loaded
MST devices:
------------
/dev/mst/mt4119_pciconf0         - PCI configuration cycles access.
                                   domain:bus:dev.fn=0000:af:00.0 addr.reg=88 data.reg=92
                                   Chip revision is: 00
------------------------------------------------------------------
```

- **check OFED version**

```
 ofed_info | grep MLNX_OFED_LINUX
MLNX_OFED_LINUX-4.3-1.0.1.0 (OFED-4.3-1.0.1)

## find location of mellanox packages
------------------------------------------------------------------
rpm -qa | grep -i mlnx 

find / -name "<mellanox rpm>"

------------------------------------------------------------------

```

\
\

- **check HCA (IB card) firmware**

```
ibstat | grep -i Firmware
	Firmware version: 2.42.5000

  ## also try ##

ibstatus

ibv_devinfo
```

\
\

### IB Network Debug

- BioHPC/MA generated netwk check script:

  - `/project/biohpcadmin/shared/scripts/ib-check-ver1` --> Run as root

- `ibdiagnet`

```
## get on ADMIN/MANAGE Node
---------------------------
sudo ibdiagnet

  # requires all ports up + firewalld off
---------------------------

## the command will do a net discovery & output the following files
---------------------------
ibdiagnet2.aguid
ibdiagnet2.db_csv
ibdiagnet2.debug
ibdiagnet2.log
ibdiagnet2.lst
ibdiagnet2.net_dump
ibdiagnet2.nodes_info
ibdiagnet2.pkey
ibdiagnet2.pm
ibdiagnet2.sm

  # these will offer good insight into health of IB network
---------------------------

```

- `ibtracert` --> traceroute for IB

```
ibtracert <lid> <lid>

 # fetch lid (link ids -- one for each port) via `ibv_devinfo` -> "port_lid" is  the one to use
 
```

- `ibqueryerrors` --> run network discovery to diagnose issues (quickly runs through all connections) 

- `ibswitches` --> fetch info on all switch that node can see

\
\

- **config files**

 - `/etc/infiniband/openib.conf` 

 - `/etc/modprobe.d/mlx4.conf` --> for ConnectX-3 & -4

   - `~/~/mlx5.conf` --> for ConnectX-5


\
\

- **restart ops**

 - restart HCA Drivers: `/etc/init.d/openibd restart`   

 - the above needs to be preceded by the following if lustre clients are installed:

```
modprobe -r lustre

lustre_rmmod  --> gets rid of lnet kernel modules

```

### IB Write Bandwidth Check

- [helpful video](https://www.google.com/search?q=how+to+test+infiniband+throughput&rlz=1C1CHBF_enUS818US818&oq=how+to+test+infiniband+throughput&aqs=chrome..69i57.10738j0j7&sourceid=chrome&ie=UTF-8#kpvalbx=_BLxVXoqOEfur0PEPoN2XwAo17)

```
# WRITE Bandwidth

# on server:
ib_write_bw 

# on client:
ib_write_bw <hostname/IP>


# Send Bandwidth
ib_send_bw	# same syntax

# Test Network via IPERF

  # on Server: iperf -s 

  # on Client: iperf -c <IP> -P 8  (8 threads)

```


### IB SWITCH UPGRADE:

```
ssh admin@<i.p>

show ib sm --> show subnet manager or not

# shut down all ports

 # select all ports & shut them down

interface ib 1/1-1/36

shut

exit (if needed)

# check a few ports to ensure 

show interfaces ib 1/<port #> --> check port status

# show available images

show images

image boot next (boot next from the backup image)

write mem

image install <image name>  # this does it on the current partition

  # if jumping major versions current switch configurations will be lost --> need serial connection to physical switch to re-implement original switch config

# after update finishes

reload --> ENTER 

# initial switch configuration:
-----------------------------------------------------
quickly shutdown all switch ports after reboot
  # bc of cnfig loss

show interfaces ib 1 1/<port number>

make sure subnet manager disabled 

after network is up do

`ibnetdiscovery` --> tee to file and grep for switch of interest

run `/project` or `/archive` ib switch command

run command to check speed
-----------------------------------------------------
```


### On GUI

"System" --> "MLNX-OS Upgrade" --> check active Partition

"Next-Boot Options" --> UNCHECK "Fallback reboot" (checks config after reboot, fail-safe in case of bad upgrade) 

\
\
\

### 3-11-2020: Core Switch 002 FW Upgrade

- Cisco Webex Meetings apps

 - share SSH session with live support

- Info

  - weird qdr 

```
# mismatch SM key---typically due to cluster nodes having subnet manager enabled or having different OS/FW versions 

  # no concern so long as priority on that switch node is low

```

- general steps

 - 192.168.53.226 (SM switch: priority 2)

 - 192.168.53.237 (SM switch: priority )



 - copy config file + FW image to Switch 

 - switch went down @ 12:40ish
 
 - check if any jobs failed during that time


```
###################### OFFICIAL DOCUMENTATION ##########################

- Spot Parallel Jobs using `squeue` --> any user more than 1 node need to be notified

- On the GUI

# Before Reboot:
- System --> UNCHECK "Fallback reboot" --> "Apply Changes" --> logout and log back in to check if configuration went through

# After Reboot, Show Continuous Log

- Status --> logs --> check logs of current events (do this while upgrading)


- On the CLI

### SSH in from Nucleus003/Nucleus006(?)

ssh admin@192.168.53.<> 

  # PASS: 1st part of root pass

# Enter Configure Mode

enable --> configure terminal 

# Ensure it's not the subnet manager

show ib sm 

# copy newest image from switch:

image fetch scp://zpang1@192.168.53.252/project/biohpcadmin/shared/Mellanox/switchers/SB7700/<image file name>.img

  # double verify the IPMI IP of the server from which you are copying from! (e.g. nucleus006)

# show switch config and copy it to text editor for backup
show running-config 

# check port statuses prior to update

show interfaces ib status

# show images

show images

  # check current partition: "Last boot partition" 

  # new image can only be installed on the inactive partition, which we want to the next boot to boot into

  # rotate boot partition to one in which we will install the new image

image boot next

  # ensure that this decision is remembered
  
write memory

  # VERIFY

image boot next


# shut down ports

  # this selects them
interface ib 1/1-1/36

  # shut down ALL of the selected ports
shut

exit

# Verify that a few ports are shutdown
show interfaces ib 1/<port>

# start upgrading to new image
image install <image name>

  # while waiting we can get into GUI and check status logs

#once upgrade finishes, reboot

show images

reload 

# confirm that changes have been made

yes

# establish console connection while rebooting, see if errors occur on booting screen

  # hit enter once serial connection has started

enable
configure terminal

# check if pre-existing configuration has been retained

show running-config

check ib ports--> make sure down

# check current partition has correct image

show images

show version

# bring up ports

interface ib 1/1-1/36

no shut

# Wait for 1-2 mins, check all port status & compare with previous output 

show interfaces ib status


\
\

# Notify Users whose jobs have failed after:
-----------------------------------------------------------
sacct -aS <YYYY>-<MM>-<DD>T<HH>:<MM>:00 | grep -i FAILED
-----------------------------------------------------------

\
\

################## END of Offical SW Upgrade Documentation ##################


### Extra INFO ###

# check priortiy of SM via Nuc006 or 003

saquery 376 --> show sm switch info

  # get SM lid via "ibv_devinfo"

# back on switch

# check ports (ignore ib0 which is internal) 

show interfaces ib status

  # if "DOWN" or "Polling" --> they are disconnected ports 

  # check previous ibnetdiscover outputs to double verify if ports were previously down

```

\
\
\

### HCA Adapter FW Upgrade

```
### IB CARD FIRMWARE UPDATE:

newGPUS=(002 003 004 005 006 007 008 009 010 011 012 013 014 015 016 017 030 031 032 033 034 035 036 037 038 039 047 250)

# count

echo ${#newGPUS[@]}
28

# double verify listing:

for i in ${newGPUS[@]}; do echo "this is NucleusC$i" ; done

# check firmware and card PID for each node

for i in ${newGPUS[@]}; do echo "HCA FW info for NucleusC$i";sudo ssh NucleusC$i "ibv_devinfo | grep -i 'board_id\|fw_ver' ";done

# get an unzipped copy of firmware bin file, then copy to /tmp directory of all nodes:

 for i in ${newGPUS[@]}; do echo "copying to NucleusC$i"; scp /tmp/fw-ConnectX5-rel-16_26_4012-872725-B21_Ax-UEFI-14.19.17-FlexBoot-3.5.805.signed.bin NucleusC$i:/tmp; done

  # verify:

 for i in ${newGPUS[@]}; do ssh NucleusC$i "ls -l /tmp | grep -i fw" ; done

# check IB card bus address of al nodes:

for i in ${newGPUs[@]}; do sudo ssh NucleusC$i "lspci | grep -i Mell"; done

  # sample output `af:00.0 Infiniband controller: Mellanox Technologies MT27800 Family [ConnectX-5]`  --> we want "af:00.0" as address!

# do a test round before applying to all nodes:

testGPUs=(002 003 004)

 for i in ${testGPUs[@]}; do echo "Updating HCA FW for NucleusC$i"; sudo ssh NucleusC$i "mstflint -d af:00.0 -i /tmp/fw-ConnectX5-rel-16_26_4012-872725-B21_Ax-UEFI-14.19.17-FlexBoot-3.5.805.signed.bin burn"; done

------------------------------------------------------------
e.g. of successful output:

Updating HCA FW for NucleusC002

    Current FW version on flash:  16.21.2808
    New FW version:               16.26.4012

Initializing image partition -   OK          
Writing Boot image component -   OK          
-I- To load new FW run mlxfwreset or reboot machine.
Updating HCA FW for NucleusC003

    Current FW version on flash:  16.21.2808
    New FW version:               16.26.4012

Initializing image partition -   OK          
Writing Boot image component -   OK          
-I- To load new FW run mlxfwreset or reboot machine.
Updating HCA FW for NucleusC004

    Current FW version on flash:  16.21.2808
    New FW version:               16.26.4012

Initializing image partition -   OK          
Writing Boot image component -   OK          
-I- To load new FW run mlxfwreset or reboot machine.

------------------------------------------------------------

# if test works; apply to rest

```

\
\

### OFED Driver Install

```
wget http://content.mellanox.com/ofed/MLNX_OFED-4.7-1.0.0.1/MLNX_OFED_LINUX-4.7-1.0.0.1-rhel7.6-x86_64.tgz

tar -xvxf MLNX_OFED_LINUX-4.7-1.0.0.1-rhel7.6-x86_64.tgz

yum install perl gtk2 atk cairo gcc-gfortran tcsh lsof tcl tk

cd /root/deploy/MLNX_OFED_LINUX-4.7-1.0.0.1-rhel7.6-x86_64

./mlnxofedinstall --without-fw-update

sudo reboot

## Upon Reboot, Verify OFED Driver & CLI utility installs

ofed_info | head -n 1

/etc/init.d/openibd status

ibdev2netdev

ibv_devinfo

ibstatus
```

\
\

### InfiniBand Background Knowledge

- What's the difference between connected/datagram mode

- Adaptive routing implementation

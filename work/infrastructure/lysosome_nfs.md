## Debugging on BioHPC NFS Servers:

### Ticket#2019102410008054

- Inquiry:

```
Dear OceanStor Dorado5000_V3 users:
System Name: Huawei.Storage
Location:
Customer Info:
ESN: 2102351GSB10J2000001
ID: 0xF03370002
Level: Warning
Occurred At: 2019-07-24 10:03:14 DST
Details: SMTP mail server is unavailable. Cause: (network error). Error code: 0x4003371c.
Suggestions: Step1 Check whether the server address is correctly configured.
1.1 If no=>correct the configuration.
1.2 If yes=>[Step2].
Step2 Check whether the user name and password of the server are correctly configured.
2.1 If no=>correct the configurations.
2.2 If yes=>[Step3].
Step3 Check whether the network connection between the controller and the server is
correct.
3.1 If no=>recover the network connection.
3.2 If yes=>[Step4].
Step4 Check whether the dump server path is configured correctly and has the write
permission.
4.1 If no=>correct the configurations.
4.2 If yes=>[Step5].
Step5 Check whether the NTP certificate matches the server certificate.
5.1 If no=>import correct NTP certificate.
5.2 If yes=>[Step6]
Step6 Collect related information and contact technical support engineers.

```

- Troubleshoot & Resolve:


### On Storage Controllers: 

- 192.168.0.233 (&234):8088

 - log in via biohpcadmin VM:

   - "Settings" --> "Alarm Settings" 

```
# Sender Email:

biohpc-help@utsouthwestern.edu (this is the origin email address we **WANT** to see)

# SMTP Server (meaning relay server in native linux terms)

192.168.0.236,237 --> ipmi network on lyosome0,1 respectively!!!

# Port

25 

# Recipient Email 

biohpc-help@utsouthwestern.edu

``` 

### On the NFS Servers:

```
### Check if postfix (mail server)  is configured properly

systemctl status postfix

### vi /etc/postfix/main.cf

# following entries are MUST!
--------------------------
relayhost=198.215.54.5 
inet_interfaces = all   # let smtp server listen for all other interfaces for requests
--------------------------

### check ports 

systemctl restart postfix

netstat -lntup | grep -i ':25' 

-------------------------
tcp        0      0 0.0.0.0:25              0.0.0.0:*               LISTEN      105203/master       
tcp6       0      0 :::25                   :::*                    LISTEN      105203/master
-------------------------
```

### CAMPUS SMTP Server: `smtp.swmed.org` 


### Pacemaker/Corosync Configurations

- `pcs status` --> check cluster status

```
Cluster name: lysosomecluster
Stack: corosync
Current DC: nfs1 (version 1.1.18-11.el7-2b07d5c5a9) - partition with quorum
Last updated: Mon Nov  4 13:05:18 2019
Last change: Sat Feb  9 21:51:42 2019 by root via cibadmin on nfs0

2 nodes configured
7 resources configured

Online: [ nfs0 nfs1 ]

Full list of resources:

 clusterVIP	(ocf::heartbeat:IPaddr2):	Started nfs1
 clusterLVM	(ocf::heartbeat:LVM):	Started nfs1
 clusterhome	(ocf::heartbeat:Filesystem):	Started nfs1
 nfsDaemon	(ocf::heartbeat:nfsserver):	Started nfs1
 clusterVIPtcp	(ocf::heartbeat:IPaddr):	Started nfs1
 nfs0Fencenfs1	(stonith:fence_ipmilan):	Started nfs0
 nfs1Fencenfs0	(stonith:fence_ipmilan):	Started nfs1

Daemon Status:
  corosync: active/disabled
  pacemaker: active/enabled
  pcsd: active/enabled

```

### NFS Server Failure: 

- 11-10->15-2019

- **debug attempts: xfs quota not being implemented** 

- `rpcinfo -p` 

```
   program vers proto   port  service
    100000    4   tcp    111  portmapper
    100000    3   tcp    111  portmapper
    100000    2   tcp    111  portmapper
    100000    4   udp    111  portmapper
    100000    3   udp    111  portmapper
    100000    2   udp    111  portmapper
    100011    1   udp    875  rquotad
    100011    2   udp    875  rquotad
    100011    1   tcp    875  rquotad
    100011    2   tcp    875  rquotad
    100024    1   udp  56666  status
    100024    1   tcp  37286  status
    100005    1   udp   4002  mountd
    100005    1   tcp   4002  mountd
    100005    2   udp   4002  mountd
    100005    2   tcp   4002  mountd
    100005    3   udp   4002  mountd
    100005    3   tcp   4002  mountd
    100003    3   tcp   2049  nfs
    100003    4   tcp   2049  nfs
    100227    3   tcp   2049  nfs_acl
    100003    3   udp   2049  nfs
    100003    4   udp   2049  nfs
    100227    3   udp   2049  nfs_acl
    100021    1   udp  32999  nlockmgr
    100021    3   udp  32999  nlockmgr
    100021    4   udp  32999  nlockmgr
    100021    1   tcp  42128  nlockmgr
    100021    3   tcp  42128  nlockmgr
    100021    4   tcp  42128  nlockmgr

### However this does not match the ports specified in `/etc/sysconfig/nfs`

```
# Optinal options passed to rquotad
RPCRQUOTADOPTS="--port 4003"

```

- `mount` --> see if quota option is being implemented

```
.
.
.
/dev/mapper/vg_export-newhome on /home2 type xfs (rw,relatime,attr2,inode64,sunit=1024,swidth=4096,noquota) --> NOT BEING Implemented! 

```

- resolve attempt: lazy unmount + remount with `-o quota` option 

```
# lazy unmount:

umount -l /home2

# re-mount: 

mount -o usrquota /dev/vg_export/newhome /home2

mount -o quota /dev/vg_export/newhome /home2 

### THIS DID NOT WORK!!! ###  

[RHEL link on why this doesn't work](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Storage_Administration_Guide/ch03s09.html#idp21353040)


```


### NFS Mount Checks

- [nfsstat mount details](https://linoxide.com/linux-how-to/example-linux-command-to-find-nfs-version/)


- [client mount verbose for debug](https://unix.stackexchange.com/questions/303846/where-are-nfs-v4-logs-under-systemd)

- [starting and stopping nfs server](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/storage_administration_guide/s1-nfs-start) 

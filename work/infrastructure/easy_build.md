## Using Easy-Build

### Info from AA's Intro Training 

- is it dependent on lmod? 


- CHECKOUT easybuilders github page 

  - `.eb` files as recipe, many examples for common packages
  
    - dependencies will be listed in the files

- alternative: Spack

- Others: sbgrid(end users), conda(end users, hard to modify and fix if broken), nix, GuixHPC

- Singularity - containers for HPC, under BSD license(more freedom than GPL)

  - can linux containers run on windows? Not so easy --> use Wine to make linux applications work on PC
  
- why PC applications depend on .NET?

- lmod (more advantageous) as alternative to tclc module environment

  - backend of easybuild?

  - more automated and works with easy-build, no module file needed as opposed to current one

  - what are different module naming schemes--? Flat or Hierarchal?
  
- when applications require IB network support for e.g. mpi libraries

  - check flags in reciple file, e.g. in the recipe files, especially for openMPI-GCC, different available handles
  will be listed, e.g. GPU support and IB support (ALWAYS check! -- look for 'cuda' which will give the GPU compiled packages)


- to create environment on cluster

  - follow script in $HOME (see script in screenshot)
  
  - to install in shared location, specify lmod location in config
  
  -  and then they can install packages in their home directory as needed
  
    - the hierarchal module structure will avoid overlapping for dependencies for packages with similar requirements
  
  - follow output to check location of log file

  - NOTE: one can specify his/her own module search locations in module file config, which can use lmod work on the cluster even though
the main module system, even though the main module system is not lmod

- to integrate to BioHPC, dedicate separate easybuild location than default module location (which doesn't use lmod)

  - how to designate "global" easybuild dependency separate from a local install location?

- to delete, simply delete module file & directory

\

- **Tutorial** 

```
module show EasyBuild  # this will show the location of log files & critical locations

# e.g. install tmux

eb tmux-2.3.eb -D  # `-D` dry-run

  # shows you the dependencies that will be installed
  
  # will automatically resolve dependencies

eb tmux-2.3.eb --robot

  # real install, `--robot` --> auto resolve dependencies
  
```

\

- Additional

```
# one can set default "D" and hidden "H" modules (hidden files are common dependencies that users do not need to interact with)

# use of symlinks for module paths 

# users can just install in their home2 dir and build packages there
```

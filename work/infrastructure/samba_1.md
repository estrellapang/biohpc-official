## Samba Setup & Troubleshooting

### Server Commands 

- Reload configurations: 

  - `smbcontrol all reload-config`

- Check active client connections:

  - `smbstatus --shares`

- Check export list & active mounts & locked files

  - `smbstatus`

- Check if users are in database:

  - `sudo pdbedit -L -u -v <>`

- Check location of important config/database files

  - ` smbd -b`

  - [reference](https://www.linuxtopia.org/online_books/network_administration_guides/samba_reference_guide/07_install_06.html)

\
\


### smb.conf

- Set server to max debug level for troubleshooting:

  - [client-specific debug logging](https://wiki.samba.org/index.php/Client_specific_logging)

```
# set the debug level
log level = 10
# add the pid to the log
debug pid = yes
# add the uid to the log
debug uid = yes
# add the debug class to the log
debug class = yes

```

- Control Log Size 

  - log file size is in KBs: `max log size = <KBs>` 

  - unrestrict log size --> `max log size = 0`

- Keep logging to per client machine

  - `log file = /var/log/samba/log.%m`

- support legacy SMB1 protocol & "unix extensions"

  - `server min protocol = NT1`



\
\

### Client CMDs

- required Enterprise Linux packages: `cifs-utils`

- **mount options**

  - fstab mount format

```
\\<server hostname/IP>\sharename   /<local mtpt>   <smb/cifs>   netdev,uid=<>,credentials=<specific hidden file>,file_mode=<###>,dir_mode=<###> 0 0

# credentials file follows format:
--------------
username=<user in samba db>
password=<plain>
domain=<samba domain, optional>
--------------
```

  - cmd line mount format

```
mount -t cifs //<server IP>/<share name> /<local mtpt>/ -o user=<domain>\<usr>,vers=<1.0-3.0>,sec=<authentication method>

# e.g.
mount.cifs -o user=ansir_ext,sec=ntlmssp,domain=BIOHPC //198.215.54.##/ansir_ext /XNAT/EXTERNAL

# add `-vvv` handle to `mount` for detailed output

```

- resources: 

- [comprehensive guide on Samba client mounting](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/storage_administration_guide/mounting_an_smb_share)


\
\

### Troubleshooting

- **network ports**

  - 445 --> CIFS/TCP

  - 139 --> SMB/TCP

  - might need UDP ports 137,138

- check smb version & CIFS kernel module versions

```
sudo rpm -qa | grep -i smb
modinfo cifs
```



- **Mount Errors**

- `mount error(13): Permission denied`

  - detailed errors:

```
kernel: CIFS VFS: Send error in SessSetup = -13
kernel: CIFS VFS: cifs_mount failed w/return 
```

- cause: client is on samba version < 3.6, whose default authentication method is different than that of current versions

- solution: use `sec=ntlmssp` on legacy client mount options 

  - [issue documentation 1](https://villasyslog.net/cifs-vfs-sesssetup-13-cifs_mount-failed-xen-unix/)

  - [issue documentation 2](https://access.redhat.com/solutions/705253)

  - [issue documentation 3](https://access.redhat.com/solutions/450913)

\


- General Debug Resources:

  - [differences between SMB and CIFS protocols 1](https://discussions.apple.com/thread/280511)



\
\
\

### Performing Backups

#### references

- [verbose info on Samba Active Directory Domain Controller backups](https://wiki.samba.org/index.php/Back_up_and_Restoring_a_Samba_AD_DC)

- [description of importatnt database files](https://wiki.samba.org/index.php/TDB_Locations)

- [show location of database files](https://www.linuxtopia.org/online_books/network_administration_guides/samba_reference_guide/07_install_06.html)

- [how to join samba as a domain controller to active directory](https://www.linuxsecrets.com/samba-wiki/index.php/Joining_a_Samba_DC_to_an_Existing_Active_Directory.html) 

- [trivial? back up and restore Samba AD DC, which not everyone uses](https://wiki.samba.org/index.php/Back_up_and_Restoring_a_Samba_AD_DC)

\

### Samba Concepts

#### TDB database

- [brief explanation](https://www.linuxsecrets.com/388-how-to-backup-samba-domain-controller-configuration-in-linux)

\

#### Domain Controller vs Active Directory

- [what's a domain controller 01](https://www.varonis.com/blog/domain-controller/)

- [domain controller vs active directory](https://adamtheautomator.com/domain-controller-vs-active-directory/)


\
\


## VM Management in vSphere Web Client

### Logging In

- URL: `https://biohpcvcenter.swmed.org/` 

- user: `biohpcadmin@vpshere.local`



### Creating A VM 

- **copy ISOs**

```
# SSH into ESXI host as "root" and traverse into data store, check actual path of iso directory

  # copy true path (should include hashed name)

# e.g.
----------------------------------------------
scp ~/Desktop/sources/isos/rhel-server-7.7-x86_64-dvd.iso root@129.112.9.36:/vmfs/volumes/5a6b61af-51854600-850a-b083feec6b3d/isos/
----------------------------------------------
```

\
\
\

```
# Decide Hardware

- how many CPUs? (16000 shares, hardware virtualization & performance counters DISABLED, reservation 0) 

- Memory (327680 MB, reservation 0, limit unlimited)

- Network (adapter type: "VMXNET 3" + "VM Network 2" + [check available 54.0/24 slots here](https://docs.biohpc.swmed.edu/dokuwiki/doku.php?id=ip_assignments_198.215.54.0_24) )

- Video (8 MB)

- Storage (160GB--Thick Provision lazy zeroed) 

# Assign Name: 

galaxy-rhel-latest2019

```

### Logs from galaxy19

```
sudo yum install xfsdump

xfsdump -f /tmp/home.bak /home 

 ============================= dump label dialog ==============================

please enter label for this dump session (timeout in 300 sec)
 -> test
session label entered: "test"

 ============================= media label dialog =============================

please enter label for media in drive 0 (timeout in 300 sec)
 -> test
media label entered: "test"

 --------------------------------- end dialog ---------------------------------

sudo umount /home

ls /dev/rhel/

sudo lvreduce -L 70G /dev/rhel/home

sudo mkfs.xfs -f /dev/rhel/home

sudo mount -a

sudo xfsrestore -f /tmp/home.bak /home

sudo lvextend -L 160G /dev/rhel/root

sudo xfs_growfs -d /dev/rhel/root


### On Nucleus006:

-------------------------------------------------------------------------------
sudo scp -vp es320-lustre-2.7.21.3.ddn23.gcefc235.tar.gz biohpcadmin@198.215.54.117:/tmp/ 

  ### trailing "/" is very important!!!

-------------------------------------------------------------------------------

sudo yum install openldap-clients nss-pam-ldapd

sudo authconfig --enableldap --enableldapauth --ldapserver=ldaps://198.215.54.45:636 --ldapbasedn="ou=users,dc=biohpc,dc=swmed,dc=edu" --disableldaptls --update

sudo yum install kernel-devel libselinux-devel rpm-build

sudo tar -xf es320-lustre-2.7.21.3.ddn23.gcefc235.tar.gz

cd lustre-2.7.21.3.ddn23.gcefc235/

sudo ./configure --with-linux=/usr/src/kernels/$(uname -r) --without-o2ib --disable-server

sudo make rpms

cd /tmp/lustre-2.7.21.3.ddn23.gcefc235/

sudo yum install *.rpm

sudo vim /etc/selinux/config

# add 10G IP to lustre cluster's iptables, then `mount -a`

```


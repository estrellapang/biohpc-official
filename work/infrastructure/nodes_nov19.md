## Server/Workstation Debug

### Kworker Utilizing 100% CPU, Gnome/X Session Frozen

- [good resource](https://askubuntu.com/questions/33640/kworker-what-is-it-and-why-is-it-hogging-so-much-cpu) 

```
### Perform Call-Trace via `echo 1 > /proc/sysrq-trigger`, this brings the debug level up to 1 

### Re-go through /var/log/messages

### The backtrace messages will return important clues as to whether the issue is arising out of hardware, drivers, network storage, etc.

```

### `/var` filling up 

```
# clean /var/cache/yum
----------------------
yum clean all
----------------------

# see what processes are writing into it:
----------------------
lsof +L1 | grep "/var"
----------------------

  # many times it is intensive, parallel jobs

```

## Operation Logs for ldap002

> 198.215.54.45 

### Ticket#2019082810008088

- **ssl certificate expiring in 30 days**

- diagnosis:

```
[biohpcadmin@ldap002 2018]$ /usr/local/etc/openldap/2018

[biohpcadmin@ldap002 2018]$ openssl x509 -in cacert.pem -text 
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number: 4096 (0x1000)
    Signature Algorithm: sha1WithRSAEncryption
        Issuer: C=US, ST=TX, O=UTSW, OU=BIOHPC, CN=BIOHPC/emailAddress=BIOHPC-HELP@UTSOUTHWESTERN.EDU
        Validity
            Not Before: Sep 27 23:09:52 2018 GMT
            Not After : Sep 24 23:09:52 2028 GMT
        Subject: C=US, ST=TX, O=UTSW, OU=BIOHPC, CN=BIOHPC/emailAddress=BIOHPC-HELP@UTSOUTHWESTERN.EDU
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:d0:a0:7d:bb:b2:aa:87:0f:89:50:91:cc:ad:5d:
                    7d:c6:4d:62:90:7a:17:83:46:41:5a:50:48:e5:6f:
                    8d:e9:24:44:69:10:5a:5b:d0:ec:bd:58:4e:1e:e1:
                    56:65:77:aa:2d:11:78:11:e2:8b:5d:b6:67:e4:5b:
                    9d:dc:42:aa:ac:9e:6e:05:59:13:05:57:6a:71:2e:
                    86:79:0a:f8:4f:f6:ad:fb:5f:23:80:ab:b1:e3:be:
                    8c:4e:08:cc:1e:c5:a1:29:03:8b:e7:2e:5c:18:3c:
                    bd:f8:75:20:1a:21:2f:ae:d6:a5:98:48:c6:71:3a:
                    6b:d8:38:2e:08:32:79:cc:ff:eb:6f:fb:17:8f:bc:
                    47:b1:02:b8:19:34:db:53:7e:e5:87:7a:61:62:4e:
                    c4:d7:a9:3c:16:c8:22:aa:2f:54:42:2b:6d:b7:d0:
                    fe:fa:77:b9:03:50:9a:77:ec:dd:6a:6d:ec:af:c4:
                    6d:bd:45:96:b6:47:74:24:7e:d6:c6:16:c9:f3:cc:
                    95:36:d8:32:e8:df:2e:a4:97:13:eb:34:31:d7:09:
                    ba:83:66:4e:82:74:91:e7:cc:87:aa:90:9a:06:be:
                    cd:f3:ed:d1:2e:4e:0f:e0:a7:9d:d5:da:b6:bb:e0:
                    80:3c:38:be:8e:35:bb:fc:e3:28:23:ca:45:98:a5:
                    d9:05
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Subject Key Identifier: 
                74:05:C8:8C:60:37:6A:6D:11:D2:C6:2A:EA:0A:89:23:E6:FE:9E:FB
            X509v3 Authority Key Identifier: 
                keyid:74:05:C8:8C:60:37:6A:6D:11:D2:C6:2A:EA:0A:89:23:E6:FE:9E:FB

            X509v3 Basic Constraints: 
                CA:TRUE
    Signature Algorithm: sha1WithRSAEncryption
         ba:6c:f2:64:33:2d:b3:d7:9a:d6:e3:49:77:eb:ce:9b:72:f0:
         ca:58:99:39:fd:58:e9:9a:78:ac:85:aa:e4:4c:8c:75:15:f6:
         d6:ca:23:68:83:8d:17:2e:a9:76:2b:ea:26:6d:25:22:47:a4:
         91:7e:85:50:80:67:7a:1e:f7:0e:37:3f:36:9d:b2:11:a2:23:
         97:16:20:86:ac:1b:71:0d:f9:a7:18:57:c4:3d:a9:b8:18:55:
         ce:3b:3c:d9:07:7f:3d:e1:73:00:c8:40:f1:6d:8d:b8:7f:ff:
         2c:1a:db:0e:5a:e1:dd:22:29:4f:d5:24:9d:62:e0:0f:2a:cb:
         a3:e3:35:7e:cd:c5:bb:cf:32:1e:79:05:ab:f6:cb:7f:aa:64:
         62:e8:b4:32:e2:78:7f:25:42:c1:b0:da:2d:99:5f:bd:ed:55:
         0f:84:b2:59:2b:71:24:a2:9c:16:2a:2b:bc:d4:40:51:95:c5:
         16:49:dc:9e:ae:28:6e:40:b7:23:77:ab:47:02:8c:be:06:5e:
         18:0e:a5:ae:fa:7e:8f:12:8a:4c:a3:91:3a:be:0e:ac:cc:d1:
         7a:79:00:27:64:15:fd:20:cc:c5:ac:ef:94:7d:fe:81:3e:e1:
         4f:2c:55:ba:78:e5:48:98:c4:68:27:27:c9:58:9e:de:90:73:
         63:0e:71:01
-----BEGIN CERTIFICATE-----
MIIDwDCCAqigAwIBAgICEAAwDQYJKoZIhvcNAQEFBQAwejELMAkGA1UEBhMCVVMx
CzAJBgNVBAgMAlRYMQ0wCwYDVQQKDARVVFNXMQ8wDQYDVQQLDAZCSU9IUEMxDzAN
BgNVBAMMBkJJT0hQQzEtMCsGCSqGSIb3DQEJARYeQklPSFBDLUhFTFBAVVRTT1VU
SFdFU1RFUk4uRURVMB4XDTE4MDkyNzIzMDk1MloXDTI4MDkyNDIzMDk1MlowejEL
MAkGA1UEBhMCVVMxCzAJBgNVBAgMAlRYMQ0wCwYDVQQKDARVVFNXMQ8wDQYDVQQL
DAZCSU9IUEMxDzANBgNVBAMMBkJJT0hQQzEtMCsGCSqGSIb3DQEJARYeQklPSFBD
LUhFTFBAVVRTT1VUSFdFU1RFUk4uRURVMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8A
MIIBCgKCAQEA0KB9u7Kqhw+JUJHMrV19xk1ikHoXg0ZBWlBI5W+N6SREaRBaW9Ds
vVhOHuFWZXeqLRF4EeKLXbZn5Fud3EKqrJ5uBVkTBVdqcS6GeQr4T/at+18jgKux
476MTgjMHsWhKQOL5y5cGDy9+HUgGiEvrtalmEjGcTpr2DguCDJ5zP/rb/sXj7xH
sQK4GTTbU37lh3phYk7E16k8Fsgiqi9UQittt9D++ne5A1Cad+zdam3sr8RtvUWW
tkd0JH7WxhbJ88yVNtgy6N8upJcT6zQx1wm6g2ZOgnSR58yHqpCaBr7N8+3RLk4P
4Ked1dq2u+CAPDi+jjW7/OMoI8pFmKXZBQIDAQABo1AwTjAdBgNVHQ4EFgQUdAXI
jGA3am0R0sYq6gqJI+b+nvswHwYDVR0jBBgwFoAUdAXIjGA3am0R0sYq6gqJI+b+
nvswDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQUFAAOCAQEAumzyZDMts9ea1uNJ
d+vOm3LwyliZOf1Y6Zp4rIWq5EyMdRX21sojaIONFy6pdivqJm0lIkekkX6FUIBn
eh73Djc/Np2yEaIjlxYghqwbcQ35pxhXxD2puBhVzjs82Qd/PeFzAMhA8W2NuH//
LBrbDlrh3SIpT9UknWLgDyrLo+M1fs3Fu88yHnkFq/bLf6pkYui0MuJ4fyVCwbDa
LZlfve1VD4SyWStxJKKcFiorvNRAUZXFFkncnq4obkC3I3erRwKMvgZeGA6lrvp+
jxKKTKOROr4OrMzRenkAJ2QV/SDMxazvlH3+gT7hTyxVunjlSJjEaCcnyVie3pBz
Yw5xAQ==
-----END CERTIFICATE-----


## HINT: we can display similar info as a client: 

[zpang1@Nucleus006 ~]$ openssl s_client -showcerts -connect 198.215.54.45:636

```

- tentative solution links:

  - [best bet](https://stackoverflow.com/questions/25620685/how-to-renew-a-self-signed-openssl-pem-certificate) 

  - [essentially the same info](https://support.qacafe.com/knowledge-base/how-do-i-display-the-contents-of-a-ssl-certificate/)

  - [info about .cert & .cacert](https://stackoverflow.com/questions/51134255/difference-between-cert-vs-cacert-x-509) 

  - [ssl info from ldap's official site](https://ldapwiki.com/wiki/Obtain%20a%20Certificate%20from%20Server)

- solution: 

 - **issue** Long created a certificate to self-sign the CA certificate; while the latter lasts for 10 years, the former only lasts for 1 year. 

 - **approach** generate a new .cert--this time lasting for 10 years--and self sign the CA again 

   - push to all clients via ansible

   - **WHERE ARE THE OLD CERTIFICATES?**
   
     - (answer will be found in the ensuing info)

### Update: September 5th, 2019 ### 

- Renewal procedure summarized 

```
[root@ldap002 2018]# cd /root/2018/
[root@ldap002 2018]# ll
total 44
-rw-r--r--. 1 root root 1054 Sep  4 16:47 biohpcadmin@198.215.56.101
-rw-r--r--. 1 root root 4453 Sep  4 16:47 cacert.pem
-rw-r--r--. 1 root root 1834 Sep  4 16:47 cakey.pem
-rw-r--r--. 1 root root 1054 Sep  4 16:47 careq.pem
-rw-r--r--. 1 root root 4629 Sep  4 16:47 ldap-002cert.pem
-rw-r--r--. 1 root root 2762 Sep  4 16:47 ldap-002.pem

openssl req -new -nodes -keyout ldap-002.pem -out ldap-002.pem​

openssl ca -policy policy_anything -keyfile cakey.pem  -cert cacert.pem  -out ldap-002cert.pem -infiles ldap-002.pem


cd /usr/local/etc/openldap/2018/
 mv ldap-002cert.pem ldap-002cert.pem.2018
mv ldap-002.pem ldap-002.pem.2018
cp /root/2018/ldap-002* .

in Client:
Copy new ldap file to client by using ansible to /etc/ssl/certs/ldap002-cert.pem​
systemctl restart nslcd

```


### Links Useful for Future Development

- [link 1](https://www.server-world.info/en/note?os=CentOS_7&p=openldap&f=4) 

- [link 2](https://www.poftut.com/openssl-shell-commands-tutorial-examples/) 

- [link 3](https://access.redhat.com/documentation/en-us/red_hat_directory_server/9.0/html/administration_guide/ldap-clients-ssl)

- [linke 4: proof of why cacerts are necessary](https://serverfault.com/questions/62496/ssl-certificate-location-on-unix-linux) 



## LDAP002 CACERT & CA-Signed-CSR: 09-26-19 

	hostname: ldap002.biohpc.swmed.edu

### Generate New CA

```
cd /usr/local/etc/openldap/2019

# KEY

openssl genrsa -out ldap002_rootCA.key 2048

[root@ldap002 2019]# openssl genrsa -out ldap002_rootCA.key 2048
Generating RSA private key, 2048 bit long modulus
................+++
.......................+++
e is 65537 (0x10001)

# SELF-SIGNED CA CERT
  
  ## does `.crt` or `.pem` extension matter?

openssl req -x509 -new -nodes -key ldap002_rootCA.key -sha256 -days 1825 -out ldap002_rootCA.crt

[root@ldap002 2019]# openssl req -x509 -new -nodes -key ldap002_rootCA.key -sha256 -days 1825 -out ldap002_rootCA.crt
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:US
State or Province Name (full name) []:Texas
Locality Name (eg, city) [Default City]:Dallas
Organization Name (eg, company) [Default Company Ltd]:UT Southwestern Medical Center at Dallas
Organizational Unit Name (eg, section) []:BioHPC
Common Name (eg, your name or your server's hostname) []: LDAP002 Root CA
Email Address []:biohpc-help@utsouthwestern.edu 

[root@ldap002 2019]# ll
total 8
-rw-r--r--. 1 root root 1549 Sep 26 14:09 ldap002_rootCA.crt
-rw-r--r--. 1 root root 1679 Sep 26 14:07 ldap002_rootCA.key

[root@ldap002 2019]# cat ldap002_rootCA.crt
-----BEGIN CERTIFICATE-----
MIIESzCCAzOgAwIBAgIJAOpVQgFbC3rXMA0GCSqGSIb3DQEBCwUAMIG7MQswCQYD
VQQGEwJVUzEOMAwGA1UECAwFVGV4YXMxDzANBgNVBAcMBkRhbGxhczExMC8GA1UE
CgwoVVQgU291dGh3ZXN0ZXJuIE1lZGljYWwgQ2VudGVyIGF0IERhbGxhczEPMA0G
A1UECwwGQmlvSFBDMRgwFgYDVQQDDA9MREFQMDAyIFJvb3QgQ0ExLTArBgkqhkiG
9w0BCQEWHmJpb2hwYy1oZWxwQHV0c291dGh3ZXN0ZXJuLmVkdTAeFw0xOTA5MjYx
OTA5MjlaFw0yNDA5MjQxOTA5MjlaMIG7MQswCQYDVQQGEwJVUzEOMAwGA1UECAwF
VGV4YXMxDzANBgNVBAcMBkRhbGxhczExMC8GA1UECgwoVVQgU291dGh3ZXN0ZXJu
IE1lZGljYWwgQ2VudGVyIGF0IERhbGxhczEPMA0GA1UECwwGQmlvSFBDMRgwFgYD
VQQDDA9MREFQMDAyIFJvb3QgQ0ExLTArBgkqhkiG9w0BCQEWHmJpb2hwYy1oZWxw
QHV0c291dGh3ZXN0ZXJuLmVkdTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC
ggEBAMKcNs/FeqtXERGx0u62z1DzHpa/Z7T53+BtuuN7LpucXfxgYgM7/Dzi+VpP
vleE0NiqsyEyDu/XOTPzOA+FHDXksjza3QMqO8YmBPC6OQDhM97Wle1OxR2QC9/l
d8kA63hqWecQ5+J/5vKA/KbNa84S+6eJJGBLWP/UPagG05PJxgcoCDEpcUGypX5N
+gqVYi/dS6WlUWAZH2iVqCRDFV4oG+v5r5rd3MWv0Byxx6YH6doaUl+3xZccdPhe
/cL17AArPlj5HpKND15oLnm1n0V0Dq+Vd9qTjzzasHaydd/ehlAl3eAibjt51Bt7
S+RC5MYHJalvkGm1w3eVAe3eOjECAwEAAaNQME4wHQYDVR0OBBYEFHcG7qONZxeL
DnXVGdh/UbGQEr4KMB8GA1UdIwQYMBaAFHcG7qONZxeLDnXVGdh/UbGQEr4KMAwG
A1UdEwQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBAH1jnXcYgYvs3a4S2NXWvlA7
2eS5K7MWKJRUwidCECdO8TiQSRAfL2T4oRucqeT56oRmEo7AthwU0sjv+2j4hADf
SIxwwdypVv/zkg6/LwkHgtmtlYkEDES4dslYJd6EyKGHGOaZ7Zf6XQDM5mR1z4AG
DL6rfXcVn2agnHiemUUoHF7WVUK8r565X+I5muZw3/IaXPjOtQnt3jxcZs9C8sm1
FV4IUoLLXPW6Yd4WXOLGXyKIDq242txRDBib8Ko+i0RDDyo/nYSl72zK92y05Wd7
sO4qaAXRYsupHf0250tNjjbguMtjPe8KahX7FpcKd9wcCn8TlRC1IgNHkQcE1Fc=
-----END CERTIFICATE-----
[root@ldap002 2019]# 
[root@ldap002 2019]# 
[root@ldap002 2019]# 
[root@ldap002 2019]# openssl x509 -in ldap002_rootCA.crt -text
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number: 16885474951485422295 (0xea5542015b0b7ad7)
    Signature Algorithm: sha256WithRSAEncryption
        Issuer: C=US, ST=Texas, L=Dallas, O=UT Southwestern Medical Center at Dallas, OU=BioHPC, CN=LDAP002 Root CA/emailAddress=biohpc-help@utsouthwestern.edu
        Validity
            Not Before: Sep 26 19:09:29 2019 GMT
            Not After : Sep 24 19:09:29 2024 GMT
        Subject: C=US, ST=Texas, L=Dallas, O=UT Southwestern Medical Center at Dallas, OU=BioHPC, CN=LDAP002 Root CA/emailAddress=biohpc-help@utsouthwestern.edu
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:c2:9c:36:cf:c5:7a:ab:57:11:11:b1:d2:ee:b6:
                    cf:50:f3:1e:96:bf:67:b4:f9:df:e0:6d:ba:e3:7b:
                    2e:9b:9c:5d:fc:60:62:03:3b:fc:3c:e2:f9:5a:4f:
                    be:57:84:d0:d8:aa:b3:21:32:0e:ef:d7:39:33:f3:
                    38:0f:85:1c:35:e4:b2:3c:da:dd:03:2a:3b:c6:26:
                    04:f0:ba:39:00:e1:33:de:d6:95:ed:4e:c5:1d:90:
                    0b:df:e5:77:c9:00:eb:78:6a:59:e7:10:e7:e2:7f:
                    e6:f2:80:fc:a6:cd:6b:ce:12:fb:a7:89:24:60:4b:
                    58:ff:d4:3d:a8:06:d3:93:c9:c6:07:28:08:31:29:
                    71:41:b2:a5:7e:4d:fa:0a:95:62:2f:dd:4b:a5:a5:
                    51:60:19:1f:68:95:a8:24:43:15:5e:28:1b:eb:f9:
                    af:9a:dd:dc:c5:af:d0:1c:b1:c7:a6:07:e9:da:1a:
                    52:5f:b7:c5:97:1c:74:f8:5e:fd:c2:f5:ec:00:2b:
                    3e:58:f9:1e:92:8d:0f:5e:68:2e:79:b5:9f:45:74:
                    0e:af:95:77:da:93:8f:3c:da:b0:76:b2:75:df:de:
                    86:50:25:dd:e0:22:6e:3b:79:d4:1b:7b:4b:e4:42:
                    e4:c6:07:25:a9:6f:90:69:b5:c3:77:95:01:ed:de:
                    3a:31
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Subject Key Identifier: 
                77:06:EE:A3:8D:67:17:8B:0E:75:D5:19:D8:7F:51:B1:90:12:BE:0A
            X509v3 Authority Key Identifier: 
                keyid:77:06:EE:A3:8D:67:17:8B:0E:75:D5:19:D8:7F:51:B1:90:12:BE:0A

            X509v3 Basic Constraints: 
                CA:TRUE
    Signature Algorithm: sha256WithRSAEncryption
         7d:63:9d:77:18:81:8b:ec:dd:ae:12:d8:d5:d6:be:50:3b:d9:
         e4:b9:2b:b3:16:28:94:54:c2:27:42:10:27:4e:f1:38:90:49:
         10:1f:2f:64:f8:a1:1b:9c:a9:e4:f9:ea:84:66:12:8e:c0:b6:
         1c:14:d2:c8:ef:fb:68:f8:84:00:df:48:8c:70:c1:dc:a9:56:
         ff:f3:92:0e:bf:2f:09:07:82:d9:ad:95:89:04:0c:44:b8:76:
         c9:58:25:de:84:c8:a1:87:18:e6:99:ed:97:fa:5d:00:cc:e6:
         64:75:cf:80:06:0c:be:ab:7d:77:15:9f:66:a0:9c:78:9e:99:
         45:28:1c:5e:d6:55:42:bc:af:9e:b9:5f:e2:39:9a:e6:70:df:
         f2:1a:5c:f8:ce:b5:09:ed:de:3c:5c:66:cf:42:f2:c9:b5:15:
         5e:08:52:82:cb:5c:f5:ba:61:de:16:5c:e2:c6:5f:22:88:0e:
         ad:b8:da:dc:51:0c:18:9b:f0:aa:3e:8b:44:43:0f:2a:3f:9d:
         84:a5:ef:6c:ca:f7:6c:b4:e5:67:7b:b0:ee:2a:68:05:d1:62:
         cb:a9:1d:fd:36:e7:4b:4d:8e:36:e0:b8:cb:63:3d:ef:0a:6a:
         15:fb:16:97:0a:77:dc:1c:0a:7f:13:95:10:b5:22:03:47:91:
         07:04:d4:57
-----BEGIN CERTIFICATE-----
MIIESzCCAzOgAwIBAgIJAOpVQgFbC3rXMA0GCSqGSIb3DQEBCwUAMIG7MQswCQYD
VQQGEwJVUzEOMAwGA1UECAwFVGV4YXMxDzANBgNVBAcMBkRhbGxhczExMC8GA1UE
CgwoVVQgU291dGh3ZXN0ZXJuIE1lZGljYWwgQ2VudGVyIGF0IERhbGxhczEPMA0G
A1UECwwGQmlvSFBDMRgwFgYDVQQDDA9MREFQMDAyIFJvb3QgQ0ExLTArBgkqhkiG
9w0BCQEWHmJpb2hwYy1oZWxwQHV0c291dGh3ZXN0ZXJuLmVkdTAeFw0xOTA5MjYx
OTA5MjlaFw0yNDA5MjQxOTA5MjlaMIG7MQswCQYDVQQGEwJVUzEOMAwGA1UECAwF
VGV4YXMxDzANBgNVBAcMBkRhbGxhczExMC8GA1UECgwoVVQgU291dGh3ZXN0ZXJu
IE1lZGljYWwgQ2VudGVyIGF0IERhbGxhczEPMA0GA1UECwwGQmlvSFBDMRgwFgYD
VQQDDA9MREFQMDAyIFJvb3QgQ0ExLTArBgkqhkiG9w0BCQEWHmJpb2hwYy1oZWxw
QHV0c291dGh3ZXN0ZXJuLmVkdTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC
ggEBAMKcNs/FeqtXERGx0u62z1DzHpa/Z7T53+BtuuN7LpucXfxgYgM7/Dzi+VpP
vleE0NiqsyEyDu/XOTPzOA+FHDXksjza3QMqO8YmBPC6OQDhM97Wle1OxR2QC9/l
d8kA63hqWecQ5+J/5vKA/KbNa84S+6eJJGBLWP/UPagG05PJxgcoCDEpcUGypX5N
+gqVYi/dS6WlUWAZH2iVqCRDFV4oG+v5r5rd3MWv0Byxx6YH6doaUl+3xZccdPhe
/cL17AArPlj5HpKND15oLnm1n0V0Dq+Vd9qTjzzasHaydd/ehlAl3eAibjt51Bt7
S+RC5MYHJalvkGm1w3eVAe3eOjECAwEAAaNQME4wHQYDVR0OBBYEFHcG7qONZxeL
DnXVGdh/UbGQEr4KMB8GA1UdIwQYMBaAFHcG7qONZxeLDnXVGdh/UbGQEr4KMAwG
A1UdEwQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBAH1jnXcYgYvs3a4S2NXWvlA7
2eS5K7MWKJRUwidCECdO8TiQSRAfL2T4oRucqeT56oRmEo7AthwU0sjv+2j4hADf
SIxwwdypVv/zkg6/LwkHgtmtlYkEDES4dslYJd6EyKGHGOaZ7Zf6XQDM5mR1z4AG
DL6rfXcVn2agnHiemUUoHF7WVUK8r565X+I5muZw3/IaXPjOtQnt3jxcZs9C8sm1
FV4IUoLLXPW6Yd4WXOLGXyKIDq242txRDBib8Ko+i0RDDyo/nYSl72zK92y05Wd7
sO4qaAXRYsupHf0250tNjjbguMtjPe8KahX7FpcKd9wcCn8TlRC1IgNHkQcE1Fc=
-----END CERTIFICATE-----


# SERVER PRIVATE KEY

openssl genrsa -out ldap002_prvt.key 2048 

[root@ldap002 2019]# openssl genrsa -out ldap002_prvt.key 2048
Generating RSA private key, 2048 bit long modulus
.......................................+++
............................................................................................................................+++
e is 65537 (0x10001)


# SERVER SIGNING REQUEST

openssl req -new -key ldap002_prvt.key -out ldap002.csr

[root@ldap002 2019]# openssl req -new -key ldap002_prvt.key -out ldap002.csr
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:US
State or Province Name (full name) []:Texas
Locality Name (eg, city) [Default City]:Dallas
Organization Name (eg, company) [Default Company Ltd]:UT Southwestern Medical Center at Dallas
Organizational Unit Name (eg, section) []:BioHPC
Common Name (eg, your name or your server's hostname) []:ldap002.biohpc.swmed.edu
Email Address []:biohpc-help@utsouthwestern.edu

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:Authentic    
An optional company name []:


# SIGN SIGNING REQUEST WITH CACERT

openssl x509 -req -in ldap002.csr -CA ldap002_rootCA.crt -CAkey ldap002_rootCA.key -CAcreateserial -out ldap002.crt -days 365 -sha256

[root@ldap002 2019]# openssl x509 -req -in ldap002.csr -CA ldap002_rootCA.crt -CAkey ldap002_rootCA.key -CAcreateserial -out ldap002.crt -days 365 -sha256
Signature ok
subject=/C=US/ST=Texas/L=Dallas/O=UT Southwestern Medical Center at Dallas/OU=BioHPC/CN=ldap002.biohpc.swmed.edu/emailAddress=biohpc-help@utsouthwestern.edu
Getting CA Private Key

  ## this doesn't register new .crt to the database & no serial number is assigned to the new ca-signed certificate...try another command

# SIGN SIGNING REQUEST WITH CACERT--2nd ATTEMPT

openssl ca -keyfile ldap002_rootCA.key -cert ldap002_rootCA.crt -out ldap002.crt -days 365 -infiles ldap002.csr

[root@ldap002 2019]# openssl ca -keyfile ldap002_rootCA.key -cert ldap002_rootCA.crt -out ldap002.crt -days 365 -infiles ldap002.csrUsing configuration from /etc/pki/tls/openssl.cnf
Check that the request matches the signature
Signature ok
Certificate Details:
        Serial Number: 4100 (0x1004)
        Validity
            Not Before: Sep 26 19:40:29 2019 GMT
            Not After : Sep 25 19:40:29 2020 GMT
        Subject:
            countryName               = US
            stateOrProvinceName       = Texas
            organizationName          = UT Southwestern Medical Center at Dallas
            organizationalUnitName    = BioHPC
            commonName                = ldap002.biohpc.swmed.edu
            emailAddress              = biohpc-help@utsouthwestern.edu
        X509v3 extensions:
            X509v3 Basic Constraints: 
                CA:FALSE
            Netscape Comment: 
                OpenSSL Generated Certificate
            X509v3 Subject Key Identifier: 
                7F:71:AB:88:D7:72:E5:EC:FF:D4:2B:48:E0:CB:FD:7F:E7:6D:56:79
            X509v3 Authority Key Identifier: 
                keyid:77:06:EE:A3:8D:67:17:8B:0E:75:D5:19:D8:7F:51:B1:90:12:BE:0A

Certificate is to be certified until Sep 25 19:40:29 2020 GMT (365 days)
Sign the certificate? [y/n]:y


1 out of 1 certificate requests certified, commit? [y/n]y
Write out database with 1 new entries
Data Base Updated

[root@ldap002 2019]# ll
total 24
-rw-r--r--. 1 root root 4869 Sep 26 14:43 ldap002.crt
-rw-r--r--. 1 root root 1167 Sep 26 14:23 ldap002.csr
-rw-r--r--. 1 root root 1675 Sep 26 14:18 ldap002_prvt.key
-rw-r--r--. 1 root root 1549 Sep 26 14:09 ldap002_rootCA.crt
-rw-r--r--. 1 root root 1679 Sep 26 14:07 ldap002_rootCA.key


# CHECK INDEX AND CA LOCATION 

cat /etc/pki/CA/index.txt

V	280924230952Z		1000	unknown	/C=US/ST=TX/O=UTSW/OU=BIOHPC/CN=BIOHPC/emailAddress=BIOHPC-HELP@UTSOUTHWESTERN.EDU
V	190927231521Z		1001	unknown	/C=US/ST=TX/L=DALLAS/O=UTSW/OU=BIOHPC/CN=BIOHPC/emailAddress=BIOHPC-HELP@UTSOUTHWESTERN.EDU
V	190927232204Z		1002	unknown	/C=US/ST=TEXAS/L=DALLAS/O=UTSW/OU=BIOHPC/CN=BIOHPC/emailAddress=BIOHPC-HELP@UTSOUTHWESTERN.EDU
V	200904174100Z		1003	unknown	/C=US/ST=TEXAS/L=DALLAS/O=BIOHPC/OU=BIOHPC/CN=BIOHPC/emailAddress=BIOHPC-HELP@UTSOUTHWESTERN.EDU
V	200925194029Z		1004	unknown	/C=US/ST=Texas/O=UT Southwestern Medical Center at Dallas/OU=BioHPC/CN=ldap002.biohpc.swmed.edu/emailAddress=biohpc-help@utsouthwestern.edu   # new entry 

[root@ldap002 2019]# ll /etc/pki/CA/newcerts/
total 40
-rw-r--r--. 1 root root 4453 Sep 27  2018 1000.pem
-rw-r--r--. 1 root root 4622 Sep 27  2018 1001.pem
-rw-r--r--. 1 root root 4629 Sep 27  2018 1002.pem
-rw-r--r--. 1 root root 4635 Sep  5 12:41 1003.pem
-rw-r--r--. 1 root root 4869 Sep 26 14:43 1004.pem   # new entry

[root@ldap002 2019]# cat /etc/pki/CA/newcerts/1004.pem 
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number: 4100 (0x1004)
    Signature Algorithm: sha1WithRSAEncryption
        Issuer: C=US, ST=Texas, L=Dallas, O=UT Southwestern Medical Center at Dallas, OU=BioHPC, CN=LDAP002 Root CA/emailAddress=biohpc-help@utsouthwestern.edu
        Validity
            Not Before: Sep 26 19:40:29 2019 GMT
            Not After : Sep 25 19:40:29 2020 GMT
        Subject: C=US, ST=Texas, O=UT Southwestern Medical Center at Dallas, OU=BioHPC, CN=ldap002.biohpc.swmed.edu/emailAddress=biohpc-help@utsouthwestern.edu
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:cc:e9:27:d7:bf:c7:9d:1f:76:9c:e6:f7:27:d8:
                    e6:cf:ec:fa:a8:57:67:27:c5:85:93:04:06:a0:79:
                    de:42:69:3a:e7:ae:f8:5e:b1:76:c7:e2:ac:29:2e:
                    2e:18:da:79:62:eb:4d:b9:de:e6:c5:98:6f:62:19:
                    08:ac:1e:79:00:ce:f7:42:37:d3:e2:ad:ed:ac:a2:
                    84:70:c1:88:e0:36:46:c8:77:2b:2d:ed:49:b3:20:
                    c6:2f:cd:09:5c:4f:f1:bf:ca:eb:4a:22:3a:86:49:
                    a6:24:48:e1:f8:6a:30:5e:f7:57:e8:13:23:2d:1c:
                    a5:21:eb:f0:d0:f0:76:48:52:7d:2f:de:72:1d:95:
                    22:ab:1b:7a:ca:49:88:eb:6f:57:eb:04:0f:c0:98:
                    23:71:df:a8:45:30:74:d9:6a:5a:83:31:82:eb:50:
                    01:30:af:d1:62:34:3f:1d:15:0c:27:67:72:30:3e:
                    75:72:30:d5:ac:bf:40:f1:a6:a7:17:47:6d:6e:fe:
                    ae:3d:59:77:3a:03:d9:8e:34:3b:d7:9b:18:2f:c6:
                    d9:de:f4:2c:ee:be:89:0b:3d:a6:90:e8:7b:97:16:
                    ce:7b:90:64:b4:60:30:f1:cf:f8:4e:1a:10:67:13:
                    6b:90:3f:44:5a:e6:9c:ec:1c:b5:e4:66:69:30:c8:
                    21:91
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Basic Constraints: 
                CA:FALSE
            Netscape Comment: 
                OpenSSL Generated Certificate
            X509v3 Subject Key Identifier: 
                7F:71:AB:88:D7:72:E5:EC:FF:D4:2B:48:E0:CB:FD:7F:E7:6D:56:79
            X509v3 Authority Key Identifier: 
                keyid:77:06:EE:A3:8D:67:17:8B:0E:75:D5:19:D8:7F:51:B1:90:12:BE:0A

    Signature Algorithm: sha1WithRSAEncryption
         25:61:ce:d9:72:82:36:22:a3:70:29:fa:f6:d5:9c:b0:b1:f2:
         08:87:37:93:dd:fe:9d:8e:dd:8b:d5:56:82:6b:ad:07:2e:b4:
         e3:19:c5:a2:cc:38:67:46:fa:32:d2:91:7a:d0:4a:d4:a4:dc:
         d4:e5:0e:a4:4a:19:94:c9:45:b9:24:09:b0:5d:d1:34:09:f5:
         9b:e4:41:41:1c:dc:07:fb:a0:79:4b:31:6a:c2:8f:c7:a9:e0:
         e5:4a:da:54:c1:40:84:23:b8:9a:18:bf:e2:2a:56:6e:36:1a:
         bb:d3:fc:79:33:79:af:aa:2f:ee:38:a5:09:46:9e:c6:5b:b5:
         b1:46:ae:89:65:a3:a6:09:dd:40:56:00:5c:d4:77:dd:c3:6d:
         e9:67:ed:3b:ec:a4:22:dc:bf:07:be:3d:d3:9c:2a:77:87:36:
         06:88:56:0c:41:7d:b6:a6:47:db:04:8e:ea:1e:93:4c:c3:2a:
         c4:15:b2:d7:dd:b6:90:9a:01:e1:85:4d:6d:8e:80:fb:d9:7e:
         81:d6:1a:0b:a6:ba:dd:1a:87:bf:14:cf:3b:83:e9:34:83:e5:
         9d:3a:60:1f:c6:6d:cb:01:33:ea:76:68:f6:fe:bf:af:09:04:
         64:62:6d:41:50:31:9b:6d:18:75:ac:a7:f0:60:46:49:ca:fb:
         ee:8c:21:44
-----BEGIN CERTIFICATE-----
MIIEZzCCA0+gAwIBAgICEAQwDQYJKoZIhvcNAQEFBQAwgbsxCzAJBgNVBAYTAlVT
MQ4wDAYDVQQIDAVUZXhhczEPMA0GA1UEBwwGRGFsbGFzMTEwLwYDVQQKDChVVCBT
b3V0aHdlc3Rlcm4gTWVkaWNhbCBDZW50ZXIgYXQgRGFsbGFzMQ8wDQYDVQQLDAZC
aW9IUEMxGDAWBgNVBAMMD0xEQVAwMDIgUm9vdCBDQTEtMCsGCSqGSIb3DQEJARYe
YmlvaHBjLWhlbHBAdXRzb3V0aHdlc3Rlcm4uZWR1MB4XDTE5MDkyNjE5NDAyOVoX
DTIwMDkyNTE5NDAyOVowgbMxCzAJBgNVBAYTAlVTMQ4wDAYDVQQIDAVUZXhhczEx
MC8GA1UECgwoVVQgU291dGh3ZXN0ZXJuIE1lZGljYWwgQ2VudGVyIGF0IERhbGxh
czEPMA0GA1UECwwGQmlvSFBDMSEwHwYDVQQDDBhsZGFwMDAyLmJpb2hwYy5zd21l
ZC5lZHUxLTArBgkqhkiG9w0BCQEWHmJpb2hwYy1oZWxwQHV0c291dGh3ZXN0ZXJu
LmVkdTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMzpJ9e/x50fdpzm
9yfY5s/s+qhXZyfFhZMEBqB53kJpOueu+F6xdsfirCkuLhjaeWLrTbne5sWYb2IZ
CKweeQDO90I30+Kt7ayihHDBiOA2Rsh3Ky3tSbMgxi/NCVxP8b/K60oiOoZJpiRI
4fhqMF73V+gTIy0cpSHr8NDwdkhSfS/ech2VIqsbespJiOtvV+sED8CYI3HfqEUw
dNlqWoMxgutQATCv0WI0Px0VDCdncjA+dXIw1ay/QPGmpxdHbW7+rj1ZdzoD2Y40
O9ebGC/G2d70LO6+iQs9ppDoe5cWznuQZLRgMPHP+E4aEGcTa5A/RFrmnOwcteRm
aTDIIZECAwEAAaN7MHkwCQYDVR0TBAIwADAsBglghkgBhvhCAQ0EHxYdT3BlblNT
TCBHZW5lcmF0ZWQgQ2VydGlmaWNhdGUwHQYDVR0OBBYEFH9xq4jXcuXs/9QrSODL
/X/nbVZ5MB8GA1UdIwQYMBaAFHcG7qONZxeLDnXVGdh/UbGQEr4KMA0GCSqGSIb3
DQEBBQUAA4IBAQAlYc7ZcoI2IqNwKfr21ZywsfIIhzeT3f6djt2L1VaCa60HLrTj
GcWizDhnRvoy0pF60ErUpNzU5Q6kShmUyUW5JAmwXdE0CfWb5EFBHNwH+6B5SzFq
wo/HqeDlStpUwUCEI7iaGL/iKlZuNhq70/x5M3mvqi/uOKUJRp7GW7WxRq6JZaOm
Cd1AVgBc1Hfdw23pZ+077KQi3L8Hvj3TnCp3hzYGiFYMQX22pkfbBI7qHpNMwyrE
FbLX3baQmgHhhU1tjoD72X6B1hoLprrdGoe/FM87g+k0g+WdOmAfxm3LATPqdmj2
/r+vCQRkYm1BUDGbbRh1rKfwYEZJyvvujCFE
-----END CERTIFICATE-----


# check openssl config file if one wants more details

vi /etc/pki/tls/openssl.cnf  --> see config file on lication of certs & index file 


# CHANGE OWNERSHIP TO `SLAPD`

[root@ldap002 2019]# chown slapd:slapd ldap*
[root@ldap002 2019]# ll
total 24
-rw-r--r--. 1 slapd slapd 4869 Sep 26 14:43 ldap002.crt
-rw-r--r--. 1 slapd slapd 1167 Sep 26 14:23 ldap002.csr
-rw-r--r--. 1 slapd slapd 1675 Sep 26 14:18 ldap002_prvt.key
-rw-r--r--. 1 slapd slapd 1549 Sep 26 14:09 ldap002_rootCA.crt
-rw-r--r--. 1 slapd slapd 1679 Sep 26 14:07 ldap002_rootCA.key

```

- [link on "X509v3 Basic Constraints" options](https://stackoverflow.com/questions/5795256/what-is-the-difference-between-the-x-509-v3-extensions-basic-constraints-and-key)

- [openssl ca manpage](https://www.mkssoftware.com/docs/man1/openssl_ca.1.asp)


### LDAP002 Server-side Ops 

- Workstations: added new CACert to all puppet environments

- VMs: added new CACert to following servers that bind to ldap002

```
flash-dmz
astrocyte7-production
astrocyte-test-rhel7
backup001
thunder
clarityAI
cpfp
foreman
galaxy-rhel7-test
NGS_rhel7
rstudio7
singularity-buid

```

- ops on server:

```
[biohpcadmin@ldap002 2019]$ vi /usr/local/etc/openldap/slapd.conf

------------------------------------------------------------------------
TLSCACertificateFile /usr/local/etc/openldap/2019/ldap002_rootCA.crt
TLSCertificateFile /usr/local/etc/openldap/2019/ldap002.crt
TLSCertificateKeyFile /usr/local/etc/openldap/2019/ldap002_prvt.key
------------------------------------------------------------------------

[root@ldap002 2019]# slaptest -u
5d8d3c7c syncrepl rid=123 searchbase="dc=biohpc,dc=swmed,dc=edu": no retry defined, using default
config file testing succeeded

[root@ldap002 2019]# service slapd restart
Shutting down ldap:                                        [  OK  ]
Starting ldap:                                             [  OK  ]

[root@ldap002 2019]# service slapd status
slapd (pid 8017) is running...

```

- Verification: 

  - restarted xymon to get ldaps status on all WS's & TC's 

  - used ansible to execute `id zpang1` to check whether clients can connec to ldap server



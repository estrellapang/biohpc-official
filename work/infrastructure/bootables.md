## Bootable Drives

### Resources

- [UNetbootin](https://unetbootin.github.io/#faq)

- [common utilities](https://beebom.com/best-rufus-alternatives-windows-linux-macos/)

- [manually bootable drive on Linux](https://itsfoss.com/bootable-windows-usb-linux/)

  - make sure partition is bootable via `fdisk` or system's DISK utility

- [make bootables on MAC](https://www.wdiaz.org/how-to-create-a-bootable-windows-usb/)


### Target Host Tips

- **KEYS**

 - BIOS: `f1`, `Enter`, `Del`

 - System Setup: `f2` (could also be BIOS`)

 - Boot Device: `f12`

- **More Tips** 

- Disable Secureboot

- Windows Hosts May Require Using Command Prompt to clear GPT partition table

\
\
\

### Common Utilities

#### Via `dd` CLI tool: 

- **DOESN'T WORK WELL OUTSIDE OF LINUX DISTROS!**

- Single boot drive

```
dd if=[directory of iso file] of=[boot device] bs=512k 

  ### DO NOT SPECIFY PARTITION!

```
\
\
\

#### Via UNetbootin

- **GENERATES ONLY LINUX BOOT DRIVES**

\
\
\

#### Rufus 

- **BEST for WINDOWS BOOT DRIVES**

  - MBR + ExFAT tend to work better

- **DEVELOPMENT IDEA**

  - run Win10 VM, open USB connection, and run Rufus to create win boot drives
\
\
\

#### Using `fdisk` to check partition status

`fdisk /dev/<device>` 

`p` --> show partition

`a` --> select partition's boot label (marked by *) 

  - `p` to double check again 

`q` --> quit

- e.g.

```
 p

Disk /dev/sdc: 30.8 GB, 30752636928 bytes, 60063744 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk label type: dos
Disk identifier: 0x48cd6d04

   Device Boot      Start         End      Blocks   Id  System
/dev/sdc1   *           0    20533247    10266624    0  Empty
/dev/sdc2            6192       23875        8842   ef  EFI (FAT-12/16/32)


Command (m for help): a 
Partition number (1,2, default 2): 2

p

Disk /dev/sdc: 30.8 GB, 30752636928 bytes, 60063744 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk label type: dos
Disk identifier: 0x48cd6d04

   Device Boot      Start         End      Blocks   Id  System
/dev/sdc1   *           0    20533247    10266624    0  Empty
/dev/sdc2   *        6192       23875        8842   ef  EFI (FAT-12/16/32)

wq
The partition table has been altered!

Calling ioctl() to re-read partition table.
Syncing disks.

```



## Troubleshooting/Stats on Cluster Modules

### Module System Commands

- module load/add

- module list

- module avail <>

  - e.g. module avail python

```

```

\
\

### Overview

- cluster's `/cm/shared/apps` is NOT the same as WSs' `/cm/shared/apps`

  - on WSs' it's symlinked to `/project/apps/..` 

![alt text](./imageEmbed/module_namespace_01.PNG)

![alt_text](./imageEmbed/module_namespace_02.PNG)

\

- ![workstations' module locations](./imageEmbed/module_workstation_locations.png)

- /home1 is read-only (for software stack)

  - we can make it active-active vs. active-backup, bc readonly will not require a write lock from users' requests

\
\

### Easy-build

- [BioHPC link](https://docs.biohpc.swmed.edu/dokuwiki/doku.php?id=easybuild)


### Permission on Module Log Files 

> Ticket#2019111410008081

- Issue: `/home2/modulestats/host_logs/biohpcwsc045.log: Permission denied` 

- Fix: `chmod 766 <module log file>` 

\
\

### Modules General:

```
# show installation location of all installed modules
-----------------------------------------------------
echo $LD_LIBRARY_PATH
  e.g. of output:
/cm/shared/apps/slurm/16.05.8/lib64/slurm:/cm/shared/apps/slurm/16.05.8/lib64
-----------------------------------------------------

``` 

\
\
\

### Module Stat/Usage Tracking

#### Script Explained

- find screenshot details in ./ImageEmbed

- the module_stat.sh file is run across every node

  - executed on /etc/profile.d/

- saves info on a directory in home2/

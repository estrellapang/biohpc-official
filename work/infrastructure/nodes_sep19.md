## Troubleshooting with GPU/COMPUTE nodes: September 2019 

### Ticket#2019091910008101 

- incident: NucleusA250, dedicated node for Albert M. & Satwik crashed

- diagnosis: via `top` it was found that `abrtd` was using up lots of CPU resources 

- solution: `systemctl restart abrtd` 

  - check again: `top` --> press "o" to filter --> enter "COMMAND=abrtd" 
[top link](https://www.binarytides.com/linux-top-command/)

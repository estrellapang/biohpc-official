## WS/TC Update General Tips

### Required Utility Tool

```
------------------------------
yum-utils
------------------------------
```


### Tools 

```
# see all repos regard
-------------------------------
yum repolist all 
-------------------------------

-------------------------------
yum history
-------------------------------

-------------------------------
yum history <info> 
-------------------------------

-------------------------------
yum history undo <info>  
-------------------------------

-------------------------------
ps aux | grep -i 'yum'
-------------------------------

# cleanup incomplete transactions
-------------------------------
yum-complete-transaction --cleanup-only
-------------------------------

# remove orphaned packages---(`package-cleanup --leaves` alone lists them)
-------------------------------
yum remove `package-cleanup --leaves`
-------------------------------

# list duplicate packages, sort out NEWER of the duplicate pairs
--------------------------------------------------------------
package-cleanup --dupes | sort -u |  grep -v 'Loaded plugins:' | sort -u | awk 'NR % 2 == 1'
--------------------------------------------------------------

# list duplicate packages, sort out OLDER of the duplicate pairs
--------------------------------------------------------------
package-cleanup --dupes | sort -u |  grep -v 'Loaded plugins:' | sort -u | awk 'NR % 2 == 0'
--------------------------------------------------------------

# DOUBLE-VERIFY via `diff` command: 
--------------------------------------------------------------
# the bash pipe above will 5% of the time fail to correct sort the old from the new, vice versa
# one must double verify by checking the difference & correct any mistakes

diff -y -W 95 <raw sorted list> <refine sorted list>

# show ONLY changes:

diff -y -W <width of choice> <new file> <file> | grep -i '>\||'  

  ## ">" "|" denote additions and appends, respectively
--------------------------------------------------------------

# remove from RPM DATABASE the duplicate files (need a refined, sorted list first!)
----------------------------------------------------------------------
for i in `cat <package list>`; do rpm -e --nodeps -f --justdb $i; done

# VERIFY DELETION!
package-cleanup --dupes
----------------------------------------------------------------------

# show security updates
----------------------------------------------------------------------
yum --security check-update

yum --security list updates
----------------------------------------------------------------------

# install minimal-security updates
----------------------------------------------------------------------
yum --security update-minimal
----------------------------------------------------------------------

# yum version lock 
----------------------------------------------------------------------
[RHEL useful link here](https://access.redhat.com/solutions/98873)

yum install yum-plugin-versionlock.noarch

yum versionlock kmod-kvdo-6.1.1.125-5.el7.x86_64

yum versionlock list

yum versionlock clear

----------------------------------------------------------------------

# yum downgrade
----------------------------------------------------------------------
[link](https://www.shellhacks.com/yum-install-specific-version-of-package/)

# combine with `--showduplicates list` option to fetch precise package version names
----------------------------------------------------------------------

\
\
\

# Download RPM packages
----------------------------------------------------------------------
yum install yum-utils

yumdownloader <package/rpm name>

  # [rhel solution link](https://access.redhat.com/solutions/10154)

  # how to download all of the dependencies?

yum localinstall <package>
----------------------------------------------------------------------

# Only Download Packages
----------------------------------------------------------------------
yum-downloadonly <package name> 
----------------------------------------------------------------------

# Show Available Duplicate Packages
----------------------------------------------------------------------
yum --showduplicates list <package name> | expand
----------------------------------------------------------------------

# Remove Yum Cache
----------------------------------------------------------------------
yum clean all
----------------------------------------------------------------------

[tips on manually cleaning yum cache](https://www.thegeekdiary.com/yum-clean-all-not-clearing-yum-repository-cache-in-centos-rhel-oel/)

```



\
\

### Query Installed Packages w/h RPM & YUM

```
# show which package(s) supply the the executable commands
----------------------------------------------------------------------
rpm -qf $(which <CMD>)

  ## e.g.  
---------------------------------
rpm -qf $(which vim)
vim-enhanced-7.4.629-6.el7.x86_64
---------------------------------
----------------------------------------------------------------------

# show all associated files of a package
----------------------------------------------------------------------
rpm -ql <full package name>

# show ONLY CONFIG FILES, "-c" handle; keep in mind that this only shows system-wide configs, and not user-specific customizations

rpm -qlc <pkg name>

  ## e.g.
---------------------------------
rpm -qlc vim-enhanced-7.4.629-6.el7.x86_64
/etc/profile.d/vim.csh
/etc/profile.d/vim.sh
---------------------------------
----------------------------------------------------------------------


# show installed packages dependencies

# show dependencies of package specified, grep only package names in output
----------------------------------------------------------------------
yum deplist <full package name> | grep -i provider
----------------------------------------------------------------------

# show the NAME of the dependencies only
----------------------------------------------------------------------
yum deplist <full package name> | grep -i depend
----------------------------------------------------------------------


- [really good IBM rpm & yum](https://developer.ibm.com/technologies/linux/tutorials/l-lpic1-102-5/)

  - rpm's various uses and flags are described in detail here

```

\
\


### Inspirations

```
[bash pipe to sort out older file versions](https://stackoverflow.com/questions/19418301/duplicate-package-update-upgrade-centos)

[diff command to compare files](https://www.howtogeek.com/410532/how-to-compare-two-text-files-in-the-linux-terminal/)

[awk process every two lines explained](https://stackoverflow.com/questions/11560544/how-to-process-every-other-line-in-bash)

```

\
\

### Common/General Troubleshoot Messages 

--------------------------------------------------------------
There are unfinished transactions remaining. You might consider running yum-complete-transaction, or "yum-complete-transaction --cleanup-only" and "yum history redo last", first to finish them. If those don't work you'll have to try removing/installing packages by hand (maybe package-cleanup can help).
--------------------------------------------------------------

- Issue with nvidia drivers & cuda-drives on WS P520c

  - nvidia-driver-latest-3:440.33.01-1.el7 will "taint the 7.6 kernel" 

  - correct way: `sudo yum install nvidia-driver-latest-3:418.87.00-2.el7.x86_64 cuda-drivers-418.87`


\

- Issue with VMs/VirtualBox Guests "curl#6 - 'Could not resolve host..'" during installs

  - check NAT/Bridged Connection---see if it's up

  - go to ifcfg files and ensure `ONBOOT=yes` 

  - `systemctl restart network`

\
\

### UPCOMING OPERATIONS:

- **delete Chrome/Chromium** on all workstations 


## Dealing with SCSI & HBA in Linux OS

### On the CLI

#### rescan devices across the board:

```
# grab all mapped devices 
fdisk -l 

# rescan lun
for host in $(ls /sys/class/scsi_host/); do echo "- - -" > /sys/class/scsi_host/$host/scan; done

\

# alternative rescan (requires listing out all SCSI channels first)

for i in $(seq 0 8); do echo "- - -" > /sys/class/scsi_host/host$i/scan; done

  # [HOW TO RESCAN for NEW LUNS](https://www.2daygeek.com/scan-detect-luns-scsi-disks-on-redhat-centos-oracle-linux)


# activate LV after expansion
```

- SCSI Addressing Resources

  - [RHEL documentation](https://access.redhat.com/articles/17628)

  - [multiple ways to rescan SCSI bus](https://geekpeek.net/rescan-scsi-bus-on-linux-system/) 

  - [a point on scsi and fc hosts](https://www.linuxquestions.org/questions/linux-server-73/difference-between-fc_host-and-scsi_host-4175592379/)


\
\

### LVM Operations

#### Striping of LVs 

```
# check details, striping of individual LVs
----------------------------
cat /etc/lvm/backup/<lv_name>

  # e.g. 

    cat /etc/lvm/backup/project1
----------------------------
```

\

#### Remove A Physical Disk or PV

- unmount filesystem

- disable LV(s) containing the PV sectors

  - e.g. `lvchange -an /dev/vg_biohpcws001/lv_shared`

  - `lvscan` to confirm

- If data already backed up -OR- data expendable, remove LV

  - e.g. `lvremove /dev/vg_biohpcws001/lv_shared`

- Take the PV in question out of its associated VG

  - e.g. `vgreduce vg_biohpcws001 /dev/sda1`

- Remove PV from LVM entirely

  - e.g. `pvremove /dev/sda1` 

  - confirm by `pvs` or `pvscan`

- For fresh slates, delete existing partitions on disk

  - `fdisk /dev/sd#`

  - `p` to list partitions

  - `d <#>` to delete a partition via its index

  - `w` save changes and quit

  - [how to here](https://www.cyberciti.biz/faq/linux-how-to-delete-a-partition-with-fdisk-command/)

- NOTE: the alternative method may be to reduce LV size, and move all extents from PV to be deleted to its peers (given that there exists enough free space), see "LVM Resources for guides"

\
\

#### LVM Resources

  - [how to remove a logical volume 1](https://www.cyberithub.com/delete-logical-volume-lvm-in-linux/)

  - [how to remove a logical volume 2](https://www.thegeekdiary.com/centos-rhel-how-to-delete-a-volume-group-in-lvm/)

  - [striping logical volumes](https://www.linuxsysadmins.com/create-striped-logical-volume-on-linux/)

  - [remove PV from LVM 01](https://www.thegeekdiary.com/centos-rhel-how-to-remove-used-physical-volumepv-from-volume-group-vg-in-lvm/)

  - [remove PV from LVM 02](https://www.2daygeek.com/linux-remove-delete-physical-volume-pv-from-volume-group-vg-in-lvm/)

  - [remove pv tip, device maps ls and remove tip](https://stackoverflow.com/questions/18042216/how-to-deactivate-a-lvm2-physical-volume-to-remove-the-drive)

\
\

### on Dell Storage Manager

```
# Creating a New Virtual Disk

synchronize using old root

Go to correct storage array (for /project1 --> it's lysosome01)

Storage & Copy Services

"Free Capacity" --> create new VDisk --> "thin provision" --> choose name carefully..! check duplicates --> assign to Nuc005 as mapping 

"no" for last prompt

```


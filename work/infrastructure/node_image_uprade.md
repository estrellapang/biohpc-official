## To-Dos for Re-Provisioining with New Images

###

```
# apply HCA firmware update

 # grab FW version and PSID via `ibv_devinfo`

 # find exisiting firmware packages under `/project/biohpcadmin/shared/Mellanox/Mellanox-network-card-firmwares/`

 # install commands:

--------------------------------------------
# Dell and Mellanox specific

unzip fw-Connect-<>bin.zip
flint -d /dev/mst/<>_pci_cr0 -i fw-XXX.bin b
--------------------------------------------

 # more detailed instructions on Docs[https://docs.biohpc.swmed.edu/dokuwiki/doku.php?id=infinibant_interface_firmware_versions_change_log]


# Install for HPE nodes:
--------------------------------------------
tar zxvf <fw compressed>.tgz

  # OR 

unzip <fw compressed>.zip

lspci | grep -i Mell 

  # grab card bus ID

mstflint -d <bus addr> -i <fw file>.bin burn

# after reboot; check version

mstflint -d <bus addr> q
--------------------------------------------

```

### Additional

```
# have `sshd_config` backedup

# to make WebVis Nodes

  # copy file

/etc/sudoers.d/portal7 

# copy `/etc/X11/xorg.conf` 

  # special note:

-----------------------
NoScanout mode is no longer supported on NVIDIA Tesla products. If NoScanout mode was previously used, then the following line in the “screen” section of /etc/X11/xorg.conf should be removed to ensure that X server starts on Tesla products:

Option         "UseDisplayDevice" "None"

Tesla products now support one display of up to 4K resolution.
-----------------------

# install xymon client

```

### Checks

```
# check adapter FW

# IB BandWidth tests

# Iperf

# DD (maybe) on filesystems

# dmesg | grep -i NVRM 

# kernel messages

# GPFS & lustre client versions

  # GPFS:
----------------------------
/usr/lpp/mmfs/bin/mmdiag --version
----------------------------
```

## Common Topics and Operations in LDAP

> Query LDAP database via sudo on nucleus006 or on WS/TCs

### Searching

- **groups** 

  -`ldapsearch -x cn=<group name>`

  -`ldapsearch -x gidNumber=1001`
  
    - this pulls the entry of everyone who belongs to the group

- **users**

  - `-x sn=<last name>` 

  - `-x givenName=<first name>` 


- **account expiration time**

  - check `shadowExpire` --> convert it into `day/month/year`

  - compare with `shadowLastChange` --> also convert it into `day/month/year`

  - [unix epoch converter](https://www.epochconverter.com/)

- **Get user list** (usernames)

  - `getent passwd | awk -F: '{print $1}'`

\
\
\

### Make New Share Group Under /project/shared -OR- /work/shared/ -OR- /archive/shared


- **ApacheDirectoryStudio**

```
# Create NEW entry under:

`ou=Group,ou=users,dc=biohpc,dc=swmed,dc=edu` 

  # these groups are outside of the ou=user,ou=<department> branch

# ObjectClasses:
----------------------------------------------------------------------------------  
top
posixGroup
sambaGroupMapping
----------------------------------------------------------------------------------  

# Attributes:
----------------------------------------------------------------------------------  
cn (essentially the group name)
gidNumber (check via `getent group <gid>` for available entries)
sambaGroupType = 2
sambaSID = S-1-5-21-270981343-1041670446-3540155302-<unique suffix>
  <unique suffix> = GID * 2 + 1001
memberUid (good to know all members within two primary groups)
----------------------------------------------------------------------------------  

```

- **On Cluster** 

```
# Check if new group is available

# assign appropriate filesystem quota
 
# make shared directory, assign permissions with SetGid bit + sticky bit

  e.g. `sudo chown root:strand_gupta /project/shared/strand_gupta/`
       `sudo chmod 2770 /project/shared/strand_gupta/`
       `sudo chmod +t /project/shared/strand_gupta/` 

# touch file as member and test

```

\
\
\

### Migrate User to New Dept

- example: Ticket#2019123110008117

- On LDAP

  - delete user memberUid from dept and lab group

  - move user entry to new dept

  - add memberUid values for new dept and lab group

  - change home directory location to home2 if user previously was a core user w/o home2 folder

- On Cluster

  - create new /project /work and /home2(if needed) directories

  - transfer existing files and remove old directories

  - double verify lustre, GPFS quotas -- remove or assign

  - log in as user to generate ssh key, touch test files in /project /work and /archive

  
\
\

### Backup Strategies

#### output entire database:

```
slapcat -l <database_bak>.ldif

# e.g.

/usr/local/sbin/slapcat -l /usr/local/etc/openldap/backup/database_bak.ldif
```

\

#### references

- [link 1](https://tylersguides.com/articles/backup-restore-openldap/)

- [link 2](http://genetics.wustl.edu/technology/backing-up-and-restoring-openldap/) 

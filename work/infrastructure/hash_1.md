## Hashes & Password Algorithms in Linux

### 

- **output hashed strings onto command line**

  - `echo -n '<string/password>' | openssl <sha512/256/md5/whirlpool> | awk '//print{ $2}'`

  - useful for putting hashed passwords in scripts i.e. kickstart files


### Links

- [output hashed strings to CLI](https://stackoverflow.com/questions/3358420/generating-a-sha256-from-the-linux-command-line)

- [change password hashing algorithm in RHEL](https://www.thegeekdiary.com/centos-rhel-how-to-change-password-hashing-algorithm/)

- [specifying hash for password in kickstart files](https://thornelabs.net/posts/hash-roots-password-in-rhel-and-centos-kickstart-profiles.html)

- [get SHA hashes for files & packages](https://www.maketecheasier.com/check-sha1-sha256-sha512-hashes-on-linux/)



## Log Watch

### Watch Services via `jouranctl` 

- **resources**

  - [comprehensive guide 1](https://linuxhandbook.com/journalctl-command/)

  


\
\
\

### Login & User Session Management

#### Resources

- [nohup 1](https://stackoverflow.com/questions/285015/how-to-prevent-a-background-process-from-being-stopped-after-closing-ssh-client)

- [nohup 2](https://unix.stackexchange.com/questions/420594/why-process-killed-with-nohup)

- [uptime command](https://www.computerhope.com/unix/uptime.htm)

- [gnome shell restarts 1](https://askubuntu.com/questions/455301/how-to-restart-gnome-shell-after-it-became-unresponsive-freeze/496999)

- [logout users via pkill 1](https://askubuntu.com/questions/12180/logging-out-other-users-from-the-command-line)

- [logout users via pkill 2](https://www.cyberciti.biz/faq/linux-logout-user-howto/)

- [halt user via pkill -STOP](https://www.cyberciti.biz/tips/howto-linux-kill-and-logout-users.html)

- [lengthy KillUserProcesses option in logind.conf](https://github.com/microsoft/vscode-remote-release/issues/1037)

\
\

#### User Sessions

```
# see active user logins & their processes
---------------------------------------------------------
w
---------------------------------------------------------

# see ~ with PIDs
---------------------------------------------------------
who -u
---------------------------------------------------------

# see all PIDS of a user
---------------------------------------------------------
ps -ef | grep <uid>
---------------------------------------------------------


```


### System Runtime

```
# system up time
---------------------------------------------------------
uptime -s	# system up since
--------------------------------------------------------

# system curent time, uptime, current users, & CPU load
--------------------------------------------------------
uptime		# CPU load:  last 1, 5, and 15 minutes
--------------------------------------------------------

```


### TESTS

```
# isolate user processes

1. create separate login as `biohpcadmin`, open some processes e.g. firefox, gedit, etc. 

2. ensure KillUserProcesses is set to `no`

3. HALT user processes, restart gdm

  3.5. Alternative, logout user, restart gdm, log back in

4. resume user, and see if processes are preserved

```

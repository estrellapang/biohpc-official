## DDN Lustre Storage System Overview:

### Overview:

- Lustre Client Version:

  - lustre-client-2.7.21.3.ddn23.gcefc235-3.10.0_957.el7.x86_64_gcefc235.x86_64 (servers)

  - lustre-client-2.7.21.3.ddn16.gb946768-3.10.0_693.2.2.el7.x86_64_gb946768.x86_64 (workstations)

- Network

- LNET

  - interfaces: bond0, ib0 

  - NO LNET Router configured (check entry in `/etc/modprobe.d/lustre.conf`), there'd be a "routes" field if LNET routers are being implemented

  - [link on LNET router](http://wiki.lustre.org/LNet_Router_Config_Guide)

- IB Switch

  - hardware V: Mellanox IB SX6025 

  - firmware V: 9.3.4000

- Storage:

  - Medata RAID10 Array: EF4024


### Future Features

- Central Monitoring:

  - DirectMon, Insight, LustrePerfMon (https://github.com/DDNStorage/LustrePerfMon)

  - IME: flash storage for Metadata targets

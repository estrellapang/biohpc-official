## DDN Lustre Storage Issues and Solutions 

### 07/10/2019 

#### Memory across OSS's 

```
# all OSS servers are routinely running with <1% free memory 

  # could be a result of too many OSTs per OSS (20/server)  

[root@oss01 ~]# free -h
             total       used       free     shared    buffers     cached
Mem:          125G       125G       388M       1.1M       5.8G       9.6G
-/+ buffers/cache:       109G        15G
Swap:          15G        24M        15G


[biohpcadmin@oss00 ~]$ free -h
             total       used       free     shared    buffers     cached
Mem:          125G       124G       1.4G       1.2M        32G        74G
-/+ buffers/cache:        17G       108G
Swap:          15G         0B        15G

[biohpcadmin@oss02 ~]$ free -h
             total       used       free     shared    buffers     cached
Mem:          125G       124G       795M       1.1M       4.9G       7.1G
-/+ buffers/cache:       112G        12G
Swap:          15G        19M        15G


[biohpcadmin@oss03 ~]$ free -h
             total       used       free     shared    buffers     cached
Mem:          125G       125G       656M       1.1M       3.1G       6.4G
-/+ buffers/cache:       115G        10G
Swap:          15G        30M        15G

```

#### Shortage on leftover space 

```
# as of 07/10/2019

10.100.164.11@o2ib:10.100.164.12@o2ib:/project lustre    3.5P  2.5P  956T  73% /project 

```


### 07/11/2019 

- Disk Failure

### `ost_29`

```
  Index:                1355                       ###
  OID:                  0x2562054b
  Pool Index:           UNASSIGNED
  Pool OID:             UNASSIGNED
  Capacity:             5576 GB
  Raw Capacity:         5589 GB(Base 2)/6001 GB(Base 10)
  Block Size:           4K_BYTE_BLOCK
  T10-PI Supported:     TRUE
  T10-PI Enabled:       TRUE
  Format in Progress:   FALSE
  Medium Corrupt:       FALSE
  Available Channels:   32      NA      NA      NA      NA      NA      NA      NA    
                        26      NA      NA      NA      NA      NA      NA      NA    
                        10      NA      NA      NA      NA      NA      NA      NA    
                        12      NA      NA      NA      NA      NA      NA      NA    
  Enabled Path IDs:     0x3260  NA      NA      NA      NA      NA      NA      NA    
                        0xb260  NA      NA      NA      NA      NA      NA      NA    
                        NA      NA      NA      NA      NA      NA      NA      NA    
                        0xa950  NA      NA      NA      NA      NA      NA      NA    
  IOC Index:Port:       16:00   NA:NA   NA:NA   NA:NA   NA:NA   NA:NA   NA:NA   NA:NA 
                        14:00   NA:NA   NA:NA   NA:NA   NA:NA   NA:NA   NA:NA   NA:NA 
                        05:00   NA:NA   NA:NA   NA:NA   NA:NA   NA:NA   NA:NA   NA:NA 
                        06:00   NA:NA   NA:NA   NA:NA   NA:NA   NA:NA   NA:NA   NA:NA 
  Enclosure Position:   1
  Enclosure Index:      1
  Enclosure OID:        0x50000001
  Disk Slot:            30  (1:30)              ### Enclosure 1: Slot 30
  Zone Index:           NA
  Vendor ID:            HGST
  Product ID:           HUS726060AL4210
  Product Revision:     AD05
  Serial Number:        NAHA3AYY
  Health State:         FAILED                  ###
  Rotation Speed:       7200 RPM
  Device Type:          SAS
  Member State:         UNASSIGNED
  Member Index:         4
  State:                READY
  Spare:                FALSE
  Failed:               TRUE
  Quorum:               FALSE
  Power Cycle Feature:  NOT SUPPORTED
  UUID:                 0x5000cca2424abf34
  Interposer Type:      NONE
  Update in Progress:   FALSE
  Current Error Count:  201
  Previous Errors:      
    Operation:          READ
    Status:             JS_AMPD_MEDIUM_ERROR
    Count:              202

    Operation:          READ
    Status:             JS_AMPD_MEDIUM_ERROR
    Count:              208

    Operation:          READ
    Status:             JS_AMPD_SW_ABORTED_CMD
    Count:              193

    Operation:          READ
    Status:             JS_AMPD_MEDIUM_ERROR
    Count:              193

    Operation:          READ
    Status:             JS_AMPD_SW_ABORTED_CMD
    Count:              178
  Self Encrypting Disk: FALSE
  Fault Status:         CRITICAL
  Child Fault Status:   OK

```

###  Summer 2019 **Critical**: The following error has been occuring constantly throughout the month of July 2019---CRITICAL: MDS can't talk to OSTs Properly? 
```
#### oss0

[root@oss00 log]# cat messages-20190804 | grep -i 'lvbo_init failed' | wc -l
10

[root@oss00 log]# cat messages-20190811 | grep -i 'lvbo_init failed' | wc -l
41

[root@oss00 log]# cat messages-20190818 | grep -i 'lvbo_init failed' | wc -l
8

[root@oss00 log]# cat messages-20190825 | grep -i 'lvbo_init failed' | wc -l
12

[root@oss00 biohpcadmin]# cat /var/log/messages-20190901 | grep -i 'lvbo_init failed' | wc -l
2

#### oss1

[biohpcadmin@oss01 log]$ sudo cat messages-20190804 | grep -i 'lvbo_init failed' | wc -l
58

[biohpcadmin@oss01 log]$ sudo cat messages-20190811 | grep -i 'lvbo_init failed' | wc -l
133

[biohpcadmin@oss01 log]$ sudo cat messages-20190818 | grep -i 'lvbo_init failed' | wc -l
111

[biohpcadmin@oss01 log]$ sudo cat messages-20190825 | grep -i 'lvbo_init failed' | wc -l
130

[root@oss01 log]# cat messages-20190901 | grep -i 'lvbo_init failed' | wc -l
26


### oss2

[biohpcadmin@oss02 log]$ sudo cat messages-20190804 | grep -i 'lvbo_init failed' | wc -l
13

[biohpcadmin@oss02 log]$ sudo cat messages-20190811 | grep -i 'lvbo_init failed' | wc -l
61

[biohpcadmin@oss02 log]$ sudo cat messages-20190818 | grep -i 'lvbo_init failed' | wc -l
21

[biohpcadmin@oss02 log]$ sudo cat messages-20190825 | grep -i 'lvbo_init failed' | wc -l
31

[root@oss02 log]# cat messages-20190901 | grep -i 'lvbo_init failed' | wc -l
9


### oss3

[biohpcadmin@oss03 log]$ sudo cat messages-20190804 | grep -i 'lvbo_init failed' | wc -l
20

[biohpcadmin@oss03 log]$ sudo cat messages-20190811 | grep -i 'lvbo_init failed' | wc -l
74

[biohpcadmin@oss03 log]$ sudo cat messages-20190818 | grep -i 'lvbo_init failed' | wc -l
19

[biohpcadmin@oss03 log]$ sudo cat messages-20190825 | grep -i 'lvbo_init failed' | wc -l
36

[root@oss03 log]# cat messages-20190901 | grep -i 'lvbo_init failed' | wc -l
12

```

### 11-11-2019: Lustre /var/log/secure "exploded"


##

###

- location of Django files:

  `/opt/biohpc_portal/media/.` 


### DMZ Portal

- portal dns name resolve

  -  1st method: the portal DNS name will direct to either internal or external ip addr based on proxy server

  -  2nd method: internal and external DNS server based on whether we are in the internal network

- components:

  - gunicorn --> python based fowarder

  - celery --> task scheduler

  - guacamole --> VNC server (alternative --> OpenVNC)

  - CMS plugins (internal portal has most of it)

- To specify which IP can access the admin page, configure it in `/etc/httpd/conf.d/ssl/conf` 

```
<Location /admin>

    Require ip <X.X.X.X>

</Location>
```

## Ansible Operations

### Aug 7th, 2019

> passing a SSH key into all servers

- **set up hosts**

 ```
vi /etc/ansible/hosts

# add test host 

[services]
198.215.54.107  # Strand_lab

```


- **playbook**

```
# make a folder for your playbooks

mkdir /etc/ansible/playbooks



### Test Play Book: add Master's pub key to hosts 

vim /etc/ansible/playbooks/master_key.yaml

-------------------------------------------------------------------------------

- hosts: all

  tasks:
    - name: line insert
      lineinfile:
        path: /root/.ssh/authorized_keys
        line: 'ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBLmp6uRAwfW4+ICZV3QzzbJO7GILrpR5u7AM5sQhr0omV5iGCS7F+zWs1lKUDeX5l7zuPdegFs0ZYZIvtrekDhs= root@master'
        insertbefore: BOF

-------------------------------------------------------------------------------

### execute playbook scripts 

`ansible-playbook -u biohpcadmin --become-user --become-method=su --ask-pass master_key.yaml`


### Output

```

PLAY [all] *********************************************************************************************************************************

TASK [Gathering Facts] *********************************************************************************************************************
ok: [198.215.54.107]

TASK [line insert] *************************************************************************************************************************
fatal: [198.215.54.107]: FAILED! => {"changed": false, "msg": "Destination /root/.ssh/authorized_keys does not exist !", "rc": 257}

PLAY RECAP *********************************************************************************************************************************
198.215.54.107             : ok=1    changed=0    unreachable=0    failed=1    skipped=0    rescued=0    ignored=0  


# COMMENT: not good approach --> may need to write playbook using `echo` --> this approach is more appropriate for adding lines to config files

```


### Experimenting with `command` module 

- .yaml file 

```
- hosts: workstations

  tasks:

   - name: touch file
     command: touch authorized_keys
     args:
       chdir: /root/.ssh
       creates: /root/.ssh/authorized_keys


   - name: echo ssh pub key
     command: /usr/bin/echo 'ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBLmp6uRAwfW4+ICZV3QzzbJO7GILrpR5u7AM5sQhr0omV5iGCS7F+zWs1lKUDeX5l7zuPdegFs0ZYZIvtrekDhs= root@master' >> /root/.ssh/authorized_keys
     args:
       creates: /root/.ssh/authorized_keys
```

- OUTPUTS


```
# without touch 

ansible-playbook -u biohpcadmin --become-user --become-method=su --ask-pass master_echo.yaml
SSH password: 

PLAY [workstations] ************************************************************************************************************************

TASK [Gathering Facts] *********************************************************************************************************************
ok: [198.215.56.37]

TASK [echo ssh pub key] ********************************************************************************************************************
changed: [198.215.56.37]

PLAY RECAP *********************************************************************************************************************************
198.215.56.37              : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   

[root@Nucleus006 playbooks]# 


# with touch

ansible-playbook -u biohpcadmin --become-user --become-method=su --ask-pass master_echo.yaml
SSH password: 

PLAY [workstations] ************************************************************************************************************************

TASK [Gathering Facts] *********************************************************************************************************************
ok: [198.215.56.37]

TASK [touch file] **************************************************************************************************************************
fatal: [198.215.56.37]: FAILED! => {"changed": false, "module_stderr": "Shared connection to 198.215.56.37 closed.\r\n", "module_stdout": "Traceback (most recent call last):\r\n  File \"/home/biohpcadmin/.ansible/tmp/ansible-tmp-1565304099.83-27838312309236/AnsiballZ_command.py\", line 114, in <module>\r\n    _ansiballz_main()\r\n  File \"/home/biohpcadmin/.ansible/tmp/ansible-tmp-1565304099.83-27838312309236/AnsiballZ_command.py\", line 106, in _ansiballz_main\r\n    invoke_module(zipped_mod, temp_path, ANSIBALLZ_PARAMS)\r\n  File \"/home/biohpcadmin/.ansible/tmp/ansible-tmp-1565304099.83-27838312309236/AnsiballZ_command.py\", line 49, in invoke_module\r\n    imp.load_module('__main__', mod, module, MOD_DESC)\r\n  File \"/tmp/ansible_command_payload_0TNPhe/__main__.py\", line 327, in <module>\r\n  File \"/tmp/ansible_command_payload_0TNPhe/__main__.py\", line 263, in main\r\nOSError: [Errno 13] Permission denied: '/root/.ssh'\r\n", "msg": "MODULE FAILURE\nSee stdout/stderr for the exact error", "rc": 1}

PLAY RECAP *********************************************************************************************************************************
198.215.56.37              : ok=1    changed=0    unreachable=0    failed=1    skipped=0    rescued=0    ignored=0   

# TRY SHELL module instead? 

### additioanl errors:

ERROR! Syntax Error while loading YAML.
  found character that cannot start any token

The error appears to be in '/etc/ansible/playbooks/master_echo.yaml': line 8, column 1, but may
be elsewhere in the file depending on the exact syntax problem.

The offending line appears to be:

     args:
	creates: /root/.ssh/authorized_keys
^ here
There appears to be a tab character at the start of the line.

YAML does not use tabs for formatting. Tabs should be replaced with spaces.

For example:
    - name: update tooling
      vars:
        version: 1.2.3
#    ^--- there is a tab there.

Should be written as:
    - name: update tooling
      vars:
        version: 1.2.3
# ^--- all spaces here.

```

[link 1](http://www.mydailytutorials.com/introduction-shell-command-module-ansible/) 

[link 2](https://stackoverflow.com/questions/40009339/ansible-echo-into-file) 

[link 3](https://docs.ansible.com/ansible/latest/modules/command_module.html)

### Extraneous Links ### 

[extra 1](https://willthames.github.io/2016/09/21/using-command-and-shell-in-ansible.html) 

[add to line module](http://www.mydailytutorials.com/ansible-add-line-to-file/)

[copy files b/w hosts](https://www.middlewareinventory.com/blog/how-to-copy-files-between-remote-servers-ansible-fetch-sync/)




stop mysql

stop httpd 

security update 

reboot


198.215.54.107


[root@strandlab ~]# cat /root/.ssh/authorized_keys
cat: /root/.ssh/authorized_keys: No such file or directory


** what has had hope ** 

ansible-playbook -u biohpcadmin -K master_key.yaml



### 09-25-19: PUSH/Copy File to Remote Hosts!!! 

- Resources: 

  - [ansible-playbook man page](https://www.mankier.com/1/ansible-playbook) 

  - [tutorial in-script become root](https://www.middlewareinventory.com/blog/ansible-playbook-introduction-and-examples/) 

  - [tutorial thorough copy task](https://crunchify.com/using-ansible-how-to-copy-files-or-script-from-localhost-to-remote-host/)


- playbook file: Nucleus006: `/etc/ansible/playbooks/push_cert.yml`

  - playbook content

```
[root@Nucleus006 ansible]# cat playbooks/push_cert.yml
---
  - name: Push Cacert
    hosts: testers
    become: yes
    become_user: root
    tasks:
       - name: Copy File to Remote Hosts
         copy: 
           src: /etc/ansible/ansible_fun.md 
           dest: /etc/ssl/certs

```

- command: `# ansible-playbook playbooks/push_cert.yml -b -vvv -u biohpcadmin -kkkk -K -i hosts`

  - `-b` run operation with `become` 

  - `-u` connect as <biohpcadmin> 

  - `-kkkk` ask for connection password

  - `-K` ask for root escalation 

  - `-i` specify where host groups are stored 

  - `-vvv` verbose


- stdout: (content included with `-vvv` not included) 

```
[root@Nucleus006 ansible]# ansible-playbook playbooks/push_cert.yml -b -vvv -u biohpcadmin -kkkk -K -i hosts

SSH password: 
BECOME password[defaults to SSH password]: 

PLAYBOOK: push_cert.yml ********************************************************************************************************************

PLAY [Push Cacert] *************************************************************************************************************************

TASK [Gathering Facts] *********************************************************************************************************************
task path: /etc/ansible/playbooks/push_cert.yml:2

ok: [198.215.56.37]

TASK [Copy File to Remote Hosts] ***********************************************************************************************************
task path: /etc/ansible/playbooks/push_cert.yml:7

changed: [198.215.56.37] => {
    "changed": true, 
    "checksum": "da39a3ee5e6b4b0d3255bfef95601890afd80709", 
    "dest": "/etc/ssl/certs/ansible_fun.md", 
    "diff": [], 
    "gid": 0, 
    "group": "root", 
    "invocation": {
        "module_args": {
            "_original_basename": "ansible_fun.md", 
            "attributes": null, 
            "backup": false, 
            "checksum": "da39a3ee5e6b4b0d3255bfef95601890afd80709", 
            "content": null, 
            "delimiter": null, 
            "dest": "/etc/ssl/certs/ansible_fun.md", 
            "directory_mode": null, 
            "follow": false, 
            "force": true, 
            "group": null, 
            "local_follow": null, 
            "mode": null, 
            "owner": null, 
            "regexp": null, 
            "remote_src": null, 
            "selevel": null, 
            "serole": null, 
            "setype": null, 
            "seuser": null, 
            "src": "/home/biohpcadmin/.ansible/tmp/ansible-tmp-1569451530.99-94566209961188/source", 
            "unsafe_writes": null, 
            "validate": null
        }
    }, 
    "md5sum": "d41d8cd98f00b204e9800998ecf8427e", 
    "mode": "0644", 
    "owner": "root", 
    "secontext": "system_u:object_r:cert_t:s0", 
    "size": 0, 
    "src": "/home/biohpcadmin/.ansible/tmp/ansible-tmp-1569451530.99-94566209961188/source", 
    "state": "file", 
    "uid": 0
}


PLAY RECAP *********************************************************************************************************************************
198.215.56.37              : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   

```

- verify: 

```
# success!

[zpang1@biohpcwsc037 ~]$ ls -lah /etc/ssl/certs/ | grep -i 'fun'
-rw-r--r--. 1 root root    0 Sep 25 17:45 ansible_fun.md

```

### 09-26-19: pushed new CACert to VMs (git, cloud not included) 

```
[root@Nucleus006 ansible]# ansible-playbook playbooks/push_cert.yml -b -u biohpcadmin -kkkk -K -i hosts
SSH password: 
BECOME password[defaults to SSH password]: 

PLAY [Push Cacert] *************************************************************************************************************************

TASK [Gathering Facts] *********************************************************************************************************************
ok: [198.215.54.68]
fatal: [129.112.9.92]: UNREACHABLE! => {"changed": false, "msg": "Invalid/incorrect password: Permission denied, please try again.", "unreachable": true}
fatal: [129.112.9.91]: UNREACHABLE! => {"changed": false, "msg": "Invalid/incorrect password: Permission denied, please try again.", "unreachable": true}
fatal: [129.112.9.31]: UNREACHABLE! => {"changed": false, "msg": "Invalid/incorrect password: Permission denied, please try again.", "unreachable": true}
ok: [198.215.54.106]
ok: [198.215.54.77]
ok: [198.215.54.67]
ok: [198.215.54.81]
ok: [198.215.54.65]
ok: [198.215.54.28]
ok: [198.215.54.108]
ok: [198.215.54.113]
ok: [198.215.54.25]
ok: [198.215.49.62]

TASK [Copy File to Remote Hosts] ***********************************************************************************************************
changed: [198.215.54.77]
changed: [198.215.54.68]
changed: [198.215.54.106]
changed: [198.215.54.65]
changed: [198.215.54.67]
changed: [198.215.54.81]
changed: [198.215.49.62]
changed: [198.215.54.28]
changed: [198.215.54.108]
changed: [198.215.54.113]
changed: [198.215.54.25]

PLAY RECAP *********************************************************************************************************************************
129.112.9.31               : ok=0    changed=0    unreachable=1    failed=0    skipped=0    rescued=0    ignored=0   
129.112.9.91               : ok=0    changed=0    unreachable=1    failed=0    skipped=0    rescued=0    ignored=0   
129.112.9.92               : ok=0    changed=0    unreachable=1    failed=0    skipped=0    rescued=0    ignored=0   
198.215.49.62              : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
198.215.54.106             : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
198.215.54.108             : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
198.215.54.113             : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
198.215.54.25              : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
198.215.54.28              : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
198.215.54.65              : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
198.215.54.67              : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
198.215.54.68              : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
198.215.54.77              : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
198.215.54.81              : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   

```

### Ansible Ad-Hoc Commands:

- example command a group of hosts to execute the same command 

`ansible VMs -k -b -K -u biohpcadmin --become-method=su -a "grep reqcert  /etc/nslcd.conf"` 







### Ansible Apply Security Updates to Workstations: 

- **tentative**

``` 
# use ad hoc option, "-a"

# check if workstations have kernel & release version excluded:
#
ansible WS -k -b -K -u biohpcadmin --become-method=su -a "grep -i 'exclude' /etc/yum.conf"
#
## if no proper exclusion applies, need to use ansible to add/change exclude list in yum.conf
#
# see how many updates are available per system
#
ansible WS -k -b -K -u biohpcadmin --become-method=su -a "yum check-update --security | grep -i available"
#
# implement update
#
ansible WS -k -b -K -u biohpcadmin --become-method=su -a "yum --security update" 
#
## we can also do "yum --security update-minimal" 

```

- **refined** 

```
ansible WS -k -b -K -u biohpcadmin --become-method=su -a "yum --disablerepo=virtualbox*,puppet* --exclude=kernel*,redhat-release*,libupnp*,libebml*,libmodplug*,libmatroska* --security update-minimal -y"

```

### 10-17-19 

- **test run on 4 WS's,TC's of diff hardware specs** 

ERROR on Thin Clients
-----------------------------------------------------------------------------------------
Transaction check error:
  file /boot/efi/EFI/redhat from install of fwupdate-efi-12-5.el7.x86_64 conflicts with file from package grub2-common-1:2.02-0.64.el7.noarch
  file /boot/efi/EFI/redhat from install of fwupdate-efi-12-5.el7.x86_64 conflicts with file from package grub2-efi-x64-1:2.02-0.64.el7.x86_64
----------------------------------------------------------------------------------------

```
- solution:

https://access.redhat.com/discussions/3689221 


- ThinClient P320 Woes:

```
yum remove facter-2.4.1-1.e17.x86_64  # not necessary 

sudo yum update grub2 firewalld 

yum install llvm-private-7.0.1-1.el7.x86_64

yum update-minimal 

```

  - **ansible commands to update them TC's** 

```

ansible WS -k -b -K -u biohpcadmin --become-method=su -a "yum remove facter-2.4.1-1.e17.x86_64" | tee -a tc_update_log_201910.log

ansible WS -k -b -K -u biohpcadmin --become-method=su -a "sudo yum update grub2 firewalld" | tee -a tc_update_log_201910.log

ansible WS -k -b -K -u biohpcadmin --become-method=su -a "yum install llvm-private-7.0.1-1.el7.x86_64" | tee -a tc_update_log_201910.log

ansible WS -k -b -K -u biohpcadmin --become-method=su -a "yum update-minimal" | tee -a tc_update_log_201910.log

```


### 10-21-19: SSH Errors due to SAME I.P.'s having been assigned to new machines


```
[root@Nucleus006 ansible]# ansible WSs -k -b -K -u biohpcadmin --become-method=su -a "cat /etc/yum.conf | grep -i exclude"
SSH password: 
BECOME password[defaults to SSH password]: 
198.215.56.14 | UNREACHABLE! => {
    "changed": false, 
    "msg": "Failed to connect to the host via ssh: @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\r\n@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @\r\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\r\nIT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!\r\nSomeone could be eavesdropping on you right now (man-in-the-middle attack)!\r\nIt is also possible that a host key has just been changed.\r\nThe fingerprint for the ECDSA key sent by the remote host is\nSHA256:7FhhxPioxtis4GesVz5NUW929f4uIE3ViBEGjDZx5nI.\r\nPlease contact your system administrator.\r\nAdd correct host key in /root/.ssh/known_hosts to get rid of this message.\r\nOffending ECDSA key in /root/.ssh/known_hosts:1158\r\nPassword authentication is disabled to avoid man-in-the-middle attacks.\r\nKeyboard-interactive authentication is disabled to avoid man-in-the-middle attacks.\r\nPermission denied (publickey,password).", 
    "unreachable": true
}
198.215.56.100 | UNREACHABLE! => {
    "changed": false, 
    "msg": "Failed to connect to the host via ssh: @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\r\n@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @\r\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\r\nIT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!\r\nSomeone could be eavesdropping on you right now (man-in-the-middle attack)!\r\nIt is also possible that a host key has just been changed.\r\nThe fingerprint for the ECDSA key sent by the remote host is\nSHA256:7FhhxPioxtis4GesVz5NUW929f4uIE3ViBEGjDZx5nI.\r\nPlease contact your system administrator.\r\nAdd correct host key in /root/.ssh/known_hosts to get rid of this message.\r\nOffending ECDSA key in /root/.ssh/known_hosts:1183\r\nPassword authentication is disabled to avoid man-in-the-middle attacks.\r\nKeyboard-interactive authentication is disabled to avoid man-in-the-middle attacks.\r\nPermission denied (publickey,password).", 
    "unreachable": true
}

### Fix: go to mentioned line in .ssh/known_hosts, and DELETE existing entries:

```

### Additional Security Update Testing: P520c + RHEL 7.6 Machines

```
# two ways to pass stdOUT & stdERR from ansible commands to log file:

1. don't specify  what streams to pass: 
 
<command & flags> | tee -a <absolute path to file>	# `-a` append

2. specify what stream then pipe to file 

<command & flags> 2>&1 | tee -a <absolute path to file>

3. pass without using pipes

<command & flags>  >>  <absolute path to file> 2>&1 

### links: 

[reference 1](https://stackoverflow.com/questions/6674327/redirect-all-output-to-file)
[reference 2](https://stackoverflow.com/questions/418896/how-to-redirect-output-to-a-file-and-stdout)


### Actual Ansible Update Command: 

ansible WSs -k -b -K -u biohpcadmin --become-method=su -a "yum --disablerepo=virtualbox*,puppet* --exclude=kernel*,redhat-release*,libupnp*,libebml*,libmodplug*,libmatroska* --security update-minimal -y" >> ws_update.log 2>&1

---------------------------------------------------------------------------------------------------------
# applied only on wsc100

198.215.56.100 | CHANGED | rc=0 >>
Loaded plugins: langpacks, nvidia, product-id, rhnplugin, search-disabled-repos,
              : subscription-manager
This system is receiving updates from RHN Classic or Red Hat Satellite.
#### NVIDIA ####
Resolving Dependencies
--> Running transaction check
---> Package chromium.x86_64 0:75.0.3770.100-3.el7 will be updated
---> Package chromium.x86_64 0:77.0.3865.90-2.el7 will be an update
---> Package chromium-common.x86_64 0:75.0.3770.100-3.el7 will be updated
---> Package chromium-common.x86_64 0:77.0.3865.90-2.el7 will be an update
---> Package chromium-libs.x86_64 0:75.0.3770.100-3.el7 will be updated
---> Package chromium-libs.x86_64 0:77.0.3865.90-2.el7 will be an update
---> Package chromium-libs-media.x86_64 0:75.0.3770.100-3.el7 will be updated
---> Package chromium-libs-media.x86_64 0:77.0.3865.90-2.el7 will be an update
--> Finished Dependency Resolution

Dependencies Resolved

================================================================================
 Package                  Arch        Version                   Repository
                                                                           Size
================================================================================
Updating:
 chromium                 x86_64      77.0.3865.90-2.el7        epel       35 M
 chromium-common          x86_64      77.0.3865.90-2.el7        epel      8.8 M
 chromium-libs            x86_64      77.0.3865.90-2.el7        epel       75 M
 chromium-libs-media      x86_64      77.0.3865.90-2.el7        epel      2.4 M

Transaction Summary
================================================================================
Upgrade  4 Packages

Total download size: 121 M
Downloading packages:
--------------------------------------------------------------------------------
Total                                              703 kB/s | 121 MB  02:55     
Running transaction check
Running transaction test
Transaction test succeeded
Running transaction
  Updating   : chromium-common-77.0.3865.90-2.el7.x86_64                    1/8 
  Updating   : chromium-libs-77.0.3865.90-2.el7.x86_64                      2/8 
  Updating   : chromium-libs-media-77.0.3865.90-2.el7.x86_64                3/8 
  Updating   : chromium-77.0.3865.90-2.el7.x86_64                           4/8 
  Cleanup    : chromium-75.0.3770.100-3.el7.x86_64                          5/8 
  Cleanup    : chromium-libs-75.0.3770.100-3.el7.x86_64                     6/8 
  Cleanup    : chromium-libs-media-75.0.3770.100-3.el7.x86_64               7/8 
  Cleanup    : chromium-common-75.0.3770.100-3.el7.x86_64                   8/8 
  Verifying  : chromium-libs-media-77.0.3865.90-2.el7.x86_64                1/8 
  Verifying  : chromium-77.0.3865.90-2.el7.x86_64                           2/8 
  Verifying  : chromium-libs-77.0.3865.90-2.el7.x86_64                      3/8 
  Verifying  : chromium-common-77.0.3865.90-2.el7.x86_64                    4/8 
  Verifying  : chromium-libs-75.0.3770.100-3.el7.x86_64                     5/8 
  Verifying  : chromium-75.0.3770.100-3.el7.x86_64                          6/8 
  Verifying  : chromium-common-75.0.3770.100-3.el7.x86_64                   7/8 
  Verifying  : chromium-libs-media-75.0.3770.100-3.el7.x86_64               8/8 

Updated:
  chromium.x86_64 0:77.0.3865.90-2.el7                                          
  chromium-common.x86_64 0:77.0.3865.90-2.el7                                   
  chromium-libs.x86_64 0:77.0.3865.90-2.el7                                     
  chromium-libs-media.x86_64 0:77.0.3865.90-2.el7                            

```
--------------------------------------------------------------------------------


### 10-21-2019: Security Update Continued; Testing on Lenovo P520c & P330 Tiny

```
[root@Nucleus006 ansible]$ pwd
/etc/ansible

[root@Nucleus006 ansible]# ansible WSs -k -b -K -u biohpcadmin --become-method=su -a "yum --disablerepo=virtualbox*,puppet* --exclude=kernel*,redhat-release*,libupnp*,libebml*,libmodplug*,libmatroska* --security update-minimal -y" >> ws_update.log 2>&1


### P520c 

---------------------------------------------------------------------------------
[WARNING]: Consider using the yum module rather than running 'yum'.  If you need to use command because yum is insufficient you can add
'warn: false' to this command task or set 'command_warnings=False' in ansible.cfg to get rid of this message.
198.215.56.14 | CHANGED | rc=0 >>
Loaded plugins: langpacks, nvidia, product-id, rhnplugin, search-disabled-repos,
              : subscription-manager
This system is receiving updates from RHN Classic or Red Hat Satellite.
#### NVIDIA ####
Resolving Dependencies
--> Running transaction check
---> Package chromium.x86_64 0:75.0.3770.100-3.el7 will be updated
---> Package chromium.x86_64 0:77.0.3865.90-2.el7 will be an update
---> Package chromium-common.x86_64 0:75.0.3770.100-3.el7 will be updated
---> Package chromium-common.x86_64 0:77.0.3865.90-2.el7 will be an update
---> Package chromium-libs.x86_64 0:75.0.3770.100-3.el7 will be updated
---> Package chromium-libs.x86_64 0:77.0.3865.90-2.el7 will be an update
---> Package chromium-libs-media.x86_64 0:75.0.3770.100-3.el7 will be updated
---> Package chromium-libs-media.x86_64 0:77.0.3865.90-2.el7 will be an update
--> Finished Dependency Resolution

Dependencies Resolved

================================================================================
 Package                  Arch        Version                   Repository
                                                                           Size
================================================================================
Updating:
 chromium                 x86_64      77.0.3865.90-2.el7        epel       35 M
 chromium-common          x86_64      77.0.3865.90-2.el7        epel      8.8 M
 chromium-libs            x86_64      77.0.3865.90-2.el7        epel       75 M
 chromium-libs-media      x86_64      77.0.3865.90-2.el7        epel      2.4 M

Transaction Summary
================================================================================
Upgrade  4 Packages

Total download size: 121 M
Downloading packages:
--------------------------------------------------------------------------------
Total                                              1.9 MB/s | 121 MB  01:04
Running transaction check
Running transaction test
Transaction test succeeded
Running transaction
  Updating   : chromium-common-77.0.3865.90-2.el7.x86_64                    1/8
  Updating   : chromium-libs-77.0.3865.90-2.el7.x86_64                      2/8
  Updating   : chromium-libs-media-77.0.3865.90-2.el7.x86_64                3/8
  Updating   : chromium-77.0.3865.90-2.el7.x86_64                           4/8
  Cleanup    : chromium-75.0.3770.100-3.el7.x86_64                          5/8
  Cleanup    : chromium-libs-75.0.3770.100-3.el7.x86_64                     6/8
  Cleanup    : chromium-libs-media-75.0.3770.100-3.el7.x86_64               7/8
  Cleanup    : chromium-common-75.0.3770.100-3.el7.x86_64                   8/8
  Verifying  : chromium-libs-media-77.0.3865.90-2.el7.x86_64                1/8
  Verifying  : chromium-77.0.3865.90-2.el7.x86_64                           2/8
  Verifying  : chromium-libs-77.0.3865.90-2.el7.x86_64                      3/8
  Verifying  : chromium-common-77.0.3865.90-2.el7.x86_64                    4/8
  Verifying  : chromium-libs-75.0.3770.100-3.el7.x86_64                     5/8
  Verifying  : chromium-75.0.3770.100-3.el7.x86_64                          6/8
  Verifying  : chromium-common-75.0.3770.100-3.el7.x86_64                   7/8
  Verifying  : chromium-libs-media-75.0.3770.100-3.el7.x86_64               8/8

Updated:
  chromium.x86_64 0:77.0.3865.90-2.el7
  chromium-common.x86_64 0:77.0.3865.90-2.el7
  chromium-libs.x86_64 0:77.0.3865.90-2.el7
  chromium-libs-media.x86_64 0:77.0.3865.90-2.el7

Complete!
198.215.56.136 | CHANGED | rc=0 >>
Loaded plugins: langpacks, nvidia, product-id, rhnplugin, search-disabled-repos,
              : subscription-manager
This system is receiving updates from RHN Classic or Red Hat Satellite.

---------------------------------------------------------------------------------


### P330 Tiny: Better to Use ` | tee -a <logfile>` as it outputs the stdOUT after execution! 

---------------------------------------------------------------------------------
198.215.56.136 | CHANGED | rc=0 >>
Loaded plugins: langpacks, nvidia, product-id, rhnplugin, search-disabled-repos,
              : subscription-manager
This system is receiving updates from RHN Classic or Red Hat Satellite.
#### NVIDIA ####
Resolving Dependencies
--> Running transaction check
---> Package chromium.x86_64 0:75.0.3770.100-3.el7 will be updated
---> Package chromium.x86_64 0:77.0.3865.90-2.el7 will be an update
---> Package chromium-common.x86_64 0:75.0.3770.100-3.el7 will be updated
---> Package chromium-common.x86_64 0:77.0.3865.90-2.el7 will be an update
---> Package chromium-libs.x86_64 0:75.0.3770.100-3.el7 will be updated
---> Package chromium-libs.x86_64 0:77.0.3865.90-2.el7 will be an update
---> Package chromium-libs-media.x86_64 0:75.0.3770.100-3.el7 will be updated
---> Package chromium-libs-media.x86_64 0:77.0.3865.90-2.el7 will be an update
---> Package firefox.x86_64 0:60.8.0-1.el7_6 will be updated
---> Package firefox.x86_64 0:60.9.0-1.el7_7 will be an update
---> Package ghostscript.x86_64 0:9.25-2.el7_7.1 will be updated
---> Package ghostscript.x86_64 0:9.25-2.el7_7.2 will be an update
---> Package ghostscript-cups.x86_64 0:9.25-2.el7_7.1 will be updated
---> Package ghostscript-cups.x86_64 0:9.25-2.el7_7.2 will be an update
---> Package libgs.x86_64 0:9.25-2.el7_7.1 will be updated
---> Package libgs.x86_64 0:9.25-2.el7_7.2 will be an update
---> Package pango.x86_64 0:1.42.4-3.el7 will be updated
---> Package pango.x86_64 0:1.42.4-4.el7_7 will be an update
---> Package perf.x86_64 0:3.10.0-1062.el7 will be updated
---> Package perf.x86_64 0:3.10.0-1062.1.1.el7 will be an update
---> Package python-perf.x86_64 0:3.10.0-1062.el7 will be updated
---> Package python-perf.x86_64 0:3.10.0-1062.1.1.el7 will be an update
---> Package qemu-img.x86_64 10:1.5.3-167.el7 will be updated
---> Package qemu-img.x86_64 10:1.5.3-167.el7_7.1 will be an update
---> Package qemu-kvm.x86_64 10:1.5.3-167.el7 will be updated
---> Package qemu-kvm.x86_64 10:1.5.3-167.el7_7.1 will be an update
---> Package qemu-kvm-common.x86_64 10:1.5.3-167.el7 will be updated
---> Package qemu-kvm-common.x86_64 10:1.5.3-167.el7_7.1 will be an update
--> Finished Dependency Resolution

Dependencies Resolved

================================================================================
 Package              Arch    Version               Repository             Size
================================================================================
Updating:
 chromium             x86_64  77.0.3865.90-2.el7    epel                   35 M
 chromium-common      x86_64  77.0.3865.90-2.el7    epel                  8.8 M
 chromium-libs        x86_64  77.0.3865.90-2.el7    epel                   75 M
 chromium-libs-media  x86_64  77.0.3865.90-2.el7    epel                  2.4 M
 firefox              x86_64  60.9.0-1.el7_7        rhel-x86_64-server-7   91 M
 ghostscript          x86_64  9.25-2.el7_7.2        rhel-x86_64-server-7  112 k
 ghostscript-cups     x86_64  9.25-2.el7_7.2        rhel-x86_64-server-7   61 k
 libgs                x86_64  9.25-2.el7_7.2        rhel-x86_64-server-7  4.6 M
 pango                x86_64  1.42.4-4.el7_7        rhel-x86_64-server-7  280 k
 perf                 x86_64  3.10.0-1062.1.1.el7   rhel-x86_64-server-7  9.4 M
 python-perf          x86_64  3.10.0-1062.1.1.el7   rhel-x86_64-server-7  7.8 M
 qemu-img             x86_64  10:1.5.3-167.el7_7.1  rhel-x86_64-server-7  699 k
 qemu-kvm             x86_64  10:1.5.3-167.el7_7.1  rhel-x86_64-server-7  1.9 M
 qemu-kvm-common      x86_64  10:1.5.3-167.el7_7.1  rhel-x86_64-server-7  435 k

Transaction Summary
================================================================================
Upgrade  14 Packages

Total download size: 237 M
Downloading packages:
No Presto metadata available for rhel-x86_64-server-7
--------------------------------------------------------------------------------
Total                                              3.1 MB/s | 237 MB  01:16
Running transaction check
Running transaction test
Transaction test succeeded
Running transaction
  Updating   : libgs-9.25-2.el7_7.2.x86_64                                 1/28
  Updating   : pango-1.42.4-4.el7_7.x86_64                                 2/28
  Updating   : ghostscript-9.25-2.el7_7.2.x86_64                           3/28
  Updating   : 10:qemu-kvm-common-1.5.3-167.el7_7.1.x86_64                 4/28
  Updating   : 10:qemu-img-1.5.3-167.el7_7.1.x86_64                        5/28
  Updating   : chromium-common-77.0.3865.90-2.el7.x86_64                   6/28
  Updating   : chromium-libs-media-77.0.3865.90-2.el7.x86_64               7/28
  Updating   : chromium-libs-77.0.3865.90-2.el7.x86_64                     8/28
  Updating   : chromium-77.0.3865.90-2.el7.x86_64                          9/28
  Updating   : 10:qemu-kvm-1.5.3-167.el7_7.1.x86_64                       10/28
  Updating   : ghostscript-cups-9.25-2.el7_7.2.x86_64                     11/28
  Updating   : firefox-60.9.0-1.el7_7.x86_64                              12/28
  Updating   : python-perf-3.10.0-1062.1.1.el7.x86_64                     13/28
  Updating   : perf-3.10.0-1062.1.1.el7.x86_64                            14/28
  Cleanup    : 10:qemu-kvm-1.5.3-167.el7.x86_64                           15/28
  Cleanup    : ghostscript-cups-9.25-2.el7_7.1.x86_64                     16/28
  Cleanup    : chromium-75.0.3770.100-3.el7.x86_64                        17/28
  Cleanup    : firefox-60.8.0-1.el7_6.x86_64                              18/28
  Cleanup    : chromium-libs-75.0.3770.100-3.el7.x86_64                   19/28
  Cleanup    : chromium-libs-media-75.0.3770.100-3.el7.x86_64             20/28
  Cleanup    : ghostscript-9.25-2.el7_7.1.x86_64                          21/28
  Cleanup    : chromium-common-75.0.3770.100-3.el7.x86_64                 22/28
  Cleanup    : libgs-9.25-2.el7_7.1.x86_64                                23/28
  Cleanup    : pango-1.42.4-3.el7.x86_64                                  24/28
  Cleanup    : 10:qemu-img-1.5.3-167.el7.x86_64                           25/28
  Cleanup    : 10:qemu-kvm-common-1.5.3-167.el7.x86_64                    26/28
  Cleanup    : python-perf-3.10.0-1062.el7.x86_64                         27/28
  Cleanup    : perf-3.10.0-1062.el7.x86_64                                28/28
  Verifying  : chromium-77.0.3865.90-2.el7.x86_64                          1/28
  Verifying  : chromium-common-77.0.3865.90-2.el7.x86_64                   2/28
  Verifying  : ghostscript-9.25-2.el7_7.2.x86_64                           3/28
  Verifying  : ghostscript-cups-9.25-2.el7_7.2.x86_64                      4/28
  Verifying  : firefox-60.9.0-1.el7_7.x86_64                               5/28
  Verifying  : pango-1.42.4-4.el7_7.x86_64                                 6/28
  Verifying  : 10:qemu-img-1.5.3-167.el7_7.1.x86_64                        7/28
  Verifying  : libgs-9.25-2.el7_7.2.x86_64                                 8/28
  Verifying  : chromium-libs-77.0.3865.90-2.el7.x86_64                     9/28
  Verifying  : perf-3.10.0-1062.1.1.el7.x86_64                            10/28
  Verifying  : 10:qemu-kvm-common-1.5.3-167.el7_7.1.x86_64                11/28
  Verifying  : chromium-libs-media-77.0.3865.90-2.el7.x86_64              12/28
  Verifying  : 10:qemu-kvm-1.5.3-167.el7_7.1.x86_64                       13/28
  Verifying  : python-perf-3.10.0-1062.1.1.el7.x86_64                     14/28
  Verifying  : 10:qemu-img-1.5.3-167.el7.x86_64                           15/28
  Verifying  : perf-3.10.0-1062.el7.x86_64                                16/28
  Verifying  : chromium-libs-75.0.3770.100-3.el7.x86_64                   17/28
  Verifying  : 10:qemu-kvm-1.5.3-167.el7.x86_64                           18/28
  Verifying  : libgs-9.25-2.el7_7.1.x86_64                                19/28
  Verifying  : pango-1.42.4-3.el7.x86_64                                  20/28
  Verifying  : firefox-60.8.0-1.el7_6.x86_64                              21/28
  Verifying  : python-perf-3.10.0-1062.el7.x86_64                         22/28
  Verifying  : ghostscript-9.25-2.el7_7.1.x86_64                          23/28
  Verifying  : chromium-75.0.3770.100-3.el7.x86_64                        24/28
  Verifying  : ghostscript-cups-9.25-2.el7_7.1.x86_64                     25/28
  Verifying  : 10:qemu-kvm-common-1.5.3-167.el7.x86_64                    26/28
  Verifying  : chromium-common-75.0.3770.100-3.el7.x86_64                 27/28
  Verifying  : chromium-libs-media-75.0.3770.100-3.el7.x86_64             28/28

Updated:
  chromium.x86_64 0:77.0.3865.90-2.el7
  chromium-common.x86_64 0:77.0.3865.90-2.el7
  chromium-libs.x86_64 0:77.0.3865.90-2.el7
  chromium-libs-media.x86_64 0:77.0.3865.90-2.el7
  firefox.x86_64 0:60.9.0-1.el7_7
  ghostscript.x86_64 0:9.25-2.el7_7.2
  ghostscript-cups.x86_64 0:9.25-2.el7_7.2
  libgs.x86_64 0:9.25-2.el7_7.2
  pango.x86_64 0:1.42.4-4.el7_7
  perf.x86_64 0:3.10.0-1062.1.1.el7
  python-perf.x86_64 0:3.10.0-1062.1.1.el7
  qemu-img.x86_64 10:1.5.3-167.el7_7.1
  qemu-kvm.x86_64 10:1.5.3-167.el7_7.1
  qemu-kvm-common.x86_64 10:1.5.3-167.el7_7.1

Complete!There are unfinished transactions remaining. You might consider running yum-complete-transaction, or "yum-complete-transaction --cleanup-only" and "yum history redo last", first to finish them. If those don't work you'll have to try removing/installing packages by hand (maybe package-cleanup can help).
---------------------------------------------------------------------------------
```

M (VIsual editor iMproved) is an updated and improved version of the

### 10-25-2019: Further Advancing Plan to Implement WS Security Update 

- **Fetch Workstation's Model# & Type**

```
# ad-hoc command printing out machine's vendor & model number:

----------------------------------------------------------------------------------------------------------
ansible NAs -k -b -K -u biohpcadmin --become-method=su -a "cat /sys/devices/virtual/dmi/id/product_version" 
----------------------------------------------------------------------------------------------------------


  ### failed commands: what doesn't work: ansible doesn't play well with REGULAR EXPRESSIONS

----------------------------------------------------------------------------------------------------------
ansible NAs -k -b -K -u biohpcadmin --become-method=su -a "cat /sys/devices/virtual/dmi/id/*"

ansible NAs -k -b -K -u biohpcadmin --become-method=su -a "cat /sys/devices/virtual/dmi/id/*|tail -1"

ansible NAs -k -b -K -u biohpcadmin --become-method=su -a "dmidecode | grep -i -A3 '^system information'"
----------------------------------------------------------------------------------------------------------

```

- **Commands For Difficult P320 Tiny's**

```
yum update grub2 firewalld

yum install llvm-private-7.0.1-1.el7.x86_64

yum update-minimal

ansible WS -k -b -K -u biohpcadmin --become-method=su -a "yum --disablerepo=virtualbox*,puppet* --exclude=kernel*,redhat-release*,libupnp*,libebml*,libmodplug*,libmatroska* update grub2 firewalld"

ansible WS -k -b -K -u biohpcadmin --become-method=su -a "yum --disablerepo=virtualbox*,puppet* --exclude=kernel*,redhat-release*,libupnp*,libebml*,libmodplug*,libmatroska* install llvm-private-7.0.1-1.el7.x86_64"

ansible WS -k -b -K -u biohpcadmin --become-method=su -a "yum --disablerepo=virtualbox*,puppet* --exclude=kernel*,redhat-release*,libupnp*,libebml*,libmodplug*,libmatroska* --security update-minimal

```

### Ansible Update Continued on 10-30-2019 

- Check ThinkStation P320 Tiny's Facter Packages: 

  - suspect they each have an extra older copy:

```
# see how many copies of facter each machine has:

### FAILED COMMAND: this DOES NOT SHOW how many of the same packages are installed:
ansible TCse -k -b -K -u biohpcadmin --become-method=su -a "yum search facter" 


### CORRECT CHECK COMMAND:
ansible TCse -k -b -K -u biohpcadmin --become-method=su -a "yum info facter"

### OUTPUT:

--------------------------------------------------------------------------------
198.215.56.33 | CHANGED | rc=0 >>
Loaded plugins: langpacks, product-id, rhnplugin, search-disabled-repos,
              : subscription-manager
This system is receiving updates from RHN Classic or Red Hat Satellite.
Installed Packages
Name        : facter
Arch        : x86_64
Epoch       : 1
Version     : 2.4.6
Release     : 1.el7
Size        : 273 k
Repo        : installed
From repo   : puppet-biohpc
Summary     : Ruby module for collecting simple facts about a host operating
            : system
URL         : http://www.puppetlabs.com/puppet/related-projects/facter
License     : ASL 2.0
Description : Ruby module for collecting simple facts about a host Operating
            : system. Some of the facts are preconfigured, such as the hostname
            : and the operating system. Additional facts can be added through
            : simple Ruby scripts

198.215.56.36 | CHANGED | rc=0 >>
Loaded plugins: langpacks, product-id, rhnplugin, search-disabled-repos,
              : subscription-manager
This system is receiving updates from RHN Classic or Red Hat Satellite.
Installed Packages
Name        : facter
Arch        : x86_64
Epoch       : 1
Version     : 2.4.6
Release     : 1.el7
Size        : 273 k
Repo        : installed
From repo   : puppet-biohpc
Summary     : Ruby module for collecting simple facts about a host operating
            : system
URL         : http://www.puppetlabs.com/puppet/related-projects/facter
License     : ASL 2.0
Description : Ruby module for collecting simple facts about a host Operating
            : system. Some of the facts are preconfigured, such as the hostname
            : and the operating system. Additional facts can be added through
            : simple Ruby scripts


### THIS NODE HAS BEEN IDENTIFIED WITH EXTRA PACKAGE!
------------------------------------------------------------------------------------------
198.215.56.42 | CHANGED | rc=0 >>
Loaded plugins: langpacks, product-id, rhnplugin, search-disabled-repos,
              : subscription-manager
This system is receiving updates from RHN Classic or Red Hat Satellite.
Installed Packages
Name        : facter
Arch        : x86_64
Version     : 2.4.1
Release     : 1.el7
Size        : 271 k
Repo        : installed
From repo   : epel
Summary     : Command and ruby library for gathering system information
URL         : https://puppetlabs.com/facter
License     : ASL 2.0
Description : Facter is a lightweight program that gathers basic node
            : information about the hardware and operating system. Facter is
            : especially useful for retrieving things like operating system
            : names, hardware characteristics, IP addresses, MAC addresses, and
            : SSH keys.
            : 
            : Facter is extensible and allows gathering of node information that
            : may be custom or site specific. It is easy to extend by including
            : your own custom facts. Facter can also be used to create
            : conditional expressions in Puppet that key off the values returned
            : by facts.

Name        : facter
Arch        : x86_64
Epoch       : 1
Version     : 2.4.6
Release     : 1.el7
Size        : 273 k
Repo        : installed
Summary     : Ruby module for collecting simple facts about a host operating
            : system
URL         : http://www.puppetlabs.com/puppet/related-projects/facter
License     : ASL 2.0
Description : Ruby module for collecting simple facts about a host Operating
            : system. Some of the facts are preconfigured, such as the hostname
            : and the operating system. Additional facts can be added through
            : simple Ruby scripts
------------------------------------------------------------------------------------------



198.215.56.101 | CHANGED | rc=0 >>
Loaded plugins: langpacks, product-id, rhnplugin, search-disabled-repos,
              : subscription-manager
This system is receiving updates from RHN Classic or Red Hat Satellite.
Installed Packages
Name        : facter
Arch        : x86_64
Epoch       : 1
Version     : 2.4.6
Release     : 1.el7
Size        : 273 k
Repo        : installed
From repo   : puppet-biohpc
Summary     : Ruby module for collecting simple facts about a host operating
            : system
URL         : http://www.puppetlabs.com/puppet/related-projects/facter
License     : ASL 2.0
Description : Ruby module for collecting simple facts about a host Operating
            : system. Some of the facts are preconfigured, such as the hostname
            : and the operating system. Additional facts can be added through
            : simple Ruby scripts

198.215.56.44 | CHANGED | rc=0 >>
Loaded plugins: langpacks, product-id, rhnplugin, search-disabled-repos,
              : subscription-manager
This system is receiving updates from RHN Classic or Red Hat Satellite.
Installed Packages
Name        : facter
Arch        : x86_64
Epoch       : 1
Version     : 2.4.6
Release     : 1.el7
Size        : 273 k
Repo        : installed
Summary     : Ruby module for collecting simple facts about a host operating
            : system
URL         : http://www.puppetlabs.com/puppet/related-projects/facter
License     : ASL 2.0
Description : Ruby module for collecting simple facts about a host Operating
            : system. Some of the facts are preconfigured, such as the hostname
            : and the operating system. Additional facts can be added through
            : simple Ruby scripts

198.215.56.136 | CHANGED | rc=0 >>
Loaded plugins: langpacks, nvidia, product-id, rhnplugin, search-disabled-repos,
              : subscription-manager
This system is receiving updates from RHN Classic or Red Hat Satellite.
#### NVIDIA ####
Installed Packages
Name        : facter
Arch        : x86_64
Epoch       : 1
Version     : 2.4.6
Release     : 1.el7
Size        : 273 k
Repo        : installed
Summary     : Ruby module for collecting simple facts about a host operating
            : system
URL         : http://www.puppetlabs.com/puppet/related-projects/facter
License     : ASL 2.0
Description : Ruby module for collecting simple facts about a host Operating
            : system. Some of the facts are preconfigured, such as the hostname
            : and the operating system. Additional facts can be added through
            : simple Ruby scripts

198.215.56.122 | CHANGED | rc=0 >>
Loaded plugins: langpacks, product-id, rhnplugin, search-disabled-repos,
              : subscription-manager
This system is receiving updates from RHN Classic or Red Hat Satellite.
Installed Packages
Name        : facter
Arch        : x86_64
Epoch       : 1
Version     : 2.4.6
Release     : 1.el7
Size        : 273 k
Repo        : installed
Summary     : Ruby module for collecting simple facts about a host operating
            : system
URL         : http://www.puppetlabs.com/puppet/related-projects/facter
License     : ASL 2.0
Description : Ruby module for collecting simple facts about a host Operating
            : system. Some of the facts are preconfigured, such as the hostname
            : and the operating system. Additional facts can be added through
            : simple Ruby scripts

---------------------------------------------------------------------------------

```

- **More Efficient** Ansible command to check facter packages:

  - this also gives the exact package names, which makes deletions a bit easier 

```
ansible TCse -k -b -K -u biohpcadmin --become-method=su -a "rpm -q facter" 

---------------------------------------------------------------------------------
### MUCH SIMPLER OUTPUT:

198.215.56.42 | CHANGED | rc=0 >>
facter-2.4.1-1.el7.x86_64
facter-2.4.6-1.el7.x86_64

198.215.56.33 | CHANGED | rc=0 >>
facter-2.4.6-1.el7.x86_64

198.215.56.36 | CHANGED | rc=0 >>
facter-2.4.6-1.el7.x86_64

198.215.56.101 | CHANGED | rc=0 >>
facter-2.4.6-1.el7.x86_64

198.215.56.44 | CHANGED | rc=0 >>
facter-2.4.6-1.el7.x86_64

198.215.56.136 | CHANGED | rc=0 >>
facter-2.4.6-1.el7.x86_64

198.215.56.122 | CHANGED | rc=0 >>
facter-2.4.6-1.el7.x86_64
---------------------------------------------------------------------------------

```


- Delete the node with extra package:

```
# Command:
ansible 198.215.56.42 -k -b -K -u biohpcadmin --become-method=su -a "yum -y remove facter-2.4.1-1.el7.x86_64"


---------------------------------------------------------------------------------
198.215.56.42 | CHANGED | rc=0 >>
Loaded plugins: langpacks, product-id, rhnplugin, search-disabled-repos,
              : subscription-manager
This system is receiving updates from RHN Classic or Red Hat Satellite.
Resolving Dependencies
--> Running transaction check
---> Package facter.x86_64 0:2.4.1-1.el7 will be erased
--> Finished Dependency Resolution

Dependencies Resolved

================================================================================
 Package          Arch             Version                Repository       Size
================================================================================
Removing:
 facter           x86_64           2.4.1-1.el7            @epel           271 k

Transaction Summary
================================================================================
Remove  1 Package

Installed size: 271 k
Downloading packages:
Running transaction check
Running transaction test
Transaction test succeeded
Running transaction
  Erasing    : facter-2.4.1-1.el7.x86_64                                    1/1 
warning: file /usr/share/doc/facter-2.4.1/README.md: remove failed: No such file or directory
warning: file /usr/share/doc/facter-2.4.1/LICENSE: remove failed: No such file or directory
warning: file /usr/share/doc/facter-2.4.1: remove failed: No such file or directory
warning: file /etc/facter/facts.d: remove failed: No such file or directory
warning: file /etc/facter: remove failed: No such file or directory
  Verifying  : facter-2.4.1-1.el7.x86_64                                    1/1 

Removed:
  facter.x86_64 0:2.4.1-1.el7                                                   

Complete!There are unfinished transactions remaining. You might consider running yum-complete-transaction, or "yum-complete-transaction --cleanup-only" and "yum history redo last", first to finish them. If those don't work you'll have to try removing/installing packages by hand (maybe package-cleanup can help).
---------------------------------------------------------------------------------

```

### 11-1-2019 LIVE UPDATE ON TCse's:

- Plan: 

  - 1. update grub2 & firewalld 

`ansible WS -k -b -K -u biohpcadmin --become-method=su -a "yum --disablerepo=virtualbox*,puppet* --exclude=kernel*,redhat-release*,libupnp*,libebml*,libmodplug*,libmatroska* update grub2 firewalld"` 

  - 2. install dependent package (pre-identified) 

`ansible WS -k -b -K -u biohpcadmin --become-method=su -a "yum --disablerepo=virtualbox*,puppet* --exclude=kernel*,redhat-release*,libupnp*,libebml*,libmodplug*,libmatroska* install llvm-private-7.0.1-1.el7.x86_64"`

  - 3. execute security update command

`ansible WS -k -b -K -u biohpcadmin --become-method=su -a "yum --disablerepo=virtualbox*,puppet* --exclude=kernel*,redhat-release*,libupnp*,libebml*,libmodplug*,libmatroska* --security update-minimal` 


```
### LIVE Documentations:

```
### 11-04-2019



```


















```

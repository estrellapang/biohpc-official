## List on VMs & Servers' LDAP Servers


### VMs using `ldap002` 

| VMs | Servers| LDAP Servers |
|:---:|:---:|:---:|
|cloud|  |ldap002 |
|git-dmz-production |  |ldap002  |
|grace  |  | no ldap  |
|portal-dmz  |  |no ldap  |
|ansir-dmz |  |no ldap |
|astrocyte?  |  |  |
|cdc?  |  |  |
|flash-dmz  |  |ldap002  |
|pops  |  |no ldap  |
|sap-dmz  |  |no ldap  |
|service-dmz  |  |no ldap|
|astrocyte7-production  |  |ldap002  |
|astrocyte-test-rhel7 |  |ldap002   |
|backup001 |  |ldap002 |
|biohpcadmin?  |  |  |
|BioHPC Thunder |  | ldap002  |
|clarityAI |  | ldap002  |
|cpfp   |  | ldap002  |
|foreman  |  |ldap002  |
|galaxy-rhel7-test  |  | ldap002  |
|NGS_rhel7  |  | ldap002  |
|rstudio7  |  |ldap002  |
| singularity-build  |  | ldap002 |
|smrtlink?  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |

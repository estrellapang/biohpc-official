## Remote Server Ops via `ipmitool` 

> A BIT OF A TANGENT: SHUT DOWN COMMANDS
> shutdown -h now
> [RHEL7 Power cmds](https://www.certdepot.net/rhel7-boot-reboot-shut-system-normally/)

### Reboot Servers

- To set boot device to PXE and then power cycle: 

[root@Nucleus006 zpang1]# ipmitool -I lanplus -U biohpcadmin -H 192.168.2.31 chassis bootdev pxe
Password: 


- [root@Nucleus006 zpang1]# ipmitool -I lanplus -U biohpcadmin -H 192.168.2.31 chassis power cycle
Password: 
Chassis Power Control: Cycle

\
\

#### reboot locally

  - `module add ipmitool/1.8.17`

  - `ipmitool chassis bootdev pxe`

  - `ipmitool chassis power reset hard`

\
\

#### reboot bmc

- `ipmitool bmc reset cold`

\
\
\

### Set IPMI Password

```
### this can be written as a script(located in /emoh)

module add ipmitool/1.8.17 
modprobe ipmi_devintf
modprobe ipmi_si
service ipmi restart

ipmitool user set name 3 biohpcadmin   # use `root` if `biohpcadmin`
ipmitool user set password 3 "*****"

ipmitool user enable 3
ipmitool channel setaccess 1 3 link=on ipmi=on callin=on privilege=4
ipmitool channel setaccess 2 3 link=on ipmi=on callin=on privilege=4
ipmitool user enable 3

```

### Check Status of Nodes

```
# show HW errors on node
----------------------------------------------------
ipmitool <IPMI IP> sel list 

  # show last Nth error mesages

ipmitool sel list last <#>
----------------------------------------------------

# show all IPMI events/logs 
----------------------------------------------------
ipmitool sel elist
----------------------------------------------------

# show node sensor status
----------------------------------------------------
ipmitool sdr list


  # see aonly temp, voltage, and fan sensors

ipmitool sdr elist full
----------------------------------------------------

# show node FRU inventory info
----------------------------------------------------
ipmitool fru print
----------------------------------------------------

# show node's BMC time
----------------------------------------------------
ipmitool sel time get

# set time

ipmitool sel time set <time format>
----------------------------------------------------

# turn on front panel LED (default 15s)
----------------------------------------------------
ipmitool chassis identify <seconds>

# turn on indefinitely `force` 

# turn off `0`
----------------------------------------------------


```

- [incredible Oracle examples](https://docs.oracle.com/cd/E19464-01/820-6850-11/IPMItool.html)

- [tutorial for basic ops](https://www.tzulo.com/crm/knowledgebase/47/IPMI-and-IPMITOOL-Cheat-sheet.html)

- [man page](https://linux.die.net/man/1/ipmitool)

\
\
\

### Remotely Configure CPU settings

> downside --> needs `omconfig` package, which is very release version specific

- [link here](https://docs.biohpc.swmed.edu/dokuwiki/doku.php?id=test-cluster_changes&s[]=test&s[]=cluster)

\
\
\

### Resources 

- [link 1](http://xgenecode.com/wp/2017/09/23/base-management-controller-bmc-ipmi-useful-commands-on-rhelcentosfedoraubuntu-linux/?i=1) 

- [link 2](http://www.theprojectbot.com/ipmitool-cheatsheet-and-configuring-drac-from-ipmitool/)  

- [link 3](https://www.tzulo.com/crm/knowledgebase/47/IPMI-and-IPMITOOL-Cheat-sheet.html)

- [link 4](http://www.fibrevillage.com/sysadmin/71-ipmitool-useful-examples)

- [bashscript array loop examples](https://stackabuse.com/array-loops-in-bash/)

\
\
\

### Bash-scripting Implementations

> .sh scripts are located under `/home2/zpang1/Desktop/Notes/My_Scripts/sysadmin_fun`

- sequential for-loop

```
#!/bin/bash

for i in {86..93}

do
	echo "setting PXE boot to  NucleusA$i"
#	ipmitool -I lanplus -U biohpcadmin -H 192.168.2.$i chassis bootdev pxe
	echo "shutting down NucleusA$i"
#	ipmitool -I lanplus -U biohpcadmin -H 192.168.2.$i chassis power off

done 

```
\
\
\

- array of unordered for-loop

```
#!/bin/bash
# load ipmitools module
module load ipmitool/1.8.17
# enter nodes to be operated on in array below
nodes=(13 153 228)

for i in "${nodes[@]}" 

do

# set PXE boot to node prior to reboot

	echo "setting PXE boot to 192.168.2.$i"
	ipmitool -I lanplus -U biohpcadmin -H 192.168.2.$i chassis bootdev pxe

# hard reset nodes in array

	echo "rebooting 192.168.2.$i"
	ipmitool -I lanplus -U biohpcadmin -H 192.168.2.$i chassis power reset hard

done 

```

\
\
\
   
### CLI For-Loop Implementations

```
### SEQUENTIAL Array

-------------------------------------------------------------------------------
for i in $(seq ## ##); do ipmitool -I lanplus -U biohpcadmin -H 192.168.#.$i chassis power reset hard;done
-------------------------------------------------------------------------------

### NON-SEQ Array

-------------------------------------------------------------------------------
sysadminArray=(## ## ##); for i in "${sysadminArray[@]}"; do ipmitool -I lanplus -U biohpcadmin -H 192.168.#.$i chassis bootdev pxe;done
-------------------------------------------------------------------------------

```

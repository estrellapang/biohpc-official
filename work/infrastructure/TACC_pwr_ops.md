## Information & Observation on Unloading TACC Racks' Power Output 

### NOTE: Power Reduction not SEEN in allocated nodes 

- **idle** nodes show a "P_OUT" reduction on average of 440Watts (baseline P_OUT, after Xeon Phi removal = ~350Watts) 

- **allocated** noes experience **no effect**, with baseline "P_OUT" values averaging b/w 1100-1200 Watts, even **after** Xeon Phi modules have been removed 
  - as seen in NucA74-81 on August 10, 2019--via IPMI interface

### Book-keeping: Completed Nodes

> total count: 116 ( )

```
### Rack BI14 ### (16)

NucleusA18-21

NucleusA22-25

NucleusA26-29

NucleusA30-33 

NucleusA34-37


### Rack BI13 ### (24)

NucleusA042-045

NucleusA46-49

NucleusA50-53

NucleusA54-57  

NucleusA58-61   

NucleusA62-65   

NucleusA66-69

NucleusA70-73

NucleusA74-77 

NucleusA78-81



### Rack BI12 ### (28)


NucleusA086-089 

NucleusA090-093

NucleusA094-097

NucleusA098-101

NucleusA102-105

NucleusA106-109

NucleusA110-113

NucleusA114-117

NucleusA118-121



### Rack BI11 ### (8)

NucleusA142-149 (8!) 



### Rack BI10 ### (8) 

NucleusA170-173

NucleusA174-177



### Rack BI08 ### (4)

NucleusB002-005

```

### Book-keeping: Shutdown Nodes (47)

```
### BI08 (8)

NucleusB022-027 

### BI09 (27) 

NucleusA205-207,218-241

### BI10 (12)

NucleusA185-189,190-196

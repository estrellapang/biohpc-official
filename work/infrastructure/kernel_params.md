## Tweaking Linux Kernel Parameters

### Number of Openfiles

#### Links

- [link1](https://rtcamp.com/tutorials/linux/increase-open-files-limit/)

- [link2](https://www.tutorialspoint.com/increase-number-of-maximum-open-files-in-linux)

- [link3](https://www.cyberciti.biz/faq/linux-increase-the-maximum-number-of-open-files/)

- [link4](https://www.tecmint.com/increase-set-open-file-limits-in-linux/)

- [link5](https://www.unixarena.com/2013/12/how-to-increase-ulimit-values-in-redhat.html/)

\
\


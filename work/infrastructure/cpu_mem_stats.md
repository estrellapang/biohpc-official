## Memory & CPU Ops

### CPU 

- **Tuned Configs**

- **Check Hyper Threading/Governor**

```
cpupower frequency-info
analyzing CPU 0:
  driver: intel_pstate
  CPUs which run at the same hardware frequency: 0
  CPUs which need to have their frequency coordinated by software: 0
  maximum transition latency:  Cannot determine or is not supported.
  hardware limits: 1000 MHz - 3.90 GHz
  available cpufreq governors: performance powersave
  current policy: frequency should be within 1000 MHz and 3.90 GHz.
                  The governor "powersave" may decide which speed to use
                  within this range.
  current CPU frequency: 1.16 GHz (asserted by call to hardware)
  boost state support:
    Supported: yes
    Active: yes

\
\
\

lscpu
Architecture:          x86_64
CPU op-mode(s):        32-bit, 64-bit
Byte Order:            Little Endian
CPU(s):                72                ### Hyper Threading Enabled~
On-line CPU(s) list:   0-71
Thread(s) per core:    2                 ### Hyper Threading !!!
Core(s) per socket:    18
Socket(s):             2
NUMA node(s):          2
Vendor ID:             GenuineIntel
CPU family:            6
Model:                 85
Model name:            Intel(R) Xeon(R) Gold 6240 CPU @ 2.60GHz
Stepping:              7
CPU MHz:               2600.000
BogoMIPS:              5200.00
Virtualization:        VT-x
L1d cache:             32K
L1i cache:             32K
L2 cache:              1024K
L3 cache:              25344K
NUMA node0 CPU(s):     0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,42,44,46,48,50,52,54,56,58,60,62,64,66,68,70
NUMA node1 CPU(s):     1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39,41,43,45,47,49,51,53,55,57,59,61,63,65,67,69,71
Flags:                 fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf eagerfpu pni pclmulqdq dtes64 monitor ds_cpl vmx smx est tm2 ssse3 fma cx16 xtpr pdcm pcid dca sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch epb cat_l3 cdp_l3 intel_pt tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 hle avx2 smep bmi2 erms invpcid rtm cqm mpx rdt_a avx512f avx512dq rdseed adx smap clflushopt clwb avx512cd avx512bw avx512vl xsaveopt xsavec xgetbv1 cqm_llc cqm_occup_llc cqm_mbm_total cqm_mbm_local dtherm ida arat pln pts
```

\
\

- **Set CPU Frequency**

  - `cpupower frequency-set --min <kHz> --max <kHz>`

  - e.g. `cpupower frequency-set --min 1200000 --max 3000000`


- **Check CPU Hardware frequency limits**

  - `cpupower -c <core #> frequency-info --hwlimits`


- **Set CPU governors**

  - `cpupower frequency-set -g powersave`  --> cpu power save mode

- **Grep current all core frequencies"

  - `grep -E '^cpu MHz' /proc/cpuinfo`

\
\

### Memory

- [drop memory cache](https://tecadmin.net/flush-memory-cache-on-linux-server/)

- [drop file cache](https://unix.stackexchange.com/questions/17936/setting-proc-sys-vm-drop-caches-to-clear-cache#17943)

- [good cache drop overview + clear swap](https://www.tecmint.com/clear-ram-memory-cache-buffer-and-swap-space-on-linux/)

- **drop PageCache** 

  - `sync; echo 1 > /proc/sys/vm/drop_caches`

- **clear dentries & inodes** ("file cache")

  - `sync; echo 2  > /proc/sys/vm/drop_caches`

- **drop page and file cache** 

  - required when we have hanging RPCs but trying to unmount or remount 

  - a bit dangerous for production systems

> "sync" flushes the filesystem buffer 

```
----------------------
echo 3 > /proc/sys/vm/drop_caches
----------------------

```

\

- **clean swap**

  - `swapoff -a` && `swapon -a`

  - [link for swapiness](https://www.redhat.com/sysadmin/clear-swap-linux)
 

## Info & Operation Logs on Xymon Monitoring: Fall 2019

### Server Configs

- set client alert thresholds in `../server/etc/analysis.cfg`

- [analysis man page](https://xymon.sourceforge.io/xymon/help/manpages/man5/analysis.cfg.5.html)

### Ticket#2019100410008431

- incident: xymon client not reporting on files,disk,cpu statuses

- cause: the machine's **hostname did not match that listed on the xymon server!!!**

- fix:

  - check on xymon server: `vim /home/xymon/server/etc/hosts.cfg` 

    - ensure the client's hostname match its corresponding entry exactl

  - restart xymon-client `systemctl restart xymon-client.service` 

  - restart xymon-server `service xymon restart` 


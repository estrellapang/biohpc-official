## ESXI Ops

### Networking 

- get active connections

  - `esxcli network ip interface ipv4 get` 

  - e.g.

```
Name  IPv4 Address    IPv4 Netmask   IPv4 Broadcast  Address Type  Gateway         DHCP DNS
----  --------------  -------------  --------------  ------------  --------------  --------
vmk0  172.18.239.201  255.255.240.0  172.18.239.255  STATIC        172.18.239.254     false
vmk1  172.18.239.204  255.255.240.0  172.18.239.255  STATIC        0.0.0.0            false
```

- list status of all NICs

  - `esxcli network nic list` 

  - e.g.

```
Name    PCI Device    Driver  Admin Status  Link Status  Speed  Duplex  MAC Address         MTU  Description
------  ------------  ------  ------------  -----------  -----  ------  -----------------  ----  ------------------------------------------------
vmnic0  0000:0a:00.0  i40en   Up            Down             0  Half    38:68:dd:2f:31:40  1500  Intel(R) Ethernet Connection X722 for 10GbE SFP+
vmnic1  0000:0a:00.1  i40en   Up            Down             0  Half    38:68:dd:2f:31:41  1500  Intel(R) Ethernet Connection X722 for 10GbE SFP+
vmnic2  0000:0a:00.2  i40en   Up            Up           10000  Full    38:68:dd:2f:31:42  1600  Intel(R) Ethernet Connection X722 for 10GbE SFP+
vmnic3  0000:0a:00.3  i40en   Up            Down             0  Half    38:68:dd:2f:31:43  1600  Intel(R) Ethernet Connection X722 for 10GbE SFP+
vusb0   Pseudo        cdce    Up            Up             100  Full    3a:68:dd:2f:31:47  1500  IBM XClarity Controller
```

\
\

- **iperf3** bandwidth testing

  - FIRST disable firewall: `esxcli network firewall set --enabled false`

  - on receiving/server end: `iperf3 -s`, one can alternatively specify the IP to receive packets from with `-B <I.P. ADDR>`

  - on client 1. go to utilitilies dir: `cd /usr/lib/vmware/vsan/bin/`

  - on cleint 2. ` ./iperf3 -c 172.18.224.6 -t 10 -V` ('-t' secs to finish test, '-V' verbose)  

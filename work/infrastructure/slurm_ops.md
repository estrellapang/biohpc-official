## SLURM COMMANDS

### root privilege

- Append to admin node's $PATH: `export PATH="$PATH:/cm/shared/apps/slurm/16.05.8/bin"`

### important mounts

- slurmctld log: `mount -t nfs 10.100.160.1:/var/log /cm/log`

```
# /cm/shared & /cm/log

master.ib1.biohpc:/cm/shared on /cm/shared type nfs (rw,relatime,vers=3,rsize=1048576,wsize=1048576,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,mountaddr=10.100.160.1,mountvers=3,mountport=4002,mountproto=udp,local_lock=none,addr=10.100.160.1)
10.100.160.1:/var/log on /cm/log type nfs (rw,relatime,vers=3,rsize=1048576,wsize=1048576,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,mountaddr=10.100.160.1,mountvers=3,mountport=4002,mountproto=udp,local_lock=none,addr=10.100.160.1)
```


### Check Node Info

```
# pull list of all nodes
`sinfo --Node | awk '{print $1}' >> slurmRaw.txt`

# summarize all node numbers and partitions
-------------------------
sinfo -s 
-------------------------

# look at only available nodes
-------------------------
sinfo -t idle
-------------------------

# look at only available nodes in a specific partition
-------------------------
sinfo -p <partition name> -t idle

  # e.g. fetch all available nodes from nucleus series nodes

  sinfo -p super -t idle 
-------------------------

# see how many nodes per user
-------------------------
`sacct -r <partition> -s running —format=User,nnodes | sort | uniq -c`

# doesn't sacct account for past info?
-------------------------

# see what nodes each user is using
-------------------------
`squeue -p <partition>`
-------------------------

# check a partition to see WHICH nodes will finish their jobs first

----------------------------------------------------------
squeue -p <partition> --Format=nodelist,timeleft,username

  ## pipe into "grep -v <uid>" if we want to avoid draining nodes allocated by specific user
----------------------------------------------------------

# see if jobs are running on a specific node
----------------------------------------------------------
squeue --nodelist <node name>

  e.g. `squeue --nodelist 92512 -o "%A %j %C %J"`

[reference](https://unix.stackexchange.com/questions/381981/check-cpu-thread-usage-for-a-node-in-the-slurm-job-manager)
----------------------------------------------------------

# fetch details on specific node/a range of nodes
----------------------------------------------------------
scontrol show node <nodename>  # case sensitive

scontrol show node=Nucleus[###-###,###-###]
----------------------------------------------------------

```



### Drain/Resume Nodes

```
### DRAIN: 
scontrol update nodeName=NucleusA[###-###] state=drain reason="Xeon Phi"

  # square brackets only work sequentially; not non-sequential groups, use bashcripts below

 ## Non-Sequential Drain e.g.
---------------------------------------------------------------------------------------
scontrol update Nodename=NucleusA[110-113,118-121] state=drain reason="Xeon Phi Removal"
---------------------------------------------------------------------------------------

### UNDRAIN:
scontrol update nodeName=NucleusA[###-###] state=resume

```

\
\
\

### Bashscript Implementations

```
## Drain MULTIPLE NODES SEQUENTIAL ORDER ONLY!!!

-----------------------------------------------------------------------------------------------------
for i in $(seq -w ### ### ####); do scontrol update nodeName=NucleusC$i state=drain reason="test";done

  # `-w` handle is to not ignore 0s in the numbering!
-----------------------------------------------------------------------------------------------------

```


\
\
\

### Job Operations

- logs stored here: `/cm/logs/slurmctld` 

```
## Check all jobs by user
-------------------------
# this checks CURRENT jobs
squeue --user <uid>

# check ALL of users jobs since a certain time
sacct -aS 2020-03-25T19:00:00 --format=User,JobID,Start,State,NNodes,NodeList,Partition | grep -i <uid>
-------------------------

## Fetch info about jobs
------------------------
scontrol show jobid <uid>
-------------------------

## Cancel specific job
-------------------------
scancel <jobID>

# with KILL signal

scancel --signal=KILL <jobID>
-------------------------

## Cancel all of a user's job
-------------------------
scancel -u <uid>

# cancel all of a user's PENDING jobs

sscancel -t PENDING -u <uid>
-------------------------

## View all job info by user
-------------------------
sacct -u <username> --format=JobID,JobNanme,NodeList,Start,End,State,AllocCPUS,ExitCode,etc. 
-------------------------

## View info on specific job
-------------------------
sacct -j <jobID>
-------------------------


## return job details during specified time internvals
-------------------------
sacct -aS <yr-mo-d>T<hr:min:sec> -E <yr-mo-d>T<hr:min:sec>
-------------------------

# e.g. see failed/cancelled jobs
-------------------------
sacct -aS 2019-11-16T23:30:00 -E 2019-11-17T02:50:00 | grep -i FAILED
sacct -aS 2019-11-16T23:30:00 -E 2019-11-17T02:50:00 | grep -i CANCELLED
-------------------------

# see job fails since specified time
-------------------------
# acceptable formats:

sacct -aS <year>-<month>-<day>T<hr:min:sec>
sacct -aS YYYY-MM_DD 
sacct -aS 2020-01-09T11:00:00 | grep -i FAILED

 ## the [MMDDYY] format also works very well

# see job failures with CUSTOMIZED FORMAT: #of nodes, start time, username, and partition

-----------------------------------------------------------
 sacct -aS 2020-03-25T12:00:00 --format=User,JobID,Start,State,NNodes,NodeList,Partition | grep -i failed

e.g. of output: if we started an maintenance at 12:00 P.M. techinically the REAL affected jobs will be the first two

    mtang 1635693      2020-03-25T12:02:00     FAILED        1      Nucleus015      super 
          1635693.bat+ 2020-03-25T12:02:00     FAILED        1      Nucleus015            
  s181706 1635760      2020-03-25T11:56:02     FAILED        1      Nucleus017      super 
          1635760.bat+ 2020-03-25T11:56:02     FAILED        1      Nucleus017            
  s430614 1635797      2020-03-25T12:44:22     FAILED        1      Nucleus039      256GB 
          1635797.bat+ 2020-03-25T12:44:22     FAILED        1      Nucleus039            
  s190450 1635811      2020-03-25T12:45:22     FAILED        1      Nucleus029      super 
          1635821.1    2020-03-25T13:02:26     FAILED        1      Nucleus113            
          1635821.2    2020-03-25T13:03:50     FAILED        1      Nucleus113            
          1635821.3    2020-03-25T13:04:11     FAILED        1      Nucleus113            
  s430614 1635825      2020-03-25T13:12:04     FAILED        1      Nucleus036      256GB 
          1635825.bat+ 2020-03-25T13:12:04     FAILED        1      Nucleus036         
-----------------------------------------------------------
-------------------------

\
\

# see failed jobs and sort out affected users
-----------------------------------------------------------
sacct -aS 2020-04-08T12:00:00 -E 2020-04-08T12:40:00 --format=User,JobID,State,NNodes,Partition | grep -i FAILED | awk '//{print $1}' | grep -Fv 'bat' | grep -Fv '.0' | sort -u
-----------------------------------------------------------

\
\

# see failed jobs since specific time, and output only jobIDs -- for accounting total amount of failed jobs
-----------------------------------------------------------
 sacct -aS 2020-08-27T14:00:00 --format=User,JobID,Start,State,NNodes,NodeList,Partition | grep -i failed | awk '//{print $2}' | sort -u | grep -v '<year #>'

   # on the last pipe we exclude year number because the jobIB field is somehow counted to be the same column as their start dates
-----------------------------------------------------------

\
\

# see user job details SINCE start day
--------------------------------------------------------------------------
sacct -aS MMDDYY -u <uid> --format=User,JobID,JobName,ExitCode,Start,State

  # `man sacct` to see more --format options

  # if `-u` handle is not specified, jobs for all users would be displayed

  # ExitCode Format: <exit code>:<termination signals>

    # [forum link on slurm exit codes](https://bugs.schedmd.com/show_bug.cgi?id=3214)

    # [unix signal exit and terminal signals 1: USERFUL MAN HINT](https://sites.google.com/a/case.edu/hpc-upgraded-cluster/cluster-faq/running-jobs/exit-code-status)

    # [unix exit codes](https://www.tldp.org/LDP/abs/html/exitcodes.html)
 
--------------------------------------------------------------------------

\
\

# extend a users' jobs
--------------------------------------------------------------------------
scontrol update jobid=1396123 EndTime=2019-09-30T05:21:35

scontrol show jobid 1396123

RunTime=02:32:22 TimeLimit=4-20:00:00 TimeMin=N/A
   SubmitTime=2019-09-25T09:21:34 EligibleTime=2019-09-25T09:21:34
   StartTime=2019-09-25T09:21:34 EndTime=2019-09-30T05:21:35

  ## done--> "TimeLimit" is a good indicator of duration
--------------------------------------------------------------------------

\
\

# jobs that've been running longe than 2 weeks
--------------------------------------------------------------------------
squeue --states=running --Format=jobID,username,timeused | grep '-' | sed 's/-/ /g' | awk '{if($3 > 14) print $0}'
--------------------------------------------------------------------------

```

- **Rsources** 

- [best SLURM mamange commands](https://www.rc.fas.harvard.edu/resources/documentation/convenient-slurm-commands/)

- [SLURM cheatsheet](http://www.physik.uni-leipzig.de/wiki/files/slurm_summary.pdf)

- [additional basic SLURM management commands](https://www.hpc.caltech.edu/documentation/slurm-commands)

- [topology of cmds with respect to SLURM components](https://slurm.schedmd.com/quickstart.html)

- [SLURM job debug](https://slurm.schedmd.com/faq.html)

- [job modifications](https://wiki.hpcc.msu.edu/pages/viewpage.action?pageId=20119995)

- [sbatch stackoverflow](https://stackoverflow.com/questions/43954469/how-to-submit-jobs-to-slurm-with-different-nodes)

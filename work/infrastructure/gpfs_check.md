## System Checks & Operations on GPFS

### General Filesystem Behaviors

- Max and Alloc Inodes

```
# from GPFSv4 Knowledge center

Inodes are allocated when they are used. When a file is deleted, the inode is reused, but inodes are never deallocated. When setting the maximum number of inodes in a file system, there is the option to preallocate inodes. However, in most cases there is no need to preallocate inodes because, by default, inodes are allocated in sets as needed. If you do decide to preallocate inodes, be careful not to preallocate more inodes than will be used; otherwise, the allocated inodes will unnecessarily consume metadata space that cannot be reclaimed.

```

  - [reference](https://www.ibm.com/support/knowledgecenter/STXKQY_4.2.0/com.ibm.spectrum.scale.v4r2.ins.doc/bl1ins_maxnfle.htm)


\
\

### Log Locations

```
# main gpfs event log
----------------------------
/var/adm/ras/mmfs.log.latest
----------------------------

# mmfsd database listing all config changes
----------------------------
/var/mmfs/gen/mmsdrfs
----------------------------
/

```

\
\

### Grab Filesystem Attributes

- check filesystem-specific parameters

  - `mmlsfs <fs_name>` 

  - `mmlsfs bassdata`

\

- query inode usage info

 - `mmlsfileset <device> <fileset> -i` 

 - `mmlsfileset <device> -i` outputs all inode usage for all filesets

 - [reference](https://www.ibm.com/support/knowledgecenter/en/STXKQY_5.0.4/com.ibm.spectrum.scale.v5r04.doc/bl1ins_maxnfle.htm)

 - [how to increase max inodes](https://www.ibm.com/support/knowledgecenter/en/STXKQY_5.0.4/com.ibm.spectrum.scale.v5r04.doc/bl1pdg_increasefilesetspace.htm)

\

- check configuration change history

  - e.g. `cat /var/mmfs/gen/mmsdrfs | grep -i pagepool`

\

- check mmfsd initialization settings

  - `mmlsconfig` --> stored to file `/var/mmfs/gen/mmfs.cfg`

  - e.g. `mmlsconfig pagepool`

  - e.g. `mmlsconfig pagepoolMaxPhysMemPct`

\

- check current NODE-SPECIFIC settings

  - `/usr/lpp/mmfs/bin/mmdiag --config`

  - e.g. `mmdiag --config | grep -i pagepool`

  - `cat /var/mmfs/gen/mmfsNodeData`

\

- check existing node classes

  - `mmlsnodeclass` 

  - see all node classes, including managers `mmlsnodeclass --all`

\
\

### Check Disk Status

```
ssh gpfsadmin@10.10.137.20 

[gpfsadmin@ems1 ~]$ sudo /usr/lpp/mmfs/bin/mmlspdisk all --not-ok
mmlspdisk: [I] No disks were found.
[gpfsadmin@ems1 ~]$ logout

```

### Check Recovery Group + Disk Status

```
# see general layout of recovery groups

mmlsrecoverygroup

# see specific recoverygroup and its disk status

mmlsrecoverygroup -L rg_gssio<1/2/3/4>-hs
```

- **disk replacement**

```
# check disks not okay

sudo /usr/lpp/mmfs/bin/mmlspdisk all --not-ok 


# release disk

sudo /usr/lpp/mmfs/bin/mmchcarrier rg_gssio<1-4>-ib --release --pdisk <disk location, e.g.e1d3s02>


# replace disk

sudo /usr/lpp/mmfs/bin/mmchcarrier rg_gssio<1-4>-ib --replace --pdisk '<disk location>`'


### NOTE: check recovery group number!!!
```

- [good article on GPFS filesystem management & details on NVR vdisks!](https://developer.ibm.com/storage/2018/01/02/ess-disk-management/)



\
\

### Filesystem Troubleshooting

#### Waiters and Deadlocks

- `mmdiag --deadlock`

- `mmdiag --waiters`

  - `grep -i 'waiter' /var/adm/ras/mmfs.log.latest`

- `mmlsnode -L -N waiters`

- [ibm knowledge base on deadlock management](https://www.ibm.com/docs/en/spectrum-scale/5.0.5?topic=troubleshooting-managing-deadlocks)

- [ibm debug slides 1](http://files.gpfsug.org/presentations/2016/anl-june/SpectrumScale_ProblemDetermination_06102016.pdf)

- [ibm debug slides 2](https://www.spectrumscaleug.org/wp-content/uploads/2020/04/SSSD20DE-Spectrum-Scale-Performance-Enhancements-for-Direct-IO.pdf)


\
\

### Quota Management 

```
### Check Quota

------------------------------------------------
mmlsquota -g/-u <gid/uid> --block-size <M/G/T> <device>:<fileset>

e.g.

mmlsquota -u zpang1 --block-size G bassdata:work

sudo /usr/lpp/mmfs/bin/mmlsquota --block-size T -g 9001 bassclient:backup
------------------------------------------------

\

### Set Quota

------------------------------------------------
/usr/lpp/mmfs/bin/mmsetquota <device>:<fileset> <--user/--group> --block <soft G/T>:<Hard G/T>

  e.g. 

mmsetquota bassdata:archive --group Bezprozvanny_lab --block 15T:17T
------------------------------------------------
```

\
\

### AFM Commands

- check if which filesets are linked via AFM relationship:

  - `mmlsfileset <device/filesystem name>`

  - `mmlsfileset bassdata`

- cmds for linking and unlinking home-cache clusters

```
mmafmctl bassdata stop -j backup
mmunlinkfileset bassdata work -f
mmunlinkfileset bassdata backup -f

mmlinkfileset bassdata archive -J /endosome/archive
mmlinkfileset bassdata work -J /endosome/work
mmlinkfileset bassdata backup -J /endosome/.backup
```

\
\

### Query Cluster Info & Configurations

\
\

- **Get clean node list**

  - ` sudo /usr/lpp/mmfs/bin/mmlscluster | awk '//{print $2}' | tail -n +12 | awk -F. '//{print $1}'`

  - the `-n +12` handle specifies at which line will the raw output be printed, here the first 11 lines are ignored

  - [reference](https://stackoverflow.com/questions/24542425/bash-how-to-remove-first-2-lines-from-output)

\

- **Eliminate Dead Nodes from Cluster**

  - `sudo /usr/lpp/mmfs/bin/mmgetstate -N all | grep -i 'unknown' | tee -a badNodes.txt`

  - `cat badNodes.txt | awk '{print $2}' | xargs -n1 -I% ping -c 3 %`

  - cleave away nodes that do not ping --> name file `excludeNodes.txt`

  - wheat-out nodes from raw mmlscluster node list: `cat excludeNodes.txt | xargs -n1 -I% sed -i "s/%//g" rawNodeList.txt`

  - delete empty spaces in `rawNodeList.txt` from previous operation: `cp rawNodeList.txt fineNodeList.txt` --> 

  -  --> `sed -i '/^$/d' fineNodeList.txt`
 
  - to compare: generate a brand new raw nodelist, then compare by: `diff -y -W 97 rawNodeList.txt fineNodeList.txt | grep -i '<' | awk '{print $1}' | sed '/^$/d' > badNodesMask.txt` --> this generates a list that can be compared to the initial `excludeNodes.txt` --> should be no differences except an extra `<` character

\
\

### GPFS Troubleshoot & Incidents

- 09/18/2020 - cannot access two specific subdirectories

- symptoms:

  - cannot ls, cannot rm

  - strace ls --> process stuck on getdent() system call

- cause:

  - C-series (C065) had to be rebooted---node detected to cause deadlocks --> locks not released for the 2 inodes previously inaccessible
  

- related links

  - https://unix.stackexchange.com/questions/53224/cannot-ls-mnt-directory 

  - https://serverfault.com/questions/367438/ls-hangs-for-a-certain-directory

  - https://unix.stackexchange.com/questions/120077/the-ls-command-is-not-working-for-a-directory-with-a-huge-number-of-files

  - https://github.com/openzfs/zfs/issues/5346

  - https://github.com/openzfs/zfs/issues/4583

\
\

### GPFS disk topology & recovery group check

```
# check emails from Craig Rose of IBM

[EMS]# xdsh gss_ppc64 "mmgetpdisktopology > /tmp/mmgetpdisktopology.2020-12-17.`hostname -s`"
[EMS]# xdsh gss_ppc64 "topsummary /tmp/mmgetpdisktopology.2020-12-17.`hostname -s` > /tmp/topsummary.2020-12-17.`hostname -s`"
[EMS]# xdsh gss_ppc64 "cat /tmp/topsummary.2020-12-17.`hostname -s`" | xcoll
[EMS]# for i in `mmlsrecoverygroup | grep rg | awk '{print $1}'`; do echo "+++ $i +++"; mmlsrecoverygroup $i -L --pdisk | grep -v '2, 4'; echo; done
```

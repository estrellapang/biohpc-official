## Firmware Management for Workstations and Servers

## Pull Hardware Info

- dmidecode

```
# get server manufacturer and model
--------------
dmidecode -t 1
--------------

# get machine's Memory hardware
--------------
dmidecode --type 17
--------------
```

- links:

  - [tutorial](https://www.tecmint.com/how-to-get-hardware-information-with-dmidecode-command-on-linux/)

\

- check CPU temperatures from the OS

  - [tutorial](https://www.2daygeek.com/view-check-cpu-hard-disk-temperature-linux/)

\
\

### System Info Commands(Linux)


- **Check Logs**

  - `dmesg` 

  - `/var/log/boot.log`

- [resource 1](https://www.dedoimedo.com/computers/linux-hardware-troubleshooting.html)

\
\

- See RAID Controller + Correponding Kernel Module

  - [lspci output fields explained](https://www.linuxnix.com/find-hardware-details-using-lspci-command-in-linux/) 

```
------------------------
lspci -vv | grep -i raid
------------------------

# e.g. of output

02:00.0 RAID bus controller: LSI Logic / Symbios Logic MegaRAID SAS-3 3008 [Fury] (rev 02)
	Kernel driver in use: megaraid_sas
	Kernel modules: megaraid_sas
```

- Correlate Bus Address to PCI slot numbers:

```
# see all available slots:
------------------------
dmidecode –t slot 
------------------------

# list device connected to slot:
------------------------
lspci -s <bus addr obtained from command above>
------------------------
```

- Fetch Hardware info on GPU Cards:

  -  [resource 1](https://www.binarytides.com/linux-get-gpu-information/)


\
\
\

### General Practice

- **Have CONSOLE ACCESS!**

  - via IPMI or Physical Terminal



### Dell R630 

- **iDRAC 8 General**

  - [generate Tech Support Report](https://www.youtube.com/watch?v=G30f8Y3BPck)

- **Updating via iDRAC**

  - [instructions](https://www.dell.com/support/article/en-us/sln292363/update-dell-poweredge-servers-firmware-remotely-using-the-idrac?lang=en)

  - [comptability of linux drivers with Dell RAID controllers](https://www.dell.com/support/article/en-us/sln312210/linux-raid-and-storage?lang=en)

  - [product support page for firmware packages](https://www.dell.com/support/home/us/en/04/product-support/product/poweredge-r630/drivers)

\
\

- **Updating via BootISO**

  - [general guide](https://www.dell.com/support/article/en-us/sln296511/update-poweredge-servers-with-platform-specific-bootable-iso?lang=en)

  - [via iDRAC's virtual media](https://www.dell.com/support/article/en-us/sln296648/using-the-virtual-media-function-on-idrac-6-7-8-and-9?lang=en)

\
\

- **Adding Hot Spares via iDRAC**

  - [Support Knowledge Base Instructions](https://www.dell.com/support/article/en-us/sln305775/dell-poweredge-how-to-assign-a-hard-drive-in-global-hot-spare?lang=en)

  - [reminder to convert disk to RAID mode](https://www.dell.com/community/PowerEdge-HDD-SCSI-RAID/Assistance-Adding-Hot-Spare/m-p/4164054#M33074)

  - [questions on reboot](https://www.dell.com/community/PowerEdge-HDD-SCSI-RAID/Do-we-really-need-to-reboot-to-add-a-hot-spare/td-p/5044220)

  - [info on warning from converting new disk from non-RAID to RAID](https://www.dell.com/community/Rack-Servers/Drive-replacement-showing-as-Non-RAID-on-R520-with-PERC-H310/td-p/6092445)

  - [info on RAID arrays with varying disk capacities](https://www.quora.com/Do-all-the-hard-drives-in-a-NAS-have-to-be-the-same-size-such-as-a-4TB-next-to-an-8TB?share=1)

 
\
\

### HPE Apollo r2600 Chassis + Proliant XL170r Gen9 Servers

- cmds:

```
# fetch serial numbers

cat /sys/devices/virtual/dmi/id/*

# check bios firmware

dmidecode -t bios -q 

```
- [demidecode firmware tutorial](https://www.linuxnix.com/how-to-get-bios-firmware-and-installed-drivers-details/)

- [check product type + warranty based on serial number](https://support.hpe.com/hpsc/wc/public/home)

- [flash/binary download for BIOs-System ROM firmware for iLOs](https://support.hpe.com/hpsc/swd/public/detail?swItemId=MTX_32cf199c94a74b1aa53a665c76#tab2)

- [hex download for chassis power management iLOS](https://support.hpe.com/hpsc/swd/public/detail?swItemId=MTX_e18d4f696ed04c4682366c0075&swEnvOid=54#tab2​) 

- [hpe support search product name for downloads](https://support.hpe.com/hpesc/public/home)
```

- iLOS op:

 - Administration --> Firmware --> Upload --> Reboot

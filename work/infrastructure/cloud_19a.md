## Operation Logs on cloud.biohpc.swmed.edu

### Resources on Database Ops:

- [enable NextCloud's mem cache](https://docs.nextcloud.com/server/16/admin_manual/configuration_server/caching_configuration.html#id1)

- [enable occ command atlas](https://docs.nextcloud.com/server/16/admin_manual/configuration_server/occ_command.html?highlight=occ%20command#) 

- [LDAP user cleanup](https://docs.nextcloud.com/server/16/admin_manual/configuration_user/user_auth_ldap_cleanup.html)

- [php memeory](https://help.nextcloud.com/t/the-php-memory-limit-is-below-the-recommended-value-of-512mb/42719/2)

- [bioHPC change-log](https://docs.biohpc.swmed.edu/dokuwiki/doku.php?id=file_exchange_cloud_change_log)


### Aug. 22nd, 2019: Space Shortage in `/` 

```
[root@cloud owncloud]# df -Th 
Filesystem           Type   Size  Used Avail Use% Mounted on
/dev/mapper/vg_cloudtestdmz-lv_root
                     ext4    50G   30G   18G  63% /
tmpfs                tmpfs   30G     0   30G   0% /dev/shm
/dev/sda1            ext4   477M  114M  338M  26% /boot
/dev/mapper/vg_cloudtestdmz-lv_home
                     ext4    45G   14G   29G  32% /home
/dev/mapper/cloud_vg-cloud_data
                     ext4   3.5T  2.7T  609G  82% /var/www/owncloud/data

# nextcloud dumps ~0.55G of .sql backup per day into `/var/www/owncloud/DB_backup` 

# if `/var/www/owncloud` becomes FULL --> the http server will TIMEOUT! 

# current status:

-------------------------------------------------------------------------------------
[root@cloud owncloud]# cd DB_backup/
[root@cloud DB_backup]# ll
total 14174056
-rw-r--r-- 1 backup_services biohpc_admin 625170644 Aug  1 03:30 db_20190801033009.sql
-rw-r--r-- 1 backup_services biohpc_admin 625985215 Aug  2 03:30 db_20190802033006.sql
-rw-r--r-- 1 backup_services biohpc_admin 626446374 Aug  3 03:30 db_20190803033003.sql
-rw-r--r-- 1 backup_services biohpc_admin 626672898 Aug  4 03:30 db_20190804033009.sql
-rw-r--r-- 1 backup_services biohpc_admin 626613547 Aug  5 03:30 db_20190805033003.sql
-rw-r--r-- 1 backup_services biohpc_admin 628754869 Aug  6 03:30 db_20190806033002.sql
-rw-r--r-- 1 backup_services biohpc_admin 630203031 Aug  7 03:30 db_20190807033004.sql
-rw-r--r-- 1 backup_services biohpc_admin 631790245 Aug  8 03:30 db_20190808033009.sql
-rw-r--r-- 1 backup_services biohpc_admin 621279248 Aug  9 03:30 db_20190809033004.sql
-rw-r--r-- 1 backup_services biohpc_admin 632546710 Aug 10 03:30 db_20190810033003.sql
-rw-r--r-- 1 backup_services biohpc_admin 632900789 Aug 11 03:30 db_20190811033002.sql
-rw-r--r-- 1 backup_services biohpc_admin 625813990 Aug 12 03:30 db_20190812033003.sql
-rw-r--r-- 1 backup_services biohpc_admin 627382634 Aug 13 03:30 db_20190813033003.sql
-rw-r--r-- 1 backup_services biohpc_admin 613110644 Aug 14 03:30 db_20190814033003.sql
-rw-r--r-- 1 backup_services biohpc_admin 613467437 Aug 15 03:30 db_20190815033004.sql
-rw-r--r-- 1 backup_services biohpc_admin 613899203 Aug 16 03:30 db_20190816033005.sql
-rw-r--r-- 1 backup_services biohpc_admin 612712973 Aug 17 03:30 db_20190817033004.sql
-rw-r--r-- 1 backup_services biohpc_admin 612888031 Aug 18 03:30 db_20190818033002.sql
-rw-r--r-- 1 backup_services biohpc_admin 612980129 Aug 19 03:30 db_20190819033002.sql
-rw-r--r-- 1 backup_services biohpc_admin 613365421 Aug 20 03:30 db_20190820033001.sql
-rw-r--r-- 1 backup_services biohpc_admin 182796288 Aug 21 03:30 db_20190821033001.sql
-rw-r--r-- 1 backup_services biohpc_admin 607358565 Aug 22 03:30 db_20190822033002.sql
-rw-r--r-- 1 backup_services biohpc_admin 635766230 May 28 03:30 db_current.sql
-rw-r--r-- 1 backup_services biohpc_admin 634180953 May 24 10:59 db_.sql

-------------------------------------------------------------------------------------


### SOLUTION: create cronjob: become `backup_services` user & delete `.sql` dumps older than 21 days 

[root@cloud DB_backup]# su -m backup_services
bash: /root/.bashrc: Permission denied
bash-4.1$ 
bash-4.1$ 
bash-4.1$ crontab -l
0 5 * * 3,6 cp /var/www/owncloud/data/data/nextcloud.log /home/backup_services/cloud_log/nextcloud.log."`date +%m%d%Y`"


bash-4.1$ crontab -e
crontab: installing new crontab


-------------------------------------------------------------------------------------

# remove `.sql` dumps older than 21 days: every Sun@3:00P.M.

0 15 * * sun /bin/find /var/www/owncloud/DB_backup -mtime +21 -exec rm {} \;  
-------------------------------------------------------------------------------------


crontab: installing new crontab


# Update: Aug. 26th, 2019 

-------------------------------------------------------------------------------------

[root@cloud biohpcadmin]# ls -lht /var/www/owncloud/DB_backup/
total 13G 
-rw-r--r-- 1 backup_services biohpc_admin 582M Aug 25 03:30 db_20190825033001.sql
-rw-r--r-- 1 backup_services biohpc_admin 582M Aug 24 03:30 db_20190824033002.sql
-rw-r--r-- 1 backup_services biohpc_admin 580M Aug 23 03:30 db_20190823033003.sql
-rw-r--r-- 1 backup_services biohpc_admin 580M Aug 22 03:30 db_20190822033002.sql
-rw-r--r-- 1 backup_services biohpc_admin 175M Aug 21 03:30 db_20190821033001.sql
-rw-r--r-- 1 backup_services biohpc_admin 585M Aug 20 03:30 db_20190820033001.sql
-rw-r--r-- 1 backup_services biohpc_admin 585M Aug 19 03:30 db_20190819033002.sql
-rw-r--r-- 1 backup_services biohpc_admin 585M Aug 18 03:30 db_20190818033002.sql
-rw-r--r-- 1 backup_services biohpc_admin 585M Aug 17 03:30 db_20190817033004.sql
-rw-r--r-- 1 backup_services biohpc_admin 586M Aug 16 03:30 db_20190816033005.sql
-rw-r--r-- 1 backup_services biohpc_admin 586M Aug 15 03:30 db_20190815033004.sql
-rw-r--r-- 1 backup_services biohpc_admin 585M Aug 14 03:30 db_20190814033003.sql
-rw-r--r-- 1 backup_services biohpc_admin 599M Aug 13 03:30 db_20190813033003.sql
-rw-r--r-- 1 backup_services biohpc_admin 597M Aug 12 03:30 db_20190812033003.sql
-rw-r--r-- 1 backup_services biohpc_admin 604M Aug 11 03:30 db_20190811033002.sql
-rw-r--r-- 1 backup_services biohpc_admin 604M Aug 10 03:30 db_20190810033003.sql
-rw-r--r-- 1 backup_services biohpc_admin 593M Aug  9 03:30 db_20190809033004.sql
-rw-r--r-- 1 backup_services biohpc_admin 603M Aug  8 03:30 db_20190808033009.sql
-rw-r--r-- 1 backup_services biohpc_admin 602M Aug  7 03:30 db_20190807033004.sql
-rw-r--r-- 1 backup_services biohpc_admin 600M Aug  6 03:30 db_20190806033002.sql
-rw-r--r-- 1 backup_services biohpc_admin 598M Aug  5 03:30 db_20190805033003.sql
-rw-r--r-- 1 backup_services biohpc_admin 598M Aug  4 03:30 db_20190804033009.sql

# Notice that the `.sql` dumps from May '19 were removed`

-------------------------------------------------------------------------------------


### 09.20.19:

- response to ticket#2019092010008037

- **wrote bash script to auto delete remnant users** 

  - [reference link for script](https://help.nextcloud.com/t/oc-user-nouserexception/19051/4) 

- location of script:

  - `/var/www/owncloud/html/orphan_sweep.sh`

  - contents below:

```
#!/bin/bash

for i in $(sudo -u apache php occ ldap:show-remnants | awk -F '|' '{print $2}'); do

	echo "Deleting Remnant User: $i"
	sudo -u apache php occ user:delete $i

done

```

- following users deleted:

```
Deleting Remnant User: 008bf3cb-5bb9-4aca-8591-c7e2ead4f5cf
The specified user was deleted
Deleting Remnant User: 00a984a6-c467-4193-804e-57acaa16f286
The specified user was deleted
Deleting Remnant User: 00f65b9f-b8a9-499f-b8d6-9683b1d13a35
The specified user was deleted
Deleting Remnant User: 00fa8975-3918-4ddb-bba4-ed85698d69b8
The specified user was deleted
Deleting Remnant User: 014a0850-ac94-4259-8964-e0af2d2c9066
The specified user was deleted
Deleting Remnant User: 02a8c078-31fc-47a8-b414-23a504d1dd5a
The specified user was deleted
Deleting Remnant User: 03decdab-bcf3-4849-a42e-25e636e16089
The specified user was deleted
Deleting Remnant User: 048ceab5-c04a-41f2-bb97-3b38b6846529
The specified user was deleted
Deleting Remnant User: 04f0cb10-b161-4b99-bffe-df87b7a24e32
The specified user was deleted
Deleting Remnant User: 04ff8ffa-2ef3-4500-9f24-8c0915189f03
The specified user was deleted
Deleting Remnant User: 0653d7bc-a8a2-42cd-9ffc-097f5795a4c5
The specified user was deleted
Deleting Remnant User: 06ed4d66-568b-44fc-b9e5-abf8ee0fbe05
The specified user was deleted
Deleting Remnant User: 07616665-58b1-4aa4-856e-449eb92ece85
The specified user was deleted
Deleting Remnant User: 083763f1-b931-4b67-a980-17d15402d807
The specified user was deleted
Deleting Remnant User: 08de4a15-ccf7-4720-8ec0-5825cd5e2d4e
The specified user was deleted
Deleting Remnant User: 08fdecb7-cf3e-4ec9-9792-51898b162d11
The specified user was deleted
Deleting Remnant User: 0b869a71-a9a4-4868-84c4-d4fb930aa582
The specified user was deleted
Deleting Remnant User: 0c05f26a-bb0b-4c93-8096-08b047a56905
The specified user was deleted
Deleting Remnant User: 0c405303-6db3-436e-8b26-b92e146293a4
The specified user was deleted
Deleting Remnant User: 0c635cc4-f6c5-4c39-b1c2-705417f99044
The specified user was deleted
Deleting Remnant User: 0ca0e5ea-5bdf-4904-8a1c-2f0afbfb890b
The specified user was deleted
Deleting Remnant User: 0db8a87b-ac69-44fe-afdb-36620e155123
The specified user was deleted
Deleting Remnant User: 0dc6f558-da03-456f-9d6d-d04721ab8153
The specified user was deleted
Deleting Remnant User: 0e42396f-1ad9-4683-8218-d0e8963ddde9
The specified user was deleted
Deleting Remnant User: 0efb6ce9-24f9-4287-86b7-f14532a4b1c8
The specified user was deleted
Deleting Remnant User: 0f36a512-182e-428a-9f49-b1ca9cca3dde
The specified user was deleted
Deleting Remnant User: 106849b4-8d28-4bed-8bb2-bc181955d8c9
The specified user was deleted
Deleting Remnant User: 10c6eacd-b6ab-4809-a2d9-5c12ac69eb7d
The specified user was deleted
Deleting Remnant User: 122c0397-2447-4579-9ca9-2fb54eec3eb5
The specified user was deleted
Deleting Remnant User: 12b022c7-b5bd-489a-8af3-7a884fee41fd
The specified user was deleted
Deleting Remnant User: 13bf1b5b-f403-444b-bbec-55d0e576ccf8
The specified user was deleted
Deleting Remnant User: 14b32b7d-9f31-4212-a5dd-1fe2aa952e7d
The specified user was deleted
Deleting Remnant User: 14c95c6c-f7d4-4ce8-b14c-d38f804287aa
The specified user was deleted
Deleting Remnant User: 14d37516-9bf4-4c68-b5df-0d7a7b28a59f
The specified user was deleted
Deleting Remnant User: 158bff48-d02f-459c-b13b-601a6907eb09
The specified user was deleted
Deleting Remnant User: 15dfb927-4c39-4ada-92ce-66f516790002
The specified user was deleted
Deleting Remnant User: 15ee46cc-01f8-4dfb-81c0-a77d918d1c69
The specified user was deleted
Deleting Remnant User: 15eec75d-1142-4c1f-9302-2fe03472df05
The specified user was deleted
Deleting Remnant User: 1673cd63-8cd5-4762-be5c-cd14b1001718
The specified user was deleted
Deleting Remnant User: 16dfb838-c92f-4a2a-b687-917be25ef78c
The specified user was deleted
Deleting Remnant User: 16f7706f-a72c-42ed-a851-3be3f1b05b9f
The specified user was deleted
Deleting Remnant User: 177edff8-64fb-40fe-8625-2a1d6716a060
The specified user was deleted
Deleting Remnant User: 18ed4b86-31ef-41dc-8063-0a7b47e9bc04
The specified user was deleted
Deleting Remnant User: 19afa740-37ee-4ca2-a5c8-998c5675d449
The specified user was deleted
Deleting Remnant User: 1a1db985-d152-4bf8-89e9-fee119aae46d
The specified user was deleted
Deleting Remnant User: 1a92bf60-1993-4a71-acf7-67f1722340db
The specified user was deleted
Deleting Remnant User: 1b4d631b-939c-4b9b-a68a-9fc50e389281
The specified user was deleted
Deleting Remnant User: 1bad82ec-3941-486e-bc00-cae81821a37f
The specified user was deleted
Deleting Remnant User: 1d664eb1-0b27-424c-ad2c-f5d769a00514
The specified user was deleted
Deleting Remnant User: 1db64b29-9cec-400c-98a9-4536cdada2a0
The specified user was deleted
Deleting Remnant User: 1ea5018c-775e-40e1-b4c9-3d02b13aabd2
The specified user was deleted
Deleting Remnant User: 1ef41702-d442-472a-b90e-853432928c7f
The specified user was deleted
Deleting Remnant User: 1f483960-61f1-4e0f-be63-ae7199da13e6
The specified user was deleted
Deleting Remnant User: 1fa1b72e-a16f-4c8f-9d02-5e4ddf94e9be
The specified user was deleted
Deleting Remnant User: 2013340c-d578-4839-a995-d2d01bf19741
The specified user was deleted
Deleting Remnant User: 2040316c-3ce7-4b37-a886-501782280ecb
The specified user was deleted
Deleting Remnant User: 204e7262-3e2b-4d62-b1b2-6b6bd103ef7b
The specified user was deleted
Deleting Remnant User: 2050709d-934d-4663-b11b-9f2b8c4b4870
The specified user was deleted
Deleting Remnant User: 207f9c71-a7a8-4965-ac06-caf363e53de2
The specified user was deleted
Deleting Remnant User: 20acdc9a-6c6e-4f27-b56b-7520328dc672
The specified user was deleted
Deleting Remnant User: 2194ab62-9792-4a65-8664-9cc62047c7d2
The specified user was deleted
Deleting Remnant User: 2207d41e-8c2b-4318-9b61-fd6bc07762fa
The specified user was deleted
Deleting Remnant User: 227eeb79-3a01-465a-a56d-3122804f0276
The specified user was deleted
Deleting Remnant User: 236d938a-dd14-4899-bf99-d3fdebdf4e3d
The specified user was deleted
Deleting Remnant User: 23a7440c-f46e-4610-b615-e0ba16aedc64
The specified user was deleted
Deleting Remnant User: 2560b64c-c9bb-458f-94b0-121155d7138d
The specified user was deleted
Deleting Remnant User: 26357625-b8bd-44ec-9f64-ad796879029c
The specified user was deleted
Deleting Remnant User: 265a2db9-6ac6-4f6f-abf8-4e7d60368072
The specified user was deleted
Deleting Remnant User: 266bafe7-4801-4c47-b112-44d746bd63fa
The specified user was deleted
Deleting Remnant User: 27d27a75-0562-4b4a-8180-492f18bb4185
The specified user was deleted
Deleting Remnant User: 27d41835-b4f1-4b16-ac82-73b2289292dc
The specified user was deleted
Deleting Remnant User: 27e67398-177e-4ec4-af56-9ecbb620c925
The specified user was deleted
Deleting Remnant User: 28606516-538c-4b61-a803-7477c1073e37
The specified user was deleted
Deleting Remnant User: 2894fd3a-151e-4799-8587-fb9db153ceea
The specified user was deleted
Deleting Remnant User: 291f749a-e4f3-4ad9-97ef-559e566f0908
The specified user was deleted
Deleting Remnant User: 29510fb3-3c4e-4b24-9a9b-331c88d5d051
The specified user was deleted
Deleting Remnant User: 295d4cb0-80e9-4370-9e8d-f2e56c311fd4
The specified user was deleted
Deleting Remnant User: 29f0024f-8d4c-4c4f-ad64-f7db76626f95
The specified user was deleted
Deleting Remnant User: 2a513211-9050-461e-ab9a-8fdd54331788
The specified user was deleted
Deleting Remnant User: 2aa5b53d-2a45-4f33-8a06-fce14762d745
The specified user was deleted
Deleting Remnant User: 2b924490-b9b6-415b-a145-9ed76557f53b
The specified user was deleted
Deleting Remnant User: 2b9b764f-9eda-47b7-a3e8-e92dad59a391
The specified user was deleted
Deleting Remnant User: 2c92cd49-85f3-4903-aa39-2640843f4366
The specified user was deleted
Deleting Remnant User: 2cefc162-cb03-4e99-a505-6045f8a514b2
The specified user was deleted
Deleting Remnant User: 2db9a7e5-639b-484f-842c-a0ff9f7c1bb2
The specified user was deleted
Deleting Remnant User: 2f541f8a-9c68-40fd-9bb5-f94a6e5ec5e4
The specified user was deleted
Deleting Remnant User: 2f95fd93-7e89-485a-af22-34660ba6c7b4
The specified user was deleted
Deleting Remnant User: 2ffd039e-7479-428b-98a2-8fdff66a91ae
The specified user was deleted
Deleting Remnant User: 302029ea-fbb7-4878-b742-03750393766e
The specified user was deleted
Deleting Remnant User: 31a6fb90-23b6-4ca4-b093-7eecefdd8715
The specified user was deleted
Deleting Remnant User: 31fca5d1-25da-4e01-9b68-c705b37ce1f5
The specified user was deleted
Deleting Remnant User: 330229bc-aad3-44b8-9df9-f8c39453c739
The specified user was deleted
Deleting Remnant User: 35a095f3-06ba-4543-a37d-bad01adf1ed6
The specified user was deleted
Deleting Remnant User: 36221482-6e00-45f2-be56-7ec121ddca29
The specified user was deleted
Deleting Remnant User: 36cc2d27-e2db-43f3-9039-5192fc9d3ebd
The specified user was deleted
Deleting Remnant User: 377f7378-717a-4b37-ac37-12bf3ee98c4c
The specified user was deleted
Deleting Remnant User: 379ae01f-763a-4b70-bc67-9ccc6f7f7fc6
The specified user was deleted
Deleting Remnant User: 37e1d75e-9c7a-44ba-9779-642c09c8ca62
The specified user was deleted
Deleting Remnant User: 380d3e19-b713-4135-81c9-056f14a1bf04
The specified user was deleted
Deleting Remnant User: 385a0839-2ca7-453a-8714-38d63fd58e43
The specified user was deleted
Deleting Remnant User: 3879ba43-b2b3-427c-9b3d-7b2d0e453fc4
The specified user was deleted
Deleting Remnant User: 39d483e7-9d89-495f-bc01-3274d07a1b79
The specified user was deleted
Deleting Remnant User: 39feba69-13ba-40e3-b2fb-9238df4cab52
The specified user was deleted
Deleting Remnant User: 3c039816-e23b-4cdd-badd-395e7f87bc9b
The specified user was deleted
Deleting Remnant User: 3cc6640b-1164-418f-98b5-1742d4dc15dd
The specified user was deleted
Deleting Remnant User: 3ccb0055-5d57-4bfc-b118-df960fb936fb
The specified user was deleted
Deleting Remnant User: 3cf65e20-e043-454d-b1ea-cc4b6f5c0a98
The specified user was deleted
Deleting Remnant User: 3deb3060-591c-4b69-96ed-8b6ed392acb0
The specified user was deleted
Deleting Remnant User: 3f3346a9-382b-40f3-a8f0-0dc307e364c0
The specified user was deleted
Deleting Remnant User: 40eafe4f-2456-46d6-9ba7-1f5c5c0bc118
The specified user was deleted
Deleting Remnant User: 41f0c3d7-0c46-47da-b2db-d2f006117d8a
The specified user was deleted
Deleting Remnant User: 43087f26-9b0a-4d9a-8e24-8dcd9f5adffa
The specified user was deleted
Deleting Remnant User: 436d36e2-91c7-4de7-a696-b5da92615c18
The specified user was deleted
Deleting Remnant User: 43e9d174-8670-4a5d-a850-2188e67cf305
The specified user was deleted
Deleting Remnant User: 4506fde6-15e9-435a-8747-9771dbff50d4
The specified user was deleted
Deleting Remnant User: 4569b60e-1f89-4e9c-a22e-e7152db7b5ea
The specified user was deleted
Deleting Remnant User: 4576928f-0f5a-467b-95b2-c44eb645809d
The specified user was deleted
Deleting Remnant User: 472e8308-4b21-47eb-8898-c4f80b5dd3d0
The specified user was deleted
Deleting Remnant User: 474197fd-8506-4f4a-9b9d-0e646728cbe3
The specified user was deleted
Deleting Remnant User: 4792ccfa-ae72-45c1-9aad-da9d1ace9ddb
The specified user was deleted
Deleting Remnant User: 485f619f-3b8b-4895-a8d2-e0aa4fab5c43
The specified user was deleted
Deleting Remnant User: 498d48cd-f292-4486-b600-422a9bc2fb5e
The specified user was deleted
Deleting Remnant User: 49bc4325-274a-4d49-84f7-498c62b9984b
The specified user was deleted
Deleting Remnant User: 4a4df621-cd7e-4022-8845-cf840c7d71e9
The specified user was deleted
Deleting Remnant User: 4b8e7b64-a0ac-4676-875e-5f047216749f
The specified user was deleted
Deleting Remnant User: 4b938891-e48d-472d-8e75-d984b9113902
The specified user was deleted
Deleting Remnant User: 4cc74197-12bf-42c2-9913-c09b14337a26
The specified user was deleted
Deleting Remnant User: 4d10d95e-ef80-482f-9982-270992f424ac
The specified user was deleted
Deleting Remnant User: 4d45e94b-5a02-401f-908d-cf394c392978
The specified user was deleted
Deleting Remnant User: 4f307733-6f2c-4097-9e22-3809bec3b7f4
The specified user was deleted
Deleting Remnant User: 4f8aa0a0-678b-4c36-89fb-0b6b69b620ce
The specified user was deleted
Deleting Remnant User: 4fb80a4c-dd83-41cd-8959-06f49de67a87
The specified user was deleted
Deleting Remnant User: 4fba8af5-ea4b-4906-a481-33d893479e4e
The specified user was deleted
Deleting Remnant User: 5264d984-0526-4e06-9896-88b4b2720bed
The specified user was deleted
Deleting Remnant User: 5348a9aa-2e14-456f-95b9-a2ed09130508
The specified user was deleted
Deleting Remnant User: 54e3f272-f3c9-42d0-a61c-190aa96943b8
The specified user was deleted
Deleting Remnant User: 550327ef-0d7c-48e8-93cb-41b6492f3322
The specified user was deleted
Deleting Remnant User: 5600571c-1c91-4edf-a407-8c236b42a9e3
The specified user was deleted
Deleting Remnant User: 56a2b5c5-9a24-44d6-9d27-02e336f2d4c7
The specified user was deleted
Deleting Remnant User: 5797b85a-7c0e-4c95-8921-12c21d347598
The specified user was deleted
Deleting Remnant User: 58161227-4b15-43f7-b4ed-2b9d273f3c23
The specified user was deleted
Deleting Remnant User: 59f79695-fd29-40f3-be78-bccd03117840
The specified user was deleted
Deleting Remnant User: 5aae9fa5-0492-4749-94cf-4dada2fc11ef
The specified user was deleted
Deleting Remnant User: 5b01814a-b697-4c0c-9a4f-4e49b3bb5d44
The specified user was deleted
Deleting Remnant User: 5b9f9d5c-b9ec-4ae8-b430-4ea219c21771
The specified user was deleted
Deleting Remnant User: 5ca1b114-b083-4779-b320-3f10537402eb
The specified user was deleted
Deleting Remnant User: 5cc723b0-23f3-431c-a714-b08c0f12b062
The specified user was deleted
Deleting Remnant User: 5cf40ef3-6646-4acd-bff9-3047553ccefc
The specified user was deleted
Deleting Remnant User: 5e3847e5-9f57-475c-b290-28e86fb67539
The specified user was deleted
Deleting Remnant User: 602feabd-3274-45fa-84b8-504798cfe841
The specified user was deleted
Deleting Remnant User: 603f6287-6295-4119-9ed5-95a7b8597846
The specified user was deleted
Deleting Remnant User: 60720514-e8fa-48b3-8995-231044294516
The specified user was deleted
Deleting Remnant User: 607a4308-33fc-4294-9771-f7638a73b471
The specified user was deleted
Deleting Remnant User: 6083f038-7b02-4565-ae67-d731252e352d
The specified user was deleted
Deleting Remnant User: 6099893b-679d-440c-9895-7e5747b6003b
The specified user was deleted
Deleting Remnant User: 613bfd6a-f52e-4d7e-8dfb-a50ad78210e4
The specified user was deleted
Deleting Remnant User: 61bdce02-f098-46f5-9abf-62280493d703
The specified user was deleted
Deleting Remnant User: 62c87720-729e-4e5f-b3da-a30952145072
The specified user was deleted
Deleting Remnant User: 63614407-31cc-4d07-910d-b8e295237882
The specified user was deleted
Deleting Remnant User: 653e7bfb-d35f-4d02-9465-f4a6e16b1341
The specified user was deleted
Deleting Remnant User: 659469ad-3c1f-48e1-a6c5-be9914d10b30
The specified user was deleted
Deleting Remnant User: 669262f3-92c1-462f-a8e4-67066bdb7b25
The specified user was deleted
Deleting Remnant User: 669ed219-455a-43c7-8d01-fe7ed09760be
The specified user was deleted
Deleting Remnant User: 66dc36bd-8e3c-4e58-9c48-c5ae0a8c2545
The specified user was deleted
Deleting Remnant User: 671208ed-1a51-456e-877d-961756db7bf9
The specified user was deleted
Deleting Remnant User: 67d5a30b-9c2a-48ac-ba28-4a0a3edbd6c3
The specified user was deleted
Deleting Remnant User: 67f98c7d-5186-4ecd-af2c-ee47cf320d9e
The specified user was deleted
Deleting Remnant User: 683e6ffa-1586-49dc-bdd6-9adc5280c173
The specified user was deleted
Deleting Remnant User: 6869fae5-e431-473c-9f94-33a79931b3df
The specified user was deleted
Deleting Remnant User: 68db63cf-79b5-4971-a104-366259147292
The specified user was deleted
Deleting Remnant User: 695201ea-8774-402f-87eb-94396a68f17c
The specified user was deleted
Deleting Remnant User: 69c25c70-b2a3-4bfa-9f89-976658aa58e9
The specified user was deleted
Deleting Remnant User: 6a4ba7c9-a9af-4248-82bc-23c1c00e5108
The specified user was deleted
Deleting Remnant User: 6b483267-e28f-4ed3-8cf0-586aa953636b
The specified user was deleted
Deleting Remnant User: 6bea37ab-c07d-4dee-be5f-3b7dad654f1e
The specified user was deleted
Deleting Remnant User: 6cd327fe-b2a0-4a48-b620-4fbbd74eaf20
The specified user was deleted
Deleting Remnant User: 6ea446e4-3c43-463c-b55e-3a08d54e493c
The specified user was deleted
Deleting Remnant User: 6f266d09-6794-4e55-865e-3ac2f572955a
The specified user was deleted
Deleting Remnant User: 6f86d57b-b9fa-4022-9358-4baf89ad3af1
The specified user was deleted
Deleting Remnant User: 6fb76af5-de04-42e3-9967-140e0de7cf3e
The specified user was deleted
Deleting Remnant User: 6fdfba9b-a09a-4082-9337-4c0f73b9adda
The specified user was deleted
Deleting Remnant User: 70104e8b-6c0a-4ede-b5fd-991ef3cc19ea
The specified user was deleted
Deleting Remnant User: 709726fb-27f4-402e-a5a4-2f355a954fd0
The specified user was deleted
Deleting Remnant User: 718a09ab-48ae-4ba9-945b-9dcb965a4863
The specified user was deleted
Deleting Remnant User: 72c4daab-cc4c-41f7-9150-656948fc1239
The specified user was deleted
Deleting Remnant User: 730eab1e-1e1e-4f58-a917-db4686b9729b
The specified user was deleted
Deleting Remnant User: 73732a24-4447-4407-9c03-ef1cc93b4825
The specified user was deleted
Deleting Remnant User: 74159a76-7fb5-4fee-9261-92f3891e034a
The specified user was deleted
Deleting Remnant User: 745cb091-9421-4261-9d6f-c5547fce5891
The specified user was deleted
Deleting Remnant User: 74b2a85f-dfe9-4cc6-87c9-a79708f7f531
The specified user was deleted
Deleting Remnant User: 77c111dd-9322-49e5-afeb-a54e03eac8f3
The specified user was deleted
Deleting Remnant User: 77ff4631-fbf4-4840-94b3-2e6639873337
The specified user was deleted
Deleting Remnant User: 77ff7487-81f8-4313-8d37-594774392a26
The specified user was deleted
Deleting Remnant User: 78fc111e-a237-49a7-9933-5b90cb9b51a5
The specified user was deleted
Deleting Remnant User: 7a722793-dae4-465b-ac1d-e0065e49074d
The specified user was deleted
Deleting Remnant User: 7b2993ba-3327-4b55-b7f8-e51c78b13169
The specified user was deleted
Deleting Remnant User: 7b7d8e27-2dc9-421a-802e-caf378361450
The specified user was deleted
Deleting Remnant User: 7bdd9cb2-3b24-447d-ae31-94c43aba40d5
The specified user was deleted
Deleting Remnant User: 7bebf22d-492b-4e5d-9485-8468333a7f59
The specified user was deleted
Deleting Remnant User: 7cacd567-ede7-4172-a654-3c8be7976e47
The specified user was deleted
Deleting Remnant User: 7cf4d678-9446-4782-883f-cf27501fd78b
The specified user was deleted
Deleting Remnant User: 7d443248-eb4d-47c4-a4c9-8e099a958545
The specified user was deleted
Deleting Remnant User: 7d500c80-68f8-4e25-8472-64e1d55b8fc6
The specified user was deleted
Deleting Remnant User: 7d59f671-cfab-46f3-84e7-cbe9cd2020a7
The specified user was deleted
Deleting Remnant User: 7e66551e-f46b-468b-a69f-ada8b9dbec61
The specified user was deleted
Deleting Remnant User: 7e74dc08-66e5-4423-84d5-109bd5b1c2c0
The specified user was deleted
Deleting Remnant User: 7f9852bd-08e3-4582-98d2-6bd37d83b250
The specified user was deleted
Deleting Remnant User: 7fb48608-b7e3-4763-b354-a4d1be1805b9
The specified user was deleted
Deleting Remnant User: 80938f8d-697e-4601-9fb2-a2410e1693bc
The specified user was deleted
Deleting Remnant User: 809662c3-10ac-4958-87f7-ab22cb508186
The specified user was deleted
Deleting Remnant User: 809f1c02-2d92-46ba-acfc-0680e747faf6
The specified user was deleted
Deleting Remnant User: 813390e9-83d4-46a8-8ffd-c35104c48574
The specified user was deleted
Deleting Remnant User: 81575916-354e-48a8-9e78-8810b7586ab0
The specified user was deleted
Deleting Remnant User: 836f6e4e-d243-4b0c-8219-5a0a718ece83
The specified user was deleted
Deleting Remnant User: 839ef260-a479-4033-a98b-38672a824879
The specified user was deleted
Deleting Remnant User: 84249a8f-d4b4-41d5-9217-85f9d87e82d0
The specified user was deleted
Deleting Remnant User: 8482e2a5-988d-403a-b6d7-5019f823ccb4
The specified user was deleted
Deleting Remnant User: 84848a64-f5cd-443d-a7f7-de9d33c96651
The specified user was deleted
Deleting Remnant User: 84e51d99-4cf4-46c7-89dc-aa9d97f1d8b1
The specified user was deleted
Deleting Remnant User: 852fabb4-0403-421c-b990-414cb9089cdd
The specified user was deleted
Deleting Remnant User: 854ac051-722f-47ac-88e7-02b312d2dafb
The specified user was deleted
Deleting Remnant User: 85654b93-e00b-49c7-ac70-1ca7005eff25
The specified user was deleted
Deleting Remnant User: 86a748be-b9cd-4290-b09c-d0e2655ef8c5
The specified user was deleted
Deleting Remnant User: 878f9d59-bec1-49d2-9b35-0c41b2117081
The specified user was deleted
Deleting Remnant User: 886b8c9b-0186-43eb-8e14-0d8be558c6d0
The specified user was deleted
Deleting Remnant User: 88d0f631-b8db-4dfc-9954-6f79b07e6416
The specified user was deleted
Deleting Remnant User: 88e6eae0-d2c0-4a79-96cc-c8f923a760e5
The specified user was deleted
Deleting Remnant User: 89c9d096-27cc-45ae-8649-378a375adeff
The specified user was deleted
Deleting Remnant User: 8a8976b4-91f6-4130-b840-1d7c38148660
The specified user was deleted
Deleting Remnant User: 8ac86b93-dff9-4c42-b7cb-aa1a23ae066a
The specified user was deleted
Deleting Remnant User: 8b5894b3-bf3c-4231-8430-1ac64f19b5b9
The specified user was deleted
Deleting Remnant User: 8c8b1f4f-e176-4d05-b19e-e931ec9359cc
The specified user was deleted
Deleting Remnant User: 8efabd56-d686-4200-b2be-b1345f9c471b
The specified user was deleted
Deleting Remnant User: 8f46919e-238d-428c-91c3-55717e17da30
The specified user was deleted
Deleting Remnant User: 8fb4b7ce-16a5-47fb-af70-c323bd1c0214
The specified user was deleted
Deleting Remnant User: 9282e4db-5d4c-4f3e-8cc0-b25d95ef8f3e
The specified user was deleted
Deleting Remnant User: 938f4788-a335-441a-ad7e-6a066c93a4a7
The specified user was deleted
Deleting Remnant User: 93a99e75-279a-4e56-9db6-021cb69d2b45
The specified user was deleted
Deleting Remnant User: 9467db1a-e827-4037-ae3b-0a7b6cc2a2d9
The specified user was deleted
Deleting Remnant User: 95228211-8377-41d3-a6f0-f788475d5b73
The specified user was deleted
Deleting Remnant User: 97641e85-7e45-465b-a7f0-ae306d593a2a
The specified user was deleted
Deleting Remnant User: 97deadf9-73b2-426f-b34e-5ee5f8a39990
The specified user was deleted
Deleting Remnant User: 98bd6a86-3b90-4744-ba57-da7020f79ca4
The specified user was deleted
Deleting Remnant User: 99148fda-c2b0-45b1-817f-416bbb232436
The specified user was deleted
Deleting Remnant User: 99351420-3faa-47ab-85a7-564cfd210728
The specified user was deleted
Deleting Remnant User: 996d5ddd-528f-44cf-a693-23b7e93df57b
The specified user was deleted
Deleting Remnant User: 9a7fac18-3b65-405f-96f0-6e5d80d42f55
The specified user was deleted
Deleting Remnant User: 9aa70e82-9a2d-4a06-9f76-c36547916661
The specified user was deleted
Deleting Remnant User: 9ae65aa8-8bd8-417a-a88e-e241c0f83696
The specified user was deleted
Deleting Remnant User: 9c5ed18e-46b4-4c4d-9295-38dca04ce04c
The specified user was deleted
Deleting Remnant User: 9c71d2fc-1914-4aec-aeca-be6e52ff3ca9
The specified user was deleted
Deleting Remnant User: 9d6f71d9-de7a-44c7-b8b9-ffc92cc8538e
The specified user was deleted
Deleting Remnant User: 9e9fe9b1-d3f1-4e92-bd9c-667d7d3b75c9
The specified user was deleted
Deleting Remnant User: 9fa174ee-bbcd-49f4-bead-b9ab67a6cf3b
The specified user was deleted
Deleting Remnant User: 9fb26248-6ac0-4bc9-be22-2a9932f05a9a
The specified user was deleted
Deleting Remnant User: a09ef297-8bc6-4bfc-9d97-a7536dfc946e
The specified user was deleted
Deleting Remnant User: a09f496c-ab60-40c4-94ca-5707f2ac6752
The specified user was deleted
Deleting Remnant User: a11ecb29-7d8c-4c75-becd-d086958e7afe
The specified user was deleted
Deleting Remnant User: a14408bf-8d08-4456-93ad-91017c0f2aca
The specified user was deleted
Deleting Remnant User: a1709251-273d-4b87-9c86-64bbd8fe899d
The specified user was deleted
Deleting Remnant User: a28de9db-8c89-4c2d-9cfe-c1ea34cc408b
The specified user was deleted
Deleting Remnant User: a2f58617-3630-46f7-9981-0e2be29849f0
The specified user was deleted
Deleting Remnant User: a3073969-8607-40b8-9eb9-6ee93ad7a753
The specified user was deleted
Deleting Remnant User: a4119a72-a7ce-4109-bc70-625ad6020100
The specified user was deleted
Deleting Remnant User: a591655b-236c-49cb-9517-2fe8dd3f5fb1
The specified user was deleted
Deleting Remnant User: a5d1677f-3fe9-49e7-b6d0-3bd18dc9fc8a
The specified user was deleted
Deleting Remnant User: a728663c-85c6-442a-aa93-7fbc57054049
The specified user was deleted
Deleting Remnant User: a8f99349-70f1-4bcb-821b-3fc6331dbe37
The specified user was deleted
Deleting Remnant User: a91e44e4-d2a3-4820-9c05-ff1a8656da51
The specified user was deleted
Deleting Remnant User: a942270f-5c0c-452e-98d2-0281b9656e8e
The specified user was deleted
Deleting Remnant User: aa339034-5039-40fd-98e8-7a9bfba0297f
The specified user was deleted
Deleting Remnant User: aa6f1a77-e20a-4ae1-bc5b-679bf533e05d
The specified user was deleted
Deleting Remnant User: ab2bede3-22bb-44b1-bdb6-a1de8b88a649
The specified user was deleted
Deleting Remnant User: ab3ff0b1-5313-4777-ad67-bb09e81ff2cc
The specified user was deleted
Deleting Remnant User: ab4db38a-b31f-466c-b913-70c1dc5cee6a
The specified user was deleted
Deleting Remnant User: ad8cdd8f-4d3c-41a4-8d2e-f048c549dfb5
The specified user was deleted
Deleting Remnant User: ade5eb1e-9cc9-4d85-a343-6883ec8b3a83
The specified user was deleted
Deleting Remnant User: ae7ccf81-2d9c-4834-95d4-8d6ac162f130
The specified user was deleted
Deleting Remnant User: ae83f653-d141-413d-bc3b-4e8d10a94839
The specified user was deleted
Deleting Remnant User: af17aedd-3af7-4589-b612-6385d7e58d8d
The specified user was deleted
Deleting Remnant User: b017ace3-3606-4121-a6f2-6cc75e8a538f
The specified user was deleted
Deleting Remnant User: b1368a6b-212a-4794-890b-249e68ce2837
The specified user was deleted
Deleting Remnant User: b159985d-afe0-400f-a7b8-e4c7569925d6
The specified user was deleted
Deleting Remnant User: b225b3ac-945a-46ec-b374-acd2cbd3a43b
The specified user was deleted
Deleting Remnant User: b22c8d48-5a64-45a2-a4a1-50f485c2a9a7
The specified user was deleted
Deleting Remnant User: b263d7c7-2eaa-4b78-b255-d2053abad241
The specified user was deleted
Deleting Remnant User: b27cc00e-6338-4407-a1c3-d46b367e56b9
The specified user was deleted
Deleting Remnant User: b2855e3d-dc29-4ad6-97ae-b89105b4fa8b
The specified user was deleted
Deleting Remnant User: b3a17e8e-92a2-43ac-abb7-97c6bc05e616
The specified user was deleted
Deleting Remnant User: b4a94da5-6bee-412f-9ec9-e839d83c75a2
The specified user was deleted
Deleting Remnant User: b50fe3dc-1607-44f9-9f08-af690d4cb2e7
The specified user was deleted
Deleting Remnant User: b5753e7d-9137-4d05-b1c4-edf79584fb53
The specified user was deleted
Deleting Remnant User: b676c60d-70c4-4944-b9fc-1487bb37b409
The specified user was deleted
Deleting Remnant User: b67b2e6c-b5ab-467a-b9b7-7bce139d9e97
The specified user was deleted
Deleting Remnant User: b683fc86-f0e4-4325-ab91-f5898a35723d
The specified user was deleted
Deleting Remnant User: b72201d2-8725-4dd6-a503-701b633569ca
The specified user was deleted
Deleting Remnant User: b7271f0b-65f2-47f8-8e9b-f47c9780455c
The specified user was deleted
Deleting Remnant User: b9c03c3e-22ab-4db7-9b36-d1f032d9a044
The specified user was deleted
Deleting Remnant User: b9d51472-a4b7-4a15-b574-f8f6c68eea1a
The specified user was deleted
Deleting Remnant User: ba0b1d5e-c51d-419a-9e0b-414724d242ec
The specified user was deleted
Deleting Remnant User: bb4a2c2f-e329-44fc-a092-fd7dd790c70f
The specified user was deleted
Deleting Remnant User: bb730034-e093-4bc5-a588-2c96f5cbc92d
The specified user was deleted
Deleting Remnant User: bb8d1067-e87a-458f-afb5-87ca69f6ba21
The specified user was deleted
Deleting Remnant User: bbbb8802-b6bb-41a3-b179-fc7bacb7aa60
The specified user was deleted
Deleting Remnant User: bbcbeeee-1c26-4cd2-9472-ff9d078ebc8e
The specified user was deleted
Deleting Remnant User: bda64e8c-c15b-4c0a-b3e5-23c2c1d37223
The specified user was deleted
Deleting Remnant User: bdaeb2a6-d6ab-4d62-ad9e-06e6390c1253
The specified user was deleted
Deleting Remnant User: be128870-3e9b-4d83-86f1-6aa9bdd43caa
The specified user was deleted
Deleting Remnant User: bef8fef5-3494-40c0-a72a-0854804933df
The specified user was deleted
Deleting Remnant User: bf1c9c70-f7f3-4dfa-ad6f-e0ecf0964c09
The specified user was deleted
Deleting Remnant User: bf8e9a9a-e476-4770-baa3-7ca8d11e7a52
The specified user was deleted
Deleting Remnant User: bf9861a3-a8db-4be5-89c9-732534e4de4d
The specified user was deleted
Deleting Remnant User: bfce1597-e435-4a69-a100-b63e2dd53acb
The specified user was deleted
Deleting Remnant User: c0af4ab9-3997-4155-a449-b6bae9a31507
The specified user was deleted
Deleting Remnant User: c0b62d50-0516-4fed-94bd-0505c4025024
The specified user was deleted
Deleting Remnant User: c0b68803-b7de-472d-92fd-dd37839d39a9
The specified user was deleted
Deleting Remnant User: c0da5e89-73ea-49ba-a4dd-412b55036582
The specified user was deleted
Deleting Remnant User: c0e63460-1c39-4b0c-b29b-dadcbc0c3b2e
The specified user was deleted
Deleting Remnant User: c1172c7a-f5b9-44f0-bee8-a45b4f58a96e
The specified user was deleted
Deleting Remnant User: c212cd0c-fbe7-452c-ad48-dcfeeacfe302
The specified user was deleted
Deleting Remnant User: c28ef488-d315-4b96-b4c4-5097f694263a
The specified user was deleted
Deleting Remnant User: c2902485-623a-4369-938a-0cac3c6e951e
The specified user was deleted
Deleting Remnant User: c2d36f68-f99b-42c5-ba83-91ce810bd3b2
The specified user was deleted
Deleting Remnant User: c315651d-4f42-4ec6-8579-cec02c1aceab
The specified user was deleted
Deleting Remnant User: c316bbb2-f844-4444-9f48-5d622c671eba
The specified user was deleted
Deleting Remnant User: c3efce8c-7769-444b-979d-6603dc9b3f32
The specified user was deleted
Deleting Remnant User: c590ddd6-7983-4012-a1b1-dd93e2474a5c
The specified user was deleted
Deleting Remnant User: c69c7625-ff7a-46f6-b862-a6aaa55cf747
The specified user was deleted
Deleting Remnant User: c8cc3c20-a6d6-4cf3-b017-4f168d1472fd
The specified user was deleted
Deleting Remnant User: ca2ab9e3-a24e-4d58-b368-30c7f61c1bf9
The specified user was deleted
Deleting Remnant User: cae25dfe-3413-4755-a337-ffe4f1232a28
The specified user was deleted
Deleting Remnant User: cae40706-fda1-4862-b97b-f321488539df
The specified user was deleted
Deleting Remnant User: cdf36f9d-b4e8-40f2-b817-c3eae7ba1e27
The specified user was deleted
Deleting Remnant User: cea55658-40f1-4ba8-bfce-e77bdea0e5c0
The specified user was deleted
Deleting Remnant User: cfe31728-1f9f-439c-afe6-9ca2798c98c2
The specified user was deleted
Deleting Remnant User: d035b755-7fa2-4e52-afec-8ff80c598825
The specified user was deleted
Deleting Remnant User: d091d4b3-79dc-4112-8797-f44db8ca2b19
The specified user was deleted
Deleting Remnant User: d14f51f8-b062-4827-945c-b2f3474336bf
The specified user was deleted
Deleting Remnant User: d24f6797-4e58-4d37-8617-39780f1f192e
The specified user was deleted
Deleting Remnant User: d27b24a7-96e8-4e9e-bc8f-8f7fc2742412
The specified user was deleted
Deleting Remnant User: d2ac090d-6501-4266-ab49-f6b3ff277469
The specified user was deleted
Deleting Remnant User: d43b6a96-f3e0-4bcc-9191-198a35fd32fa
The specified user was deleted
Deleting Remnant User: d578ee5d-64dc-4e41-8e96-9b37ce6e2294
The specified user was deleted
Deleting Remnant User: d5c3b20b-a1bb-48b1-b3ca-6cabfbb2e298
The specified user was deleted
Deleting Remnant User: d60f4e58-f921-4b50-97b6-2a393477f870
The specified user was deleted
Deleting Remnant User: d6ba3afe-b2e3-4e85-a15b-c4176de254f9
The specified user was deleted
Deleting Remnant User: d7e1c6a5-68a4-4ad3-8d0e-0d0a81542ab4
The specified user was deleted
Deleting Remnant User: d836eee1-8bc4-46ef-9c60-a02778a1e724
The specified user was deleted
Deleting Remnant User: d90eee7d-501d-44fa-bba8-d8042acab5e9
The specified user was deleted
Deleting Remnant User: d936b94a-fc89-49aa-9297-397015bc55a9
The specified user was deleted
Deleting Remnant User: d9cbaca4-a16c-4f66-8a76-d81fac54003b
The specified user was deleted
Deleting Remnant User: daecc04a-7801-4f68-818a-ea2abdd4ac8d
The specified user was deleted
Deleting Remnant User: db573abf-df94-4ab0-a953-075175d0817d
The specified user was deleted
Deleting Remnant User: dc426198-7bc7-42e3-afc3-be206e80ca2b
The specified user was deleted
Deleting Remnant User: dc6ca32a-be66-43ba-b8ce-6f2168c5be9b
The specified user was deleted
Deleting Remnant User: dc9f65d6-8bbb-4fb7-bd8c-55ac235275f3
The specified user was deleted
Deleting Remnant User: de35c844-e076-4a4c-a230-ce0c722c343e
The specified user was deleted
Deleting Remnant User: decfa4ba-c0c7-4441-a932-77e70be17677
The specified user was deleted
Deleting Remnant User: e1268a3f-0a67-45fb-bdac-4d52bf204d22
The specified user was deleted
Deleting Remnant User: e232717e-4769-4622-9b9f-47eb4d1f5532
The specified user was deleted
Deleting Remnant User: e26c1a15-3fcb-4a1c-8710-64c6d2723800
The specified user was deleted
Deleting Remnant User: e3fcaa25-5998-4a6d-94d5-e420ae9528b3
The specified user was deleted
Deleting Remnant User: e4523314-9493-4eac-be03-2318a0fd0c07
The specified user was deleted
Deleting Remnant User: e465f9d9-990d-4fce-87ba-82abd6dfcda1
The specified user was deleted
Deleting Remnant User: e5afe1fe-6cf7-4ace-bafb-c65bddcb5f11
The specified user was deleted
Deleting Remnant User: e5d99a7c-5845-4dc0-ac42-ca2e3b462c9d
The specified user was deleted
Deleting Remnant User: e5f08cde-b080-436a-8f79-7beec41cb342
The specified user was deleted
Deleting Remnant User: e624ec10-c680-4df4-8098-a87d11637ca8
The specified user was deleted
Deleting Remnant User: e6d46003-6cbb-4dd6-b791-10dd7568db58
The specified user was deleted
Deleting Remnant User: e6d97147-25c7-4be2-9393-16af196313fa
The specified user was deleted
Deleting Remnant User: e6f5f6c3-4b6c-4025-bef9-d554efc5481e
The specified user was deleted
Deleting Remnant User: e743ace0-5c52-4b93-a123-4035e66aef49
The specified user was deleted
Deleting Remnant User: e8329344-c032-4dd1-a0d2-f77f40b48786
The specified user was deleted
Deleting Remnant User: e8ce2e30-d153-4331-939d-373dad0e64c8
The specified user was deleted
Deleting Remnant User: e906c80b-828c-4af6-bf0d-4444969e9413
The specified user was deleted
Deleting Remnant User: e97e2a1e-bc28-40fe-996c-9a74298a861a
The specified user was deleted
Deleting Remnant User: ea64c522-aa99-4b7a-bf26-fb6b2898337b
The specified user was deleted
Deleting Remnant User: ea97c196-fbff-44e9-bfc7-1ca1438071b1
The specified user was deleted
Deleting Remnant User: eab431e3-f0aa-4d05-b5bb-3be4c8eed9b0
The specified user was deleted
Deleting Remnant User: eb04acda-d4ae-488a-98c2-245a408e4fed
The specified user was deleted
Deleting Remnant User: ebbf946b-baba-4a1c-9be3-04f00106d94f
The specified user was deleted
Deleting Remnant User: ebf3a7c8-2069-40f7-8255-e08416d8556a
The specified user was deleted
Deleting Remnant User: ec00a1e7-5e3d-476e-ba23-2b4c21df75b9
The specified user was deleted
Deleting Remnant User: ec024811-c7ce-4ec4-89cb-de8887391dc0
The specified user was deleted
Deleting Remnant User: ed25e83b-e086-48b2-867c-7076de083fc9
The specified user was deleted
Deleting Remnant User: ed6fa81f-5e3e-4db9-bb8a-a04d6e546bec
The specified user was deleted
Deleting Remnant User: eee10bca-aa08-4a10-87c9-330589cde915
The specified user was deleted
Deleting Remnant User: ef6198ec-f04d-487d-80c4-3e5cd94b6854
The specified user was deleted
Deleting Remnant User: f006a892-9cad-43db-ab6f-a1542e247ac7
The specified user was deleted
Deleting Remnant User: f04fe288-428d-493b-8485-8a46bc927ada
The specified user was deleted
Deleting Remnant User: f0aa7849-c8bc-4201-b44e-303c88602c5a
The specified user was deleted
Deleting Remnant User: f17a65e8-403a-4d54-b73b-f44055b6ff7d
The specified user was deleted
Deleting Remnant User: f1ebef71-37b4-43b0-95d5-552909bb9a55
The specified user was deleted
Deleting Remnant User: f31cf784-c26b-4afe-bdf8-2766be0ad864
The specified user was deleted
Deleting Remnant User: f347381d-9b0f-4202-a4d2-23e3a7bdef21
The specified user was deleted
Deleting Remnant User: f3516383-f1a3-4c03-be64-abab5dc119f7
The specified user was deleted
Deleting Remnant User: f3568a6b-1b37-4b9f-be61-5f762379edc5
The specified user was deleted
Deleting Remnant User: f392070a-27f7-4344-af1b-93aafceb8875
The specified user was deleted
Deleting Remnant User: f52bbef4-fcc9-4a44-80db-3b0ce8acd875
The specified user was deleted
Deleting Remnant User: f6a4bbde-65bc-400e-a5f9-71c72631ebb3
The specified user was deleted
Deleting Remnant User: f6e3418f-8b7f-4572-9439-4f41976734f1
The specified user was deleted
Deleting Remnant User: f87df6e4-a2d5-49b6-bec8-dc6874269440
The specified user was deleted
Deleting Remnant User: f8dbeeab-e4ff-490a-83e6-34de720c46b7
The specified user was deleted
Deleting Remnant User: f96acc1c-297f-4de6-8025-f22dbacc4620
The specified user was deleted
Deleting Remnant User: fa92172b-c2b9-4d9d-8e36-b7ad8892f822
The specified user was deleted
Deleting Remnant User: faa1f483-0182-4329-8961-d2f9ea0d9faf
The specified user was deleted
Deleting Remnant User: fadf72da-ce98-4d25-8cc9-7e811aa76a48
The specified user was deleted
Deleting Remnant User: fca19bdb-f4b1-4800-bc19-5eed5c1743ba
The specified user was deleted
Deleting Remnant User: fcac0a55-dfa1-4e2a-bc22-f718f838e724
The specified user was deleted
Deleting Remnant User: fcd8b4d8-1160-4f7c-9a1d-958c4108710f
The specified user was deleted
Deleting Remnant User: fce80aee-94b6-46bf-9d3e-c1d61d054020
The specified user was deleted
Deleting Remnant User: fd9bbf31-6a81-4630-88d2-261279ee834a
The specified user was deleted
Deleting Remnant User: fe08b261-2c2a-4979-ba99-a777c82ba308
The specified user was deleted
Deleting Remnant User: fe704d81-acc4-49e8-b655-95751353045d
The specified user was deleted
Deleting Remnant User: fef092af-90b4-4540-af71-d34976d4875e
The specified user was deleted
Deleting Remnant User: ff60ff1b-4a40-4fdd-8c4d-fd6acbfdfa7d
The specified user was deleted
Deleting Remnant User: ff72e10b-3128-4c7e-8df5-8e14f86deea2
The specified user was deleted

```

### 10-07-2019

- **Mistake Found** on cloud's "orphan sweep" cronjob!

```
[root@cloud biohpcadmin]# crontab -l
# auto-delete LDAP's orphaned users, @9 P.M. every Wednesday & Saturday 

0 21 * * 3,6 /bin/bash var/www/owncloud/html/orphan_sweep.sh   # SHOULD BE "/var/..."!!!

```

- check on remnant-users:

```
[root@cloud html]# sudo -u apache php occ ldap:show-remnants
+--------------------------------------+------------------------+----------+------------------------------------------------------------------+-------------------+--------------------+-----+--------+
| Nextcloud name                       | Display Name           | LDAP UID | LDAP DN                                                          | Last Login        | Detected on        | Dir | Sharer |
+--------------------------------------+------------------------+----------+------------------------------------------------------------------+-------------------+--------------------+-----+--------+
| 21e7a85d-f3de-4ea6-9aed-d45aecd2a865 | Antonio FernandezPerez | afern4   | uid=afern4,ou=mchgd,ou=users,dc=biohpc,dc=swmed,dc=edu           | November 9, 2018  | September 26, 2019 |     | N      |
| 21fcf0bc-d35a-4423-8ffb-04021a931046 | Nirmish Singla         | nsing3   | uid=nsing3,ou=urology,ou=users,dc=biohpc,dc=swmed,dc=edu         | November 16, 2018 | September 26, 2019 |     | N      |
| 222d64f5-5378-4628-9542-f276838eb24b | Guoji Xu               | s190556  | uid=s190556,ou=bioinformatics,ou=users,dc=biohpc,dc=swmed,dc=edu | August 21, 2019   | September 26, 2019 |     | N      |
| 22b7c6bf-67c1-4bae-b231-fb34decbe789 | Wei Guo                | s167891  | uid=s167891,ou=bioinformatics,ou=users,dc=biohpc,dc=swmed,dc=edu | June 18, 2019     | September 26, 2019 |     | N      |
| 22ca6dea-6ebc-4086-89e2-9cc583f65c77 | Deborah Hess           | s175720  | uid=s175720,ou=urology,ou=users,dc=biohpc,dc=swmed,dc=edu        | -                 | September 26, 2019 |     | N      |
| 29b3a123-cf17-4436-8613-6285fb727fb4 | Ji She                 | jshe     | uid=jshe,ou=biophysics,ou=users,dc=biohpc,dc=swmed,dc=edu        | April 10, 2019    | October 1, 2019    |     | N      |
| 2bdff47a-415d-4c25-a412-b4ed08d4ab43 | Kelly Tornow           | ktorno   | uid=ktorno,ou=radiology,ou=users,dc=biohpc,dc=swmed,dc=edu       | -                 | October 1, 2019    |     | N      |
| 2d6d7345-3cdc-4ea3-a894-3ab80cc6d94a | David Goodwin          | dgood3   | uid=dgood3,ou=greencenter,ou=users,dc=biohpc,dc=swmed,dc=edu     | -                 | October 1, 2019    |     | N      |
| 312b3b6c-41dc-4008-a042-ef5017ec888d | William Moore          | wmoor2   | uid=wmoor2,ou=radiology,ou=users,dc=biohpc,dc=swmed,dc=edu       | -                 | October 1, 2019    |     | N      |
| 31bb30a8-6832-47c3-bc4c-bafefeb8b970 | Joseph Kang            | s187520  | uid=s187520,ou=bioinformatics,ou=users,dc=biohpc,dc=swmed,dc=edu | -                 | October 7, 2019    |     | N      |
| 35e0babe-d2d5-4680-90d9-2634a2e0a99e | Steven Wright          | s185648  | uid=s185648,ou=tibir,ou=users,dc=biohpc,dc=swmed,dc=edu          | April 30, 2019    | October 7, 2019    |     | N      |
+--------------------------------------+------------------------+----------+------------------------------------------------------------------+-------------------+--------------------+-----+--------+

```

- corrected missing `/` in script's directory 

- executed `./orphan_sweep.sh` to manually delete them

- "Finished OC\Settings\BackgroundJobs\VerifyUserData job" --> causes permission changes to alter back to original

## Interview Notes with Anh N Le 

> Date: 08-27-2020
> Interviewers: Hossein F., Neha Sina, & Zengxing Pang


### Candidate Info

- Image viewer:

  - 1000-3000 thousands patients

  - IPACS database

    - 1-2 Teratbytes

    - contrast; measurement of size

  - UTSW Collaborator

- Google Cloud API: for DICOM viewing

- one server inside each server remote

- one big server local 

- Personify

  - AWS

  - nginx

- Python web development

  - NVC framework: laravel, Django

- IT:

  - 7 server

- Teaching:

  - machine learning --> data categorization; nerual-links
 
- Expressed interest in web-based pipeline platforms and really interested in how to visualize the results for users running them
  
- built his own streaming service

\
\

### Questions

- image viewer: dataset size

- Azure

- Linux

  - Ubuntu

  - +10 years in Linux

- Cloud computing

  - in-house or using commerical cloud providers

- Web development using python


\
\

### Verdict

- avoided certain questions; teaching and load balancer works, vague

- Astrocyte

- interested in building his career, getting to inteface with a computer cluster

- UTSW DICOM viewer; containerization, genomic pipeline platforms, computer cluster

  - Astrocyte; visualization

## 1:1 Conversations with LW

### 02-05-2021

- **Reporting**

  - knowledge transfer, ipmi reboot, check tickets

  - lustre transfer speed

\

- **Career Goals**

- system/solution architect --> build stable, high performing/available platforms, implement on the larger scale new and unique designs

  - e.g. rebuild lamella with more availability, scale out, people can use it reliably

  - building road, not becoming a slave to something I create---not 100% developer who's receiving 100 complaints/issue requests day on gitlab/github. 

  - I want to become expert at Linux, Cloud, HPC architecture

\

- remaining 20-30% deep learning and data science

  - understanding the methodologies andm making them run more efficiently

  - image processing and prediction

  - lots of data science, everybody knows how to use deep learning libraries and algorithms, tons of developers

\

- Overall, I enjoy working on something bigger, something that fewer people can touch, the infrastructure and system itself

  - openshift cluster, kubernetes cluster, storage systems or cloud infrastructure

  - automation, performance, and reliability


\

- **hpc images**

- to update kernel images

  - Option 1. start new image from scratch

  - Option 2. upgrade host to latest kernel

    - upgrade image to host's kernel

    - `yum --install root` (?) --- this pulls kernel modules from the host to CHROOT

\

### 02-11-2021

- 4 mil + 900k money needed

- new DWDM switches, lamella servers, QTS storage

  - CUH network

- Networking protocols --> topology

  - irq balance daemon 

- foundamental network, distributed system design, system architecure book, concept network system

\

- **To-Do's**

- adaptive routing for IB

- QSF adapter

\
\

### 03-05-2021

- Lamella Design

  - SQL cache hitting rate

    - still inside lamella01 - connect to parallel filesystem like /project or /work

  - HA x 2 on NEW ESXI

  - Lamella x 2 NEW HW

- File cache:

  - /home2, /work, /project at QTS

  - /archive at CUH

\
\

- DWDM:

\
\

### 04-09-2021

- Questions:

  - work from home

  - data-center; projects change--work from home

  - /project OSTs, scanning

  - scripting; it's fun

\

- InfiniBand:

  - from DWDM to dark fibre --> should save some money; Metro-X-XC2/Skyway switches may not be as urgent

\
   
- Lamella:

  - setup environment samba

  - containerize which component, nextcloud 

\
\
\

### 04-16-2021

- Career Development Plan

  - containerization

    - performance of containerized applications

    - software installation inside containerers

\

  - kubernetes/openshift

    - deploy and adminstration

    - job scheduling/deployment

\

  - Scripting via BASH and Python

    - backend scripts like how data is pulled from one service to another, and then organized for display

    - automation and system checkups via BASH

    - command line utilities; creation of such

\

  - data analytics(?)

\
\

- LW's comments

  - specialization: feels like I've not specialized

  - nfs, lustre(multi_raid), kubernetes/openshift(permission_map), lamella(direct routing)

  - think about training afterwards

\
\

- Deep Learning Network (CNN network)

  - cuda and how GPU works


\
\
\

### 04-30-2021

#### DDN Lustre Training

- Performance tuning:

  - maximize metadata bandwidth/throughput

  - metadata servers cross mounting different mdts, like the osts

  - OSTs performance optimization, how many osts per OSS

\

- Fault Tolerance:

  - OSS/MDS failover, inbetween VMs within a box --> how do we failover to a different box?

\

- Architecture:

  - LNet router, how to isolate the RPC requests of the clients to their own network, without interferring with the core DDN cluster

  - Deploy multiple filesystems and group them under the the same parent directory, like gpfs filesets, optimize performance based on workflow; software stack, for large files, and for general purpose usage

- Encryption: self-encrypting disks

\

- GPU-direct Training

\

- General Troubleshooting:

  - notify when specific clients are requesting too much metadata/storage IO operations (right now is manual)

  - SFA CLI troubleshooting on the hardware

\
\

#### Openshift

- Persistent volume and authentication

  - login node has openshift backend, and each user's bash session is a pod

  - how to mount shared storage and carry the LDAP authentication, to use modules or use software images

- managing image repositories


\
\


#### General

- Edge computing

- Singularity images and install them as a module

- Nextcloud, truly active-active load-balanced

- RDMA training?

- Customers from outside the campus, from other local universities?


\
\


### Week of 05-17-2021

- **DDN**

  - September storage:

  - CSI plugin driver

  - onDemand

  - hot nodes, use lfs_migrate, no write benefit

  - pacemaker/hearbeat failover

\
\

- **Lamella**

  - backup `/var/lib/samba/private/secret.tdb`

\
\

- **CSI**

  - filesystem base for spectrum, and present on Thursday 

\
\

### 06-25-2021

#### DDN Lustre

- Everything on QTS?

  - data replication (?)


\

- Multi-rail

  - each VM controls ONE card, can handle data streams through all ports simultaneously (max 4 port/VM)

\

- Hardware

  - 2x controller (RAID) per Box
 
  - no across-the-box failovers; each box has a pair of 

\
\

### 07-09-2021

#### Backups

- Lamella: additions

  - Samba secret for LDAP authen

  - Samba conf

  - vsftpd conf

  - ctdb conf

  - Nextcloud conf

  - traditionally: remote SSH trigger SQL drop, backback Nextcloud data directory located on /project/cloud_data

\
\

- Cloud:

  - locally scheduled SQL drops to /var/www/html  --> rsnapshot pulls whole dir

  - backs Nextcloud data --> separate backup data dir

\
\

- LDAP002 & LamellaSE --> not backed up!

  - ldap002: 

```
/usr/local/etc/openldap/backup/   # update x3/week and clean backup dir older than 1 week
/usr/local/etc/openldap/*.conf
/etc/sysconfig/iptables
``` 

  - LamellaSE:

```
/etc/samba/
/var/lib/samba/private/
/etc/nfs
/etc/sysconfig/iptables
```

\
\
\

#### Townhall/Lamella

- Large Local SSD, several 2-5 TBs for `tmp` upload directory

  - increase upload size

- High memory for Samba caching 

  - read/write cache

  - send and receive buffer

\
\

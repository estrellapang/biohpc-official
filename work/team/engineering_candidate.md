### Interview Questions

### BioHPC Introduction

> **Greetings** and welcome! **In case of Foreign names** Correct me for my pronounciation, is it " " ? Okay great! <Name> we are glad you could join us for this call today---my name is Zengxing; please feel freeto call me Zeng--- 

> 40 Mins

> **Introduce the Crew** We also have on-the-line with us two additional staff members from our department; we have Hossein, who is an experienced application developer on our HPC team, and we also have Neha from department administration. 
> So the purpose of this call is for us to get to know your experience and your skillset a bit more before we can decide move you further down the interview process. 

> We will first offer clearer picture of what computer scientists do at BioHPC, then follow up with some questions regarding your experiences, okay?

> So our BioHPC team is a function inside the UTSW Bioinformatics department; and although we have a large portion of researchers and PIs dedicated to biomedical data anayltics like image processing and genomic pipelines, the special role is to provide these researchers a platform, and enough computing power. So, what we do, in one sentence, is the maintanence and development of a super computer cluster, which is comprised of 500 CPU/GPU servers, 30 Petabytes of network storage, all linked together with high-speed-fiber optic network and managed by the SLURM resource management and job scheduler.

> We offer our computing resources in a cloud-like fashion to researchers who require intensive work loads and big data. 

> In term of our jobs as computational scientists and computer scientists, our work is divided into three main categories, Linux system administration; this involves network and data management, building physical computing infrastrucutres, hosting the linux OS, and maintaining them with frequent backups and upgrades.  The second area of our work is focused around web development, as we host a centralized web portal serving a front-end interface to software tools installed on the cluster to our users. The lastly, we also spend a fair amount of time supporting our userbase of 1000+ individuals, this includes educating them via training, how to work their way around the cluster, responding to their requests through an online help-desk, and sometimes collaborating them on their research projects.

> Okay without further ado, could you tell us about your most recent work experiences, and the type of projects you were involved in? 

>  May I ask what got you interested? follow up, what kind of opportunities and are you looking for?

>  What is your preferred programming language? To follow up, built? 

>  What made you interested in our HPC organization; *follow up* And how do you
see your skills and talents being best utilized here?

>  Could you describe some of the recent and most challenging projects you've worked on 
and what you have learned from them?

>  Do you have any experience with Linux systems? How familiar are you with Cloud Computing
infrastructure? Are you aware of the many automation tools used to quickly deploy & manage
cloud computing systems?

> Django Web Frame? Web development using python or javascript?


### Technical Questions

- Could you quickly summary your specific role in the development of your company's various modeling softwares? **Follow up**: among the things you've mentioned, what in your opinion has been your most valuable contribution?  

- What are some common challenges you or your company as a whole have experienced with regard to the users of your products? **Follow up**: so what methodss do you guys take towards enhancing the user experience? 

- You mentioned in your resume that you also carry the responsibility of maintaining virtual servers for your company? I'm curious as to what componentsof your infrastructure you guys have virtualized? And running on what type of host?

  - ont top of OS or directly on top of Hareware like VMWare ESXI host?

### Interpersonal/Motivation Questions 

- So what has motivated you to search for new career opportunities outside of engineering? What do you see here that aligns with your career ambitions?

- What's the work culture at your current job? How is your team/department structured? (& your ideal team dynamic?)

- What is your approach to conflict management?

- Through all of your experiences in both work and academia, what have you concluded to be the most important values an individual needs to be successful? 

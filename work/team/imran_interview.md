## Imran Moolla

### What makes you seek employment outside of TACC?

- TACC - Dec. 2020

  - too busy? not enough pay
 
- **How did you hear about us?**

  - looking for research/teaching/development environment, not enough flexibility in industry

- Willing to work on-site depending on compensation

- Technical roles

\

### Chevron / Oil

- Violatile environment --> resources vary too quickly

- Off load parallel to cold storage

- Biggest challenge?

- cloud is too costly


### What areas do you like to work in?


### Technical & Storage

- Central management:

  - Ansible, place default keys in image

  - configuration changes

  - hanging nodes: Gather node status prior to pushing config change, generate new host file based on each run

  - did ansible scripts via command line & cron jobs

- Provisioning tools

  - Bright, most commonly used

  - RedHat Satellite - provisioning; could overwrite GRUB configuration

- worked with different scheduler

  - slurm, lfs, bps

- Network & InfiniBand

  - IntelQTR --> Mellanox --> IntelOPA

  - FDR --> QDR

  - not cross-site 

- Linux

  - various distributions, since 2000s

- Data migration

  - rsync

    - create directory-tree first

    - sparse file policy

  - ECLs take consideration when going across different filesystems

  - knows cache-based data migration, not efficient

- Containerization

- Openshift/Kubernetes

  - exposing backend filesystem like GPFS/Lustre to containers on being spun up on a fly --> no CSI

  - persistent volumes --> NEW

- Lustre

  - use Splunk/scripting to monitoring

  - NVMe systems

  - ~ metadata challenge, use DNE to overcome (BFL)

  - LNET routing, no multi-rail

\
\

### 07-11-2021: Group Interview

- DDN Performance Optimization

  - added more

  - Lustre doesn't care about the status of its clients too much

- Data Migration, 5 PB parallel filesystem

  - migrate to 15 PB DDN Lustre storage

  - factors you would consider

  - keep the home storage running while migrating data to the new storage system

  - database to queue metadata

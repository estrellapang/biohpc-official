## 09252020

### Overview

- Engineering

- BME Background, PHD in Medical Imaging Modality, Data processing pipelines

### Interests

- Collaborative Research

- Web-Application Development

- User support

- Infrastructurual Projects

  - configure, fine-tune parallel filesystems, /project, /work, /archive

  - file-serving servers, like lamella, external cloud server--NFS/Samba

  - infinibands

  - workstation 

### Questions 

- Academic route vs. making things see the product

- Data analytics

- brainstorm, build test-environment, and push it to production

  - life cycle of projects are shorter in comparison to purely research projects

  - wide-variety of projects; never run of out things to do---

- Research; HPC Research

  - place yourself towards something you know you will like best

  - projects you have to brute-force, to a lot of bench-marks; compare alternate configuratio

  - complex systems 

### Why is Python slow

- interpreter slower than compilers

  - GIL: global interpreter lock makes it slow; prevents it from being fully parallel


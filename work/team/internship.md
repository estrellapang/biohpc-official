## Internship Planning with BioHPC 

### Mission 

- **From the Perspective of BioHPC**: dstribute knowledge & stimulate interest in research & computing--offering a unique glimpse of the interface between high performance computing and biomedical sciences

  - not to be mistaked with the goal of translating them into full-time employees  

  - fundamental understanding of HPC: clustering, containerization, performance (networking & data transfer)

  - development:

    - software tools for research: parallel computing, R, Cuda, git, Docker, Kubernetes, Singularity, Python, Django Web frame

  - DevOps: building packages from source, matching specific kernel versions, backporting, sysadmin, storage, networking, Docker, Kubernetes, Bash scripting, Anscible, deployment, SLURM

- **Intern's contributions** 

  -  easing workload & idea generation (outsider's perspective; how can we do it better: training, system, & user enhancement)

    - open with David's data visualization training example (as an outsider, what did you think?) 

    - why are research & engineering students assigned projects? --> serving as a thinktankfor new ideas --> guide & gear our interns to generate new ideas for us (every training, require to add a feature that was not required of them)


### Training Tracks

  - 2 tracks: 

    - Sysadmin-Userenhance

    - Development-Userenhance
 
  - Shorten training: modularizing with additional granularity
  
    - based on intern's choices, the training path will be formed by modules that correlate with their interests

  
### Further thoughts on training

- **continuity**: nfs & ldap server --> integrate with OpenHPC cluster

  - pre-packaged VM's (e.g. LDAP / Samba Server / mini-OpenHPC cluster for Development team) 

- **FTE Goal** (on top of Internship goals)---it's not the force-feeding of information, but CRITICAL Thinking: manifested in the acts of troubleshooting & independent research

  - **FORM**: philosophy, culture, & disposition 

    - Opensource: circulation of knowledge (why we are using Linux)---education, safe-guarding and forwarding knowledge in our local research community 

    - Collaborative 

    - Non-egotistical

  - **Method**: critical thinking, troubleshooting, resourcefulness, documentation

  - Hakumararu's Problem --> Startrek, pilots & team captains are put into contained, simulation grounds and be demanded to solve a problem in a live situation

    - e.g. a 3-node cluster with the MySQL libraries not entirely removed & at the same time, Samba set up but not authenticating via LDAP, networking MAC addresses that do not match with that shown in the interface config file, proxy environment variables not enabled (ouuuu..how about at the end of every session) 

    - 2nd e.g. extra credit w/h no solution: exercise on mental flexibility. Do you want your thinking to be so linear that you cannot fathom that sometimes we simply need to comment`issue not reproducible` & move on? 

      - IDENTIFY problem + provide solution in a documentation (more for the FTEs)

### Onboarding 

- **Education**: bachelor's Jr level & above, plus graduates, master's

  - phD's should be last resorts (fellowship)

  - Absolutely no mentioning of FTE opportunities---no mentioning of possibilities unless the intern makes an serious inquiry (also if we are seriously impressed by the performance) 

  - make known to interns that per their job title, THEY ARE PERMANENT 

  - not externally driven; do not learn & work to secure the a job, but being here and just being fully immersed in the work---people who are passionate will let their interest drive them to want to work here or find a similar position in the field 

- Are they interested? Partial purpose of the internship 

- **BioHPC's resources** 

  - FTE time--> No Interns allowed if BioHPC is 8 members or under 

    - both our time & how much interns can benefit from us!

- **Start & End date**

  - leave out ambiguities, clearly defined start & end dates 

  - why waste a candidate's time & energy leading them on, without any sense of whether they are going to make the cut or not (goes back to no MENTIONING of FTE)

  - Not fair towards them & not fair towards the FTEs who don't know how to gauge the training & mentorship duration

  - FTEs need to notify the team a a minimum of _3 months_ (preferrably 6) notice for new job prospects & career changes

## 06-11-2021

### Background & Notes

- Cyphen --> writing Python and compiling as C binary

- OpenCL on AMD cards

  - how to convert from GPU to CPU programming

- Difference between different MPI libraries

  - OpenMP, OpenMPI

- Software development

  - added a class for existing software

- Linux

  - Debian

  - has fair amount of debug experience

- a bit shy from user interactions

  - shy, but pretty enthusiastic from the interview

  - but we did scare him, finding bottle-neck for 1000's users' code and algorithms

- SolidWorks

  - 3D Design

- Researcher mentality

- Eager for opportunity

  - he seemed accepting of mundane tasks

- Honest and Humorous

  - expressing what's acceptable and what's not acceptable
 
\

### phD defense suspicion

- Took 11 years, and wishes to complete defense after coming to America

- interested in solving genetic disease

## Hung Le, 07-08-2021

### Info

- How to ensure reproducibility of computational results

  - software version, hardware specs

  - containerization of pipelines

### Good Questions

- How do you handle failures

  - there is no complete failure, you can learn something from every experience

  - in science and engineering, the rule of thumb is that you will always fail upon first try of anything

\

\

### Critique

- enthusiasm

  - never explained motivation

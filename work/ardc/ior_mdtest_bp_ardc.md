## IOR mdtest results: Bass-ARDC

### 2-Node Multi-Threaded Testing

> Nodes: Nucleus025,NucleusT002
> NucleusT002 --> ARDC node

```
----------------------------------------------------------------------------------------------------------------------------------------------------
### 2 threads

## check mpi host file:

NucleusT002 slots=1
Nucleus025 slots=1


mpirun --hostfile /home2/zpang1/ior_benchmark/mpi_hfile --map-by node -np 2 hostname
Nucleus025
NucleusT002


## CREATION: 

time mpirun --hostfile $HOME/ior_benchmark/mpi_hfile --map-by node -np 2 $HOME/ior_benchmark/ior_3_2_1/src/mdtest -d /project/biohpcadmin/zpang1/mdtest_bp_ardc_thread2 -i 3 -b 30 -z 1 -L -I 100 -u -t -w 2k -C
--------------------------------------------------------------------------
WARNING: There was an error initializing an OpenFabrics device.

  Local host:   Nucleus025
  Local device: mlx4_0
--------------------------------------------------------------------------
-- started at 08/19/2020 12:17:07 --

mdtest-3.4.0+dev was launched with 2 total task(s) on 2 node(s)
Command line used: /home2/zpang1/ior_benchmark/ior_3_2_1/src/mdtest '-d' '/project/biohpcadmin/zpang1/mdtest_bp_ardc_thread2' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-w' '2k' '-C'
Path: /project/biohpcadmin/zpang1
FS: 3536.7 TiB   Used FS: 80.5%   Inodes: 1613.5 Mi   Used Inodes: 40.4%

Nodemap: 10
2 tasks, 6000 files/directories
[Nucleus006:31125] 1 more process has sent help message help-mpi-btl-openib.txt / error in device init
[Nucleus006:31125] Set MCA parameter "orte_base_help_aggregate" to 0 to see all help / error messages

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :       1272.396       1269.577       1271.266          1.102
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :        412.471        408.347        410.919          1.831
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :        620.085        597.532        605.149         10.562
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 08/19/2020 12:18:05 --


real	1m0.348s
user	0m0.030s
sys	0m0.072s


\

## RANDOM STAT:

time mpirun --hostfile $HOME/ior_benchmark/mpi_hfile --map-by node -np 2 $HOME/ior_benchmark/ior_3_2_1/src/mdtest -d /project/biohpcadmin/zpang1/mdtest_bp_ardc_thread2 -i 3 -b 30 -z 1 -L -I 100 -u -t -R -T
--------------------------------------------------------------------------
WARNING: There was an error initializing an OpenFabrics device.

  Local host:   Nucleus025
  Local device: mlx4_0
--------------------------------------------------------------------------
-- started at 08/19/2020 12:23:25 --

mdtest-3.4.0+dev was launched with 2 total task(s) on 2 node(s)
Command line used: /home2/zpang1/ior_benchmark/ior_3_2_1/src/mdtest '-d' '/project/biohpcadmin/zpang1/mdtest_bp_ardc_thread2' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-R' '-T'
Path: /project/biohpcadmin/zpang1
FS: 3536.7 TiB   Used FS: 80.5%   Inodes: 1613.5 Mi   Used Inodes: 40.4%

Nodemap: 10
random seed: 1597857805
2 tasks, 6000 files/directories
[Nucleus006:32661] 1 more process has sent help message help-mpi-btl-openib.txt / error in device init
[Nucleus006:32661] Set MCA parameter "orte_base_help_aggregate" to 0 to see all help / error messages

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :       1280.209       1270.491       1275.779          3.880
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :       1256.028       1253.879       1255.107          0.902
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 08/19/2020 12:23:53 --


real	0m30.025s
user	0m0.039s
sys	0m0.062s

\

## REMOVAL

time mpirun --hostfile $HOME/ior_benchmark/mpi_hfile --map-by node -np 2 $HOME/ior_benchmark/ior_3_2_1/src/mdtest -d /project/biohpcadmin/zpang1/mdtest_bp_ardc_thread2 -i 3 -b 30 -z 1 -L -I 100 -u -t -r
--------------------------------------------------------------------------
WARNING: There was an error initializing an OpenFabrics device.

  Local host:   Nucleus025
  Local device: mlx4_0
--------------------------------------------------------------------------
-- started at 08/19/2020 12:25:26 --

mdtest-3.4.0+dev was launched with 2 total task(s) on 2 node(s)
Command line used: /home2/zpang1/ior_benchmark/ior_3_2_1/src/mdtest '-d' '/project/biohpcadmin/zpang1/mdtest_bp_ardc_thread2' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-r'
Path: /project/biohpcadmin/zpang1
FS: 3536.7 TiB   Used FS: 80.5%   Inodes: 1613.5 Mi   Used Inodes: 40.4%

Nodemap: 10
2 tasks, 6000 files/directories
[Nucleus006:33074] 1 more process has sent help message help-mpi-btl-openib.txt / error in device init
[Nucleus006:33074] Set MCA parameter "orte_base_help_aggregate" to 0 to see all help / error messages

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :        661.095        658.624        660.031          1.001
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :        652.144        650.320        651.389          0.777
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :        343.798        341.852        342.653          0.831
-- finished at 08/19/2020 12:26:21 --


real	0m57.486s
user	0m0.033s
sys	0m0.077s

----------------------------------------------------------------------------------------------------------------------------------------------------

\
\

----------------------------------------------------------------------------------------------------------------------------------------------------
### 4 threads

## CREATION: 

time mpirun --hostfile $HOME/ior_benchmark/mpi_hfile --map-by node -np 4 $HOME/ior_benchmark/ior_3_2_1/src/mdtest -d /project/biohpcadmin/zpang1/mdtest_bp_ardc_thread4 -i 3 -b 30 -z 1 -L -I 100 -u -t -w 2k -C
--------------------------------------------------------------------------
WARNING: There was an error initializing an OpenFabrics device.

  Local host:   Nucleus025
  Local device: mlx4_0
--------------------------------------------------------------------------
-- started at 08/19/2020 12:38:38 --

mdtest-3.4.0+dev was launched with 4 total task(s) on 2 node(s)
Command line used: /home2/zpang1/ior_benchmark/ior_3_2_1/src/mdtest '-d' '/project/biohpcadmin/zpang1/mdtest_bp_ardc_thread4' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-w' '2k' '-C'
Path: /project/biohpcadmin/zpang1
FS: 3536.7 TiB   Used FS: 80.5%   Inodes: 1613.5 Mi   Used Inodes: 40.4%

Nodemap: 1010
4 tasks, 12000 files/directories
[Nucleus006:36254] 3 more processes have sent help message help-mpi-btl-openib.txt / error in device init
[Nucleus006:36254] Set MCA parameter "orte_base_help_aggregate" to 0 to see all help / error messages

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :       1315.515       1311.567       1313.353          1.563
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :        788.507        756.604        767.330         14.975
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :        325.321        314.521        319.765          4.414
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 08/19/2020 12:39:53 --


real	1m16.335s
user	0m0.041s
sys	0m0.068s


\

## RANDOM STAT:

time mpirun --hostfile $HOME/ior_benchmark/mpi_hfile --map-by node -np 4 $HOME/ior_benchmark/ior_3_2_1/src/mdtest -d /project/biohpcadmin/zpang1/mdtest_bp_ardc_thread4 -i 3 -b 30 -z 1 -L -I 100 -u -t -R -T
--------------------------------------------------------------------------
WARNING: There was an error initializing an OpenFabrics device.

  Local host:   Nucleus025
  Local device: mlx4_0
--------------------------------------------------------------------------
-- started at 08/19/2020 12:54:09 --

mdtest-3.4.0+dev was launched with 4 total task(s) on 2 node(s)
Command line used: /home2/zpang1/ior_benchmark/ior_3_2_1/src/mdtest '-d' '/project/biohpcadmin/zpang1/mdtest_bp_ardc_thread4' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-R' '-T'
Path: /project/biohpcadmin/zpang1
FS: 3536.7 TiB   Used FS: 80.5%   Inodes: 1613.5 Mi   Used Inodes: 40.4%

Nodemap: 1010
random seed: 1597859649
4 tasks, 12000 files/directories
[Nucleus006:39769] 3 more processes have sent help message help-mpi-btl-openib.txt / error in device init
[Nucleus006:39769] Set MCA parameter "orte_base_help_aggregate" to 0 to see all help / error messages

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :       2533.757       2510.096       2524.619         10.136
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :       2451.171       2428.208       2442.089          9.924
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 08/19/2020 12:54:38 --


real	0m31.339s
user	0m0.040s
sys	0m0.074s

\

## REMOVAL

time mpirun --hostfile $HOME/ior_benchmark/mpi_hfile --map-by node -np 4 $HOME/ior_benchmark/ior_3_2_1/src/mdtest -d /project/biohpcadmin/zpang1/mdtest_bp_ardc_thread4 -i 3 -b 30 -z 1 -L -I 100 -u -t -r
--------------------------------------------------------------------------
WARNING: There was an error initializing an OpenFabrics device.

  Local host:   Nucleus025
  Local device: mlx4_0
--------------------------------------------------------------------------
-- started at 08/19/2020 12:56:05 --

mdtest-3.4.0+dev was launched with 4 total task(s) on 2 node(s)
Command line used: /home2/zpang1/ior_benchmark/ior_3_2_1/src/mdtest '-d' '/project/biohpcadmin/zpang1/mdtest_bp_ardc_thread4' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-r'
Path: /project/biohpcadmin/zpang1
FS: 3536.7 TiB   Used FS: 80.5%   Inodes: 1613.5 Mi   Used Inodes: 40.4%

Nodemap: 1010
4 tasks, 12000 files/directories
[Nucleus006:40288] 3 more processes have sent help message help-mpi-btl-openib.txt / error in device init
[Nucleus006:40288] Set MCA parameter "orte_base_help_aggregate" to 0 to see all help / error messages

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :       1107.146       1086.974       1097.641          8.223
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :       1113.633       1110.079       1111.507          1.495
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :        283.271        266.870        274.252          6.795
-- finished at 08/19/2020 12:57:10 --


real	1m7.896s
user	0m0.027s
sys	0m0.075s

----------------------------------------------------------------------------------------------------------------------------------------------------

\
\

----------------------------------------------------------------------------------------------------------------------------------------------------
### 8 threads

## CREATION: 

time mpirun --hostfile $HOME/ior_benchmark/mpi_hfile --map-by node -np 8 $HOME/ior_benchmark/ior_3_2_1/src/mdtest -d /project/biohpcadmin/zpang1/mdtest_bp_ardc_thread8 -i 3 -b 30 -z 1 -L -I 100 -u -t -w 2k -C
--------------------------------------------------------------------------
WARNING: There was an error initializing an OpenFabrics device.

  Local host:   Nucleus025
  Local device: mlx4_0
--------------------------------------------------------------------------
-- started at 08/19/2020 13:02:02 --

mdtest-3.4.0+dev was launched with 8 total task(s) on 2 node(s)
Command line used: /home2/zpang1/ior_benchmark/ior_3_2_1/src/mdtest '-d' '/project/biohpcadmin/zpang1/mdtest_bp_ardc_thread8' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-w' '2k' '-C'
Path: /project/biohpcadmin/zpang1
FS: 3536.7 TiB   Used FS: 80.5%   Inodes: 1613.5 Mi   Used Inodes: 40.4%

Nodemap: 10101010
8 tasks, 24000 files/directories
[Nucleus006:45341] 7 more processes have sent help message help-mpi-btl-openib.txt / error in device init
[Nucleus006:45341] Set MCA parameter "orte_base_help_aggregate" to 0 to see all help / error messages

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :       1324.268       1295.507       1312.986         12.498
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :        887.090        827.484        857.559         24.336
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :        165.812        155.591        160.336          4.205
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 08/19/2020 13:04:21 --


real	2m21.858s
user	0m0.035s
sys	0m0.073s

\

## RANDOM STAT:

 time mpirun --hostfile $HOME/ior_benchmark/mpi_hfile --map-by node -np 8 $HOME/ior_benchmark/ior_3_2_1/src/mdtest -d /project/biohpcadmin/zpang1/mdtest_bp_ardc_thread8 -i 3 -b 30 -z 1 -L -I 100 -u -t -R -T
--------------------------------------------------------------------------
WARNING: There was an error initializing an OpenFabrics device.

  Local host:   Nucleus025
  Local device: mlx4_0
--------------------------------------------------------------------------
-- started at 08/19/2020 13:07:28 --

mdtest-3.4.0+dev was launched with 8 total task(s) on 2 node(s)
Command line used: /home2/zpang1/ior_benchmark/ior_3_2_1/src/mdtest '-d' '/project/biohpcadmin/zpang1/mdtest_bp_ardc_thread8' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-R' '-T'
Path: /project/biohpcadmin/zpang1
FS: 3536.7 TiB   Used FS: 80.5%   Inodes: 1613.5 Mi   Used Inodes: 40.4%

Nodemap: 10101010
random seed: 1597860448
8 tasks, 24000 files/directories
[Nucleus006:46597] 7 more processes have sent help message help-mpi-btl-openib.txt / error in device init
[Nucleus006:46597] Set MCA parameter "orte_base_help_aggregate" to 0 to see all help / error messages

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :       4109.900       4034.764       4075.427         30.634
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :       4079.340       3995.884       4041.265         34.455
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 08/19/2020 13:08:04 --


real	0m37.880s
user	0m0.035s
sys	0m0.082s

\

## REMOVAL
----------------------------------------------------------------------------------------------------------------------------------------------------

time mpirun --hostfile $HOME/ior_benchmark/mpi_hfile --map-by node -np 8 $HOME/ior_benchmark/ior_3_2_1/src/mdtest -d /project/biohpcadmin/zpang1/mdtest_bp_ardc_thread8 -i 3 -b 30 -z 1 -L -I 100 -u -t -r
--------------------------------------------------------------------------
WARNING: There was an error initializing an OpenFabrics device.

  Local host:   Nucleus025
  Local device: mlx4_0
--------------------------------------------------------------------------
-- started at 08/19/2020 13:10:26 --

mdtest-3.4.0+dev was launched with 8 total task(s) on 2 node(s)
Command line used: /home2/zpang1/ior_benchmark/ior_3_2_1/src/mdtest '-d' '/project/biohpcadmin/zpang1/mdtest_bp_ardc_thread8' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-r'
Path: /project/biohpcadmin/zpang1
FS: 3536.7 TiB   Used FS: 80.5%   Inodes: 1613.5 Mi   Used Inodes: 40.4%

Nodemap: 10101010
8 tasks, 24000 files/directories
[Nucleus006:47339] 7 more processes have sent help message help-mpi-btl-openib.txt / error in device init
[Nucleus006:47339] Set MCA parameter "orte_base_help_aggregate" to 0 to see all help / error messages

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :       1137.317       1127.044       1132.124          4.167
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :       1252.940       1179.662       1225.133         32.417
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :        123.973        122.767        123.328          0.496
-- finished at 08/19/2020 13:12:29 --


real	2m5.623s
user	0m0.037s
sys	0m0.069s

\
\

----------------------------------------------------------------------------------------------------------------------------------------------------
### 16 threads

## CREATION: 

time mpirun --hostfile $HOME/ior_benchmark/mpi_hfile --map-by node -np 16 $HOME/ior_benchmark/ior_3_2_1/src/mdtest -d /project/biohpcadmin/zpang1/mdtest_bp_ardc_thread16 -i 3 -b 30 -z 1 -L -I 100 -u -t -w 2k -C
--------------------------------------------------------------------------
WARNING: There was an error initializing an OpenFabrics device.

  Local host:   Nucleus025
  Local device: mlx4_0
--------------------------------------------------------------------------
-- started at 08/19/2020 13:16:39 --

mdtest-3.4.0+dev was launched with 16 total task(s) on 2 node(s)
Command line used: /home2/zpang1/ior_benchmark/ior_3_2_1/src/mdtest '-d' '/project/biohpcadmin/zpang1/mdtest_bp_ardc_thread16' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-w' '2k' '-C'
Path: /project/biohpcadmin/zpang1
FS: 3536.7 TiB   Used FS: 80.5%   Inodes: 1613.5 Mi   Used Inodes: 40.4%

Nodemap: 1010101010101010
16 tasks, 48000 files/directories
[Nucleus006:48895] 15 more processes have sent help message help-mpi-btl-openib.txt / error in device init
[Nucleus006:48895] Set MCA parameter "orte_base_help_aggregate" to 0 to see all help / error messages

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :       1333.096       1288.748       1315.281         19.108
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :        899.796        884.416        893.449          6.559
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :         84.315         83.111         83.672          0.495
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 08/19/2020 13:21:11 --


real	4m34.316s
user	0m0.030s
sys	0m0.093s

\

## RANDOM STAT:

time mpirun --hostfile $HOME/ior_benchmark/mpi_hfile --map-by node -np 16 $HOME/ior_benchmark/ior_3_2_1/src/mdtest -d /project/biohpcadmin/zpang1/mdtest_bp_ardc_thread16 -i 3 -b 30 -z 1 -L -I 100 -u -t -R -T
--------------------------------------------------------------------------
WARNING: There was an error initializing an OpenFabrics device.

  Local host:   Nucleus025
  Local device: mlx4_0
--------------------------------------------------------------------------
-- started at 08/19/2020 13:30:40 --

mdtest-3.4.0+dev was launched with 16 total task(s) on 2 node(s)
Command line used: /home2/zpang1/ior_benchmark/ior_3_2_1/src/mdtest '-d' '/project/biohpcadmin/zpang1/mdtest_bp_ardc_thread16' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-R' '-T'
Path: /project/biohpcadmin/zpang1
FS: 3536.7 TiB   Used FS: 80.5%   Inodes: 1613.5 Mi   Used Inodes: 40.4%

Nodemap: 1010101010101010
random seed: 1597861840
16 tasks, 48000 files/directories
[Nucleus006:03333] 15 more processes have sent help message help-mpi-btl-openib.txt / error in device init
[Nucleus006:03333] Set MCA parameter "orte_base_help_aggregate" to 0 to see all help / error messages

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :       5235.255       5195.620       5216.073         15.914
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :       5265.452       5215.731       5234.461         22.068
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 08/19/2020 13:31:35 --


real	0m57.727s
user	0m0.041s
sys	0m0.076s

\

## REMOVAL
----------------------------------------------------------------------------------------------------------------------------------------------------

time mpirun --hostfile $HOME/ior_benchmark/mpi_hfile --map-by node -np 16 $HOME/ior_benchmark/ior_3_2_1/src/mdtest -d /project/biohpcadmin/zpang1/mdtest_bp_ardc_thread16 -i 3 -b 30 -z 1 -L -I 100 -u -t -r
--------------------------------------------------------------------------
WARNING: There was an error initializing an OpenFabrics device.

  Local host:   Nucleus025
  Local device: mlx4_0
--------------------------------------------------------------------------
-- started at 08/19/2020 13:32:26 --

mdtest-3.4.0+dev was launched with 16 total task(s) on 2 node(s)
Command line used: /home2/zpang1/ior_benchmark/ior_3_2_1/src/mdtest '-d' '/project/biohpcadmin/zpang1/mdtest_bp_ardc_thread16' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-r'
Path: /project/biohpcadmin/zpang1
FS: 3536.7 TiB   Used FS: 80.5%   Inodes: 1613.5 Mi   Used Inodes: 40.4%

Nodemap: 1010101010101010
16 tasks, 48000 files/directories
[Nucleus006:03692] 15 more processes have sent help message help-mpi-btl-openib.txt / error in device init
[Nucleus006:03692] Set MCA parameter "orte_base_help_aggregate" to 0 to see all help / error messages

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :       1165.738       1133.802       1148.879         13.084
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :       1250.348       1233.730       1241.571          6.815
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :         70.911         68.211         69.234          1.195
-- finished at 08/19/2020 13:36:28 --


real	4m5.315s
user	0m0.038s
sys	0m0.083s

```

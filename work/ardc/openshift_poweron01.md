##

### Gen. System Info

```
# BMC Interfaces:

172.18.239.221-223 (ESX1,2,3)

u: USERID p: P@ssw0rd!234

172.18.239.201 (Vcenter on ESX1 --> GPFS cluster)

172.18.239.202 (Vcenter on ESX2)

  # system/BIOS auth --> u: root p: P@ssw0rd1

172.18.239.200 --> vSphere central (vcsa.biohpc.swmed.edu/ui)

  # u: administrator@vsphere.local p: <>

  # 172.18.238.2 --> DNS (T002) --> EVERY NODE needs to be configured with the DNS

nmcli con mod ens192 ipv4.dns "172.18.238.2"

  # needed for reconfig to kick in
nmcli con up ens192



\

# on Nucleus003

proxy info is stored in /etc/profile
```

### ESS Storage Cluster (ESX1)

```
# internal ESS Cluster
198.216.50.[1-3]  

# WebGUI 172.18.239.213

# pdsh installed on cluster for management
```

\
\

### LSF-Cluster Bastion (ESX2) 

```
# log in to bastion to SSH into the controller & infrastructure nodes

bastion.ocp.biohpc.swmed.edu

ssh core@controller-node1.ocp.biohpc.swmed.edu
ssh core@controller-node2.ocp.biohpc.swmed.edu
ssh core@controller-node3.ocp.biohpc.swmed.edu
ssh core@infra-node1.ocp.biohpc.swmed.edu
ssh core@infra-node2ocp.biohpc.swmed.edu

# inside Bastion

  # see cluster status 

oc get node 

  # exportfs --> this is more of a placeholder here

/var/nfs  --> exports for pod image-registry --- (not container-image registry)

  # HA proxy, handles all ingres traffi for cluster

systemctl status ha proxy 

    # /etc/haproxy/haproxy.cfg
-------------------------------  
local port 6443

  # check front-end back-end!
-------------------------------  

  # cluster info: check cluster status

oc cluster-info

  # check resources--> shows ALL PODS on cluster

oc get pods -A

     # STATUS: "CrashLoopBackOff" --> stuck in power-up-power-down


  # check status of specific pod

oc get pods -n openshift-console


  # get details on each cluster node on LSF cluster

oc describe node <lsf cluster node>

cd /ocp-install/auth/kubeadmin-password   # only user on cluster

  # openshift web console

"console-openshif-console.apps.ocp.biohpc.swmed.edu"

  # ? 

oc adm node uncordon <node name>

  # check certificates

oc get csr   # no output---> abnormal; sho

\

# on one of controller nodes(controller-node1)

sudo systemctl status kublet

  # get detailed logs on a service

sudo journalctl -u kublet



# BOOT ORDER

  # ocp-workers do not turn on 1-3 (4-6 are legit ones)

  # controller nodes boot first, then infrastructure nodes

# intrafrastructure and controller nodes power on first (spread across ESX2,3)

  # controllers 

  # ocp worker hold the application pods(NO core user; just root)

# NOTES: 

"oc get nodes" show all nodes' status being not-ready

caused by the controllers' need to connect to the internet (RHEL registration & licensing)

nodes' time are behind

/var/namd/db.biohpc.swmed.edu  # dns config file

  # what are zones in DNS?

chronyd is the time keeping daemon

# controller nodes are heavily dependent on DNS

```


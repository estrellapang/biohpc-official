## Install and Test Instructions for IOR and mdtest

### Installation

```
git clone https://github.com/hpc/ior.git 

cd ior/

./bootstrap

module load openmpi/intel/4.0.3

./configure --without-gpfs --prefix=/home2/zpang1/ior_benchmark/ior_3_2_1

make clean && make

```

\
\

### Tests: 2-Node Multi-threaded

> # increase "slot" count in mpi hosts file every time thread-count is increased
> # e.g. total of 4 threads used, host file:
> Nucleus026 slots=2
> Nucleus025 slots=2
>
> . 
> 
> # check that the threads are being evenly utilized across the nodes
>  mpirun --hostfile /home2/zpang1/ior_benchmark/mpi_hfile --map-by node -np 4 hostname
> Nucleus026
> Nucleus026
> Nucleus025
> Nucleus025

- Bass Results

  - nodes Nucleus025,026

  - 128GB RAM, 32 Logical CPUs


```
----------------------------------------------------------------------------------------------------------------------------------------------------
### 2 threads

## CREATION: files/directories 2*2*30*100*3 = 36000

time mpirun --hostfile $HOME/ior_benchmark/mpi_hfile --map-by node -np 2 $HOME/ior_benchmark/ior_3_2_1/src/mdtest -d /project/biohpcadmin/zpang1/mdtest_bass_2_thread -i 3 -b 30 -z 1 -L -I 100 -u -t -w 2k -C
--------------------------------------------------------------------------
WARNING: There was an error initializing an OpenFabrics device.

  Local host:   Nucleus026
  Local device: mlx4_0
--------------------------------------------------------------------------
-- started at 08/19/2020 08:29:40 --

mdtest-3.4.0+dev was launched with 2 total task(s) on 2 node(s)
Command line used: /home2/zpang1/ior_benchmark/ior_3_2_1/src/mdtest '-d' '/project/biohpcadmin/zpang1/mdtest_bass_2_thread' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-w' '2k' '-C'
Path: /project/biohpcadmin/zpang1
FS: 3536.7 TiB   Used FS: 80.6%   Inodes: 1613.5 Mi   Used Inodes: 40.4%

Nodemap: 10
2 tasks, 6000 files/directories
[Nucleus006:01521] 1 more process has sent help message help-mpi-btl-openib.txt / error in device init
[Nucleus006:01521] Set MCA parameter "orte_base_help_aggregate" to 0 to see all help / error messages

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :       9653.647       8967.796       9361.672        289.119
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :       2704.145       2272.873       2483.292        176.214
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :       4873.963       4386.064       4602.788        202.871
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 08/19/2020 08:29:49 --


real	0m10.623s
user	0m0.036s
sys	0m0.057s

\

## RANDOM STAT

time mpirun --hostfile $HOME/ior_benchmark/mpi_hfile --map-by node -np 2 $HOME/ior_benchmark/ior_3_2_1/src/mdtest -d /project/biohpcadmin/zpang1/mdtest_bass_2_thread -i 3 -b 30 -z 1 -L -I 100 -u -t -R -T
--------------------------------------------------------------------------
WARNING: There was an error initializing an OpenFabrics device.

  Local host:   Nucleus025
  Local device: mlx4_0
--------------------------------------------------------------------------
-- started at 08/19/2020 08:43:34 --

mdtest-3.4.0+dev was launched with 2 total task(s) on 2 node(s)
Command line used: /home2/zpang1/ior_benchmark/ior_3_2_1/src/mdtest '-d' '/project/biohpcadmin/zpang1/mdtest_bass_2_thread' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-R' '-T'
Path: /project/biohpcadmin/zpang1
FS: 3536.7 TiB   Used FS: 80.6%   Inodes: 1613.5 Mi   Used Inodes: 40.4%

Nodemap: 10
random seed: 1597844614
2 tasks, 6000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :      14211.229      12912.441      13498.319        537.754
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :      11950.341      10138.237      10847.053        790.480
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 08/19/2020 08:43:37 --

[Nucleus006:04948] 1 more process has sent help message help-mpi-btl-openib.txt / error in device init
[Nucleus006:04948] Set MCA parameter "orte_base_help_aggregate" to 0 to see all help / error messages

real	0m4.148s
user	0m0.045s
sys	0m0.061s

\

## REMOVAL

 time mpirun --hostfile $HOME/ior_benchmark/mpi_hfile --map-by node -np 2 $HOME/ior_benchmark/ior_3_2_1/src/mdtest -d /project/biohpcadmin/zpang1/mdtest_bass_2_thread -i 3 -b 30 -z 1 -L -I 100 -u -t -r
--------------------------------------------------------------------------
WARNING: There was an error initializing an OpenFabrics device.

  Local host:   Nucleus025
  Local device: mlx4_0
--------------------------------------------------------------------------
-- started at 08/19/2020 08:47:53 --

mdtest-3.4.0+dev was launched with 2 total task(s) on 2 node(s)
Command line used: /home2/zpang1/ior_benchmark/ior_3_2_1/src/mdtest '-d' '/project/biohpcadmin/zpang1/mdtest_bass_2_thread' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-r'
Path: /project/biohpcadmin/zpang1
FS: 3536.7 TiB   Used FS: 80.6%   Inodes: 1613.5 Mi   Used Inodes: 40.4%

Nodemap: 10
2 tasks, 6000 files/directories
[Nucleus006:06068] 1 more process has sent help message help-mpi-btl-openib.txt / error in device init
[Nucleus006:06068] Set MCA parameter "orte_base_help_aggregate" to 0 to see all help / error messages

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :       6152.281       5994.114       6057.392         68.305
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :       5213.912       5176.553       5198.923         16.092
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :       4392.374       4010.310       4189.270        156.909
-- finished at 08/19/2020 08:47:59 --


real	0m7.698s
user	0m0.039s
sys	0m0.065s

----------------------------------------------------------------------------------------------------------------------------------------------------

\
\

----------------------------------------------------------------------------------------------------------------------------------------------------
### 4 threads

## CREATION: files/directories 4*2*30*100*3 = 72000

time mpirun --hostfile $HOME/ior_benchmark/mpi_hfile --map-by node -np 4 $HOME/ior_benchmark/ior_3_2_1/src/mdtest -d /project/biohpcadmin/zpang1/mdtest_bass_4_thread -i 3 -b 30 -z 1 -L -I 100 -u -t -w 2k -C
--------------------------------------------------------------------------
WARNING: There was an error initializing an OpenFabrics device.

  Local host:   Nucleus026
  Local device: mlx4_0
--------------------------------------------------------------------------
-- started at 08/19/2020 08:57:23 --

mdtest-3.4.0+dev was launched with 4 total task(s) on 2 node(s)
Command line used: /home2/zpang1/ior_benchmark/ior_3_2_1/src/mdtest '-d' '/project/biohpcadmin/zpang1/mdtest_bass_4_thread' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-w' '2k' '-C'
Path: /project/biohpcadmin/zpang1
FS: 3536.7 TiB   Used FS: 80.6%   Inodes: 1613.5 Mi   Used Inodes: 40.4%

Nodemap: 1010
4 tasks, 12000 files/directories
[Nucleus006:08199] 3 more processes have sent help message help-mpi-btl-openib.txt / error in device init
[Nucleus006:08199] Set MCA parameter "orte_base_help_aggregate" to 0 to see all help / error messages

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :      10980.277       9474.025      10206.572        615.577
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :       5036.952       3852.251       4449.733        483.663
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :       3080.745       1985.925       2568.169        449.665
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 08/19/2020 08:57:35 --


real	0m13.067s
user	0m0.035s
sys	0m0.080s

\

## RANDOM STAT:

time mpirun --hostfile $HOME/ior_benchmark/mpi_hfile --map-by node -np 4 $HOME/ior_benchmark/ior_3_2_1/src/mdtest -d /project/biohpcadmin/zpang1/mdtest_bass_4_thread -i 3 -b 30 -z 1 -L -I 100 -u -t -R -T
--------------------------------------------------------------------------
WARNING: There was an error initializing an OpenFabrics device.

  Local host:   Nucleus026
  Local device: mlx4_0
--------------------------------------------------------------------------
-- started at 08/19/2020 09:03:19 --

mdtest-3.4.0+dev was launched with 4 total task(s) on 2 node(s)
Command line used: /home2/zpang1/ior_benchmark/ior_3_2_1/src/mdtest '-d' '/project/biohpcadmin/zpang1/mdtest_bass_4_thread' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-R' '-T'
Path: /project/biohpcadmin/zpang1
FS: 3536.7 TiB   Used FS: 80.6%   Inodes: 1613.5 Mi   Used Inodes: 40.4%

Nodemap: 1010
random seed: 1597845799
4 tasks, 12000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :      27126.244      26070.292      26600.437        430.577
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :      23892.612      22705.360      23130.672        539.857
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 08/19/2020 09:03:21 --

[Nucleus006:13451] 3 more processes have sent help message help-mpi-btl-openib.txt / error in device init
[Nucleus006:13451] Set MCA parameter "orte_base_help_aggregate" to 0 to see all help / error messages

real	0m4.079s
user	0m0.033s
sys	0m0.068s

\

## REMOVAL

time mpirun --hostfile $HOME/ior_benchmark/mpi_hfile --map-by node -np 4 $HOME/ior_benchmark/ior_3_2_1/src/mdtest -d /project/biohpcadmin/zpang1/mdtest_bass_4_thread -i 3 -b 30 -z 1 -L -I 100 -u -t -r
--------------------------------------------------------------------------
WARNING: There was an error initializing an OpenFabrics device.

  Local host:   Nucleus025
  Local device: mlx4_0
--------------------------------------------------------------------------
-- started at 08/19/2020 09:05:33 --

mdtest-3.4.0+dev was launched with 4 total task(s) on 2 node(s)
Command line used: /home2/zpang1/ior_benchmark/ior_3_2_1/src/mdtest '-d' '/project/biohpcadmin/zpang1/mdtest_bass_4_thread' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-r'
Path: /project/biohpcadmin/zpang1
FS: 3536.7 TiB   Used FS: 80.6%   Inodes: 1613.5 Mi   Used Inodes: 40.4%

Nodemap: 1010
4 tasks, 12000 files/directories
[Nucleus006:13942] 3 more processes have sent help message help-mpi-btl-openib.txt / error in device init
[Nucleus006:13942] Set MCA parameter "orte_base_help_aggregate" to 0 to see all help / error messages

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :      11701.077      11102.776      11462.617        258.845
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :       9995.117       9760.503       9906.806        104.053
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :       2916.418       2453.289       2609.949        216.725
-- finished at 08/19/2020 09:05:40 --


real	0m8.069s
user	0m0.044s
sys	0m0.066s

----------------------------------------------------------------------------------------------------------------------------------------------------

\
\

----------------------------------------------------------------------------------------------------------------------------------------------------
### 8 threads

## CREATION: files/directories 2*8*30*100*3 = 144000

 time mpirun --hostfile $HOME/ior_benchmark/mpi_hfile --map-by node -np 8 $HOME/ior_benchmark/ior_3_2_1/src/mdtest -d /project/biohpcadmin/zpang1/mdtest_bass_8_thread -i 3 -b 30 -z 1 -L -I 100 -u -t -w 2k -C
--------------------------------------------------------------------------
WARNING: There was an error initializing an OpenFabrics device.

  Local host:   Nucleus026
  Local device: mlx4_0
--------------------------------------------------------------------------
-- started at 08/19/2020 09:10:27 --

mdtest-3.4.0+dev was launched with 8 total task(s) on 2 node(s)
Command line used: /home2/zpang1/ior_benchmark/ior_3_2_1/src/mdtest '-d' '/project/biohpcadmin/zpang1/mdtest_bass_8_thread' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-w' '2k' '-C'
Path: /project/biohpcadmin/zpang1
FS: 3536.7 TiB   Used FS: 80.6%   Inodes: 1613.5 Mi   Used Inodes: 40.4%

Nodemap: 10101010
8 tasks, 24000 files/directories
[Nucleus006:15135] 7 more processes have sent help message help-mpi-btl-openib.txt / error in device init
[Nucleus006:15135] Set MCA parameter "orte_base_help_aggregate" to 0 to see all help / error messages

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :      10834.745       9696.163      10115.331        510.932
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :       7386.530       7116.394       7275.176        115.229
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :       1469.804       1260.145       1392.751         94.177
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 08/19/2020 09:10:45 --


real	0m18.384s
user	0m0.033s
sys	0m0.083s

\

## RANDOM STAT

time mpirun --hostfile $HOME/ior_benchmark/mpi_hfile --map-by node -np 8 $HOME/ior_benchmark/ior_3_2_1/src/mdtest -d /project/biohpcadmin/zpang1/mdtest_bass_8_thread -i 3 -b 30 -z 1 -L -I 100 -u -t -R -T
--------------------------------------------------------------------------
WARNING: There was an error initializing an OpenFabrics device.

  Local host:   Nucleus026
  Local device: mlx4_0
--------------------------------------------------------------------------
-- started at 08/19/2020 09:14:03 --

mdtest-3.4.0+dev was launched with 8 total task(s) on 2 node(s)
Command line used: /home2/zpang1/ior_benchmark/ior_3_2_1/src/mdtest '-d' '/project/biohpcadmin/zpang1/mdtest_bass_8_thread' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-R' '-T'
Path: /project/biohpcadmin/zpang1
FS: 3536.7 TiB   Used FS: 80.6%   Inodes: 1613.5 Mi   Used Inodes: 40.4%

Nodemap: 10101010
random seed: 1597846443
8 tasks, 24000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :      63858.854      57348.751      59699.077       2948.439
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :      52316.990      49126.772      50485.706       1343.036
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 08/19/2020 09:14:06 --

[Nucleus006:16287] 7 more processes have sent help message help-mpi-btl-openib.txt / error in device init
[Nucleus006:16287] Set MCA parameter "orte_base_help_aggregate" to 0 to see all help / error messages

real	0m3.865s
user	0m0.037s
sys	0m0.063s

\

## REMOVAL

time mpirun --hostfile $HOME/ior_benchmark/mpi_hfile --map-by node -np 8 $HOME/ior_benchmark/ior_3_2_1/src/mdtest -d /project/biohpcadmin/zpang1/mdtest_bass_8_thread -i 3 -b 30 -z 1 -L -I 100 -u -t -r
--------------------------------------------------------------------------
WARNING: There was an error initializing an OpenFabrics device.

  Local host:   Nucleus026
  Local device: mlx4_0
--------------------------------------------------------------------------
-- started at 08/19/2020 09:15:43 --

mdtest-3.4.0+dev was launched with 8 total task(s) on 2 node(s)
Command line used: /home2/zpang1/ior_benchmark/ior_3_2_1/src/mdtest '-d' '/project/biohpcadmin/zpang1/mdtest_bass_8_thread' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-r'
Path: /project/biohpcadmin/zpang1
FS: 3536.7 TiB   Used FS: 80.6%   Inodes: 1613.5 Mi   Used Inodes: 40.4%

Nodemap: 10101010
8 tasks, 24000 files/directories
[Nucleus006:16725] 7 more processes have sent help message help-mpi-btl-openib.txt / error in device init
[Nucleus006:16725] Set MCA parameter "orte_base_help_aggregate" to 0 to see all help / error messages

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :      14578.597      12547.771      13598.550        830.464
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :      11841.080      11224.452      11616.147        277.962
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :       1520.122       1461.758       1481.499         27.313
-- finished at 08/19/2020 09:15:55 --


real	0m12.885s
user	0m0.040s
sys	0m0.074s
----------------------------------------------------------------------------------------------------------------------------------------------------

\
\

----------------------------------------------------------------------------------------------------------------------------------------------------
### 16 threads

## CREATION: files/directories 16*2*30*100*3 = 288000

 time mpirun --hostfile $HOME/ior_benchmark/mpi_hfile --map-by node -np 16 $HOME/ior_benchmark/ior_3_2_1/src/mdtest -d /project/biohpcadmin/zpang1/mdtest_bass_16_thread -i 3 -b 30 -z 1 -L -I 100 -u -t -w 2k -C
--------------------------------------------------------------------------
WARNING: There was an error initializing an OpenFabrics device.

  Local host:   Nucleus026
  Local device: mlx4_0
--------------------------------------------------------------------------
-- started at 08/19/2020 09:22:15 --

mdtest-3.4.0+dev was launched with 16 total task(s) on 2 node(s)
Command line used: /home2/zpang1/ior_benchmark/ior_3_2_1/src/mdtest '-d' '/project/biohpcadmin/zpang1/mdtest_bass_16_thread' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-w' '2k' '-C'
Path: /project/biohpcadmin/zpang1
FS: 3536.7 TiB   Used FS: 80.6%   Inodes: 1613.5 Mi   Used Inodes: 40.4%

Nodemap: 1010101010101010
16 tasks, 48000 files/directories
[Nucleus006:19004] 15 more processes have sent help message help-mpi-btl-openib.txt / error in device init
[Nucleus006:19004] Set MCA parameter "orte_base_help_aggregate" to 0 to see all help / error messages

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :      11311.935       7841.767       9497.393       1421.094
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :       8204.007       7314.786       7784.672        364.766
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :        765.352        566.257        643.504         87.184
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 08/19/2020 09:22:50 --


real	0m35.668s
user	0m0.038s
sys	0m0.072s

\

## RANDOM STAT:

 time mpirun --hostfile $HOME/ior_benchmark/mpi_hfile --map-by node -np 16 $HOME/ior_benchmark/ior_3_2_1/src/mdtest -d /project/biohpcadmin/zpang1/mdtest_bass_16_thread -i 3 -b 30 -z 1 -L -I 100 -u -t -R -T
--------------------------------------------------------------------------
WARNING: There was an error initializing an OpenFabrics device.

  Local host:   Nucleus025
  Local device: mlx4_0
--------------------------------------------------------------------------
-- started at 08/19/2020 09:33:24 --

mdtest-3.4.0+dev was launched with 16 total task(s) on 2 node(s)
Command line used: /home2/zpang1/ior_benchmark/ior_3_2_1/src/mdtest '-d' '/project/biohpcadmin/zpang1/mdtest_bass_16_thread' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-R' '-T'
Path: /project/biohpcadmin/zpang1
FS: 3536.7 TiB   Used FS: 80.6%   Inodes: 1613.5 Mi   Used Inodes: 40.4%

Nodemap: 1010101010101010
random seed: 1597847604
16 tasks, 48000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :     119705.228      92153.261     109470.931      12310.188
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :     109524.637     101840.865     105738.745       3135.516
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 08/19/2020 09:33:27 --

[Nucleus006:21821] 15 more processes have sent help message help-mpi-btl-openib.txt / error in device init
[Nucleus006:21821] Set MCA parameter "orte_base_help_aggregate" to 0 to see all help / error messages

real	0m4.122s
user	0m0.054s
sys	0m0.055s

\

# REMOVAL:

 time mpirun --hostfile $HOME/ior_benchmark/mpi_hfile --map-by node -np 16 $HOME/ior_benchmark/ior_3_2_1/src/mdtest -d /project/biohpcadmin/zpang1/mdtest_bass_16_thread -i 3 -b 30 -z 1 -L -I 100 -u -t -r
--------------------------------------------------------------------------
WARNING: There was an error initializing an OpenFabrics device.

  Local host:   Nucleus026
  Local device: mlx4_0
--------------------------------------------------------------------------
-- started at 08/19/2020 09:35:58 --

mdtest-3.4.0+dev was launched with 16 total task(s) on 2 node(s)
Command line used: /home2/zpang1/ior_benchmark/ior_3_2_1/src/mdtest '-d' '/project/biohpcadmin/zpang1/mdtest_bass_16_thread' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-r'
Path: /project/biohpcadmin/zpang1
FS: 3536.7 TiB   Used FS: 80.6%   Inodes: 1613.5 Mi   Used Inodes: 40.4%

Nodemap: 1010101010101010
16 tasks, 48000 files/directories
[Nucleus006:22313] 15 more processes have sent help message help-mpi-btl-openib.txt / error in device init
[Nucleus006:22313] Set MCA parameter "orte_base_help_aggregate" to 0 to see all help / error messages

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :      14262.070      13875.028      14088.276        160.395
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :      13337.867      12261.747      12827.984        441.088
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :        805.634        791.750        799.978          5.953
-- finished at 08/19/2020 09:36:20 --


real	0m23.061s
user	0m0.035s
sys	0m0.069s

----------------------------------------------------------------------------------------------------------------------------------------------------

```

 

## IOR/mdtest Basic Tutorial

### IOR/Mdtest Setup and Installation

```
## Install openmpi suite   

sudo yum install openmpi openmpi-devel

vim ~/.bashrc
------------------------
export PATH="/usr/lib64/openmpi/bin:$PATH"
------------------------

\

## add command subfolder to $PATH

source ~/.bashrc

-OR-

vim /etc/profile
------------------------
# add openmpi's bin to $PATH:
export PATH="/usr/lib64/openmpi/bin:$PATH"
------------------------

  # log out and back in

\

## download and compile IOR/mdtest 

# make dedicated folder as install location    
mkdir /home/<user>/mdtest

# git clone repo

cd /home/<user>/mdtest
git clone https://github.com/hpc/ior.git
cd ior

# install
./bootstrap
./configure --prefix=/home/<user>/mdtest
make clean && make

# verify 
cd /home/<user>/mdtest/ior/src/
./mdtest --help

# add mdtest to $PATH
export PATH="$HOME/mdtest/ior/src/:$PATH"

  # ensure this is done on all nodes participating in MPI runs
```

\
\

### MPI Run Tutorial

- how to use `mpirun`

```
## Create host file, ON SHARED filesystem so all nodes can access

  # hostnames have to be registered either in DNS or in all nodes' `/etc/hosts`

# e.g. 2 hosts, each with 8 maximum allocatable "slots" or logical cores
------------------
<hostname> slots=8
<hostname> slots=8
------------------

\

## Verify mpi is working

  # stop firewalld or special iptables rules

e.g. on Bass Sandbox
------------------
mpirun --hostfile /gpfs/biohpc/mpi_file_bass --map-by node -np 8 hostname
gpfs-client01.biohpc.swmed.edu
gpfs-client01.biohpc.swmed.edu
gpfs-client01.biohpc.swmed.edu
gpfs-client02.biohpc.swmed.edu
gpfs-client02.biohpc.swmed.edu
gpfs-client02.biohpc.swmed.edu
gpfs-client02.biohpc.swmed.edu
gpfs-client01.biohpc.swmed.edu
------------------

  # `--map-by node` --> ensure all processes/threads are distributed EVENLY across all hosts

e.g. what happens if ABOVE OPTION is NOT SPECIFIED, one host does all the work until all cores are allocated; not evenly distributed processing
------------------
mpirun --hostfile /gpfs/biohpc/mpi_file_bass -np 9 hostname
gpfs-client01.biohpc.swmed.edu
gpfs-client01.biohpc.swmed.edu
gpfs-client01.biohpc.swmed.edu
gpfs-client01.biohpc.swmed.edu
gpfs-client01.biohpc.swmed.edu
gpfs-client01.biohpc.swmed.edu
gpfs-client01.biohpc.swmed.edu
gpfs-client01.biohpc.swmed.edu
gpfs-client02.biohpc.swmed.edu
------------------

```

- **use `mpirun` to run MPI mdtest benchmarks**

  - below consists ONE set of tests for a specific # of mpi processes, to generate a whole data set, repeat for 2,4,8, & 16 mpi
processes (`-np 2`, `-np 4`, ...)

  - check your mpirun host file to ensure that enough cores are made available for each host/node (if one runs -np 16, but in the host file, each node only has max of 4 allocatable slots, then mpirun will return an error) 

```
## both the `host file` and the `benchmark directory` should be on the network filesystems i.e. gpfs

## Follow the specific order to Create --> Stat --> and Remove for keeping the filesystem clean 

## First create directory tree, this yields directory and file creation IO results

time mpirun --hostfile <host file> --map-by node -np <# of mpi processes> mdtest -d <location to create dir tree> -i 3 -b 30 -z 1 -L -I 100 -u -t -w 2k -C

  # `-i 3` --> 3 repeats/iterations  --> e.g. if combined with `-np 2` then EACH node runs 3 iterations of the mdtest = 3x2x30x100 = 18000 files/directories created in this example

  # `-b 30` --> create 30 branches/topmost directories

  # `-z 1` --> traverse depth = 1 

  # `-L` --> create files at the leaf level

  # `-I 100` --> touch/create 100 files at the leaf of each dir 

  # `-u` --> unique filenames to mimic realistic app usage

  # `-t` --> count in overhead in benchmark results(?)

  # `-w 2k` --> each block/file = 2kB, choose small files for gauging metadata/latency performance

  # `-C` --> Run file creation Benchmark

## Next Run Random Stat Test on created dir tree

time mpirun --hostfile <host file> --map-by node -np <# of mpi proccesses> mdtest -d <USE SAME location as BEFORE!> -i 3 -b 30 -z 1 -L -I 100 -u -t -R -T

  # `-R` --> random access pattern

  # `-T` --> run STAT benchmark

## Finally Run Removal Test

time mpirun --hostfile <host file> --map-by node -np <# of mpi processes> mdtest -d <USE SAME location as BEFORE!> -i 3 -b 30 -z 1 -L -I 100 -u -t -r

  # `-r` --> run removal benchmark --> this ACTUALLY cleans up all of the test dir tree created under `-d ../../some_dir`

  # always run this LAST!
```

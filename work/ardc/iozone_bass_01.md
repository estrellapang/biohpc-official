## 

### From the DWDM Days: IOZone Benchmarks -- From Bass

> Nodes: Nucleus006

> Date: 7-26-2020

> IOZONE version: 3.434 (installed as module on cluster)

\

#### Test Parameters

- Check Max Throughput for 12 processes

  - throughput tests can only do one record length/test

  - `iozone -l 3 -u 3 -r 512k -s 1g -F rough_thread01 rough_thread02 rough_thread03`

  - repeat for 6 and 12 threads

- Run sequential & random read/write with thread count locks

  - `-R` to generate Excel report

  - `-i 0 -i 1 -i2` for sequential read & write, then random read & write

  - `-r 4k -r 8k -r 16k -r 64k -r 256k -r 512k -r 1M -r 4M -r 8M -r 16M` for  record length/block size data-set

  - `-s 1g` --> create 1GB tmp files for tests 

  - `-b` to output data file into excel sheet 

  - `-l <# threads> -u <# threads>`

  - `-F <mnt pt1> <mnt pt2> ... <mnt ptn>` 

\
\

### General Guide

\

- Standard Tests

  - sequential tests:

  - `-i 0` read/re-read; `-i 1` write/re-write

  - non-sequential tests:

  - `-i 2` random-read/write

  - `-I` direct I/O

  - `iozone -R -I -i 0 -i 1 -i 2 -r <1M/4M> -s 512M -f /gpfs/biohpc/ARDC_test/iozone -b <>.txt`

\

- Throughput Tests

  - `-I` ??? --> can we combine with directIO mode?

  - `-t <# threads>`

  - `-F` for files

  - `-l <# threads>` 

  - `-u <# threads>`

\

- Offset Latency Tests

  - `-I` option possible?

  - `iozone -Q -r 1m/4m -s 512m -f /gpfs/biohpc/ARDC_test/<>`

  - outputs <rol/rrol/rwol/wol>.dat --> read, re-read, and write, and re-write off-set latency

  - **parse technique**: `cat <>.dat | awk '{print $1,$2}' | tail -n +2 | tr -s '[:blank:]' ',' > <>.csv`

    - adds comma as delimiter for the empty spaces between fields
\
\

### Installation and Setup

wget http://www.iozone.org/src/current/iozone-3-490.x86_64.rpm

yum localinstall iozone-3-490.x86_64.rpm

- add the dir to $PATH; append to `/etc/profile` for persistent setup

export PATH="/opt/iozone/bin:$PATH"


\
\

### References

- [reference 1](https://www.thegeekstuff.com/2011/05/iozone-examples/)

- [reference 2](https://www.cyberciti.biz/tips/linux-filesystem-benchmarking-with-iozone.html)

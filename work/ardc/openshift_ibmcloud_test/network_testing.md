## Long Distance Ntwk Testing: ARDC and IBM Cloud DCs

### Tools

#### iperf3: bandwidth testing

```
# server
iperf3 -s -B <local IP to listen on> -p <receiving port>

  ## `-B` = "bind IP" --- which IP to receive packets from

# clients
iperf3 -c <server IP> -p <target port>

e.g.
---------------------------
iperf3 -B 10.240.128.7 -s -p 23

iperf3 -c 10.240.128.7 -p 23
---------------------------

# QUESTIONS

  # when is `--cport` used? it mandates the `-B` option to be cocurrently used

```

- **references**

  - [stack overflow 1](https://stackoverflow.com/questions/10065379/how-to-specify-iperf-client-port)

  - [iperf output info](https://linuxthrill.blogspot.com/2016/04/iperf-test-network-throughput-delay.html)

\
\
\

## Node Info

- Discrepancies

  - different backend storage systems

  - difference in processor generation, power/frequency configuration

  - effects of page cache (refer to IOR manual) 

### BASS clients

- `gpfs-client01` & `gpfs-client02`

  - 198.215.54.12[4-5]

  - 32GB RAM, 16 Logical CPUs

### ARDC clients

### Cloud clients

\
\
\

## BASS-BASS Test Cases

- Bass Filesystem Attribute:

  - innode, 3 million (changed from 1 million)

```
sudo /usr/lpp/mmfs/bin/mmchfileset gpfs biohpc --inode-limit 3M
```

### Native Client Mount

#### mdtest

- **2x mpiprocesses**

  - 6000 files/directories PER node x 3 repeats -OR-

  - 2 nodes * 3 iterations * 30 branches * 100 files/branch = 18000 files/directories

  - 2 kB blocks

```
## FILE CREATION
-------------------------------------------------------------------------------
time mpirun --hostfile /gpfs/biohpc/mpi_file_bass --map-by node -np 2 mdtest -d /gpfs/biohpc/ARDC_test/mdtest_bass_bass -i 3 -b 30 -z 1 -L -I 100 -u -t -w 2k -C

-- started at 12/31/2020 02:47:22 --

mdtest-3.4.0+dev was launched with 2 total task(s) on 2 node(s)
Command line used: mdtest '-d' '/gpfs/biohpc/ARDC_test/mdtest_bass_bass' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-w' '2k' '-C'Path: /gpfs/biohpc/ARDC_test
FS: 7.0 TiB   Used FS: 75.5%   Inodes: 4.0 Mi   Used Inodes: 25.1%

Nodemap: 10
2 tasks, 6000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :       2671.117        133.715       1775.770       1162.688
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :       1773.104       1550.002       1630.034        101.401
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :       1044.301         76.505        487.646        408.280
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 12/31/2020 02:48:23 --


real    1m1.820s
user    0m0.246s
sys     0m1.155s
-------------------------------------------------------------------------------

## RANDOM STAT
-------------------------------------------------------------------------------
time mpirun --hostfile /gpfs/biohpc/mpi_file_bass --map-by node -np 2 mdtest -d /gpfs/biohpc/ARDC_test/mdtest_bass_bass -i 3 -b 30 -z 1 -L -I 100 -u -t -R -T
-- started at 12/31/2020 02:52:51 --

mdtest-3.4.0+dev was launched with 2 total task(s) on 2 node(s)
Command line used: mdtest '-d' '/gpfs/biohpc/ARDC_test/mdtest_bass_bass' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-R' '-T'
Path: /gpfs/biohpc/ARDC_test
FS: 7.0 TiB   Used FS: 75.4%   Inodes: 4.0 Mi   Used Inodes: 25.9%

Nodemap: 10
random seed: 1609404771
2 tasks, 6000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :       1043.050        912.644        961.471         58.055
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :       1122.285        918.596       1023.043         83.236
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 12/31/2020 02:53:28 --


real    0m36.953s
user    0m0.709s
sys     0m0.997s
-------------------------------------------------------------------------------

## FILE REMOVAL
-------------------------------------------------------------------------------
 time mpirun --hostfile /gpfs/biohpc/mpi_file_bass --map-by node -np 2 mdtest -d /gpfs/biohpc/ARDC_test/mdtest_bass_bass -i 3 -b 30 -z 1 -L -I 100 -u -t -r
-- started at 12/31/2020 02:56:27 --

mdtest-3.4.0+dev was launched with 2 total task(s) on 2 node(s)
Command line used: mdtest '-d' '/gpfs/biohpc/ARDC_test/mdtest_bass_bass' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-r'
Path: /gpfs/biohpc/ARDC_test
FS: 7.0 TiB   Used FS: 75.4%   Inodes: 4.0 Mi   Used Inodes: 25.9%

Nodemap: 10
2 tasks, 6000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :        954.462        781.178        885.630         75.092
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :        938.360        720.606        799.713         98.360
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :       1588.363        577.296       1145.129        422.065
-- finished at 12/31/2020 02:57:10 --


real    0m43.859s
user    0m1.876s
sys     0m2.385s
-------------------------------------------------------------------------------

```

\

- **4 mpi processes**

```
## FILE CREATION
-------------------------------------------------------------------------------
time mpirun --hostfile /gpfs/biohpc/mpi_file_bass --map-by node -np 4 mdtest -d /gpfs/biohpc/ARDC_test/mdtest_bass_bass -i 3 -b 30 -z 1 -L -I 100 -u -t -w 2k -C
-- started at 12/31/2020 09:28:10 --

mdtest-3.4.0+dev was launched with 4 total task(s) on 2 node(s)
Command line used: mdtest '-d' '/gpfs/biohpc/ARDC_test/mdtest_bass_bass' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-w' '2k' '-C'
Path: /gpfs/biohpc/ARDC_test
FS: 7.0 TiB   Used FS: 75.4%   Inodes: 4.0 Mi   Used Inodes: 25.1%

Nodemap: 1010
4 tasks, 12000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :       5373.852       3301.731       4099.517        910.553
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :       2553.413       1977.265       2343.431        259.362
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :        769.870        360.226        585.106        169.625
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 12/31/2020 09:28:35 --


real    0m25.381s
user    0m2.107s
sys     0m4.226s
-------------------------------------------------------------------------------

## RANDOM STAT
-------------------------------------------------------------------------------
time mpirun --hostfile /gpfs/biohpc/mpi_file_bass --map-by node -np 4 mdtest -d /gpfs/biohpc/ARDC_test/mdtest_bass_bass -i 3 -b 30 -z 1 -L -I 100 -u -t -R -T
-- started at 12/31/2020 09:29:58 --

mdtest-3.4.0+dev was launched with 4 total task(s) on 2 node(s)
Command line used: mdtest '-d' '/gpfs/biohpc/ARDC_test/mdtest_bass_bass' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-R' '-T'
Path: /gpfs/biohpc/ARDC_test
FS: 7.0 TiB   Used FS: 75.4%   Inodes: 4.0 Mi   Used Inodes: 30.8%

Nodemap: 1010
random seed: 1609428598
4 tasks, 12000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :       1660.957       1451.103       1527.689         94.583
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :       1717.454       1670.759       1701.007         21.411
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 12/31/2020 09:30:43 --


real    0m45.310s
user    0m2.253s
sys     0m3.377s
-------------------------------------------------------------------------------

## REMOVAL
-------------------------------------------------------------------------------
time mpirun --hostfile /gpfs/biohpc/mpi_file_bass --map-by node -np 4 mdtest -d /gpfs/biohpc/ARDC_test/mdtest_bass_bass -i 3 -b 30 -z 1 -L -I 100 -u -t -r
-- started at 12/31/2020 09:50:33 --

mdtest-3.4.0+dev was launched with 4 total task(s) on 2 node(s)
Command line used: mdtest '-d' '/gpfs/biohpc/ARDC_test/mdtest_bass_bass' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-r'
Path: /gpfs/biohpc/ARDC_test
FS: 7.0 TiB   Used FS: 75.4%   Inodes: 4.0 Mi   Used Inodes: 26.8%

Nodemap: 1010
4 tasks, 12000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :       1387.439       1201.905       1324.312         86.566
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :       1181.708       1079.531       1147.647         48.164
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :       1247.312        913.053       1026.842        155.923
-- finished at 12/31/2020 09:51:32 --


real    0m59.340s
user    0m3.149s
sys     0m5.202s
-------------------------------------------------------------------------------
```

\

- **8 mpi processes**

```
## FILE CREATION
-------------------------------------------------------------------------------
-- started at 12/31/2020 10:18:34 --

mdtest-3.4.0+dev was launched with 8 total task(s) on 2 node(s)
Command line used: mdtest '-d' '/gpfs/biohpc/ARDC_test/mdtest_bass_bass' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-w' '2k' '-C'
Path: /gpfs/biohpc/ARDC_test
FS: 7.0 TiB   Used FS: 75.4%   Inodes: 4.0 Mi   Used Inodes: 25.1%

Nodemap: 10101010
8 tasks, 24000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :       6934.261       4887.273       5625.501        927.943
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :       3803.196       3565.607       3686.365         97.026
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :        652.799        305.276        424.739        161.323
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 12/31/2020 10:19:07 --

1.86user 6.07system 0:33.39elapsed 23%CPU
-------------------------------------------------------------------------------

## RANDOM STAT
-------------------------------------------------------------------------------
-- started at 12/31/2020 10:19:07 --

mdtest-3.4.0+dev was launched with 8 total task(s) on 2 node(s)
Command line used: mdtest '-d' '/gpfs/biohpc/ARDC_test/mdtest_bass_bass' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-R' '-T'
Path: /gpfs/biohpc/ARDC_test
FS: 7.0 TiB   Used FS: 75.4%   Inodes: 4.0 Mi   Used Inodes: 26.8%

Nodemap: 10101010
random seed: 1609431547
8 tasks, 24000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :       2714.219       2318.990       2517.502        161.348
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :       3468.854       2490.639       2865.715        430.667
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 12/31/2020 10:20:02 --

6.56user 10.08system 0:54.89elapsed 30%CPU
-------------------------------------------------------------------------------

## REMOVAL
-------------------------------------------------------------------------------
-- started at 12/31/2020 10:20:02 --

mdtest-3.4.0+dev was launched with 8 total task(s) on 2 node(s)
Command line used: mdtest '-d' '/gpfs/biohpc/ARDC_test/mdtest_bass_bass' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-r'
Path: /gpfs/biohpc/ARDC_test
FS: 7.0 TiB   Used FS: 75.4%   Inodes: 4.0 Mi   Used Inodes: 26.8%

Nodemap: 10101010
8 tasks, 24000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :       2090.450       2031.276       2057.514         24.609
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :       1832.264       1814.349       1821.131          7.928
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :        267.942        148.825        190.012         55.134
-- finished at 12/31/2020 10:21:17 --

6.46user 12.31system 1:15.59elapsed 24%CPU
-------------------------------------------------------------------------------
```

\

- **16 mpi processes**

```
## FILE CREATION
-------------------------------------------------------------------------------
-- started at 12/31/2020 10:21:18 --

mdtest-3.4.0+dev was launched with 16 total task(s) on 2 node(s)
Command line used: mdtest '-d' '/gpfs/biohpc/ARDC_test/mdtest_bass_bass' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-w' '2k' '-C'
Path: /gpfs/biohpc/ARDC_test
FS: 7.0 TiB   Used FS: 75.4%   Inodes: 4.0 Mi   Used Inodes: 26.3%

Nodemap: 1010101010101010
16 tasks, 48000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :       5773.976       4728.818       5309.060        434.386
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :       4376.293       3450.566       3953.849        382.202
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :        331.252        145.471        209.224         86.316
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 12/31/2020 10:22:22 --

6.26user 16.17system 1:05.10elapsed 34%CPU
-------------------------------------------------------------------------------

## RANDOM STAT
-------------------------------------------------------------------------------
-- started at 12/31/2020 10:22:23 --

mdtest-3.4.0+dev was launched with 16 total task(s) on 2 node(s)
Command line used: mdtest '-d' '/gpfs/biohpc/ARDC_test/mdtest_bass_bass' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-R' '-T'
Path: /gpfs/biohpc/ARDC_test
FS: 7.0 TiB   Used FS: 75.4%   Inodes: 4.0 Mi   Used Inodes: 28.8%

Nodemap: 1010101010101010
random seed: 1609431743
16 tasks, 48000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :       3279.053       1954.677       2721.695        560.628
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :       3438.338       3366.523       3409.619         31.013
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 12/31/2020 10:24:01 --

26.41user 37.25system 1:38.31elapsed 64%CPU
-------------------------------------------------------------------------------

## REMOVAL
-------------------------------------------------------------------------------
-- started at 12/31/2020 10:24:01 --

mdtest-3.4.0+dev was launched with 16 total task(s) on 2 node(s)
Command line used: mdtest '-d' '/gpfs/biohpc/ARDC_test/mdtest_bass_bass' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-r'
Path: /gpfs/biohpc/ARDC_test
FS: 7.0 TiB   Used FS: 75.4%   Inodes: 4.0 Mi   Used Inodes: 31.9%

Nodemap: 1010101010101010
16 tasks, 48000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :       2685.283       2195.998       2458.216        201.283
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :       2373.396       1960.192       2189.198        171.616
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :        131.826        112.320        123.606          8.253
-- finished at 12/31/2020 10:26:07 --

14.35user 26.23system 2:06.51elapsed 32%CPU
-------------------------------------------------------------------------------
```

\
\
\


## ARDC-BASS Test Cases

### Native Client Mount

- **Results**

```
################# STARKIST TEST RESULTS ! ###########################
######################## TWO THREADS ################################
-- started at 01/04/2021 16:31:34 --

mdtest-3.4.0+dev was launched with 2 total task(s) on 2 node(s)
Command line used: mdtest '-d' '/gpfs/biohpc/ARDC_test/mdtest_ardc_ardc' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-w' '2k' '-C'
Path: /gpfs/biohpc/ARDC_test
FS: 7.0 TiB   Used FS: 75.4%   Inodes: 4.0 Mi   Used Inodes: 34.4%

Nodemap: 10
2 tasks, 6000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :        919.296        890.936        904.830         11.576
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :        468.619        452.869        463.278          7.360
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :        301.597        151.067        236.433         63.092
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 01/04/2021 16:32:34 --

0.41user 2.62system 0:59.80elapsed 5%CPU (0avgtext+0avgdata 5764maxresident)k
0inputs+8outputs (0major+3978minor)pagefaults 0swaps
#####################################################################
-- started at 01/04/2021 16:32:34 --

mdtest-3.4.0+dev was launched with 2 total task(s) on 2 node(s)
Command line used: mdtest '-d' '/gpfs/biohpc/ARDC_test/mdtest_ardc_ardc' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-R' '-T'
Path: /gpfs/biohpc/ARDC_test
FS: 7.0 TiB   Used FS: 75.4%   Inodes: 4.0 Mi   Used Inodes: 25.5%

Nodemap: 10
random seed: 1609799554
2 tasks, 6000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :        440.819        439.309        439.814          0.709
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :        485.959        468.556        475.790          7.401
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 01/04/2021 16:33:53 --

0.77user 1.97system 1:19.34elapsed 3%CPU (0avgtext+0avgdata 5764maxresident)k
0inputs+8outputs (0major+3969minor)pagefaults 0swaps
#####################################################################
-- started at 01/04/2021 16:33:54 --

mdtest-3.4.0+dev was launched with 2 total task(s) on 2 node(s)
Command line used: mdtest '-d' '/gpfs/biohpc/ARDC_test/mdtest_ardc_ardc' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-r'
Path: /gpfs/biohpc/ARDC_test
FS: 7.0 TiB   Used FS: 75.4%   Inodes: 4.0 Mi   Used Inodes: 25.9%

Nodemap: 10
2 tasks, 6000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :        297.789        286.038        292.362          4.839
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :        245.033        240.161        243.052          2.090
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :        354.310        321.902        336.647         13.390
-- finished at 01/04/2021 16:36:10 --

1.22user 3.15system 2:16.57elapsed 3%CPU (0avgtext+0avgdata 5756maxresident)k
0inputs+8outputs (0major+3976minor)pagefaults 0swaps
######################## FOUR THREADS ###############################
-- started at 01/04/2021 16:36:10 --

mdtest-3.4.0+dev was launched with 4 total task(s) on 2 node(s)
Command line used: mdtest '-d' '/gpfs/biohpc/ARDC_test/mdtest_ardc_ardc' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-w' '2k' '-C'
Path: /gpfs/biohpc/ARDC_test
FS: 7.0 TiB   Used FS: 75.4%   Inodes: 4.0 Mi   Used Inodes: 25.4%

Nodemap: 1010
4 tasks, 12000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :       1822.553       1650.083       1735.840         70.404
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :        919.724        895.504        904.493         10.827
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :        446.012        210.046        300.032        104.152
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 01/04/2021 16:37:11 --

1.74user 5.97system 1:01.50elapsed 12%CPU (0avgtext+0avgdata 6116maxresident)k
0inputs+784outputs (4major+6442minor)pagefaults 0swaps
#####################################################################
-- started at 01/04/2021 16:37:12 --

mdtest-3.4.0+dev was launched with 4 total task(s) on 2 node(s)
Command line used: mdtest '-d' '/gpfs/biohpc/ARDC_test/mdtest_ardc_ardc' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-R' '-T'
Path: /gpfs/biohpc/ARDC_test
FS: 7.0 TiB   Used FS: 75.4%   Inodes: 4.0 Mi   Used Inodes: 26.1%

Nodemap: 1010
random seed: 1609799832
4 tasks, 12000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :        739.741        669.314        698.844         29.851
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :        884.732        758.862        813.599         52.675
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 01/04/2021 16:38:48 --

2.44user 4.43system 1:36.68elapsed 7%CPU (0avgtext+0avgdata 6112maxresident)k
0inputs+832outputs (4major+6449minor)pagefaults 0swaps
#####################################################################
-- started at 01/04/2021 16:38:48 --

mdtest-3.4.0+dev was launched with 4 total task(s) on 2 node(s)
Command line used: mdtest '-d' '/gpfs/biohpc/ARDC_test/mdtest_ardc_ardc' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-r'
Path: /gpfs/biohpc/ARDC_test
FS: 7.0 TiB   Used FS: 75.4%   Inodes: 4.0 Mi   Used Inodes: 26.8%

Nodemap: 1010
4 tasks, 12000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :        568.424        544.535        554.868         10.015
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :        477.010        446.237        461.964         12.571
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :        314.077        237.574        270.050         32.283
-- finished at 01/04/2021 16:41:12 --

3.82user 7.48system 2:23.91elapsed 7%CPU (0avgtext+0avgdata 6108maxresident)k
0inputs+848outputs (4major+6457minor)pagefaults 0swaps
####################### EIGHT THREADS ###############################
-- started at 01/04/2021 16:41:12 --

mdtest-3.4.0+dev was launched with 8 total task(s) on 2 node(s)
Command line used: mdtest '-d' '/gpfs/biohpc/ARDC_test/mdtest_ardc_ardc' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-w' '2k' '-C'
Path: /gpfs/biohpc/ARDC_test
FS: 7.0 TiB   Used FS: 75.4%   Inodes: 4.0 Mi   Used Inodes: 25.7%

Nodemap: 10101010
8 tasks, 24000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :       3103.728       2724.872       2858.451        173.650
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :       1702.448       1665.198       1677.682         17.510
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :        468.783        148.449        258.484        148.758
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 01/04/2021 16:42:21 --

3.10user 12.24system 1:09.29elapsed 22%CPU (0avgtext+0avgdata 6116maxresident)k
0inputs+1656outputs (6major+11321minor)pagefaults 0swaps
#####################################################################
-- started at 01/04/2021 16:42:22 --

mdtest-3.4.0+dev was launched with 8 total task(s) on 2 node(s)
Command line used: mdtest '-d' '/gpfs/biohpc/ARDC_test/mdtest_ardc_ardc' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-R' '-T'
Path: /gpfs/biohpc/ARDC_test
FS: 7.0 TiB   Used FS: 75.4%   Inodes: 4.0 Mi   Used Inodes: 27.0%

Nodemap: 10101010
random seed: 1609800142
8 tasks, 24000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :       1248.383       1178.988       1212.325         28.394
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :       1315.928       1276.231       1291.262         17.577
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 01/04/2021 16:44:17 --

9.07user 12.49system 1:55.82elapsed 18%CPU (0avgtext+0avgdata 6120maxresident)k
0inputs+1768outputs (6major+11326minor)pagefaults 0swaps
#####################################################################
-- started at 01/04/2021 16:44:17 --

mdtest-3.4.0+dev was launched with 8 total task(s) on 2 node(s)
Command line used: mdtest '-d' '/gpfs/biohpc/ARDC_test/mdtest_ardc_ardc' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-r'
Path: /gpfs/biohpc/ARDC_test
FS: 7.0 TiB   Used FS: 75.4%   Inodes: 4.0 Mi   Used Inodes: 28.5%

Nodemap: 10101010
8 tasks, 24000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :       1047.289       1008.931       1021.990         17.891
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :        858.849        791.550        827.327         27.637
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :        248.141        124.099        172.281         54.291
-- finished at 01/04/2021 16:46:56 --

10.91user 17.87system 2:38.86elapsed 18%CPU (0avgtext+0avgdata 6108maxresident)k
0inputs+1760outputs (6major+11316minor)pagefaults 0swaps
###################### SIXTEEN THREADS ##############################
-- started at 01/04/2021 16:46:56 --

mdtest-3.4.0+dev was launched with 16 total task(s) on 2 node(s)
Command line used: mdtest '-d' '/gpfs/biohpc/ARDC_test/mdtest_ardc_ardc' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-w' '2k' '-C'
Path: /gpfs/biohpc/ARDC_test
FS: 7.0 TiB   Used FS: 75.4%   Inodes: 4.0 Mi   Used Inodes: 26.0%

Nodemap: 1010101010101010
16 tasks, 48000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :       4819.728       4173.769       4415.842        287.442
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :       2651.163       2582.848       2613.419         28.342
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :        131.671         61.395         96.070         28.698
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 01/04/2021 16:48:25 --

11.47user 29.70system 1:29.58elapsed 45%CPU (0avgtext+0avgdata 6444maxresident)k
0inputs+7528outputs (13major+21707minor)pagefaults 0swaps
#####################################################################
-- started at 01/04/2021 16:48:26 --

mdtest-3.4.0+dev was launched with 16 total task(s) on 2 node(s)
Command line used: mdtest '-d' '/gpfs/biohpc/ARDC_test/mdtest_ardc_ardc' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-R' '-T'
Path: /gpfs/biohpc/ARDC_test
FS: 7.0 TiB   Used FS: 75.4%   Inodes: 4.0 Mi   Used Inodes: 28.8%

Nodemap: 1010101010101010
random seed: 1609800506
16 tasks, 48000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :       2074.592       2040.038       2057.601         14.110
   Directory removal         :          0.000          0.000          0.000          0.000
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :       2161.970       2132.247       2145.678         12.295
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :          0.000          0.000          0.000          0.000
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :          0.000          0.000          0.000          0.000
-- finished at 01/04/2021 16:50:43 --

27.53user 31.62system 2:17.74elapsed 42%CPU (0avgtext+0avgdata 6444maxresident)k
0inputs+7784outputs (12major+21752minor)pagefaults 0swaps
#####################################################################
-- started at 01/04/2021 16:50:44 --

mdtest-3.4.0+dev was launched with 16 total task(s) on 2 node(s)
Command line used: mdtest '-d' '/gpfs/biohpc/ARDC_test/mdtest_ardc_ardc' '-i' '3' '-b' '30' '-z' '1' '-L' '-I' '100' '-u' '-t' '-r'
Path: /gpfs/biohpc/ARDC_test
FS: 7.0 TiB   Used FS: 75.4%   Inodes: 4.0 Mi   Used Inodes: 31.9%

Nodemap: 1010101010101010
16 tasks, 48000 files/directories

SUMMARY rate: (of 3 iterations)
   Operation                      Max            Min           Mean        Std Dev
   ---------                      ---            ---           ----        -------
   Directory creation        :          0.000          0.000          0.000          0.000
   Directory stat            :          0.000          0.000          0.000          0.000
   Directory removal         :       1683.604       1537.833       1613.504         59.638
   File creation             :          0.000          0.000          0.000          0.000
   File stat                 :          0.000          0.000          0.000          0.000
   File read                 :          0.000          0.000          0.000          0.000
   File removal              :       1483.984       1434.518       1456.565         20.546
   Tree creation             :          0.000          0.000          0.000          0.000
   Tree removal              :        133.383         75.953         96.494         26.141
-- finished at 01/04/2021 16:53:53 --

42.61user 51.99system 3:09.96elapsed 49%CPU (0avgtext+0avgdata 6436maxresident)k
0inputs+8040outputs (12major+21783minor)pagefaults 0swaps
######################## END OF RESULTS! ############################


```

\
\


## ARDC-BASS AFM-Caching

### Information on AFM Cache and Test Environment

- **AFM mode: SW**

  - cache fileset: zorba (device name: fs01)

  - remote mount location (NOT cached): `/biohpcgpfs/biohpc/`

  - AFM fileset mount location: `/gpfs/fs01/zorba/`

    - fileset has 3 million inodes assigned: `/usr/lpp/mmfs/bin/mmchfileset fs01 zorba --inode-limit 3M`

    - all mdtests were written into this path

  - mpi host file location: `/gpfs/fs01/zorba/ARDC_AFM_tests/mpi_starkists_file` 

> test by: `mpirun --hostfile /gpfs/fs01/zorba/ARDC_AFM_tests/mpi_starkists_file -np 9 --map-by node hostname` 

\

- **considerations**

  - how'd AFM pagepool and afm tuning parameters change caching performance?

  - print out current configs after initial run

\

### Results

#### mdtest script location: `/gpfs/fs01/zorba/ARDC_AFM_tests/mdtests/ardc_afm_mdtest.sh`


- **2 threads**

```
## FILE CREATION
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------

## RANDOM STAT
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------


## FILE DELETION
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
```

\

- **4 threads**

```
## FILE CREATION
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------

## RANDOM STAT
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------


## FILE DELETION
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
```

\

- **8 threads**

```
## FILE CREATION
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------

## RANDOM STAT
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------


## FILE DELETION
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
```

- **16 threads**

```
## FILE CREATION
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------

## RANDOM STAT
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------


## FILE DELETION
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
```

\
\
\

#### GPFSPERF

- Specify thread count

- difference between multiprocessing and multithreading

- compile mpi process? intel C compiler NEEDED

####


- dd

- iperf

- gpfsperf

- iozone

\
\

## ARDC-BASS Remote Mount

\
\

 

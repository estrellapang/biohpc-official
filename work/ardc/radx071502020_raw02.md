## Raw Session Script 01

> Context: BioHPC, Network Services, and Vcinity gather via video conferencing session to go live on IB network extension via DWDM waves
> 2nd bash session entailing network bandwidth and sequential read/write performances on ARDC DWDM network

```
To run a command as administrator (user "root"), use "sudo <command>".
See "man sudo_root" for details.

estrella@thinkPad01:.../Users/estrella$ nucleus006
zpang1@198.215.54.58's password:
Last login: Wed Jul 15 13:57:23 2020 from 172.17.145.163

                 Welcome to BioHPC-Nucleus Supercomputer
                     UT Southwestern Medical Center

                                      Based on RedHat Enterprise Linux Server 7
                                                    Cluster ID: Nucleus

-------------------------------------------------------------------------------

                ** Unauthorized use/access is prohibited. **

If you log on to this computer system, you acknowledge your awareness
of and concurrence with the UT Southwestern Medical Center Use Policy. The
University will prosecute violators to the full extent of the law.

-------------------------------------------------------------------------------

Use the following commands to adjust your environment:

'module avail'            - show available modules
'module add <module>'     - adds a module to your environment for this session
'module initadd <module>' - configure module to be loaded at every login

-------------------------------------------------------------------------------
[zpang1@Nucleus006 ~]$
[zpang1@Nucleus006 ~]$
[zpang1@Nucleus006 ~]$
[zpang1@Nucleus006 ~]$
[zpang1@Nucleus006 ~]$ sudo su
[sudo] password for zpang1:
[root@Nucleus006 zpang1]#
[root@Nucleus006 zpang1]#
[root@Nucleus006 zpang1]#
[root@Nucleus006 zpang1]# cd
[root@Nucleus006 ~]#
[root@Nucleus006 ~]#
[root@Nucleus006 ~]#
[root@Nucleus006 ~]#
[root@Nucleus006 ~]#
[root@Nucleus006 ~]#
[root@Nucleus006 ~]# iperf -c 10.100.189.2 -P 8
------------------------------------------------------------
Client connecting to 10.100.189.2, TCP port 5001
TCP window size:  178 KByte (default)
------------------------------------------------------------
[  8] local 10.100.160.6 port 37824 connected with 10.100.189.2 port 5001
[  3] local 10.100.160.6 port 37816 connected with 10.100.189.2 port 5001
[  4] local 10.100.160.6 port 37814 connected with 10.100.189.2 port 5001
[  7] local 10.100.160.6 port 37822 connected with 10.100.189.2 port 5001
[  6] local 10.100.160.6 port 37818 connected with 10.100.189.2 port 5001
[  5] local 10.100.160.6 port 37820 connected with 10.100.189.2 port 5001
[ 10] local 10.100.160.6 port 37828 connected with 10.100.189.2 port 5001
[  9] local 10.100.160.6 port 37826 connected with 10.100.189.2 port 5001
[ ID] Interval       Transfer     Bandwidth
[  8]  0.0-10.0 sec  1.34 GBytes  1.15 Gbits/sec
[  3]  0.0-10.0 sec  1.34 GBytes  1.15 Gbits/sec
[  4]  0.0-10.0 sec  1.33 GBytes  1.14 Gbits/sec
[  7]  0.0-10.0 sec  1.34 GBytes  1.15 Gbits/sec
[  6]  0.0-10.0 sec  1.34 GBytes  1.15 Gbits/sec
[  5]  0.0-10.0 sec  1.34 GBytes  1.15 Gbits/sec
[ 10]  0.0-10.0 sec  1.34 GBytes  1.15 Gbits/sec
[  9]  0.0-10.0 sec  1.35 GBytes  1.16 Gbits/sec
[SUM]  0.0-10.0 sec  10.7 GBytes  9.21 Gbits/sec
[root@Nucleus006 ~]# ib_read_bw -b -a 10.100.189.2
---------------------------------------------------------------------------------------
                    RDMA_Read Bidirectional BW Test
 Dual-port       : OFF          Device         : mlx4_0
 Number of qps   : 1            Transport type : IB
 Connection type : RC           Using SRQ      : OFF
 TX depth        : 128
 CQ Moderation   : 100
 Mtu             : 2048[B]
 Link type       : IB
 Outstand reads  : 16
 rdma_cm QPs     : OFF
 Data ex. method : Ethernet
---------------------------------------------------------------------------------------
 local address: LID 0x11a QPN 0x0306 PSN 0x13b6c3 OUT 0x10 RKey 0x010900 VAddr 0x002aaaad400000
 remote address: LID 0x1d0 QPN 0x021b PSN 0x8cfe73 OUT 0x10 RKey 0x010200 VAddr 0x002aaaad400000
---------------------------------------------------------------------------------------
 #bytes     #iterations    BW peak[MB/sec]    BW average[MB/sec]   MsgRate[Mpps]
Conflicting CPU frequency values detected: 2499.877000 != 1199.951000. CPU Frequency is not max.
 2          1000             0.05               0.05               0.024983
Conflicting CPU frequency values detected: 2499.877000 != 1255.859000. CPU Frequency is not max.
 4          1000             0.10               0.10               0.024912
Conflicting CPU frequency values detected: 2499.877000 != 1247.192000. CPU Frequency is not max.
 8          1000             0.19               0.19               0.024910
Conflicting CPU frequency values detected: 2494.384000 != 1203.002000. CPU Frequency is not max.
 16         1000             0.38               0.38               0.024894
Conflicting CPU frequency values detected: 2299.926000 != 1199.951000. CPU Frequency is not max.
 32         1000             0.76               0.76               0.024830
Conflicting CPU frequency values detected: 2394.531000 != 1286.010000. CPU Frequency is not max.
 64         1000             1.53               1.51               0.024812
Conflicting CPU frequency values detected: 2499.877000 != 1285.522000. CPU Frequency is not max.
 128        1000             3.05               3.04               0.024869
Conflicting CPU frequency values detected: 2397.094000 != 1269.042000. CPU Frequency is not max.
 256        1000             6.10               6.07               0.024853
Conflicting CPU frequency values detected: 2494.506000 != 1227.172000. CPU Frequency is not max.
 512        1000             12.21              12.15              0.024879
Conflicting CPU frequency values detected: 2480.468000 != 2375.854000. CPU Frequency is not max.
 1024       1000             24.42              24.27              0.024854
Conflicting CPU frequency values detected: 2491.821000 != 1199.951000. CPU Frequency is not max.
 2048       1000             48.80              48.45              0.024808
Conflicting CPU frequency values detected: 2499.877000 != 1199.951000. CPU Frequency is not max.
 4096       1000             97.31              96.70              0.024755
Conflicting CPU frequency values detected: 2497.070000 != 1222.412000. CPU Frequency is not max.
 8192       1000             193.33             192.70             0.024666
Conflicting CPU frequency values detected: 2452.758000 != 1218.750000. CPU Frequency is not max.
 16384      1000             377.89             376.05             0.024067
Conflicting CPU frequency values detected: 2500.000000 != 1202.026000. CPU Frequency is not max.
 32768      1000             746.79             745.62             0.023860
Conflicting CPU frequency values detected: 2458.496000 != 1199.951000. CPU Frequency is not max.
 65536      1000             1346.01            1346.00            0.021536
Conflicting CPU frequency values detected: 2300.048000 != 1204.589000. CPU Frequency is not max.
 131072     1000             2012.55            2012.55            0.016100
Conflicting CPU frequency values detected: 2496.337000 != 1279.663000. CPU Frequency is not max.
 262144     1000             2193.78            2193.47            0.008774
Conflicting CPU frequency values detected: 2500.000000 != 1232.666000. CPU Frequency is not max.
 524288     1000             2188.79            2179.30            0.004359
Conflicting CPU frequency values detected: 2497.070000 != 1252.441000. CPU Frequency is not max.
 1048576    1000             2254.58            2253.96            0.002254
Conflicting CPU frequency values detected: 2312.988000 != 1203.613000. CPU Frequency is not max.
 2097152    1000             2240.94            2235.45            0.001118
Conflicting CPU frequency values detected: 2499.877000 != 1199.951000. CPU Frequency is not max.
 4194304    1000             2251.70            2251.70            0.000563
Conflicting CPU frequency values detected: 2500.000000 != 1199.951000. CPU Frequency is not max.
 8388608    1000             2258.14            2258.09            0.000282
---------------------------------------------------------------------------------------
[root@Nucleus006 ~]# /usr/lpp/mmfs/bin/mmlscluster | grep -i quorum
   7   Nucleus003.cm.cluster   10.100.160.3    Nucleus003.cm.cluster   quorum-manager
   9   ems2-hs                 10.100.190.56   ems2-hs                 quorum-manager
  10   NucleusA002.ib1.biohpc  10.100.161.2    NucleusA002.ib1.biohpc  quorum
[root@Nucleus006 ~]#
[root@Nucleus006 ~]#
[root@Nucleus006 ~]#
[root@Nucleus006 ~]#
[root@Nucleus006 ~]#
[root@Nucleus006 ~]# iperf -c 10.100.189.2 -P 8
------------------------------------------------------------
Client connecting to 10.100.189.2, TCP port 5001
TCP window size:  128 KByte (default)
------------------------------------------------------------
[  3] local 10.100.160.6 port 39968 connected with 10.100.189.2 port 5001
[  4] local 10.100.160.6 port 39966 connected with 10.100.189.2 port 5001
[  5] local 10.100.160.6 port 39970 connected with 10.100.189.2 port 5001
[  6] local 10.100.160.6 port 39972 connected with 10.100.189.2 port 5001
[ 10] local 10.100.160.6 port 39980 connected with 10.100.189.2 port 5001
[  9] local 10.100.160.6 port 39978 connected with 10.100.189.2 port 5001
[  7] local 10.100.160.6 port 39976 connected with 10.100.189.2 port 5001
[  8] local 10.100.160.6 port 39974 connected with 10.100.189.2 port 5001
[ ID] Interval       Transfer     Bandwidth
[  3]  0.0-10.0 sec  1.35 GBytes  1.16 Gbits/sec
[  4]  0.0-10.0 sec  1.35 GBytes  1.16 Gbits/sec
[  5]  0.0-10.0 sec  1.33 GBytes  1.14 Gbits/sec
[  6]  0.0-10.0 sec  1.34 GBytes  1.15 Gbits/sec
[ 10]  0.0-10.0 sec  1.34 GBytes  1.15 Gbits/sec
[  9]  0.0-10.0 sec  1.35 GBytes  1.15 Gbits/sec
[  7]  0.0-10.0 sec  1.35 GBytes  1.16 Gbits/sec
[  8]  0.0-10.0 sec  1.36 GBytes  1.16 Gbits/sec
[SUM]  0.0-10.0 sec  10.8 GBytes  9.24 Gbits/sec
[root@Nucleus006 ~]# i^Cread_bw -b -a 10.100.189.2
[root@Nucleus006 ~]#
[root@Nucleus006 ~]#
[root@Nucleus006 ~]#
[root@Nucleus006 ~]# ib_read_bw -b -a 10.100.189.2
---------------------------------------------------------------------------------------
                    RDMA_Read Bidirectional BW Test
 Dual-port       : OFF          Device         : mlx4_0
 Number of qps   : 1            Transport type : IB
 Connection type : RC           Using SRQ      : OFF
 TX depth        : 128
 CQ Moderation   : 100
 Mtu             : 2048[B]
 Link type       : IB
 Outstand reads  : 16
 rdma_cm QPs     : OFF
 Data ex. method : Ethernet
---------------------------------------------------------------------------------------
 local address: LID 0x11a QPN 0x0307 PSN 0x716701 OUT 0x10 RKey 0x8010900 VAddr 0x002aaaad400000
 remote address: LID 0x1d0 QPN 0x0241 PSN 0x35dbeb OUT 0x10 RKey 0x8010200 VAddr 0x002aaaad400000
---------------------------------------------------------------------------------------
 #bytes     #iterations    BW peak[MB/sec]    BW average[MB/sec]   MsgRate[Mpps]
Conflicting CPU frequency values detected: 1339.965000 != 1694.458000. CPU Frequency is not max.
 2          1000             0.05               0.05               0.024875
Conflicting CPU frequency values detected: 1339.965000 != 1680.419000. CPU Frequency is not max.
 4          1000             0.10               0.10               0.024910
Conflicting CPU frequency values detected: 1711.059000 != 1504.394000. CPU Frequency is not max.
 8          1000             0.19               0.19               0.024985
Conflicting CPU frequency values detected: 1711.059000 != 1467.773000. CPU Frequency is not max.
 16         1000             0.38               0.38               0.024874
Conflicting CPU frequency values detected: 1711.059000 != 2077.514000. CPU Frequency is not max.
 32         1000             0.76               0.76               0.024898
Conflicting CPU frequency values detected: 1711.059000 != 2318.237000. CPU Frequency is not max.
 64         1000             1.53               1.53               0.024999
Conflicting CPU frequency values detected: 1711.059000 != 1921.020000. CPU Frequency is not max.
 128        1000             3.05               3.04               0.024875
Conflicting CPU frequency values detected: 1711.059000 != 1897.216000. CPU Frequency is not max.
 256        1000             6.10               6.06               0.024816
Conflicting CPU frequency values detected: 2277.221000 != 1210.205000. CPU Frequency is not max.
 512        1000             12.21              12.15              0.024874
Conflicting CPU frequency values detected: 2277.221000 != 1628.051000. CPU Frequency is not max.
 1024       1000             24.47              24.28              0.024863
Conflicting CPU frequency values detected: 2301.025000 != 1574.829000. CPU Frequency is not max.
 2048       1000             48.78              48.42              0.024790
Conflicting CPU frequency values detected: 2301.025000 != 1199.951000. CPU Frequency is not max.
 4096       1000             97.31              96.68              0.024750
Conflicting CPU frequency values detected: 2301.025000 != 1555.175000. CPU Frequency is not max.
 8192       1000             193.23             192.61             0.024654
Conflicting CPU frequency values detected: 2300.292000 != 1239.135000. CPU Frequency is not max.
 16384      1000             377.66             376.00             0.024064
Conflicting CPU frequency values detected: 2300.292000 != 1838.500000. CPU Frequency is not max.
 32768      1000             745.55             745.55             0.023858
Conflicting CPU frequency values detected: 2256.225000 != 1233.398000. CPU Frequency is not max.
 65536      1000             1372.82            1372.82            0.021965
Conflicting CPU frequency values detected: 2256.225000 != 1662.231000. CPU Frequency is not max.
 131072     1000             2047.83            2047.83            0.016383
Conflicting CPU frequency values detected: 2256.225000 != 1900.146000. CPU Frequency is not max.
 262144     1000             2229.99            2229.17            0.008917
Conflicting CPU frequency values detected: 2256.225000 != 1535.766000. CPU Frequency is not max.
 524288     1000             2254.15            2254.09            0.004508
Conflicting CPU frequency values detected: 2256.225000 != 1530.395000. CPU Frequency is not max.
 1048576    1000             2261.19            2261.19            0.002261
Conflicting CPU frequency values detected: 2301.147000 != 1574.707000. CPU Frequency is not max.
 2097152    1000             2259.70            2259.70            0.001130
Conflicting CPU frequency values detected: 2300.170000 != 1543.945000. CPU Frequency is not max.
 4194304    1000             2265.73            2265.73            0.000566
Conflicting CPU frequency values detected: 2266.601000 != 1527.832000. CPU Frequency is not max.
 8388608    1000             2265.94            2265.89            0.000283
---------------------------------------------------------------------------------------
[root@Nucleus006 ~]#
[root@Nucleus006 ~]#
[root@Nucleus006 ~]#
[root@Nucleus006 ~]# Connection reset by 198.215.54.58 port 22
estrella@thinkPad01:.../Users/estrella$
estrella@thinkPad01:.../Users/estrella$
estrella@thinkPad01:.../Users/estrella$
estrella@thinkPad01:.../Users/estrella$

```

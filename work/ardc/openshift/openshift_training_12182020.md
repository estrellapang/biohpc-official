## RHEL & IBM Training

> contacts:
> Chris Reed - RHEL
> Jonathan Shaw - RHEL

### Concept Introduction

- 9-year support cycle

- automated installation, deployment on-site or to clouds

- can run both on RHEL CoreOS and RHEL standard OS

  - when Openshift upgrades, the backend OS also gets upgraded

- can manage multiple clusters

- **why use containers**

  - faster deployments and upgrades for dependencies

 
\
\

### Features

- remote worker nodes use kubectl to talk to their masters

- Helm charts?

- Serverless environment

  - expose a pod while it's running and spin-down when not used

- Openshift Pipelines may be used to do "rolling upgrades" across pods without downtime

- WorkSpaces --> cloud IDEs


\
\

### Structure

- projects are meant for isolating sets of pods to specific groups

- master gauges the state of all elements

  - kubernetes components

  - openshift API server, lifecyle management, web console

  - Instracture nodes/services (DNS, NTP, etc)

- worker nodes

  - local registry

  - monitor daemons (grafana or xymon or prometheus)

  - router

\
\

### Installation 

- CoreOS updates are automatic when integrated with Openshift (no manual upgrades with RHEL)

  - no maintenance; correlate version with Openshift

- models: full stack automated, pre-existing infrastructure(hook into existing dns and namespaces), hosted on clouds, openshift dedicated
  

\
\

### Extras

- OpenShift Virtualization

  - a VM inside a container?

- use API to expose VMs to namespace and dedicated storage

- what is "Edge Computing"

\
\

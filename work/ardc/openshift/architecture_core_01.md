##


### Core Container Concepts

- containers --> provide dependences needed to isolate processes from their native OS

  - in terms of virtualization, while hypervisors isolate the entire host and its devices, containers only isolate the components i.e. kernel, modules, and namespace, of OS's for the application/services within

  - init containers? must run to completion unlike application containers, actually pre-cursors to application containers in a pod, if init containers FAIL, then its successors cannot launch

\
\

- images

  - binaries containing all requirements for a single container, e.g. resources and dependencies

  - "horizontal scaling" --> deployment of SAME image in MULTIPLE containers across MULTIPLE hosts (HA & redundancy)

  - image versions are distinguished by their hexadecimal hashes

\
\

- Container Registries

  - a repository of container images (e.g. DockerHub, Quay, "registry.access.redhat.com", etc.)

  - allows other hosts to pull the same image uploaded by the original builder, distribution center 

\
\

- Runtime Environments

\
\
\

### Kubernetes

- [details here](https://docs.openshift.com/container-platform/3.9/architecture/infrastructure_components/kubernetes_infrastructure.html)

- Master Nodes

  -  API Servers --> pod assignment and configuration

  - etcd --> manages persistent master state

  - controller/scheduler server --> enforces desired state based on etcd's signals;  watches for resource availabilities and chooses which node deploys the pod [topology here](https://www.openshift.com/blog/linux-capabilities-in-openshift)

  - HAProxy --> optional for HA masters via load balancing

  - performs node validatation and health checks based on node's "objects"

\
\

- Woker Nodes

  - where container applications are stored?

  - RUNTIME ENVIRONMENTS for containers

  - can be instancees of physical or virtual systems

  - requires

    - docker service, kubelet, and a service proxy

\
\

- Kubelet

  - service for CHECKING and UPDATING container manifests (YAML files describing pods)

  - checks containers/pods via the manifests stored to specific locations or HTTP endpoints or API requests

    - kubelet watches changes made by etcd server at `/registry/hosts/<$hostname>`

\
\

- Pods

  - 1 or more containers deployed on ONE host, smallest compute unit -- similar to a VM instance

  - containers within a pod share the resources assigned to the pod

  - attributes defined by YAML

    - namespace, pre-set configs e.g. which image-registry to use, set of containers, TCP/IP ports for containers to bind to the pod's IP, external storage mounts and mount points, restart policies

    - [detailed info on pod definition/YAML elements](https://docs.openshift.com/container-platform/3.9/architecture/core_concepts/pods_and_services.html)

\

- each pod has:

  - internal IP/networking --> its own port range/space

  - assigned storage

  - containers

\

- Pod Behaviors

  - assigned life cycles --> can be definied via policy or exit code to be deleted or retained 

  - generally "immutable" --> CANNOT be modified while running

  - stateless --> do not maintain state from previous instance, fresh every time --> states managed by controllers

\
\
\

### LSF

### Processes



### Configuration Files

\
\
\

## Openshift Components

### References

- [Fedora OKD VMWare guide](https://itnext.io/guide-installing-an-okd-4-5-cluster-508a2631cbee)

- [RHEL Knowledge center on Openshift](https://docs.openshift.com/container-platform/4.6/installing/installing_bare_metal_ipi/ipi-install-prerequisites.html)

### Installation Requirements

- Provisioner node 

  - openshift installation packages/binaries are free to download

  - DNS, HA-proxy all sit here

  - CoreOS and dependency packages for each cluster node type hosted via HTTPS

  - other nodes will network boot and pull the binaries to bootstrap; they can PXE boot or manually

  - cluster installation config yaml file, kubernetes cluster manifests, and ignition-configs (kick-start for CoreOS) --> it servers to partition disks, format filesystems, and create users before the userspace is fully booted

    - [ignition provisioning tool](https://docs.fedoraproject.org/en-US/fedora-coreos/fcct-config/)

- 3 Control/Master Nodes

- however many Worker Nodes

- Provisioning node need BMC/IPMI power control over the rest of the cluster

- NTP, same time  

- Persistent storage, minimally for a registry, standardly NFS

\
\


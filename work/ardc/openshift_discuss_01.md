## Openshift Cluster & Astrocyte Integration

### 07232020: Astrocyte Demo & Openshift Buildi

```
### Astrocyte Overview

- user needs to upload data to Astrocyte with BioHPC backend storage & module stack

  - user chose pipelines and select parameters to run them; upcoming feature, visualize progress

  - download results

- developers add pipelines to project

  - via Git clones; each upload version only needs to do once

- administration

  - "repos" --> pipelines --> Web GUI demonstrates which version of which pipeline was run by whom

  - repos are meant to be private before publishing for UTSW

\

### Integration

- Convert SLURM to LSF scheduler

- Software modules --> Openshift stack

  - we need to containerize each pipeline with its sofware dependencies

- Filecaching will be needed for remote uploads and downloads (AFM)

\

### Containerization w/h Openshift/Kubernetes worker nodes

- which containerization runtime

  - can run Docker-compatible runtimes, so our container images can be Singularity, Docker, etc. 
 

\


### VMware

- VMotion

- Bastion



### Cluster 

## AFM/GPFS

- Caching for ARDC/Cloud; License


\

### Questions

- Vmotion/VMware

- Openshift management

- AFM Caching

```

\
\
\

### 08-07-2020: Update

- OS:

  - RHEL 7.7

- Build:

  - LSF & VMWare Running

    - LSF used as a scheduler & resource manager as opposed to Kubernetes

    - can also do pod scheduler

    - concern: we have SLURM, how could that integrate with LSF

    - LSF has community version

    - Tim; offers LSF-SLURM conversion sheet!

    - Mark III wants us to nominate some base software for testing scheduling 

    - Astrocyte has Singularity containers & not sure if we need to convert to Kubernetes---Singularity works better with MPI---
  - Network:

    - SFP+ 10Gbit connectors


  - Openshift = Kubernetes Cluster

    - 2 controllers

      - some pods, preferrably not

    - 3 workers min

      - "rails", need license; RHEL 7.8: where pods will run applications

    - 2 infrastructure nodes

      - runs majority of cluster pods

- once nodes are up then 


- Bastion host;

  - centOS

  - for Openshift cluster

  - HA Proxy; engres load balancer

  - hold SSH keys

  - also configured to be rail

  - log collecting

- GPFS GUI NEEDED;


\
\

### 08-28-2020: Update from Mark III 

> Mark III affected by Hurricane

- Mark III sent redbook for Spectrum Scaler CSI driver

  - can use multiple remote clusters

- LSF connector to be done

- DNS records

-  

### 09-11-2020:
 
- MA building local DNS server to accomodate Openshift cluster

  - SRV entries?

  - Reverse?

  - nslookup

### 09-18-2020

- Openshift Nodes have inadequate power 110V

  - we need 208V 

  - move nodes to a different rack that can accomodate -OR- purchase PDUs ath can deliver 220V

\
\

### 09-25-2020

- Mark III has lower rated PSUs

  - downside; cannot power on certain peripherals

\
\

### 10-09-2020

#### Cluster info

- **LSF**

  - LSF talks to Kubernetes server --> Server via Kubelet handles the pods

  - on compute "worker" nodes, only LSF running

  - on control nodes, we have kubernetes & LSF client

  - to submit a job: "bsub" --> goes to master or worker VMs by default if we do not specify (?)

  - "oc" command similar to "kube" commands

\

- **To Do**

  - overall architecture --> Kubernetes + Openshift + Astrocyte

  - Openshift & LSF training

  - Scale testing; figure out network 

\

### 10-30-2020

### 11-06-2020

- Info


- **To-do**

 - setup chrony across all nodes

\
\

### Internal Astrocyte Discussion

- split astrocyte into openshift branch for lsf-openshift integration

- word count work flow
 
- lsf-openshift integration

  - user submits runs pipe-line --> lfs --> submits to lsf queue

  - containerization need user-end work (we do not have enough info; has multiple upload download steps; using different types of nodes) --> pressure BICF to containerize their workflows (and independent of BioHPC environment)

  - lsf-openshift --> submit jobs to openshift using lsf

- late-stage; need to containerize NextFlow 

\
\

- **To-Do**

  - submit job openshift via lsf

  - Peng: slurm-nextflow vs. kubernetes-nextflow

  - Zeng: difference between slurm and lsf

  - meeting 11-30-2020


\
\

### Internal Astrocyte Discussion

- RNASeq as a trend is dying down?

- Single-cell gene analysis becoming more prominent?

- JSON's role in parsing workflow options into batch-jobs

- what's NextFlow? WorkFlow control? 

  - what do you specify to chain a set of jobs and processes together?

- IBM wishes for Astrocyte license & we want their proprietary work-flow   

- **News**

  - currently Astrocyte Developer version uses CWL, difficult to translate between CWL & Nextflow

- Astrocyte Info

  - workflows are separated by "projects" --> within each project, severeal versions of pipelines/workflows are available 

  - from developers: develop their pipelines on Astrocyte, and use version control (only UTSW Gitlab) to manage their pipelines

  - naming convention: <>_#.#.# (name,major version breaking parameter structure of previous version, minor version with new functionality, patch with bug fixes)

  - Astrocyte parses the developer YAML configuration files that define metadata of pipeline & properties
    - metadata: author, publication, version

    - container choice, nextflow version, user choices, module files to use

    - YAML converts web input into command line (translated into main.nf)

  - `main.nf` 

    - defines set of processes --> per process resource allocation (CPU,MEM,Queue) --> file input --> modules to use/containers to use --> finally, "***" python syntax for specifying running bash/python commands/scripts

  
\
\

### 12-04-2020: Mark III

- dns forward - to core DNS on headnode - to connect to outside net

- create new namespace for kubectl

- lsf master submit

- bastion kind of a login node

- internal registry - related to self-start
  
\
\

### 12-07-2020: Internal Discussion

- nextflow integration depends on the type of executor to choose 

  - [nextflow executors](https://www.nextflow.io/docs/latest/executor.html)

\

- Openshift/Kubernetes info

  - easier cluster update easier than Kubernetes

  - How to map storage namespace back to Kubernetes??? Containers have separate namespace --> persistent volume plugin ---> test how to mount gpfs filesystem from containers

  - [persistent volume](https://docs.openshift.com/container-platform/4.5/storage/understanding-persistent-storage.html)

  - astrocyte as single user (we have to map that all files are owned by astrocyte user INSIDE the container)

- `bapp -l kube` --> list applications

  - `kube` is the main application! k8s API!

- Use `bsub` to launch pods

  - `bsub -app kube sleep 120`

  - `kubectl get pods`
 
- check `pyi#` YAML on lsf master node

\

- To-Dos 

- mount GPFS on containers, map namespace (correct user and group owner)

  - easy to do on block storage but not sure on filesystems

- how to setup unique user's namespace available to container persistent volume

- nextflow executor for lsf or kubernetes

  - setup own registry
 
\

### 12-11-2020: Mark III Meetings

- DNS fixed; PK jobs hanging in queue - forwarding to Tim to debug

\

### 12-14-2020: Internal Meeting

- check Astrocyte Gitlab page

- Astrocyte Structure

- `astrocyte_cli`

  - test or run work flows

  - e.g. `astrocyte <test/run> --param=name,value.. <workflow>`

  - run `~ --help`

  - for `shiny_prepare` & `shiny-cluster` handles --> developer user need to upload their own shiny packages, the astrocyte starts a VNC server with port-forwardig with unique port #

- workflow_package.py

  - checks and validates yaml schema

  - `astrocyte-pkg.jsonschema.yaml` --> this is more or less just a wrapper, need to add lsd and openshift parameters

  - schema file does not specify executor; the nextflow config file has executor file to use SLURM

  - nextflow calls "Ignite" to allocate nodes? (it's a scheduler on its own) --> `ignite_sbatch.py` --> ignite allocates nodes, and integrates it into its own cluster --> how to integrate it with Kubernetes? Add nodes from Kubernetes cluster?

  - nextflow is supposed to treat SLURM and Ignite equally; but some differences exist; slurm goes to nextflow config, but for ignite, YAML file 

- How to let containers access data --> via a SHARED folder

- in singularity, the container mounts BioHPC directories via user's parent process/session

  - but is this compatible with Kubernetes?

\

- To-do's

  - use nextflow to submit jobs from nextflow to LSF cluster; then onto submitting to Openshift cluster

  - PK to go over port forwarding rules

  - how does Kubernetes work with nextflow?


\

### 01-08-2021

- etcd nodes controller nodes are the database hosters for the cluster

  - the are "master" nodes

- infra nodes test pods, not as critical

- to do's:

  - follow up with Chris on Openshift upcoming training modules

  - run yaml job without lsf scheduler entry

  - reset worker nodes 

  - meet next week to discuss architecture

  - licensing for building our own test environment

\
\

### 01-14-2021 Internal Meeting

#### Ideas and Directions

- How to rebuild image on Openshift/Kubernetes

- Replicate LSF's ability to integrate with Kubernetes

\
\

#### Container Intro by PL

- Images for containers are better performing and fast

  - kernel modules are in container images, and link back to user/host space

  - `.singularity/cache` --> container environment uses user home dir to act as cache (?)

- one app/container image = portable and easy restart 

- multiple app/container image --> more resemble a workflow, but it's not resilient against outtages

- deploy docker/singularity image check permission


\

- **Kubernetes STORAGE**

- PV

  - a kubernetes cluster resource, life cyclde independent of pods that uses it

- PVC - persistent volume claim --> a user storage request by user to the kubernetes cluster

  - reclaim, delete, extend volumes
\
\

- **Docker Storage Types**

- Docker gives root privilege to images

- /tmpfs mount --> stays in memory

- the storage volumes are stored on dedicated location in host's filesystem

  - `/var/lib/docker/volumes/..` 

- "Named pipes" --> communication b/w docker host and container 

- Bind mount --> uses root privilege to map from host filesystem to internal filesystem, trouble translating external namespace to container internal namespace -- how could user inside container recognize or pick up on permissions on the outside?
 
  - solution 1: use cron to manually change all uid and gid to implement permission adjustment inside

  - solution 2: set up docker user while running docker container

  - solution 3: rebuild image by adding host user's UID and GID to container Dockerfile

\
\

- **Singularity Storage Permissions**

- does not grant root privileges

- needs executable permission by everyone in order for imagesto be run

- user cannot rebuild image into their own image and make their own namespace

- mounts current working directory, your home dir, and /tmp by default, and inherits your user's UID and GID default

\
\

### 01-15-2021 Weekly Friday Meeting

- talks of rebuilding the Openshift cluster - keeping BioHPC in the loop

\
\

### 1-28-2021 DD-Openshift Quay Registry Intro

- **tech news**

- Windows can now utilize containers via WSL 

- Containers don't need kernel stack; map proc, c groups to work in Linux (windows kernel is blackbox, makes it difficult to achieve this) 

- "Windows Container Base Images; Windows base OS images" for containers to become cross-platform b/w Windows and Linux

\
\

- **BioHPC's Goal**: become central hub for medical container workflow for UTSW medical community

- can be standalone (entirely!) or a part of openshift cluster

- it's really an opensource registry

- Gitlab of Containers

  - LDAP/active directory integration; multiple authentication approaches e.g. BOTH EMAIL and LDAP

  - integrity checks

  - access control

  - "continuous integration"  --> VERSION CONTROL of IMAGES

  - "Clair", scanner has its own standards of what images are safe or vulnerable via "Open Container Initiative" standards

  - GITLAB integration

\

- A container is NOT an image --> multiple containers can run the same image?

  - look this up

- [opensource quay](https://www.projectquay.io/#contribute)

\
\

### 01-28-2021 PK: Container Storage

- Infrastructure/worker nodes rely on CSI Driversin tandem with gpfs client to access GPFS filesystem

  - RestAPI calls can be made with GUI server with infra nodes

  - is it compatible with other filesystems? object based filesystems 

  - [more info here](https://kubernetes-csi.github.io/docs/drivers.html)

- [k8s storage](https://events19.linuxfoundation.org/wp-content/uploads/2017/12/Internals-of-Docking-Storage-with-Kubernetes-Workloads-Dennis-Chen-Arm.pdf)

- To-do:

  - create namespace; map user namespace to containers

  - connect GPFS servers to LDAP

  - create shared PV across all, and see if LDAP users can access their own files

\
\

### 1-29-2021 Mark III Meeting

- Chris working on non-expiriting Quay license in ~60 days

  - use trial license while waiting

- Hal Kussler is another point of contact for Mark III

\
\

### 02-05-2021 Mark III Meeting

- Mark III working on scheduling Openshift deployment education session/IBM

  - DNS, certificate issue

  - SpectrumScale CSI requires all workers to be UP 

- IBM AFM: ES3000 (flash) serving as cache for ES5000 parallel HDD storage

\
\

### 02-11-2021 Internal Astrocyte Meeting

- Isamu Hartman: Lawyer for IBM-Astrocyte Project

  - 90-days initial phase

  - source code contract w/h IBM: lsf-kubernetes integration code

  - NO MODIFICATIONS or IMPROVEMENTS to source code  --> allowed via contract to modify within Astrocyte platform

  - NAMES need to be added to the WHITELIST - no sharing otherwise

  - client-based trials/showcases are okay

\

- Quay requires LDAP authentication

  - LW wants security check

  - backup LDAP database prior to Quay integration

\

- Quay Deploy Info:

  - 198.215.54.31:8080

  - graphical installation webGUI

  - "NFS" mounts are not supported 

  - setup UTSW mail server as SMTP server (used for user password)
 

\

- Use KVM VMs as opposed to VirtualBox for easy-to-click deployments(?)


### 02-12-2021: Mark III Meeting

- Feb. 22nd, 2021 --> Openshift Installation Meeting

\

### 02-19-2021: Mark III Meeting

- Bryan (former Mark III) left some back files, Tim requests additional outputs from cluster

\

### 02-22-2021: RHEL Support

- check expired/not-started csrs

```
oc get secret -A -o json  --insecure-skip-tls-verify=true | jq -r ' .items[] | select( .metadata.annotations."auth.openshift.io/certificate-not-after" | .!=null and fromdateiso8601<='$( date --date='+1year' +%s )' ) | "expiration: \( .metadata.annotations."auth.openshift.io/certificate-not-after" ) \( .type ) -n \( .metadata.namespace ) \( .metadata.name )" ' | sort | column -t
```

- controller logs manages the cluster certificates

  - etcd running on each node updates the states

- check status of components on controller

  - log in to controller and become root

```
crictl ps

# get logs from individual components

crictl logs <hashed name>

# stop and delete pods

crictl stop <hash> <hash> ..
crictl rm <hash> <hash> ..

  # rm (regenerate all) - `crictl rm -a -f`

```

- check status of pods on Bastion

```
oc get pods 

```

- location of kube-api-server-certs

  - what are static pod resources?

```
openssl x509 -text -noout -in /etc/kubernetes/static-pod-resources/kube-apiserver-certs/secrets/internal-loadbalancer-serving-certkey/tls.crt

  # don't forget about the `../external-loadbalancer-serving-cert-key/.` directory

```

- **regenerate ssl cert**

  - **easiest way**

```

\

# regen certificates

  # use pipe from above to show expired certs to SHOW INDIVIDUAL CERT NAMES:

oc patch secret -p='{"metadata": {"annotations": {"auth.openshift.io/certificate-not-after": null}}}' <cert name> -n openshift-kube-apiserver --insecure-skip-tls-verify=true

- e.g.

oc patch secret -p='{"metadata": {"annotations": {"auth.openshift.io/certificate-not-after": null}}}' internal-loadbalancer-serving-certkey -n openshift-kube-apiserver --insecure-skip-tls-verify=true

oc patch secret -p='{"metadata": {"annotations": {"auth.openshift.io/certificate-not-after": null}}}' external-loadbalancer-serving-certkey -n openshift-kube-apiserver --insecure-skip-tls-verify=true

# accetp csrs
oc get csr -o name | xargs oc adm certificate approve
```

- manual way: needs to manually insert in all cert and key locations, particularly internal and external loadbalancer

```
# bastion:
/root/cert_fix/

# command to re-gen loadbalancer cert:
oc get secret/loadbalancer-serving-signer -o=jsonpath='{.data.tls\.crt}' -n openshift-kube-apiserver-operator --insecure-skip-tls-verify=true | base64 -d > lbsigner.crt

# gen key
oc get secret/loadbalancer-serving-signer -o=jsonpath='{.data.tls\.key}' -n openshift-kube-apiserver-operator --insecure-skip-tls-verify=true | base64 -d > lbsigner.key

# location of script
/root/cert_fix/cert-4x-gen-master/control-plane-recover-certs.sh 

# needs to set `BASE_DOMAIN=ocp.biohpc.swmed.edu` in order for script to work - `export BASE_DOMAIN=ocp.biohpc.swmed.edu`

  # it will generate an `out_cert` directory, where new certs will be located and ready to be dished out to nodes

```

- **accept pending certs**

```
oc get csr -o name | xargs oc adm certificate approve

```

- **list pods for kube-apiserver

```
oc get pods -n openshift-kube-apiserver
```

\

### 02-23-2021: MarkIII - RHEL Debug Further


- generate SOSreport on all controller nodes (which are the MASTER NODES)

  - https://access.redhat.com/solutions/3820762

- check etcd:

  - check who is "leader"

  - check on each controller to see if they all report the same info to see if etcd is working

```
crictl exec `crictl ps --label io.kubernetes.container.name=etcdctl -q` etcdctl endpoint status -w table

```

- pull sosreport

```
podman run -it registry.access.redhat.com/rhel7/support-tools /usr/bin/bash

sosreport -k crio.all=on -k crio.logs=on

  # didn't work

# alternative--> SET PROXY FIRST! 

toolbox 

then run `sosreport...` to enter prompt 

enter case id..02863072

at the end it will show location of tar report file
# afterwards run:

redhat-support-tool addattachment -c 02863072 /host/var/tmp/sosreport-controller-node1-02863072-2021-02-23-oqqnrxi.tar.xz

  # need to export proxy

  # uploads the logs directly to rhel, via MarkIII's account

  # /host/tmp --> we can put additional files into host's /tmp directory and upload files to the RHEL case easily, `toolbox` to enter container, then upload by `redhat-support-tool addattachment -c ... /host/tmp...`

```

- added cert_check script to bastion

```
# under /root/cert_check/cert.sh

  # generates `report` file containing current status of all certs

```

- patched the following certs

```
 oc patch secret -n openshift-config-managed kube-controller-manager-client-cert-key -p='{"metadata": {"annotations": {"auth.openshift.io/certificate-not-after": null}
}}'

# did a few others, check Bastion history
```

- on controller02: reverted back to old pems, restarted kubelet, approved csrs

- on worker06 --> changed dns to appropriate one, restarted kubelet, approved cs

- missing apisever

```
# check operators
oc get co

oc get events -n openshift-apiserver

  # shows unhealthy

# on controller01, crictl ps is missing openshift-apiserver

# pending 
oc get pods -n openshift-network-operator


# get cluster version
oc get clusterversion
  # v 4.5.3 --> rhel recommends updates
```

-  concluded by pulling api logs:

  - [steps to pull error logs when API up](https://access.redhat.com/articles/4971311)

\
\

### Feb-25-2021: Internal Meeting

- **report on debug with MarkIII and RHEL**

  - NTP wrong date, setup chrony across all

  - all controller operator pods like "kube-controller-manager" "kube-scheduler-<server>/<client>" "internal-loadbalancer" they all need valid and not expired TLS cerficates to even start 

  - we did tried manually regenerating the certificates, and inserting them to the right location, but eventually what worked was some built in command `oc patch secret <controller pod>`

  - once we regenerate the certificate, you log in to each controller node and force restart the pods, and they'll generate the updated certificates ---> at which point you have to approve them on the bastion host

  - all nodes have recovered their state, but the scheduler is still not fully up yet, but I think we've already cleaned up a lot of mess, and I think the cluster should be back to operation soon, doing additional health checks on the scheduler pods


\

### Info

- Djando requires a super-user/admin account to run; for example `portal7`

\

### To-Dos

- Present afm cache bench marks on Mar. 8th, 2021


\
\
\

### 03-01-2021: Further Debugging with BioHPC

- applied proxy

  - first create a .yaml file (check commands)

```
oc apply -f proxy.yaml
oc get proxy cluster -o yaml
oc get mcp
oc get mc

# the new configuration will run through all nodes `oc get node` to see which nodes is being worked on---if it's scheduling disabled, then it is working through the configurations

  # worker 4 node not up --> (check commands)

 
oc get pods -n openshift-machine-config-operator -o wide


oc get pods -n openshift-ingress -o wide

oc delete pod router-default-6599c5f984-hfsnk -n openshift-ingress

  # has trouble deleting pods immediately

oc get events -n openshift-ingress

journalctl -u kubelet -f   # follow on problematic controller node

oc get csr --> approve pending

oc adm certificate approve <>

  # wait for additional cert to approve and return a new cert

  # approve again

oc get mcp  --> to see if master is done working

# in worker4:

crictl  ps --> look for router

crictl stop <router pod>

###

# controller-2 has issues

oc get pods -n openshift-kube-controller-manager
oc get events -n openshift-kube-controller-manager  --> pod create error

  # ssh into controller-2, and restarted crios

# deleted problematic pod

oc get pods -n openshift-kube-controller-manager
oc delete pod kube-controller-manager-controller-node2.ocp.biohpc.swmed.edu -n openshift-kube-controller-manager

  # sshed into controller-2 and restarted kubelet--> didn't work, then rebooted controller node 2

# changed ESXI-2's BMC pass to `BioP@ssw0rd1`

<more notes to append, look at terminal history>

```

\
\

### Debug Cont'd: 03-02-2021

```
# VMWare Info: one may configure VMs to boot into the BIOS via vsphere settings --- the BIOS is called "PhoenixBIOS"

# issued apiserver to update its configuration files and certifications

  # this launches installers to implement any changes; 

oc patch kubeapiserver cluster -p='{"spec": {"forceRedeploymentReason": "recovery-'"$( date --rfc-3339=ns )"'"}}' --type=merge kubeapiserver.operator.openshift.io/cluster patched

  # check presence of installers after the previous command

oc get kubeapiserver -o=jsonpath='{range .items[0].status.conditions[?(@.type=="NodeInstallerProgressing")]}{.reason}{"\n"}{.message}{"\n"}'

oc get pods -n openshift-kube-apiserver

  # wait for all installers to come up, not completed

oc get co --> check status of apiserver 

oc get pods -n openshift-apiserver

oc get co openshift-apiserver -o yaml | less 

[disaster recovery docs as reference](https://docs.openshift.com/container-platform/4.6/backup_and_restore/disaster_recovery/scenario-2-restoring-cluster-state.html)

nslookup <ip>  # shows hostname of up

\

due to error:
-----------
bastion ~]# oc logs apiserver-585fff7d89-gdw6c -n openshift-apiserver
Error from server: Get https://172.18.239.208:10250/containerLogs/openshift-apiserver/apiserver-585fff7d89-gdw6c/openshift-apiserver: URLBlocked
-----------
  # ran the following:

oc edit proxy edit
-----------
noProxy: .biohpc.swmed.edu,.utsw.edu,172.18.224.0/20

  # the subnet blocks range from .224 to .239
-----------
```
  # check if "machine configs operators" were created after the proxy change

  # still cannot pull logs --> because we are doing it from the bastion?

# spun up a debug pod on controller to 

oc debug node/controller-node1.ocp.biohpc.swmed.edu

# info on slurm-openshift integration
https://www.openshift.com/blog/running-hpc-workloads-with-red-hat-openshift-using-mpi-and-lustre-filesystem

```

\
\

### 03-08-2021: IBM UK Project

> contacts:
> Vadim Elisseev (project manager), Ritesh Krishna, Samuel Freitas Antao - IBM Cloud on CWI Research
> Frank Lee (IBM)

#### UK Demo

- name: "HCP4B" --> Hybrid Cloud Platform for Bioinformatics

- "Data Broker" for data-management?

  - centralized storage for workflows

  - API to comm with redis

- also uses hybrid cloud design

  - openshift and traditional scheduler as resource manager

  - ApacheSpark, redis, databroker, nodeRed 

  - **diagram**

  - [structure](./astrocyte_openshift_images/ )

- leans towards "In Memory processing"

  - splits input file into small chunks, and sends to Redis, process in memory

  - opposite of "in full processing" or "in part processing" --> where the data uploaded in full or split into individual components

- Node-RED interface --> block coding, similar to signal processing like in LabVIEW

  - each workflow componenet is considered a "node", with different options and gives user the power to graphically design their own work flow

  - pulls the README and description in the GUI for each imported node from **npm repositories**

  - similar to LSF gui as well

  - has its own workflow language backend

  - each node or workflow has an associated docker image, and has a .json file that defines it as an node JS "project", where the container image details are defined

  - the backend is nodeJS and javascript to present the HTML web interface

  - community-driven node definitions like DockerHub

  - DOES NOT SUPPORT multiple-tenenancy -- each user its own server and its own namespace using OPENSHIFT pod  --> and the jobs will be scheduled to the pods as each node runs --> has proprietary scheduling mechanism called "flow" that works with Openshift kube-scheduler

  - has "default" or "agnostic" nodes for default functions, like "Input nodes" into each a user can upload his/her data

  - each node has its own folder and namespace and forwards it from one node to the next, does not map to existing namespaces to production large filesystems, the backend filesystem will be that belonging to the openshift cluster (question -- how does block storage map to namespace?) 

  - BioHPC has "workflow_de" which was DT's own version of the workflow designer

- Observation: Openshift-Singularity integration is of interest

- IBM AFM Observation (Frank Lee)

  - use Spectrum Discover to build a catelog

  - and push prefetch for specific files depending on the job's request, triggered by LSF

- What we have provide them:

  - namespace mapping

  - big data movement

  - nextflow

  - flow library -- integration for LSF and Kube

- What we can learn from them:

  - Redis in memory processing

  - kube integration

  - developer version - AngularJS and Python/Django

- Strategy:

  - Used Astrocyte to lower price for Spectrumscale

  - AVOID the word "LEARN" and use collaborate


\
\
\

### 03-8-2021: Mark III debug cont'd

- Openshift Web Console

  - u: kubeadmin

  - p: smRIP-4zKzb-o4K5T-3cKd9

- turned on the rounter pods, made the openshift webconsole activative once more

  - `oc patch -n openshift-ingress-operator ingresscontroller/default --patch '{"spec":{"replicas": 2}}' --type=merge`

- logged into the console and found that administrative cluster operators such as "image-registry" is not working along with "openshift-insights"

- reimplemented the `proxy.yaml` under bastion's root dir --> simplified the file ALOT

  - watched `oc get nodes` to see if all nodes are available

  - check individual pods inside each operator name space: `oc get pods -n <operator name space, e.g. openshift-insights> -o wide`

  - BEHAVIOR: when `oc apply -f <configuration,e.g. proxy>.yaml` applying new changes, all master nodes will restart, then worker nodes

- after reboot --> `oc project openshift-image-registry`

  - NOTE: this is internal registry, by default it sends to this imagistry, one does not have to necessarily use QUAY

  - once inside project, do `oc get jobs` to see progress: `oc get cj` and `oc get pods`

  - had to force restart pod, because cron job scheduled at midnight for image-pruner

  - `oc create job --from=cronjob/image-pruner manual-pruner` -- to manually restart cron job

  - issue: the pruner job cannot connect to certain http URLs: after creating the cron job, do `oc get pods` then `oc logs <pod name>` to see error

  - error message:

```
oc logs manual-pruner-tscd8
Error from server: Get https://172.18.239.211:10250/containerLogs/openshift-image-registry/manual-pruner-tscd8/image-pruner: URLBlocked
```
  - after waiting, the image-registry came online

- Remaining Issues

  - `authentication` co is not ready, URL cannot connect issues

  - `oc edit proxy cluster proxy`

  - added back: `noProxy: .biohpc.swmed.edu,.utsw.edu,172.18.239.224,172.18.239.225,172.18.239.226,172.18.239.207,172.18.239.208,172.18.239.209,172.18.239.210,172.18.239.21`

\

- PENDING TASK:

  - add worker-6 back into /etc/hosts of all nodes

\
\

### 03-10-2021: Mark III debug


- update:

  - "co":  machine config and networking are degraded

  - issue: worker6's network manager had wrong configurations, used nmtui to change it, or else every time network restarts, all changes to resolv.conf will be wiped

  - waited for worker6 to restart, and checked console

- health check: performed a must-gather --> seems to go through

  - wrote this yaml file: `pijob_2.yaml`

```
apiVersion: batch/v1
kind: Job
metadata:
  name: pi2
  namespace: default
spec:
  backoffLimit: 10
  activeDeadlineSeconds: 1000
  template:
    metadata:
      name: pi2
    spec:
      containers:
      - name: pi2
        image: perl
        command: ["perl",  "-Mbignum=bpi", "-wle", "print bpi(2000)"]
      restartPolicy: OnFailure
```

- job submission and check status:

```
oc apply -f pijob_2.yaml

oc project default

oc get pods
NAME        READY   STATUS      RESTARTS   AGE
pi2-b4xp9   0/1     Completed   0          111s

oc get jobs
NAME   COMPLETIONS   DURATION   AGE
pi2    1/1           32s        7m56s

# to see job output
oc logs pi2-b4xp9
```

- additional errors:

  - on web console --> Monitoring --> Alerting

  - saw "time not synced" error

  - used chronyd, enabled, but there are ways to enable them through the machine config operators

    - [link 1](https://docs.openshift.com/container-platform/4.5/installing/install_config/installing-customizing.html#installation-special-config-chrony_installing-customizing)

    - [link_2](https://docs.openshift.com/container-platform/4.5/installing/install_config/installing-customizing.html)

  - deleted failed pruner jobs from Console GUI

\

- NOTES:

  - much of the admin operators are meant for the admins to monitor, e.g. "insight" talks to the cloud for status updates (kind of like RHEL Satellite)

\
\

### 03-11-2021: Internal Meeting

- LW: major interest in NodeRed's flow library

  - wishes to see the integration with Kubernetes and Openshift

- NOTE: the initial 90-day contract requires us to share full code if snippets are shared

  - CAUTION: try to stick to topology and no more than that

\

#### CWL: Common Workflow Language

- [cwl info](commonwl.org/user_guide)

- **components**

- YAML

  - store value of your variables

  - can also use JSON style/format

\

- CWL file

  - file definition your workflow

  - `class`: class/library of functions

  - `baseCommand`: command, specific function

  - `inputs`: input file and type, we have to specify all

\
\

### 03-12-2021: Mark III Touch-base

- Tim:

  - upgrade container runtime on RHEL/worker nodes

  - configure chrony to run on machine-config operators

  - check lsf integration

  - check csi driver

\
\

### 03-15-2021: IBM UK Meeting

> Contacts: Daryl Williams (IBM Business - LSF)
> Frank Lee (IBM - LSF)
> Samuel Freitas Antao (IBM UK)


- **more on NodeRed**

  - doesn't use NodeJS backend for compute, but use it to only for editor GUI functionality --> connects to second server, openshift cluster for scheduling

  - all of the customization from them in terms of the workflow implementation comes from building specialized nodes -- nodered frame itself is not changed all that much

  - look for npm repositories with more pre-made nodes for this platform --> can be easily uploaded by importing (already active community?)

  - NodeRed is actually programmed for general block-programming, IoT applications, IBM sees it as already very mature

  - (CWL diagram is the old, from 2018) 


\

- **agreements**

  - IBM wants to see how we can implement our own workflows on NodeRed

  - BioHPC wants to open-source Astrocyte User and Developer interface

  - IBM is reluctant to dive into collaborations currently

  - IBM wants to install NodeRed via call for us; need persistent storage for claim; need to rebuild csi **ACTION**

  - IBM does not want to share lsf plugin info

  
\

- on BioHPC OnDemand Application

  - singularity can preserve user privileges and more secure than Docker

  - more popular for HPC/GPU

\

- OpenShift/k8s/Slurm integration

  - openshift works with Docker images

  - slurm/kubernetes integration NOT being considered by IBM

\

- Zero to JuypterHub

  - direct way to deploy JupyterHub to Kubernetes/Openshift cluster

\

- Next Meeting, Next Monday

\
\

### 03-19-2021: Mark III - Debug

#### backup etcd

- [rhel instruction for etcd backup](https://docs.openshift.com/container-platform/4.5/backup_and_restore/backing-up-etcd.html#backup-etcd)


  - perform on one master node is enough:

```
[root@bastion ~]# oc debug node/controller-node1.ocp.biohpc.swmed.edu
sh-4.4# /usr/local/bin/cluster-backup.sh /home/core/assets/backup
e8721d0f6e374614aed61248b6b626fec7824e57a88c08165224b4d810816869
etcdctl version: 3.4.7
API version: 3.4
found latest kube-apiserver-pod: /etc/kubernetes/static-pod-resources/kube-apiserver-pod-52
found latest kube-controller-manager-pod: /etc/kubernetes/static-pod-resources/kube-controller-manager-pod-20
found latest kube-scheduler-pod: /etc/kubernetes/static-pod-resources/kube-scheduler-pod-12
found latest etcd-pod: /etc/kubernetes/static-pod-resources/etcd-pod-2
{"level":"info","ts":1616174499.741968,"caller":"snapshot/v3_snapshot.go:110","msg":"created temporary db file","path":"/home/core/assets/backup/snapshot_2021-03-19_172138.db.part"}
{"level":"info","ts":1616174499.7554379,"caller":"snapshot/v3_snapshot.go:121","msg":"fetching snapshot","endpoint":"https://172.18.239.207:2379"}
{"level":"info","ts":1616174508.1088407,"caller":"snapshot/v3_snapshot.go:134","msg":"fetched snapshot","endpoint":"https://172.18.239.207:2379","took":8.366818169}
{"level":"info","ts":1616174508.109045,"caller":"snapshot/v3_snapshot.go:143","msg":"saved","path":"/home/core/assets/backup/snapshot_2021-03-19_172138.db"}
Snapshot saved at /home/core/assets/backup/snapshot_2021-03-19_172138.db
snapshot db and kube resources are successfully saved to /home/core/assets/backup
.
.
.
[core@controller-node1 ~]$ ls -lt /home/core/assets/backup/
total 1231848
-rw-------. 1 root root 1186668576 Mar 19 17:21 snapshot_2021-03-19_172138.db
-rw-------. 1 root root      67290 Mar 19 17:21 static_kuberesources_2021-03-19_172138.tar.gz

```

\

#### fixing networking

- backed up and appended proxy settings to include gpfs gui

  - this is required for fixing the CSI driver; GUI node related

```
oc get proxy cluster -o yaml > /root/backups/cluster_configs/cluster_proxy_config.yaml

oc edit proxy cluster

# appended: `172.18.239.213` to `noProxy` list

```

- dns entry for nsd1 and nsd2 were wrong---corrected on NucleusT002

  - 172.18.239.227 & 228

- commented out the 172 entries on all nodes' /etc/hosts

  - added search domain via nmtui as well 

\

#### configure machine config for chrony

- [rhel instructions](https://docs.openshift.com/container-platform/4.5/installing/install_config/installing-customizing.html#installation-special-config-chrony_installing-customizing) 

  - created 2 separate files for both masters and workers

  - base64 encoded text file content came from system's chrony.conf

\

-**next steps**

 - [cluster_update_rhel_nodes](https://docs.openshift.com/container-platform/4.5/updating/updating-cluster-rhel-compute.html)

 - set "no_proxy" varialbe on bastion for openshift IPs

 - for ansible update commands: `ansible -i /root/ocp-install/inventory`

\
\

### 03-24-2021: OC Upgrade on RHEL/Worker Nodes

- [node setup instructions here](https://docs.openshift.com/container-platform/4.5/updating/updating-cluster-rhel-compute.html#rhel-compute-updating_updating-cluster-rhel-compute)

  - look under section: "Updating RHEL compute machines in your cluster"

- **proxy pre-requisites**

  - add: `proxy=proxy.swmed.edu:3128` into `/etc/yum.conf`

  - add the following to `/etc/rhsm/rhsm.conf`

```
proxy_hostname = proxy.swmed.edu
proxy_port = 3128
```

- remove `/etc/profile.d/proxy.sh` 

  - `unset http_proxy`

  - `unset https_proxy`

- **NOTE**: after the steps above, then follow steps in link

  - it's not upgrading the oc cluster version, but the kubernetes components version and their dependent/relevant rhel packages 

  - e.g. all worker nodes were pushed to `v1.18.3+cdb0358` and this is the kubernetes verstion notation

\

- **IMPORTANT**:

  - the ansible uses the following inventory file: `/root/ocp-install/inventory`

  - descend to `cd /usr/share/ansible/openshift-ansible` to use ansible command

  - to run upgrade script: `ansible-playbook -i /root/ocp-install/inventory /usr/share/ansible/openshift-ansible/play-books/upgrade.yaml`

  - check what's inside the yaml file

\
\

### 03-25-2021: Internal Discussion

#### Quay

- singulartiy requires files to be 775 for files within the container so that other users can execute it


\
\

### 04-08-2021: Mark III Scale CSI

```
# installed newer versions of spectrum scale, bc the previous oc upgrade moved the kernels from 1062 to 1160

  # worker nodes now running scale 5.0.5-6 due to kernel incompatibilities 

\
\

### RESOURCES

[CSI pods CrashLoopBackOff Issue](https://www.ibm.com/docs/en/scalecontainernative?topic=troubleshooting-debugging-spectrum-scale-container-storage-interface-csi-deployment)

[csi deployment guide](https://marukhno.com/install-and-configure-ibm-spectrum-scale-for-openshift-4-3/)

[IBM kubernetes redbook on Persistent Volumes](https://www.redbooks.ibm.com/redpapers/pdfs/redp5589.pdf)

## cmds to check status of pods

oc describe pod <pod_name>

oc logs <pod_name> -c <container_name>

\

# generate new csisecret

oc get secrets csisecret -o yaml > csisecret_new

# content:
--------------------------
apiVersion: v1
data:
  password: UEBzc3cwcmQxCg==
  username: Y3NpdXNlcgo=   
kind: Secret
type: Opaque
metadata:
  name: scalecsisecret   
  namespace: ibm-spectrum-scale-csi-driver
  labels: 
    app.kubernetes.io/name: ibm-spectrum-scale-csi-operator
--------------------------

  # to undo xs86_64 bit hash, do:

echo <hash> | base64 --decode

# list secret files/pods
oc get secrets

# delete old secret file
oc delete secret csisecret

# create new csisecret object
oc create -f /root/csi_operator_files/csisecret_new

### Scale GUI csi and admin users expired
-----------------------------------------
cd /usr/lpp/mmfs/gui/cli/

# see available users
./lsuser

  # this will display the available users and their <active/expired> status

./chuser admin -p ~P@ssw0rd1~
./chuser csiuser -p P@ssw0rd1
-----------------------------------------

# after this the csi pods were properly running again

\
\

# create static provisioning

  # from bastion

oc create pv -f pv_claim_test.yaml
-----------------------------------------
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-claim-test
spec:
  capacity:
    storage: 10Gi
  accessModes:
    - ReadWriteMany
  csi:
    driver: ibm-spectrum-scale-csi
    volumeHandle: 5911080194486933017;fs01;path=/gpfs/fs01/pv_claim_test
-----------------------------------------

  # volumeHandle format: <clusterID>;<filesystem uid>;path=<specific dir>

# see status of created volumes

oc get pv


\

# create PVC

oc project default

  # pvcs are dependent on namespace, go to where you want to launch your jobs to create your PVCs---contrarily PVs are NOT dependent on which namespace it is in

oc create -f pv_claim_test/pvc_claim_test.yaml
-----------------------------------------
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc-claim-test
spec:
  accessModes:
  - ReadWriteMany
  resources:
    requests:
      storage: 1Gi
-----------------------------------------

# check status of pvc:
oc get pvc

# delete pvcs

oc delete <pv/pvc> <name>

### BEHAVIOR:

  # MULTIPLE PVCs CANNOT USE the SAME PV

  # deleting PV/PVC, default retention policies, backend filesystem data is left undeleted

### NEED TO FIGURE OUT:

  # need to specify PVC which PV to use


### FUTURE DIRECTION:

  # user mapping -- may not be possible with default csi driver static pv pvcs

  # might need to use some jerry-rigged method to remote mount by installing a virtual gpfs cluster inside the openshift cluster

\

# Define Pod using PVC:

  # page 36 of Persistent Volume Redbook

\

# make sure we are in the default namespace!

oc create -f pv_claim_test/nginx_claim_test.yaml
-----------------------------------------
apiVersion: v1
kind: Pod
metadata:
  name: nginx-demo-pod
  labels:
    app: nginx
spec:
  containers:
  - name: web-server
    image: nginx
    volumeMounts:
      - name: mypvc
        mountPath: /usr/share/nginx/html/scale
    ports:
    - containerPort: 80
  volumes:
    - name: mypvc
      persistentVolumeClaim:
        claimName: pvc-claim-test
        readOnly: false
-----------------------------------------

  # check for erros: `watch oc describe pod nginx-demo-pod`
 
\

### Test: Data Retention

  # when pvcs and pvs are deleted, the real data on the filesystem is NOT deleted by default

  # also depends on the Retention policy
```

\
\

### 04-08-2021: Internal Meeting

#### Tasks

- Arrange internal training explaining persistent volumes in kubernetes/openshift + csi driver (it's an API on GUI node? `csi.ibm.com/v1`) 

- Research what user privileges are the pods/pvcs using to access the filesystem

  - how does data generated via pods/PVCs map back to the users, files created in the pod, what are the permissions assigned back to files in the filesystem after pod deployment

  - is `csiuser` similar to root user

- How to connect Openshift to private registries: Quay/Clair(?)


#### Docker Storage: PL

```
# location of docker images and containers
/var/lib/docker

  # ../docker/tmp -or- ../docker/builder --> where docker places files for installing and untarring,etc.


# read-only and writeably parts of the images here:

/var/snap/docker/common/var-lib-docker/overlay2  

# docker can mount a host's folder to the container's folder

  # check `PL_Docker_files_02_data_share.png` for steps 

  # obviously via root privilege

\
\

### Pull Through Cache

# compares local image in local storage to remote registry --> if exists locally, then use local image

  # similar to local repo

  # an Docker registry feature?

```

\
\

### 04-09-2021: Persistent Volume cont'd; Mark III

- [scale csi driver v2.1.0 documentation](https://www.ibm.com/docs/en/spectrum-scale-csi)


```
# NOTE: csi-driver is v2.1.0 

### NGINX Test Pod

# changes to pod yaml
------------------------------------
apiVersion: v1
kind: Pod
metadata:
  name: nginx-demo-pod
  labels:
    app: nginx
spec:
  nodeName: ocp-worker5
  containers:
  - name: web-server
    image: nginx
    volumeMounts:
      - name: mypvc
        mountPath: /usr/share/nginx/html/scale
    ports:
    - containerPort: 80
  volumes:
   - name: mypvc
     persistentVolumeClaim:
       claimName: pvc-claim-test
       readOnly: false
  nodeSelector:
    scale: "true"  # this is the correct syntax for nodeselector
------------------------------------

\
\

# download csi snap debug tool:

wget https://raw.githubusercontent.com/IBM/ibm-spectrum-scale-csi/v2.1.0/tools/spectrum-scale-driver-snap.sh

  # collect debug logs

./spectrum-scale-driver-snap.sh -n ibm-spectrum-scale-csi-driver -o .

  # -n = namespace, -o = output directory

\
\

# CSI behavior: per worker node, a csi pod is created on behalf of them

oc project ibm-spectrum-scale-csi-driver
Now using project "ibm-spectrum-scale-csi-driver" on server "https://api.ocp.biohpc.swmed.edu:6443".

oc get pods

NAME                                               READY   STATUS    RESTARTS   AGE
ibm-spectrum-scale-csi-4bdmx                       2/2     Running   7          26h   # dedicated pod
ibm-spectrum-scale-csi-7lz88                       2/2     Running   7          26h   # dedicated pod
ibm-spectrum-scale-csi-attacher-0                  1/1     Running   0          15d
ibm-spectrum-scale-csi-operator-7b86f5c797-vlcmz   1/1     Running   0          20d
ibm-spectrum-scale-csi-provisioner-0               1/1     Running   0          46h
ibm-spectrum-scale-csi-r78wk                       2/2     Running   7          26h   # dedicated pod


\
\

## general tips:

# get yaml info on node

oc get node <node name> -o yaml 

  # -OR-

oc describe node <node name>

\
\

### TESTING CONCLUSION:

# gpfs fs permission changed to 777 still has no effect on fixing volume attachment, issue not specific to nodes

# errors
-----------------------------------------------------------------------------------
Events:
  Type     Reason              Age                  From                     Message
  ----     ------              ----                 ----                     -------
  Warning  FailedMount         3m27s (x6 over 17m)  kubelet, ocp-worker5     Unable to attach or mount volumes: unmounted volumes=[mypvc], unattached volumes=[mypvc default-token-hswjk]: timed out waiting for the condition
  Warning  FailedAttachVolume  2m4s (x8 over 17m)   attachdetach-controller  AttachVolume.Attach failed for volume "pv-claim-test" : attachdetachment timeout for volume 5911080194486933017;fs01;path=/gpfs/fs01/pv_claim_test
  Warning  FailedMount         73s (x2 over 12m)    kubelet, ocp-worker5     Unable to attach or mount volumes: unmounted volumes=[mypvc], unattached volumes=[default-token-hswjk mypvc]: timed out waiting for the condition
-----------------------------------------------------------------------------------
```

- **some starter links on kubernetes pod yamls**

- [on object labels and selectors](https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/)

- [on pod assignments](https://kubernetes.io/docs/tasks/configure-pod-container/assign-pods-nodes/)

- [security contexts/permissions on pvs](https://stackoverflow.com/questions/50156124/kubernetes-nfs-persistent-volumes-permission-denied)

- [lengthy pod deployment yaml with interesting options](https://github.com/kubernetes/minikube/issues/9871)


\
\
\

### 04-15-2021: CSI driver debug with IBM

```
# the pv_claim_test.yaml has WRONG filesystem uid

mmlsfs fs01 --uid

# replaced `fs01` with the uid returned from command

# deleted pvc, pc

oc get pv,pvc

oc delete pvc pvc-claim-test 

oc delete pv pv-claim-test

# then recreated the pv, pvc, and test nginx pod

oc get pod -w  # watch pod status

oc get pods -o wide  # see which worker the container has been assigned to, and which csi pod is assigned to which worker

# failed still, check logs

oc get volumeattachments

oc logs ibm-spectrum-scale-csi-attacher-0

oc logs ibm-spectrum-scale-csi-4bdmx -c ibm-spectrum-scale-csi  # worker05 csi pod at the time

# restarted csi pod for worker05

oc delete pod ibm-spectrum-scale-csi-4bdmx

oc logs ibm-spectrum-scale-csi-992kc -c ibm-spectrum-scale-csi # new pod

\
\

# test with dynamic provisioning

`storage_class.yaml`

----------------------------------
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
   name: ibm-spectrum-scale-csi-fileset
provisioner: spectrumscale.csi.ibm.com
parameters:
    volBackendFs: "fs01"
    clusterId: "<clusterID>"
reclaimPolicy: Delete
----------------------------------

oc create -f storage_class.yaml

# test with fileset pod

dynamic_prov.yaml

----------------------------------
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: scale-fset-pvc
spec:
  accessModes:
  - ReadWriteMany
  resources:
    requests:
      storage: 1Gi
  storageClassName: ibm-spectrum-scale-csi-fileset
----------------------------------

oc create -f dynamic_prov.yaml

oc get pvc -w  # watch: the fileset pod's status will be shown here

oc logs ibm-spectrum-scale-csi-7lz88 -c ibm-spectrum-scale-csi

# check provisioner pod

oc logs ibm-spectrum-scale-csi-provisioner-0

# check if fileset created: mmlsfileset fs01 ---> appears to be so

# the dynamic PVC generated is stuck in 'pending' state

  # suggests issue CSI has problems communicating with kubernetes API

# vendor recommends re-deploying csi-driver altogether, or restart the GUI server

  # on scale cluster mmlsfileset fs01 returns '--filesetdf yes' --> vendor says it's not recommended

# restart gpfs_gui

systemctl restart gpfsgui  

oc get pvc --> the pvc is now bound!

# deleted nginx-demo-pod and recreated for static provisioning

  # still didn't work --> going to attempt nginx demo file

'pv_claim_test'

# deleted pod name

# claimName: `scale-fset-pvc`

oc create -f nginx_pv_test_2.yaml   # 'nginx-demo-pod1'

  # RPC error, possible bc hostname mismatch

oc get csiscaleoperators.csi.ibm.com ibm-spectrum-scale-csi -o yaml

# check if csiuser can accss GUI node

curl -i -u "csiuser:P@ssw0rd1" -X GET https://172.18.239.213:443/scalemgmt/v2/filesystems/fs01 --insecure

  # this has the proper client worker/node names, which should be entered exactly into the csioperator yaml file


# change CSIOperator.yaml file, and gave the full hostnames! and trimmed all unncessary info---the new YAML file in `CSIScaleOperator_2.yaml`

- [original simple yaml template](https://raw.githubusercontent.com/IBM/ibm-spectrum-scale-csi/v2.1.0/operator/deploy/crds/csiscaleoperators.csi.ibm.com_cr.yaml)

# delete all csi drivers
oc delete csiscaleoperators.csi.ibm.com ibm-spectrum-scale-csi

# de-deploy csi pods
oc apply -f CSIScaleOperator_2.yaml

oc get pods -w

# recreate the nginx demo pod with dynamic provisioning --> the dynamic pod created successfully!

# but static provisioning still doesn't work

oc get pv <ID> -o yaml 

  # check volume handle to see if it matches with that listed in that demo pod's PVC, matches..


# edit the nginx_demo YAML, deleted the "pod" option, and recreated the static demopod

# DEBUG/LOG steps

  # check volume attachments

  # check csi provsioner

  # check csi attacher logs

  # check csi pod on workers --> get from `oc get pod <pod_name> -o wide`

  # oc describe test pods

# check on local workers which scale volumes are being used

ls -ltrR /var/lib/kubelet/pods/* | grep fs01

```

\
\

### Mark III Update: 04-16-2021

- Tim proposes re-install LSF deployment after CSI driver

\
\

### 4-22-2021:Internal Meeting

#### CSI Update

- static provisioing resolved

- to SSH remote into the demo pod

  - `oc rsh <pod_name>`
  
  - e.g. `oc rsh nginx-demo-pod`

- Question of Object vs. Block storage

  - LW suspects that it's block-based storage we PV/PVCs are being leveraged to launch pods

  - can users create PVCs? not just root

\
\

### Openshift CSI Driver Deep Dive

- [csi official git page](https://github.com/container-storage-interface/spec/blob/master/spec.md)

- [kubernetes blog on CSI introduction](https://kubernetes.io/blog/2019/01/15/container-storage-interface-ga/)

- [detailed info on CSI internals](https://arslan.io/2018/06/21/how-to-write-a-container-storage-interface-csi-plugin/)

- [gRPC breakdown](https://www.freecodecamp.org/news/what-is-grpc-protocol-buffers-stream-architecture/)

- [IBM docs on scale CSI driver](https://www.ibm.com/docs/en/spectrum-scale-csi?topic=210-introduction)

- [IBM scale CSI driver CoreOS mounts](https://medium.com/possimpible/ibm-spectrum-scale-container-native-storage-access-7f4e91cc34b8)


\
\

#### Upcoming topics

- IBM spectrum scale CSI installation

  - show pods

  - critical nature of registered hostnames

  - log info as a function of rRPC interceptor

  - testing? UID mapping?

  - limitations?

\
\

- Persistent Volume deploy options
  
  - advantages and disadvantages of static vs dynamic allocation

    - manual administration vs. automation

  - retention policies

  - map individual user's UID and subdir

\
\

- submit pod as BASH session, create files, check permission

\
\
\

### Private Registries

- [kubernetes docs: how to pull images from private registry](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/)



\
\
\

### 07-06-2021: Volume Permissions and Configs

- [user and group permissions](https://stackoverflow.com/questions/43544370/kubernetes-how-to-set-volumemount-user-group-and-file-permissions)

- [dynamic provisioning with GlusterFS](https://computingforgeeks.com/configure-kubernetes-dynamic-volume-provisioning-with-heketi-glusterfs/)

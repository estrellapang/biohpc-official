## Training Session with MarkIII and IBM

### Contacts

- Larry Adams (IBM)

- Dr. Frank Lee from UTSW?

### cmds

- kubernetes is assigned as a resource
 
- `lshost -w` 

  - lsf master & worker nodes have resource called "mg"

  - lfs compute1&2: no resources

- Openshift uses cryodaemon

```
bsub -m <individual host/hostGROUPS> -Is bash


# run on docker resource

bsub -R "select[docker]" -Is bash
```

- `bhosts` --> kind of like `sinfo`

  - shows all nodes

  - `-w` more verbose --> need to be `lsfadmin`

- `lsload`

- `bhost -l <hostname>`

- `badmin hclose -C "comment" <hostname>` --> drain/close a node

- `bslots` ?

- `bqueues` --> check queues

- `bsub -J "test[1-10000]" sleep 0

  - `bjobs -A` --> check jobs status 

  - `bjobs -l <job array>` --> check status

- job scripts can be based on BASH

 - ends with a bsub command

 - can use curl 

- `bmgroup` --> list groups

   - 

### UI "IBM Spectrum Computing"

- monitor node usage very efficiently

  - can report historical utilization/distribution

  - which nodes has most failed jobs

  - which nodes are utilized most

  - guarantee pool; ppl's reserved nodes can be borrow

### LSF + Containerization

- Spectrum LSF does the container startup & filesystem mounts; users do not gain elevated privilege

- GPU reservations is passed into container by LSF

- Admins can access all attributes of a container

- Note: our deployment

  - lsf not running as daemon

  - job submissions need to specified via manifest for worker nodes
 
- Job submissions

  - use bsub for bare-metal job submission via `-app` option

  - use bsub for non-containerized jobs with w/o `-app` option   

  - note: this is not RUNNING a job inside pod!

```
### Syntax
Begin Application

NAME = <container name>
CONTAINER = <image location and options>
DESCRIPTION = <comments>

End Application
```
- ABOVE are stored in LSF config

```
cd $LSF_ENVDIR

cd ../../../lsp.application

the containers can be pulled from a container hub like from docker hub
```

- **kubecontrol** to submit as batch jobs to containers

- **kubeflow** can also submit jobs 

- LOOK into how to go into interactive jobs inside containers

- LSF does thread based scheduling/if users want cores they need to specify


\
\

- exit codes in LSF

  - "Process Manager" --> creates a pipe-line in terms of providing stop points for jobs



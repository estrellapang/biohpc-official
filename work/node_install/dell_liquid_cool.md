## Zoom Meeting with Dell for Liquid Cooling Demo

> 7-24-2020

> Contacts:
> Liquid: 
> - Luke Quick
> - Sumit
> - Jamila Gunawardena 
> Dell:
> - Clint Baker

### Presentation

- CDI: Composable Disaggregated Infrastructure

  -  provides alternative approach; lower-cost, higher performance compared to Cloud

  -  Liquid software & Dell hardware

  - organize in a way similar to a scheduler

    - use as you deploy --> based on workload, the resourcesa are allocated accordingly

    - move CPUs & GPUs around

  - bare metal as opposed to use of hypervisor

  - slurm, kubernetes, MAAS integration via Rest API (uses Swagger UI---ready-to-go APIs are 

    - how does software instruct loading of Nvidia kernel modules during hot-plugging (~15 secs to bring up)  

- cold boot takes long time to reinitialize; this is on the fly

- LIMIT:not splitting compute nodes like VMs to create dual sockets

- LIMIT: DRAM is dedicated, but the Intel OPtane is the diaggregated memory

  - DRAM can be failed over to erroneous nodes

- supported kernels: all 
  
- it's like the hardware implementation of VMware/Kubernetes

- every new re-configuration creats a new PCIe map

- Advantages: Optane + GPU still thermally hot, disaggreagation is a work around

- 

\
\

- Creates what's called "Liqid SuperPods"  --> scale better

  - interconnected with PCIe Fabric Switches + Compute nodes, JBOX (storage + storage) -- has internal cooling & very loud; (~8000W support per box; tested with high-end GPUs---passive cooling) ---> has different GPU capacities; this modular approach can allow servicing without taking down entire nodes

  - the PCIe switch is at the CORE: interconenct limited to inter-rack; cannot span across differnt rows---supports Ethernet & InfiniBand; PCIe fabric gets really expensive over long distances.

  - Gen4 PCIe fabric/switch 300-400 nano second latency per hop 
 
  - not all components need to be disaggregated --> could be modular; compute, storage, network,  

 

### Liqid follow up 09/01/2020

> Luke Quick --> Liqid
> Client Baker --> Dell
> Rich Vo (Liqid?)


### DEMO

- Resources are added to "Groups" 

  - nodes can be added to groups

  - similar to partitions in SLURM  

- can hot-plug SSDs and resources on provisioned machines:

  - go into the pools and re-program

- for SLURM integration

  - need to edit slurm conf for additional PCIe device

  - restart slurmd needed -- need automation

- for nodes that have never had GPUs before

  - need to first reduce numbers to 0

  - GPUs are in Persistent mode

  - re-scan bus command sent out every time new cards are added


- If GPU removed from nodes loaded with SLURM, the node will need reboot? 

  - need to stop slurmd first, edit out the slurm.conf and gres.conf

- Documentation of REST API calls for SLURM

  - look into vendor's UI addr  

- 10.204.105.70:8080 --> UI portal

- Network Connectivity

  - Manger switch used PCIe ID to recognize the boards & differentiate the nodes for assigning

    - identifiers are unique

  - each node has HBA, 4xPCIe from one host to switch

  - each JBOD boxes have
 
- each connection for meshed network could be limited by PCIe lanes

- Each PCIe switch can handle up to how many nodes?

- 2 R640s in Test demo

  - connected to 20x JBOD boxes

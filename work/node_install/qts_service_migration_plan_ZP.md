## QTS Datahall Move Planning: ZP

### DDN Lustre

```
1. Sync data from DDN storage to new IBM storage

  a. Select and drain 2x128GB + 5x 32GB CPU nodes as datamovers

  b. Analyze filessytem quota, list top 20 lab/groups by usage

  c. Draft rsync script for each datamover node, parallelize rsync processes for large folders

  d. Create cron on Nucleus003/006 to report on rsync progress twice/day

  e. Finalize data transfer:

    i. convert source /project directories to read-only mode (parallelize conversion)

    ii. repeat rsync scripts 1-2 times, check for errors, permission mismatches,etc.

    iii. mount /project at CUH storage as read-only, inform users

\

2. Powering down and dis-assembly of DDN storage (**pending on DDN vendor**)

  a. contact vendor, formalize storage move plan

    i. graceful filesystem shutdown

    ii. disassembly and transport logistics

    iii. reassembly planning at QTS

\

3. DDN Powerup at QTS

  a. (optional - run SFA OS and firmware upgrade && update Lustre version on DDN cluster)

  b. schedule DDN support for powerup and integrity check

    i. homework: ensure QTS switch is added to the same BioHPC VLAN for Ethernet, and InfiniBand network has been extended from UTSW to QTS (if there will be stretch cluster configured i.e. UTSW will locally host compute nodes)

    ii. powerup storage, check for controller errors; initialize Lustre filesystem, check data integrity

    iii. unmount /project from CUH, remount /project from DDN
```

\
\

### Lamella Cloud Gateway Storage System

```
1. Hardware

  a. Purchase 2x Nodes to be Lamella Hosts

    i. replace 16 core with 24 - 36 Intel Plantinum series processor

   ii. replace 192 GB (2400MHz) of RAM to 256GB DDR4 (3200MHZ+, DDR5 if availble by 2022)  

   iii. replace HDD local disk with NVMe SSDs, 2 - 4 TB/node

      iiib. (optional: configure each Lamella node to be "hot node" Lustre clients---use local NVMe SSD as cache to read/write into Lustre storage)

   iv. install new InfiniBand NIC, EDR/HDR (100-200 GB/s), if dual-ported NIC cards, set up IPvIB bonding per card, enable active-passive mode to enhance availability

\

2. SQL Database Storage

  a. Benchmark and compare 4 SQL clustering configurations

    i. conventional: single SQL server hosted on one Lamella node, second Lamella node granted root privilege to SQL database over the network

      ia. SQL server saved to local NVMe SSD  - method 1

      ib. saved to Parallel storage, via optimized local memory caching (if possible) - method 2

    ii. SQL clustering: two SQL servers in Master-Slave configuration, database stored in parallel storage OR locally - method 3

   iii. Containerize SQL server (further brainstorming needed) - method 4

  b. fine tune OPcache for SQL memory utilization

\

3. "LamellaHA" -- HAProxy and KeepAlive Upgrades

  a. currently both servers installed in SINGLE VMware host --> establish HA for BOTH KeepAlive and HAProxy to run active-passively across two LamellaHA nodes

  b. build new LamellaHA server on upgraded ESXI node with SSD disks

\

4. Nextcloud

  a. rebuild from version 16 (current), import database, cloud data directory, and production nextcloud configuration

  b. incremental version upgrade from 16 to 21

  c. fine-tune PHP settings to boost webserver performance

  d. establish true multi-instance Webhosting, all HTTPS traffic evenly distributed across TWO Nextcloud instances

    i. resolve session mixup issue

  e. enable new Nextcloud native-apps for users, i.e. enable previews for more file types (csv, xlx, pdf, etc.), possible limited file edit function (previously only viewing,browsing, upload, and download)

  f. enhance web server appearance - has to look  "cooler" and more "large-scale"

- Lamella Storage size --> 17TB

  - owned by `apache`

```
lamella01 ~]$ tail /tmp/cloud_data_report_06182021.txt
7.2G    /cloud_data/be309172-38ce-4c6d-a379-9c4ffecf236a
708K    /cloud_data/996d5ddd-528f-44cf-a693-23b7e93df57b
5.0M    /cloud_data/098174e4-9087-451f-879b-b33032b3b1b7
4.6M    /cloud_data/ed5c4480-90f3-4b2b-82c4-adaa16e50863
4.6M    /cloud_data/299f9437-35a9-4228-af9b-0e0925b3924e
7.9M    /cloud_data/01171b5a-ec43-437e-9cd5-18f08a4f1d17
23G     /cloud_data/7bebf22d-492b-4e5d-9485-8468333a7f59
456M    /cloud_data/f890d429-f3a4-458b-a8cd-ca10bf90b66d
18M     /cloud_data/9282e02a-7587-4f1c-bac2-472ed7f90aac
17T     /cloud_data/
```

\

5. VSFTP Fine Tuning

  a. increase performance by scaling up # of processes/data streams allowed per node

```

\
\

### External Cloud, LamellaSE, LDAP002

```
1. schedule downtime --> take OVF

2. pre-transfer OVF files to designated QTS ESXi servers

3. import VMs on QTS EXSi

  a. requires proper networking setup on ESXi host

4. Roll-back, if migration attempt fails

  a. shutdown all copies at QTS

  b. restart UTSW instances
```

## Liqid Testing

### 09-23-2020

- prolog & epilog --> slurm time out parameters???

- PCIe4 switch; Infiniband storage systems --> how do we connect?

\
\

### 10-15-2020

#### Liqid API Training

- Terms & Info

  - BioHPC is not using the SLURM database; we are using the job management script to hold jobs; on 005 called nodelimiter --> holds the job, determines priority and assigns resources & then releases the jobs

  - "POC" (leased equipment)

- Questions:

  - list available resources to users; user via SLURM allocate them --> when job is done resources are released

\
\

- Contacts:

  - Joe Vavrik (regional director/sales manager)

  - Blake Scott & Jeff McGhean (Engineering)

  - Jamila (Engineering team)

\
\

- CLI:

  - no CLI, everything is controlled via UI through the Swagger API

- Slurm-Integration:

  - no integration yet

- Swagger API

  - 10.204.105.38 --> REST api documentation

  - curl to get parameters to generate URL to fetch response

\
\


### 10-22-2020

> LIQID Demo: API & CLI 
> Scott Cannata: Slurm integration demo

- CLI

```
# program script

`liqidcli.py` 

# e.g.

liqidcli.py reset --IP <IP>

# can create nodes & assign & offload resources

```

- SLURM

```
### Approach #1

# slurm controller

restarting the controller restarts the switch

  # via restAPI comms to switch and create groups/resources/machines dedicated to slurm poop

# has option to reinitialize/reboot node before assigning resources


### Aproach #2

"OnDemand"



# Concern/Questions:

- requirements to current SLURM server

- slurm server needs python & liqid_plugin

  - no hardware needed   

- between jobs; individual nodes will get allocated or offload

- restart slurmctld

```

\
\

### To-Do/Discussion

```

python version;
slurm version

```


### Fun:

```
"test bed"


```

- NEWS

```
# kubernetes/openshift integration

# Redfish/Openstack integration

# can create virtualNICs/drivers INSIDE LIQID group


```


#### BioHPC Expectatationsbb

- Resource reporting




- Web integration of SLURM via webUI



- CLI

#### 10-27-2020: Internal Meeting

- Slurm is essentially a process manager

  - slurm is parent ID for submitted jobs 

- Job limiter/epilog/prolog to work with the LIQID CLI

  - prolog assigns resources prior to the starting of a job --> integrate with liqid CLI

  - job limiter as a cron job does not assign resources

  - epilog?

- New Equipment will be shipped to integrate with Liqig

  - no need to worry about current integrating with existing environment

  - biggest concern is re-intialization

  - demonstrate submitting a job

\
\

### 11-02-2020: Follow-Up Meeting with Liqid

- CLI/SLURM in sandbox

- server restart?

- Client handling jobs 

- developer node (Scott.Cannata)

  - over-provisioning com to SLURM server, and liqid plugin to create resources

- BioHPC SLURM Server v. 16.0.5.8 mentioned to them

- Liqid interested in the time-line of BioHPC's integration


\
\

### 01-06-2021

> contacts:
> Liqid Engineers:
> Blake Scott, Joe Vavrik, Jose Faria (developer)

### Demo Notes

- To add a new nodes, need to restart slurmctld

  - modifying slurm conf to add entries
 
- show partition on slurm?

- **to do**: show how we add nodes to slurm controller on the fly 

\
\

### 05-10-2021: Liqid SLURM Demo

- liqid pcie switch holds and queues the job, and then submit it based on requested resources

  - once job completes, the transiently provisioned node is "kept warm" for a default of 9 minutes, in the case that other jobs will require similar resources (decreases reinitializing time)

  - works with "ResumeTimeout" "SuspendTime" "SuspendTimeout" parameters

  - after the "warm" time, the provisioned node will be shutdown and will need to restart/reinitialize node--the IPMI governs the power-up/power-down

  - Nvidia Persistent Mode/Power-saving mode may be the cause for need to reinitialize

- supports multiple fabrics/networks --> slurm server can access hardware from different networks

- has PCIe4 HBA/NICs that are backwards compatible with PCIe3 servers (nice)

- News

  - Texas A&M, TAC are also testing LIQID

- demo notes:

  - can successfully talk to slurm server and submit jobs and provision nodes(no need to restart SLURM server)

  - on second submission, once nodes are WARM, the job deployment takes much faster

  - if user require more resources than what's available on the pool, then the job will simply wait and be queued

  - users can now access the gui with the "readonly" mode

- desired features:

  - be able to output available resources and present it to users

  - restrict resource allocation based-on active directory

  - bandwidth results of PCIe network

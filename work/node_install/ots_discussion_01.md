## OTS Data Hall Plan

### contacts

> Tracie Moseley (System Engineer for IR; equivalent of a Kham for diff team) 
> William Halbert (Network Services, leads Brandon)
> Tim Mckeen (VP of Network Services)
> Sahara Lee (Director of SysOps)
> Patrick Chambers (Academic IR research)
> Dennis Pfeifer (VP CTO of Health Sys)
> Gary Upton (Network guy reports to Sahara?)

### 12-02-2020: Floor Plan

- 18 miles of fiber between UTSW-OTS

- 10 year lease!

- April 1st, 2021 --> first access/infrastructure completion date

  - Leasing of space won't officially start until August 1st, 2021

  - LW plans for sotre to move to OTS in may 2020


- no force kickouts

- tighter control; networking and powering now NOC employee responsibilities

- 12000 sq. ft --> 30-40% increase 

  - reduced storage space

- separate entrance for COLO vs management

- vendor/test area racks (raised floor, 2nd floor)

  - currently have production there (according to QTS)

  - complete/partial air-gap between test and production

  - traditionally it's airgaped on the 8th floor of Bass

  - proposed environment is called "Pegasus"

- NOC workspace, main office outside data floor; pod/satellite space might sit inside "Burn-in/Tape Storage"

- long ramp leading up to the data-hall floor 

- all 10G fiber switches (for SFP+) --> even for management nodes

- Sahara Lee mentioned room for additional fiber based network equipment

  - backup network; move storage to backup network

  - shoot for 100Gbit bandwidth

- DWDM rings - 2x waves 100 Gbits each, primary and backup

  - chose DWDM bc of only 2 dark fiber available going outbound for UTSW
\

- "Proposed Tape Storage" --> new build room 

\

- **COLO** --> caged in as usual

- 22 + 7 (TACC) racks counted for BioHPC

  - 20 racks of grow space

  - another 12 racks (marked in blue above TACC nodes) for expansion

- LW's plan

  - IB core switch box

  - move storage out by May 2021

  - move headnode there first, then downtime only have to be storage cutover? 

- 1st row Radiology + Police Department

- 2nd row for growth

- 3rd row for HPC

- currently main fiber cabinets are opposite of COLO, William/LW wants dedicated core switch space/fiber distribution (LW wants DWDM networking)
 
- Questions & Info

  - Test environment, need to access datasets from production storage

  - InfiniBand to access parallel storage, maybe a GPU node to process some data-set, then in that case we cannot entirely have strict air-gapped system

  - fair comparison
\

- To-Do:

  - LW works with William Herbert (Blair) to define DWDM needs 

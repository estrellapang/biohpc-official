## Intel Briefing on 3rd Gen Xen Scalable Chip

### Contacts:

- Dietrich Brian (vendor for UT); Kirkendall Keith ("Solutions Architect" Intel team)

### Summary:

- **Xeon Ice Lake**

  - 10nm Ice Xeon Lake (previously, Sandy Bridge --> Skylake --> Cascade Lake, 14 nm) chip (good for small form factor) 

  - +18% IPC (instructions per clock performance increase)

  - reflects the pre-core performance

  - core count increase

  - Speed Select as a new feature

  - Hardware acceleration for deep learning?

  - would benefit from HDR IB Switches 

  - splits into "Platinum" and "Gold" series (and "Extreme core" & "High core" count dyes; extreme core dyes are larger and present more cores)

    - Extreme Core(XCC) series will ship at the end of the year to March 2021, followed by HCC series around June 2021---HCC will always be 8 weeks behind)

    - maximum of 40 cores 

- Intel Speed Select

  - allows adjustable CPU frequency by turning on/off cores to elevate frequencies of other cores

  - can be controlled from user/application level softwares (or BIOS) by talking to the mailbox of CPU comm with kernel to alter the frequency (requires root permission to configure)----on the fly? apply in job sripts?

  - can reduce a chip's core count and in exchange increase the frequency per core (see screenshot); beneficial for single core, sequential jobs
  
  - can allocate "high performance cores" and "low performance cores"---via differential power allocation

\
\

- New Optane Memory
  
  - 6 to now 8 channel DDR4 I/O chanel

  - PCIe 4 (x64 lanes)


- same folks gave us the intel persistent memory

  - decent for sequential data but not for  random read  

- BioHPC recently purchased GPUs with NVLink

- LW expresses interest to purchase a small-scale cluster

  - high density + 300 nodes

  - more than 1U/server

- Intel will be releasing a new GPU "XE" or "SE" (end of next year?)

  - check out "Arctic Sound" GPU---mid-tier GPU

- "OEMs"

  - trial units

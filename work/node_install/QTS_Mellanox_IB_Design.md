## Network Equipment and Layout for QTS Datahall

> Contacts:
> Kashif Chauhan (Main engineer, Mellanox, Nvidia - based in Israel)
> Ortal Bashan (Long-haul engineer, Mellanox)
> Kent Snider (director, Central Mellanox, Nvidia)
> Clint Baker (Dell, organizer)

### Proposed Topology

- 6 "spine" core switches --> 4 uplinks to each leaf switch; 12 uplinks between each spine

  - QM7800 and QM7890 (HDR) 

  - expandable up to 20 racks (with 4 extra ports per switch), 48 node each (assuming 1 line/node)

  - splitter cable will be used --> each port connects to 2 nodes, 100Gbit/line, 2 lanes shared/adapter port (requires ConnectX-6 to meet the 50Gbit/lane bandwidth --- EDR cards cannot meet this threshold, passing max of 25Gbit/lane)

- EDR = 4x lanes of 25Gbit

- HDR = either 8 lanes of 25 Gbit or 4 lanes of 50Gbit


\
\

### Requirements

- Existing 700 nodes will use old Leaf switches

- Newer 300 nodes will use new HDR leaf switches

- plan is to gradually replace old leaf switches

\
\

### Long-Haul/

- MetroX-2 (dark fibre only) --> 40km/25mi range

  - C-band support

  - 2x EDR long haul & 8x HDR local

  - supports darkfibre but NOT DWDM

- DWDM model, MetroX-2XE (IB in, Ethernet out) to arrive March-April 2021 --> same port density, may require unique transceiver

  - supports along side with RoCE DWDM boxes ("cienna" boxes)

  - 40km range, dedicated management app

  - 2xIB port(connectX-6 for IB; RDMA --> RoCE) --> 2x100Gbit Ethernet --> DWDM box
  
  - supports encryption 

  - these boxes alone do not have a presence on the IB network

- Intermediate Switch --> IR manages this?

\
\

### Hardware

- QM7800 & QM7890 --> HDR switches/40 ports/switch

- ConnectX-6 Adapters

- Possible InfiniBand to Ethernet ("SkyWay" =  ~$25000/SERVER!)

  - IPoIB traffic, not RDMA

  - alternative if IB-DWDM doesn't work --> IB to Ethernet --> Ethernet to DWDM

  - 300-400 ns latency?
 
  - VPI cards on each side, using IB Router layer 3, not layer 2 ---> each appliances are NOT SWITCHES (layer 2), but servers using protocol translation

  - 8 Ports Ethernet and 8 Ports IB

  - uses firmware to do protocol translation, not exactly software based IPoIB routing

  - 16Gbit/connection - link aggregation possible = 2x 4xport bundles --> active-active(HA) or increase in bandwidth? (in IB only processing) 

  - Definitely supports link aggregation on Ethernet protocol

  - supports HDR connections

  - needs Ethernet/Cisco box inbetween to connect to DWDM boxes --> need TCPIP assignment 

\
\

### To-Do

- 2x TX6280 DWDM-capable demo units  

- Which DWDM band is supported by IR

- Could IR assign us DWDM waves for testing, using ARDC?

- next meeting Thursday 21st, 12-2 P.M.

\
\

### 01-26-2021 Meeting

- next meeting Feb. 4th, 2021

\
\

### 02-20-2021: Meeting with IR

> Brandon Blasingame
> William Halbert (Blair?)
> Kevin Maney


- IR clarifies that no dark fibre can be leveraged

- MetroX2-XE/XC

  - uses QSFP28 adapters

- RoCE/datalink layer

- IR is hesitant

  - "from an engineering perspective, it doesn't make sense"

- **IR DWDM uplinks**

  - 100Gbit/LR Ethernet uplink - single mode fibre

- **To-Do**
  
  - IR to build new IB network backbone/core-switches in MMB/CUH (Materials Management Building)

  - send requirements for MMB core-switches

- **Proposed Solution**

- a chassis is needed for multiple 2x100Gbit cards

  - expandable

  - Mar - May for DWDM equipment arrival

\
\

### 02-04-2021: Follow-up wtih Mellanox/Nvidia

> Mellanox Engineers
> Kashif Chauhan
> Ortal Bashan

- Current quote 10 core HDR switches

  - EDR and HDR cable new purchase as well

  - LW asks for at least 3 quotes from diff vendors (use university policy as leverage)
 
- MetroX-2 XC - DWDM connectivity solution

  - IBswitch-MetroX-DWDM-MetroX-IBswitch
  
  - requires adaptive routing on IB networks on both sides -- to avoid "contention", also for load balancing

  - per MetroX switch, max 100/GbE, two ports --> no link aggregation

  - LR cables/transceiver QFP28 (?) 

  - earliest available date: ~ end of Q2..?!

  - MetroX as an alternative for testing

\

- IPoIB -- Skyway

  - CANNOT support DWDM devices in the middle; buffer-size too small, designed for bridging local networks together

  - high connections would cause high latency

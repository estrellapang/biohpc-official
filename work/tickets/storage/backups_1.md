## Tickets Related to Backups

### Ticket#2019080610008039 

- original inquiry: 

```
Do you have snapshot on BioHPC?

 

I deleted 4 tar.gz files by mistake around 5mins ago from this location - /project/Neuroinformatics_Core/Takahashi_lab/s160136​

 

Is there a way to recover them?

```

- investigate:

```
[root@Nucleus006 ~]# cd /work/.backup/backup_project/.config/
[root@Nucleus006 .config]# 
[root@Nucleus006 .config]# 
[root@Nucleus006 .config]# 
[root@Nucleus006 .config]# ls
rsnapshot-BICF-archive.conf              rsnapshot-greencenter-Lin_lab.conf    rsnapshot-shared-bicf_workflow_ref.conf
rsnapshot-BICF.conf                      rsnapshot-immunology-Hancks_lab.conf  rsnapshot-shared-zhu_bicf.conf
rsnapshot-biohpcadmin.conf               rsnapshot-PHG.conf                    rsnapshot-urology-Strand_lab.conf
rsnapshot-cellbiology-Friedman_lab.conf  rsnapshot-radiology.conf
rsnapshot-CRI-CRI_GMDP.conf              rsnapshot-radiology-FILMS.conf

```

- conclusion:

```
data not backed up

```

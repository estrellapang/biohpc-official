## GPFS Ops on Nucleus: 

### Sep. 3rd, 2019: Ticket#2019090310008006

- Inquiry: `> /work not mounted on NucleusC003`

- Diagnosis:

```
# SSH'ed in, found that the `/etc/fstab` entry was missing 

data                 /work                gpfs       rw,mtime,atime,quota=userquota;groupquota;filesetquota;perfileset,dev=data,noauto 0 0 

# `mount -a`   --> does not WORK --> error message about node not belonging to GPFS cluster 

```


- Solution: Node "Polluted" --> needs to re-add node into GPFS cluster and start GPFS agent

  - links: 

    - [link 1](https://docs.biohpc.swmed.edu/dokuwiki/doku.php?id=gpfs&s[]=gpfs&s[]=nfs) 

    - [link 2](https://docs.biohpc.swmed.edu/dokuwiki/doku.php?id=gpfs_client_configure&s[]=gpfs&s[]=nfs) 


```

/usr/lpp/mmfs/bin/mmsdrrestore -p nucleus006 

/usr/lpp/mmfs/bin/mmstartup 

  # wait for a minute or two 

/usr/lpp/mmfs/bin/mmgetstate  

df -Th  

  # at this point /work should be mounted


```

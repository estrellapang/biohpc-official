## Issues on Lustre & `/project`: July 2019 


### Ticket#2019071210008083

- request:

```
 green Fri Jul 12 16:52:46 CDT 2019 - Files ok

&green <a
href="/xymon-cgi/svcstatus.sh?CLIENT=biohpcwsc015.biohpc.swmed.edu&amp;SECTION=file:/home1/.home1">/home1/.home1</a>

&green <a
href="/xymon-cgi/svcstatus.sh?CLIENT=biohpcwsc015.biohpc.swmed.edu&amp;SECTION=file:/home2/.home2">/home2/.home2</a>

&green <a
href="/xymon-cgi/svcstatus.sh?CLIENT=biohpcwsc015.biohpc.swmed.edu&amp;SECTION=file:/project/.project">/project/.project</a>

&green <a
href="/xymon-cgi/svcstatus.sh?CLIENT=biohpcwsc015.biohpc.swmed.edu&amp;SECTION=file:/work/.work">/work/.work</a>


See http://xymon.biohpc.swmed.edu/xymon-cgi/svcstatus.sh?HOST=biohpcwsc015.bioh[..]

```

- diagnosis:

  - `/project` is not mounted

  - `lsmod | grep -i 'lnet'`  --> loaded

  - `lsmod | grep -i 'lustre'` --> not loaded 

  - `modprobe -v lustre` --> cannot load, error message `network is down` 

  - netstat -a | grep -i '988'  --> port being utilized by `/home2` server as opposed to LNet 


- fix: 

  - move lustre mount ABOVE home2 mount in `/etc/fstab` 

  - reboot 


- future counter-measures: 

  - prevent /home2 nfs server from utilizing port 988 (reserved for Lustre LNet!)  



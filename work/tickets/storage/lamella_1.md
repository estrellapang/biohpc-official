## Tickets Related to Lamella-Cloud Aug 2019-

### Ticket#2019090510008093 

- **Issue**
 
  - user can map `/work` but not `cloud` and `/home2`, the latter of  which should be mapped by default 

- **Troubleshooting** 

> NOTE: cloud database lives inside lamella01

```

# User's gid DOES NOT MATCH ownership of his `home2` 

[root@Nucleus006 zpang1]# ll /home2 | grep -i xzhan9
drwx------   94 xzhan9       BICF_Core             8192 Sep 14 20:25 xzhan9

[root@Nucleus006 zpang1]# id xzhan9
uid=152189(xzhan9) gid=26001(PCDC_Core) groups=26001(PCDC_Core),11000(Bioinformatics),15000(PHG),15002(PHG_Research),11003(Xiao_lab),11007(Li_lab),26000(PCDC),26101(xiao_hoshida)


[root@Nucleus006 zpang1]# ll /home2/xzhan9

drwxr-xr-x 306 xzhan9 PCDC_Core     12288 Aug  8 01:21 anaconda2
-rw-r--r--   1 xzhan9 PCDC_Core 499266771 Jul 25 09:55 Anaconda2-2019.07-Linux-x86_64.sh
lrwxrwxrwx   1 xzhan9 BICF_Core        26 Dec 12  2016 anno -> /home2/xzhan9/project/anno
drwxr-xr-x   2 xzhan9 PCDC_Core        10 Jan  8  2019 antibiogram
lrwxrwxrwx   1 xzhan9 BICF_Core        21 Aug 11  2018 antibiogram.0803 -> proj/antibiogram.0803
lrwxrwxrwx   1 xzhan9 BICF_Core        23 Dec 23  2015 apps_database -> /project/apps_database/
drwxrwxr-x   5 xzhan9 BICF_Core        61 Apr 28  2017 arrayfire-3
-rw-r--r--   1 xzhan9 PCDC_Core 164772868 Dec  3  2018 bicf.new.lfs
-rw-r--r--   1 xzhan9 PCDC_Core 164772868 Dec  3  2018 bicf.new.sorted
-rw-r--r--   1 xzhan9 PCDC_Core 158340106 Dec  3  2018 bicf.old.lfs
-rw-r--r--   1 xzhan9 PCDC_Core 158340106 Dec  3  2018 bicf.old.sorted
drwxr-xr-x   8 xzhan9 BICF_Core     16384 Sep  9 17:08 bin
lrwxrwxrwx   1 xzhan9 BICF_Core        27 Mar 16  2017 bingshan -> /home2/xzhan9/work/bingshan
drwxr-xr-x   3 xzhan9 BICF_Core        34 Jun 11  2018 code
drwxr-xr-x   3 xzhan9 BICF_Core        36 Jul  2  2018 cwls
drwxr-xr-x  11 xzhan9 BICF_Core      4096 Aug 26 16:41 data
drwxr-xr-x   6 xzhan9 PCDC_Core       115 Feb 12  2019 defor
drwxr-xr-x   3 xzhan9 BICF_Core        38 Feb 22  2019 Desktop
-rw-r--r--   1 xzhan9 PCDC_Core    640271 Dec  4  2018 diff.result
drwxr-xr-x   3 xzhan9 BICF_Core        28 Apr  5 00:24 Documents
drwxr-xr-x   8 xzhan9 BICF_Core       169 Jul  4  2018 dotFiles
drwxr-xr-x   8 xzhan9 BICF_Core       169 Jul  4  2018 dotFiles.bak
drwxr-xr-x   3 xzhan9 BICF_Core       116 Apr 16 11:11 Downloads
-rw-r--r--   1 xzhan9 BICF_Core      1033 Oct 31  2018 du.bicf.usage
-rw-r--r--   1 xzhan9 BICF_Core      1200 Jul 29  2018 du.out
lrwxrwxrwx   1 xzhan9 BICF_Core        20 Sep  2  2015 emacs -> dotFiles/emacs/emacs
-rw-r--r--   1 xzhan9 PCDC_Core     30659 Nov  8  2018 example.pptx
lrwxrwxrwx   1 xzhan9 BICF_Core        13 Oct 20  2015 gecco -> project/gecco
lrwxrwxrwx   1 xzhan9 PCDC_Core        59 Jun 19 14:41 GVHD -> /work/archive/PCDC/PCDC_Core/jkim23/projects/MetaPrism/GVHD
-rw-r--r--   1 xzhan9 BICF_Core        75 Nov  6  2015 harvey
drwxr-xr-x   2 xzhan9 PCDC_Core        29 Aug 16 16:16 igv
lrwxrwxrwx   1 xzhan9 PCDC_Core        72 May  8 12:41 immune_checkpoint_inhibitor -> /work/archive/PCDC/PCDC_Core/jkim23/projects/immune_checkpoint_inhibitor
drwxr-xr-x   3 xzhan9 BICF_Core        25 Sep  2  2015 intel
-rw-r--r--   1 xzhan9 PCDC_Core        63 Jun 20 22:46 jupyter.ip.txt
drwxr-xr-x   3 xzhan9 BICF_Core        40 Apr 25 09:57 jupyter_notebooks
-rw-r--r--   1 xzhan9 BICF_Core       135 Aug  4  2018 load.gpu.module.python2.sh
-rw-r--r--   1 xzhan9 BICF_Core        87 Jul 18  2018 load.gpu.module.sh
drwxr-xr-x   4 xzhan9 PCDC_Core        41 Jun 25 06:51 local
drwxr-xr-x   2 xzhan9 BICF_Core        10 Sep 28  2015 Music
lrwxrwxrwx   1 xzhan9 BICF_Core        25 May 11  2017 mycode -> /home2/xzhan9/proj/mycode
drwxrwxr-x   4 xzhan9 BICF_Core        41 Jan  7  2015 mylib
-rw-r--r--   1 xzhan9 PCDC_Core        77 Jul  9 12:35 my.module.sh
drwxr-xr-x   5 xzhan9 BICF_Core       156 Jul 18  2018 mysql
lrwxrwxrwx   1 xzhan9 PCDC_Core        28 Dec 10  2018 ncbi -> /home2/xzhan9/proj/data/ncbi
-rw-r--r--   1 xzhan9 PCDC_Core     11117 Jun 12 06:32 ncbi_error_report.xml
-rwx--x--x   1 xzhan9 BICF_Core     11178 Dec  4  2016 nextflow
drwxr-xr-x   4 xzhan9 BICF_Core        51 Apr  4  2017 nltk_data
drwxr-xr-x   4 xzhan9 BICF_Core        65 Jun  5  2017 nvvp_workspace
-rw-r--r--   1 xzhan9 PCDC_Core       100 Apr 25 16:12 pcdc.tf.gpu.sh
drwxr-xr-x   5 xzhan9 PCDC_Core        55 Feb  8  2019 perl5
lrwxrwxrwx   1 xzhan9 BICF_Core        25 Jan 20  2017 PHG_Clinical -> /project/PHG/PHG_Clinical
drwxr-xr-x   2 xzhan9 BICF_Core        10 Sep 28  2015 Pictures
drwxr-xr-x  14 xzhan9 BICF_Core       287 Apr 25 09:59 portal_jobs
-rw-r--r--   1 xzhan9 PCDC_Core       134 Apr  4 22:44 prj_18341.ngc
lrwxrwxrwx   1 xzhan9 BICF_Core         8 Jan 10  2017 proj -> project/
lrwxrwxrwx   1 xzhan9 PCDC_Core        30 Nov 27  2018 project -> /archive/PCDC/PCDC_Core/xzhan9
drwxr-xr-x   2 xzhan9 BICF_Core        10 Sep 28  2015 Public
lrwxrwxrwx   1 xzhan9 BICF_Core        11 Mar 31  2017 pubmed -> work/pubmed
lrwxrwxrwx   1 xzhan9 BICF_Core        28 Aug  6  2018 R -> /home2/xzhan9/project/home/R
-rw-r--r--   1 xzhan9 BICF_Core       186 Sep  9  2015 remoteGUI.txt
lrwxrwxrwx   1 xzhan9 PCDC_Core        56 Apr 24 15:13 RMS-relapsed -> /archive/PCDC/PCDC_Core/jkim23/projects/Lin/RMS-relapsed
-rw-------   1 xzhan9 BICF_Core  20892077 May  6  2016 RMS-RNA-seq.xlsx
lrwxrwxrwx   1 xzhan9 BICF_Core        61 Dec 14  2016 RNA-seq_FASTQ -> /home2/xzhan9/project/software/rnaseq_nextflow/RNA-seq_FASTQ/
drwxr-xr-x   2 xzhan9 BICF_Core        10 Jul 12  2018 rstudio_jobs
drwxr-xr-x  14 xzhan9 BICF_Core      4096 Sep 11 16:08 rvtests
drwxr-xr-x  17 xzhan9 BICF_Core      4096 Sep 10 09:44 rvtests.dev
drwxr-xr-x  16 xzhan9 PCDC_Core      4096 Sep 14 20:26 rvtests.gl
drwxr-xr-x  17 xzhan9 BICF_Core      4096 Sep 11 16:17 rvtests.intel
drwxrwxrwx   2 xzhan9 PCDC_Core        47 Feb 16  2019 share
lrwxrwxrwx   1 xzhan9 BICF_Core        35 Aug  6  2018 software -> /home2/xzhan9/project/home/software
-rwxr--r--   1 xzhan9 PCDC_Core       165 Jun 20 08:22 start.jupyter.sh
lrwxrwxrwx   1 xzhan9 PCDC_Core        22 Feb  7  2019 temp -> /work/PCDC/xzhan9/temp
drwxr-xr-x   3 xzhan9 BICF_Core       150 Feb  7  2019 temp2
drwxr-xr-x   2 xzhan9 BICF_Core        10 Sep 28  2015 Templates
drwxr-xr-x  14 xzhan9 BICF_Core       204 Feb  6  2019 usr
drwxr-xr-x   2 xzhan9 BICF_Core        10 Sep 28  2015 Videos
drwxr-xr-x   2 xzhan9 BICF_Core      4096 Mar 15  2017 wentao
lrwxrwxrwx   1 xzhan9 PCDC_Core        40 Dec  3  2018 work -> /archive/PCDC/PCDC_Core/xzhan9/old_work/



  # NOTICE: user's primary group is PCDC_Core, yet the group permission on his `home2` is `BICF_core` 

```

- **Counter Measures**

  - re-align permissions: `chgrp -R PCDC_Core /home2/xzhan9` 

  - refresh connection to LDAP: 1. log in as administrator (`biohpcadmin` or `zpang1`) --> "LDAP/AD integration" --> "Users" --> "Verify settings and count users" 

    - this can also be done via `occ` command, [link](https://docs.nextcloud.com/server/16/admin_manual/configuration_user/user_auth_ldap_cleanup.html) 

  - check nextcloud log:  look into `/cloud_data` and read `nexcloud*.log` --> grep user's username, e.g. `xzhan9` was searched in the logs, and it was found that sometime before Aug 2019, his LDAP `ou` was `BICF_Core`, which was altered to `PCDC_Core` afterwards, suggesting a group change that perhaps didn't go smoothly  

- **Ongoing Troubleshooting**

  - log entries related to user's home2 map issue:

```
{"reqId":"XYQOJRqSlg97BBN-nBnY0wAAAAo","level":0,"time":"2019-09-19T18:24:22-05:00","remoteAddr":"198.215.54.120","user":"e826ca93-a7b4-4c3b-8fde-4b67f54bbbf5","app":"no app in context","method":"GET","url":"\/index.php\/apps\/files_external\/userglobalstorages\/946?testOnly=true","message":{"Exception":"Icewind\\SMB\\Exception\\ForbiddenException","Message":"Invalid request for \/PHG_Clinical (ForbiddenException)","Code":13,"Trace":[{"file":"\/var\/www\/nextcloud\/apps\/files_external\/3rdparty\/icewind\/smb\/src\/Native\/NativeState.php","line":62,"function":"fromMap","class":"Icewind\\SMB\\Exception\\Exception","type":"::","args":[{"1":"Icewind\\SMB\\Exception\\ForbiddenException","2":"Icewind\\SMB\\Exception\\NotFoundException","13":"Icewind\\SMB\\Exception\\ForbiddenException","16":"Icewind\\SMB\\Exception\\FileInUseException","17":"Icewind\\SMB\\Exception\\AlreadyExistsException","20":"Icewind\\SMB\\Exception\\InvalidTypeException","21":"Icewind\\SMB\\Exception\\InvalidTypeException","22":"Icewind\\SMB\\Exception\\InvalidArgumentException","28":"Icewind\\SMB\\Exception\\OutOfSpaceException","39":"Icewind\\SMB\\Exception\\NotEmptyException","110":"Icewind\\SMB\\Exception\\TimedOutException","111":"Icewind\\SMB\\Exception\\ConnectionRefusedException","112":"Icewind\\SMB\\Exception\\HostDownException","113":"Icewind\\SMB\\Exception\\NoRouteToHostException"},13,"\/PHG_Clinical"]},{"file":"\/var\/www\/nextcloud\/apps\/files_external\/3rdparty\/icewind\/smb\/src\/Native\/NativeState.php","line":74,"function":"handleError","class":"Icewind\\SMB\\Native\\NativeState","type":"->","args":["\/PHG_Clinical"]},{"file":"\/var\/www\/nextcloud\/apps\/files_external\/3rdparty\/icewind\/smb\/src\/Native\/NativeState.php","line":184,"function":"testResult","class":"Icewind\\SMB\\Native\\NativeState","type":"->","args":["*** sensitive parameter replaced ***","smb:\/\/localhost\/homes\/PHG_Clinical"]},{"file":"\/var\/www\/nextcloud\/apps\/files_external\/3rdparty\/icewind\/smb\/src\/Native\/NativeShare.php","line":133,"function":"stat","class":"Icewind\\SMB\\Native\\NativeState","type":"->","args":["smb:\/\/localhost\/homes\/PHG_Clinical"]},{"file":"\/var\/www\/nextcloud\/apps\/files_external\/3rdparty\/icewind\/smb\/src\/Native\/NativeShare.php","line":98,"function":"getStat","class":"Icewind\\SMB\\Native\\NativeShare","type":"->","args":["\/\/PHG_Clinical"]},{"function":"Icewind\\SMB\\Native\\{closure}","class":"Icewind\\SMB\\Native\\NativeShare","type":"->","args":["*** sensitive parameters replaced ***"]},{"file":"\/var\/www\/nextcloud\/apps\/files_external\/3rdparty\/icewind\/smb\/src\/Native\/NativeFileInfo.php","line":82,"function":"call_user_func","args":[{"__class__":"Closure"}]},{"file":"\/var\/www\/nextcloud\/apps\/files_external\/3rdparty\/icewind\/smb\/src\/Native\/NativeFileInfo.php","line":99,"function":"stat","class":"Icewind\\SMB\\Native\\NativeFileInfo","type":"->","args":[]},{"file":"\/var\/www\/nextcloud\/apps\/files_external\/lib\/Lib\/Storage\/SMB.php","line":306,"function":"getMTime","class":"Icewind\\SMB\\Native\\NativeFileInfo","type":"->","args":[]},{"file":"\/var\/www\/nextcloud\/apps\/files_external\/lib\/Lib\/Storage\/SMB.php","line":291,"function":"shareMTime","class":"OCA\\Files_External\\Lib\\Storage\\SMB","type":"->","args":[]},{"file":"\/var\/www\/nextcloud\/lib\/private\/Files\/Storage\/Common.php","line":452,"function":"stat","class":"OCA\\Files_External\\Lib\\Storage\\SMB","type":"->","args":[""]},{"file":"\/var\/www\/nextcloud\/apps\/files_external\/lib\/Lib\/Storage\/SMB.php","line":592,"function":"test","class":"OC\\Files\\Storage\\Common","type":"->","args":[]},{"file":"\/var\/www\/nextcloud\/apps\/files_external\/lib\/config.php","line":269,"function":"test","class":"OCA\\Files_External\\Lib\\Storage\\SMB","type":"->","args":["*** sensitive parameter replaced ***","*** sensitive parameter replaced ***"]},{"file":"\/var\/www\/nextcloud\/apps\/files_external\/lib\/Controller\/StoragesController.php","line":256,"function":"getBackendStatus","class":"OC_Mount_Config","type":"::","args":["*** sensitive parameters replaced ***"]},{"file":"\/var\/www\/nextcloud\/apps\/files_external\/lib\/Controller\/UserGlobalStoragesController.php","line":121,"function":"updateStorageStatus","class":"OCA\\Files_External\\Controller\\StoragesController","type":"->","args":["*** sensitive parameters replaced ***"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/AppFramework\/Http\/Dispatcher.php","line":166,"function":"show","class":"OCA\\Files_External\\Controller\\UserGlobalStoragesController","type":"->","args":[946,"*** sensitive parameter replaced ***"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/AppFramework\/Http\/Dispatcher.php","line":99,"function":"executeController","class":"OC\\AppFramework\\Http\\Dispatcher","type":"->","args":[{"__class__":"OCA\\Files_External\\Controller\\UserGlobalStoragesController"},"show"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/AppFramework\/App.php","line":126,"function":"dispatch","class":"OC\\AppFramework\\Http\\Dispatcher","type":"->","args":[{"__class__":"OCA\\Files_External\\Controller\\UserGlobalStoragesController"},"show"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/AppFramework\/Routing\/RouteActionHandler.php","line":47,"function":"main","class":"OC\\AppFramework\\App","type":"::","args":["OCA\\Files_External\\Controller\\UserGlobalStoragesController","show",{"__class__":"OC\\AppFramework\\DependencyInjection\\DIContainer"},{"id":"946","_route":"files_external.user_global_storages.show"}]},{"function":"__invoke","class":"OC\\AppFramework\\Routing\\RouteActionHandler","type":"->","args":[{"id":"946","_route":"files_external.user_global_storages.show"}]},{"file":"\/var\/www\/nextcloud\/lib\/private\/Route\/Router.php","line":297,"function":"call_user_func","args":[{"__class__":"OC\\AppFramework\\Routing\\RouteActionHandler"},{"id":"946","_route":"files_external.user_global_storages.show"}]},{"file":"\/var\/www\/nextcloud\/lib\/base.php","line":975,"function":"match","class":"OC\\Route\\Router","type":"->","args":["\/apps\/files_external\/userglobalstorages\/946"]},{"file":"\/var\/www\/nextcloud\/index.php","line":42,"function":"handleRequest","class":"OC","type":"::","args":[]}],"File":"\/var\/www\/nextcloud\/apps\/files_external\/3rdparty\/icewind\/smb\/src\/Exception\/Exception.php","Line":30,"CustomMessage":"--"},"userAgent":"Mozilla\/5.0 (X11; Linux x86_64; rv:60.0) Gecko\/20100101 Firefox\/60.0","version":"16.0.1.1"}
{"reqId":"XYQOJhqSlg97BBN-nBnY1AAAAAo","level":1,"time":"2019-09-19T18:24:23-05:00","remoteAddr":"198.215.54.120","user":"e826ca93-a7b4-4c3b-8fde-4b67f54bbbf5","app":"no app in context","method":"GET","url":"\/index.php\/apps\/files_external\/userglobalstorages\/946?testOnly=true","message":"External storage not available: Invalid request for \/PHG_Clinical (ForbiddenException)","userAgent":"Mozilla\/5.0 (X11; Linux x86_64; rv:60.0) Gecko\/20100101 Firefox\/60.0","version":"16.0.1.1"}



{"reqId":"XYQOa-H0dP8s2OiacZWA@gAAAAE","level":0,"time":"2019-09-19T18:25:31-05:00","remoteAddr":"198.215.54.120","user":"e826ca93-a7b4-4c3b-8fde-4b67f54bbbf5","app":"no app in context","method":"PUT","url":"\/index.php\/apps\/files_external\/userstorages\/1096","message":{"Exception":"Icewind\\SMB\\Exception\\AlreadyExistsException","Message":"Invalid request for \/xzhan9 (AlreadyExistsException)","Code":17,"Trace":[{"file":"\/var\/www\/nextcloud\/apps\/files_external\/3rdparty\/icewind\/smb\/src\/Native\/NativeState.php","line":62,"function":"fromMap","class":"Icewind\\SMB\\Exception\\Exception","type":"::","args":[{"1":"Icewind\\SMB\\Exception\\ForbiddenException","2":"Icewind\\SMB\\Exception\\NotFoundException","13":"Icewind\\SMB\\Exception\\ForbiddenException","16":"Icewind\\SMB\\Exception\\FileInUseException","17":"Icewind\\SMB\\Exception\\AlreadyExistsException","20":"Icewind\\SMB\\Exception\\InvalidTypeException","21":"Icewind\\SMB\\Exception\\InvalidTypeException","22":"Icewind\\SMB\\Exception\\InvalidArgumentException","28":"Icewind\\SMB\\Exception\\OutOfSpaceException","39":"Icewind\\SMB\\Exception\\NotEmptyException","110":"Icewind\\SMB\\Exception\\TimedOutException","111":"Icewind\\SMB\\Exception\\ConnectionRefusedException","112":"Icewind\\SMB\\Exception\\HostDownException","113":"Icewind\\SMB\\Exception\\NoRouteToHostException"},17,"\/xzhan9"]},{"file":"\/var\/www\/nextcloud\/apps\/files_external\/3rdparty\/icewind\/smb\/src\/Native\/NativeState.php","line":74,"function":"handleError","class":"Icewind\\SMB\\Native\\NativeState","type":"->","args":["\/xzhan9"]},{"file":"\/var\/www\/nextcloud\/apps\/files_external\/3rdparty\/icewind\/smb\/src\/Native\/NativeState.php","line":184,"function":"testResult","class":"Icewind\\SMB\\Native\\NativeState","type":"->","args":["*** sensitive parameter replaced ***","smb:\/\/localhost\/home2\/xzhan9"]},{"file":"\/var\/www\/nextcloud\/apps\/files_external\/3rdparty\/icewind\/smb\/src\/Native\/NativeShare.php","line":133,"function":"stat","class":"Icewind\\SMB\\Native\\NativeState","type":"->","args":["smb:\/\/localhost\/home2\/xzhan9"]},{"file":"\/var\/www\/nextcloud\/apps\/files_external\/3rdparty\/icewind\/smb\/src\/Native\/NativeShare.php","line":112,"function":"getStat","class":"Icewind\\SMB\\Native\\NativeShare","type":"->","args":["\/xzhan9"]},{"file":"\/var\/www\/nextcloud\/apps\/files_external\/lib\/Lib\/Storage\/SMB.php","line":178,"function":"stat","class":"Icewind\\SMB\\Native\\NativeShare","type":"->","args":["\/xzhan9"]},{"file":"\/var\/www\/nextcloud\/apps\/files_external\/lib\/Lib\/Storage\/SMB.php","line":278,"function":"getFileInfo","class":"OCA\\Files_External\\Lib\\Storage\\SMB","type":"->","args":["\/xzhan9"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/Files\/Storage\/Common.php","line":452,"function":"stat","class":"OCA\\Files_External\\Lib\\Storage\\SMB","type":"->","args":[""]},{"file":"\/var\/www\/nextcloud\/apps\/files_external\/lib\/Lib\/Storage\/SMB.php","line":592,"function":"test","class":"OC\\Files\\Storage\\Common","type":"->","args":[]},{"file":"\/var\/www\/nextcloud\/apps\/files_external\/lib\/config.php","line":269,"function":"test","class":"OCA\\Files_External\\Lib\\Storage\\SMB","type":"->","args":["*** sensitive parameter replaced ***","*** sensitive parameter replaced ***"]},{"file":"\/var\/www\/nextcloud\/apps\/files_external\/lib\/Controller\/StoragesController.php","line":256,"function":"getBackendStatus","class":"OC_Mount_Config","type":"::","args":["*** sensitive parameters replaced ***"]},{"file":"\/var\/www\/nextcloud\/apps\/files_external\/lib\/Controller\/UserStoragesController.php","line":207,"function":"updateStorageStatus","class":"OCA\\Files_External\\Controller\\StoragesController","type":"->","args":["*** sensitive parameters replaced ***"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/AppFramework\/Http\/Dispatcher.php","line":166,"function":"update","class":"OCA\\Files_External\\Controller\\UserStoragesController","type":"->","args":["*** sensitive parameters replaced ***"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/AppFramework\/Http\/Dispatcher.php","line":99,"function":"executeController","class":"OC\\AppFramework\\Http\\Dispatcher","type":"->","args":[{"__class__":"OCA\\Files_External\\Controller\\UserStoragesController"},"update"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/AppFramework\/App.php","line":126,"function":"dispatch","class":"OC\\AppFramework\\Http\\Dispatcher","type":"->","args":[{"__class__":"OCA\\Files_External\\Controller\\UserStoragesController"},"update"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/AppFramework\/Routing\/RouteActionHandler.php","line":47,"function":"main","class":"OC\\AppFramework\\App","type":"::","args":["OCA\\Files_External\\Controller\\UserStoragesController","update",{"__class__":"OC\\AppFramework\\DependencyInjection\\DIContainer"},{"id":"1096","_route":"files_external.user_storages.update"}]},{"function":"__invoke","class":"OC\\AppFramework\\Routing\\RouteActionHandler","type":"->","args":[{"id":"1096","_route":"files_external.user_storages.update"}]},{"file":"\/var\/www\/nextcloud\/lib\/private\/Route\/Router.php","line":297,"function":"call_user_func","args":[{"__class__":"OC\\AppFramework\\Routing\\RouteActionHandler"},{"id":"1096","_route":"files_external.user_storages.update"}]},{"file":"\/var\/www\/nextcloud\/lib\/base.php","line":975,"function":"match","class":"OC\\Route\\Router","type":"->","args":["\/apps\/files_external\/userstorages\/1096"]},{"file":"\/var\/www\/nextcloud\/index.php","line":42,"function":"handleRequest","class":"OC","type":"::","args":[]}],"File":"\/var\/www\/nextcloud\/apps\/files_external\/3rdparty\/icewind\/smb\/src\/Exception\/Exception.php","Line":30,"CustomMessage":"--"},"userAgent":"Mozilla\/5.0 (X11; Linux x86_64; rv:60.0) Gecko\/20100101 Firefox\/60.0","version":"16.0.1.1"}

```

### Ticket #2019091510008083

- inquiry: user's Display Name is incorrect 

- solution:  log in as `biohpcadmin` (old root passwd) 

  - "Settings" --> "LDAP Integration" --> "Advanced" 

  - "User Display Name Field" --> "displayName"  

  - "2nd User Display Name Field" --> "uid" --> Test Configuration 

- useful commands: 

  - NOTE: include `occ` commands absolute directory e.g. `/var/www/nextcloud/occ` or traverse first into that directory

`sudo -u apache php occ user:info [nextcloud userID]` --> see if database userid correlates to the correct credentials 

`sudo -i apache php occ ldap:search "[user real name **lower case**]" 

`sudo -u apache php occ ldap:show-config`

`sudo -u apache php occ ldap:show-remnants`  ##CLOUD has many remnants!

`sudo -u apache php occ user:delete [nextcloud ID]` 


**files:scan** command  **files:cleanup** command  


- useful links: 

 -https://docs.nextcloud.com/server/16/admin_manual/configuration_user/user_auth_ldap.html

 -https://docs.nextcloud.com/server/16/admin_manual/configuration_server/occ_command.html?highlight=ldap#ldap-commands-label

 -https://docs.nextcloud.com/server/16/admin_manual/configuration_user/user_auth_ldap_cleanup.html?highlight=ldap


- Additional approaches: 

 - check web interface's log --> if particular user is getting stuck, delete them via occ

 - check lamella change log on database's info on user mount point for home!



### Ticket#2019091710008203

- Inquiry: user cannot access his cloud data 

- Cause: a "remnant" user is causing hiccups in Nextcloud's database 

 - example of `nextcloud.log` entry

```
# search term: `not a valid user` 

# expected log entry: LEVEL 3, containing messages pertaining both the the USER having issues, as well as the remnant user causing it

-----------------------------------------------------------------------------------

{"reqId":"Aaw1PKzNXLJNkBDJzqgS","level":3,"time":"2019-09-17T21:05:49-05:00","remoteAddr":"98.156.208.162","user":"44e3f56f-1992-4cee-b56d-c6f00c479af3","app":"no app in context","method":"PROPFIND","url":"\/remote.php\/dav\/files\/44e3f56f-1992-4cee-b56d-c6f00c479af3\/","message":{"Exception":"OC\\User\\NoUserException","Message":"c194f437-8bf0-4f70-a525-8d646c42d812 is not a valid user anymore"... 


# notice how the error message mentions BOTH Gaudenz's account and another user?


# another remnant in suspect: 

{"reqId":"tzIp0HnlQ1u8Q3qMR7EC","level":3,"time":"2019-09-17T08:45:35-05:00","remoteAddr":"129.112.203.78","user":"5a492d52-4006-496b-925c-9f6c3977bd14","app":"no app in context","method":"PROPFIND","url":"\/remote.php\/dav\/files\/5a492d52-4006-496b-925c-9f6c3977bd14\/","message":{"Exception":"OC\\User\\NoUserException","Message":"fbc45bd2-74a0-44aa-b9bc-52a9aecea77d is not a valid user anymore" 

-----------------------------------------------------------------------------------
``` 

- Troubleshoot: 

  - see if user is still valid in LDAP

    - `sudo -u apache php occ ldap:check-user c194f437-8bf0-4f70-a525-8d646c42d812"` 

  - see user no longer valid in LDAP server, go on to delete it 

    - `sudo -u apache php occ user:delete c194f437-8bf0-4f70-a525-8d646c42d812`

  - see `nextcloud.log` for this action:

```
{"reqId":"Gd0yWmC1GAhTEWkRle5i","level":1,"time":"2019-09-18T16:29:24-05:00","remoteAddr":"","user":"--","app":"user_ldap","method":"","url":"--","message":"Cleaning up after user c194f437-8bf0-4f70-a525-8d646c42d812","userAgent":"--","version":"15.0.7.0"}

{"reqId":"Gd0yWmC1GAhTEWkRle5i","level":0,"time":"2019-09-18T16:29:27-05:00","remoteAddr":"","user":"--","app":"user_ldap","method":"","url":"--","message":"No DN found for c194f437-8bf0-4f70-a525-8d646c42d812 on 192.168.54.1","userAgent":"--","version":"15.0.7.0"}

```

  - verify user has been deleted
  
    - `sudo -u apache php occ user:info c194f437-8bf0-4f70-a525-8d646c42d812` 



- **EXTRA: export user's mount point config from database** 

  - `sudo -u apache php occ files_external:export <lamella_ID>` 


### Ticket#2019092310008059 

- file share became unavailable for Albert M. 

- Troubleshoot:

  - two existing accounts? one has NEVER been active? 

```
[live][root@lamella01 nextcloud]# sudo -u apache php occ user:info 01734746-ac8b-4694-882a-4c121c8bfc2a
  - user_id: 01734746-ac8b-4694-882a-4c121c8bfc2a
  - display_name: Albert Montillo (albert.montillo@utsouthwestern.edu)
  - email: albert.montillo@utsouthwestern.edu
  - cloud_id: 01734746-ac8b-4694-882a-4c121c8bfc2a@198.215.54.119
  - enabled: true
  - groups:
    - Radiology_2
    - Bioinformatics_2
    - lega_ansir_2
    - BrainParc
    - DLLab
    - nanocourse
    - montillo_trivedi
    - hackathon
    - hackers09
    - Feb2019
  - quota: 100 GB
  - last_seen: 2019-09-23T13:47:25+00:00
  - user_directory: /cloud_data/01734746-ac8b-4694-882a-4c121c8bfc2a
  - backend: LDAP

#2 Account: this one has NEVER been active---deleting:
[live][root@lamella01 nextcloud]# sudo -u apache php occ user:info e9bb8de1-f7e3-4337-aae8-271d53081039
  - user_id: e9bb8de1-f7e3-4337-aae8-271d53081039
  - display_name: Albert Montillo (albert.montillo@UTSouthwestern.edu)
  - email: albert.montillo@UTSouthwestern.edu
  - cloud_id: e9bb8de1-f7e3-4337-aae8-271d53081039@198.215.54.119
  - enabled: true
  - groups:
    - Bioinformatics_2
    - DLLab
  - quota: 100 GB
  - last_seen: 1970-01-01T00:00:00+00:00
  - user_directory: /cloud_data/e9bb8de1-f7e3-4337-aae8-271d53081039
```

  - NOTE: tried to delete extraneous user, but cannot delete...


- Look into Logs:

```
# potential fault: 

{"reqId":"XYjQyHQ530EwYMCYAfDenwAAAAQ","level":3,"time":"2019-09-23T09:03:52-05:00","remoteAddr":"198.215.54.120","user":"01734746-ac8b-4694-882a-4c121c8bfc2a","app":"files","method":"PROPFIND","url":"\/remote.php\/dav\/files\/01734746-ac8b-4694-882a-4c121c8bfc2a\/","message":" Backends provided no user object for 2ec52a24-c4b6-49da-8786-0218354a9e51","userAgent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/64.0.3282.140 Safari\/537.36 Edge\/18.17763","version":"16.0.1.1"}
{"reqId":"XYjQyCXTMVHUfh5KDb4QgAAAAAE","level":3,"time":"2019-09-23T09:03:53-05:00","remoteAddr":"198.215.54.120","user":"01734746-ac8b-4694-882a-4c121c8bfc2a","app":"files","method":"GET","url":"\/index.php\/apps\/recommendations\/api\/recommendations","message":" Backends provided no user object for 2ec52a24-c4b6-49da-8786-0218354a9e51","userAgent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/64.0.3282.140 Safari\/537.36 Edge\/18.17763","version":"16.0.1.1"}

# delete the orhpan user: 

[live][root@lamella01 nextcloud]# sudo -u apache php occ ldap:check-user 2ec52a24-c4b6-49da-8786-0218354a9e51
The user does not exists on LDAP anymore.
Clean up the user's remnants by: ./occ user:delete "2ec52a24-c4b6-49da-8786-0218354a9e51"


[live][root@lamella01 nextcloud]# sudo -u apache php occ user:delete 2ec52a24-c4b6-49da-8786-0218354a9e51
The specified user was deleted

```

- **LDAP User Clean-up NOT Enabled! APCuMemCache Cli NOT ENABLED!!!**

```

# from log:

{"reqId":"s1scMdWmPPO5GQYfJUeF","level":1,"time":"2019-09-23T09:50:09-05:00","remoteAddr":"","user":"--","app":"cli","method":"","url":"--","message":"Memcache \\OC\\Memcache\\APCu not available for local cache","userAgent":"--","version":"16.0.1.1"}
{"reqId":"s1scMdWmPPO5GQYfJUeF","level":1,"time":"2019-09-23T09:50:09-05:00","remoteAddr":"","user":"--","app":"cli","method":"","url":"--","message":"Memcache \\OC\\Memcache\\APCu not available for distributed cache","userAgent":"--","version":"16.0.1.1"}


# from config.php


'ldapUserCleanupInterval' => 0,


# from /etc/php.d/40-apcu.ini

;apc.enable_cli=0

```


- **fixes**

```
# /etc/php.d/40-apcu.ini

apc.enable_cli=1

# /var/www/nextcloud/config/config.php

'ldapUserCleanupInterval' => 9

```


- **ADDITIONAL Orphan Users causing Issues**

  - steps to isolate orphaned user from log file with **+100000** lines: 

```
# pass all instances of remnant user entries to a file:

grep -i 'no user object' /cloud_data/nexcloud1.log | tee orphans0919.txt

# remove repeating entries on remnant users already identied: 

sed -i "/\b\(bf8e9a9a-e476-4770-baa3-7ca8d11e7a52\)\b/d" orphans0919.txt 

sed -i "/\b\(f17a65e8-403a-4d54-b73b-f44055b6ff7d\)\b/d" orphans0919.txt

sed -i "/\b\(bf1c9c70-f7f3-4dfa-ad6f-e0ecf0964c09\)\b/d" orphans0919.txt

sed -i "/\b\(fce80aee-94b6-46bf-9d3e-c1d61d054020\)\b/d" orphans0919.txt

sed -i "/\b\(2ec52a24-c4b6-49da-8786-0218354a9e51\)\b/d" orphans0919.txt 

# after that, only a handful or entries were left for easy-IDing un-discovered remnants


## References:

1. https://askubuntu.com/questions/354993/how-to-remove-lines-from-the-text-file-containing-specific-words-through-termina 

```

- additional remnants & deleting them:


```
{"reqId":"XYjbTnJIUfb62FHn7duhOwAAAAY","level":3,"time":"2019-09-23T09:48:46-05:00","remoteAddr":"198.215.54.120","user":"1bca801f-6bfd-48d1-8364-44e943a0a4b2","app":"files","method":"PROPFIND","url":"\/remote.php\/webdav\/","message":" Backends provided no user object for bf8e9a9a-e476-4770-baa3-7ca8d11e7a52","userAgent":"WebDAVFS\/3.0.0 (03008000) Darwin\/16.7.0 (x86_64)","version":"16.0.1.1"}


{"reqId":"XYjfP4MkFZHzC2mdy9S6gwAAAAw","level":3,"time":"2019-09-23T10:05:35-05:00","remoteAddr":"198.215.54.120","user":"e35d644f-86ed-41b4-89e6-9d586b4f1e3b","app":"files","method":"PROPFIND","url":"\/remote.php\/dav\/files\/e35d644f-86ed-41b4-89e6-9d586b4f1e3b\/","message":" Backends provided no user object for f17a65e8-403a-4d54-b73b-f44055b6ff7d","userAgent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko\/20100101 Firefox\/69.0","version":"16.0.1.1"}

{"reqId":"XS9VFcAq@jteeUn85p-3@gAAABA","level":3,"time":"2019-07-17T12:04:21-05:00","remoteAddr":"198.215.54.120","user":"e35d644f-86ed-41b4-89e6-9d586b4f1e3b","app":"files","method":"PROPFIND","url":"\/remote.php\/dav\/files\/e35d644f-86ed-41b4-89e6-9d586b4f1e3b\/","message":" Backends provided no user object for d6ba3afe-b2e3-4e85-a15b-c4176de254f9","userAgent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/75.0.3770.100 Safari\/537.36","version":"16.0.1.1"}

{"reqId":"XS9VFcAq@jteeUn85p-3@gAAABA","level":3,"time":"2019-07-17T12:04:21-05:00","remoteAddr":"198.215.54.120","user":"e35d644f-86ed-41b4-89e6-9d586b4f1e3b","app":"files","method":"PROPFIND","url":"\/remote.php\/dav\/files\/e35d644f-86ed-41b4-89e6-9d586b4f1e3b\/","message":" Backends provided no user object for bf1c9c70-f7f3-4dfa-ad6f-e0ecf0964c09","userAgent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/75.0.3770.100 Safari\/537.36","version":"16.0.1.1"}

{"reqId":"XS9VOfMN@2S3A4NzHcaOrAAAAAU","level":3,"time":"2019-07-17T12:04:57-05:00","remoteAddr":"198.215.54.120","user":"--","app":"files","method":"PROPFIND","url":"\/public.php\/webdav\/","message":" Backends provided no user object for fce80aee-94b6-46bf-9d3e-c1d61d054020","userAgent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/75.0.3770.142 Safari\/537.36","version":"16.0.1.1"}

{"reqId":"XWR0oiZS3k81Y-en@gIEZgAAAAA","level":3,"time":"2019-08-26T19:09:07-05:00","remoteAddr":"198.215.54.120","user":"f7b89aa7-6719-4ae3-96fb-a623f5743a59","app":"files","method":"PROPFIND","url":"\/remote.php\/dav\/files\/f7b89aa7-6719-4ae3-96fb-a623f5743a59\/","message":" Backends provided no user object for 4d45e94b-5a02-401f-908d-cf394c392978","userAgent":"Mozilla\/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/76.0.3809.100 Safari\/537.36","version":"16.0.1.1"}

{"reqId":"XXkyzYjD9j9Ic70Z1EBI4wAAAAE","level":3,"time":"2019-09-11T12:45:50-05:00","remoteAddr":"198.215.54.120","user":"5637b2d3-f422-4f7a-9e53-6497bf7df7af","app":"files","method":"GET","url":"\/ocs\/v1.php\/apps\/files_sharing\/api\/v1\/shares?format=json&shared_with_me=true&include_tags=true","message":" Backends provided no user object for afaad071-d2d8-4425-93f4-212875840408","userAgent":"Mozilla\/5.0 (X11; Linux x86_64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/77.0.3865.75 Safari\/537.36","version":"16.0.1.1"}



# CONFIRM:

[live][root@lamella01 nextcloud]# sudo -u apache php occ ldap:check-user bf8e9a9a-e476-4770-baa3-7ca8d11e7a52
The user does not exists on LDAP anymore.
Clean up the user's remnants by: ./occ user:delete "bf8e9a9a-e476-4770-baa3-7ca8d11e7a52"


[live][root@lamella01 nextcloud]# sudo -u apache php occ ldap:check-user f17a65e8-403a-4d54-b73b-f44055b6ff7d
The user does not exists on LDAP anymore.
Clean up the user's remnants by: ./occ user:delete "f17a65e8-403a-4d54-b73b-f44055b6ff7d"


# delete: copied Cloud's `orphan_sweep` into nextcloud's directory & ran it to delete orphaned users: 

-------------------------------------------------------------------------------------------------------
[live][root@lamella01 nextcloud]# ./orphan_dump.sh
Deleting Remnant User: Nextcloud
User does not exist
Deleting Remnant User: name
User does not exist
Deleting Remnant User: 4d45e94b-5a02-401f-908d-cf394c392978
The specified user was deleted
Deleting Remnant User: afaad071-d2d8-4425-93f4-212875840408
The specified user was deleted
Deleting Remnant User: bf1c9c70-f7f3-4dfa-ad6f-e0ecf0964c09
The specified user was deleted
Deleting Remnant User: bf8e9a9a-e476-4770-baa3-7ca8d11e7a52
The specified user was deleted
Deleting Remnant User: f17a65e8-403a-4d54-b73b-f44055b6ff7d
The specified user was deleted
Deleting Remnant User: fce80aee-94b6-46bf-9d3e-c1d61d054020
The specified user was deleted
-------------------------------------------------------------------------------------------------------

```

- **UPDATE: Albert's File-mappings still NOT fixed**

```
# log entry in question:


{"reqId":"XYkoP0H5dl2lYqSkvk5TkgAAAAM","level":3,"time":"2019-09-23T15:17:28-05:00","remoteAddr":"198.215.54.120","user":"01734746-ac8b-4694-882a-4c121c8bfc2a","app":"index","method":"GET","url":"\/index.php\/apps\/recommendations\/api\/recommendations","message":{"Exception":"OCP\\Files\\StorageInvalidException","Message":"Sabre\\HTTP\\ClientHttpException: Unauthorized","Code":0,"Trace":[{"file":"\/var\/www\/nextcloud\/lib\/private\/Files\/Storage\/DAV.php","line":276,"function":"convertException","class":"OC\\Files\\Storage\\DAV","type":"->","args":[{"__class__":"Sabre\\HTTP\\ClientHttpException"},"Documents\/Onboarding_Materials\/Python\/Johansson_Scientific_Python\/scientific-python-lectures-master\/images"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/Files\/Storage\/DAV.php","line":705,"function":"propfind","class":"OC\\Files\\Storage\\DAV","type":"->","args":["Documents\/Onboarding_Materials\/Python\/Johansson_Scientific_Python\/scientific-python-lectures-master\/images"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/Files\/Storage\/Wrapper\/Wrapper.php","line":215,"function":"getPermissions","class":"OC\\Files\\Storage\\DAV","type":"->","args":["Documents\/Onboarding_Materials\/Python\/Johansson_Scientific_Python\/scientific-python-lectures-master\/images"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/Files\/Storage\/Wrapper\/Availability.php","line":230,"function":"getPermissions","class":"OC\\Files\\Storage\\Wrapper\\Wrapper","type":"->","args":["\/Documents\/Onboarding_Materials\/Python\/Johansson_Scientific_Python\/scientific-python-lectures-master\/images"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/Files\/Storage\/Wrapper\/Wrapper.php","line":215,"function":"getPermissions","class":"OC\\Files\\Storage\\Wrapper\\Availability","type":"->","args":["\/Documents\/Onboarding_Materials\/Python\/Johansson_Scientific_Python\/scientific-python-lectures-master\/images"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/Files\/Storage\/Wrapper\/PermissionsMask.php","line":79,"function":"getPermissions","class":"OC\\Files\\Storage\\Wrapper\\Wrapper","type":"->","args":["\/Documents\/Onboarding_Materials\/Python\/Johansson_Scientific_Python\/scientific-python-lectures-master\/images"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/Files\/Storage\/Wrapper\/Jail.php","line":220,"function":"getPermissions","class":"OC\\Files\\Storage\\Wrapper\\PermissionsMask","type":"->","args":["\/Documents\/Onboarding_Materials\/Python\/Johansson_Scientific_Python\/scientific-python-lectures-master\/images"]},{"file":"\/var\/www\/nextcloud\/apps\/files_sharing\/lib\/SharedStorage.php","line":199,"function":"getPermissions","class":"OC\\Files\\Storage\\Wrapper\\Jail","type":"->","args":["Python\/Johansson_Scientific_Python\/scientific-python-lectures-master\/images"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/Files\/Storage\/Wrapper\/Wrapper.php","line":215,"function":"getPermissions","class":"OCA\\Files_Sharing\\SharedStorage","type":"->","args":["Python\/Johansson_Scientific_Python\/scientific-python-lectures-master\/images"]},{"file":"\/var\/www\/nextcloud\/apps\/files_sharing\/lib\/Cache.php","line":148,"function":"getPermissions","class":"OC\\Files\\Storage\\Wrapper\\Wrapper","type":"->","args":["Python\/Johansson_Scientific_Python\/scientific-python-lectures-master\/images"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/Files\/Cache\/Wrapper\/CacheWrapper.php","line":71,"function":"formatCacheEntry","class":"OCA\\Files_Sharing\\Cache","type":"->","args":[{"__class__":"OC\\Files\\Cache\\CacheEntry"}]},{"file":"\/var\/www\/nextcloud\/lib\/private\/Files\/Cache\/Wrapper\/CacheJail.php","line":110,"function":"get","class":"OC\\Files\\Cache\\Wrapper\\CacheWrapper","type":"->","args":["Documents\/Onboarding_Materials\/Python\/Johansson_Scientific_Python\/scientific-python-lectures-master\/images"]},{"file":"\/var\/www\/nextcloud\/apps\/files_sharing\/lib\/Cache.php","line":116,"function":"get","class":"OC\\Files\\Cache\\Wrapper\\CacheJail","type":"->","args":["Documents\/Onboarding_Materials\/Python\/Johansson_Scientific_Python\/scientific-python-lectures-master\/images"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/Files\/View.php","line":1321,"function":"get","class":"OCA\\Files_Sharing\\Cache","type":"->","args":["Python\/Johansson_Scientific_Python\/scientific-python-lectures-master\/images"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/Files\/View.php","line":1376,"function":"getCacheEntry","class":"OC\\Files\\View","type":"->","args":[{"cache":null,"scanner":null,"watcher":null,"propagator":null,"updater":null,"__class__":"OCA\\Files_Trashbin\\Storage"},"Python\/Johansson_Scientific_Python\/scientific-python-lectures-master\/images","\/01734746-ac8b-4694-882a-4c121c8bfc2a\/files\/Shared\/Onboarding_Materials\/Python\/Johansson_Scientific_Python\/scientific-python-lectures-master\/images"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/Files\/Node\/Root.php","line":198,"function":"getFileInfo","class":"OC\\Files\\View","type":"->","args":["\/01734746-ac8b-4694-882a-4c121c8bfc2a\/files\/Shared\/Onboarding_Materials\/Python\/Johansson_Scientific_Python\/scientific-python-lectures-master\/images"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/Files\/Node\/Node.php","line":274,"function":"get","class":"OC\\Files\\Node\\Root","type":"->","args":["\/01734746-ac8b-4694-882a-4c121c8bfc2a\/files\/Shared\/Onboarding_Materials\/Python\/Johansson_Scientific_Python\/scientific-python-lectures-master\/images"]},{"file":"\/var\/www\/nextcloud\/apps\/recommendations\/lib\/Service\/RecentlyEditedFilesSource.php","line":55,"function":"getParent","class":"OC\\Files\\Node\\Node","type":"->","args":[]},{"function":"OCA\\Recommendations\\Service\\{closure}","class":"OCA\\Recommendations\\Service\\RecentlyEditedFilesSource","type":"->","args":["*** sensitive parameters replaced ***"]},{"file":"\/var\/www\/nextcloud\/apps\/recommendations\/lib\/Service\/RecentlyEditedFilesSource.php","line":60,"function":"array_map","args":[{"__class__":"Closure"},["*** sensitive parameter replaced ***",{"__class__":"OC\\Files\\Node\\File"},{"__class__":"OC\\Files\\Node\\File"},{"__class__":"OC\\Files\\Node\\File"},{"__class__":"OC\\Files\\Node\\File"},{"__class__":"OC\\Files\\Node\\File"}]]},{"file":"\/var\/www\/nextcloud\/apps\/recommendations\/lib\/Service\/RecommendationService.php","line":91,"function":"getMostRecentRecommendation","class":"OCA\\Recommendations\\Service\\RecentlyEditedFilesSource","type":"->","args":[{"__class__":"OC\\User\\User"},6]},{"function":"OCA\\Recommendations\\Service\\{closure}","class":"OCA\\Recommendations\\Service\\RecommendationService","type":"->","args":["*** sensitive parameters replaced ***"]},{"file":"\/var\/www\/nextcloud\/apps\/recommendations\/lib\/Service\/RecommendationService.php","line":92,"function":"array_reduce","args":[[{"__class__":"OCA\\Recommendations\\Service\\RecentlyCommentedFilesSource"},"*** sensitive parameter replaced ***",{"__class__":"OCA\\Recommendations\\Service\\RecentlySharedFilesSource"}],{"__class__":"Closure"},"*** sensitive parameter replaced ***"]},{"file":"\/var\/www\/nextcloud\/apps\/recommendations\/lib\/Controller\/RecommendationController.php","line":63,"function":"getRecommendations","class":"OCA\\Recommendations\\Service\\RecommendationService","type":"->","args":[{"__class__":"OC\\User\\User"}]},{"file":"\/var\/www\/nextcloud\/lib\/private\/AppFramework\/Http\/Dispatcher.php","line":166,"function":"index","class":"OCA\\Recommendations\\Controller\\RecommendationController","type":"->","args":[]},{"file":"\/var\/www\/nextcloud\/lib\/private\/AppFramework\/Http\/Dispatcher.php","line":99,"function":"executeController","class":"OC\\AppFramework\\Http\\Dispatcher","type":"->","args":[{"__class__":"OCA\\Recommendations\\Controller\\RecommendationController"},"index"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/AppFramework\/App.php","line":126,"function":"dispatch","class":"OC\\AppFramework\\Http\\Dispatcher","type":"->","args":[{"__class__":"OCA\\Recommendations\\Controller\\RecommendationController"},"index"]},{"file":"\/var\/www\/nextcloud\/lib\/private\/AppFramework\/Routing\/RouteActionHandler.php","line":47,"function":"main","class":"OC\\AppFramework\\App","type":"::","args":["OCA\\Recommendations\\Controller\\RecommendationController","index",{"__class__":"OC\\AppFramework\\DependencyInjection\\DIContainer"},{"_route":"recommendations.recommendation.index"}]},{"function":"__invoke","class":"OC\\AppFramework\\Routing\\RouteActionHandler","type":"->","args":[{"_route":"recommendations.recommendation.index"}]},{"file":"\/var\/www\/nextcloud\/lib\/private\/Route\/Router.php","line":297,"function":"call_user_func","args":[{"__class__":"OC\\AppFramework\\Routing\\RouteActionHandler"},{"_route":"recommendations.recommendation.index"}]},{"file":"\/var\/www\/nextcloud\/lib\/base.php","line":975,"function":"match","class":"OC\\Route\\Router","type":"->","args":["\/apps\/recommendations\/api\/recommendations"]},{"file":"\/var\/www\/nextcloud\/index.php","line":42,"function":"handleRequest","class":"OC","type":"::","args":[]}],"File":"\/var\/www\/nextcloud\/lib\/private\/Files\/Storage\/DAV.php","Line":841,"CustomMessage":"--"},"userAgent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/64.0.3282.140 Safari\/537.36 Edge\/18.17763","version":"16.0.1.1"}


```

- DEBUG: for Albert M.'s resolved issue:


```
sudo -u apache php occ files:scan --path 01734746-ac8b-4694-882a-4c121c8bfc2a   #rescans user's file cache (`--path` is to limit to one specific user)

   # don't think this made a difference...

## deactive his extraneous LDAP account (from LDAP server), then delete it

[live][root@lamella01 nextcloud]# sudo -u apache php occ ldap:check-user e9bb8de1-f7e3-4337-aae8-271d53081039
The user does not exists on LDAP anymore.
Clean up the user's remnants by: ./occ user:delete "e9bb8de1-f7e3-4337-aae8-271d53081039"

[live][root@lamella01 nextcloud]# sudo -u apache php occ user:delete e9bb8de1-f7e3-4337-aae8-271d53081039
The specified user was deleted

# user regenerated after re-enabling them on LDAP

[live][root@lamella01 nextcloud]# sudo -u apache php occ ldap:search "albert"
Albert Montillo (albert.montillo@utsouthwestern.edu) (01734746-ac8b-4694-882a-4c121c8bfc2a)
Albert Montillo (albert.montillo@UTSouthwestern.edu) (e9bb8de1-f7e3-4337-aae8-271d53081039)
Albert Lin (albert.lin@utsouthwestern.edu) (19a6c317-ab94-488f-b55a-e11071807bd5)


# ran a file scan on Albert M.'s directory---it took 9 HOURS?!

-------------------------------------------------------------------------------------------------------------------------------------
[live][root@lamella01 nextcloud]# sudo -u apache php occ files:scan --path 01734746-ac8b-4694-882a-4c121c8bfc2a
Starting scan for user 1 out of 1 (01734746-ac8b-4694-882a-4c121c8bfc2a)
lp_load_ex: refreshing parameters
added interface bond0 ip=198.215.54.118 bcast=198.215.54.255 netmask=255.255.255.0
added interface bond0 ip=198.215.54.19 bcast=198.215.54.19 netmask=255.255.255.255
added interface eno4 ip=192.168.54.230 bcast=192.168.54.255 netmask=255.255.255.0
added interface lo ip=127.0.0.1 bcast=127.255.255.255 netmask=255.0.0.0
added interface ib1 ip=10.100.164.34 bcast=10.100.191.255 netmask=255.255.224.0
+---------+-------+--------------+
| Folders | Files | Elapsed time |
+---------+-------+--------------+
| 232     | 4461  | 09:57:57     |    
+---------+-------+--------------+   # we may need to reconfigure the memory settings here
-------------------------------------------------------------------------------------------------------------------------------------

# ran a file scan for `xzhan9` and encountered the following errors

-------------------------------------------------------------------------------------------------------------------------------------

Exception during scan: Unknown error (103) for /PCDC/xzhan9/temp/bioconda-recipes/recipes
#0 /var/www/nextcloud/apps/files_external/3rdparty/icewind/smb/src/Exception/Exception.php(35): Icewind\SMB\Exception\Exception::unknown('/PCDC/xzhan9/te...', 103)
#1 /var/www/nextcloud/apps/files_external/3rdparty/icewind/smb/src/Native/NativeState.php(62): Icewind\SMB\Exception\Exception::fromMap(Array, 103, '/PCDC/xzhan9/te...')
#2 /var/www/nextcloud/apps/files_external/3rdparty/icewind/smb/src/Native/NativeState.php(74): Icewind\SMB\Native\NativeState->handleError('/PCDC/xzhan9/te...')
#3 /var/www/nextcloud/apps/files_external/3rdparty/icewind/smb/src/Native/NativeState.php(105): Icewind\SMB\Native\NativeState->testResult(false, 'smb://localhost...')
#4 /var/www/nextcloud/apps/files_external/3rdparty/icewind/smb/src/Native/NativeShare.php(92): Icewind\SMB\Native\NativeState->opendir('smb://localhost...')
#5 /var/www/nextcloud/apps/files_external/lib/Lib/Storage/SMB.php(195): Icewind\SMB\Native\NativeShare->dir('/PCDC/xzhan9/te...')
#6 /var/www/nextcloud/apps/files_external/lib/Lib/Storage/SMB.php(491): OCA\Files_External\Lib\Storage\SMB->getFolderContents('/PCDC/xzhan9/te...')
#7 /var/www/nextcloud/lib/private/Files/Storage/Wrapper/Wrapper.php(102): OCA\Files_External\Lib\Storage\SMB->opendir('temp/bioconda-r...')
#8 /var/www/nextcloud/lib/private/Files/Storage/Wrapper/Wrapper.php(102): OC\Files\Storage\Wrapper\Wrapper->opendir('temp/bioconda-r...')
#9 /var/www/nextcloud/lib/private/Files/Storage/Wrapper/Availability.php(109): OC\Files\Storage\Wrapper\Wrapper->opendir('temp/bioconda-r...')
#10 /var/www/nextcloud/lib/private/Files/Storage/Wrapper/Wrapper.php(102): OC\Files\Storage\Wrapper\Availability->opendir('temp/bioconda-r...')
#11 /var/www/nextcloud/lib/private/Files/Cache/Scanner.php(375): OC\Files\Storage\Wrapper\Wrapper->opendir('temp/bioconda-r...')
#12 /var/www/nextcloud/lib/private/Files/Cache/Scanner.php(426): OC\Files\Cache\Scanner->getNewChildren('temp/bioconda-r...')
#13 /var/www/nextcloud/lib/private/Files/Cache/Scanner.php(406): OC\Files\Cache\Scanner->handleChildren('temp/bioconda-r...', true, 3, '995559533', true, 0)
#14 /var/www/nextcloud/lib/private/Files/Cache/Scanner.php(409): OC\Files\Cache\Scanner->scanChildren('temp/bioconda-r...', true, 3, '995559533', true)
#15 /var/www/nextcloud/lib/private/Files/Cache/Scanner.php(409): OC\Files\Cache\Scanner->scanChildren('temp/bioconda-r...', true, 3, '995431974', true)
#16 /var/www/nextcloud/lib/private/Files/Cache/Scanner.php(409): OC\Files\Cache\Scanner->scanChildren('temp', true, 3, '995431870', true)
#17 /var/www/nextcloud/lib/private/Files/Cache/Scanner.php(338): OC\Files\Cache\Scanner->scanChildren('', true, 3, '995431869', true)
#18 /var/www/nextcloud/lib/private/Files/Utils/Scanner.php(245): OC\Files\Cache\Scanner->scan('', true, 3)
#19 /var/www/nextcloud/apps/files/lib/Command/Scan.php(145): OC\Files\Utils\Scanner->scan('/e826ca93-a7b4-...', true, NULL)
#20 /var/www/nextcloud/apps/files/lib/Command/Scan.php(201): OCA\Files\Command\Scan->scanFiles('e826ca93-a7b4-4...', '/e826ca93-a7b4-...', Object(Symfony\Component\Console\Output\ConsoleOutput), false, true, false)
#21 /var/www/nextcloud/3rdparty/symfony/console/Command/Command.php(255): OCA\Files\Command\Scan->execute(Object(Symfony\Component\Console\Input\ArgvInput), Object(Symfony\Component\Console\Output\ConsoleOutput))
#22 /var/www/nextcloud/core/Command/Base.php(166): Symfony\Component\Console\Command\Command->run(Object(Symfony\Component\Console\Input\ArgvInput), Object(Symfony\Component\Console\Output\ConsoleOutput))
#23 /var/www/nextcloud/3rdparty/symfony/console/Application.php(901): OC\Core\Command\Base->run(Object(Symfony\Component\Console\Input\ArgvInput), Object(Symfony\Component\Console\Output\ConsoleOutput))
#24 /var/www/nextcloud/3rdparty/symfony/console/Application.php(262): Symfony\Component\Console\Application->doRunCommand(Object(OCA\Files\Command\Scan), Object(Symfony\Component\Console\Input\ArgvInput), Object(Symfony\Component\Console\Output\ConsoleOutput))
#25 /var/www/nextcloud/3rdparty/symfony/console/Application.php(145): Symfony\Component\Console\Application->doRun(Object(Symfony\Component\Console\Input\ArgvInput), Object(Symfony\Component\Console\Output\ConsoleOutput))
#26 /var/www/nextcloud/lib/private/Console/Application.php(213): Symfony\Component\Console\Application->run(Object(Symfony\Component\Console\Input\ArgvInput), Object(Symfony\Component\Console\Output\ConsoleOutput))
#27 /var/www/nextcloud/console.php(97): OC\Console\Application->run()
#28 /var/www/nextcloud/occ(11): require_once('/var/www/nextcl...')
#29 {main}
+---------+-------+--------------+
| Folders | Files | Elapsed time |
+---------+-------+--------------+
| 2329    | 24228 | 00:09:53     |
+---------+-------+--------------+

-------------------------------------------------------------------------------------------------------------------------------------

### DISCOVERY on XZHAN9 --> SYMBOLIC LINK (as seen in error) to /project/PHG/PHG_Clinical 

lrwxrwxrwx   1 xzhan9 PCDC_Core        25 Jan 20  2017 PHG_Clinical -> /project/PHG/PHG_Clinical 


# dis-engage the symbolic link

[root@Nucleus006 xzhan9]# unlink PHG_Clinical

 ## NOTE: didn't fix issue and needs to remove all additional symbolic links


# error logs on .conda:

-------------------------------------------------------------------------------------------------------------------------------------
{"reqId":"XYpePNTk57TBBhb7uyf2MwAAAAE","level":1,"time":"2019-09-24T13:19:40-05:00","remoteAddr":"198.215.54.120","user":"e826ca93-a7b4-4c3b-8fde-4b67f54bbbf5","app":"no app in context","method":"GET","url":"\/index.php\/apps\/files_external\/userglobalstorages\/946?testOnly=false","message":"External storage not available: Invalid request for \/.conda (ForbiddenException)","userAgent":"Mozilla\/5.0 (X11; Linux x86_64; rv:60.0) Gecko\/20100101 Firefox\/60.0","version":"16.0.1.1"}
-------------------------------------------------------------------------------------------------------------------------------------

# [root@Nucleus006 xzhan9]# ls -lah | grep -i conda
drwxr-xr-x 306 xzhan9 PCDC_Core  12K Aug  8 01:21 anaconda2
-rw-r--r--   1 xzhan9 PCDC_Core 477M Jul 25 09:55 Anaconda2-2019.07-Linux-x86_64.sh
lrwxrwxrwx   1 xzhan9 PCDC_Core   24 Feb  6  2019 .conda -> /work/PCDC/xzhan9/.conda
-rw-r--r--   1 xzhan9 PCDC_Core   94 Aug  8 01:26 .condarc

[root@Nucleus006 xzhan9]# mv .condarc .condarc.bak  ### undid this later, as it did not cause the issue!

[root@Nucleus006 xzhan9]# unlink .conda  # LAST Symbolic Link to remove---> after this `home2` became mapped!!! YES!!

```




- **Extra** 

  - look into opcache.ini config 

  - look into 40-apcu.ini
  
  - [optimization link](https://blog.viking-studios.net/en/synology-php-7-cli-without-errors-nextcloud-and-php-7/)

  - [optimiaztion link](https://blog.viking-studios.net/en/your-own-cloud-nextcloud-optimization-on-a-synology-diskstation-and-dsm-6/)



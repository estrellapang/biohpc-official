## Tickets Related to Quota: 

### Ticket#2019072310007858 

- original inquiry

```
gid 	Group_Name 	FILE_SYSTEM 	USED 	SOFT 	HARD(GB) 	TIME
5001 	Morrison_lab 	/archive 	83440 	81920 	92160 	Sun Jul 21 04:12:26

``` 

- troubleshoot for GPFS 


```
------------------------------------------------------------------------------------------------
# Display quota for all GPFS related directories:

/usr/lpp/mmfs/bin/mmlsquota -u <uid> --block-size=auto 

  # replace `-u` with `-g` for group quotas


# Set Quota: Must be ROOT!

[root@ems1 ~]# /usr/lpp/mmfs/bin/mmsetquota data:<fileset> <-u/-g> --block <soft G/T>:<Hard G/T>

  e.g. mmsetquota data:archive -g Bezprozvanny_lab --block 15T:17T
------------------------------------------------------------------------------------------------
```


- troubleshoot for Lustre

```
------------------------------------------------------------------------------------------------
# check quota by GROUP 

**lfs quota -g 5001 -h /project**
Disk quotas for grp 5001 (gid 5001):
     Filesystem    used   quota   limit   grace   files   quota   limit   grace
       /project  36.72T    100T    120T       - 1261717       0       0       -


# Raising quota: 


lfs setquota -g <primary_group> --block-softlimit 5T --block-hardlimit 7T /project
------------------------------------------------------------------------------------------------

# lustre quota deals in integer values!!! Smallest unit of storage is MB, and the conversion is 1:1024! 
 
  # units: "T", "G", "M"

  # NO DECIMALS!

```

- troubleshoot for NFS (/home2) 

```
------------------------------------------------------------------------------------------------
# see quota: log in as user on nucleus006

  quota -s -w -f /home2   


# set quota: log in whichever lysosome server is running properly

  setquota -u -F xfs <uid> <soft quota in MBs> <hard quota in MBs> 0 0 /home2
------------------------------------------------------------------------------------------------
```

- troubleshoot for nfs servers

```
# SEE USER's QUOTA

----------------------
quota -wps -u <uid>

  - w = "no wrap" 

  - s = "human readable"

  - p = "show grace period?"
----------------------

------------------------------------------------------------------------------------------------
# grab all users and get the # of files from their directories via `quota` command

cd /home2; for user in *; do echo $user; done | head 

cd /home2; for user in *; do quota 2>&1 -wp -s -u $user | awk -v user=$user '{if(NR == 3) {print $6,user}}' ; done | head

------------------------------------------------------------------------------------------------

```

\
\
\

### Quota Breakdown by Dept and by PIs

#### radiology

- put all labs in an array

```
# createa array variable
radPrjctLabs=(Agarwal_lab ANSIR_lab ANSIR_Childrens ANSIR_Collaborator ANSIR_dhms ANSIR_Parkland ANSIR_UTSW Bowen_lab Chhabra_lab Chopra_lab Dogan_lab Duan_lab Guo_lab ImageScience_lab JWang_lab Kay_lab Lenkinski_lab Madhuranthakam_lab Mason_lab Mattrey_lab Ng_lab Pedrosa_lab Shah_lab Sun_lab Vinogradov_lab)


# check array elements
for i in ${radPrjctLabs[@]}; do echo $i; done


# Project Quota by Lab
for i in ${radPrjctLabs[@]}; do echo "$i usage:"; sudo lfs quota -g $i -h /project | grep T | awk '{print $2}'; done


# Archive Quota by Lab

   # NOTE: needs revision---need to grab both Gigabytes and Terabytes

for i in ${radPrjctLabs[@]}; do echo "$i usage"; mmlsquota -g $i --block-size auto bassdata:archive | grep T | awk '{print $4}';done
```

\
\
\

### REVISED: Quota Breakdown by Lab and by Users

#### radiology

- **/project**

```
# parse lab members into a space delimited/separated array

ansirList=( $(getent group ANSIR_lab | cut -f 1,2,3 -d ':' --complement | sed 's/,/ /g') )

  ## check array elements

for i in ${ansirList[@]}; do echo $i; done

# confirm lab member array

for i in ${ansirList[@]}; do echo "$i corresponds to $(getent passwd $i | cut -f 5 -d ':' | awk -F, '{print $1}')"; done

# further trim lab member array down to ACTIVE MEMBERS ONLY

ansirListFined=( $(for i in ${ansirList[@]}; do echo "$i corresponds to $(getent passwd $i | cut -f 5 -d ':' | awk -F, '{print $1}')"; done | grep -wv 'corresponds to ' | awk '{print $1}') )

# Individual QUOTA: loop througha all members of lab; prints user's real name

for i in ${ansirListFined[@]}; do echo "usage for $(getent passwd $i | cut -f 5 -d ':' | awk -F, '{print $1}')"; sudo lfs quota -u $i -h /project | sed -n 3p | awk '{print $2}'; done > ansirLab_project_use_10092020.txt

# filter out users who are no longer active

cat ansirLab_project_use_10092020.txt | grep -wv 'usage for '

```

\
\

- **/work**

```
# fetch for each user

for i in ${ansirList[@]}; do echo "usage for $(getent passwd $i | cut -f 5 -d ':' | awk -F, '{print $1}')"; sudo /usr/lpp/mmfs/bin/mmlsquota -u $i --block-size auto bassclient:work | sed -n 3p | awk '{print $4}'; done > ansirLab_work_use_110_10092020.txt

```


\
\

#### Scan via GPFS Policy Engine

-**References**

  - [GPFS filesystem SQL parameters](https://www.ibm.com/support/knowledgecenter/STXKQY_5.0.4/com.ibm.spectrum.scale.v5r04.doc/bl1adv_usngfileattrbts.htm)

  - [general SQL syntax](https://www.w3schools.com/sql/sql_and_or.asp)

\
\

- **USE CASE**: /archive user-based usage report requires using the GPFS-based policy engine to submit SQL queries in order to determine per-user's total usage on the fileset

  - this is due to /archive having GROUP(gid)-exclusive quota reporting

- e.g. for ANSIR lab

```
## insert all ACTIVE lab members into space-separated array

ansirList=( $(getent group ANSIR_lab | cut -f 1,2,3 -d ':' --complement | sed 's/,/ /g') )

  # here the array is trimmed down to active only members

ansirListFined=( $(for i in ${ansirList[@]}; do echo "$i corresponds to $(getent passwd $i | cut -f 5 -d ':' | awk -F, '{print $1}')"; done | grep -wv 'corresponds to ' | awk '{print $1}') )


  # one can do a before and after comparison for 2 arrays generated
-------------------------------------------------------------------
for i in ${ansirListFined[@]}; do echo $i; done | wc -l
33

for i in ${ansirList[@]}; do echo $i; done | wc -l
41
-------------------------------------------------------------------

## populate an array with integer elements that match the total # of elements in the uid array, e.g. 1 - 33 in this case

ansirIndex=( $(for i in $(seq 1 33); do echo $i; done) )

  # confirm

for i in ${ansirListIndex[@]}; do echo $i; done

# simple policy file
-------------------------------------------------------------------
RULE 'filesRule' LIST 'files'

SHOW(varchar(file_size))
  FOR FILESET('archive')

WHERE USER_NAME=='<uid>'

  # for gids, use the following line `WHERE GROUP_ID=<integer>`  

  # NOTE: do not use single quotes, as it's registered as a character string and not an integer value

  ### MATCH MULTIPLE PARAMETERS ### --- use SQL's `AND` and `OR` operators

e.g. 

`WHERE GROUP_ID==9001 OR GROUP_ID==9002`
-------------------------------------------------------------------


\

# sample policy file -- scans for file sizes of both GID=9001 and 9002
-------------------------------------------------------------------
RULE 'filesRule' LIST 'files'

SHOW(varchar(file_size))
  FOR FILESET('backup')

WHERE GROUP_ID==9002 OR GROUP_ID=9001
-------------------------------------------------------------------

\

## loop through both arrays and replace the UID in the policy file

  # NOTICE: use "" when calling the variable elements

for ((i=0; i<${#ansirList[@]}; ++i)); do cat list_user_file_size.policy | sed "s/s433301/${ansirListFined[i]}/g" > archive_ansir_user_${ansirListIndex[i]}; done


  # Verify the contents of policy files against the original array

for i in ${ansirListIndex[@]}; do cat archive_ansir_user_$i | grep USER_NAME; done

for i in ${ansirListFined[@]}; do echo $i; done

## submit the SQL query on EMS node

  # template: mmapplypolicy <specific dir to look under> -P <policy file to be parsed> -I defer -f <output file>

    # NOTE: do not specify extensions to the output file names

  # combine with nohup

  # if there are no files, gpfs will not generate a .out file

mmapplypolicy /endosome/archive/radiology/ -P /tmp/archive_ansir_user_5 -I defer -f /tmp/s159817_archive_policy

## isolate the 4th column/field using awk and save result as .txt file

cat /tmp/s159817_archive_policy.out | awk '{print $4}' > /tmp/s159817_archive_policy.txt

  # mmapplypolicy does not insert space characters between the lines; trimmed output column can DIRECTLY BE IMPORTED into Excel as a data set.

  # btw: to add "," as delimiter to replace space characters for Excel importing, pipe into `tr -s '[:blank:]'`

```

- **IDEA**: use head and tail to listout LARGEST and SMALLEST files

\
\
\

> cont'd off of the steps before

### Using Python for Processing Arrays/Tuples with Large #s of Elements

####



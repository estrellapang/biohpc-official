## Debug On Lustre Clients: Fall 2019 

### Ticket#2019100910008074 

```
[root@biohpcwsc030 biohpcadmin]# modprobe -v lustre
insmod /lib/modules/3.10.0-693.el7.x86_64/extra/kernel/fs/lustre/ptlrpc.ko 
modprobe: ERROR: could not insert 'lustre': Network is down

# lnet can't connect via port 988
-----------------------------------------------------------------------------------------------------------------------------------------------------
[ 5671.672131] LNetError: 10681:0:(linux-tcpip.c:570:libcfs_sock_listen()) Can't create socket: port 988 already in use
[ 5671.672135] LNetError: 122-1: Can't start acceptor on port 988: port already in use
-----------------------------------------------------------------------------------------------------------------------------------------------------

# used by nfs? guat?
-----------------------------------------------------------------------------------------------------------------------------------------------------
tcp        0      0 biohpcwsc030:988        lysosome:nfs            ESTABLISHED
-----------------------------------------------------------------------------------------------------------------------------------------------------

# temp fix (tried and true), move lustre mount ahead of nfs mounts in `/etc/fstab`, reboot WS
-----------------------------------------------------------------------------------------------------------------------------------------------------
198.215.54.49@tcp0:198.215.54.50@tcp0:/project  /project        lustre  defaults,nosuid,flock,_netdev   0 0
lysosome:/home2              /home2               nfs          nolock,rsize=32768,wsize=32768,hard,intr,async 0 0
lysosome:/home1              /home1               nfs          nolock,rsize=32768,wsize=32768,hard,intr,async 0 0
-----------------------------------------------------------------------------------------------------------------------------------------------------

```

- **resources** 

  - [forum 1](https://lustre-discuss.clusterfs.narkive.com/QEVElss9/why-lctl-list-nids-ioc-libcfs-get-ni-error-100-network-is-down) 


## VM Related Tickets: Starting Oct. 2019

### Ticket#2019101810008021 

- Inquiry:

```

The device helper structure version has changed.

If you have upgraded VirtualBox recently, please make sure you have terminated all VMs and upgraded any extension packs. If this error persists, try re-installing VirtualBox. (VERR_PDM_DEVHLPR3_VERSION_MISMATCH).

Result Code: 
	

NS_ERROR_FAILURE (0x80004005)

Component: 
	

ConsoleWrap

Interface: 
	

IConsole {872da645-4a9b-1727-bee2-5585105b9eed}​

```

- Diagnosis: VirtualBox Version Mismatches Extension Pack Version


```
yum list installed | grep -i virtualbox
VirtualBox-5.1.x86_64              5.1.38_122592_el7-1     @virtualbox 

## upon entering launching VirtualBox --> his extension pack came from a previous minor version

```

- Solution:


```
## download the extension pack that matches his Vbox installation 

wget http://download.virtualbox.org/virtualbox/5.1.38/Oracle_VM_VirtualBox_Extension_Pack-5.1.38-122592.vbox-extpack

## add new extension pack inside virtualbox GUI

```

- Resources: 

  - [answer link](https://dwaves.org/2017/06/10/virtualbox-verr_pdm_devhlpr3_version_mismatch-0x80004005/)

## July 2019: Tickets Related to VMs

### Ticket#2019071510008005

- Original Request:

```
Hi,
 
I’m close to running out of space on nuclia-test:
[nuclia_admin@nuclia-test ~]$ df -h
Filesystem                                 Size  Used Avail Use% Mounted on
/dev/mapper/rhel-root                       44G   39G  5.2G  89% /
devtmpfs                                   3.9G     0  3.9G   0% /dev
tmpfs                                      3.9G     0  3.9G   0% /dev/shm
tmpfs                                      3.9G  169M  3.7G   5% /run
tmpfs                                      3.9G     0  3.9G   0% /sys/fs/cgroup
/dev/sda1                                 1014M  234M  781M  23% /boot
//198.215.54.13/project/PHG/PHG_BarTender  3.5P  2.6P  922T  74% /PHG_BarTender
//198.215.54.13/project/PHG/PHG_Clinical   3.5P  2.6P  922T  74% /PHG_Clinical
tmpfs                                      783M     0  783M   0% /run/user/0
//198.215.54.13/project/PHG/PHG_BarTender  3.5P  2.6P  922T  74% /PHG_BarTender
//198.215.54.13/project/PHG/PHG_Clinical   3.5P  2.6P  922T  74% /PHG_Clinical
tmpfs                                      783M     0  783M   0% /run/user/58644
 
Can someone increase the size as soon as possible? I need to dump a large file (7GB+) on the system and I currently can’t.

``` 


- Relevant Info: 

  - machine type: VM on ESXI hosts

  - VM Manager: VMware 

  - VM IP: 198.215.54.104 

    - Hostname: `nuclia-test`

- Diagnosis: the root LV is running low on storage space 

- Fix: back up VM (maybe snap shot), shut down, & then add virtual disk


- Machine Info: 

-------------------------------------------------------------------------------

[root@nuclia-test ~]# vgdisplay
  --- Volume group ---
  VG Name               rhel
  System ID             
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  3
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                2
  Open LV               2
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <49.00 GiB
  PE Size               4.00 MiB
  Total PE              12543
  Alloc PE / Size       12543 / <49.00 GiB
  Free  PE / Size       0 / 0   
  VG UUID               DlpQyl-QHOR-ECir-K3Xh-4fOA-REF3-jYMrtV
   
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# lvdisplay
  --- Logical volume ---
  LV Path                /dev/rhel/root
  LV Name                root
  VG Name                rhel
  LV UUID                6zd4mK-2u0T-4s6I-zfn9-DwoX-CcEl-HflmTM
  LV Write Access        read/write
  LV Creation host, time localhost.localdomain, 2018-01-12 11:50:01 -0500
  LV Status              available
  # open                 1
  LV Size                <44.00 GiB
  Current LE             11263
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:0

[biohpcadmin@nuclia-test ~]$ lsblk
NAME          MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
fd0             2:0    1    4K  0 disk 
sda             8:0    0   50G  0 disk 
├─sda1          8:1    0    1G  0 part /boot
└─sda2          8:2    0   49G  0 part 
  ├─rhel-root 253:0    0   44G  0 lvm  /
  └─rhel-swap 253:1    0    5G  0 lvm  [SWAP]
sr0            11:0    1  3.5G  0 rom 

-------------------------------------------------------------------------------




- Operations to expand LV inside the machine: 


```
# add partition table & partition drive
-------------------------------------------------------------------------------

lsblk

sudo parted /dev/sdb mklabel gpt   # specify GPT partitioning standard 

sudo parted -a opt /dev/sdb mkpart primary xfs 0% 100%   # whole-disk partition

lsblk	# confirm

# /dev/sdb1
-------------------------------------------------------------------------------


# add partition as a PV

pvs 

pvcreate /dev/sdb1

pvs   # we should see free space


# extend VG then LV 

vgs 

vgextend rhel /dev/sdb1

vgs / pvscan   # the latter shows which PV creates what VG

vgdisplay   # see free size available 


lvdisplay  # fetch directory of LV

lvextend -l + <physical extends> /dev/rhel/root

lvs # check updated space 

```

- **Live Operation History** 

```
[root@Nucleus006 ~]# ssh biohpcadmin@198.215.54.104
biohpcadmin@198.215.54.104's password: 
Last login: Tue Jul 16 14:18:40 2019 from 198.215.54.58
[biohpcadmin@nuclia-test ~]$ 
[biohpcadmin@nuclia-test ~]$ 
[biohpcadmin@nuclia-test ~]$ 
[biohpcadmin@nuclia-test ~]$ 
[biohpcadmin@nuclia-test ~]$ 
[biohpcadmin@nuclia-test ~]$ lsblk
NAME          MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
fd0             2:0    1    4K  0 disk 
sda             8:0    0   50G  0 disk 
├─sda1          8:1    0    1G  0 part /boot
└─sda2          8:2    0   49G  0 part 
  ├─rhel-root 253:0    0   44G  0 lvm  /
  └─rhel-swap 253:1    0    5G  0 lvm  [SWAP]
sdb             8:16   0  250G  0 disk 
sr0            11:0    1  3.5G  0 rom  
[biohpcadmin@nuclia-test ~]$ 
[biohpcadmin@nuclia-test ~]$ 
[biohpcadmin@nuclia-test ~]$ ls /dev | grep sdb
sdb
[biohpcadmin@nuclia-test ~]$ 
[biohpcadmin@nuclia-test ~]$ 
[biohpcadmin@nuclia-test ~]$ 
[biohpcadmin@nuclia-test ~]$ su -
Password: 
Last login: Tue Jul 16 14:23:46 EDT 2019 on pts/1
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# lsblk
NAME          MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
fd0             2:0    1    4K  0 disk 
sda             8:0    0   50G  0 disk 
├─sda1          8:1    0    1G  0 part /boot
└─sda2          8:2    0   49G  0 part 
  ├─rhel-root 253:0    0   44G  0 lvm  /
  └─rhel-swap 253:1    0    5G  0 lvm  [SWAP]
sdb             8:16   0  250G  0 disk 
sr0            11:0    1  3.5G  0 rom  
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# parted /dev/sdb mklabel gpt
Information: You may need to update /etc/fstab.

[root@nuclia-test ~]#                                                     
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# df -Th
Filesystem                                Type      Size  Used Avail Use% Mounted on
/dev/mapper/rhel-root                     xfs        44G   39G  5.1G  89% /
devtmpfs                                  devtmpfs  3.9G     0  3.9G   0% /dev
tmpfs                                     tmpfs     3.9G     0  3.9G   0% /dev/shm
tmpfs                                     tmpfs     3.9G  8.6M  3.9G   1% /run
tmpfs                                     tmpfs     3.9G     0  3.9G   0% /sys/fs/cgroup
/dev/sda1                                 xfs      1014M  234M  781M  23% /boot
//198.215.54.13/project/PHG/PHG_Clinical  cifs      3.5P  2.6P  927T  74% /PHG_Clinical
//198.215.54.13/project/PHG/PHG_BarTender cifs      3.5P  2.6P  927T  74% /PHG_BarTender
tmpfs                                     tmpfs     783M     0  783M   0% /run/user/58644
tmpfs                                     tmpfs     783M     0  783M   0% /run/user/58643
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# parted -a opt /dev/sdb mkpart primary xfs 0% 100%
Information: You may need to update /etc/fstab.

[root@nuclia-test ~]#                                                     
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# lsblk
NAME          MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
fd0             2:0    1    4K  0 disk 
sda             8:0    0   50G  0 disk 
├─sda1          8:1    0    1G  0 part /boot
└─sda2          8:2    0   49G  0 part 
  ├─rhel-root 253:0    0   44G  0 lvm  /
  └─rhel-swap 253:1    0    5G  0 lvm  [SWAP]
sdb             8:16   0  250G  0 disk 
└─sdb1          8:17   0  250G  0 part 
sr0            11:0    1  3.5G  0 rom  
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# pvs
  PV         VG   Fmt  Attr PSize   PFree
  /dev/sda2  rhel lvm2 a--  <49.00g    0 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# pvcreate /dev/sdb1
  Physical volume "/dev/sdb1" successfully created.
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# pvs
  PV         VG   Fmt  Attr PSize    PFree   
  /dev/sda2  rhel lvm2 a--   <49.00g       0 
  /dev/sdb1       lvm2 ---  <250.00g <250.00g
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# vgs
  VG   #PV #LV #SN Attr   VSize   VFree
  rhel   1   2   0 wz--n- <49.00g    0 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# vgextend rhel /dev/sdb1
  Volume group "rhel" successfully extended
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# vgs
  VG   #PV #LV #SN Attr   VSize   VFree   
  rhel   2   2   0 wz--n- 298.99g <250.00g
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# lvs
  LV   VG   Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  root rhel -wi-ao---- <44.00g                                                    
  swap rhel -wi-ao----   5.00g                                                    
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# lvdisplay
  --- Logical volume ---
  LV Path                /dev/rhel/root
  LV Name                root
  VG Name                rhel
  LV UUID                6zd4mK-2u0T-4s6I-zfn9-DwoX-CcEl-HflmTM
  LV Write Access        read/write
  LV Creation host, time localhost.localdomain, 2018-01-12 11:50:01 -0500
  LV Status              available
  # open                 1
  LV Size                <44.00 GiB
  Current LE             11263
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:0
   
  --- Logical volume ---
  LV Path                /dev/rhel/swap
  LV Name                swap
  VG Name                rhel
  LV UUID                DpDLKi-MSJe-P576-Y7Pk-oCIX-BTcJ-G71S6w
  LV Write Access        read/write
  LV Creation host, time localhost.localdomain, 2018-01-12 11:50:01 -0500
  LV Status              available
  # open                 2
  LV Size                5.00 GiB
  Current LE             1280
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:1
   
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# vgdisplay
  --- Volume group ---
  VG Name               rhel
  System ID             
  Format                lvm2
  Metadata Areas        2
  Metadata Sequence No  4
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                2
  Open LV               2
  Max PV                0
  Cur PV                2
  Act PV                2
  VG Size               298.99 GiB
  PE Size               4.00 MiB
  Total PE              76542
  Alloc PE / Size       12543 / <49.00 GiB
  Free  PE / Size       63999 / <250.00 GiB
  VG UUID               DlpQyl-QHOR-ECir-K3Xh-4fOA-REF3-jYMrtV
   
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# lvextend -l +63999 /dev/rhel/root
  Size of logical volume rhel/root changed from <44.00 GiB (11263 extents) to 293.99 GiB (75262 extents).
  Logical volume rhel/root successfully resized.
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# lvs
  LV   VG   Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  root rhel -wi-ao---- 293.99g                                                    
  swap rhel -wi-ao----   5.00g                                                    
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# lvdisplay
  --- Logical volume ---
  LV Path                /dev/rhel/root
  LV Name                root
  VG Name                rhel
  LV UUID                6zd4mK-2u0T-4s6I-zfn9-DwoX-CcEl-HflmTM
  LV Write Access        read/write
  LV Creation host, time localhost.localdomain, 2018-01-12 11:50:01 -0500
  LV Status              available
  # open                 1
  LV Size                293.99 GiB
  Current LE             75262
  Segments               2
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:0
   
  --- Logical volume ---
  LV Path                /dev/rhel/swap
  LV Name                swap
  VG Name                rhel
  LV UUID                DpDLKi-MSJe-P576-Y7Pk-oCIX-BTcJ-G71S6w
  LV Write Access        read/write
  LV Creation host, time localhost.localdomain, 2018-01-12 11:50:01 -0500
  LV Status              available
  # open                 2
  LV Size                5.00 GiB
  Current LE             1280
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:1
   
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# df -Th
Filesystem                                Type      Size  Used Avail Use% Mounted on
/dev/mapper/rhel-root                     xfs        44G   39G  5.1G  89% /
devtmpfs                                  devtmpfs  3.9G     0  3.9G   0% /dev
tmpfs                                     tmpfs     3.9G     0  3.9G   0% /dev/shm
tmpfs                                     tmpfs     3.9G  8.6M  3.9G   1% /run
tmpfs                                     tmpfs     3.9G     0  3.9G   0% /sys/fs/cgroup
/dev/sda1                                 xfs      1014M  234M  781M  23% /boot
//198.215.54.13/project/PHG/PHG_Clinical  cifs      3.5P  2.6P  926T  74% /PHG_Clinical
//198.215.54.13/project/PHG/PHG_BarTender cifs      3.5P  2.6P  926T  74% /PHG_BarTender
tmpfs                                     tmpfs     783M     0  783M   0% /run/user/58644
tmpfs                                     tmpfs     783M     0  783M   0% /run/user/58643
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# resize2fs /dev/rhel/root
resize2fs 1.42.9 (28-Dec-2013)
resize2fs: Bad magic number in super-block while trying to open /dev/rhel/root
Couldn't find valid filesystem superblock.
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# df -Th
Filesystem                                Type      Size  Used Avail Use% Mounted on
/dev/mapper/rhel-root                     xfs        44G   39G  5.1G  89% /
devtmpfs                                  devtmpfs  3.9G     0  3.9G   0% /dev
tmpfs                                     tmpfs     3.9G     0  3.9G   0% /dev/shm
tmpfs                                     tmpfs     3.9G  8.6M  3.9G   1% /run
tmpfs                                     tmpfs     3.9G     0  3.9G   0% /sys/fs/cgroup
/dev/sda1                                 xfs      1014M  234M  781M  23% /boot
//198.215.54.13/project/PHG/PHG_Clinical  cifs      3.5P  2.6P  926T  74% /PHG_Clinical
//198.215.54.13/project/PHG/PHG_BarTender cifs      3.5P  2.6P  926T  74% /PHG_BarTender
tmpfs                                     tmpfs     783M     0  783M   0% /run/user/58644
tmpfs                                     tmpfs     783M     0  783M   0% /run/user/58643
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# xfs_growfs /dev/rhel/root
meta-data=/dev/mapper/rhel-root  isize=512    agcount=4, agsize=2883328 blks
         =                       sectsz=512   attr=2, projid32bit=1
         =                       crc=1        finobt=0 spinodes=0
data     =                       bsize=4096   blocks=11533312, imaxpct=25
         =                       sunit=0      swidth=0 blks
naming   =version 2              bsize=4096   ascii-ci=0 ftype=1
log      =internal               bsize=4096   blocks=5631, version=2
         =                       sectsz=512   sunit=0 blks, lazy-count=1
realtime =none                   extsz=4096   blocks=0, rtextents=0
data blocks changed from 11533312 to 77068288
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# 
[root@nuclia-test ~]# df -Th
Filesystem                                Type      Size  Used Avail Use% Mounted on
/dev/mapper/rhel-root                     xfs       294G   39G  256G  14% /
devtmpfs                                  devtmpfs  3.9G     0  3.9G   0% /dev
tmpfs                                     tmpfs     3.9G     0  3.9G   0% /dev/shm
tmpfs                                     tmpfs     3.9G  8.6M  3.9G   1% /run
tmpfs                                     tmpfs     3.9G     0  3.9G   0% /sys/fs/cgroup
/dev/sda1                                 xfs      1014M  234M  781M  23% /boot
//198.215.54.13/project/PHG/PHG_Clinical  cifs      3.5P  2.6P  926T  74% /PHG_Clinical
//198.215.54.13/project/PHG/PHG_BarTender cifs      3.5P  2.6P  926T  74% /PHG_BarTender
tmpfs                                     tmpfs     783M     0  783M   0% /run/user/58644

```

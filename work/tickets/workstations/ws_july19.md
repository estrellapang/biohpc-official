## Workstation Related Tickets: July 2019 



### Ticket#2019071710007941 


- Inquiry

>Hi:
>I am not able to listen to training videos on my workstation.  
>
>For example I am trying to play training material on QUPath.  The video plays in the
>browser but no sound can be heard.
>
>Can you help make the sound system active?
>
>work station details:  my id: s186208.  Workstation:  biohpcwsc038.  


- Relevant Info: 

  - Lenovo Workstation: new shipment 

    - MODEL: p520c 

- Diagnosis: 

```
### on GUI: no sound devices found

### Troubleshooting via CLI

# physical sound cards detected 

$ lspci | grep Audio
00:1f.3 Audio device: Intel Corporation 200 Series PCH HD Audio
65:00.1 Audio device: NVIDIA Corporation GP106 High Definition Audio Controller (rev a1)

# get more info on physical sound cards 

[biohpcadmin@biohpcwsc038 ~]$ lspci -v | grep -A7 -i "audio"
00:1f.3 Audio device: Intel Corporation 200 Series PCH HD Audio
	Subsystem: Lenovo Device 103c
	Flags: bus master, fast devsel, latency 32, IRQ 60, NUMA node 0
	Memory at 43ffff10000 (64-bit, non-prefetchable) [size=16K]
	Memory at 43ffff00000 (64-bit, non-prefetchable) [size=64K]
	Capabilities: <access denied>
	Kernel driver in use: snd_hda_intel
	Kernel modules: snd_hda_intel
--
65:00.1 Audio device: NVIDIA Corporation GP106 High Definition Audio Controller (rev a1)
	Subsystem: NVIDIA Corporation Device 11b3
	Flags: bus master, fast devsel, latency 0, IRQ 61, NUMA node 0
	Memory at e0080000 (32-bit, non-prefetchable) [size=16K]
	Capabilities: <access denied>
	Kernel driver in use: snd_hda_intel
	Kernel modules: snd_hda_intel

# get devices attached to sound card  

 cat /proc/asound/devices
  1:        : sequencer
  2: [ 0]   : control
  3: [ 0- 0]: digital audio playback
  4: [ 0- 0]: digital audio capture
  5: [ 0- 2]: digital audio capture
  6: [ 0- 0]: hardware dependent
  7: [ 1]   : control
  8: [ 1- 3]: digital audio playback
  9: [ 1- 7]: digital audio playback
 10: [ 1- 8]: digital audio playback
 11: [ 1- 9]: digital audio playback
 12: [ 1- 0]: hardware dependent
 33:        : timer			# looks normal 

# get aforementioned sound device info 

[biohpcadmin@biohpcws#### ~]$ aplay --list-devices
aplay: device_list:270: no soundcards found...

  # COMMENT: as root, the commands return information needed: PERMISSION ISSUES?!!

sudo aplay --list-devices

**** List of PLAYBACK Hardware Devices ****
card 0: PCH [HDA Intel PCH], device 0: ALC662 rev3 Analog [ALC662 rev3 Analog]
  Subdevices: 1/1
  Subdevice #0: subdevice #0
card 1: NVidia [HDA NVidia], device 3: HDMI 0 [HDMI 0]
  Subdevices: 1/1
  Subdevice #0: subdevice #0
card 1: NVidia [HDA NVidia], device 7: HDMI 1 [HDMI 1]
  Subdevices: 1/1
  Subdevice #0: subdevice #0
card 1: NVidia [HDA NVidia], device 8: HDMI 2 [HDMI 2]
  Subdevices: 1/1
  Subdevice #0: subdevice #0
card 1: NVidia [HDA NVidia], device 9: HDMI 3 [HDMI 3]
  Subdevices: 1/1
  Subdevice #0: subdevice #0

# check kernel Module related to Sound Card 

[biohpcadmin@biohpcwsc038 ~]$ cat /proc/asound/modules| grep snd
 0 snd_hda_intel
 1 snd_hda_intel

# sound modules installed? 

-------------------------------------------------------------------------------

[biohpcadmin@biohpcwsc038 ~]$ find /lib/modules/`uname -r` | grep snd
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/usb/caiaq/snd-usb-caiaq.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/usb/bcd2000/snd-bcd2000.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/usb/misc/snd-ua101.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/usb/snd-usb-audio.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/usb/line6/snd-usb-pod.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/usb/line6/snd-usb-line6.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/usb/line6/snd-usb-podhd.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/usb/line6/snd-usb-variax.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/usb/line6/snd-usb-toneport.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/usb/snd-usbmidi-lib.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/usb/usx2y/snd-usb-usx2y.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/usb/usx2y/snd-usb-us122l.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/usb/6fire/snd-usb-6fire.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/usb/hiface/snd-usb-hiface.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/intel/skylake/snd-soc-skl.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/intel/skylake/snd-soc-skl-ipc.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/intel/atom/sst/snd-intel-sst-acpi.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/intel/atom/sst/snd-intel-sst-core.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/intel/atom/snd-soc-sst-mfld-platform.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/intel/common/snd-soc-sst-dsp.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/intel/common/snd-soc-sst-acpi.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/intel/common/snd-soc-sst-match.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/intel/common/snd-soc-sst-ipc.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/intel/boards/snd-soc-skl_rt286.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/intel/boards/snd-soc-sst-cht-bsw-rt5672.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/intel/boards/snd-soc-sst-bytcr-rt5651.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/intel/boards/snd-soc-sst-cht-bsw-rt5645.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/intel/boards/snd-soc-skl_nau88l25_ssm4567.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/intel/boards/snd-skl_nau88l25_max98357a.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/intel/boards/snd-soc-sst-bytcr-rt5640.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/intel/boards/snd-soc-sst-cht-bsw-max98090_ti.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/snd-soc-core.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/codecs/snd-soc-max98090.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/codecs/snd-soc-hdac-hdmi.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/codecs/snd-soc-rt5640.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/codecs/snd-soc-dmic.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/codecs/snd-soc-rt5645.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/codecs/snd-soc-rt5670.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/codecs/snd-soc-rt286.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/codecs/snd-soc-rt5651.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/codecs/snd-soc-ts3a227e.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/codecs/snd-soc-rl6347a.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/codecs/snd-soc-rl6231.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/codecs/snd-soc-nau8825.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/soc/codecs/snd-soc-ssm4567.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/synth/emux/snd-emux-synth.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/synth/snd-util-mem.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/echoaudio/snd-indigodjx.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/echoaudio/snd-layla20.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/echoaudio/snd-gina24.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/echoaudio/snd-darla24.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/echoaudio/snd-echo3g.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/echoaudio/snd-indigo.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/echoaudio/snd-indigoio.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/echoaudio/snd-mona.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/echoaudio/snd-gina20.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/echoaudio/snd-layla24.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/echoaudio/snd-darla20.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/echoaudio/snd-mia.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/echoaudio/snd-indigoiox.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/echoaudio/snd-indigodj.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/trident/snd-trident.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/mixart/snd-mixart.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/snd-cmipci.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/snd-intel8x0.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/snd-via82xx-modem.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/snd-atiixp.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/ctxfi/snd-ctxfi.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/emu10k1/snd-emu10k1x.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/emu10k1/snd-emu10k1.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/emu10k1/snd-emu10k1-synth.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/ali5451/snd-ali5451.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/snd-via82xx.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/pcxhr/snd-pcxhr.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/rme9652/snd-hdspm.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/rme9652/snd-rme9652.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/rme9652/snd-hdsp.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/snd-es1968.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/lx6464es/snd-lx6464es.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/asihpi/snd-asihpi.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/snd-rme32.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/snd-intel8x0m.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/ca0106/snd-ca0106.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/korg1212/snd-korg1212.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/cs46xx/snd-cs46xx.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/snd-bt87x.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/ice1712/snd-ice1712.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/ice1712/snd-ice1724.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/ice1712/snd-ice17xx-ak4xxx.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/oxygen/snd-oxygen.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/oxygen/snd-virtuoso.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/oxygen/snd-oxygen-lib.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/vx222/snd-vx222.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/lola/snd-lola.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/snd-atiixp-modem.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/snd-ens1371.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/ac97/snd-ac97-codec.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/snd-ens1370.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/hda/snd-hda-codec-ca0110.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/hda/snd-hda-codec-ca0132.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/hda/snd-hda-codec.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/hda/snd-hda-codec-generic.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/hda/snd-hda-codec-cirrus.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/hda/snd-hda-codec-si3054.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/hda/snd-hda-codec-analog.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/hda/snd-hda-codec-realtek.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/hda/snd-hda-codec-cmedia.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/hda/snd-hda-intel.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/hda/snd-hda-codec-conexant.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/hda/snd-hda-codec-idt.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/hda/snd-hda-codec-hdmi.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/hda/snd-hda-codec-via.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/snd-rme96.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/snd-maestro3.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/au88x0/snd-au8820.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/au88x0/snd-au8810.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/au88x0/snd-au8830.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/pci/snd-ad1889.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/firewire/snd-isight.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/firewire/snd-scs1x.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/firewire/snd-firewire-speakers.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/firewire/snd-firewire-lib.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/drivers/snd-aloop.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/drivers/snd-mtpav.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/drivers/snd-virmidi.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/drivers/snd-dummy.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/drivers/mpu401/snd-mpu401-uart.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/drivers/mpu401/snd-mpu401.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/drivers/pcsp/snd-pcsp.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/drivers/opl3/snd-opl3-synth.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/drivers/opl3/snd-opl3-lib.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/drivers/vx/snd-vx-lib.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/core/snd-rawmidi.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/core/snd-compress.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/core/snd-hrtimer.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/core/snd.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/core/snd-timer.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/core/snd-pcm.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/core/seq/snd-seq-midi.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/core/seq/snd-seq-virmidi.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/core/seq/snd-seq-dummy.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/core/seq/snd-seq-midi-emul.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/core/seq/snd-seq-midi-event.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/core/seq/oss/snd-seq-oss.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/core/seq/snd-seq.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/core/seq/snd-seq-device.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/core/snd-hwdep.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/i2c/snd-cs8427.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/i2c/other/snd-pt2258.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/i2c/other/snd-ak4xxx-adda.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/i2c/other/snd-ak4113.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/i2c/other/snd-ak4114.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/i2c/other/snd-tea575x-tuner.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/i2c/snd-i2c.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/hda/ext/snd-hda-ext-core.ko.xz
/lib/modules/3.10.0-693.el7.x86_64/kernel/sound/hda/snd-hda-core.ko.xz

# I think so
-------------------------------------------------------------------------------


[biohpcadmin@biohpcwsc038 ~]$ lsmod | grep snd
snd_hda_codec_hdmi     47938  1 
snd_hda_codec_realtek    92355  1 
snd_hda_codec_generic    73789  1 snd_hda_codec_realtek
snd_hda_intel          39938  2 
snd_hda_codec         137153  4 snd_hda_codec_realtek,snd_hda_codec_hdmi,snd_hda_codec_generic,snd_hda_intel
snd_hda_core           85885  5 snd_hda_codec_realtek,snd_hda_codec_hdmi,snd_hda_codec_generic,snd_hda_codec,snd_hda_intel
snd_hwdep              13608  1 snd_hda_codec
snd_seq                62699  0 
snd_seq_device         14356  1 snd_seq
snd_pcm               106416  4 snd_hda_codec_hdmi,snd_hda_codec,snd_hda_intel,snd_hda_core
snd_timer              29822  2 snd_pcm,snd_seq
snd                    83383  14 snd_hda_codec_realtek,snd_hwdep,snd_timer,snd_hda_codec_hdmi,snd_pcm,snd_seq,snd_hda_codec_generic,snd_hda_codec,snd_hda_intel,snd_seq_device
soundcore              15047  1 snd		# looks NORMAL


# check if default `alsa` drivers are installed:

# yum list installed | grep -i 'alsa'
alsa-firmware.noarch               1.0.28-2.el7            @anaconda/7.4        
alsa-lib.x86_64                    1.1.3-3.el7             @anaconda/7.4        
alsa-plugins-pulseaudio.x86_64     1.1.1-1.el7             @anaconda/7.4        
alsa-tools-firmware.x86_64         1.1.0-1.el7             @anaconda/7.4        
alsa-utils.x86_64                  1.1.3-2.el7             @anaconda/7.4  

	# looks NORMAL 


# let's look at the permissions on sound device files

getfacl /dev/snd

getfacl: Removing leading '/' from absolute path names
# file: dev/snd
# owner: root
# group: root
user::rwx
group::r-x
other::r-x

  # COMMENT: user should include the currently logged in user as well

# Diagnosis: Combined with regular users no being able to return info via `aplay --list-devices`, it would seem that this issue was caused by a permissions

```

- Fix Attempt #1 


```

cat /etc/group | grep -i 'audio'
audio:x:63:	# no one's in this group 

# maybe add `rw-` ACL for user to the entire `/dev/snd` directory? 

sudo setfacl -m u:s186208:rw /dev/snd/*

# add user to `audio` group 

`usermod -a -G audio s186208`

# COMMENT: did not solve problems

``` 

- Further Diagnosis

 - when logged into root, sound plays just fine 

 - but when that specific user logs in, his session does not pick up audio controls ---> SUSPECT funky `.bashrc`


```
### on user's workstaion:

[s186208@biohpcwsc038 ~]$  pactl list sinks |egrep -e 'Sink|State'
Failed to create secure directory (/pulse): Permission denied
Connection failure: Connection refused
pa_context_connect() failed: Connection refused


[s186208@biohpcwsc038 ~]$  pulseaudio -v --check
E: [pulseaudio] core-util.c: Failed to create secure directory (/pulse): Permission denied
I: [pulseaudio] main.c: Daemon not running		# IT SHOULD BE RUNNING

[s186208@biohpcwsc038 ~]$  ps aux | grep pulse
s182510   12625  0.0  0.0 1359096 7288 ?        S<l  14:07   0:00 /usr/bin/pulseaudio --start --log-target=syslog
s186208   13695  0.0  0.0 112716   988 pts/0    S+   14:10   0:00 grep --color=auto pulse

  # this has to do with the "PulseAudio" daemon that talks to the "alsa" sound drivers [info here](https://en.wikipedia.org/wiki/PulseAudio#/media/File:Linux_kernel_and_daemons_with_exclusive_access.svg)

[s186208@biohpcwsc038 ~]$  pacmd list | grep "active port"
Failed to create secure directory (/pulse): Permission denied
No PulseAudio daemon running, or not running as session daemon.


### on my workstation:

[zpang1@biohpcwsc037 ~]$ ps aux | grep pulse  # the other user seemed to have on less process running locally 

zpang1    3285  0.0  0.0 3456936 9668 ?        S<l  09:26   0:10 /usr/bin/pulseaudio --start --log-target=syslog
zpang1   25886  0.0  0.0 112716   992 pts/6    S+   14:41   0:00 grep --color=auto pulse

[zpang1@biohpcwsc037 ~]$ pactl list sinks |egrep -e 'Sink|State'
Sink #1
	State: SUSPENDED
Sink #7
	State: SUSPENDED

   # I have permission locally

# check pulse audio daemon

[zpang1@biohpcwsc037 ~]$ pulseaudio -v --check
I: [pulseaudio] main.c: Daemon running as PID 3285   # good 

[zpang1@biohpcwsc037 ~]$ pacmd list | grep "active port"
	active port: <analog-output-speaker>
	active port: <hdmi-output-0>
	active port: <analog-input-front-mic>

```

- Fix Attempt #2 

```
vi /etc/pulse/daemon.conf

# changed 'daemonize = no' to 'daemonize = yes'

# changed 'system-instance = no' to 'system-instance= yes'  ### UPDATE: the pulse daemon's config file modifications alone were enough

# added user to pulseaudio related groups

[root@biohpcwsc038 ~]# usermod -a -G pulse-access s186208
[root@biohpcwsc038 ~]# usermod -a -G pulse-rt s186208
[root@biohpcwsc038 ~]# usermod -a -G pulse s186208
[root@biohpcwsc038 ~]# cat /etc/group | grep pulse
pulse-access:x:992:s186208
pulse-rt:x:991:s186208
pulse:x:171:s186208

# needs to reboot the system & try test things out

```

- Possible Fixes


```
### good error leads

```
aplay -l
aplay: device_list:270: no soundcards found...

```

```
Failed to create secure directory (/pulse): Permission denied

```

# 1. A long fix

# Try starting pulseaudio system wide upon boot

# The problem goes back to the PA (pulse audio) not running on under their account

[reference 1](https://unix.stackexchange.com/questions/338687/pulseaudio-as-system-wide-systemd-service)

[reference 2](https://www.raspberrypi.org/forums/viewtopic.php?t=156120) 


# 2. clear their `.bashrc`


#### References:

[rhel bugzilla link 1](https://bugzilla.redhat.com/show_bug.cgi?id=1585498) 

[pulse audio daemon info & fix](https://unix.stackexchange.com/questions/89977/pulseaudio-failed-to-create-secure-directory-in-nfs-share) 

[Ubuntu forum on sound debugging](https://askubuntu.com/questions/57810/how-to-fix-no-soundcards-found)

[Ubuntu forum on Pulse Audio](https://askubuntu.com/questions/15223/how-can-i-restart-pulseaudio-without-having-to-logout) 

[pavucontrol](https://unix.stackexchange.com/questions/175930/change-default-port-for-pulseaudio-line-out-not-headphones) 

[pactl 1](https://askubuntu.com/questions/998336/disable-and-enable-audio-output-from-the-command-line) 

[standard debug](https://www.osetc.com/en/centos-rhel-how-to-check-the-different-properties-of-sound-card.html) 

[misc forum 1](https://centos.org/forums/viewtopic.php?t=49619) 

[misc forum 2](https://www.centos.org/forums/viewtopic.php?t=68769)  

[misc forum 3](https://centos.org/forums/viewtopic.php?t=49619) 


### Ticket/Incident  July 29, 2019 

- original inquiry: 

```
User has trouble logging in after python code crashed 

``` 

- Diagnosis: 

```

$ ps aux | grep gnome  

### no gnome session process detected under user's ID 

  ### e.g. there should be an entry like this: 

-------------------------------------------------------------------------------

zpang1    6625  0.0  0.0  51412   580 ?        Ss   12:36   0:00 /usr/bin/ssh-agent /bin/sh -c exec -l /bin/bash -c "env GNOME_SHELL_SESSION_MODE=classic gnome-session --session gnome-classic"
zpang1    6664  0.0  0.0 233028  5988 ?        Sl   12:36   0:00 /usr/libexec/at-spi2-registryd --use-gnome-session
zpang1    6680  0.6  1.7 2944992 555920 ?      Sl   12:36   1:21 /usr/bin/gnome-shell

-------------------------------------------------------------------------------

``` 

- Solution: 

```
### Commented out extra lines in user's `.bashrc` 

  ## two lines related to loading modules 

  ## one line related to aliasing a directory 

```

- Conclusion: 

```
### Recommend users to NOT load MODULES in .bashrc---doing so will break things like `pulseaudio` daemons & gnome sessions in this case


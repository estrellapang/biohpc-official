## Extra Packages Added to WS per User-Request

### VSCode on CentOS & RHEL

- **Ticket**: 2020011310008053

\
\

### May 11th, 2021: Upgrade Firefox on Workstations

- `google-chrome-stable` requires `vulkan` and `vulkan-filesystem`

  - did a `yum download-only` and moved the relevant packages to

  - `/project/biohpcadmin/shared/rpms/WSs` 


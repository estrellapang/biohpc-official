## Workstation tickets: August 2019

### Aug.12: Ticket#2019080910007963 

- inquiry:


```
Hi Zengxing,


Yes. Can you mount it to /run/media/s185483? I will name it Backup_disk_1. I have another Seagate disk with the same problem, can you help me mount later?


Thanks,

Kai

```

- Investigation

```
NAME                         MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda                            8:0    0 238.5G  0 disk 
├─sda1                         8:1    0   200M  0 part /boot/efi
├─sda2                         8:2    0   500M  0 part /boot
└─sda3                         8:3    0 232.2G  0 part 
  ├─vg_biohpcws001-lv_root   253:0    0 148.8G  0 lvm  /
  ├─vg_biohpcws001-lv_swap   253:1    0    16G  0 lvm  [SWAP]
  ├─vg_biohpcws001-lv_home   253:2    0     1G  0 lvm  /home
  └─vg_biohpcws001-lv_shared 253:3    0  66.4G  0 lvm  /shared
sdb                            8:16   0   7.3T  0 disk 
├─sdb1                         8:17   0   128M  0 part 
└─sdb2                         8:18   0   7.3T  0 part   ## system picks it up


# see permissions on folder

[s185483@biohpctc0085 media]$ getfacl s185483/
# file: s185483/
# owner: root
# group: root
user::rwx
user:s185483:r-x  ### NO WRITE 
group::---
mask::r-x


# give user permission to write 

[root@biohpctc0085 media]# setfacl -Rm u:s185483:rwx s185483/
[root@biohpctc0085 media]# 
[root@biohpctc0085 media]# 
[root@biohpctc0085 media]# getfacl s185483/
# file: s185483/
# owner: root
# group: root
user::rwx
user:s185483:rwx
group::---
mask::rwx
other::---


# make mount point 

[s185483@biohpctc0085 ~]$ cd /run/media/s185483/
[s185483@biohpctc0085 s185483]$ mkdir Backup_disk_1
[s185483@biohpctc0085 s185483]$ 
[s185483@biohpctc0085 s185483]$ mount /dev/sdb2 /run/media/s185483/Backup_disk_1/
mount: only root can do that 

# mount as root

[root@biohpctc0085 ~]# mount /dev/sdb2 /run/media/s185483/Backup_disk_1/

### ERROR ###

$MFTMirr does not match $MFT (record 0).
Failed to mount '/dev/sdb2': Input/output error
NTFS is either inconsistent, or there is a hardware fault, or it's a
SoftRAID/FakeRAID hardware. In the first case run chkdsk /f on Windows
then reboot into Windows twice. The usage of the /f parameter is very
important! If the device is a SoftRAID/FakeRAID then first activate
it and mount a different device under the /dev/mapper/ directory, (e.g.
/dev/mapper/nvidia_eahaabcc1). Please see the 'dmraid' documentation
for more details.

```

- **Researching for Solution** 

```
[useful link 1](https://www.rootusers.com/how-to-mount-a-windows-ntfs-disk-in-linux/) 

[useful link2](https://wmarkito.wordpress.com/2010/12/29/how-to-fix-mftmirr-does-not-match-mft-record-0/) 

[useful link3](https://tecnicambalandia.blogspot.com/2011/06/mftmirr-does-not-match-mft-record-0.html)

# we need the `ntfs-3g` utility, which requires the EPEL repository

[root@biohpctc0085 ~]# yum search ntfs-3g
Loaded plugins: langpacks, product-id, rhnplugin, search-disabled-repos, subscription-manager
This system is receiving updates from RHN Classic or Red Hat Satellite.
==================================================================== N/S matched: ntfs-3g ====================================================================
ntfs-3g-devel.x86_64 : Development files and libraries for ntfs-3g
ntfs-3g.x86_64 : Linux NTFS userspace driver

  ## GOOD: we can install this on our RHEL WS' 

# confirm that the blockdevice is INDEED `ntfs` 

[root@biohpctc0085 ~]# blkid /dev/sdb2
/dev/sdb2: LABEL="Seagate Backup Plus Drive" UUID="4C1222F41222E326" TYPE="ntfs" PARTLABEL="Basic data partition" PARTUUID="c39ea21b-6e03-48b1-acc8-6c036135c261" 
 
  ## YEP.

# install ntfs-3g,ntfsprogs package

root@biohpctc0085 ~]# yum install ntfsprogs
Loaded plugins: langpacks, product-id, rhnplugin, search-disabled-repos, subscription-manager
This system is receiving updates from RHN Classic or Red Hat Satellite.
Package 2:ntfsprogs-2017.3.23-11.el7.x86_64 already installed and latest version
Nothing to do

[root@biohpctc0085 ~]# yum install ntfs-3g
Loaded plugins: langpacks, product-id, rhnplugin, search-disabled-repos, subscription-manager
This system is receiving updates from RHN Classic or Red Hat Satellite.
biohpc_el7                                                                                                           | 2.1 kB  00:00:00     
Package 2:ntfs-3g-2017.3.23-11.el7.x86_64 already installed and latest version

# Try `ntfsfix` command 

[root@biohpctc0085 ~]# ntfsfix /dev/sdb2
Mounting volume... $MFTMirr does not match $MFT (record 0).
FAILED
Attempting to correct errors... 
Processing $MFT and $MFTMirr...
Reading $MFT... OK
Reading $MFTMirr... OK
Comparing $MFTMirr to $MFT... FAILED
Correcting differences in $MFTMirr record 0...OK
Processing of $MFT and $MFTMirr completed successfully.
Setting required flags on partition... OK
Going to empty the journal ($LogFile)... OK
Checking the alternate boot sector... OK
NTFS volume version is 3.1.
NTFS partition /dev/sdb2 was processed successfully.

  ## GOOD! 


# Now mount block device --> WORKED! 

[root@biohpctc0085 ~]# mount /dev/sdb2 /run/media/s185483/Backup_disk_1/
[root@biohpctc0085 ~]# ls /run/media/s185483/Backup_disk_1/
Autorun.inf  BackupPlus.ico  Data  Kai_Backup  Start_Here_Mac.app  Start_Here_Win.exe  System Volume Information  Warranty.pdf


# Enable Permanent Mount 


[root@biohpctc0085 ~]# echo '/dev/sdb2 /run/media/s185483/Backup_disk_1/ ntfs-3g defaults 0 0' >> /etc/fstab
[root@biohpctc0085 ~]# 
[root@biohpctc0085 ~]# 
[root@biohpctc0085 ~]# cat /etc/fstab

#
# /etc/fstab
# Created by anaconda on Tue Nov  6 13:23:35 2018
#
# Accessible filesystems, by reference, are maintained under '/dev/disk'
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info
#
/dev/mapper/vg_biohpcws001-lv_root /                       ext4    defaults        1 1
UUID=58467865-2339-49a1-a2f1-523713f4d785 /boot                   ext4    defaults        1 2
UUID=A36D-32CF          /boot/efi               vfat    defaults,uid=0,gid=0,umask=0077,shortname=winnt 0 0
/dev/mapper/vg_biohpcws001-lv_home /home                   ext4    defaults        1 2
/dev/mapper/vg_biohpcws001-lv_shared /shared                 ext4    defaults        1 2
/dev/mapper/vg_biohpcws001-lv_swap swap                    swap    defaults        0 0
lysosome:/home2              /home2               nfs          nolock,hard,intr,async 0 0
lysosome:/home1              /home1               nfs          nolock,hard,intr,async 0 0
198.215.54.49@tcp0:198.215.54.50@tcp0:/project	/project	lustre	defaults,nosuid,flock,_netdev	0 0
/dev/sdb2 /run/media/s185483/Backup_disk_1/ ntfs-3g defaults 0 0


# VERIFY

[root@biohpctc0085 ~]# umount /run/media/s185483/Backup_disk_1/ 

[root@biohpctc0085 ~]# mount -a

[root@biohpctc0085 ~]# df -Th

Filesystem                                   Type      Size  Used Avail Use% Mounted on
/dev/mapper/vg_biohpcws001-lv_root           ext4      147G  102G   38G  73% /
devtmpfs                                     devtmpfs  7.7G     0  7.7G   0% /dev
tmpfs                                        tmpfs     7.8G   51M  7.7G   1% /dev/shm
tmpfs                                        tmpfs     7.8G  212M  7.5G   3% /run
tmpfs                                        tmpfs     7.8G     0  7.8G   0% /sys/fs/cgroup
/dev/sda2                                    ext4      477M  139M  309M  31% /boot
/dev/sda1                                    vfat      200M  9.8M  191M   5% /boot/efi
/dev/mapper/vg_biohpcws001-lv_home           ext4      880M   16M  798M   2% /home
/dev/mapper/vg_biohpcws001-lv_shared         ext4       57G   52M   54G   1% /shared
lysosome:/home2                              nfs        40T   11T   30T  27% /home2
lysosome:/home1                              nfs        37T   18T   19T  49% /home1
198.215.54.49@tcp:198.215.54.50@tcp:/project lustre    3.5P  2.7P  834T  77% /project
tmpfs                                        tmpfs     1.6G     0  1.6G   0% /run/user/607
tmpfs                                        tmpfs     1.6G     0  1.6G   0% /run/user/0
tmpfs                                        tmpfs     1.6G   96K  1.6G   1% /run/user/179389
tmpfs                                        tmpfs     1.6G  8.0K  1.6G   1% /run/user/42
tmpfs                                        tmpfs     1.6G  104K  1.6G   1% /run/user/162250
tmpfs                                        tmpfs     1.6G  192K  1.6G   1% /run/user/185483
tmpfs                                        tmpfs     1.6G  236K  1.6G   1% /run/user/178563
tmpfs                                        tmpfs     1.6G     0  1.6G   0% /run/user/58643
/dev/sdb2                                    fuseblk   7.3T  972G  6.4T  14% /run/media/s185483/Backup_disk_1

# Switch to user & verify

su - s185483

[s185483@biohpctc0085 ~]$ cd /run/media/s185483/Backup_disk_1
[s185483@biohpctc0085 Backup_disk_1]$ 
[s185483@biohpctc0085 Backup_disk_1]$ ls
./   Autorun.inf*     Data/        Start_Here_Mac.app/  System Volume Information/
../  BackupPlus.ico*  Kai_Backup/  Start_Here_Win.exe*  Warranty.pdf*

  ## Good! 

```

- **update**: the same has to be applied to the following workstations

`198.215.49.65,67,69,70,84` 

- **Implementation**

```
# Issue: cannot `ntfsfix` without having the drive connected to the workstations

  # or is it a one-time operation applied on the SSD's side but not the workstation's side? 

# Would the same drive be registered as `sdb` in all of the workstations???

  # Yes: ###.###.49.65,67,

# Check: correct NTFS packages

[root@biohpctc0065 ~]# yum list installed | grep -i 'ntfs'
ntfs-3g.x86_64                     2:2017.3.23-11.el7      @epel                
ntfsprogs.x86_64                   2:2017.3.23-11.el7      @epel

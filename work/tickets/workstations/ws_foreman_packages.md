## Workstations and Foreman Management

### Foreman Cert - May 11th, 2021

- on client, `puppet agent -t` will show if client certificate is expired

- to delete old cert on client:

  - `mv /ssl /ssl_old`

  - on Foreman: `puppet cert clean <ws_hostname>`

  - `/var/lib/puppet/ssl/ca/signed/` --> location of all clients' SSL certs on Foreman server; torenew all client certs, simply delete all certs here, and on client, do `puppet agent -t`

    - then on Foreman's webGUI, check "pending" certs, and "sign", also change host environment ton `production_el7`  

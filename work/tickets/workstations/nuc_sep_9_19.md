## Issues with NUC Thin Clients: Sep. 2019 

### Ticket#2019081210008027

- **ISSUE** 

- NUC cannot boot past "starting gnome desktop" and  "starting virtualization daemon"

- kernel error messages: 

```
nuovoton-cir 08:01: IR PHP Port not valid!

iwififi ... capa flags index 3 larger...

``` 


- **Troubleshooting** 

```
# regarding nuvoton-cir

  53  modprobe -v nuvoton-cir
   54  lsmod | grep -i nuvoton
   55  modprobe -r nuvoton-cir
   56  lsmod | grep -i nuvoton    # reloading the CIR driver didn't help 
   57  systemctl restart display-manager.service
   58  systemctl restart gdm.service   # restarting display-manager didn't help
   59  modinfo nuvoton_ci
   60  modinfo nuvoton_cir
   61  rmmod nuvoton_cir
   62  cd /lib/modules/3.10.0-693.el7.x86_64/kernel/drivers/media/rc/
   63  ls
   64  rm nuvoton-cir.ko.xz
   65  modinfo nuvoton_cir   # removed driver in question----kernel error disappeared, but still has issue starting the GUI

```

```
# regarding kernel messages on USB devices


 # MTP thinks the keyboard is a MTP device? 

------------------------------------------------------------------------------------------

Sep 10 22:41:46 biohpctcc007 mtp-probe: bus: 2, device: 100 was not an MTP device
Sep 10 22:41:55 biohpctcc007 sshd[28678]: error: Could not load host key: /etc/ssh/ssh_host_dsa_key
Sep 10 22:41:55 biohpctcc007 sshd[28678]: Connection closed by 198.215.49.64 port 60276 [preauth]
Sep 10 22:42:46 biohpctcc007 kernel: usb 2-4: USB disconnect, device number 100
Sep 10 22:42:48 biohpctcc007 kernel: usb 2-4: new low-speed USB device number 101 using xhci_hcd
Sep 10 22:42:48 biohpctcc007 kernel: usb 2-4: New USB device found, idVendor=17ef, idProduct=608d
Sep 10 22:42:48 biohpctcc007 kernel: usb 2-4: New USB device strings: Mfr=1, Product=2, SerialNumber=0
Sep 10 22:42:48 biohpctcc007 kernel: usb 2-4: Product: Lenovo USB Optical Mouse
Sep 10 22:42:48 biohpctcc007 kernel: usb 2-4: Manufacturer: PixArt
Sep 10 22:42:48 biohpctcc007 kernel: input: PixArt Lenovo USB Optical Mouse as /devices/pci0000:00/0000:00:14.0/usb2/2-4/2-4:1.0/input/input356
Sep 10 22:42:48 biohpctcc007 kernel: hid-generic 0003:17EF:608D.015B: input,hidraw1: USB HID v1.11 Mouse [PixArt Lenovo USB Optical Mouse] on usb-0000:00:14.0-4/input0
Sep 10 22:42:48 biohpctcc007 mtp-probe: checking bus 2, device 101: "/sys/devices/pci0000:00/0000:00:14.0/usb2/2-4"
Sep 10 22:42:48 biohpctcc007 mtp-probe: bus: 2, device: 101 was not an MTP device
Sep 10 22:43:48 biohpctcc007 kernel: usb 2-4: USB disconnect, device number 101
Sep 10 22:43:49 biohpctcc007 kernel: usb 2-4: new low-speed USB device number 102 using xhci_hcd
Sep 10 22:43:50 biohpctcc007 kernel: usb 2-4: New USB device found, idVendor=17ef, idProduct=608d
Sep 10 22:43:50 biohpctcc007 kernel: usb 2-4: New USB device strings: Mfr=1, Product=2, SerialNumber=0
Sep 10 22:43:50 biohpctcc007 kernel: usb 2-4: Product: Lenovo USB Optical Mouse
Sep 10 22:43:50 biohpctcc007 kernel: usb 2-4: Manufacturer: PixArt
Sep 10 22:43:50 biohpctcc007 kernel: input: PixArt Lenovo USB Optical Mouse as /devices/pci0000:00/0000:00:14.0/usb2/2-4/2-4:1.0/input/input357
Sep 10 22:43:50 biohpctcc007 kernel: hid-generic 0003:17EF:608D.015C: input,hidraw1: USB HID v1.11 Mouse [PixArt Lenovo USB Optical Mouse] on usb-0000:00:14.0-4/input0
Sep 10 22:43:50 biohpctcc007 mtp-probe: checking bus 2, device 102: "/sys/devices/pci0000:00/0000:00:14.0/usb2/2-4"
Sep 10 22:43:50 biohpctcc007 mtp-probe: bus: 2, device: 102 was not an MTP device
Sep 10 22:45:09 biohpctcc007 kernel: usb 2-4: USB disconnect, device number 102
Sep 10 22:45:11 biohpctcc007 kernel: usb 2-4: new low-speed USB device number 103 using xhci_hcd
Sep 10 22:45:11 biohpctcc007 kernel: usb 2-4: New USB device found, idVendor=17ef, idProduct=608d
Sep 10 22:45:11 biohpctcc007 kernel: usb 2-4: New USB device strings: Mfr=1, Product=2, SerialNumber=0
Sep 10 22:45:11 biohpctcc007 kernel: usb 2-4: Product: Lenovo USB Optical Mouse
Sep 10 22:45:11 biohpctcc007 kernel: usb 2-4: Manufacturer: PixArt
Sep 10 22:45:11 biohpctcc007 kernel: input: PixArt Lenovo USB Optical Mouse as /devices/pci0000:00/0000:00:14.0/usb2/2-4/2-4:1.0/input/input358
Sep 10 22:45:11 biohpctcc007 kernel: hid-generic 0003:17EF:608D.015D: input,hidraw1: USB HID v1.11 Mouse [PixArt Lenovo USB Optical Mouse] on usb-0000:00:14.0-4/input0
Sep 10 22:45:11 biohpctcc007 mtp-probe: checking bus 2, device 103: "/sys/devices/pci0000:00/0000:00:14.0/usb2/2-4"
Sep 10 22:45:11 biohpctcc007 mtp-probe: bus: 2, device: 103 was not an MTP device

-----------------------------------------------------------------------------------------------


# `lsusb` command identified the device in question -- keyboard? 

---------------------------------------------------------------
[root@biohpctcc007 biohpcadmin]# lsusb
Bus 001 Device 002: ID 8087:8001 Intel Corp. 
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 003 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 002 Device 004: ID 8087:0a2a Intel Corp. 
Bus 002 Device 104: ID 17ef:608d Lenovo 
Bus 002 Device 002: ID 17ef:6099 Lenovo 
Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub

[root@biohpctcc007 biohpcadmin]# lsusb
Bus 001 Device 002: ID 8087:8001 Intel Corp. 
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 003 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 002 Device 004: ID 8087:0a2a Intel Corp. 
Bus 002 Device 105: ID 17ef:608d Lenovo 
Bus 002 Device 002: ID 17ef:6099 Lenovo 
Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub

# device number keeps changing...
---------------------------------------------------------------



```

- **Possible Lead 1**

 - found prospects with respect to `udev` 

 - [link 1](https://superuser.com/questions/1206664/disable-mtp-udev-rules-for-specific-device-so-it-can-be-mount-as-a-usb-mass-stor) 

 - [link 2](https://www.tecmint.com/udev-for-device-detection-management-in-linux/) 

 - [link 3](https://www.reddit.com/r/archlinux/comments/3fi0i0/question_about_udev_rules_for_usb_keyboards/) 

 - [link 4](https://www.reddit.com/r/linuxquestions/comments/80dskd/udev_rule_to_ignore_usb_devices_input_event_but/)

 - [link 5](https://www.thegeekdiary.com/beginners-guide-to-udev-in-linux/) 

 - [link 6](https://wiki.archlinux.org/index.php/Modalias) 

 - [link 7](https://superuser.com/questions/249064/udev-rule-to-auto-load-keyboard-layout-when-usb-keyboard-plugged-in) 



- **alternative 1** (which worked!) 

 - just run an update!

```
yum --exclude=virtualbox*,initscripts*,zlib* update 

  # sept. 12, 2019 update: also performed the same operation on biohpctcc066 (Justine Pinskey @ Nicastro's Lab) 

  # sept. 27, 2019: performed update on biohpctc0074 (198.215.49.74) 

----------------------------------------------------------------------------------------------------------------
yum --exclude=lustre*,init*,zlib* update 

  gnome-dictionary.x86_64 0:3.26.1-2.el7                                      gnome-shell.x86_64 0:3.28.3-11.el7                            
  gnome-tweak-tool.noarch 0:3.28.1-2.el7.2                                    grub2.x86_64 1:2.02-0.80.el7                                  
  grub2-tools.x86_64 1:2.02-0.80.el7                                          grub2-tools-extra.x86_64 1:2.02-0.80.el7                      
  grub2-tools-minimal.x86_64 1:2.02-0.80.el7                                  insights-client.noarch 0:3.0.6-2.el7_6                        
  python2-pyatspi.noarch 0:2.26.0-3.el7                                       subscription-manager-rhsm.x86_64 0:1.24.13-3.el7_7            
  subscription-manager-rhsm-certificates.x86_64 0:1.24.13-3.el7_7             urw-base35-fonts.noarch 0:20170801-10.el7   

  caribou.x86_64 0:0.4.21-1.el7                   caribou-gtk2-module.x86_64 0:0.4.21-1.el7  caribou-gtk3-module.x86_64 0:0.4.21-1.el7    
  gnome-dictionary-libs.x86_64 0:3.20.0-1.el7     gnome-tweak-tool.noarch 0:3.22.0-1.el7     grub2.x86_64 1:2.02-0.64.el7                 
  grub2-tools.x86_64 1:2.02-0.64.el7              pyatspi.noarch 0:2.20.3-1.el7              python-rhsm.x86_64 0:1.19.9-1.el7            
  python-rhsm-certificates.x86_64 0:1.19.9-1.el7  python2-caribou.noarch 0:0.4.21-1.el7      redhat-access-insights.noarch 0:1.0.13-2.el7 
  urw-fonts.noarch 0:2.4-16.el7             

----------------------------------------------------------------------------------------------------------------

# Verify:

modinfo -i lustre

df -Th

dmesg | grep -i lustre

vi /var/log/messages

systemctl status nslcd

ps aux | grep -i gnome 

ps aux | grep 'X'

```

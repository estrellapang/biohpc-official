## Tickets Based on Routine Linux Operations: November 2019

### #2019103010008033

- inquiry:

```
Dear BioHPC team,

I am have a permission problem in the following folder:
/project/cryoem/cryoem_transfer/krios/s172986/Images-Disc/20191024

This folder contains the EM data I have collected on Oct 24 2019 and I did not have the problem with the folder earlier. Until I would like to share this folder to the lab members by typing
chmod 777 -r 20191024
After I typed this command, I lost my access to this folder. Could you help me with this issue and restore my access to this folder so that I can copy my data out

```

- solution:

```
d-wx-wx---+ 13 s172986 CryoEM_Transfer 4096 Oct 25 10:19 20191024


[root@Nucleus006 Images-Disc]# getfacl 20191024
# file: 20191024
# owner: s172986
# group: CryoEM_Transfer
user::-wx
group::rwx			#effective:-wx
group:CryoEM_Core:r-x		#effective:--x
mask::-wx
other::---
default:user::rwx
default:group::rwx
default:group:CryoEM_Core:r-x
default:mask::rwx


[root@Nucleus006 Images-Disc]# setfacl -R -m u:s172986:rwx 20191024/


[root@Nucleus006 Images-Disc]# getfacl 20191024/
# file: 20191024/
# owner: s172986
# group: CryoEM_Transfer
user::-wx
user:s172986:rwx
group::rwx
group:CryoEM_Core:r-x
mask::rwx
other::---
default:user::rwx
default:group::rwx
default:group:CryoEM_Core:r-x
default:mask::rwx
default:other::---

[root@Nucleus006 Images-Disc]# setfacl -R -m u::rwx 20191024/

[root@Nucleus006 Images-Disc]# getfacl 20191024/
# file: 20191024/
# owner: s172986
# group: CryoEM_Transfer
user::rwx
user:s172986:rwx
group::rwx
group:CryoEM_Core:r-x
mask::rwx
other::---
default:user::rwx
default:group::rwx
default:group:CryoEM_Core:r-x
default:mask::rwx
default:other::---

[root@Nucleus006 krios]# getfacl s172986
# file: s172986
# owner: s172986
# group: CryoEM_Transfer
user::rwx
group::rwx
group:CryoEM_Core:r-x
mask::rwx
other::---
default:user::rwx
default:group::rwx
default:group:CryoEM_Core:r-x
default:mask::rwx
default:other::---

  ### only CryoEM_Transfer & Core can see files, solution add CryoEM r-x to user directory

setfacl -R -m g:CryoEM:rx s172986/ 

[root@Nucleus006 krios]# getfacl s172986/
# file: s172986/
# owner: s172986
# group: CryoEM_Transfer
user::rwx
group::rwx
group:CryoEM:r-x
group:CryoEM_Core:r-x
mask::rwx
other::---
default:user::rwx
default:group::rwx
default:group:CryoEM_Core:r-x
default:mask::rwx
default:other::---

```




```

## Tickets Related to Operations on Cluster: Aug. 2019 

### Ticket#2019073110008083 

- original inquiry: 

```
# copy all files from `Project\shared\CRI_NGS_to_Zhu\Run332_CC_HZ_7-30-19` 

to `/archive/shared/zhu_wang/` 

# because "people in Hao’s lab don’t have access to my folder and I don’t have access to theirs" 

```


- prep-work: 

```
# log in to nucleus006 & become root
 

# check original directory & files' permissions 

[root@Nucleus006 CRI_NGS_to_Zhu]# ll Run332_CC_HZ_7-30-19/
total 4
drwxrwx---+ 8 s153851 CRI_NGS 4096 Jul 31 14:09 190730_NS500717_0398_AH2NLYBGXC


#  check s153851 --> is this user who submitted the ticket? 

[root@Nucleus006 CRI_NGS_to_Zhu]# ldapsearch -x uid=s153851

  # it is not "Yunguan," the user who submitted the ticket 

# s153851, ***CRI***, users, biohpc.swmed.edu
dn: uid=s153851,ou=CRI,ou=users,dc=biohpc,dc=swmed,dc=edu
objectClass: posixAccount
objectClass: shadowAccount
objectClass: inetOrgPerson
objectClass: sambaSamAccount
givenName: Jian
sn: Xu
cn: Jian Xu
***gid: 5005***
 
# check if Yunguan Wang & Jian Xu are actually in the same group 

ldapsearch -x sn=wang; --> look for Yunguan's entry

  # Yunguan does not belong to CRI nor does he belog to the same GID!!!  

# s190548, InternalMedicine, users, biohpc.swmed.edu
dn: uid=s190548,ou=InternalMedicine,ou=users,dc=biohpc,dc=swmed,dc=edu
displayName: Yunguan Wang
cn: Yunguan Wang
gidNumber: 21005


# Extra lead: original transfer email sent by "Cemere Celen"

ldapsearch -x sn=celen

# ccelen, ***CRI***, users, biohpc.swmed.edu
dn: uid=ccelen,ou=CRI,ou=users,dc=biohpc,dc=swmed,dc=edu
displayName: Cemre Celen
cn: Cemre Celen
title: graduate student
objectClass: inetOrgPerson
objectClass: posixAccount
objectClass: sambaSamAccount
objectClass: shadowAccount
loginShell: /bin/bash
sambaDomainName: BIOHPC
uidNumber: 414333
sambaSID: S-1-5-21-270981343-1041670446-3540155302-829666
sambaAcctFlags: [U]
gecos: Cemre Celen,*** Hao Zhu ***, CRI
sn: Celen
homeDirectory: /home2/ccelen
mail: cemre.celen@utsouthwestern.edu
postalAddress: NL12.120F
givenName: Cemre
employeeNumber: 414333
*** gidNumber: 5003 ***
uid: ccelen


# look up transfer target user's gid 

ldapsearch -x sn=zhu

# hzhu1, CRI, users, biohpc.swmed.edu
dn: uid=hzhu1,ou=CRI,ou=users,dc=biohpc,dc=swmed,dc=edu
displayName: Hao Zhu
cn: Hao Zhu
title: Assistant Professor
objectClass: inetOrgPerson
objectClass: posixAccount
objectClass: sambaSamAccount
objectClass: shadowAccount
loginShell: /bin/bash
sambaDomainName: BIOHPC
uidNumber: 134601
sambaSID: S-1-5-21-270981343-1041670446-3540155302-270202
sambaAcctFlags: [U]
gecos: Hao Zhu, Sean Morrison, Children's Research Institute
sn: Zhu
homeDirectory: /home2/hzhu1
postalCode: 8502
mail: hao.zhu@utsouthwestern.edu
postalAddress: NL12.138A
givenName: Hao
employeeNumber: 134601
gidNumber: 5003
uid: hzhu1
shadowExpire: 18173
shadowLastChange: 17808
sambaKickoffTime: 1570199649
sambaPwdLastSet: 1538663649


### Conclusion: 

  # staff in Zhu's lab wishes to access the data in `/project/shared/CRI_NGS_to_Zhu/Run332_CC_HZ-7-30-19`
 
  # both the people who generated the data (Xu's lab, 5005) & the people who need to access (Zhu's lab, 5003) it belong to the ou "CRI"

```



- implementation: 


```
# check how big the files are

du -sh Run332_CC_HZ_7-30-19/
38G	Run332_CC_HZ_7-30-19/

# secure copy, preserve mod. & access times 

scp -rvp [source] [target]  


# decided to **abandon** the above commands, and executed the following instead: 

`cp -rvp /project/shared/CRI_NGS_to_Zhu/Run332_CC_HZ-7-30-19 /archive/shared/zhu_wang`

  # `-v` was not a smart choice---as the directory contained MANY small files, and the verbose flag flood the terminal for nearly 1.5 hours during the transfer

  # **question**: why does copying ~30G of data take so long? Is it because of a LARGE number of small files? (when I copied my VM from /project to /work, ~7G of data took minutes to complete transfer)



### NOTE: WHY does the newly copied folder take up more space? Possibly due to differences in DDN Lustre vs. IBM GPFS..? 

# count numnber of files:

  # original location: 
 
root@Nucleus006 190730_NS500717_0398_AH2NLYBGXC]# ls -R | wc -l
71521
[root@Nucleus006 190730_NS500717_0398_AH2NLYBGXC]# pwd
/project/shared/CRI_NGS_to_Zhu/Run332_CC_HZ_7-30-19/190730_NS500717_0398_AH2NLYBGXC

  # target location: 

ot@Nucleus006 zhu_wang]# cd Run332_CC_HZ_7-30-19/190730_NS500717_0398_AH2NLYBGXC/
[root@Nucleus006 190730_NS500717_0398_AH2NLYBGXC]# 
[root@Nucleus006 190730_NS500717_0398_AH2NLYBGXC]# 
[root@Nucleus006 190730_NS500717_0398_AH2NLYBGXC]# ls -R | wc -l
71521

  # Number of files remains the SAME...


# change group & permission of folder & its contents once copied 

  ### NOTE: it appears that someone had already added a ACL for the Zhu_lab with rwx access--no need to further modify permissions, good thing the `-p` option was used when copying


# Aug. 2nd follow-up:

  # granted Yunguan Wang rw access to the entire directory

setfacl -Rm u:s190548:rx Run332_CC_HZ_7-30-19/

  ### NOTE: the above command would only work if the option is `-Rm` and NOT `-mR` 

  # confirm:

[root@Nucleus006 zhu_wang]# getfacl Run332_CC_HZ_7-30-19/
# file: Run332_CC_HZ_7-30-19/
# owner: s153851
# group: CRI_NGS
# flags: -s-
user::rwx
user:s184984:rwx
user:s190548:r-x
group::rwx
group:Zhu_lab:rwx
mask::rwx
other::---
default:user::rwx
default:user:s184984:rwx
default:group::rwx
default:group:Zhu_lab:rwx
default:mask::rwx
default:other::---

[useful ACL link](https://www.2daygeek.com/how-to-configure-access-control-lists-acls-setfacl-getfacl-linux/)

### getent group <gid/groupname>

```




### Ticket#2019071210007879 

- original inquiry

`user wanted to use X11 Port Forwarding on a MAC machine`
Good morning Junyu Guo,

Thanks for reporting your issue; so from Apple's support page, it's stated that X11 is longer included by default with MacOS's. (https://support.apple.com/en-us/HT201341)

In order to obtain the X11 Forwarding feature, you'd have to install the "XQuartz" package. Here is the official project link: (https://www.xquartz.org/)

Setup:  
---------------------------------------------------------------------------------------------------------------------------------------------
1. download & install XQuartz

2. logout/reboot in order for XQuartz to become your default X11 server
   - (requires you to shut down currently running apps)

3. log back in, and try the SSH with the "-X'" flag once more
    - before your SSH session @nucleus begins, the XQuartz app willl autostart (should be popping up on your Dock)

4. verify: once you are in, try inputting the command `gedit` ---> a simple GUI editor should launch in your local environment
---------------------------------------------------------------------------------------------------------------------------------------------

Let us know if this worked for you!
Zengxing Pang


### Aug. 22nd, 2019--testing Emacs via SSH X11 forwarding ###  

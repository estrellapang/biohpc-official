## Tickets on Browsers

### Ticket#2019121610008101

- [.lock file removal](https://unix.stackexchange.com/questions/78689/fix-firefox-is-already-running-issue-in-linux)

- [ffox version issue](https://bugzilla.redhat.com/show_bug.cgi?id=1455798)

- [firefox CLI ops](https://www.cyberciti.biz/faq/howto-run-firefox-from-the-command-line/)

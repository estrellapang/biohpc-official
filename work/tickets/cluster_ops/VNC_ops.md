## Tickets Related to VNC on the BioHPC Cluster

### Ticket#2019082310008033

- inquiry: user has the error while installing TurboVNC v2.2.2: 

```
.dmg package can't be opened because Apple cannot check it for malicious software

```

- solution:

 - [tried and tested](http://osxdaily.com/2016/09/27/allow-apps-from-anywhere-macos-gatekeeper/) 

 - [another possibility](https://www.charlessoft.com/)

   - [Pacifist tutorial 1](https://www.cnet.com/news/mini-tutorial-re-installing-apple-applications-from-a-mac-os-x-discupdate-package-using-pacifist/) 

   - [Pacifist tutorial 2](https://www.cnet.com/news/tutorial-what-to-do-when-a-mac-os-x-application-will-not-launch/) 

### Ticket#2020032510009222: Issues caused by python module 3.7.x-anaconda in .bashrc

- solution:

  - debug by submitting VNC sesssions on similar nodes; use SLURM commands to check his recent job submissions

  - comment out the module in user's .bashrc and things worked fine

## TIckets on MATlab

### SPM Suite: Ticket#2019121610008146

- **Relevant Links**

  - [link 1](https://www.fmrwhy.com/2018/06/28/spm12-matlab-scripting-tutorial-1/)

  - [link 2](https://www.mathworks.com/matlabcentral/fileexchange/68729-statistical-parametric-mapping)

  - [link 3](https://www.fil.ion.ucl.ac.uk/spm/software/)

  - [link 4](https://en.wikibooks.org/wiki/SPM)



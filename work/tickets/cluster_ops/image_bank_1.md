## Debugging on Omero Imagebank

### Resources 

- [Docs](https://docs.biohpc.swmed.edu/dokuwiki/doku.php?id=omero_change_log&s[]=image&s[]=bank)

### Ticket2019123010008101

- **Issue**

  - Omero server not responding to user logins 

- **Solution**

```
sudo su - omero

rm /opt/OMERO/.omero/repository/dd1a7415-d73b-492b-ad25-98f9283157cc/.lock

cd /opt/OMERO/OMERO.server

bin/omero web stop
bin/omero admin stopbin/omero admin start
bin/omero admin start
bin/omero web start

``` 

- Additional: if user still cannot login, instruct them to clear Browser cache


## Tickets Related to Data/File Transfer 

> Sept. 2019

### Ticket #2019090110008242

- Inquiry:

```
Please move the following folders in /work/Neuroinformatics_Core/hlai/:

2018_09_05_10X23_5224_0
LifeCanvas
LifeCanvas_D193CCD192nonCC

to 

/work/archive/Neuroinformatics_Core/Lai_lab/hlai

```

- Action: 

```
# evaluate size 

[root@Nucleus006 hlai]# du -sh 2018_09_05_10X23_5224_0/
81G	2018_09_05_10X23_5224_0/

[root@Nucleus006 hlai]# du -sh LifeCanvas
331G	LifeCanvas

[root@Nucleus006 hlai]# du -sh LifeCanvas_D193CCD192nonCC/
1.8T	LifeCanvas_D193CCD192nonCC/


# check permissions 

[root@Nucleus006 hlai]# ll 2018_09_05_10X23_5224_0/
total 0
drwxr-xr-x 3 hlai Takahashi_lab 4096 Nov 14  2018 analysis_results
drwxr-xr-x 3 hlai Takahashi_lab 4096 Nov 14  2018 raw
 
 ## all folders to be transferred need to have their primary groups modified to `Lai_lab` 

# use `scp -rp` to copy directory content recursively and preserver their ownerships

  # double verify where you are 

[root@Nucleus006 hlai]# pwd
/work/Neuroinformatics_Core/hlai


scp -rp 2018_09_05_10X23_5224_0/ /work/archive/Neuroinformatics_Core/Lai_lab/hlai/


ls -l /work/archive/Neuroinformatics_Core/Lai_lab/hlai/
total 0
drwxr-xr-x 4 hlai Takahashi_lab 4096 Nov 14  2018 2018_09_05_10X23_5224_0


scp -rp LifeCanvas /work/archive/Neuroinformatics_Core/Lai_lab/hlai/


### noticed significant LAG during this 2nd 331G transfer 

  # reason being that `scp` uses AES-128 encryption to in data transfer, which makes it slower than `ftp`, however, transfer speed can be improved upon by optionally choosing more lighweight encryption algorithms 

  # e.g. scp -c arcfour ... OR ... scp -c blowfish 

  # [useful link 1](https://www.garron.me/en/articles/scp.html)

  # [useful link 2](https://www.systutorials.com/5450/improving-sshscp-performance-by-choosing-ciphers/) 

  # [knowledge 1](https://serverfault.com/questions/478678/how-to-speed-up-file-transfer-from-server-to-server-over-the-internet) 

  # [knowlefge 2](https://serverfault.com/questions/559501/ftp-ftps-sftp-scp-speed-comparison) 



# try transfer last 1.8T directory with lighter encryption

scp -c arcfour LifeCanvas_D193CCD192nonCC/ /work/archive/Neuroinformatics_Core/Lai_lab/hlai/ 

 

## Tickets on SLURM Job Management: Starting 09-25-19

### Ticket#2019092510008046 

- inquiry:

```

Hello BioHPC,
 
Please would you extent a web visualization job I have running (32GB node) for a total time of 4 days?
The ID is: 1396123


```

- investigate:


```

[root@Nucleus006 ~]# scontrol show jobid 1396123
JobId=1396123 JobName=webGUI
   UserId=s164085(164085) GroupId=DLLab(11008) MCS_label=N/A
   Priority=4294226572 Nice=0 Account=(null) QOS=normal
   JobState=RUNNING Reason=None Dependency=(null)
   Requeue=1 Restarts=0 BatchFlag=1 Reboot=0 ExitCode=0:0
   RunTime=01:49:52 TimeLimit=20:00:00 TimeMin=N/A
   SubmitTime=2019-09-25T09:21:34 EligibleTime=2019-09-25T09:21:34
   StartTime=2019-09-25T09:21:34 EndTime=2019-09-26T05:21:35 Deadline=N/A
   PreemptTime=None SuspendTime=None SecsPreSuspend=0
   Partition=32GB AllocNode:Sid=portal-rhel7-live:8711
   ReqNodeList=(null) ExcNodeList=(null)
   NodeList=NucleusA046
   BatchHost=NucleusA046
   NumNodes=1 NumCPUs=32 NumTasks=1 CPUs/Task=1 ReqB:S:C:T=0:0:*:*
   TRES=cpu=32,node=1
   Socks/Node=* NtasksPerN:B:S:C=0:0:*:* CoreSpec=*
   MinCPUsNode=1 MinMemoryNode=0 MinTmpDiskNode=0
   Features=(null) Gres=(null) Reservation=(null)
   OverSubscribe=NO Contiguous=0 Licenses=(null) Network=(null)
   Command=/home2/s164085/portal_jobs/webGUI/webGUI.sh
   WorkDir=/home2/s164085/portal_jobs/webGUI
   StdErr=/home2/s164085/portal_jobs/webGUI/webGUI_1396123.out
   StdIn=/dev/null
   StdOut=/home2/s164085/portal_jobs/webGUI/webGUI_1396123.out
   Power=


  ## job will end in 1.5 days, running on TACC nodes--which are readily available
 
  ## extendsion granted

```


- action:


```
# extend user's job

scontrol update jobid=1396123 EndTime=2019-09-30T05:21:35

# scontrol show jobid 1396123

RunTime=02:32:22 TimeLimit=4-20:00:00 TimeMin=N/A
   SubmitTime=2019-09-25T09:21:34 EligibleTime=2019-09-25T09:21:34
   StartTime=2019-09-25T09:21:34 EndTime=2019-09-30T05:21:35 

  ## done--> "TimeLimit" is a good indicator of duration

```

### Ticket#2019101610008006


- inquiry:

```
Why my job is hold by Admin?
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
           1432268    GPUp40 Classify  s187445 PD       0:00      1 (JobHeldAdmin)

Thanks,

Junyu ( Richard) Guo

```

- investigate:

```
[zpang1@biohpcwsc037 ~]$ squeue --user s187445
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
           1427855    GPUp40     Seg1  s187445  R 5-05:01:53      1 NucleusC033
           1430246    GPUp40     Seg2  s187445  R 1-10:35:00      1 NucleusC023
           1432252    GPUp40   webGPU  s187445  R    6:39:18      1 NucleusC024
           1432425    GPUp40 Classify  s187445  R    5:41:17      1 NucleusC019

```
- resolve:

**User has reached the MAX # of GPU nodes allowed---which is four**


### Disclose All Jobs on a Partition:

----------------------------------------------------------------------
# see how many nodes per user
`sacct -r <partition> -s running —format=User,nnodes | sort | uniq -c`

# see what nodes each user is using
`squeue -p <partition>` 
----------------------------------------------------------------------

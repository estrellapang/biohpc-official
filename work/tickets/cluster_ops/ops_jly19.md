## Tickets Related to Using Services on The Cluster: July 2019

### Ticket#2019071210007879

- original inquiry:

```
I have a problem when I use SSH in terminal on Mac to connect bioHPC. I can login to bioHPC, but I can’t open any GUI editor to edit the code.
I use the following command: ssh -X s187445@nucleus.biohpc.swmed.edu
Do I need any setup to fix it?
Thanks.

```

- solution:

```

Thanks for reporting your issue; so from Apple's support page, it's stated that X11 is longer included by default with MacOS's. (https://support.apple.com/en-us/HT201341)

In order to obtain the X11 Forwarding feature, you'd have to install the "XQuartz" package. Here is the official project link: (https://www.xquartz.org/)

Setup:  
----------------------------------------------------------------------------------------------------------------------------------------
1. download & install XQuartz

2. logout/reboot in order for XQuartz to become your default X11 server
   - (requires you to shut down currently running apps)

3. log back in, and try the SSH with the "-X'" flag once more
    - before your SSH session @nucleus begins, the XQuartz app willl autostart (should be popping up on your Dock)

4. verify: once you are in, try inputting the command `gedit` ---> a simple GUI editor should launch in your local environment
----------------------------------------------------------------------------------------------------------------------------------------

```

- follow-up & solution: 

```

----------------------------------------------------------------------------------------------------------------------------------------
# Follow-up

Thank you for the information. It works. But it has some error messages when I open ‘gedit’ (or any other GUI software).
 

libGL error: No matching fbConfigs or visuals found

libGL error: failed to load driver: swrast

----------------------------------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------------------------------
# solution

I'm glad you can now visualize GUI apps/executables in your SSH sessions.

Regarding to the error (related to software rendering & OpenGL graphics engine) you are getting, I suspect it should not have anything to do with your local machine settings.

To test whether your machine is configured properly, start Terminal, and execute (locally)

`glxgears`

     --> this is a test program, which launches a window with displaying a set of 3-D gears;  if the gears are turning, your machine is set to support OpenGL graphics using GLX extensions.

To fetch more info on your 3D driver stack, run (locally in Terminal)

`   glxinfo | grep -E " version| string| rendering|display"   `

     --> as long as you see the entry "direct rendering: Yes" among the output, it should be fine.

----------------------------------------------------------------------------------------------------------------------------------------

```

### Ticket#2019071210007879


- original inquiry 


```
 yellow Mon Jul 22 10:43:58 CDT 2019 - Memory low
   Memory              Used       Total  Percentage
&green Physical         128028M     128701M         99%
&yellow Actual           103155M     128701M         80%
&green Swap              24102M      65535M         36%

```

- solution: 

```
ssh nucleus 

top --> once inside the top prompt, press `SHIFT + m` to view tasks by memory usage 

# if a user's process is taking significant amount of memory:

  # `kill -9 <PID>` 

  #  note down the users uid (before the process is killed) 

  #  email user & instruct them to run their jobs on compute nodes

```
 
### Ticket#2019062510007785

- original inquiry: 

```
gid 	Group_Name 	FILE_SYSTEM 	USED 	SOFT 	HARD(GB) 	TIME
2005 	white_lab 	/project 	5121 	5120 	7168 	Fri Jul 19 20:12:45

```

- solution 

```
getent group 2005 

[zpang1@biohpcwsc037 Learning_Linux]$ getent group 2005
white_lab:*:2005:shight

[zpang1@biohpcwsc037 Learning_Linux]$ ldapsearch -x uid=shight 

### email user & everyeone iunder her "gecos" entry 

``` 



### Ticket#2019072310007992 

- original inquiry

```
yellow Tue Jul 23 16:00:48 CDT 2019 up: 102 days, 132 users, 1840 procs, load=24.34
&yellow Load is HIGH


top - 16:00:49 up 102 days, 35 min, 132 users,  load average: 28.44, 24.34, 21.1
Tasks: 1842 total,   5 running, 1782 sleeping,  24 stopped,  31 zombie
%Cpu(s): 12.9 us,  2.5 sy,  0.0 ni, 84.3 id,  0.0 wa,  0.0 hi,  0.3 si,  0.0 st
KiB Mem : 13179001+total, 15979128 free, 31795548 used, 84015344 buff/cache
KiB Swap: 67108860 total, 41280788 free, 25828072 used. 89097856 avail Mem

   PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
22284 root       0 -20 23.244g 3.037g 1.865g S 108.7  2.4   3921:57 mmfsd
46073 s171926   20   0  180556   6512   3168 R  56.5  0.0   2:53.60 ssh
41487 s184554   20   0   20948   3708   2308 D  34.8  0.0   1:34.74 bedtools
41495 s184554   20   0   21100   3924   2308 R  26.1  0.0   1:02.16 bedtools
108279 s157824   20   0  569232 265872   2824 D  26.1  0.2  15:24.63 fastq-dump
41493 s184554   20   0   20148   3100   2308 D  21.7  0.0   0:51.70 bedtools
55646 xymon     20   0   57884   3796   1448 R  21.7  0.0   0:00.06 top
143557 s163194   20   0  152316   4960   1160 S  21.7  0.0   4:10.64 sshd
144275 s157824   20   0  133516  28512   2688 R  21.7  0.0   3:44.94 fastq-dump
144156 s157824   20   0  132684  27500   2688 D  17.4  0.0   4:04.22 fastq-dump
41475 s184554   20   0   20580   3328   2308 D  13.0  0.0   1:02.08 bedtools
41477 s184554   20   0   20540   3364   2308 D  13.0  0.0   0:56.77 bedtools
41489 s184554   20   0   20340   3100   2308 D  13.0  0.0   0:38.95 bedtools
41491 s184554   20   0   20708   3536   2308 D  13.0  0.0   1:00.77 bedtools
46072 s171926   20   0  160928   6372   1680 S  13.0  0.0   0:43.71 scp
  6993 nslcd     20   0  447956   6028   1696 S   8.7  0.0 225:20.42 nslcd
41481 s184554   20   0   20440   3324   2308 D   8.7  0.0   0:48.12 bedtools
41483 s184554   20   0   20368   3320   2308 R   8.7  0.0   0:28.02 bedtools
41484 s184554   20   0   20752   3600   2308 D   8.7  0.0   0:55.27 bedtools
41486 s184554   20   0   20652   3476   2308 D   8.7  0.0   0:33.97 bedtools
  3367 root      20   0       0      0      0 S   4.3  0.0  10:58.99 usb-stora+
  7616 root      20   0       0      0      0 S   4.3  0.0 325:54.99 kiblnd_sd+
  7638 root      20   0       0      0      0 S   4.3  0.0 154:39.40 ptlrpcd_0+
  7639 root      20   0       0      0      0 S   4.3  0.0 151:52.37 ptlrpcd_0+
  7640 root      20   0       0      0      0 S   4.3  0.0 154:43.95 ptlrpcd_0+
41478 s184554   20   0   20140   3084   2308 D   4.3  0.0   0:19.19 bedtools
41479 s184554   20   0   20140   3084   2308 D   4.3  0.0   0:14.81 bedtools
41480 s184554   20   0   21956   4780   2308 D   4.3  0.0   1:35.46 bedtools
41482 s184554   20   0   20760   3672   2308 D   4.3  0.0   0:44.12 bedtools
41485 s184554   20   0   20628   3484   2308 D   4.3  0.0   0:32.37 bedtools
41488 s184554   20   0   20952   3776   2308 D   4.3  0.0   1:11.00 bedtools
41492 s184554   20   0   20628   3296   2308 D   4.3  0.0   0:21.92 bedtools
41496 s184554   20   0   20664   3448   2308 D   4.3  0.0   0:30.61 bedtools
41498 s184554   20   0   20412   3264   2308 D   4.3  0.0   0:24.53 bedtools
135075 s187445   20   0  564308  54628  12544 S   4.3  0.0   0:11.29 emacs
143558 s163194   20   0  160892   6232   1536 D   4.3  0.0   1:47.15 scp
144225 s157824   20   0  133060  27968   2688 S   4.3  0.0   3:51.20 fastq-dump

```


- solution: 

```

### check which user-run process is taking significant CPU or Memory 

  # in exmaple above: we see that it's `bedtools` & `fastq-dump`

### check if either of the processes correspond to modules available on the login node 

  # `module avail` --> turns out `bedtools` is a module! (& fastq-dump is not) 

### reply & notify user: 

`Could you please move your fastq-dump run to compute nodes from nucleus005? is it not allowed to run jobs on login node`


```


### Ticket#2019072910008061 

- original inquiry: 

```
Ticket#2019072910008061 — Xymon [425618] rstudio.biohpc.swmed.edu:cpu warning (YELLOW) 


 PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
18608 s175864   20   0 5849844   2.8g  24172 R 100.0  4.4 797:20.53 rsession
24678 s421955   20   0   23.4g  22.6g  26280 R  94.4 36.1   6805:07 rsession
3126 xymon     20   0   61032   2720   1504 R  11.1  0.0   0:00.02 top 

```

- diagnosis: 

```
# logged into `rstudio.biohpc.swmed.edu` as root (?) 

  # `top`
_________________________________________________________________
PID     USER     PR   NI   VIRT   RES     SHR S  %CPU %MEM   TIME+ COMMAND                  
24678 s421955   20   0   23.7g  22.7g  26280 R 100.0 36.2   7345:13 rsession
_________________________________________________________________ 

# the other user's job has stopped, leaving one process utilizing large amount of CPU 

`ldapsearch -x uid=s421955` 

# emailed user & informed him about his 300+ days of `rsession` process 

## Current Speculation: user probably has Rstudio opened for a very long time 

  # link reporting similar issue: (https://support.rstudio.com/hc/en-us/community/posts/207687188-Erratic-CPU-load-both-for-RStudio-and-rsession-processes-on-Mac) 

```

### Ticket#2019073110008011 

- original inquiry: 

```
Sbgrid not working on BioHPC workstation

```

- diagnosis: 

```
ssh biohpcadmin@198.215.51.### 

su - <userid> 

### ERROR upon logging in


********************************************************************************
                  Software Support by SBGrid (www.sbgrid.org)
********************************************************************************
                              SBGrid Announcements
  - OpenMPI has been updated. RELION users please see
    <https://sbgrid.org/software/titles/relion> for mpirun version info.
********************************************************************************
 Your use of the applications contained in the /programs  directory constitutes
 acceptance of  the terms of the SBGrid License Agreement included  in the file
 /programs/share/LICENSE.  The applications  distributed by SBGrid are licensed
 exclusively to member laboratories of the SBGrid Consortium.
******************************************************************************** 
 SBGrid was developed with support from its members, Harvard Medical School,    
 HHMI, and NSF. If use of SBGrid compiled software was an important element     
 in your publication, please include the following reference in your work:      
                                                                                      
 Software used in the project was installed and configured by SBGrid.                   
 cite: eLife 2013;2:e01456, Collaboration gets the most out of software.                
********************************************************************************
 SBGrid installation last updated: 2018-12-19 (Update available)
 Please submit bug reports and help requests to:       <bugs@sbgrid.org>  or
                                                       <http://sbgrid.org/bugs>
********************************************************************************
 Capsule Status: InActive
       For additional information visit https://sbgrid.org/wiki/capsules
********************************************************************************


## check loaded modules

[s159269@biohpcwsb119 ~]$ module list
Currently Loaded Modulefiles:
  1) shared          2) slurm/16.05.8

  #  the module `SBGrid/latest` is not laoded

## check `.bashrc` 

cat .bashrc
# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific aliases and functions
module load shared
module load slurm 
. /programs/sbgrid.shrc

  ### notice how the script tries to run sbgrid without first loading it as a module? 

# check last reboot; suspect user previously had loaded SBgrid's module, but since it's not persistently loaded, upon reboot, it will become unavailable

who -b
         system boot  2019-07-31 11:39

  ## recently rebooted

# there is a sbgrid process running? Even though the module has not been loaded? 

ps aux | grep sbgrid
s159269  24714  0.0  0.0 112744  1020 pts/1    S+   12:39   0:00 grep --color=auto sbgrid

###########################WAIT#######################################################

## Attempt the replicate on my workstation 

[zpang1@biohpcwsc037 ~]$ module load SBGrid/latest 
[zpang1@biohpcwsc037 programs]$  
[zpang1@biohpcwsc037 programs]$ module list
init.c(556):ERROR:161: Cannot initialize TCL

  ## this message does not disappear until closing terminal session 

## jerry-rigged a solution:


`module load` --> `. /programs/sbgrid.shrc` --> ###ERROR Message### --> `[s159269@biohpcwsb119 ~]$ module list` --> module loaded

Currently Loaded Modulefiles:
  1) shared          2) slurm/16.05.8   3) SBGrid/latest


- solution: ******EVERYTHING STATED ABOVE WAS NOT RELEVANT********* 


- get SBGRID's contact info

- what is our license? 

- how does the BioHPC SBgrid work??? 

- ANSWER The following questions & document to `work/infrastructure/...`


### August 5th, 2019 Update ### 

``` 
# Liqiang diagnosed the issue, which consisted of our SBGrid VM server not synching with the remote repository 

# after sync, loading prompt shows 

-------------------------------------------------------------------------------

[zpang1@biohpcwsc037 ~]$ . /programs/sbgrid.shrc
********************************************************************************
                  Software Support by SBGrid (www.sbgrid.org)
********************************************************************************
 Your use of the applications contained in the /programs  directory constitutes
 acceptance of  the terms of the SBGrid License Agreement included  in the file
 /programs/share/LICENSE.  The applications  distributed by SBGrid are licensed
 exclusively to member laboratories of the SBGrid Consortium.
******************************************************************************** 
 SBGrid was developed with support from its members, Harvard Medical School,    
 HHMI, and NSF. If use of SBGrid compiled software was an important element     
 in your publication, please include the following reference in your work:      
                                                                                      
 Software used in the project was installed and configured by SBGrid.                   
 cite: eLife 2013;2:e01456, Collaboration gets the most out of software.                
********************************************************************************
 SBGrid installation last updated: 2019-08-03
 Please submit bug reports and help requests to:       <bugs@sbgrid.org>  or
                                                       <http://sbgrid.org/bugs>
********************************************************************************
 Capsule Status: Active
       For additional information visit https://sbgrid.org/wiki/capsules
********************************************************************************

-------------------------------------------------------------------------------

# however, when remoting into user's bash profile, the following errors occur when executing `nmrview` or `nmrviewj` 

could not open /com/onemoonscientific/swank/library/init.tcl



# CHECK CHANGELOG: 

https://docs.biohpc.swmed.edu/dokuwiki/doku.php?id=sbgrid_change_log 

   # also check portal page on SBGrid info 

```

## OnDemand Project Meetings and Discussions

### 04-15-2021

- [docs page](https://docs.biohpc.swmed.edu/dokuwiki/doku.php?id=applications-ondemand)

  - also check out BioHPC github for portal milestones

\
\

### Overview

- **Django**

  - current version v1.8 --> upgrade to v2.2

- **Features**

  - user registration forms

  - CAS; centralized authentication

  - changing No-VNC from Guacamole

  - SLURM-Kubernetes integration

  - software containerization and OnDemand -- Quay

\
\

### Portal Components

- Django CMS --> front page display management

- celery --> scheduler

- gunicorn --> webserver?

- guacamole --> VNC

\
\

#### Slurm External Runner

- "webGUI.sh" --> generate user passwds for webDesktop

- "slurm_exector.py" --> submits slurm jobs and corrects permissions to users 

  - is it "slurm_runner.py?"

\
\

#### Django

- requires database/data structure changes

  - "chunk upload/download"

\
\

#### WinDCV, KVM

- NiceCV

  - OpenGL forwarding?

- PCIe Passthrough w/h KVM

- Script and Form and batch jobs changed to "windows"

\
\

#### XDMoD

- standalone server

- XDMoD pulls info from portal database

- portal grabs info from XDMoD to display

\
\

#### User Registration Forms

- 

\
\

### CAS Authentication

- buy certificate from IR to use in CAS

\
\

### Guacamole --> NoVNC

- Terminal and VNC server --> Guacamole

- look into "HPC OnDemand"

- based on HTML5

- compare difference between Guac and NoVNC

\
\

### Slurm-Kubernetes Integration

- LSF is a possibility...

- ask David Trudgian for SLURM-Kubernetes integration

- or Kubernetes as a separate cluster 

\
\

#### To-Do

- cryospark and winDCV in next downtime

- quickly pull test developer environment to Django 2.2, check bugs

- open portal-test test environment

- create test-ldap server, copy database from production

  - create 'portal7' user on LDAP

- MA/ZP to test Guacamole vs. NoVNC

- MA to do KVM

\
\

### 04-29-2021

#### How `portal` user submits jobs

- via `sudo` privileges and via `slurm_external_runner.py`

- location of webGUI scripts on Git: `biohpc_portal/./blob/rhel*/biohpc_terminal/scripts/*`

  - webGUI/GPU script calculates the ssh port forwarding numbers via node numbers

  - `/opt/virtualGL/bin/vglrun` to launch gnome classic sessions

  - [RHEL 8 defaults to GNOME3, but option to launch Gnome Classic is still viable](https://www.redhat.com/en/blog/red-hat-enterprise-linux-8-gnome-and-display-server-changes)


#### KVM

- main scripts

- `webWinDCV.sh`

- `WinDCVup.sh`

  - launches a KVM vm on target/selected host

  - "pGina" windows authentication plugin for LDAP, and gets each user's LDAP password to setup authentication

  - uses "tightVNC" for forwarding
 
- node requirements

  - enable hardware virtualization, `intel_iommu_on` (`dmesg | grep -i iommu` to check)

  - there's more, check screenshots

- gpu up

  - generates private virtual IP

  - `VFIO_GROUP` --> points to proper GPU device

- requires nvidia license server

  - for PCI passthrough

\


- **challenge**

  - how to map storage mounts, and provisioning image local disks (writable? so that users can install their own programs e.g. in C://users/..)


## KVM Project with Murat Atis

### Overview

- winDCV software: RHEL Windows-VM GPU for Windows exclusive applications

  - uses KVM for passthrough


- **Setup**

  - windows ISO from /project

  - Windows Images has VNC Server: 5900

  - pgina - ldap for windows

  - KVM disk format: `QCOW2`

    - enables "copy on write" feature ---> when users make changes, disk images are not changed, instead, each user's changes are cached in their INDIVIDUAL files (advanced feature) 

    - bus: `VirtIO` better performance

  - nvidia driver

  - bridge adapter --> WinVM 

- portal script, relase GPU to user and port forward to WinVM

- WinDCV script (/portal_jobs/webWinDCV) -- release node resources to VM, performs passthrough from host GPU to VM (VM needs entry for virtual PCI card, Network configs, and other bus devices)

  - requireds vfio-net kernel modules for Pass Through (on host)

  - VM needs Nvidia Driver

  - needs virtual Network card

- look up on Docs

- **links**


### Progress

- Windows VM with NIC, connects to internet via NAT, realVNC server


### Tasks 

- change VM's network adapter to "port-forward" mode

  - right now, NAT cannot allow port forwarding to be conducted 

  - vnc needs to forward and reachable from vnc client

  - GPU needs to get recognized; Nvidia driver

  - netdev vs. nic virtual network management


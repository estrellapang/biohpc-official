## Instructions to Setup and Mount Remote Client Cluster 

### Sources

- [gpfs mount client cluster](https://www.ibm.com/support/knowledgecenter/STXKQY_5.0.3/com.ibm.spectrum.scale.v5r03.doc/bl1adv_admrmsec.htm)

- setup client cluster?

### client cluster info

- name: `biohpc-gpfs-client.cm.cluster` 

 - view name `mmlscluster` 


`
### Setup 

- **perequisites**

  - copy `/etc/hosts` to all nodes 
  
  - add nodes to client cluster; accept appropriate license    

  - need to AllowUser root@<all gpfs cluster nodes, gssio1-4, ems,et> 

  - may need add nodes' ib IP under it's own sshd_config

  - check if bass cluster's nodes' pub keys are added to client cluster nodes' `/root/authorized_keys`

  - all clients need to have same `/etc/hosts` files listing members of gpfs cluster 

- add note to client cluster

- for earlier steps refer to link

- owning cluster: Bass

- step 7. both owning & client cluster need to have eachother's pub key

 - under /root/bass_key/<>.pub

 - `mmremotecluster` with owning cluster's key 

   - use `-k` before `-n` (may need to copy "-k" from man page) 

- step 8. `mmremotefs add bassclient -f bassdata -C biohpc-ess.gssio1-hs -T /endosome`

  -  mount point will be created automatically after this

  - `mmlsfs` --> grep "-T" to show mount point on owning cluster (extra info; non critical) 

- step 9. mount

  - `mmount /endosome` 

  -  make sure mmfsd has started `mmgetstate` 


\
\

### Add ems2 as quorum manager

- /project/biohpcadmin/shared/GPFS-install/GPFS-5.0.4.2-DM/
 
- exchange pub keys (slave key) to ems2

- all nodes have the same `/etc/hosts` make sure not to erase headnode's hostname from /etc/hosts

- hop on NucleusA10

- `mmaddnode -N ems2-hs:quotrum-manager(or client) --accept` 



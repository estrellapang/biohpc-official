###

## Team Meet 02-05-2020

- R740 Installation Guide 

  - BIOS config (power)

- RHEL Satellite Security update (quarterly for internal services, monthly for DMZ)



### Davide’s Report

- Memory - RSS utilization

- slurm check point jobs



## GPFS AFM Planning: Feb-06-2020

### Highlights 

- Existing GL6S scale code-base upgrade; rolling upgrade  (live, incremental)—> migrate data

- Encryption at the same time: 

\
\
\

#### Details

compartmental update, one node at a time & firmware

ESS is like a headnode, but doesn’t server as a cluster manager—

isso3 is cluster manager

isso1 filesystem manager

GPFS client update? (3.5.0 current)

  - ansible or image

  - clients to be changed to new system (v5)

### Needs

- sudo wrapper had to be disabled?

  - restore root —> give root access to gpfsadmin

- HMC firmware update

  —> needs to upgrade FIRST 


  - curent: 

  - future: 

- no AFM software

  - RHEL 7.6 

- select fileset to replicate, not everything 


## GPFS AFM Planning: Feb-13-2020

- 2-IB ports total 
 
  —> bonding

  —> 


#### Hardware Tasks

- dual power in Bass, switches & afm ndes

- BMC in afm nodes

- HMC—> roles?



### Learning

- check quick deployment guide for ESS

  - build filesystems, etc

- discuss quorum options

### Base installation Tasks

- 7.6 boot drive 
- console access for AFM nodes?
- repos
- hosts
- resolv.conf
- vim
- ifconfig
- register RHEL license
- minimal security update
- proxy?
- ssh-key copy
- ntp servers


### Feb 18th, 2020—AFM Initial ###

## LOG:
——————————————————————————————————————
disable sudo wrapper

gnrjealthcheck

  - raid, network, enclosure,etc check

dump XCAT database & sort it 
——————————————————————————————————————

# Infiniband Install:


## Rolling Update:

- keep filesystem at v.4, but cluster to v.5 

   - keep “minimal release to v.4”  during transfer

   - right before moving clients —> bring clients to v.5 

- Migration:

  - Single-writer? independent write, local update?

  - AFM-AFM: both nodes will mount GPFS filesystem during replication

     - traditionally AFM uses NFS

  - AFM-DR (read-only): replication in a snapshot-like fashion, could still fail-over




##  performance tests:

- dd is not a good test for block not for file or multi-threaded

- nsdperf , gpfsperf, iozones are better tests

  
## General System:

- stretch cluster would only work with both sides having the same Storage Space

- Filesystem manager is separate from the cluster manager 

  —> moving to which nodes?

- page pools: 65% of local memory for GPFS/IO nodes

 “The GPFS pagepool attribute is used to cache user data and file system metadata. The pagepool mechanism allows GPFS to implement read as well as write requests asynchronously. Increasing the size of the pagepool attribute increases the amount of data or metadata that GPFS can cache without requiring synchronous I/O” 

- **disable sudo wrapper**:  `mmchluster`

```
| grep Remote —> check sudo wrapper in use

mmchcluser —nouse-sudo-wrapper. ## disable —> this causes issues

grep Remote


sudo /usr/lpp/mmfs/bin/mmchcluster —use-sudo-wrapper

~ mmegetstate -N <nodename> —> get status to see if communication is back


```

## Remote GPFS Mount for Clients:

- put clients in their own remote cluster (during migrating from CUH to Bass)

  - any new ESS cluster will remote mount to the created client cluster

  - needs upgrade client to new GPFS version (/biohpcadmin/shared/GPFS-5.0.4.2-DM)

  - Quorum in Client Cluster: 5-7 clients will become quorum nodes

     - 003, 006, NucleuS003

- 0.5 day reserved for client down time

## GPFS Client Update

- recommend to reboot client to make sure kernel module gpfs client comes back

- clients need to update to rhel 7.6 (and need reboot)

  - leaving clients to rhel 7.4 and do remote mount 

- 


## Retaining Quota 

- install openldap, enable it, enable quota, then enable clients


# Incremental Update:
- 7.2 —> 7.4 —> 7.6  (plus corresponding ESS versions have to be incrementally implemented as well)

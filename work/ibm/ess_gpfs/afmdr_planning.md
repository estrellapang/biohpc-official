## AFMDR Planning & Meeting Notes

### 8-20-2020 

> Mark III: Chris, Tim;  IBM: Don, Isaiah, Norm Bogard (bogey@us.ibm.com)

### Requirement Discussion

- AFMDR: Asynchronous replication

- DR: complete fileset failover

  - Secondary site: AFMDR uses and holds snap, but cannot be modified or else the failover may not work

  - IBM recommends: doing scheduled snapshot on Home cluster (through GUI)
  
  - normal replication cannot bring local snaps to remote, but DR can (secondary is read-only under normally)

    - DR uses its own snap to keep changes that occur on primary site

    - NOTE: AFMDR cannot independently keep snaps that DO NOT EXIST on the PRIMARY

    - When DR takes snaps, it's for when whole system is in DR mode--for sending new writes back to PRIMARY

- How Failover is setup: 

  - Secondary/DR site first replicates in RO from Primary

  - Clients need to be configured to remote mount Secondary site in order to failover; could mount on the same mountpoint

  - Secondary site queues changes during failover, and sends back to Primary after recovery

  - can choose which fileset to backup

  - BOTH primary & secondary sites need their independent protocol nodes

  - if Secondary does not have IB; then GATEWAY nodes needs use export primary to secondary via NFS---they will rely on Ethernet connections

    - if primary down; secondary has only ETHERNET---will primary clients access secondary via Ethernet? 


- Data recovery: granular retrieval

\
\

### 09-22-2020: Internal Discussion

- **difference between AFMDR and AFM

  - `afmAsyncDelay` 

  - needs to be behind live fs by 7 days? how long does it take?

- **recovery point objective (RPO) snapshots**

  - can only take one at a time; take after the traditional afm sync 

  - system restore point

- conclusion

  - try prefetch and set afmAsyncDelay (snapshot based) to 7 days

    - performance penalty---figure out time needed

  - read-only afm relationship, does that apply to the whole filesystem? can we create a separate fileset for independent use?

  - DO SNAPSHOTS need to be used in combination with AFMDR? can we cancel them?

  - how does backup/rsync affect encrypted filesets (see if we only use rsync)? 

\
\

### 10-06-2020

- **AFMDR Capabilities**

  - `afmAsyncDelay=7days` is not recommended, the metadata changes may get lost by keeping in queue in memory that long (the default interval of 15 seconds is recommended)

  - as an alternative, keep queuing active, and the prefetch not running every 7 days (testing needed)

  - Venkat claims that filesets that are "secondary mode" can independently handle read-write while the afm fileset stays in read only mode

  - snapshots; Venkat claims that we can directly "copy directory" to the live filesystem (testing needed?)

  - **idea**: use hybrid --- /work rsync & /clinical doing AFMDR snapshot

\
\

### 10-14-2020 AFM Migration Wrap Up Meeting & CUH GL6 Upgrade

- Point of Contact:

  - Wazim Reid (Isaiah, Don, Grant)

  - Isaiah goes first; Grant schedule to end of the week
 
\

- Next week Oct 19. 2020:

  - ESS upgrade (BE POWER8 system) 

  - SKLM 

\

- AFM license good---will not expire

\
\

### 10-26-2020 Mark III-IBM Call: Bass Unlink Fileset

- **Questions/Requirements**

  - converting a fileset to AFM requires time---depends on the # of files on the fileset

\

- **Prep Work**

  - these steps are separate from changing the fileset attribute themselves to Non-AFM

  - afm stop

  - unmount /Work

  - unlink CUH and Bass filesets

\

### 10-29-2020 Mark III - IBM Isaiah & Venkat

- to unlink the filesets --> all I/O must stop

- AFMDR recommends filesets 10 million files/fileset

- STEPS

  - clear warning LED lights

  - stop AFM, unmount /Work

  - upgrade CUH

  - downtime, unlink and convert --> select "No" for verification when executing mmchfileset

  - create /clinical fileset on BOTH --> encryption BEFORE FILES ARE ADDED

  - AFMDR recommend below 10- `mmhealth` 

- Nov. 17th / 30th

  - `mmhealth node show -N <node name>`

  - checks network, filesystem, perfmon, and threshold by default

  - e.g. `mmheatlh <> -v`

  - e.g. `mmhealth node show --unhealthy`  # show ONLY unhealthy nodes


\
\

### 11-11-2020 BioHPC Internal Discussion: Downtime Procedure

#### Chassis Replacement

- **shutdown cluster and unmount**

  - force unmount

  - shutdown gpfs daemon across cluster

  - setup script on shared directory, and setup cron job to timely execute; use `mmgetstate` to cross-validate

  - **gpfs main cluster shutdown**

  - **gpfs main cluster powerup**

    - cluster status checks; check redundant paths

- **AFMDR Conversion**

  - mmchfileset

\
\

#### 11-18-2020 IBM-MarkIII SKLM Planning

- Progress Thus-Far

  - SSL Cert created for Master, used for both sides

- SKLM IB IP

  - stop service, assign new IB-IP, restart service

  - alternative: assign new port in place of 2222

\
\

### 01-12-2021 IBM-MarkIII AFMDR Continuation

#### AFMDR Capabilities

- AFMDR caps 100 million files per fileset

  - intensive to recover for large filesets - this uses snapshots - requires several days to a week to recover WITHOUT replication

  - the long time interval for queued changes stays in AFM gateway node's memory/queue (no snapshot is used in replication) ---gateway nodes could be evicted and kicked from cluster --- thus losing the queue

  - solution under development, by year-end solution may be possible

  - AFM replication would also have the similar challenges in single writer and independent writer modes (home and cache can both be changed) --> it's less risky only because it does not run a recovery progress in tandem with data replication

  - for AFMDR, secondary is always read-only, and after fail-over, becomes read and writable

  - Verdict: DR auto fetches changes, whereas AFM relies on prefetch to fetch changes

\
\

#### AFM Capability Inquiries

- if users write to same file on both home and cache under Independent Writer mode, will there be a global lock?

  - if write to cache, changes will be synced to home

  - if simultaneous write, whoever writes last will be cached

  - "metadata changes --> home wins; data changes --> cache wins" (?)

- Independent Writer would impose the same 100 million file/scalability issues (read-only and local update modes do not suffer from this)

- In case of network loss

  - cache will pull from local copy only -- once network is established, the older version untouched in home will be put to .ptrash and the newer copy on cache will be selected

- **Gateway Nodes**

  - only pull or push primarily data changes in functionality

  - they handle the RPCs between home and cache -- responsible for read from home, writes to cache, vice versa: reads from cache, sync to home depending on AFM mode

  - multiple gateway nodes/parallel streams --- Master-Worker relationships will be established per fileset if enough nodes are available; can also split the files into separate writing streams

  - For AFM independent writer between CUH and Bass, Venkat does not recommend except read-only or local update mode --> use prefetch and policy engine to sync changes --> AFMDR can be applied after CUH fetches most of the data from Bass

\
\

#### AFM-AFMDR Existing Examples of Scalability

- **AFM**

- 30-40 PB; 2.3 billion files 

- **AFMDR**

- 17PB; 15 million files 

\
\

#### **OTS-Bass** possibility

- 16x100Gbit DWDM long-haul between sites (Home ~ 700 client nodes; Cache ~ 300 client nodes -- active-active mode)

- # of Gateway nodes recommended ??? Venkat suggests testing

- **two-cache scenario**

  - ?

\
\

#### Current Plan

- AFMDR convert back to AFM, requires a brief downtime

- Migration can start immediately, disable primary fileset on Bass then proceed

  - create AFM filesets on CUH

- Policies

  - list changes between home and cache, and run on regular intervals

- To-do's

  - initiate prefetch

  - Venkat to offer policies file samples

  - First week of Feb. 1st@9AM -- create /clinical fileset; start AFM prefetch 

  - email Angel over fileset names, cluster names, and mount points

\
  

#### Requirements

- No immediate deletion

  - AFM will delete that



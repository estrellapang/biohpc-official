## IBM Spectrum Discovery Try-Out

### Overview:

- capacity license: based on storage size

- location: on test gpfs VM

- IP: 198.215.54.132

- GUI: u:sdadmin p:Passw0rd

  - how to add a filesystem: "Admin" --> "Connections" 

  - "Connection Type" --> specify type of filesystem; open to S3,NFS,GPFS

  - "Scan directory" --> fileset/directory to scan

  - admin: "add connection" --> "spectrumscale" for type --> "spectrum-discover"  --> "root" as User (needs password-less ssh) --> password: "discover"  --> "/tmp" working directory --> scan directory --> cluster name --> filesystem --> "all" under node

\

- Metadata Section:

  - "Tags" --> enrich metadata, "Type?" 

  - "Policies" --> "edit policy"

    - "policy types" --> AUTOTAG,DEEP-INSPECT,CONTENT-SEARCH

- "Search" section

  - can do user, group specific tagging/usage tracking

\

- Performance:

  - 3-4 mins for a complete scan of 5T filesystem

  - lab-based insights perhaps

```
# 3 Main Types of Policies

AUTOTAG, CONTENTSEARCH, DEEP INSPECT

### AUTOTAG:

add "filter" --> LOOK UP DIFFERENT Types of filters 

Tags & values can be aadded to files that MATCH the filter

\

### for CONTENT-SEARCH:

 - SEARCHES WITHIN file themselves --> could take a while
  
 - generally relies on regular expressions to query the database for data types

 - new categoriezes can be added using regular expressions

\

### ACTION AGENT

  - REST API --> "Action Agents"

  - uses action agents, combined with kafka packages to run tasks on the actual GPFS filesystem --> enriched metada to be sent back to Discover database

```

\
\

### Test Plans:

- file usage over time, creation, changes over time

  - e.g. past 4 weeks by much growth & break down by type 

- Apply 180 Action Items

\
\

### 08-24-2020

- test case --> distribution based file type, by size and by number of files

### 09-21-2020

- Phase I Planning

  - marked by filesystem utilization, track how many files added, changed, how big, and where---specific to labs

  - integration with Portal bc of RESTful API (how does that work?)

    - e.g. per user login --> a scan is made to generate some results; like a quota report; maybe PI can choose the parameter to display usage



\

- Phase II Planning

  - Data growth prediction (check publication) e.g. based on file type distribution over time, make prediction (what deep learning models---what algorithems and pattern recognitions are used?)

\

- Phase III: Fancy

  - deadlock recognition --> mmfsd can give inode info--who has deadlock, but the mmfsd may not have the ability to get path based on inode info

  - can use file-scan to more quickly return path based innode

- Phase Lng-term (if viable)

  - web service forwarded to a specific port of portal IP, like xdmod

  - officially acquiring license and hardware for production

  - hire post-doc/intern dedicated to building the machine-learning model 

\

- ACTION ITEM:

  - speak with GD, Satwik, and Venkat on what type of attributes are useful to them 
 

\
\

### 10-05-2020

- test criteria:

  - files that have not been accessed/modified over a time period

  - department distribution(by PI groups) for a particular file type 
  
\
\

### 10-20-2020

- Session with Venkat Malladi for Discover Use Cases

  - spot files not-touched, off-load or create symlinks to cold storage locations

     - differentiate between Mtime and Atime

     - generate data on how MANY times per interval is a file accessed or read

     - vice versa; scan cold locations and recommend loading back to active filesystems

  - scan for group's files that do not sit in their native directories e.g. in other depts' folders and share folders

  - File type distributionts

    - dicom,nifty,fasqz,bam

    - monthly distribution reports to PIs 

  - reduce /work default qouta --> low usage

  - conda environments alternative besides from home2

    - educate users on better usage of /work
  
    - OR expansion of /home2

  - find file duplicates, recommend share links or deletion


\
\

### 11-02-2020: Meeting with LW

- **design notes**

  - daily scan; check io of scan processes

  - MySQL indexes for file entries has limitations, 256 TB?

  - Portal fetches static info from data processing  

  - db2 is heavily licensed, cannot bring to opensource platforms (commonly used for Banks)

- LW interested in OnDemand

  - user request --> onDemand --> portal via RestAPI --> discovery outputs graphical results --> ports to portal

- Issue cannot curl, the application cannot accept license

\
\

### 11-16-2020: Meeting with IBM Regarding OVA File CheckSum Error

- Original:

```
52273ba11df2452a60177f8469a71e822e2557e8a8cc968674274e40c7737efb  MetaOcean_2.0.3-57-signed.ova
```

- On cluster:

```
md5sum <file>.ova

sha1sum <file>.ova

sha256sum <file>.ova ---> this is A MATCH
```

- Causes:

  - newer versions of Discover does not support ESXI 6.0 (uses SHA1), but at least 6.5 and later (uses SHA256)


- Workaround: Convert the .ova into an .ovg file

  - [ovf opensource tool](https://www.vmware.com/support/developer/ovf/)

  - [support page forum 1](https://kb.vmware.com/s/article/2151537)

  - [evidence of vSphere 6.0 no longer supported](https://kb.vmware.com/s/article/66977)

  - [OVF Tool 4.4.0 Release download](https://my.vmware.com/group/vmware/downloads/details?downloadGroup=OVFTOOL441&productId=734)

  - [independent instructions on how to use it](http://www.vmwarearena.com/convert-ova-to-ovf-using-the-vmware-ovf-tool/)

  - download and run to convert checksum from sha256 to sha1

  - ~###ZxP~ + UTSW email

```
# run installer

[link to run installer](https://www.youtube.com/watch?v=g1AeQNmwNEk)

/usr/bin/ovftool --shaAlgorithm=SHA1 --allowExtraConfig <original>.ova <altered_SHA1>.ova

```

\

- **NEWS & Questions**

  - Discover 2.4.- can now be implemented on Openshift Cluster

  - Discover generated reports in Excel but cannot generate natively graphs

  - IBM virtual machine/containers in the backend of the Discover --> IBM promotes database recovery and re-install rather than startup-shutdown order (sold as "appliance")

  - supports NFS,SMB, but not samba just yet

\
\

### 12-14-2020: Internal Meeting

- run restAPI script on test-portal

- LW want directly querying the Discover databse via restAPIs --> Isom's scripts will not do

  - define specific functions for database queries

- LW wants querying read-only mount points for production data

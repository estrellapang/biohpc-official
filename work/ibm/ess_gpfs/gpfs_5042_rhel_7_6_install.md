## IBM Spectrum_Scale_Data_Management(5.0.4.2-x86_64) Install on RHEL 7.6

### COMMANDs

```
### Install GPFS on node

scp -r /project/biohpcadmin/shared/GPFS-5.0.4.2-DM/ root@10.100.164.26:/var/.

cd /var/GPFS-5.0.4.2-DM
md5sum -c Spectrum_Scale_Data_Management-5.0.4.2-x86_64-Linux-install.md5
chmod +x Spectrum_Scale_Data_Management-5.0.4.2-x86_64-Linux-install

./Spectrum_Scale_Data_Management-5.0.4.2-x86_64-Linux-install


# Accept the license by hitting "1" .


cd /usr/lpp/mmfs/5.0.4.2/gpfs_rpms/

yum install ksh
yum install openssl-devel
yum install cyrus-sasl-devel

rpm -ivh ./gpfs.msg.en_US-5.0.4-2.noarch.rpm ./gpfs.java-5.0.4-2.x86_64.rpm ./gpfs.gskit-8.0.50-86.x86_64.rpm ./gpfs.gpl-5.0.4-2.noarch.rpm ./gpfs.docs-5.0.4-2.noarch.rpm ./gpfs.crypto-5.0.4-2.x86_64.rpm ./gpfs.compression-5.0.4-2.x86_64.rpm ./gpfs.callhome-ecc-client-5.0.4-2.noarch.rpm ./gpfs.base-5.0.4-2.x86_64.rpm ./gpfs.adv-5.0.4-2.x86_64.rpm ./rhel/rhel7/python-subprocess32-3.2.7-0.el7.x86_64.rpm ./rhel/gpfs.librdkafka-5.0.4-2.x86_64.rpm ./rhel/gpfs.kafka-5.0.4-2.x86_64.rpm

/usr/lpp/mmfs/bin/mmbuildgp

```

### OUTPUTS

```
[root@afm04 GPFS-5.0.4.2-DM]# md5sum -c Spectrum_Scale_Data_Management-5.0.4.2-x86_64-Linux-install.md5
Spectrum_Scale_Data_Management-5.0.4.2-x86_64-Linux-install: OK


[root@afm04 GPFS-5.0.4.2-DM]# chmod +x Spectrum_Scale_Data_Management-5.0.4.2-x86_64-Linux-install
[root@afm04 GPFS-5.0.4.2-DM]# ls -l
total 2103756
-rwxr-xr-x. 1 root root 2154234384 Mar  2 14:28 Spectrum_Scale_Data_Management-5.0.4.2-x86_64-Linux-install
-rw-r--r--. 1 root root         94 Mar  2 14:27 Spectrum_Scale_Data_Management-5.0.4.2-x86_64-Linux-install.md5
-rw-r--r--. 1 root root        850 Mar  2 14:28 SpectrumScale_public_key.pgp


[root@afm04 GPFS-5.0.4.2-DM]# ./Spectrum_Scale_Data_Management-5.0.4.2-x86_64-Linux-install

Extracting License Acceptance Process Tool to /usr/lpp/mmfs/5.0.4.2 ...
tail -n +641 ./Spectrum_Scale_Data_Management-5.0.4.2-x86_64-Linux-install | tar -C /usr/lpp/mmfs/5.0.4.2 -xvz --exclude=installer --exclude=*_rpms --exclude=*_debs --exclude=*rpm  --exclude=*tgz --exclude=*deb --exclude=*tools* 1> /dev/null

Installing JRE ...

If directory /usr/lpp/mmfs/5.0.4.2 has been created or was previously created during another extraction,
.rpm, .deb, and repository related files in it (if there were) will be removed to avoid conflicts with the ones being extracted.

tail -n +641 ./Spectrum_Scale_Data_Management-5.0.4.2-x86_64-Linux-install | tar -C /usr/lpp/mmfs/5.0.4.2 --wildcards -xvz  ibm-java*tgz 1> /dev/null
tar -C /usr/lpp/mmfs/5.0.4.2/ -xzf /usr/lpp/mmfs/5.0.4.2/ibm-java*tgz
Defaulting to --text-only mode.

Invoking License Acceptance Process Tool ...
/usr/lpp/mmfs/5.0.4.2/ibm-java-x86_64-80/jre/bin/java -cp /usr/lpp/mmfs/5.0.4.2/LAP_HOME/LAPApp.jar com.ibm.lex.lapapp.LAP -l /usr/lpp/mmfs/5.0.4.2/LA_HOME -m /usr/lpp/mmfs/5.0.4.2 -s /usr/lpp/mmfs/5.0.4.2  -text_only

LICENSE INFORMATION

The Programs listed below are licensed under the following 
License Information terms and conditions in addition to the 
Program license terms previously agreed to by Client and 
IBM. If Client does not have previously agreed to license 
terms in effect for the Program, the International Program 
License Agreement (Z125-3301-14) applies.

Program Name (Program Number):
IBM Spectrum Scale Data Management Edition V5.0.4.2 (5737-
F34)
IBM Spectrum Scale Data Management Edition V5.0.4.2 (5641-
DM1)

Press Enter to continue viewing the license agreement, or 
enter "1" to accept the agreement, "2" to decline it, "3" 
to print it, "4" to read non-IBM terms, or "99" to go back 
to the previous screen.
1

License Agreement Terms accepted.

Extracting Product RPMs to /usr/lpp/mmfs/5.0.4.2 ...
tail -n +641 ./Spectrum_Scale_Data_Management-5.0.4.2-x86_64-Linux-install | tar -C /usr/lpp/mmfs/5.0.4.2 --wildcards -xvz  installer gpfs_rpms/rhel/rhel7 hdfs_debs/ubuntu16/hdfs_3.1.0.x hdfs_rpms/rhel7/hdfs_2.7.3.x hdfs_rpms/rhel7/hdfs_3.0.0.x hdfs_rpms/rhel7/hdfs_3.1.0.x hdfs_rpms/rhel7/hdfs_3.1.1.x smb_debs/ubuntu/ubuntu16 smb_debs/ubuntu/ubuntu18 zimon_debs/ubuntu/ubuntu16 zimon_debs/ubuntu/ubuntu18 ganesha_debs/ubuntu16 ganesha_rpms/rhel7 ganesha_rpms/rhel8 ganesha_rpms/sles12 gpfs_debs/ubuntu16 gpfs_rpms/sles12 object_debs/ubuntu16 object_rpms/rhel7 smb_rpms/rhel7 smb_rpms/rhel8 smb_rpms/sles12 tools/repo zimon_debs/ubuntu16 zimon_rpms/rhel7 zimon_rpms/rhel8 zimon_rpms/sles12 zimon_rpms/sles15 gpfs_debs gpfs_rpms manifest 1> /dev/null
   - installer
   - gpfs_rpms/rhel/rhel7
   - hdfs_debs/ubuntu16/hdfs_3.1.0.x
   - hdfs_rpms/rhel7/hdfs_2.7.3.x
   - hdfs_rpms/rhel7/hdfs_3.0.0.x
   - hdfs_rpms/rhel7/hdfs_3.1.0.x
   - hdfs_rpms/rhel7/hdfs_3.1.1.x
   - smb_debs/ubuntu/ubuntu16
   - smb_debs/ubuntu/ubuntu18
   - zimon_debs/ubuntu/ubuntu16
   - zimon_debs/ubuntu/ubuntu18
   - ganesha_debs/ubuntu16
   - ganesha_rpms/rhel7
   - ganesha_rpms/rhel8
   - ganesha_rpms/sles12
   - gpfs_debs/ubuntu16
   - gpfs_rpms/sles12
   - object_debs/ubuntu16
   - object_rpms/rhel7
   - smb_rpms/rhel7
   - smb_rpms/rhel8
   - smb_rpms/sles12
   - tools/repo
   - zimon_debs/ubuntu16
   - zimon_rpms/rhel7
   - zimon_rpms/rhel8
   - zimon_rpms/sles12
   - zimon_rpms/sles15
   - gpfs_debs
   - gpfs_rpms
   - manifest

Removing License Acceptance Process Tool from /usr/lpp/mmfs/5.0.4.2 ...
rm -rf  /usr/lpp/mmfs/5.0.4.2/LAP_HOME /usr/lpp/mmfs/5.0.4.2/LA_HOME

Removing JRE from /usr/lpp/mmfs/5.0.4.2 ...
rm -rf /usr/lpp/mmfs/5.0.4.2/ibm-java*tgz

==================================================================
Product packages successfully extracted to /usr/lpp/mmfs/5.0.4.2
 
   Cluster installation and protocol deployment
      To install a cluster or deploy protocols with the Spectrum Scale Install Toolkit:  /usr/lpp/mmfs/5.0.4.2/installer/spectrumscale -h
      To install a cluster manually:  Use the gpfs packages located within /usr/lpp/mmfs/5.0.4.2/gpfs_<rpms/debs>

      To upgrade an existing cluster using the Spectrum Scale Install Toolkit:
      1) Copy your old clusterdefinition.txt file to the new /usr/lpp/mmfs/5.0.4.2/installer/configuration/ location
      2) Review and update the config:  /usr/lpp/mmfs/5.0.4.2/installer/spectrumscale config update
      3) (Optional) Update the toolkit to reflect the current cluster config:
         /usr/lpp/mmfs/5.0.4.2/installer/spectrumscale config populate -N <node>
      4) Run the upgrade:  /usr/lpp/mmfs/5.0.4.2/installer/spectrumscale upgrade -h

      To add nodes to an existing cluster using the Spectrum Scale Install Toolkit:
      1) Add nodes to the clusterdefinition.txt file:  /usr/lpp/mmfs/5.0.4.2/installer/spectrumscale node add -h
      2) Install GPFS on the new nodes:  /usr/lpp/mmfs/5.0.4.2/installer/spectrumscale install -h
      3) Deploy protocols on the new nodes:  /usr/lpp/mmfs/5.0.4.2/installer/spectrumscale deploy -h

      To add NSDs or file systems to an existing cluster using the Spectrum Scale Install Toolkit:
      1) Add nsds and/or filesystems with:  /usr/lpp/mmfs/5.0.4.2/installer/spectrumscale nsd add -h
      2) Install the NSDs:  /usr/lpp/mmfs/5.0.4.2/installer/spectrumscale install -h
      3) Deploy the new file system:  /usr/lpp/mmfs/5.0.4.2/installer/spectrumscale deploy -h

      To update the toolkit to reflect the current cluster config examples:
         /usr/lpp/mmfs/5.0.4.2/installer/spectrumscale config populate -N <node>
      1) Manual updates outside of the install toolkit
      2) Sync the current cluster state to the install toolkit prior to upgrade
      3) Switching from a manually managed cluster to the install toolkit
              
==================================================================================
To get up and running quickly, visit our wiki for an IBM Spectrum Scale Protocols Quick Overview: 
https://www.ibm.com/developerworks/community/wikis/home?lang=en#!/wiki/General%20Parallel%20File%20System%20%28GPFS%29/page/Protocols%20Quick%20Overview%20for%20IBM%20Spectrum%20Scale
===================================================================================


[root@afm04 gpfs_rpms]# yum install ksh openssl-devel cyrus-sasl-devel

rpm -ivh ./gpfs.msg.en_US-5.0.4-2.noarch.rpm ./gpfs.java-5.0.4-2.x86_64.rpm ./gpfs.gskit-8.0.50-86.x86_64.rpm ./gpfs.gpl-5.0.4-2.noarch.rpm ./gpfs.docs-5.0.4-2.noarch.rpm ./gpfs.crypto-5.0.4-2.x86_64.rpm ./gpfs.compression-5.0.4-2.x86_64.rpm ./gpfs.callhome-ecc-client-5.0.4-2.noarch.rpm ./gpfs.base-5.0.4-2.x86_64.rpm ./gpfs.adv-5.0.4-2.x86_64.rpm ./rhel/rhel7/python-subprocess32-3.2.7-0.el7.x86_64.rpm ./rhel/gpfs.librdkafka-5.0.4-2.x86_64.rpm ./rhel/gpfs.kafka-5.0.4-2.x86_64.rpm
Preparing...                          ################################# [100%]
Updating / installing...
   1:gpfs.java-5.0.4-2                ################################# [  8%]
   2:python-subprocess32-3.2.7-0.el7  ################################# [ 15%]
   3:gpfs.docs-5.0.4-2                ################################# [ 23%]
   4:gpfs.gskit-8.0.50-86             ################################# [ 31%]
   5:gpfs.msg.en_US-5.0.4-2           ################################# [ 38%]
   6:gpfs.base-5.0.4-2                ################################# [ 46%]
Created symlink from /etc/systemd/system/multi-user.target.wants/mmautoload.service to /usr/lib/systemd/system/mmautoload.service.
   7:gpfs.gpl-5.0.4-2                 ################################# [ 54%]
   8:gpfs.crypto-5.0.4-2              ################################# [ 62%]
   9:gpfs.compression-5.0.4-2         ################################# [ 69%]
  10:gpfs.callhome-ecc-client-5.0.4-2 ################################# [ 77%]
  11:gpfs.adv-5.0.4-2                 ################################# [ 85%]
  12:gpfs.librdkafka-5.0.4-2          ################################# [ 92%]
checking for OS or distribution... ok (rhel)
checking for C compiler from CC env... failed
checking for gcc (by command)... ok
checking for C++ compiler from CXX env... failed
checking for C++ compiler (g++)... ok
checking executable ld... ok
checking executable nm... ok
checking executable objdump... ok
checking executable strip... ok
checking for pkgconfig (by command)... ok
checking for install (by command)... ok
checking for PIC (by compile)... ok
checking for GNU-compatible linker options... ok
checking for GNU linker-script ld flag... ok
checking for __atomic_32 (by compile)... ok
checking for __atomic_64 (by compile)... ok
checking for socket (by compile)... ok
parsing version '0x010000ff'... ok (1.0.0)
checking for librt (by pkg-config)... failed
checking for librt (by compile)... ok
checking for libpthread (by pkg-config)... failed
checking for libpthread (by compile)... ok
checking for c11threads (by pkg-config)... failed
checking for c11threads (by compile)... failed (disable)
checking for libdl (by pkg-config)... failed
checking for libdl (by compile)... ok
checking for zlib (by pkg-config)... ok
checking for zlib (by compile)... ok (cached)
checking for libcrypto (by pkg-config)... ok
checking for libcrypto (by compile)... ok (cached)
checking for libssl (by pkg-config)... ok
checking for libssl (by compile)... ok (cached)
checking for libsasl2 (by pkg-config)... ok
checking for libsasl2 (by compile)... ok (cached)
checking for zstd (by pkg-config)... failed
checking for zstd (by compile)... failed (disable)
checking for libm (by pkg-config)... failed
checking for libm (by compile)... ok
checking for liblz4 (by pkg-config)... failed
checking for liblz4 (by compile)... failed (disable)
checking for rapidjson (by compile)... failed (disable)
checking for crc32chw (by compile)... ok
checking for regex (by compile)... ok
checking for strndup (by compile)... ok
checking for strerror_r (by compile)... ok
checking for pthread_setname_gnu (by compile)... ok
checking for nm (by env NM)... ok (cached)
checking for python (by command)... ok
Generated Makefile.config
Generated config.h

Configuration summary:
  prefix                   /usr/local
  MKL_DISTRO               rhel
  SOLIB_EXT                .so
  ARCH                     x86_64
  CPU                      generic
  GEN_PKG_CONFIG           y
  ENABLE_SSL               y
  ENABLE_GSSAPI            y
  ENABLE_DEVEL             n
  ENABLE_VALGRIND          n
  ENABLE_REFCNT_DEBUG      n
  ENABLE_SHAREDPTR_DEBUG   n
  ENABLE_LZ4_EXT           y
  MKL_APP_NAME             librdkafka
  MKL_APP_DESC_ONELINE     The Apache Kafka C/C++ library
  CC                       gcc
  CXX                      g++
  LD                       ld
  NM                       nm
  OBJDUMP                  objdump
  STRIP                    strip
  CPPFLAGS                 -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align
  PKG_CONFIG               pkg-config
  INSTALL                  install
  LIB_LDFLAGS              -shared -Wl,-soname,$(LIBFILENAME)
  LDFLAG_LINKERSCRIPT      -Wl,--version-script=
  RDKAFKA_VERSION_STR      1.0.0
  MKL_APP_VERSION          1.0.0
  LIBS                     -lm -lsasl2   -lssl -lcrypto   -lcrypto   -lz   -ldl -lpthread -lrt
  CFLAGS                          
  CXXFLAGS                 -Wno-non-virtual-dtor
  SYMDUMPER                $(NM) -D
  exec_prefix              /usr/local
  bindir                   /usr/local/bin
  sbindir                  /usr/local/sbin
  libexecdir               /usr/local/libexec
  datadir                  /usr/local/share
  sysconfdir               /usr/local/etc
  sharedstatedir           /usr/local/com
  localstatedir            /usr/local/var
  libdir                   /usr/local/lib
  includedir               /usr/local/include
  infodir                  /usr/local/info
  mandir                   /usr/local/man
  BUILT_WITH               GCC GXX PKGCONFIG INSTALL GNULD LDS LIBDL PLUGINS ZLIB SSL SASL_CYRUS HDRHISTOGRAM SNAPPY SOCKEM SASL_SCRAM CRC32C_HW
Generated config.cache

Now type 'make' to build
make[1]: Entering directory `/opt/kafka/librdkafka-1.0.0/src'
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka.c -o rdkafka.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_broker.c -o rdkafka_broker.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_msg.c -o rdkafka_msg.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_topic.c -o rdkafka_topic.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_conf.c -o rdkafka_conf.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_timer.c -o rdkafka_timer.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_offset.c -o rdkafka_offset.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_transport.c -o rdkafka_transport.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_buf.c -o rdkafka_buf.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_queue.c -o rdkafka_queue.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_op.c -o rdkafka_op.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_request.c -o rdkafka_request.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_cgrp.c -o rdkafka_cgrp.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_pattern.c -o rdkafka_pattern.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_partition.c -o rdkafka_partition.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_subscription.c -o rdkafka_subscription.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_assignor.c -o rdkafka_assignor.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_range_assignor.c -o rdkafka_range_assignor.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_roundrobin_assignor.c -o rdkafka_roundrobin_assignor.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_feature.c -o rdkafka_feature.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdcrc32.c -o rdcrc32.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c crc32c.c -o crc32c.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdmurmur2.c -o rdmurmur2.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdaddr.c -o rdaddr.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdrand.c -o rdrand.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdlist.c -o rdlist.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c tinycthread.c -o tinycthread.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c tinycthread_extra.c -o tinycthread_extra.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdlog.c -o rdlog.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdstring.c -o rdstring.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_event.c -o rdkafka_event.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_metadata.c -o rdkafka_metadata.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdregex.c -o rdregex.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdports.c -o rdports.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_metadata_cache.c -o rdkafka_metadata_cache.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdavl.c -o rdavl.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_sasl.c -o rdkafka_sasl.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_sasl_plain.c -o rdkafka_sasl_plain.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_interceptor.c -o rdkafka_interceptor.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_msgset_writer.c -o rdkafka_msgset_writer.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_msgset_reader.c -o rdkafka_msgset_reader.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_header.c -o rdkafka_header.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_admin.c -o rdkafka_admin.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_aux.c -o rdkafka_aux.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_background.c -o rdkafka_background.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_idempotence.c -o rdkafka_idempotence.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdvarint.c -o rdvarint.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdbuf.c -o rdbuf.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdunittest.c -o rdunittest.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_sasl_cyrus.c -o rdkafka_sasl_cyrus.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_sasl_scram.c -o rdkafka_sasl_scram.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c snappy.c -o snappy.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdgz.c -o rdgz.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdhdrhistogram.c -o rdhdrhistogram.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_lz4.c -o rdkafka_lz4.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -O3 -c xxhash.c -o xxhash.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -O3 -c lz4.c -o lz4.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -O3 -c lz4frame.c -o lz4frame.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -O3 -c lz4hc.c -o lz4hc.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rddl.c -o rddl.o
gcc -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -c rdkafka_plugin.c -o rdkafka_plugin.o
Generating linker script librdkafka.lds from rdkafka.h
Creating shared library librdkafka.so.1
gcc  -shared -Wl,-soname,librdkafka.so.1 -Wl,--version-script=librdkafka.lds rdkafka.o rdkafka_broker.o rdkafka_msg.o rdkafka_topic.o rdkafka_conf.o rdkafka_timer.o rdkafka_offset.o rdkafka_transport.o rdkafka_buf.o rdkafka_queue.o rdkafka_op.o rdkafka_request.o rdkafka_cgrp.o rdkafka_pattern.o rdkafka_partition.o rdkafka_subscription.o rdkafka_assignor.o rdkafka_range_assignor.o rdkafka_roundrobin_assignor.o rdkafka_feature.o rdcrc32.o crc32c.o rdmurmur2.o rdaddr.o rdrand.o rdlist.o tinycthread.o tinycthread_extra.o rdlog.o rdstring.o rdkafka_event.o rdkafka_metadata.o rdregex.o rdports.o rdkafka_metadata_cache.o rdavl.o rdkafka_sasl.o rdkafka_sasl_plain.o rdkafka_interceptor.o rdkafka_msgset_writer.o rdkafka_msgset_reader.o rdkafka_header.o rdkafka_admin.o rdkafka_aux.o rdkafka_background.o rdkafka_idempotence.o rdvarint.o rdbuf.o rdunittest.o rdkafka_sasl_cyrus.o rdkafka_sasl_scram.o snappy.o rdgz.o rdhdrhistogram.o rdkafka_lz4.o xxhash.o lz4.o lz4frame.o lz4hc.o rddl.o rdkafka_plugin.o -o librdkafka.so.1 -lm -lsasl2   -lssl -lcrypto   -lcrypto   -lz   -ldl -lpthread -lrt
Creating static library librdkafka.a
ar rcs librdkafka.a rdkafka.o rdkafka_broker.o rdkafka_msg.o rdkafka_topic.o rdkafka_conf.o rdkafka_timer.o rdkafka_offset.o rdkafka_transport.o rdkafka_buf.o rdkafka_queue.o rdkafka_op.o rdkafka_request.o rdkafka_cgrp.o rdkafka_pattern.o rdkafka_partition.o rdkafka_subscription.o rdkafka_assignor.o rdkafka_range_assignor.o rdkafka_roundrobin_assignor.o rdkafka_feature.o rdcrc32.o crc32c.o rdmurmur2.o rdaddr.o rdrand.o rdlist.o tinycthread.o tinycthread_extra.o rdlog.o rdstring.o rdkafka_event.o rdkafka_metadata.o rdregex.o rdports.o rdkafka_metadata_cache.o rdavl.o rdkafka_sasl.o rdkafka_sasl_plain.o rdkafka_interceptor.o rdkafka_msgset_writer.o rdkafka_msgset_reader.o rdkafka_header.o rdkafka_admin.o rdkafka_aux.o rdkafka_background.o rdkafka_idempotence.o rdvarint.o rdbuf.o rdunittest.o rdkafka_sasl_cyrus.o rdkafka_sasl_scram.o snappy.o rdgz.o rdhdrhistogram.o rdkafka_lz4.o xxhash.o lz4.o lz4frame.o lz4hc.o rddl.o rdkafka_plugin.o
Creating librdkafka.so symlink
rm -f "librdkafka.so" && ln -s "librdkafka.so.1" "librdkafka.so"
Generating pkg-config file rdkafka.pc
Generating pkg-config file rdkafka-static.pc
Checking librdkafka integrity
librdkafka.so.1                OK
librdkafka.a                   OK
Symbol visibility              OK
make[1]: Leaving directory `/opt/kafka/librdkafka-1.0.0/src'
make[1]: Entering directory `/opt/kafka/librdkafka-1.0.0/src-cpp'
g++ -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align -Wno-non-virtual-dtor -c RdKafka.cpp -o RdKafka.o
g++ -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align -Wno-non-virtual-dtor -c ConfImpl.cpp -o ConfImpl.o
g++ -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align -Wno-non-virtual-dtor -c HandleImpl.cpp -o HandleImpl.o
g++ -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align -Wno-non-virtual-dtor -c ConsumerImpl.cpp -o ConsumerImpl.o
g++ -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align -Wno-non-virtual-dtor -c ProducerImpl.cpp -o ProducerImpl.o
g++ -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align -Wno-non-virtual-dtor -c KafkaConsumerImpl.cpp -o KafkaConsumerImpl.o
g++ -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align -Wno-non-virtual-dtor -c TopicImpl.cpp -o TopicImpl.o
g++ -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align -Wno-non-virtual-dtor -c TopicPartitionImpl.cpp -o TopicPartitionImpl.o
g++ -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align -Wno-non-virtual-dtor -c MessageImpl.cpp -o MessageImpl.o
g++ -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align -Wno-non-virtual-dtor -c HeadersImpl.cpp -o HeadersImpl.o
g++ -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align -Wno-non-virtual-dtor -c QueueImpl.cpp -o QueueImpl.o
g++ -MD -MP -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align -Wno-non-virtual-dtor -c MetadataImpl.cpp -o MetadataImpl.o
Creating shared library librdkafka++.so.1
gcc  -shared -Wl,-soname,librdkafka++.so.1 RdKafka.o ConfImpl.o HandleImpl.o ConsumerImpl.o ProducerImpl.o KafkaConsumerImpl.o TopicImpl.o TopicPartitionImpl.o MessageImpl.o HeadersImpl.o QueueImpl.o MetadataImpl.o -o librdkafka++.so.1 -L../src -lrdkafka -lstdc++
Creating static library librdkafka++.a
ar rcs librdkafka++.a RdKafka.o ConfImpl.o HandleImpl.o ConsumerImpl.o ProducerImpl.o KafkaConsumerImpl.o TopicImpl.o TopicPartitionImpl.o MessageImpl.o HeadersImpl.o QueueImpl.o MetadataImpl.o
Creating librdkafka++.so symlink
rm -f "librdkafka++.so" && ln -s "librdkafka++.so.1" "librdkafka++.so"
Generating pkg-config file rdkafka++.pc
Generating pkg-config file rdkafka++-static.pc
Checking librdkafka++ integrity
librdkafka++.so.1              OK
librdkafka++.a                 OK
make[1]: Leaving directory `/opt/kafka/librdkafka-1.0.0/src-cpp'
make -C examples
make[1]: Entering directory `/opt/kafka/librdkafka-1.0.0/examples'
gcc -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -I../src rdkafka_example.c -o rdkafka_example  \
	../src/librdkafka.a -lm -lsasl2   -lssl -lcrypto   -lcrypto   -lz   -ldl -lpthread -lrt
# rdkafka_example is ready
#
# Run producer (write messages on stdin)
./rdkafka_example -P -t <topic> -p <partition>

# or consumer
./rdkafka_example -C -t <topic> -p <partition>

#
# More usage options:
./rdkafka_example -h
gcc -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -I../src rdkafka_performance.c -o rdkafka_performance  \
	../src/librdkafka.a -lm -lsasl2   -lssl -lcrypto   -lcrypto   -lz   -ldl -lpthread -lrt
# rdkafka_performance is ready
#
# Run producer
./rdkafka_performance -P -t <topic> -p <partition> -s <msgsize>

# or consumer
./rdkafka_performance -C -t <topic> -p <partition>

#
# More usage options:
./rdkafka_performance -h
g++ -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align -Wno-non-virtual-dtor -I../src-cpp rdkafka_example.cpp -o rdkafka_example_cpp  \
	../src-cpp/librdkafka++.a ../src/librdkafka.a -lm -lsasl2   -lssl -lcrypto   -lcrypto   -lz   -ldl -lpthread -lrt -lstdc++
gcc -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -I../src rdkafka_consumer_example.c -o rdkafka_consumer_example  \
	../src/librdkafka.a -lm -lsasl2   -lssl -lcrypto   -lcrypto   -lz   -ldl -lpthread -lrt
# rdkafka_consumer_example is ready
#
./rdkafka_consumer_example <topic[:part]> <topic2[:part]> ..

#
# More usage options:
./rdkafka_consumer_example -h
g++ -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align -Wno-non-virtual-dtor -I../src-cpp rdkafka_consumer_example.cpp -o rdkafka_consumer_example_cpp  \
	../src-cpp/librdkafka++.a ../src/librdkafka.a -lm -lsasl2   -lssl -lcrypto   -lcrypto   -lz   -ldl -lpthread -lrt -lstdc++
g++ -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align -Wno-non-virtual-dtor -I../src-cpp kafkatest_verifiable_client.cpp -o kafkatest_verifiable_client  \
	../src-cpp/librdkafka++.a ../src/librdkafka.a -lm -lsasl2   -lssl -lcrypto   -lcrypto   -lz   -ldl -lpthread -lrt -lstdc++
gcc -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -I../src rdkafka_simple_producer.c -o rdkafka_simple_producer  \
	../src/librdkafka.a -lm -lsasl2   -lssl -lcrypto   -lcrypto   -lz   -ldl -lpthread -lrt
gcc -g -O2 -fPIC -Wall -Wsign-compare -Wfloat-equal -Wpointer-arith -Wcast-align  -I../src rdkafka_idempotent_producer.c -o rdkafka_idempotent_producer  \
	../src/librdkafka.a -lm -lsasl2   -lssl -lcrypto   -lcrypto   -lz   -ldl -lpthread -lrt
make[1]: Leaving directory `/opt/kafka/librdkafka-1.0.0/examples'
Updating
CONFIGURATION.md CONFIGURATION.md.tmp differ: byte 457, line 6
Checking  integrity
CONFIGURATION.md               OK
examples/rdkafka_example       OK
examples/rdkafka_performance   OK
examples/rdkafka_example_cpp   OK
make[1]: Entering directory `/opt/kafka/librdkafka-1.0.0/src'
Checking librdkafka integrity
librdkafka.so.1                OK
librdkafka.a                   OK
Symbol visibility              OK
make[1]: Leaving directory `/opt/kafka/librdkafka-1.0.0/src'
make[1]: Entering directory `/opt/kafka/librdkafka-1.0.0/src-cpp'
Creating shared library librdkafka++.so.1
gcc  -shared -Wl,-soname,librdkafka++.so.1 RdKafka.o ConfImpl.o HandleImpl.o ConsumerImpl.o ProducerImpl.o KafkaConsumerImpl.o TopicImpl.o TopicPartitionImpl.o MessageImpl.o HeadersImpl.o QueueImpl.o MetadataImpl.o -o librdkafka++.so.1 -L../src -lrdkafka -lstdc++
Checking librdkafka++ integrity
librdkafka++.so.1              OK
librdkafka++.a                 OK
make[1]: Leaving directory `/opt/kafka/librdkafka-1.0.0/src-cpp'
make[1]: Entering directory `/opt/kafka/librdkafka-1.0.0/src'
Install librdkafka to /usr/local
install -d $DESTDIR/usr/local/include/librdkafka && \
install -d $DESTDIR/usr/local/lib && \
install rdkafka.h $DESTDIR/usr/local/include/librdkafka && \
install librdkafka.a $DESTDIR/usr/local/lib && \
install librdkafka.so.1 $DESTDIR/usr/local/lib && \
[ -f "rdkafka.pc" ] && ( \
	install -d $DESTDIR/usr/local/lib/pkgconfig && \
	install -m 0644 rdkafka.pc $DESTDIR/usr/local/lib/pkgconfig \
) && \
[ -f "rdkafka-static.pc" ] && ( \
	install -d $DESTDIR/usr/local/lib/pkgconfig && \
	install -m 0644 rdkafka-static.pc $DESTDIR/usr/local/lib/pkgconfig \
) && \
(cd $DESTDIR/usr/local/lib && ln -sf librdkafka.so.1 librdkafka.so)
make[1]: Leaving directory `/opt/kafka/librdkafka-1.0.0/src'
make[1]: Entering directory `/opt/kafka/librdkafka-1.0.0/src-cpp'
Install librdkafka++ to /usr/local
install -d $DESTDIR/usr/local/include/librdkafka && \
install -d $DESTDIR/usr/local/lib && \
install rdkafkacpp.h $DESTDIR/usr/local/include/librdkafka && \
install librdkafka++.a $DESTDIR/usr/local/lib && \
install librdkafka++.so.1 $DESTDIR/usr/local/lib && \
[ -f "rdkafka++.pc" ] && ( \
	install -d $DESTDIR/usr/local/lib/pkgconfig && \
	install -m 0644 rdkafka++.pc $DESTDIR/usr/local/lib/pkgconfig \
) && \
[ -f "rdkafka++-static.pc" ] && ( \
	install -d $DESTDIR/usr/local/lib/pkgconfig && \
	install -m 0644 rdkafka++-static.pc $DESTDIR/usr/local/lib/pkgconfig \
) && \
(cd $DESTDIR/usr/local/lib && ln -sf librdkafka++.so.1 librdkafka++.so)
make[1]: Leaving directory `/opt/kafka/librdkafka-1.0.0/src-cpp'
  13:gpfs.kafka-5.0.4-2               ################################# [100%]
[root@afm04 gpfs_rpms]# 
[root@afm04 gpfs_rpms]# 
[root@afm04 gpfs_rpms]# 
[root@afm04 gpfs_rpms]# 
[root@afm04 gpfs_rpms]# 
[root@afm04 gpfs_rpms]# 
[root@afm04 gpfs_rpms]# /usr/l
lib/     lib64/   libexec/ local/   lpp/     
[root@afm04 gpfs_rpms]# /usr/lpp/mmfs/bin/mmbuildgpl
--------------------------------------------------------
mmbuildgpl: Building GPL (5.0.4.2) module begins at Mon Mar  2 22:09:17 CST 2020.
--------------------------------------------------------
Verifying Kernel Header...
  kernel version = 31000957 (310000957000000, 3.10.0-957.el7.x86_64, 3.10.0-957) 
  module include dir = /lib/modules/3.10.0-957.el7.x86_64/build/include 
  module build dir   = /lib/modules/3.10.0-957.el7.x86_64/build 
  kernel source dir  = /usr/src/linux-3.10.0-957.el7.x86_64/include 
  Found valid kernel header file under /usr/src/kernels/3.10.0-957.el7.x86_64/include
Verifying Compiler...
  make is present at /bin/make
  cpp is present at /bin/cpp
  gcc is present at /bin/gcc
  g++ is present at /bin/g++
  ld is present at /bin/ld
Verifying Additional System Headers...
  Verifying kernel-headers is installed ...
    Command: /bin/rpm -q kernel-headers  
    The required package kernel-headers is installed
make World ...
make InstallImages ...
--------------------------------------------------------
mmbuildgpl: Building GPL module completed successfully at Mon Mar  2 22:09:31 CST 2020.
--------------------------------------------------------

```

## AFM Installation Day 3

### Questions: ESS vs GSS (we have ESS)

### Reasons to Have Client Node

- Every time new cluster configs are added, the changes have to PROPAGATE to ALL clients in current config

- when ESS cluster has failures, the traffic is less busy


### moving recovery groups 

- move recovery group owned by io1 to io2

  - make io2 the server for the same recovery group

  - log:

### moved cluster manager to gssio4

### moved recovery belonging to gssio1 to gssio2, confirmed, umounted data from io1, and shutdown io1

```
@ems1 mmchrecoverygroup rg_gssio2-ib --servers gssio2-ib, gssio1-ib`

 # check: `mmlsrecoverygroup rg_gssio1-ib -L | grep active -A2`

  `mmlsrecoverygroup rg_gssio2-ib -L | grep active -A2`
 
--> should be that io2 has become primary for BOTH recovery groups

 # this propagates the cluster database to ALL clients--we have 500+ live clients, which takes A WHILE (20 mins)

## check state

`mmgetstate -N gss_ppc64,ems1-ib` --> check cluster status

## after io1 down, update it

`@ems1: updatenode gssio1 -P gss_updatenode`
```

### IO Node Upgrade sequence:

```

- Bring firmware update straight to the LATEST version 

  - ftp copy from HMC: EMS1 to ssio1--> firmware updates from the HMC network layer --> RHEL OpenO/Op protocol (similar to IPMI)

  - `mmchfirmware --type host-adapter` --> check firmware
 
```

### Firmware updates io1 requires that the 

```
# DEBUG: DELETE Following `sshd_config` for all gssio# nodes --> move recovery group --> remove config and restart sshd

root@10.100.164.20/28 --> delete host I.P.

#KeyRegenerationInterval 0
```

### add node to cluster --> no quorum --> add nodeclass --> replicate finish --> make quorum

```
mmaddnode -N <node #>:quorum:server --accept 

 # can specify quorum, node type (server ndode)

 # mmchnode --> change to non-quorum if needed
 
``` 

 
### Package Options for Spectrum Scale 

```
- [link](https://www.ibm.com/support/knowledgecenter/STXKQY_5.0.4/com.ibm.spectrum.scale.v5r04.doc/bl1ins_instswa.htm)

- packages selected for AFM nodes:

  - need to execute spectrumscale script to create directories: so copying rpms would not work

    gpfs.base-5.0.*.rpm
    gpfs.gpl-5.0.*.noarch.rpm
    gpfs.msg.en_US-5.0.*.noarch.rpm
    gpfs.gskit-8.0.50.*.rpm
    gpfs.license*.rpm

    gpfs.docs-5.0.*noarch.rpm
    gpfs.java-5.0.*.rpm
    gpfs.callhome-ecc-client-5.0.*.rpm
    gpfs.compression-5.0.*.rpm  # NO NEED
    gpfs.gui-5.0.*.rpm # NO NEED
    gpfs.kafka-5.0.*.x86_64.rpm (Red Hat Enterprise Linux x86_64 only)  # NO NEED
    gpfs.librdkafka-5.0.*.x86_64.rpm (Red Hat Enterprise Linux x86_64 only) # NO NEED

    gpfs.gss.pmcollector-5.0.*.rpm
    gpfs.gss.pmsensors-5.0.*.rpm


# dependencies: `ksh` & `gcc` 

```

- location in nodes:

```
ls -l /usr/lpp/mmfs/5.0.4.2/gpfs_debs/
total 242100
-rw-r--r--. 1 root root     39478 Jan 28 00:27 gpfs.adv_5.0.4-2_amd64.deb
-rw-r--r--. 1 root root  23030662 Jan 28 00:27 gpfs.base_5.0.4-2_amd64.deb
-rw-r--r--. 1 root root  22928548 Jan 28 00:27 gpfs.callhome-ecc-client_5.0.4-2_all.deb
-rw-r--r--. 1 root root     36980 Jan 28 00:27 gpfs.compression_5.0.4-2_amd64.deb
-rw-r--r--. 1 root root    552534 Jan 28 00:27 gpfs.crypto_5.0.4-2_amd64.deb
-rw-r--r--. 1 root root    522392 Jan 28 00:27 gpfs.docs_5.0.4-2_all.deb
-rw-r--r--. 1 root root    669088 Jan 28 00:27 gpfs.gpl_5.0.4-2_all.deb
-rw-r--r--. 1 root root  11619348 Jan 28 00:27 gpfs.gskit_8.0.50-86_amd64.deb
-rw-r--r--. 1 root root  78470164 Jan 28 00:27 gpfs.gui_5.0.4-2_all.deb
-rw-r--r--. 1 root root 109496590 Jan 28 00:28 gpfs.java_5.0.4-2_amd64.deb
-rw-r--r--. 1 root root      2294 Jan 28 00:28 gpfs.license.dm_5.0.4-2_amd64.deb
-rw-r--r--. 1 root root    200526 Jan 28 00:28 gpfs.msg.en-us_5.0.4-2_all.deb
-rw-r--r--. 1 root root      1616 Jan 28 00:28 gpfs.protocols-support_5.0.4-2_all.deb
-rw-r--r--. 1 root root    314544 Jan 28 00:28 gpfs.tct.client-1.1.7.3.x86_64.deb
-rw-r--r--. 1 root root      3948 Jan 28 00:34 Packages.gz
drwxr-xr-x. 2 root root       166 Jan 28 00:27 ubuntu16

```

### Infrastructure Insights

- GL6 System --> 2 recovery groups (boxes) per 2 I/O nodes

  - this means if one of the two is being updated, the OTHER I/O server has to finish update

  - this also means when we are changing primary owner of recovery group, the replacement can only be the other node in the pair

  - the system is physically configured the same way---hba adapters are criss-crossed connected ONLY across the enclosures/recovery groups in the same rack; not cross-rack connected.  

# quick node commands

check number of nodes --> specified with "n" option --> number of nodes that can be communicated to at once

`mmumount` --> umount filesystem

`mmshutdown` --> avoid `-A` option

mmlscluster  --> shows nodes 

### Bond Networking, Network Manager Behavior

```
### Check IB Interface Status
------------------
ibstat
------------------

### Show individual interface/device settings 
------------------
ip link show dev ib0
------------------

### 


### Show Connection Setting for Connection
------------------
nmcli c   # show connection profile names

nmcli c show bond-bond0

  --OR--

nmcli c show id 'bond-bond0'

  # grep for specific attributes, i.e. `nmcli c show bond-bond0 | grep -i mtu`
------------------

### Check if interfaces datagram mode 
------------------
cat /sys/class/net/ib0/mode
datagram
------------------

### NM_CONTROLLED relationship with connections/interfaces
-----------------


-----------------


 






```

### Set bond slaves with 4092 MTU
--------------------------------------------------------------------------------
echo "NM_CONTROLLED=yes" >> /etc/sysconfig/network-scripts/ifcfg-bond-slave-ib0
echo "NM_CONTROLLED=yes" >> /etc/sysconfig/network-scripts/ifcfg-bond-slave-ib1
cat /etc/sysconfig/network-scripts/ifcfg-bond-slave-ib0
cat /etc/sysconfig/network-scripts/ifcfg-bond-slave-ib1

nmcli c show bond-bond0 | grep mtu
nmcli c show bond-slave-ib0 | grep mtu
nmcli c show bond-slave-ib1 | grep mtu
nmcli c show ib0 | grep mtu
nmcli c show ib1 | grep mtu

nmcli c modify 'ib0' infiniband.mtu 4092
nmcli c modify 'ib1' infiniband.mtu 4092
nmcli c modify 'bond-slave-ib0' infiniband.mtu 4092
nmcli c modify 'bond-slave-ib1' infiniband.mtu 4092

nmcli c reload

systemctl restart network

## don't use ifup/ifdown to test fail-over


--------------------------------------------------------------------------------



### IB Active-Backup Bond Integrity Test

```
# check current active slave for bond0

cat /sys/class/net/bond0/bonding/active_slave
--------------------------------------------------------------------------------
ib1
--------------------------------------------------------------------------------

  -OR-

cat /proc/net/bonding/bond0
--------------------------------------------------------------------------------
Ethernet Channel Bonding Driver: v3.7.1 (April 27, 2011)

Bonding Mode: fault-tolerance (active-backup) (fail_over_mac active)
Primary Slave: None            
Currently Active Slave: ib1    ### Here is the active slave
MII Status: up
MII Polling Interval (ms): 100
Up Delay (ms): 0
Down Delay (ms): 0

Slave Interface: ib0
MII Status: up
Speed: 100000 Mbps
Duplex: full
Link Failure Count: 1
Permanent HW addr: 20:00:10:8f:fe:80:00:00:00:00:00:00:98:03:9b:03:00:c1:3b:4c
Slave queue ID: 0

Slave Interface: ib1
MII Status: up
Speed: 100000 Mbps
Duplex: full
Link Failure Count: 0
Permanent HW addr: 20:00:10:8f:fe:80:00:00:00:00:00:00:98:03:9b:03:00:c1:3b:5c
Slave queue ID: 0
--------------------------------------------------------------------------------

```

### Resources:


- [RHEL nmcli modify connection name & mtu](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/networking_guide/sec-Configuring_IP_Networking_with_nmcli) 

- [RHEL 7 Networking Guide: Chap. 12--IB](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/networking_guide/sec-configuring_ipoib)

- [nmcli guide 1](https://developer.gnome.org/NetworkManager/stable/nmcli.html)
 
- [nmcli guide: edit connection attributes](https://www.linuxquestions.org/questions/linux-networking-3/nmcli-connection-questions-4175616996/)
 
- [nmcli guide 2 RHEL 7](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/networking_guide/sec-Configuring_IP_Networking_with_nmcli)

- [nmcli guide 4 RHEL 8](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_networking/getting-started-with-nmcli_configuring-and-managing-networking)

- [nmcli guide 3](https://www.golinuxcloud.com/nmcli-command-examples-cheatsheet-centos-rhel/)

- [nmcli create bond trivial](https://advantech-ncg.zendesk.com/hc/en-us/articles/360025316411-How-to-set-NIC-bonding-in-Linux-through-nmcli-command)

- [RHEL explain connection profile vs. device](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/networking_guide/sec-network_bonding_using_the_command_line_interface)

- [gen bonding guide](https://www.thegeekdiary.com/centos-rhel-7-how-to-create-an-interface-bonding-nic-teaming-using-nmcli/) 

- [RHEL 8 bonding guide: auto start slave info](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_networking/configuring-network-bonding_configuring-and-managing-networking)

- [IBM RDMA best practice](https://www.ibm.com/developerworks/community/wikis/home?lang=en#!/wiki/General%20Parallel%20File%20System%20%28GPFS%29/page/Best%20Practices%20RDMA%20Tuning)

- [IBM Network best practice](https://www.ibm.com/developerworks/community/wikis/home?lang=en#!/wiki/General+Parallel+File+System+(GPFS)/page/Best+Practices+Network+Tuning)



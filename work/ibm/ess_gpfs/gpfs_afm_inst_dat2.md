### AFM Installation Day 2—02/19/20

### Remote Mount

- less traffic between client nodes and cluster

- slow nodes won't get kicked out of cluster

- sudo wrapper won't affect the clients during updates

- AFM 

### Replication

- mount GPFS on AFM, so we rely on nsd to transfer data rather than via NFS native for protocol nodes

- afm manages the data replication but doesn't do the bulk work

- /archive --> TCT --> azure cold storage

 - gpfs cluster can set policy to move data straight to cloud

### Performance Test Info

- mdtest (metadata test) 

- dd direct flag --> means no cache/buffer

- check MTU on ports and interfaces -- 4092!!! (2044 is default)

- check if the mode of IB interfaces are in "datagram" mode 

### IB Bond

- 10.100.191.52/19 (AFM02)

- `bond0` inherits the config for bonded I.P.  —> this will be master

   - UID needed from both or several slaves to create bond interface

- `mmcrnodeclass`

  - `mmchnodeclass`

  - `mmaddnodeclass`

  - in order to set verbs ports --> add server to cluster --> create bond --> add server to node class 

- **create bond**

```
## check if interfaces up

ibstat

nmcli c  --> brings up UUID of each interface

## bring down infiniband ports

ifdown ib0

ifdown ib1

## add bond0 --> ifcg file will be created 

nmcli c add type bond ifname bond0 mode active-backup ip4 10.100.191.52/19

nmcli c
NAME           UUID                                  TYPE        DEVICE 
eno1           abf4c85b-57cc-4484-4fa9-b4a71689c359  ethernet    eno1   
ib0            21669912-273c-492d-9a9a-305d70f93d98  infiniband  ib0    
bond-bond0     e788f225-dc9f-4436-b36e-0f9e38b10528  bond        bond0  
eno2           46a031f5-3498-486d-8fd2-1f3c952d4aad  ethernet    --     
enp0s20f0u1u6  72f33f14-35f4-4042-a3d1-015733099291  ethernet    --     
ib1            2c07366d-ada9-4a49-8d6f-f96006011ebf  infiniband  --     


`nmcli d` for more info


## add slaves

 nmcli c add type bond-slave ifname ib0 master bond0
Connection 'bond-slave-ib0' (15e17d24-09d1-4faa-b2e6-30a0a13a7787) successfully added.

nmcli c add type bond-slave ifname ib1 master bond0
Connection 'bond-slave-ib1' (0fa44b4c-134f-4d1c-843d-1a39b589b2da) successfully added.

## change ifcfg for slave ports

vi ifcfg-bond-bond0

vi ifcfg-bond-bond1

# changes
----------------------
TYPE=Infiniband

NM_CONTROLLED=yes

MTU=4092  ### any time this is changed, nmcli reload or systemctl restart network wouldn't implement the change, ifup & ifdown will work
----------------------

# bring up slave interfaces

 ifup bond-slave-ib1

 ifup bond-slave-ib0

nmcli c reload

nmcli c/d --> only slaves & bond are supposed to be up, not ib0, ib1

cat /proc/net/bonding/bond0
Ethernet Channel Bonding Driver: v3.7.1 (April 27, 2011)

Bonding Mode: fault-tolerance (active-backup) (fail_over_mac active)
Primary Slave: None
Currently Active Slave: ib0
MII Status: up
MII Polling Interval (ms): 100
Up Delay (ms): 0
Down Delay (ms): 0

Slave Interface: ib0
MII Status: up
Speed: 100000 Mbps
Duplex: full
Link Failure Count: 0
Permanent HW addr: 20:00:10:8f:fe:80:00:00:00:00:00:00:98:03:9b:03:00:c1:3a:2c
Slave queue ID: 0

Slave Interface: ib1
MII Status: up
Speed: 100000 Mbps
Duplex: full
Link Failure Count: 0
Permanent HW addr: 20:00:10:8f:fe:80:00:00:00:00:00:00:98:03:9b:03:00:c1:3a:24

## AFM IB LOG
--------------------------------
ifup bond0

(process:134025): GLib-GIO-WARNING **: 12:31:04.435: gdbusobjectmanagerclient.c:1589: Processing InterfaceRemoved signal for path /org/freedesktop/NetworkManager/IP4Config/19 but no object proxy exists

(process:134025): GLib-GIO-WARNING **: 12:31:04.435: gdbusobjectmanagerclient.c:1589: Processing InterfaceRemoved signal for path /org/freedesktop/NetworkManager/IP6Config/19 but no object proxy exists
Connection successfully activated (master waiting for slaves) (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/16)

### all but afm01 has "incorrect address" messages from `ifconfig` --> look into it

  ## check slave bond
--------------------------------

```

### AFM Setup

- install ESS installer on AFM so they can be added to the cluster

  - pulls rpms, gpls, enables integration to cluster 

- bond the ib interfaces —> assign 1 IB I.P.

- **AFM in CUH as “Home” first —assign them as “GATEWAY” —>  transfer from old to new —> then turn new AFM in Bass as “Home” —> Assign CUH AFM as “Cache”**




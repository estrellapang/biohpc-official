## General Network & System Configurations for GL6S Servers

### Install RHEL "Infrastructure Server" Package Group

```
# crete repo file for RHEL7.6 ISO

vi /etc/yum.repos.d/rhel_7_6_iso.repo

# copy exact contents into it
------------------------------------
[InstallMedia]
name=Red Hat Enterprise Linux 7.6
mediaid=1539194970.388895
metadata_expire=-1
gpgcheck=1
cost=500
enabled=1
baseurl=file:///mnt/rhel_7_6_iso
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
------------------------------------


yum repolist  

  # you should see "Install Media" now listed

# make mount point & make it available to filesystem

mkdir /mnt/rhel_7_6_iso/

mount -o loop /root/rhel-server-7.6-x86_64-dvd.iso /mnt/rhel_7_6_iso/

  # do a `df -Th` to see it mounted

# group install "Infrastructure Server"

  # the package "kmod-kvdo" from online repo will force system to install RHEL7.7 kernel, so we will have to version-lock it to 7.6 version

yum install yum-plugin-versionlock.noarch
yum versionlock kmod-kvdo-6.1.1.125-5.el7.x86_64

yum versionlock list 
  
yum --exclude=kernel*,redhat-release* groupinstall 'Infrastructure Server' 

yum grouplist  

   # you should see "Infrastructure Server" under "Installed Groups"

yum --exclude=kernel*,redhat-release* --security update-minimal

  # the only security update at this point should be "sudo"

# check your default kernel

awk -F\' '$1=="menuentry " {print $2}' /etc/grub2.cfg

```

\
\
\

### Install ALL InfiniBand RPMS (in case nsdperf needs to be compiled later) 

```
# traverse into directory of Mellanox rpms

cd /root/MLNX_OFED_LINUX-4.7-3.2.9.0-rhel7.6-x86_64/RPMS/MLNX_LIBS/

# install an needed utility

yum install createrepo

# make sure you are really THERE
pwd 

createrepo .

yum install *   

  # this will take 5-10 mins 

# restart server's ib adapter & drivers

/etc/init.d/openibd restart

# check if your IB port connections are up

ibdev2netdev

```

\
\
\

### Setup your IB Bond Connection

```
# show your current connection profiles

nmcli c 

  # you'll compare output to upcoming steps

# bring down old puppies 

ifdown ib0

ifdown ib1

# remove default connection profiles that rely on DHCP boot protocols 

rm /etc/sysconfig/network-scripts/ifcfg-ib*

  # confirm "yes~" 

nmcli c reload

nmcli c

  # now you should see them missing

# do a double-take; check if your ib devices (not connection profiles) are up

ibdev2netdev

# CREATE BOND MASTER n' SLAVES
---------------------------------------------------
nmcli c add type bond ifname bond0 mode active-backup ip4 10.100.164.27/19

  # KEEP THE I.P. AS LISTED ABOVE!!! (afm03 is ##.###.164.26)
 
nmcli c
  
  # you should see it


# SLAVES

nmcli c add type bond-slave ifname ib0 master bond0
nmcli c add type bond-slave ifname ib1 master bond0

nmcli c

# Edit ONLY THE SLAVES' ifcfg-files

vi /etc/sysconfig/network-scripts/ifcfg-bond-slave-ib0
----------------------------------
# CHANGE/ADD the FOLLOWING:

TYPE=Infiniband
NM_CONTROLLED=yes
----------------------------------

# repeat:

vi /etc/sysconfig/network-scripts/ifcfg-bond-slave-ib1


# reload config files for Network Manager

nmcli c reload


# bring connections up

ifup bond-slave-ib0
ifup bond-slave-ib1
ifup bond0

nmcli c  
 
 # they should be highlighted in darkgreen "active"

# ping afm03

ping 10.100.164.26

# modify bond0's MTU to 4092

nmcli c modify bond-bond0 802-3-ethernet.mtu 4092
nmcli c modify bond-slave-ib0 infiniband.mtu 4092
nmcli c modify bond-slave-ib1 infiniband.mtu 4092
nmcli d modify ib0 infiniband.mtu 4092
nmcli d modify ib1 infiniband.mtu 4092


  # the only effective cmd is really the first one---as discussed before, the slaves won't alter to 4092, only the bond 

nmcli c reload

# bring down bond0 and then back up (slaves will follow automatically) 

ifdown bond0

ifup bond0 

nmcli c --> are the slaves back up?

# verify Bond's MTU

nmcli c show bond-bond0 | grep -i mtu

  # 4092? If not, call "1800-WHERE-IS-MY-MTUS"

# VERIFY "datagram" mode for slaves

cat /sys/class/net/ib0/mode
cat /sys/class/net/ib1/mode

# check bond0 again

ifconfig bond0
---------------------------------------------------

```

\
\
\


### Time-Sync with BioHPC

```
# once again, for RHEL release compatibility's sake

yum downgrade ntpdate-4.2.6p5-28.el7.x86_64 

yum install ntp

systemctl status ntpd 
systemctl enable ntpd

  # make sure it's off for now 

# Add headnode as NTP Server

mv /etc/ntp.conf /etc/ntp.conf.bak

vi /etc/ntp.conf
----------------------------
driftfile /var/lib/ntp/drift
disable auth
restrict 127.0.0.1
server 172.18.224.1 iburst
----------------------------

vi /etc/ntp/step-tickers
----------------------------
172.18.224.1
----------------------------

# check server's current time (EST Time?)

date  

# do a sync & update time-zone

ntpdate 172.18.224.1

timedatectl set-timezone America/Chicago

date 

timedatectl set-ntp true

# verify that NTP is enabled
timedatectl status 

systemctl restart ntpd

```

\
\
\

### Touch-Ups

```
# disable RHEl bug-reporting daemon

systemctl disble abrtd
systemctl stop abrtd

# IMPORTANT: put head node as DNS server! Or you'll lose connection to RHEL Satellite servers

vi /etc/resolv.cnf
-------------------------------
search ib1.biohpc cm.cluster ib.biohpc ipmi.biohpc biohpc.swmed.edu
nameserver 10.100.160.1
-------------------------------

# setup proxy env variables

vi /etc/profile.d/proxy.sh
-------------------------------
export http_proxy=http://proxy.swmed.edu:3128
export https_proxy=http://proxy.swmed.edu:3128
-------------------------------

source /etc/profile.d/proxy.sh

vi /etc/yum.conf
-------------------------------
proxy=http://proxy.swmed.edu:3128
-------------------------------

systemctl restart network

# do one more security update

yum --exclude=kernel*,redhat-release*  --disablerepo=* --enablerepo=rhel-x86_64-server-7 --security update-minimal
```

## WebX w/h Isaiah Eaton & Venkat

###

```
# clarify which nodes are gateways

mmlscluster

mmfsadm dump afm

mmfsadm dump waiters

  # if a lot of waiters, indicate too much filesystem changes

mmafmctl bassdata getstate

# check status of filesets

mmafmctl bassdata prefetch -j backup

# if everything is 0, could indicate issue

# execute on each afm node to stats by number--changes live very quickly

echo afm_s | mmpmon


###


# check existig snapshots

mmlssnapshots bassdata

mmlssnapshots data

  # SNAPSHOTS are dangerous to keep long-term; as they continuously log changes to filesystem!!!

e.g. this snapshot was created 3 years prior to date of inquiry, and has accumulated 1T of
data---size is not an issue, but it has affected the performance & workload of the servers
---------------------------------------------------
Directory                SnapId    Status  Created                   Fileset
snap1                    1         Valid   Thu Mar  2 16:56:46 2017  
---------------------------------------------------

# delete snapshot

  # STOP AFM migration first!

nohup mmdelsnapshot <filesystem name> <directory name> 

e.g. nohup mmdelsnapshot data snap1

CTRL + Z to exit

cat nohup.out --> see process!

  # once finish, to final prefetch 

  # ps -ef | grep tspcache  --> kill all prefetch before deleting snapshots

# check uploads  --> if too many nodes uploading will prevent system from deleting 
mmlsnode -L <CHECK!> 



### INFO

Snapshot, once created will CONTINUOUSLY reflect changes to Live Filesystem!!!

  # snap1 on OLD cluster could be taking up .9 Pebibytes!

```

### Follow-Up WebX on 03/24/2020

```

# filesystem sub-blocks are a feature of GPFS 5+ (old CUH is v.4.2 which cannot)

# afmctl error code 6 --> filesystem unavailable

# do dry-run RSYNC to compare filelistings between CUH and BASS

# check sub-blocks:

--------------------------
mmlsfs data/bassdata

# CUH: 32k & 128K

# Bass: 
--------------------------

# fixing error code 6

mmchfileset bassdata archive/backup/work -p afmParallelReadThreshold=disable

  # disable other gateways from killing migration process when cannot talk to remote gateway node

# start trace on afm01
mmtracectl --start --trace-file-size=2G --tracedev-write-mode=blocking -N afm01-hs

 # mmtrace --off -N afm01-hs

# increase innode:

mmchfileset bassdata archive/backup/work --inode-limit 900M

### NOTE ###

  # from mmlsfileset -L output:
  
-"AllocInodes" --> # of files in each fileset; BUT not most updated, as deleted files on the existing cluster will still shown to occupy a innodes.

- to see the most detailed info, it is better to use `mmlsfileset <filesystem> <fileset> -i` to compare between the two filesystems

```

### 03-30-2020

```
# efix ?

# stop and collect trace
  
  # trace need to be collected/stopped as soon as the prefetch finishes 

mmtracectl --stop -N afm01-hs

# look at trace files

zless trcrpt.###.gz file

# Venkat writing a script to pull trace as soon as AFM stops
----------------------------------------------------   
## vi tspcache.sh @ afm01

#!/bin/bash

while true
do 
pgrep -x tspcache > /dev/null 2>&1
if [[ $? -ne 0 ]]
then
mmtracetl --stop -N afm01-hs
break 
if
fi
sleep 2
done


mmtracectl --start -N afm01-hs
mmtrace

  
## location of script `/tmp/mmfs/<script file>` 

# debug
nobup sh afmtrace.sh

bg

fg 

## renamed file to not be confused with tspcache prefetch

mv tspcache.sh afmtrace.sh

chmod 700 afmtrace.sh

nohup sh afmtrace.sh

bg 

# check if nobup process exists
ps -ef | grep afmtra --> should collect trace as soon as migration stops
----------------------------------------------------   

#

```

### 03-31-2020

```
# changed afmtrace.sh

 # when test run: `sh afmtrace.sh` --> cat afmtrace.log

```


### 04-06-2020

```
# archive migration has stopped trace file created with new script

in `/tmp/mmfs`

# tried to grep for terms and pass them to a files (so far nothing yet)

zgrep "err 6" <trace file> > err6

zgrep "rc 6" <trace file> > rc6

zgrep PCACHE <trace file> > pcache

# Or we can just read the trace file directly

\

ls -id /endosome/archive 

# 524291 

tspcacheutil bassdata archive -x 552778

tsfindinode -i 552778 /endosome/archive

# suspect from nohup out that the prefetch had failed due to memory issue/run out

  # recommend submitting ticket with nohup.out

cd /tmp/mmfs/prefetch 

mv nohup.out crash.txt

# restarted prefetch with 36 threads & got rid of `--split-dirs` handle 

nohup mmafmctl bassdata prefetch -j prefetch -j archive --directory /endosome/archive --prefetch-threads 36

```

### 04-28-2020

- tracing status can be found via `mmlsconfig` (afm01 was in "blocking mode" for tracing)

- STOP mmtrace --> don't STOP, but OFF --> affects performances perhaps 

- #ems1: `mmtracectl --off -N all`

- deleted trace files under /tmp/mmfs 

- **info on prefetch threads**

  - # of threads reading the home 

- `mmfsadm dump afm`

  - under "listed by execution order" --> innode number

  - `mmfsadm dump afm fset archive` --> show fileset specific info

- `mmfsadm dump waiters`  --> shows waiters (how to clean them?) 

- `tspcacheutil bassdata archive -x <innode #> --> give info about particular innode being processed

\
\

- `mmchfileset bassdata archive -p afmNumFlushThreads=64` --> increase # of "FlushThreads" 

  - [documentation](https://www.ibm.com/support/knowledgecenter/en/STXKQY_5.0.4/com.ibm.spectrum.scale.v5r04.doc/bl1adm_mmchfileset.htm)

\
\

- `mmlscluster` see cluster

- `dstat -tcmsn -N bond0` --> see transfer rate

- start trace for a node

  - `mmtracectl --start -N afm01-hs` --> `mmfsadm dump all > /tmp/all` 
 
  - `--off` to stop again 

  - start trace in "blocking mode" --> ` mmtracectl  --start --tracedev-write-mode=blocking --trace-file-size=1G -N afm01-hs` --> `mmfsadm dump afm fset archive` --> `mmtracectl --off -N afm01-hs`

  - `/tmp/mmfs/trcrpt-YYYY-MM-DD_HH.MM.SS` --> trace file name 

- under `mmfsadm dump all > /tmp/all` --> grep for `io list` --> grep for `pagepool` (dictates memory usage) 

- see memory threshold? --> `mmfsadm dump config | grep maxFile` 

\
\

- if we change "ro" to "local update" mode, then clients can change the filesystem during migration

  - applications that need missing files will look into the old filesystem

\
\

- see afm config: `mmfsadm dump config | grep <attribute>` 

- increased memory threshold 5G --> 20G 

  - #afm01 `mmchconfig afmHardMemThreshold=20G -i`

  - [documentation](https://www.ibm.com/support/knowledgecenter/en/STXKQY_5.0.4/com.ibm.spectrum.scale.v5r04.doc/bl1adm_mmchconfig.htm) 

\
\

- **Summary**

  - stopped trace

  - increased hardmemthreshold

  - increase flushthreads

  - need to stop tracing on CUH


- **QUESTIONS** 

  - read Isaiah's email on afm modes

  - of we cut over soon---how would the quota work?

    - we will have to create implement the quota once more 

  - what does blocking mode mean in mmtracectl? 

  - what does afmNUMFlushThreads mean? 

- [SC019 AFM presentation](https://www.spectrumscaleug.org/wp-content/uploads/2019/12/SC19-Spectrum-Scale-Data-Migration-with-AFM-Nuance.pdf)

- [DDN AFM Case Study 2017](http://files.gpfsug.org/presentations/2017/Manchester/07_Spectrum_Scale_UserMeeting_2017_16x9.pdf) 

\
\

### 04-30-2020

- cache state: dirty

  - mmdiag --version  --> GPFS version is minor version on AFM than on GPFS cluster
 

- the pasue could be due to queuing changes

  - afm needs to scan the files first 

- workerThreads may have been tuned --> previously 128

  - increase

  - difference between workerThreads (gpfs filesystem) and flushThreads (afm read)
```
mmchconfig workerThreads=1024 -N afm01-hs,afm02-hs

mmfsadm dump config | grep -i prefetch

mmchconfig prefetchPct=40 -N afm01-hs,afm02-hs

mmfsadm dump config | grep -i pagepool

mmchconfig pagepool=40G -N afm01-hs,afm02-hs

mmchconfig afmHardMemThreshold=50G -i

mmfsadm dump config | grep maxFiles

```

- restart

```
mmafmctl bassdata stop -j archive

mmafmctl []

mmafmctl bassdata start -j archive 

mmlsfileset bassdata archive

  # check link status

  # traverse into /endosome

cd MIL

mmgetacl -k nfs4 chen

mmlsattr -L -d chen

mmgetacl -k nfs4 <filename> --> check ACLs (gpfs)

# CHANGE
mmchfileset bassdata archive afmForceCtimeChange=no

# check afm attributes i.e. threads  
mmlsfileset bassdata archive --afm -L

# similar 

mmfsadm dump mb | less

# started prefetch again with 32 prefetch threads (for archive) 

# stoped and restarted work and backup too

mmafmctl bassdata stop -j work

 # need to CTRL + C ? 

kill prefetch processes

mmshutdown

mmstartup

mmgetstate

# prefetch for workcould not start, work unmounted --> mmafmctk bassdata stop/start -j work

``` 
\
\

#### GPFS AFM DEBUG June 8th:

```
mmchconfig afmCheckRefreshDisable=no -i

# check afm attributes (ems01)
mmlsfileset bassdata work -L —afm

# increased look up intervals by 10x (from attributes listed above)

`time git status` 

# 5.0.4.4 (lookup in old fs doesn’t happen in earlier versions)

# AFM, NFS, SAMBA, GIT nodes upgrade, 005

#  9:30 AM meeting
 
- delete **.ptrash** and quota doubles will decrease (parallely)

```

#### GPFS AFM DEBUG June 9th:

```
# decided that gpfs 5.0.4.4 will be the correct patch

# disabled refresh interval 
mmchfileset bassdata work -p afmDirLookupRefreshInterval=disable 
mmchfileset bassdata archive -p afmDirLookupRefreshInterval=disable 
mmchfileset bassdata backup -p afmDirLookupRefreshInterval=disable 

> Defines the frequency of revalidation that is triggered by a look-up operation such as ls or stat on a directory from the cache cluster. AFM sends a message to the home cluster to find out whether the metadata of that directory is modified since it was last revalidated. If so, the latest metadata information at the home cluster is reflected on the cache cluster.

# debug migration once again
/var/adm/ras/mmfs.log.latest

# new prefetch run options?

`--readdir-only` added to prefetch run

  # gets/populated all meta data and fetches for populate namespace properly

  # looks for uncached files, then we can run separate commmand to fetch what's missing to finish


# debugging prefetch, whether mmfsd has established connections to nodes

lsof > /tmp/losf.out

grep 'ESTABLISHED' /tmp/lsof.out

mmfsadm dump cfgmgr > /tmp/cfgmgr.out

# version upgrade

install GPFS 5.0.4.4, then install efix packages
 
```

\
\

### AFM Debug 6-24-2020

```
# differences betwen CUH and Bass ~ 70T more on CUH for certain labs on archive, majorly Danuser_lab and Nick_lab

# afm read-dir run complete, does not show files missing

## Venkat suggests runing "re-build prefetch" with "uncached" option to check what's missing; or use policy engine to compare filelists for archive fro Danuser_lab and Nick_lab (runs directly on the disk, faster than find command) 
.

# performance compare: run dd on both filesystems

# backup issue w/h rsnapshot (creates lots of hardlinks to save space)

  ## we had already prefetched the backups to Bass without hardlinks, needed `--preserve-hardlinks` handle, which slows down prefetch performance a bit

  ## without hardlinks, files takeup different innodes, which could have attributed to increasing the usage/quota

  ##  Venkat suggests: running policy on CUH to find hardlinks, and their duplicates, on Bass we could find duplicate files and keep only the hardlinks

.

# Cut-over Planning

  # Venkat suggests: enable AFM DR vs. single writer -- the only way, we can't convert cache to single writer mode for AFM

    ## single writer cannot convert non-afm filesets to AFM? 

    ## OPEN-discussion for IBM: we don't want to commit yet due to not wanting instantaneous deletes across the home and cache directories; NOTE it turns out for all afm modes the deletion is instantaneous; Venkat recommends snapshots being kept using DR mode
  

```


\
\

### IBM Meeting 7-7-2020

```
# AFMDR without RPO/RTO similar to AFM SW

```


\
\

### 7-22-2020

- Topic: policy file scan & prefetch

- Overview: 

  - ems1 running policy engine script to check un-prefetched files

  - `nohup mmapplypolicy /endosome/archive/ -I defer -P policy_venkat_2.txt -f /tmp/policy/policy_venkat_archive_7_22_2020_2`

  - afm02 metadata prefetch dirlistprefetch running for backup & work to cache un-prefetched (running too long--WILL CANCEL, not interdependent with policy scripts)

    - original dirlistprefetch cmd: `

- `policy_venkat[1-4].txt` --> policy file revalidates files b/w old and new fs; list files un-validated & list them

  - currently running ...4.txt (presumably first round)

  - another policy file (FIND OUT WHERE) --> listes un-prefetched files

  - run prefetch for each produced list 



\
\

- Topic: S005 & git slow-down

- Full debug:

  - `mmfsadm dump all > /tmp/all`

  - `mmdumpkthreads`

  - `mmtracectl --start -N <node>` --> wait for 60 secs

  - `mmtracectl --stop -N <node>`

  - `mmlsnode -L -N waiters > /tmp/waiters`


- NFS debug:

  - grep for 'hung' in /var/log/messages --> leads to hung_task_timeout_secs


\
\

### 7-30-2020

- Overview: 

```
validation and recache policy files making headway for archive, but work has trouble starting the prefetch

```

- `/tmp/reval_archive_7_30_2020.list.ctimeFiles` --> one of the more current policy output files

  - `echo file | sed 's/%F/\//g'` to escape all of the '2%' delimiters

- `tspcacheutil <filename>` --> check caching status of the file

  - NOTE: most of the recent files on policy output are shown to be already cached on endosome's side; need to double verify via `tspcacheutil`

- `gdb attach 450354` --> check process status

  - `strace`

- `tspcacheutil bassdata work -x <innode #>` --> check cached state of files under /endosome/work fileset 

  - `mmfsadm dump afm` --> check currently innodes, copy as argument to cmd above

- `mmlsfileset bassdata work --afm -L` --> check afm status of work fileset 

  - shows you the afm mode + its linked "Target",etc

- `mmchfileset bassdata work -p afmforcectimechange=no` --> if set to yes bc needing to transfer ACLs, this will imppact performance

- `mmchfileset bassdata work -p afmfilelookuprefreshinterval=disable`

- `mmchfileset bassdata work -p afmfileopenrefreshinterval=disable`

- `mmchfileset bassdata work -p afmdiropenrefreshinterval=disable`

  - all this is to ease the I/O load & increase performance!!!

  - `dstat -tcmsn -N bond0` confirms throughput improved

  - restarted prefetch

- for prefetch `--prefetch-threads` the max threads allowed is 255

/
/

### 08-13-2020

- nnafmctl outpu --> if "Queued" is "0", then either all files have been cached or no progress

- status

  - `/endosome/.backup/policy/uncache_policy_work.list.uncachedFiles_2`

  - ~1000 files returned to be uncached by policy

  - those are NOT cached via `tspcacheutil`

  - `md5sum <filename>`

- test: select a uncached file, parse it via sed and place in list file --> try prefetch on it (afm01)

  - using `prefetch` failed; `mmafmctl bassdata prefetch -j work --gateway afm01-hs` --> to CHECK status of last run

  - seems the file belongs to user who has exceeded quota--> after adjusting, the issue was resolved 

  - ACTION: check for remaining ~1000 files 

- another issue: 

  - cannot ls files in a CAND sub-dir (too many files, non-issues)

  - start `mmtracectl` on afm01 & briefly turned off to collect trace

  - `mmlsconfig` to check if trace is disabled

/
/

### 09-03-2020

- the remaining 30 uncached files have sizes larger than actual size

  - reserving block before actual datablock is written to them ---> "sparse files"

  - `truncate --size 10M /tmp/example_file_size`

- note: metadata is 1MB on Bass

- Venkat recommends ignoring those files

- .ptrash files need removal


/
/


- question on network outtage

  - collect snap for IBM to investigate

## GPFS Rolling Upgrade & AFM Installation Day 5

> Resumed on 02/24/2020

### Operations

- Drained Nucleuc047 due to low network speed 

- Negotiate TACC Nodes

### QUESTIONS 

- Client HCA Version:

- Client OFED Version:

### INFO

- firmware update failure on gssio1, may have run update on FORCE mode

- spectrumScale doesn't have GNR, ESS has GNR (embedded RAID), that's the only difference between the two products 

- mmdelnode --> delete node on cluster

- mmstartup --> the nodes won't join cluster until this

- **nsdperf** --> if run on I/O --> `-s` needed, if run on clients --> no `-s`

- ems provisioning script: `/var/tmp/..`

- in order to delete node from cluster, delete from different client node!

- to be able to join two GPFS cluster, do remote cluster mounts

- remove sudo wrapper, root still has access to `/usr/lpp/mmfs/bin/mmchcluster`

    - `--nouse-sudo-wrapper` --> can only be used if node belongs to cluster

- client clusters can be created from the client nodes --> need to define quorum with +1 nodes total

  - `mmcrcluster` --> need node list txt file with node and quorum designation

  - `mmcrcluster -c <cluster name> -N <node list file>` 

  - `mmchlicense` needed to make quorum nodes "servers" --> check with `mmlslicense` 

  - `mmchcluster -C <new cluster name>` --> change GPFS cluster name

  - **check history** on NucleusA010  

- `mmchmgr data gssio3-ib` --> filesystem manager changed to I/O3

- `mmchmgr -c gssio4-ib1 --> cluster manager change to I/O4 

### Remote Cluster Mount

```
- ensure /etc/hosts from ems (of both gpfs cluster) is added to client cluster files 

- no need to disable sudo wrapper (if disabled, doesn't take away the comands)  

```

### Network Check:

- **check command on "ems1"**

  `xdsh ems1, gss_ppc64 /usr/lpp/mmfs/bin/mmnetverifiy`



### SEQUENCE: EMS1 next version

- `gssinstallcheck` on ems1 before update

- cd `/home/deploy/535` --> where gpfs cluster packages are (..datamanagement.tar.gz --> cluster code) 

- ems1 to need to be removed from the cluster prior to gpfs updates

- reboot then firmware upgrade


### Resources 

- [nsdperf](https://developer.ibm.com/storage/2017/07/03/network-performance-assessment-using-spectrum-scale-nsdperf-tool/)

- [gpfsperf_serial](https://www.ibm.com/developerworks/community/wikis/home?lang=en#!/wiki/General%20Parallel%20File%20System%20(GPFS)/page/Testing%20File%20System%20Performance)

### Restart in case of I/O node crash

-  mmshutdown (gpfs cluster only)

- mmstartup (gpfs ~ ~ )

- mmmount -A all clients


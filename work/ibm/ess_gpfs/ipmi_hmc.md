## 

###

10.0.0.11 --> has to start a VNC server on ems01 to connect

- fsp network, not XCAT

  - we need to connect a x86_64 node into this network and install ipmitools

- u: admin

- p: abc123

- password change?

\

- IPMI cluster nodes

  - `ipmitool -I lanplus -H 10.0.0.11 -P PASSW0RD power status` 

  - only reachable via XCat; how do we reach outside of the cluster  

  - open session into 

  - `ipmitool -I lanplus -H 10.0.0.11 -P PASSW0RD psol deactivate/activate` 

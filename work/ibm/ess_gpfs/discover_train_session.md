## 

> Contact: Isom Crawford Jr.
>

### RestAPI 

- Isom writing script


### Questions & Observation

- Can recognize invidual samba shares

- "Metadata" --> "Tags" --> How to create Buckets with multiple attributes?

  - e.g. "SizeRange" = Bucket --> small,medium, and large

- SQL Query List? --> unix based and tape metadata attributes default to each connection, e.g. `path` & `atime`  

- Tag Types?

  - Open -> empty 

  - Restricted -> can limit values to a certain set of values

  - Bucket - cannot be created

- Collections under "Add new policy"? --> DO NOT SELECT!

- [A report can be created via JSON query](2.0.3/com.ibm.spectrum.discover.v2r03.doc/api_rstapi_epts_db2-report_runnew_r.html)

  - [https://www.ibm.com/support/knowledgecenter/SSY8AC_]

#### "Admin" --> "Discover Database"

- Metadata Summarize

  - main filesystem attribute, will give total connection SIZE

- Application catalog Cache

  - specialized?

- Duplicate Record Database

  - checks duplicate files based on name and size

#### "Node List" under "Connections" --> "Edit"

- better to use I/O, ems nodes, client nodes may not be that advanced

- the mmapplypolicy cmd is issued via SSH to the nodelists when scans are made 

#### "Admin" --> "Discover Database" ?

## AFM Setup Day 11

###

```
# delete filesets

mmdelfileset bassdata work 

# get AFM state on fileset
mmafmctl bassdata getstate -j work

  # this shows amount queued

# show cluster licenses

/usr/lpp/mmfs/bin/mmlicense -L

# see cluster configuration 

/usr/lpp/mmfs/bin/mmlsconfig

```
### AFM Cont'd

```
# check status ON THE AFM nodes--shows status

mmfsadm dump afm 

# perform commands from all nodes in "afm_bass" node class 

   # use `mmlsnodeclass` to see all classes
----------------------------------------------------------------
mmlsnodeclass 
Node Class Name       Members
--------------------- -----------------------------------------------------------
gss_ppc64             gssio2-hs,gssio4-hs,gssio1-hs,gssio3-hs
ems                   ems1-hs
gssio3_hsgssio4_hs    gssio4-hs,gssio3-hs
gssio1_hsgssio2_hs    gssio2-hs,gssio1-hs
GUI_MGMT_SERVERS      ems1-hs
GUI_SERVERS           ems1-hs,gssio2-hs,gssio4-hs,gssio1-hs,gssio3-hs,afm01-hs
                      afm02-hs
GUI_RG_SERVERS        gssio2-hs,gssio4-hs,gssio1-hs,gssio3-hs
CALLHOME_SERVERS      ems1-hs
afm_bass              afm01-hs,afm02-hs
NucleusNods           NucleusA109.ib1.biohpc
----------------------------------------------------------------

  # NOW USE `mmdsh`:
----------------------------------------------------------------
  mmdsh -N afm_bass '<commands>' 

 # " " also works

 # e.g. check afm migration process on all afm nodes

mmdsh -N afm_bass 'ps -ef | grep -i tspcache' 
----------------------------------------------------------------

\
\

### BREAK THROUGH: selectively prefetching subdirectories under work without creating individual filesets for each directories is now possible!


#### Sequence of Steps

```

 ## starting to create fileset to start pre-fetch

--> specified locations: `/endosome/work/<sub-directory>`


1. check number of inodes fork /work fileset

  --> GUI --> filesets --> "root" --> vew details --> 150Milion

2.  create fileset then assign inode

3. link fileset 

4. go to afm node to touch

 getstate

5. prefetch but now with  `--dir-list-file` handle

## VERIFY:

- before nohup start, output to a different file (check) 

- check creation name of fileset make sure it matches, and the linked location is the SAME as the ones listed in list.file

```

### Post AFM Migration

```
# donwtime: 

- umount all client nodes

mmumount all -N client.list/NucleusNodes

- do nohup pre-fetch once more on afm

- unlink filesets on Bass cluster

- umount CUH from Bass

- put all clients to their own cluster

- disable AFM targets; set attribute for fs w/o afm

mmchfileset bassdata work/backup/archive -p afmTargets=disabled afmMode=iw (independent writer) or local update "lu")

- link filesets aggain

mmlinkfileset bassdata archive -J /endosome/archive

-Remote mount clients on Bass

- Upgrade CUH to v5

- Remote mount Bass from CUH

- create clinical fileset & encrypt

# ACTIONS:

- Keep NucleusS003,004 directly attached to CUH/Bass Cluster

```

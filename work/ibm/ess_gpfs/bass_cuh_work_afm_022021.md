## Setting Up AFM for work Fileset: Bass to CUH

### 02-01-2021

#### Remove old AFMDR work fileset from CUH

```
mmlsfileset cuhdata work

mmlsfileset cuhdata work -f

mmcrfileset cuhdata work -p afmTarget=gpfs:///endosome/work -p afmMode=ro --inode-space new --inode-limit 943718400 

  ## ~ 1 billion 

mmlinkfileset cuhdata work -J /exosome/work

mmlsfileset cuhdata work
```

### Start Pre-fetch

```
## confirm/define gateways on CUH
mmafmctl cuhdata getgateway -j work

  # if wants to change, then work fileset needs to be first unlinked

## assign 45G of pagepool to gateways

mmchconfig pagepool=45G -i N afm03-ib,afm04-ib

  # use `mmdiag --config | grep -i pagepool` to confirm on nodes

## start afm process

  ## check configs before starting: `mmafmctl cuhdata getstate -j work`

mmafmctl cuhdata start -j work

  ## needs to TOUCH the filesystem for afm to start

ls -l /exosome/work 

  # should see filesystem structure already cached on /exosome/work

nohup mmafmctl cuhdata prefetch -j work --directory /exosome/work --prefetch-threads 36 > work_prefetch.out

yum install dstat

dstat -tcmsn -N bond0

  ## current status; low transfer rate
```

\
\

### Debug with Venkat

- error:

  - `Input/output error` --> solved by upgraded release of Scale

  - generally caused by a bug in AFM code, where worker node prematurely stops reading as demanded by master afm node

  - master afm node will communicate with worker node regardless of whether it's partaking in the prefetch or not


\
\

### Further Debug with Venkat

- updated AFM nodes to newest version: `Current GPFS build: "5.0.5.5 ".` 

  - crashed tspcache with error 06 during running

- general health checks

```
grep -i 'afm' /var/adm/ras/mmfs.log.latest

grep -i 'cuhdata' /var/adm/ras/mmfs.log.latest

mmfsadm dump config | grep afm 

  ## list AFM settings

mmfsadm dump config | grep workerThread 

mmfsadm dump config | grep prefetchThreads 

mmfsadm dump config | grep -i maxfiles

mmfsadm dump config | grep numaMemoryInterleave 

   # should be YES, not no

mmfsadm dump config | grep ignorePrefetchLUNCount

   # should be YES, not no

---------------------------------
numaMemoryInterleave
    In a Linux NUMA environment, the default memory policy is to allocate memory from the local NUMA node of the CPU from which the allocation request was made. This parameter is used to change to an interleave memory policy for GPFS by starting GPFS with numactl --interleave=all.

    Valid values are yes and no. The default is no. 

     
ignorePrefetchLUNCount
    The GPFS client node calculates the number of sequential access prefetch and write-behind threads to run concurrently for each file system by using the count of the number of LUNs in the file system and the value of maxMBpS. However, if the LUNs being used are composed of multiple physical disks, this calculation can underestimate the amount of IO that can be done concurrently.

    Setting the value of the ignorePrefetchLUNCount parameter to yes does not include the LUN count and uses the maxMBpS value to dynamically determine the number of threads to schedule the prefetchThreads value.
---------------------------------

```

- **INCREASED worker and prefetch Threads**

  - this requires mmfsdaemon restart!!!

  - `ps -aux | grep -i tspcache` --> `kill -9 <PID>`

  - `mmchconfig workerThreads=512 -N afm03-ib,afm04-ib`
 
  - `mmchconfig prefetchThreads=128 -N afm03-ib,afm04-ib`

  - `mmchconfig prefetchTimeout=30 -N afm03-ib, afm04-ib`

  - `mmchconfig prefetchPct=25 -N afm03-ib,afm04-ib`

  - `mmchconfig numaMemoryInterleave=yes -N afm03-ib,afm04-ib`

  - `mmchconfig ignorePrefetchLUNCount=yes -N afm03-ib,afm04-ib`

  - `mmshudown -N afm03-ib,afm04-ib` --> `mmstartup ...`  --> `mmmount -N afm03-ib,afm04-ib` (mounts remote mounts)

  - finally restart prefetch 

- How to enable trace if specific errors keep occuring

  - `mmtracectl --start --tracedev-overwrite-buffer-size=1G --tracedev-write-mode=overwrite -N <gateway>` (how to stop?)

  - `mmtracectl --stop -N <gateway>`  # to stop 

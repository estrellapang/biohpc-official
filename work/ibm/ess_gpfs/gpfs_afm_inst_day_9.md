## Day 9 of AFM Installation

### Sequence:

- During Migration, new cluster will be "Cache" while the old cluster will be "Home"

- **on old cluster**

`mmafmconfig enable /work/*`  

- allocate certain amount of subdirectories to each afm node

- do pre-fetching

- start migration

### Actions:

- `mmfsd` taking up ~75% of memory on all nodes on GPFS clients

  - `mmlsconfig` --> see pagepools for different nodes on cluster
 
### Info

```
# ib_write_bw
ERROR:Unable to open file descriptor for socket connection Unable to init the socket connection 

  - [link](https://mymellanox.force.com/mellanoxcommunity/s/question/0D51T00006uGa1WSAS/error-in-ibsendbw-and-ibwritelat-using-mlnx-ofed-451010/)
  
  ## solution disable firewalld

# Pebi (PiB) vs. Peta (PB)

------------------------
df -h --> displays piB 
df -H --> displays PB
------------------------

# FETCH QUOTA for specific filesets--grep for size column

mmrepquota -u -v data:archive --block-size M | awk '{ print $4 }'


## ADD NEW NODES:

## add root pubkey from-to all nodes

## copy /etc/hosts files

scp /etc/hosts root@afm03-ib:/etc/hosts

scp /etc/hosts root@afm04-ib:/etc/hosts

mmaddnode -N afm03-ib,afm04-ib

mmchlicense client --accept -N afm03-ib,afm04-ib

mmchconfig verbsPorts="mlx5_0",verbsRdma="enable" -N afm03-ib,afm04-ib

mmstartup -N afm03-ib,afm04-ib

mmgetstate -N gss_ppc64,ems1-ib

\
\
\

### NSDPerf

## FQDN names should be different from hostnames

hostname --fqdn 

e.g. `afm02.gpfs.net` 

  ## location of testnode files (for GPFS) 

/usr/lpp/mmfs/samples/net/testnodes

### location of client cluster nodes -- add all nodes to there


### Create node class for nodes

mmlsnodeclass

mmchnodeclass <node_class> add -N <nodes,nodes>

mmcrnodeclass <nodeclass_name>  -N <nodename>

  ## change nodeclass config

mmchconfig pagepool=1G -N <nodeclass_name>

  ## could add `-i` option to make changes instantenous
```

### NEW GPFS Cluster as New Client Cluster for CUH

```
# copy host file from ems to rest of the cluster(both sides)


xdcp gss_ppc64 /etc/hosts /etc/hosts

  # host file has been updated with IB entries ONLY for the new GPFS cluster

  # include the afm nodes as well

# copy pub from old to new

scp /var/mmfs/ssl/id_rsa.pub root@ems1-hs:/root/cuh_key 

# also do vice versa new to old

~/root/bass_key

# do auth with newly received key on old cluster  

mmauth add <new cluster name> -k id_rsa.pub

  # if hanging then add root's pub keys and then hit ENTER

# do grant

mmauth grant <cluster name> -f data

### on client cluster side

mmremotecluster add <old cluster side> -n <nodenames>-ib,...,..., -k id_rsa.pub

  # id_rsa.pub from sub-directory /cuh_keys

# in order to not cause conflicts, we changed local filesystem name from "data" to "bassdata"
  # first need to umount original "data" before renaming it `mmumount data -a`

mmchfs data -W bassdata

# add old clusters filesystem to the local filesystem, a pseudo "data" filesystem 

mmremotefs add data -f data -C biohpc-ess.gssio1-ib -T /Work

  # also the original filesystem "bassdata"'s mount point is "/work" therefore the remote mount has to be different, hence the capital "/Work"

# now this will mount /Work (remount client mount)
mmount data -a 

```

### Quota

```
# policy file: /work/quota_example

# implement default quota via CLI

mmdefaultquotaon -g bassdata

  # this does it for groups

# another way:

mmsetquota -F <stanza file> 
  
  ## policy file: /work/quota_example..

```

### IB Debug CMDs

```
  447  ib_write_bw 10.100.164.27
 456  man ibdiagpath
  457  ibswitches
  458  man ibswitches
  459  ibv_devinfo
  460  ibtracert 419 386
  461  ibqueryerrors

```

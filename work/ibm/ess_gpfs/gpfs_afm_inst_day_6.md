## Day 6 in AFM Installation--gssio2,3 Upgrade to 5.3.5

### INFO

- NucleusA209: drained to test 4.2 client mounting new cluster

  - need to rebuild filesystem with compat mode (see bullets below) 

- encryption --> create fileset, create pool for fileset, then set policy for pool for encryption

  - for migration, receiving end has to be a fileset created for AFM

- performance testing: write speed could be hampered by write-cache: direct-write would improve

- "backup" fileset is mounted on `/work/.backup`

- Block-size consideration:

  - avg. file size: ~400MB 

- client migration to new system

  - client HCA firmware
 
  - client gpfs version (current: 4.2. --> target: 5.0.4) 

- `mmchfs -V compat` execute on new gpfs cluster to make it backward compatible with older clients

  - `create filesystem compat with older release` --> `then mmchfs -V compat` to make it forward compatible

- **clear GUI error messages**

- client cluster (remote mounting) 

  - if they do not umount --> GPFS cluster CANNOT delete file system! 

- `mmlsnsd`  --> list all nsds

- `mmvdisk vdiskset list` --> list all vdisks

- `mmlsvdisk` --> see vdisks

- `mmumount data -N NucleusA209` 

  - `mmshutdown -N ~` 

- `gssaddnode -N <nodename> --cluster-node gssio1-hs --accept-license`

  - `cluster-node` --> cluster manager

- `xdcp gss_ppc64 /etc/hosts /etc/hosts` --> sync files across all gpfs nodes in cluster 

- `mmlscluster` --> see nodes in cluster
 

```
# check gui status 
systemctl status gpfsgui 

/usr/lpp/mmfs/gui/cli/runtask list GPFS_JOBS --debug 

  ## just `list` alone gives the "task" options

```

- `xdsh` --> perform parallel tasks on all gpfs nodes

```
# e.g: show verbose HCA info on all nodes

-------------------------- 
xdsh ems1,gss_ppc64 "ibdev2netdev -v" 
  
## will run across all members of `gss_ppcc64` 

xdsh ems1,gss_ppc64 'nmcli c show bond-bond0 | grep -i mtu'

xdsh ems1,gss_ppc64 'nmcli c modify bond-bond0 802-3-ethernet.mtu 2044'

xdsh ems1,gss_ppc64 'systemctl restart network'

-------------------------- 

```

- `mmdsh -N` --> perform parallel tasks all client nodes

```
# e.g.:
mmdsh -N all "ibdev2netdev -v" 

```

- `pdsh` 

- show node health: `mmhealth node show -a`  

### UPDATE SEQUENCE

- update node, reboot, update node, firmware update, reboot

### QUOTA

```
## output qouta policy
mmlsattr <stanza file> 

  ## current cluster doesn't use a stanza file


## LIST DEFAULT QUOTA
-----------------------------------------
mmlsquota -d --block-size T data   

---------------
[root@ems1 ~]# mmlsquota -d --block-size T data
         Default Block Limits(TB)                 |  Default File Limits
Filesystem Fileset    type      quota      limit  |     quota    limit  entryType
data       root       USR           5          7  |         0        0  default on
data       root       GRP           0          0  |         0        0  default off
data       archive    USR         150        150  |         0        0  default on
data       archive    GRP           5          7  |         0        0  default on
data       backup     USR         150        200  |         0        0  default on
data       backup     GRP         150        200  |         0        0  default on
---------------

  # where "data" is the filesystem
  # where "--block-size" can be "K", "T", "G"

# fileset quota can also be displayed 

mmlsquota -d --block-size T data:root 

mmlsquota -d --block-size T data:backup

mmlsquota -d --block-size T data:archive
-----------------------------------------
  
```

### Recreate FS:

```
mmumount data -a

mmgetstate -a

---------------------------------
# GUI

"files" --> "delete"
 
# Create

6 + 2p (RAID) --> "2 meta data rep policy 1 date policy" --> "Assign all arrays" 

Innode size --> "4k" --> "Estimated # of mounts = 1000" --> "

# add new nodes to `/etc/hosts`, make sure it's the same across all nodes on cluster

# add node: `mmaddenode <node name>` 


```

### Resources

- [spectrumscale quota cmds](https://www.ibm.com/support/knowledgecenter/STXKQY_5.0.2/com.ibm.spectrum.scale.v5r02.doc/bl1adm_liquot.htm)


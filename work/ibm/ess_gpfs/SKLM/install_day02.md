## SKLM Patch Install: Day 02

### 

- individual backups for each SKLM server

  - `P@55w0rd` 

- reset servers after backup

  -  `/opt/IBM/WebSphere/AppServer/bin/stopServer.sh server1`

  - use `wasadmin` pass

  - `cd /tmp: mkdir wasbackup`

  - `cd /opt/IBM/WebSphere/AppServer/configuration/` --> tar all files in the this directory

- patches are located in /root/sklm_install/<fp1/fp1>

- check the passwd hashes in each node's silent install XML files --> copy the hashes

  - place hashes into `SKLM_Silent_Update_Linux_Resp.xml`

  - check the install location, should be `../sklm/repository.config`

- perform update command:

- `./silent_updateSKLM.sh /opt/IBM/InstallationManager /opt/IBM/WebSphere/AppServer/ wasadmin <passwd>`

  - get on GUI to check version--> first version should be 4.0.0.1

- Repeat steps for second patch, whose packages are located in `/root/sklm_install/fp2/`

- Performed upgrades across SKLM0-4

  - create master backup 

  - Master backup password: root password with @ instead of dot 

- Password change --> login as sklmadmin and change pass - create backup!

- shutdown database, shutdown server01

\
\

###




\
\

###


\\

##

### 03-23-2021

#### Replication Port Issue

- port checks

```
sudo netstat -anp | grep '1111\|2222' 

  # not listening

  # but 1441 is listening

tracepath -p 1111 10.10.137.11

  # shows the port from sklm01 to sklm04 is reachable

```

- WEBGUI-check

- made new master replication setup, pass: "Cell@2013~"

```
# ON MASTER - "administration" --> "Replication"

 "Basic Properties"

# list all 3 other clones' IPs

# use port 2222

\
\

# ON Slave - "administration" --> "Clone" --> "Replication" --> "Start Replication Server" (clikc "OK" first)


\
\

# Backon Master - "Replicate Now"

  # fails for 10.10.137.10/11 network, but not the 172.18.239.54

  # date is off on those servers

\

# Fix attempt - installed ntpd on both sklm03,sklm04

  # replication hanging
```

\
\

### 04-20-2021: encryption setup

### Questions

- what is KIMP protocol?


\

#### Setup

```
# config dir location 

/opt/IBM/WebSphere/AppServer/products/sklm/config

../SKLMConfig.properties

# ports to use

  # 5696 --> KIMP port

\
\

# on CUH ems01

  # add all 4 SKLM04 servers to `/etc/hosts`

  # sync to all nodes on CUH GPFS cluster 

\

# add SKLM servers to ems node

mmkeyserv server add sklm01 --port 5696 # wrong PORT! see section below

  # enter sklmadmin password

# error: "Failed to check the SKLM configuration"

# check compliance standard 

mmlsconfig nistCompliance

  # looks normal

# enable debug

cd /opt/IBM/WebSphere/AppServer/products/sklm/config/SKLMConfig.properties

  # `debug = SEVERE` --> already enabled

# check debug from GUI

"Configuration" --> "Audit and Debug"  (already enabled)

"download logs" --> go to `/logs/logs/debug/sklm.log.0` to check newest logs


\
\

### SOLUTION: correct port

mmkeyserv server add sklm01 --port 9443

# verify

mmkeyserv server show

  # added all other sklm nodes as well

# did a manual replication on GUI
"Administration" --> "replication" --> "Replicate Now"

  # failed from 10.10.137.10 2222 (node is up)

  # on GUI: "KIMP failed to initialize" --> Restarted SKLM server, FIXED

  # repeated manual replication on SKLM01 after problem resolved

\

# deleted sklm02,03,04 

mmkeyserv server delete sklm[2,3,4]


# MISSING PARTS

# created  tenant?

# ?

/# rkm settings here
/var/mmfs/ssl/KeyServ/RKM.conf

# create KEYS

mmkeyserv client create cuhESSClient1 --server sklm01

  # used sklmadmin as pass phrase

mmkeyserv client register cuhESSClient --tenant devG1 --rkm-id sklm01_devG1

# confirm 

mmkeyserv tenant show

mmkeyserv client show

mmkeyserv rkm show

  # this shows the password

mmkeyserv key create --server sklm01 --tenant devG1

  # verify mmkeyserv key show --server sklm01 --tenant devG1

\

# create encrpytion rules

  # default setting
--------------------------------------------------------
RULE ’p1’ SET POOL ’data’ /* one placement rule is required at all times */
RULE ’Encrypt all files in file system with rule E1’
SET ENCRYPTION ’E1’
FOR FILESET ('PHI')
WHERE NAME LIKE ’%’
RULE ’simpleEncRule’ ENCRYPTION ’E1’ IS
ALGO ’DEFAULTNISTSP800131A’
KEYS('KEY-<manual input>:sklm01_devG1')
--------------------------------------------------------

# check default pool name --> `mmlspolicy cuhdata` ('data' on CUH)

# if performance is low, we can change 'ALGO' to `FAST****' (check spectrum scale documentation)

# to apply

mmchpolicy <device> <policy file>

# create fileset PHI

mmcrfileset cuhdata PHI --inode-space new

# link fileset

mmlinkfileset cuhdata PHI -K /exosome/PHI

# rkm settings here
/var/mmfs/ssl/KeyServ/RKM.conf

# for group-based encryption

`WHERE GROUP_ID LIKE '<gid>'`

  # each new group will need a set of new keys (last 3 lines of policy above)

  # e.g. ENCRYPTION 'E2',etc.

# apply policy

mmchpolicy cuhdata encryption_policy.txt

# verify
mmlspolicy cuhdata -L 

# test file

touch /exosome/PHI/test.txt

mmlsattr -L test.txt

  # should return encrypted

# add backup server

vim /var/mmfs/ssl/KeyServ/RKM.conf

added:
---------------------------------
kmipServerUri = tls://172.18.239.53:5696
kmipServerUri2 = tls://172.18.239.54:5696
kmipServerUri3 = tls://10.10.137.10:5696
kmipServerUri4 = tls://10.10.137.11:5696
---------------------------------

# apply change
mmkeyserv rkm change sklm01_devG1 --backup sklm02,sklm03,sklm04

# verify

mmkeyserv rkm show


\
\
\

# lsattribute cmd will tell whether files are encrypted

# SKLM server are required to be present in order for files to be accessible, which checks for keys

# PHI mount point

# INFO:

we are configuring encryption from AFM nodes


# add backup sklm servers as well


\
\
\

### 05-06-2021: Follow-Up with IBM Lab Services

#### Questions

```
- We moved a file from the encrypted fileset to a different filesystem and still readable by user. Is that expected? As long as a node is on the network as the SKLM servers?
  - how do we verify that encryption is really working?
  - how will users know?
- Changing Encryption settings on the fly? e.g. use new master key/algorithm, will this interrupt users' access to encrypted files?
- Same SKLM servers can be used by the different GPFS clusters, in that case are there special configurations needed to be implemented on the fileset we want to do the encryption on?
  - we've thus far encrypted CUH(?), but want to repeat for Bass, new master key and SSL cert needed? 
- We want to setup AFM-DR for the two encrypted filesets. Except the regular AFM-DR configuration, are there any additional configs we need to perform because of the fact that the filesets are encrypted filesets?
- We wanted implementation of GID-based encryption, Not fileset-based--walk us through procedure?
  - with each new group added for encryption, new tenant and client keys need to be created each time?
  - apply changes on the fly? interruptions to existing encrypted groups?
```

#### Discussion

- Verifying Encryption

  - as long as files are moved out, no longer encryption

- This is "Data-at-rest" encryption

- Cannot switch from Fileset to GID-based encryption, data will need to be rewritten

  - b.c. keys will be changed

\
\

#### To-Do List

- EFIX for AFM Gateways waiting

- set up AFMDR

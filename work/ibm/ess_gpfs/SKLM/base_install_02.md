## SKLM CUH Install

### Overview/Layout

- **design**

  - all 4 SKLM servers replicate from eachother in regular time intervals

  - one set of SSL keys
 
  - SKLM servers 

\
\

### Intall

#### Baseset up

- Retain primary SKLM01 backup off cluster, as a system restore point (to do later)

cp `SKLM4.0_LNX####.tar.gz` (2 packages) copy both to CUH
cp `SKLM_Silent_Linux_Resp.xml` from Bass to CUH

- installation packages placed under /root/sklm_install/

- created xfsdump of `/` to `/home/biohpcadmin/root_bak` on both SKLM03 and SKLM04

#### SKLM install

- repeat ONLY the FIRST installation step specified on docs (not noted here;creates a tmp)

- move ~Resp.xml to /root/sklm_install/disk1/

- inside same dir run installation

```
# do not specify absolute paths for install for .xml file or the installer will throw an ERROR

./silent_install.sh SKLM_Silent_Linux_Resp.xml --acceptLicense

```

\
\

#### GUI Config

10.10.137.10:9443

```
### How to drop backup on Master and replication on new SKLM servers

# on SKLM01

"Administration" --> "Basic Properties" --> add 10.10.137.10 & 11:2222 as clones

"Backup & Restore" --> "DownloadL Master backup"

  # on SKLM03 --> "Backup & Restore" --> "Browse"(to upload) --> "cancel" --> "disaply backups" --> select new bak --> "restore" 

  # NOTE: web GUI will freeze due to restart at this point! Logout (takes a few secs)

  # CONFIRM: log back in --> "Advanced Configurations" --> "Cerficates" (show see "Active") 

  # log backin "Administration" --> "Replication" --> "Stop Replicating Server" --> "Clone" --> Master port: 1111 --- Clone port: 2222 --> " Start Replication Server" --> "Welcome"

  # back on SKLM01 --> "Replication" --> "replicate now" --> "welcome" --> check replication status
```


\
\

#### fixpack installation

- TWO Impending ones before ENCRYPTION BEGINS

  - SKLM is changing to "IBM Guardian Key Lifecycle Manager" 

  - need theses fixpacks for migrating to newer versions

  - need DOWNLOAD LINK from Mark III

```
# cd sklm_install subdir

mkdir fixpack

# make xml file

```

### To-Do's/Info

```
# Per Master-Clone group: 1 Master + MAX of 6 clones

# Clone Errors

"Welcome" --> if "Connectivity" is RED --> then we need to ensure ports are open and do a manual replication step

# UNIFY TIME across all servers

# KIMP Error on SKLM02

  # fixed upon restart

# Replication Role set as "None" while port 2222 is blocking the replication

# To Enter DEBUG mode

  "Configuration" --> check "Debug Mode"

# "Administration" 

  --> control the replication schedule and log size

  --> choose location of backup files

# "Clients" --> filesets to be encrypted 

# SKLM config file

`/opt/IBM/WebSphere/AppServer/products/sklm/config/SKLMConfig.properties`

- make sure 'debug=all' is present in the file

  - 'SEVERE' is also a value

- ensure the SSL protocol is `TLSv1.2`


# To restart the server from CLI

  ## `/opt/IBM/WebSphere/AppServer/bin/stopServer.sh server1`

  ## enter pass for `wasadmin`

  ## `/opt/IBM/WebSphere/AppServer/bin/startServer.sh server1`

```
- change root pass on sklm01 

## GPFS SKLM Server Base Installation

### Overview

- I.P's: 

  - sklm01-1G: 172.18.239.53

  - sklm01-IB: 10.100.190.58

  - sklm02-1G: 172.18.239.54

  - sklm02-IB: 10.100.190.59


### Setup

```
## Deployment RPMs are located at: 
/root/deploy/

sudo hostnamectl set-hostname sklm01.gpfs.net

/etc/profile.d/proxy.sh
export https_proxy=http://proxy.swmed.edu:3128
export http_proxy=http://proxy.swmed.edu:3128

/etc/resolv.conf
search biohpc.swmed.edu
nameserver 192.168.54.1

/etc/yum.conf
# UTSW proxy
proxy=http://proxy.swmed.edu:3128

source /etc/profile.d/proxy.sh

ping utsw.edu

vi /etc/yum.repos.d/rhel_7_6_iso.repo

------------------------------------
[InstallMedia]
name=Red Hat Enterprise Linux 7.6
mediaid=1539194970.388895
metadata_expire=-1
gpgcheck=1
cost=500
enabled=1
baseurl=file:///mnt/rhel_7_6_iso
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
------------------------------------

yum repolist

sudo mkdir /mnt/rhel_7_6_iso

sudo mount -o loop /root/deploy/rhel-server-7.6-x86_64-dvd.iso /mnt/rhel_7_6_iso/

sudo yum install vim pciutils wget net-tools nmap-ncat

cd /root/deploy/

wget http://content.mellanox.com/ofed/MLNX_OFED-4.7-1.0.0.1/MLNX_OFED_LINUX-4.7-1.0.0.1-rhel7.6-x86_64.tgz

tar -xvxf MLNX_OFED_LINUX-4.7-1.0.0.1-rhel7.6-x86_64.tgz

yum install perl gtk2 atk cairo gcc-gfortran tcsh lsof tcl tk

cd /root/deploy/MLNX_OFED_LINUX-4.7-1.0.0.1-rhel7.6-x86_64

./mlnxofedinstall --without-fw-update

sudo reboot

## Upon Reboot, Verify OFED Driver & CLI utility installs

ofed_info | head -n 1

/etc/init.d/openibd status

ibdev2netdev

ibv_devinfo

ibstatus

ibstat

## Add InfiniBand Bond

sudo ifdown ib0
sudo ifdown ib1

sudo rm -i /etc/sysconfig/network-scripts/ifcfg-ib*

sudo nmcli c reload

sudo nmcli c add type bond ifname bond0 mode active-backup ip4 10.100.190.58/19

nmcli c add type bond-slave ifname ib0 master bond0
nmcli c add type bond-slave ifname ib1 master bond0

sudo vi /etc/sysconfig/network-scripts/ifcfg-bond-slave-ib0 & ...ib1
--------------------------
TYPE=Infiniband
--------------------------

sudo vi /etc/sysconfig/network-scripts/ifcfg-bond-bond0
--------------------------
MTU=2044
--------------------------

sudo ifdown bond0
sudo ifup bond0

```

### Errors

```
An error has occurred:

Error Message:
    Maximum usage count of 250 reached
Error Class Code: 61
Error Class Info: Too many systems registered using this registration token
Explanation:
     An error has occurred while processing your request. If this problem
     persists please consult the Red Hat Customer Portal Knowledge Base
     landing page on common registration Error Class Codes at
     https://access.redhat.com/solutions/17036 for a possible resolution.
     If you choose to open a support case in the Red Hat Customer Portal,
     please be sure to include details of what you were trying to do when
     this error occurred and specifics on how to reproduce this problem.

```


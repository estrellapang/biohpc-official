## 

### 06-18-2020 meeting with Grant

- **OS requirements** 

  - specific libraries to be installed for 7.6 

  - firewall ports

  - create sudo account

  - LDAP

\

- Operation Requirements

  - machine SSH & IE capabilities

  - go to IBM passport & download from web portal, Grant to provide

\

- **scheduling**

  - June 29th, 2020

  - Estimate of 3 days

\

- accounts to be created: 
 
  - db40,wiseadmin,sklm 

- SSL certs to be created
 
\
\

### 07-07-2020

- Pre-requisites

```
/root/SKLM/

yum 7.6 packages

IBM passport to download to nodes

LDAP

Infiniband Connections
```

\
\

### 03-23-2021

#### Questions/Requirements

- re-discuss size

- filesets need to be encrypted before data is made/migration

  - encrypt both CUH and Bass PHI prior to migration

- discuss AFM-DR or policy-based prefetch w/h LW

  - ask Venkat whether AFM relationship is absolutely required/optimal for policy-based prefetch


#### Info

- all work is done on master

- any time fileset changes are made, a backup has to be made at the master

  - backups can be manually done, without automation from GUI


\
\

#### 04-30-2021: Mark III-AFMDR Planning


- encryption status: Angel had issue setting up primary and slave encryption nodes

- Replication: use AFMDR for encrypted filesets

  - contract is real strict, 3 opportunities to perform replications

  - one for /work, one for /PHI, and one for ??? (extra opportunity)


\
\

### Concepts

- Different types of storage encryption methods

  - e.g. RPC encryption


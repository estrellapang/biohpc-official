## CUH Filesystem Upgrade - Day 01

### Prep Work across both clusters

# within Bass, make sure all

```
# unmount /work from CUH

mmunmount data -a 

# unmount /Work from Bass

mmumount data -a

  # "/Work: target is busy" errors reported by afm nodes

  # check `mmafmctl bassdata getstate` --> check on CUH and Bass (should only work on CUH)

  # unmount from afm nodes, and stop afm `mmafmctl bassdata stop -j <work/archive/backup>`
  
  # do a `mmafmctl bassdata getstate` to verify

  # finally unmount `mmumount data -a` across Bass 

  # still AFM02 get trouble unmounting -> shutdown
```

\

# Prep CUH

```
# disable sudo wrapper
----------------------
/opt/xcat/share/xcat/scripts/setup-local-client.sh

updatenode ems1 -V -P "gss_sudo -d"

# this is the REAL disable COMMAND!!!

mmchcluster --nouse-sudo-wrapper --sudo-user DELETE
----------------------

# unmount /work

mmumount data -a

```

\
\

### CUH FS Delete

```
# Delete FS

mmdelfs data 

mmlsnsd --> this will show that all nsds are free


# create stanza file --> delete all nsds

mmlsnsd | awk '{print $3}' 

mmdelnsd -F /tmp/stanza.txt

mmlsnd --> verify


# delete vdisk

#  you want to keep 'logTip' and 'logTipBackup', and delete the vdisks for DATA and METADATA

mmlsvdisk | awk '{print $1}' | grep -v "log"

 # delimit this into semi-colon separated output, then do `mmdelvdisk "< >"`

mmlsvdisk --> verify

\

`mmvdisk recoverygroup list` --> previous recovery groups are based on non-mmvdisks   

  # going forward GPFS v.5.*, all recovery groups will be based on mmvdisks, we needed to convert in our cluster

# conversion

 # check convention (Naming) for Bass

`mmvdisk nodeclass list`

`mmdvdisk recoverygroup covert --recovery-group <name1>,<name2> --node-class gssio1_ibgssio2_ib --recycle one`
`mmdvdisk recoverygroup covert --recovery-group <name3>,<name4> --node-class gssio1_ibgssio2_ib --recycle one`

   # `one` specifies the gpfs daemon to restart after configuration change
 
# verify

mmvdisk nodeclass list --node-class all

mmvdisk server list

  # this cmd checks the size of each recovery group, for recovery group names separate them by comma

mmvdisk vdiskset list --recovery-group rg_gssio<1-4>-ib

```  

\
\

### Create new FS on CUH

```
# define system pool same as on Bass, 1M metadata, 3% occupy --> this creates "metadata" vdiskset ONLY
mmvdisk vdiskset define --vdisk-set metadata --recovery-group all --set-size 3% --code 8+2p --block-size 1m --nsd-usage MetadataOnly --storage-pool system

# define data pool, 93% occupy
mmvdisk vdiskset define --vdisk-set data --recovery-group all --set-size 93% --code 8+2p --block-size 4m --nsd-usage dataOnly --storage-pool data

# create both
mmvdisk vdiskset create --vdisk-set metadata,data

# verify

mmlsnsd 

# Create FS

mmvdisk filesystem create --file-system cuhdata --vdisk-set metadata,data --mmcrfs cuhdata -A yes -T /exosome

  # -A --> automount

  # -T --> mount pt

  # `mmchconnfig release=LATEST` was ran ahead of time to reverse the minReleaseLevel parameter specified during AFM installation to test GPFS' backward compatibility


# Mount

`mmmount -a`

```

\
\

### Extra Notes

- vdisks are virtual LUNs created (with RAID codes) created from the disk arrays --> nsds are on top of that 

  - [explanation](https://community.ibm.com/community/user/storage/blogs/archive-user/2018/01/02/ess-disk-management)

  - [explanation 2](https://www.ibm.com/support/knowledgecenter/SSYSP8_5.3.5/com.ibm.spectrum.scale.raid.v5r04.adm.doc/bl1adv_mmvdiskmanage.htm)

- if we want to create different filesystems, they'd each their own dedicated vdisk-sets, only one filesystem will be created on CUH

- existing filesystems can be expanded via additional --vdisk-sets

\
\

### Upcoming Steps

- **Encryption**

```

```


- **Bass**

- client unmount ?

- unlink filesetS (this stops all I/O) force (-f) --> convert to independent/AFMDR --> link back --> mount? --> tests for clients sanity checks --> enclosure repair

- Dec. 15-18th AFMDR conversion & Enclosure Repair  

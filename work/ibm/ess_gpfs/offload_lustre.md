## IBM Lustre Migration Prospect

> Contacts
> Victor Baiano - Data guy
> Kim Cleator - Contractor for IBM

### Requirements

- Live Data Migration

  - cut over after all data

  - as short of a downtime as possible 


### Questions

- How would AFM gain access lustre?

  - Vendor: Gateway node (NFS,Samba) 

- Gateway node vs. Performance

  - Vendor: split lustre into different filesets

- AFM Mode:

  - Vendor: lustre do most of the work


  
- Biggest Question: PERFORMANCE DROP on Lustre

  - why I ask experience

- User/Group Quota

- File Striping 


### Vendor Feedback

- Run prefetch and simultaneously fetch "deltas" or changes

- AFM tool can monitor and estimate progress
 
- How long is the distance b/w new ESS and DDN cluster?

  - local/remote?

\
\

### 12-14-2020 Mark III - followup

> Contacts
> Randy Crystian
> Victor Baiano


#### Info

- IBM claims that they cannot transfer ACLs from lustre  

- IBM opted for rsync??

- Contracts?

  - #1 up to 2 PB transfer; 2 days of outtage

  - IBM is sitting at the contract-drawing stage, no specific plans

  - to get specific plan --> money needs to be spent to "hire" IBM Lab Services (Washington Systems center); sales people cannot make accurate estimate

- Requirements

  - short downtime; performance; cost

- Shane to work on technical info

\
\

### 12-16-2020 IBM-Mark III followup

> contacts
> Don Schulte (IBM sales - knows LW from past DDN)
> Paul Vandermost (storage manager for IBM for UTSW)
> Jan Jitze Krol (IBM engineer/sales - also DDN, migration engineer)


#### Questions

- script maintenace

  - how to gauge the script's bandwidth usage so that the users will not be affected by the data move

- quota

- nodes

- answers

```
# minimization of downitme

- LW mentions 3 months total (?)

# initial steps

- full filesystem scan, to determine maximum bandwidth, change-rate (200TB/month; & how many files changed/month?)


# our bandwidth

- delivering: 4x OSS nodes: 2 FDR, 6-12 FDR uplinks each

- receiving: 12 EDR uplinks to core-switches, ES5000 storage system (4x EDR connnections each)

# vendor does not have experience moving quota

  # 400-500 GIDs --> vendor suggests dumping quota file and translating to GPFS quota file

  # GPFS to lustre is difficult, but lustre to GPFS to lustre

```

#### Technical Info, to-do's

- vendor suggests to oppportunity to restructure data from lustre to gpfs - diff filesets

- rsync dry-run to determine how long we can scan through the whole filesystem

  - use stat() calls, ls throughout the filesystem

- Max throughput out of mds server is a limiting factor, bigger than data size

- vendor does not have experience using AFM to do bulk transfer

  - think of AFM as primarily a caching mechanism for instantenous

  - will require splitting AFM destination into multiple filesets

  - GPFS has trouble with recovery if +100 million files/fileset

  - data-mover will force us to restructure destination filesystem 

  - gateway nodes cannot transfer the "ACKLs/echoes(?)" used by the lustre unix filesystem attributes (currently AFM uses NFSv3, but NFSv4 is needed to do this) --> rsync-patch is available to pickup on the ACKLs

  - how does NFS move to scale for destination?

- split filesystem into different streams, to achieve parallelism

  - avoid multiple threads/processes to access the same directory structure

\

#### Conclusions, To-Dos

- find out # of files

- 1-24 threads how long does it take to scan through entire filesystem 

- how long does it take to move 50TB of files (doesn't have to be single thread)

- max mds threads for mds server

- gpfs to lustre quota translation


\
\

### 1-07-2021

> Contacts:
> Randy Crystian - IBM Storage Representative
> Don Schulte & Jan Krol 
> Shane Midkiff - UTSW IBM Reprep 

#### Questions

- **Benchmarks & Metrics**

  - sizes of files & inodes

  - rsync benchmarks

     - multiple threads

- **to-do**

  - check for email from Shane Midkiff  

\
\
\

### Lustre Storage Attributes

- check space and inode space

```
### OST by space usage
$ lfs df -h /project
UUID                       bytes        Used   Available Use% Mounted on
project-MDT0000_UUID        2.3T      573.9G        1.6T  26% /project[MDT:0]
project-OST0000_UUID       43.1T       35.9T        6.7T  84% /project[OST:0]
project-OST0001_UUID       43.1T       36.4T        6.3T  85% /project[OST:1]
project-OST0002_UUID       43.1T       36.4T        6.3T  85% /project[OST:2]
project-OST0003_UUID       43.1T       36.5T        6.2T  85% /project[OST:3]
project-OST0004_UUID       43.1T       36.7T        6.0T  86% /project[OST:4]
project-OST0005_UUID       43.1T       36.2T        6.5T  85% /project[OST:5]
project-OST0006_UUID       43.1T       37.0T        5.6T  87% /project[OST:6]
project-OST0007_UUID       43.1T       36.2T        6.5T  85% /project[OST:7]
project-OST0008_UUID       43.1T       36.3T        6.4T  85% /project[OST:8]
project-OST0009_UUID       43.1T       36.4T        6.3T  85% /project[OST:9]
project-OST000a_UUID       43.1T       36.3T        6.4T  85% /project[OST:10]
project-OST000b_UUID       43.1T       36.4T        6.3T  85% /project[OST:11]
project-OST000c_UUID       43.1T       35.5T        7.1T  83% /project[OST:12]
project-OST000d_UUID       43.1T       36.8T        5.9T  86% /project[OST:13]
project-OST000e_UUID       43.1T       35.9T        6.8T  84% /project[OST:14]
project-OST000f_UUID       43.1T       35.8T        6.9T  84% /project[OST:15]
project-OST0010_UUID       43.1T       36.6T        6.0T  86% /project[OST:16]
project-OST0011_UUID       43.1T       36.3T        6.4T  85% /project[OST:17]
project-OST0012_UUID       43.1T       37.1T        5.6T  87% /project[OST:18]
project-OST0013_UUID       43.1T       36.2T        6.5T  85% /project[OST:19]
project-OST0014_UUID       43.1T       35.8T        6.9T  84% /project[OST:20]
project-OST0015_UUID       43.1T       36.4T        6.3T  85% /project[OST:21]
project-OST0016_UUID       43.1T       36.6T        6.1T  86% /project[OST:22]
project-OST0017_UUID       43.1T       36.3T        6.4T  85% /project[OST:23]
project-OST0018_UUID       43.1T       36.8T        5.9T  86% /project[OST:24]
project-OST0019_UUID       43.1T       36.3T        6.4T  85% /project[OST:25]
project-OST001a_UUID       43.1T       36.6T        6.1T  86% /project[OST:26]
project-OST001b_UUID       43.1T       36.6T        6.1T  86% /project[OST:27]
project-OST001c_UUID       43.1T       36.1T        6.6T  85% /project[OST:28]
project-OST001d_UUID       43.1T       36.4T        6.3T  85% /project[OST:29]
project-OST001e_UUID       43.1T       36.0T        6.7T  84% /project[OST:30]
project-OST001f_UUID       43.1T       36.3T        6.4T  85% /project[OST:31]
project-OST0020_UUID       43.1T       36.0T        6.7T  84% /project[OST:32]
project-OST0021_UUID       43.1T       36.5T        6.2T  86% /project[OST:33]
project-OST0022_UUID       43.1T       36.2T        6.5T  85% /project[OST:34]
project-OST0023_UUID       43.1T       36.0T        6.7T  84% /project[OST:35]
project-OST0024_UUID       43.1T       36.0T        6.6T  84% /project[OST:36]
project-OST0025_UUID       43.1T       36.2T        6.5T  85% /project[OST:37]
project-OST0026_UUID       43.1T       36.7T        6.0T  86% /project[OST:38]
project-OST0027_UUID       43.1T       36.8T        5.9T  86% /project[OST:39]
project-OST0028_UUID       43.1T       33.6T        9.1T  79% /project[OST:40]
project-OST0029_UUID       43.1T       34.4T        8.3T  81% /project[OST:41]
project-OST002a_UUID       43.1T       33.8T        8.9T  79% /project[OST:42]
project-OST002b_UUID       43.1T       34.6T        8.1T  81% /project[OST:43]
project-OST002c_UUID       43.1T       34.8T        7.9T  81% /project[OST:44]
project-OST002d_UUID       43.1T       34.5T        8.2T  81% /project[OST:45]
project-OST002e_UUID       43.1T       34.4T        8.3T  81% /project[OST:46]
project-OST002f_UUID       43.1T       33.6T        9.1T  79% /project[OST:47]
project-OST0030_UUID       43.1T       34.0T        8.7T  80% /project[OST:48]
project-OST0031_UUID       43.1T       33.5T        9.2T  78% /project[OST:49]
project-OST0032_UUID       43.1T       34.2T        8.5T  80% /project[OST:50]
project-OST0033_UUID       43.1T       34.0T        8.7T  80% /project[OST:51]
project-OST0034_UUID       43.1T       34.0T        8.7T  80% /project[OST:52]
project-OST0035_UUID       43.1T       34.3T        8.4T  80% /project[OST:53]
project-OST0036_UUID       43.1T       34.1T        8.6T  80% /project[OST:54]
project-OST0037_UUID       43.1T       34.1T        8.6T  80% /project[OST:55]
project-OST0038_UUID       43.1T       33.2T        9.5T  78% /project[OST:56]
project-OST0039_UUID       43.1T       33.9T        8.8T  79% /project[OST:57]
project-OST003a_UUID       43.1T       34.4T        8.3T  80% /project[OST:58]
project-OST003b_UUID       43.1T       33.7T        9.0T  79% /project[OST:59]
project-OST003c_UUID       43.1T       33.9T        8.8T  79% /project[OST:60]
project-OST003d_UUID       43.1T       33.5T        9.2T  78% /project[OST:61]
project-OST003e_UUID       43.1T       36.0T        6.7T  84% /project[OST:62]
project-OST003f_UUID       43.1T       34.3T        8.4T  80% /project[OST:63]
project-OST0040_UUID       43.1T       34.2T        8.5T  80% /project[OST:64]
project-OST0041_UUID       43.1T       33.7T        9.0T  79% /project[OST:65]
project-OST0042_UUID       43.1T       34.0T        8.7T  80% /project[OST:66]
project-OST0043_UUID       43.1T       33.8T        8.9T  79% /project[OST:67]
project-OST0044_UUID       43.1T       33.5T        9.2T  78% /project[OST:68]
project-OST0045_UUID       43.1T       34.4T        8.3T  81% /project[OST:69]
project-OST0046_UUID       43.1T       34.0T        8.7T  80% /project[OST:70]
project-OST0047_UUID       43.1T       34.6T        8.1T  81% /project[OST:71]
project-OST0048_UUID       43.1T       33.9T        8.8T  79% /project[OST:72]
project-OST0049_UUID       43.1T       34.0T        8.7T  80% /project[OST:73]
project-OST004a_UUID       43.1T       33.9T        8.8T  79% /project[OST:74]
project-OST004b_UUID       43.1T       33.3T        9.4T  78% /project[OST:75]
project-OST004c_UUID       43.1T       34.1T        8.6T  80% /project[OST:76]
project-OST004d_UUID       43.1T       33.6T        9.1T  79% /project[OST:77]
project-OST004e_UUID       43.1T       34.4T        8.3T  80% /project[OST:78]
project-OST004f_UUID       43.1T       34.3T        8.4T  80% /project[OST:79]
project-OST0050_UUID       43.1T       33.8T        8.9T  79% /project[OST:80]
project-OST0051_UUID       43.1T       34.0T        8.7T  80% /project[OST:81]

filesystem_summary:         3.5P        2.8P      617.3T  82% /project

\
\

### OST by inode usage

lfs df -i /project
UUID                      Inodes       IUsed       IFree IUse% Mounted on
project-MDT0000_UUID  1691910144   942196728   749713416  56% /project[MDT:0]
project-OST0000_UUID   365363200    10837394   354525806   3% /project[OST:0]
project-OST0001_UUID   365363200    10620705   354742495   3% /project[OST:1]
project-OST0002_UUID   365363200    10661959   354701241   3% /project[OST:2]
project-OST0003_UUID   365363200    10637343   354725857   3% /project[OST:3]
project-OST0004_UUID   365363200    10576155   354787045   3% /project[OST:4]
project-OST0005_UUID   365363200    10852402   354510798   3% /project[OST:5]
project-OST0006_UUID   365363200    10114362   355248838   3% /project[OST:6]
project-OST0007_UUID   365363200    10709035   354654165   3% /project[OST:7]
project-OST0008_UUID   365363200    10545710   354817490   3% /project[OST:8]
project-OST0009_UUID   365363200    10725723   354637477   3% /project[OST:9]
project-OST000a_UUID   365363200    10553305   354809895   3% /project[OST:10]
project-OST000b_UUID   365363200    10861634   354501566   3% /project[OST:11]
project-OST000c_UUID   365363200    11104889   354258311   3% /project[OST:12]
project-OST000d_UUID   365363200    10754468   354608732   3% /project[OST:13]
project-OST000e_UUID   365363200    11042046   354321154   3% /project[OST:14]
project-OST000f_UUID   365363200    11029270   354333930   3% /project[OST:15]
project-OST0010_UUID   365363200    10547164   354816036   3% /project[OST:16]
project-OST0011_UUID   365363200    10507025   354856175   3% /project[OST:17]
project-OST0012_UUID   365363200    10422357   354940843   3% /project[OST:18]
project-OST0013_UUID   365363200    10710530   354652670   3% /project[OST:19]
project-OST0014_UUID   365363200    11284470   354078730   3% /project[OST:20]
project-OST0015_UUID   365363200    10638370   354724830   3% /project[OST:21]
project-OST0016_UUID   365363200    10749809   354613391   3% /project[OST:22]
project-OST0017_UUID   365363200    10675869   354687331   3% /project[OST:23]
project-OST0018_UUID   365363200    10273866   355089334   3% /project[OST:24]
project-OST0019_UUID   365363200    10512209   354850991   3% /project[OST:25]
project-OST001a_UUID   365363200    10493194   354870006   3% /project[OST:26]
project-OST001b_UUID   365363200    10609956   354753244   3% /project[OST:27]
project-OST001c_UUID   365363200    10533893   354829307   3% /project[OST:28]
project-OST001d_UUID   365363200    10622278   354740922   3% /project[OST:29]
project-OST001e_UUID   365363200    10751250   354611950   3% /project[OST:30]
project-OST001f_UUID   365363200    10659738   354703462   3% /project[OST:31]
project-OST0020_UUID   365363200    10944323   354418877   3% /project[OST:32]
project-OST0021_UUID   365363200    10644932   354718268   3% /project[OST:33]
project-OST0022_UUID   365363200    10949989   354413211   3% /project[OST:34]
project-OST0023_UUID   365363200    10767877   354595323   3% /project[OST:35]
project-OST0024_UUID   365363200    10760095   354603105   3% /project[OST:36]
project-OST0025_UUID   365363200    10588985   354774215   3% /project[OST:37]
project-OST0026_UUID   365363200    10405046   354958154   3% /project[OST:38]
project-OST0027_UUID   365363200    10114877   355248323   3% /project[OST:39]
project-OST0028_UUID   365428736    10826387   354602349   3% /project[OST:40]
project-OST0029_UUID   365428736    10460902   354967834   3% /project[OST:41]
project-OST002a_UUID   365428736    10668220   354760516   3% /project[OST:42]
project-OST002b_UUID   365428736    10299496   355129240   3% /project[OST:43]
project-OST002c_UUID   365428736    10207629   355221107   3% /project[OST:44]
project-OST002d_UUID   365428736    10482367   354946369   3% /project[OST:45]
project-OST002e_UUID   365428736    10364834   355063902   3% /project[OST:46]
project-OST002f_UUID   365428736    10716493   354712243   3% /project[OST:47]
project-OST0030_UUID   365428736    10589535   354839201   3% /project[OST:48]
project-OST0031_UUID   365428736    10713293   354715443   3% /project[OST:49]
project-OST0032_UUID   365428736    10694253   354734483   3% /project[OST:50]
project-OST0033_UUID   365428736    10689552   354739184   3% /project[OST:51]
project-OST0034_UUID   365428736    10699316   354729420   3% /project[OST:52]
project-OST0035_UUID   365428736    10229852   355198884   3% /project[OST:53]
project-OST0036_UUID   365428736    10520135   354908601   3% /project[OST:54]
project-OST0037_UUID   365428736    10304705   355124031   3% /project[OST:55]
project-OST0038_UUID   365428736    10894038   354534698   3% /project[OST:56]
project-OST0039_UUID   365428736    10668160   354760576   3% /project[OST:57]
project-OST003a_UUID   365428736    10200773   355227963   3% /project[OST:58]
project-OST003b_UUID   365428736    10860226   354568510   3% /project[OST:59]
project-OST003c_UUID   365428736    10668530   354760206   3% /project[OST:60]
project-OST003d_UUID   365428736    10686135   354742601   3% /project[OST:61]
project-OST003e_UUID   365428736     9529532   355899204   3% /project[OST:62]
project-OST003f_UUID   365428736    10577758   354850978   3% /project[OST:63]
project-OST0040_UUID   365428736    10524756   354903980   3% /project[OST:64]
project-OST0041_UUID   365428736    10352657   355076079   3% /project[OST:65]
project-OST0042_UUID   365428736    10653852   354774884   3% /project[OST:66]
project-OST0043_UUID   365428736    10753301   354675435   3% /project[OST:67]
project-OST0044_UUID   365428736    10918278   354510458   3% /project[OST:68]
project-OST0045_UUID   365428736    10573963   354854773   3% /project[OST:69]
project-OST0046_UUID   365428736    10563899   354864837   3% /project[OST:70]
project-OST0047_UUID   365428736     9965503   355463233   3% /project[OST:71]
project-OST0048_UUID   365428736    10621562   354807174   3% /project[OST:72]
project-OST0049_UUID   365428736    10719468   354709268   3% /project[OST:73]
project-OST004a_UUID   365428736    10522511   354906225   3% /project[OST:74]
project-OST004b_UUID   365428736    11035785   354392951   3% /project[OST:75]
project-OST004c_UUID   365428736    10501844   354926892   3% /project[OST:76]
project-OST004d_UUID   365428736    10801675   354627061   3% /project[OST:77]
project-OST004e_UUID   365428736    10668228   354760508   3% /project[OST:78]
project-OST004f_UUID   365428736    10177820   355250916   3% /project[OST:79]
project-OST0050_UUID   365428736    10698712   354730024   3% /project[OST:80]
project-OST0051_UUID   365428736    10547307   354881429   3% /project[OST:81]

filesystem_summary:   1691910144   942196728   749713416  56% /project

```

\
\

### Lustre Rsync Dry-Runs

#### Multinode Testing

- **Candidates**

  - picked from lab share directories/inactive user folders

- Danuser_lab (2001) 

```
459G     /project/bioinformatics/Danuser_lab/shared/To_Test_Pixel_Counter
222G    /project/bioinformatics/Danuser_lab/shared/MollyZheng
1.4T    /project/bioinformatics/Danuser_lab/shared/assaf
4.5T    /project/bioinformatics/Danuser_lab/shared/proudot
94G     /project/bioinformatics/Danuser_lab/shared/OliBiosensorForXuexia
456G    /project/bioinformatics/Danuser_lab/shared/s181504
```

- Lin_lab (4002)

```
/project/greencenter/Lin_lab/shared/ (329G)
/project/greencenter/Lin_lab/milolin (580G)
```

- ANSIR_lab (9001)

```
892G    /project/radiology/ANSIR_lab/s421360
1.1T    /project/radiology/ANSIR_lab/s178776
318G    /project/radiology/ANSIR_lab/s163266
319G    /project/radiology/ANSIR_lab/s167746
```

- Wakeland_lab (14001) 

```
1.3T    /project/immunology/Wakeland_lab/carana/
7.8T    /project/immunology/Wakeland_lab/s181706
```

\

#### Rsync Benchmarks: 

-  **Testing Directory**

  - `/project/bioinformatics/Danuser_lab/shared/proudot`

  - size, 4.5TB
  
  - num. of files/dirs = 1256147 --> ~1.26 million files

    - 56 subdirs under primary/parent dir --> good test case for multiple processing

```
# gathered from outputting first iteration of rsync logs

cat NucleusA072_P2_D1_multi.log | wc -l
1256147

  # e.g. of log
---------------------
tail NucleusA072_P2_D1_multi.log
>f+++++++++ spheroidCounting/vasanth-binning/results_analysis/stack-4.tif
>f+++++++++ spheroidCounting/vasanth-binning/results_analysis/stack-5.tif
>f+++++++++ spheroidCounting/vasanth-binning/results_analysis/stack-6.tif
cd+++++++++ spheroidCounting/vasanth-binning/version/
>f+++++++++ spheroidCounting/vasanth-binning/version/v0.9zip
cd+++++++++ spheroidCounting/vasanth-binning/version/v0.9/
>f+++++++++ spheroidCounting/vasanth-binning/version/v0.9/.exp20180905.sh.swp
>f+++++++++ spheroidCounting/vasanth-binning/version/v0.9/demo.sh
>f+++++++++ spheroidCounting/vasanth-binning/version/v0.9/detectSpheroid.sh
>f+++++++++ spheroidCounting/vasanth-binning/version/v0.9/vasanth-view-detect.sh
---------------------
```

\

```
# scopy script to be executed across all nodes
in $(seq 1 3); do sudo scp lustre_gauge_datamove/P1_D1_multi_run.sh NucleusA07$i:/tmp ; done

# flush pagecache across all nodes, inbetween each run
for i in $(seq 1 3); do sudo ssh NucleusA07$i "sync; echo 3 > /proc/sys/vm/drop_caches"; done

# remote parallel execute dry-runs
for i in $(seq 1 3); do echo NucleusA07$i; done | sudo xargs -P3 -n1 -I% sh -c 'ssh % "bash /tmp/P1_D1_multi_run.sh"'

  # COMMENT: if `-P3` is not specified, then xargs will execute the remote ssh commands sequentially != parallel processing..

--------------------------------------------
### SCRIPT - Single Thread ###

#!/bin/bash

echo "$HOSTNAME scanning 4.5TB of files, 1 proc total"
time rsync -aihn /project/bioinformatics/Danuser_lab/shared/proudot /tmp > ${HOSTNAME}_P1_D1_multi.log
-------------------------------------------

#
#

-------------------------------------------
### SCRIPT - multiple threads ###

#!/bin/bash

echo "$HOSTNAME scanning 4.5TB of files, 2 proc total"
time ls /project/bioinformatics/Danuser_lab/shared/proudot | xargs -n1 -P2 -I%  rsync -aihn /project/bioinformatics/Danuser_lab/shared/proudot/% /tmp/ > ${HOSTNAME}_P2_D1_multi.log

  # COMMENT: to increase number of concurrent/parallel rsync processes, change "-P2" to "-P<#>" -- e.g. in this test case we used -P4, -P8 as a test set as well
-------------------------------------------


### RESULTS ###

### 1 PROC ###
--------------------------------------------
for i in $(seq 1 3); do echo NucleusA07$i; done | sudo xargs -P3 -n1 -I% sh -c 'ssh % "bash /tmp/P1_D1_multi_run.sh"'
NucleusA071 scanning 4.5TB of files, 1 proc total
NucleusA072 scanning 4.5TB of files, 1 proc total
NucleusA073 scanning 4.5TB of files, 1 proc total

real    2m11.702s
user    0m9.629s
sys     0m42.152s

real    2m11.703s
user    0m10.978s
sys     0m49.777s

real    2m11.703s
user    0m10.343s
sys     0m46.775s
-------------------------------------------

### 1 PROC ### - 2
--------------------------------------------
  # note that we dropped the page cache before running the second test-run -- this practice was maintained; it'll not be documented for later test run results 

for i in $(seq 1 3); do sudo ssh NucleusA07$i "sync; echo 3 > /proc/sys/vm/drop_caches"; done

for i in $(seq 1 3); do echo NucleusA07$i; done | sudo xargs -P3 -n1 -I% sh -c 'ssh % "bash /tmp/P1_D1_multi_run.sh"'
NucleusA071 scanning 4.5TB of files, 1 proc total
NucleusA073 scanning 4.5TB of files, 1 proc total
NucleusA072 scanning 4.5TB of files, 1 proc total

real    1m14.598s
user    0m8.679s
sys     0m38.840s

real    1m44.698s
user    0m10.261s
sys     0m48.802s

real    2m8.926s
user    0m9.253s
sys     0m46.580s
--------------------------------------------

### 1 PROC ### - 3
--------------------------------------------
for i in $(seq 1 3); do echo NucleusA07$i; done | sudo xargs -P3 -n1 -I% sh -c 'ssh % "bash /tmp/P1_D1_multi_run.sh"'
NucleusA072 scanning 4.5TB of files, 1 proc total
NucleusA073 scanning 4.5TB of files, 1 proc total
NucleusA071 scanning 4.5TB of files, 1 proc total

real    1m14.610s
user    0m8.557s
sys     0m38.017s

real    1m19.721s
user    0m9.049s
sys     0m39.931s

real    1m19.851s
user    0m9.453s
sys     0m41.330s
--------------------------------------------
### 1 PROC - AVG ###
104.168 secs
#
#
#

### 2 PROC ###
--------------------------------------------
for i in $(seq 1 3); do echo NucleusA07$i; done | sudo xargs -P3 -n1 -I% sh -c 'ssh % "bash /tmp/P2_D1_multi_run.sh"'
NucleusA073 scanning 4.5TB of files, 2 proc total
NucleusA071 scanning 4.5TB of files, 2 proc total
NucleusA072 scanning 4.5TB of files, 2 proc total

real    0m57.151s
user    0m8.683s
sys     0m39.452s

real    1m17.196s
user    0m8.563s
sys     0m39.620s

real    1m27.949s
user    0m8.887s
sys     0m40.549s
-------------------------------------------

### 2 PROC ### - 2
--------------------------------------------
for i in $(seq 1 3); do echo NucleusA07$i; done | sudo xargs -P3 -n1 -I% sh -c 'ssh % "bash /tmp/P2_D1_multi_run.sh"'
NucleusA071 scanning 4.5TB of files, 2 proc total
NucleusA072 scanning 4.5TB of files, 2 proc total
NucleusA073 scanning 4.5TB of files, 2 proc total

real    1m50.719s
user    0m8.911s
sys     0m39.267s

real    2m7.385s
user    0m8.814s
sys     0m41.921s

real    2m13.196s
user    0m8.679s
sys     0m39.350s
-------------------------------------------

### 2 PROC ### - 3
--------------------------------------------
for i in $(seq 1 3); do echo NucleusA07$i; done | sudo xargs -P3 -n1 -I% sh -c 'ssh % "bash /tmp/P2_D1_multi_run.sh"'
NucleusA072 scanning 4.5TB of files, 2 proc total
NucleusA073 scanning 4.5TB of files, 2 proc total
NucleusA071 scanning 4.5TB of files, 2 proc total

real    0m54.662s
user    0m8.507s
sys     0m39.818s

real    1m22.698s
user    0m8.837s
sys     0m40.545s

real    2m18.153s
user    0m8.645s
sys     0m40.394s
-------------------------------------------
### 2 PROC - AVG ###
96.569 secs
#
#
#
### 4 PROC ### 
--------------------------------------------
for i in $(seq 1 3); do echo NucleusA07$i; done | sudo xargs -P3 -n1 -I% sh -c 'ssh % "bash /tmp/P4_D1_multi_run.sh"'
NucleusA072 scanning 4.5TB of files, 4 proc total
NucleusA071 scanning 4.5TB of files, 4 proc total
NucleusA073 scanning 4.5TB of files, 4 proc total

real    0m23.948s
user    0m9.383s
sys     0m43.187s

real    0m41.515s
user    0m9.401s
sys     0m43.457s

real    1m3.057s
user    0m9.379s
sys     0m43.979s
-------------------------------------------
### 4 PROC ### - 2
--------------------------------------------
NucleusA073 scanning 4.5TB of files, 4 proc total
NucleusA072 scanning 4.5TB of files, 4 proc total
NucleusA071 scanning 4.5TB of files, 4 proc total

real    0m24.366s
user    0m9.386s
sys     0m43.308s

real    0m47.167s
user    0m9.467s
sys     0m43.863s

real    1m24.673s
user    0m9.327s
sys     0m44.062s
-------------------------------------------
### 4 PROC ### - 3 
--------------------------------------------
for i in $(seq 1 3); do echo NucleusA07$i; done | sudo xargs -P3 -n1 -I% sh -c 'ssh % "bash /tmp/P4_D1_multi_run.sh"'
NucleusA072 scanning 4.5TB of files, 4 proc total
NucleusA071 scanning 4.5TB of files, 4 proc total
NucleusA073 scanning 4.5TB of files, 4 proc total

real    0m24.353s
user    0m9.194s
sys     0m44.023s

real    1m4.272s
user    0m9.532s
sys     0m44.989s

real    1m11.345s
user    0m9.611s
sys     0m45.144s
-------------------------------------------
### 4 PROC - AVG ###
49.409 secs
#
#
#
### 8 PROC ### 
--------------------------------------------
for i in $(seq 1 3); do echo NucleusA07$i; done | sudo xargs -P3 -n1 -I% sh -c 'ssh % "bash /tmp/P8_D1_multi_run.sh"'
NucleusA071 scanning 1.25 million files, 4.5TB in size, 8 concurrent procs total
NucleusA073 scanning 1.25 million files, 4.5TB in size, 8 concurrent procs total
NucleusA072 scanning 1.25 million files, 4.5TB in size, 8 concurrent procs total

real    1m7.302s
user    0m9.953s
sys     0m50.723s

real    1m48.474s
user    0m9.909s
sys     0m50.472s

real    1m56.176s
user    0m10.062s
sys     0m50.050s
-------------------------------------------
### 8 PROC ### - 2
--------------------------------------------
for i in $(seq 1 3); do echo NucleusA07$i; done | sudo xargs -P3 -n1 -I% sh -c 'ssh % "bash /tmp/P8_D1_multi_run.sh"'
NucleusA071 scanning 1.25 million files, 4.5TB in size, 8 concurrent procs total
NucleusA073 scanning 1.25 million files, 4.5TB in size, 8 concurrent procs total
NucleusA072 scanning 1.25 million files, 4.5TB in size, 8 concurrent procs total

real    1m27.503s
user    0m9.877s
sys     0m50.924s

real    1m47.717s
user    0m9.883s
sys     0m51.161s

real    1m59.185s
user    0m10.189s
sys     0m50.715s
-------------------------------------------
### 8 PROC ### - 3 
--------------------------------------------
for i in $(seq 1 3); do echo NucleusA07$i; done | sudo xargs -P3 -n1 -I% sh -c 'ssh % "bash /tmp/P8_D1_multi_run.sh"'
NucleusA073 scanning 1.25 million files, 4.5TB in size, 8 concurrent procs total
NucleusA071 scanning 1.25 million files, 4.5TB in size, 8 concurrent procs total
NucleusA072 scanning 1.25 million files, 4.5TB in size, 8 concurrent procs total

real    1m18.228s
user    0m9.967s
sys     0m49.460s

real    1m40.961s
user    0m9.845s
sys     0m50.900s

real    1m48.468s
user    0m10.310s
sys     0m50.922s
------------------------------------------

### 8 PROC - AVG ###
99.341 seconds 
```

### OBSERVATIONS ###

- significant metadata scan performance increase at 4 concurrent rsync processes/datamover node

  - best single node, parallel rsync results: 1256147 files/24 secs = 52339 files/sec; 4.5TB/24 secs = 187.5 GB/sec
  - best avg results from 3-nodes concurrently running parallel rsyncs = 1256147 files/49 secs = 25636 files/sec
    - 4.5TB / 49 secs = 91.8 GB/sec 

\
\

### Real File Transfer

#### 3 Nodes

- candidates: NucleusA[082-84]

- scripts: 4 parallel procs/node; ~ 1TB of data/node

  - details

```
#!/bin/bash

echo "$HOSTNAME transferring 1.4 TB of data, 4 proc total"
time ls /project/bioinformatics/Danuser_lab/shared/assaf | xargs -n1 -P4 -I% rsync -aih /project/bioinformatics/Danuser_lab/shared/assaf/% /endosome/archive/biohpcadmin/zpang1/test_dir/assaf/ > ${HOSTNAME}_P4_D3_multi.log
#!/bin/bash

echo "$HOSTNAME transferring 1.1 TB of data, 4 proc total"
time ls /project/radiology/ANSIR_lab/s178776 | xargs -n1 -P4 -I% rsync -aih /project/radiology/ANSIR_lab/s178776/% /endosome/archive/biohpcadmin/zpang1/test_dir/s178776/ > ${HOSTNAME}_P4_D3_multi.log
#!/bin/bash

echo "$HOSTNAME transferring 1.3 TB of data, 4 proc total"
time ls /project/immunology/Wakeland_lab/carana/ | xargs -n1 -P4 -I% rsync -aih /project/immunology/Wakeland_lab/carana/% /endosome/archive/biohpcadmin/zpang1/test_dir/carana/ > ${HOSTNAME}_P4_D3_multi.log
```

- **results


```
### command used

# set project norootsquash for data movers, give proper permission to testdir

for i in $(seq 2 4); do sudo ssh NucleusA08$i "sync; echo 3 > /proc/sys/vm/drop_caches"; done

for i in $(seq 2 4); do echo NucleusA08$i; done | sudo xargs -P3 -n1 -I% sh -c 'ssh % "bash /tmp/P4_D3_multi_run.sh"'
NucleusA083 transferring 1.1 TB of data, 4 proc total
NucleusA084 transferring 1.3 TB of data, 4 proc total
NucleusA082 transferring 1.4 TB of data, 4 proc total

\

### numbers

    3 data movers
    3×4 = 12 parallel processes
    3.8 TB of data — 1667020 files
    Results
        3.8TB / 196 minutes = ~19.39 GB/sec 

```

\
\

#### 5 nodes

- scripts:

```
for i in $(seq -w 073 077); do ssh NucleusA$i cat /tmp/P4_D5_multi_run.sh; done

#!/bin/bash

echo "$HOSTNAME transferring 1.4 TB of data, 4 proc total"
/usr/bin/time -o ${HOSTNAME}_time.log ls /project/bioinformatics/Danuser_lab/shared/assaf | xargs -n1 -P4 -I% rsync -rih /project/bioinformatics/Danuser_lab/shared/assaf/% /endosome/archive/biohpcadmin/zpang1/test_dir/assaf/ 2>&1 > ${HOSTNAME}_P4_D5_multi.log
#!/bin/bash

echo "$HOSTNAME transferring 1.1 TB of data, 4 proc total"
/usr/bin/time -o ${HOSTNAME}_time.log ls /project/radiology/ANSIR_lab/s178776 | xargs -n1 -P4 -I% rsync -rih /project/radiology/ANSIR_lab/s178776/% /endosome/archive/biohpcadmin/zpang1/test_dir/s178776/ 2>&1 > ${HOSTNAME}_P4_D5_multi.log
#!/bin/bash

echo "$HOSTNAME transferring 1.3 TB of data, 4 proc total"
/usr/bin/time -o ${HOSTNAME}_time.log ls /project/immunology/Wakeland_lab/carana/ | xargs -n1 -P4 -I% rsync -rih /project/immunology/Wakeland_lab/carana/% /endosome/archive/biohpcadmin/zpang1/test_dir/carana/ 2>&1 > ${HOSTNAME}_P4_D5_multi.log
#!/bin/bash

echo "$HOSTNAME transferring 2.3 TB of data, 4 proc total"
/usr/bin/time -o ${HOSTNAME}_time.log ls /project/bioinformatics/Danuser_lab/shared/proudot | head -n 26 | xargs -n1 -P4 -I% rsync -rih /project/bioinformatics/Danuser_lab/shared/proudot/% /endosome/archive/biohpcadmin/zpang1/test_dir/proudot_first/ 2>&1 > ${HOSTNAME}_P4_D5_multi.log
#!/bin/bash

echo "$HOSTNAME transferring 2.2 TB of data, 4 proc total"
/usr/bin/time -o ${HOSTNAME}_time.log ls /project/bioinformatics/Danuser_lab/shared/proudot | tail -n 30 | xargs -n1 -P4 -I% rsync -rih /project/bioinformatics/Danuser_lab/shared/proudot/% /endosome/archive/biohpcadmin/zpang1/test_dir/proudot_second/ 2>&1 > ${HOSTNAME}_P4_D5_multi.log

```

\

- commands:

```
# clean cache

for i in $(seq -w 073 077); do echo "NucleusA$i"; sudo ssh NucleusA$i "sync; echo 3 > /proc/sys/vm/drop_caches"; done


# real cmd
------------------------------------------------------
for i in $(seq -w 073 77); do echo NucleusA$i; done | xargs -P5 -n1 -I% sh -c 'sudo ssh % "hostname;sync; echo 3 > /proc/sys/vm/drop_caches"'
------------------------------------------------------

# issue transfer command; as root on 006

  # inserted cmd in `/home2/zpang1/lustre_gauge_datamove/5_node_transfer.sh`

------------------------------------------------------
for i in $(seq -w 073 77); do echo NucleusA$i; done | xargs -P5 -n1 -I% sh -c 'ssh % "bash /tmp/P4_D5_multi_run.sh"

  # issue cmd: `nohup sudo bash 5_node_transfer.sh > 5_node_migrate_test_03162021.out`

------------------------------------------------------
```

- stats:

  - 8.3 TB of production files

  - cat | wc -l each outfile to see number of files

\


- 1st try - results:

```
# start time: Mar. 16th, 2021 @ 5:19 P.M.

# finish times:
------------------------------------------------------
ls -lt NucleusA073_P4_D5_multi.log
-rw-r--r-- 1 root root 109648731 Mar 16 19:50 NucleusA073_P4_D5_multi.log

ls -l NucleusA074_P4_D5_multi.log
-rw-r--r-- 1 root root 11421045 Mar 17 00:41 NucleusA074_P4_D5_multi.log

ls -l NucleusA075_P4_D5_multi.log
-rw-r--r-- 1 root root 48120809 Mar 16 19:02 NucleusA075_P4_D5_multi.log

ls -l NucleusA076_P4_D5_multi.log
-rw-r--r-- 1 root root 111226017 Mar 16 20:39 NucleusA076_P4_D5_multi.log

ls -l NucleusA077_P4_D5_multi.log
-rw-r--r-- 1 root root 36390018 Mar 16 21:43 NucleusA077_P4_D5_multi.log
------------------------------------------------------

# observations: the time command did not work
```

- 2nd try - results:

  - removed `/usr/bin/time -o ${HOSTNAME}_time.log`

- updated cmds:

```
for i in $(seq -w 073 077); do sudo ssh NucleusA$i cat /tmp/P4_D5_multi_run.sh; done
#!/bin/bash

echo "$HOSTNAME transferring 1.4 TB of data, 4 proc total"
ls /project/bioinformatics/Danuser_lab/shared/assaf | xargs -n1 -P4 -I% rsync -rih /project/bioinformatics/Danuser_lab/shared/assaf/% /endosome/archive/biohpcadmin/zpang1/test_dir/assaf/ 2>&1 > ${HOSTNAME}_P4_D5_multi.log
#!/bin/bash

echo "$HOSTNAME transferring 1.1 TB of data, 4 proc total"
ls /project/radiology/ANSIR_lab/s178776 | xargs -n1 -P4 -I% rsync -rih /project/radiology/ANSIR_lab/s178776/% /endosome/archive/biohpcadmin/zpang1/test_dir/s178776/ 2>&1 > ${HOSTNAME}_P4_D5_multi.log
#!/bin/bash

echo "$HOSTNAME transferring 1.3 TB of data, 4 proc total"
ls /project/immunology/Wakeland_lab/carana/ | xargs -n1 -P4 -I% rsync -rih /project/immunology/Wakeland_lab/carana/% /endosome/archive/biohpcadmin/zpang1/test_dir/carana/ 2>&1 > ${HOSTNAME}_P4_D5_multi.log
#!/bin/bash

echo "$HOSTNAME transferring 2.3 TB of data, 4 proc total"
ls /project/bioinformatics/Danuser_lab/shared/proudot | head -n 26 | xargs -n1 -P4 -I% rsync -rih /project/bioinformatics/Danuser_lab/shared/proudot/% /endosome/archive/biohpcadmin/zpang1/test_dir/proudot_first/ 2>&1 > ${HOSTNAME}_P4_D5_multi.log
#!/bin/bash

echo "$HOSTNAME transferring 2.2 TB of data, 4 proc total"
ls /project/bioinformatics/Danuser_lab/shared/proudot | tail -n 30 | xargs -n1 -P4 -I% rsync -rih /project/bioinformatics/Danuser_lab/shared/proudot/% /endosome/archive/biohpcadmin/zpang1/test_dir/proudot_second/ 2>&1 > ${HOSTNAME}_P4_D5_multi.log

```

- cmds:


```
# launch:
sudo bash lustre_gauge_datamove/5_node_transfer.sh

# check:
for i in $(seq -w 073 77); do echo NucleusA$i; sudo ssh NucleusA$i 'pgrep -a rsync'; done

ps -aux | grep -i bash | grep -i lustre

root      3013  0.0  0.0 243364  4628 ?        S    14:03   0:00 sudo bash lustre_gauge_datamove/5_node_transfer.sh
root      3014  0.0  0.0 113156  1168 ?        S    14:03   0:00 bash lustre_gauge_datamove/5_node_transfer.sh

for i in $(seq -w 073 77); do echo NucleusA$i; sudo ssh NucleusA$i 'ps -aux | grep -i bash | grep -i multi_run'; done

```

- results:

```
# start time: Mar. 18th,2021 @2:03 P.M.

# finish times
------------------------------------------------
Mar 18 22:57 NucleusA073_P4_D5_multi.log
Mar 18 16:48 NucleusA074_P4_D5_multi.log
Mar 18 15:32 NucleusA075_P4_D5_multi.log
Mar 18 16:41 NucleusA076_P4_D5_multi.log
Mar 18 15:52 NucleusA077_P4_D5_multi.log
------------------------------------------------
```
\
\

### 5-node Data Migration Trial on Danuser_lab Data

#### The Data

- `/project/bioinformatics/Danuser_lab/melanoma/`

  - subdir size breakdowns: `/home2/zpang1/lustre_gauge_datamove/5_node_danuser_test/subdir_sizes_[01-02].out`

```
# ../raw/
----------------------------------------------------------------
4.0K    /project/bioinformatics/Danuser_lab/melanoma/raw/melanomaDrugEvaluation  # NucleusA062
127M    /project/bioinformatics/Danuser_lab/melanoma/raw/temp  # NucleusA062
186M    /project/bioinformatics/Danuser_lab/melanoma/raw/Olfactory Receptors  # NucleusA062
230M    /project/bioinformatics/Danuser_lab/melanoma/raw/HighResolutionEpi  # NucleusA062
338M    /project/bioinformatics/Danuser_lab/melanoma/raw/3DFixedMorphology  # NucleusA062
485M    /project/bioinformatics/Danuser_lab/melanoma/raw/Jiggity-Gels  # NucleusA062
793M    /project/bioinformatics/Danuser_lab/melanoma/raw/P29S  # NucleusA062
839M    /project/bioinformatics/Danuser_lab/melanoma/raw/CollagenSHG  # NucleusA062
882M    /project/bioinformatics/Danuser_lab/melanoma/raw/posterFigures # NucleusA062
1.4G    /project/bioinformatics/Danuser_lab/melanoma/raw/tisogai  # NucleusA058
1.5G    /project/bioinformatics/Danuser_lab/melanoma/raw/TIRF  # NucleusA060
2.4G    /project/bioinformatics/Danuser_lab/melanoma/raw/UprightInvasion  # NucleusA058
2.5G    /project/bioinformatics/Danuser_lab/melanoma/raw/exampleMovies # NucleusA058 
2.9G    /project/bioinformatics/Danuser_lab/melanoma/raw/InterestingCells # NucleusA058
3.7G    /project/bioinformatics/Danuser_lab/melanoma/raw/AnalyzedData # NucleusA058
3.9G    /project/bioinformatics/Danuser_lab/melanoma/raw/GoingBlind # NucleusA058
7.5G    /project/bioinformatics/Danuser_lab/melanoma/raw/JChi # NucleusA058
8.9G    /project/bioinformatics/Danuser_lab/melanoma/raw/JustinData # NucleusA058 
11G     /project/bioinformatics/Danuser_lab/melanoma/raw/WetLabScope # NucleusA058
13G     /project/bioinformatics/Danuser_lab/melanoma/raw/Presentations  # NucleusA058
19G     /project/bioinformatics/Danuser_lab/melanoma/raw/CultureScope  # NucleusA058
22G     /project/bioinformatics/Danuser_lab/melanoma/raw/InvertedInvasion  # NucleusA061
24G     /project/bioinformatics/Danuser_lab/melanoma/raw/2DMigration  # NucleusA061
63G     /project/bioinformatics/Danuser_lab/melanoma/raw/primaryMelanomaBasics # NucleusA058
122G    /project/bioinformatics/Danuser_lab/melanoma/raw/MorphologyViability  # NucleusA058
147G    /project/bioinformatics/Danuser_lab/melanoma/raw/3D_Assay  # NucleusA061
200G    /project/bioinformatics/Danuser_lab/melanoma/raw/3DViability  # NucleusA060
272G    /project/bioinformatics/Danuser_lab/melanoma/raw/FieldSynethesis # NucleusA062
297G    /project/bioinformatics/Danuser_lab/melanoma/raw/3DCollagenLowResWidefield #NucleusA060
321G    /project/bioinformatics/Danuser_lab/melanoma/raw/LiveCell_Epi #NucleusA060
333G    /project/bioinformatics/Danuser_lab/melanoma/raw/RandomImages  #NucleusA060
335G    /project/bioinformatics/Danuser_lab/melanoma/raw/SpinningDisk  #NucleusA060
417G    /project/bioinformatics/Danuser_lab/melanoma/raw/hiRes3D(cheap knockoff) #NucleusA060
541G    /project/bioinformatics/Danuser_lab/melanoma/raw/3DCollagenSpinningDisk #NucleusA060
606G    /project/bioinformatics/Danuser_lab/melanoma/raw/randomNonMelanoma #NucleusA060
1.2T    /project/bioinformatics/Danuser_lab/melanoma/raw/amohan #NucleusA061
1.3T    /project/bioinformatics/Danuser_lab/melanoma/raw/2Dmorphology  #NucleusA061
1.3T    /project/bioinformatics/Danuser_lab/melanoma/raw/bweiss # NucleusA061
3.7T    /project/bioinformatics/Danuser_lab/melanoma/raw/ECM_Modulation # NucleusA061
5.6T    /project/bioinformatics/Danuser_lab/melanoma/raw/hiRes3D # NucleusA062
6.2T    /project/bioinformatics/Danuser_lab/melanoma/raw/Cytoskeleton_Metabolism #NucleusA061
8.5T    /project/bioinformatics/Danuser_lab/melanoma/raw/anevarez   # NucleusA061
19T     /project/bioinformatics/Danuser_lab/melanoma/raw/3DCollagenPhase  #NucleusA059
43T     /project/bioinformatics/Danuser_lab/melanoma/raw/Drug_Discovery   #NucleusA058
---------------------------------------------------------------

\

# ../analysis/

```
---------------------------------------------------------------
4.6G    /project/bioinformatics/Danuser_lab/melanoma/analysis/ESW  # NucleusA059
16G     /project/bioinformatics/Danuser_lab/melanoma/analysis/tisogai  # NucleusA059
17G     /project/bioinformatics/Danuser_lab/melanoma/analysis/mdriscoll # NucleusA059
81G     /project/bioinformatics/Danuser_lab/melanoma/analysis/Philippe  # NucleusA059
105G    /project/bioinformatics/Danuser_lab/melanoma/analysis/Jungsik  # NucleusA059
193G    /project/bioinformatics/Danuser_lab/melanoma/analysis/Abbee  # NucleusA059
335G    /project/bioinformatics/Danuser_lab/melanoma/analysis/bweiss # NucleuA059
498G    /project/bioinformatics/Danuser_lab/melanoma/analysis/Hanieh #NucleusA062
1.8T    /project/bioinformatics/Danuser_lab/melanoma/analysis/Weems  #NucleusA062
12T     /project/bioinformatics/Danuser_lab/melanoma/analysis/Vasanth  #NucleusA060
----------------------------------------------------------------
```
\

#### Script: Directory Allocations/Data Mover

```

```

\
\
\


### Lustre iozone throughput tests

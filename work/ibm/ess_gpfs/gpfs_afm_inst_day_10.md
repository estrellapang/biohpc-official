## AFM Day 10

### Info

```
mmlsmgr 

# IO3 --> current filesystem manager

# IO1 --> cluster manager

# AFM works the fileset level, not the filesystem level

# In order for AFM to replicate---everything under /work has to be created as either dependent or independent filesets

  # dependent filesets can get default quotas from root fileset

  # independent fileset gets their own quotas

# to create fileset is to create a directory, but directories aren't automatically filesets
 
# WebX Sessions (can remote control our machines; also screen sharing)

# list full info on recovery groups

mmlsrecoverygroup -L

 # list recoverygroups for specific I/O node

mmlsrecoverygroup rg_gssio-hs -L

# list vidisks

mmvdisk vdiskset list

# see pools:

mmlspool bassdata

 # see pools for specific pools

mmlspool bassdata clinical -L

mmlspool bassdata data -L

# change pools --> we can re-assign one pools' vdisks to another pool --> this will result in re-stripping 

  `mmchpool` --> this was not done

# we can also delete the pool and then add more vdisks to existing pools


## GPFS structure

- data will by default go to the "default pool" commonly named "data", unless otherwise specified on the policy file

  -NOTE: if there are MORE THAN 1 DATA POOLS (& therefore vdisksets for each pool), The SEQUENTIAL ORDER of vdisk and filesystem creation will be CRUCIAL in DETERMINING the PRIMARY/DEFAULT data pool

e.g
----------------------------------------------------------
mmvdisk vdiskset create --vdisk-set metadata,data,clinical
mmvdisk filesystem create --file-system data --vdisk-set metadata,data,clinical

  # "data" would be the DEFAULT pool to which all information will be BY DEFAULT stored, for example, if we set the above fs as cache for replication, then the migrated data chunks would be written to the data pool rather than the clinical pool
----------------------------------------------------------
 
the only way to keep locks on size of data chunks is through POOLs, FILESYSTEMS, NOT THROUGH FILESETS (we can only limit and manipulate the innodes for each fileset)

vdisks --> system pool ---> data & metadata pools --> filesystem --> filesets

 # pools can be extended with Vdisks or Mdisksets
  

# see GPFS version 

mmdiag --version

# debug commands

mmtracectl --start

mmtrace

mmafmctl bassdata stop -j archive

mmafmctl bassdata start  -j archive

mmtrace 

zgrep enablePCACHE <trace file>.gz 


### LONG TERM PLAN ###

- /endosome/archive + /endosome/.backup --> migrate first

- move work rysnc backup scripts to /endosome/work 


## check node health on one node

/usr/lpp/mmfs/bin/mmhealth --> shows options

mmhealth node show  # local node

mmhealth cluster # show all nodes' health on cluster

```

### AFM Set-up

```
# make nodeclass for CUH AFM (afm03-ib, afm04-ib)

  # change page pool to 112G for them

# change afm nodes as servers, then gateways (check ems01 on Bass) 

  # make server
mmchlicense server -N afm01-hs,afm02-hs  

  # make gateway
mmchnode --gateway -N afm01-hs,afm02-hs

# create fileset archive for bass

  # creates archive under bass filesystem 

mmcrfileset bassdata archive -p afmTarget=gpfs:///Work/archive -p afmmode=ro -p afmEnableAutoEviction=no --inode-space=new

  # link fileset

mmlinkfileset bassdata archive -J /work/archive 

  # mmlscluster should see afm nodes as "gateway" nodes

mmafmctl bassdata getgateway -j archive 

  # see logs if anything wrong: `tail -f /var/adm/ras/mmfs.log.latest`

`mmafmctl bassdata getstate` --> see Cache State, Gateway Node, and Fileset info

# A line was changed in the following file in order for client cluster to link/mount remote filesets to current one

copy `/usr/lpp/mmfs/bin/mmcommon` to ALL AFM NODES!

# debug command
mmcommon pcache mount bassdata 1 gpfs:///Work/archive 0 0 


# create fileset for backup fileset

mmcrfileset bassdata .backup -p afmTarget=gpfs:///Work/.backup -p afmmode=ro -p ~ ~ 

mmlinkfileset bassdata .backup -J /work/.backup

ls /work/.backup --> see if files there 

mmafmctl bassdata getstate --> we should see active

 # assign .backup to other afm node (02)
mmchfileset bassdata .backup -p afmGateway=afm02-hs

# if need to delete
mmunlinkfileset bassdata .backup

mmdelfileset bassdata .backup -f

# see current filesets

@ems1
mmlsfileset bassdata


# could not assign separate nodes--shut down and thn rehash

mmshutdown -a
mmchconfig afmHashVersion=5
mmstartup -a (wait for arbitration) 

mmgetstate -a 

mmafmctl bassdata stop -j .backup

mmount data -a --- you should see /work/archive & /work/.backup now

mmafmctl bassdata start -j .backup

mmafmctl bassdata getstate

# set innode for backup fileset

mmchfileset bassdata .backup --inode-limit 350M

mmchfileset bassdata archive  --inode-limit 250M

# confirm
mmlsfileset bassdata -L 

### start prefetch

----------------------------------------------
nohup mmafmctl bassdata prefetch -j archive --directory /work/archive --prefetch-threads 8 --split-dirs

  # CTRL + Z to do in background

cat nohup.out to see 

# do same for .backup (on AFM02)

bg  # see background processes

# install dstat package (need registrate)
dstat 

# check status of pretching

echo afm_s | mmpmon

# need to see this process for afm migration

ps -ef | grep tspca   

mmfsadm dump afm  --> see status

df -h --> see progress!!!

----------------------------------------------

## if process not seen; we can @ems restart; we have to be in /root/ directory

mmafmctl bassdata stop -j .backup

mmafmctl bassdata resync -j .backup

mmafmctl bassdata start -j .backup
```
### UPDATE:

```
# migration began to to move data into clinical pool

## stop migration

mmafmctl bassdata stop -j archive 

mmafmctl bassdata stop -j .backup

## check process 

ps -ef | grep tspca  

mmafmctl bassdata getstate --> should show stopped

 # kill any remaining processes

## delete pool (while filesystem still mounted?) 

mmunlinkfileset bassdata .backup

mmunlinkfileset bassdata archive

mmumount all -a

 # if this doesn't work;  shutup gpfsdaemon

mmshutdown -N afm01-hs,afm02-hs

mmlsdisk bassdata --> shows which disks (NSDS=recovery groups + vdisks) belongs to which pools

  # put all NSD disks in a file and delete them

mmdeldisk bassdata -F <disklist>.txt

 # didn't work again

# tried to delete filesystem, but "/work is busy" -- needs to have GPFS cluster up & ummount in order to delete

 # turns out a node was in /work directory

# now we can delete the NSDs 

```

### RECREATE FILESYSTEM

```
# commands available on Docs

mmvdisk vdiskset define --vdisk-set metadata --recovery-group all --set-size 3% --code 8+2p --block-size 1m --nsd-usage MetadataOnly --storage-pool system

mmvdisk vdiskset define --vdisk-set data --recovery-group all --set-size 93% --code 8+2p --block-size 4m --nsd-usage dataOnly --storage-pool data

mmvdisk vdiskset create --vdisk-set metadata,data

mmvdisk filesystem create --file-system bassdata --vdisk-set metadata,data

mmlsfs bassdata

 # change fs mountpoint to "/endosome"

mmchfs bassdata -T /endosome

# enable quota options

mmchfs bassdata -Q yes

mmchfs bassdata  --perfileset-quota

mmdefquotaon -g bassdata

mmlsfs bassdata

### differences in latest filesystem

  # change filesystem name to "endosome"

  `mmchfs bassdata -Q yes` --> enable quota

   # enable default quota, perfilest-quota


# since remote mount was already setup, no need to re-add remote mount on cluster(separate from local file system)

mmount all -a 
 
  # remote filesystem should be there 

df -h to check in Pebibytes (factors of 1024) 

df -H to echk in Petabytes (human readable, factors of 1000's between byte levels)
```

### Re-initialization Migration

```
# check steps in Isiah's email

 # difference: "backup" instead of ".backup" fileset named

mmlinkfileset bassdata archive -J /endosome/archive (do this later! afm node assign need to happen first)

 # repeat for backup: "/endosome/.backup"

# remember to assgin afm nodes for each fileset --> then link them 

# mmafmctl getstate bassdata

# check Hashversion

mmlsconfig | grep -i afm

# start -j archive and backup 

 # if doesn't work traverse into directory on both afm nodes, then start them again

# start prefetch --> DIFFERENCE: `--directory /endosome/archive`

# once we start -- Check!
mmafmctl bassdata prefetch -j archive (and backup)

# do dstat on both nodes
dstat -tcmsn -N bond0

  # we paused to change inode limit on both filesets

# To RESTART: unlinked filessets and relinked them

# re-execute prefech commands on both of them afm nodes
 
# if nothing starts when we do afmctl get-state, then we need to traverse or ls the linked directories in BOTH AFM nodes

 e.g. ls /endosome/archive  

```

### ACTIONS:

```
# check afm dstat commands--check history

# make cron job for checking

# delete test filesets on new cluster

# enable sudo-wrappers

# create clinical fileset and assign Innodes

# get xdsh to talk to all servers

# change pagepool for client nodes

  # check how much memory "mmfsd" is using to dictate proper pagepool size 

```

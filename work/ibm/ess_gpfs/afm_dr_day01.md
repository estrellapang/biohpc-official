##

### Convert to Independent Fileset

```
# all nodes on bassclient and bassdata were shutdown (gpfs) except nsd nodes and afm nodes

# on ems01

# unlink all filesets at their junction points, then disable it as a target

mmunlinkfileset bassdata archive -f

mmchfileset bassdata archive -P afmTarget=disable

  # confirm `yes`, then `no` for "fileset should be verified" warning

# re-link fileset

# check if filesets are independent

mmlsfileset bassdata <filesets> -Y

mmafmctl bassdata getstate -k work (should say "fileset not AFM fileset")

# relink filesets back to their mount point junction

mmlinkfileset bassdata archive -J /endosome/archive

cd /endosome/archive  # check if filesystem viable

## REPEAT for `backup` `work`

# check nodes by mounting a few clients

mmstartup -N 26,21   # these are their cluster index numbers (S003 and lamella01)

# special for work fileset (convert to independent)

- for nodes that cannot immediately remount, do `mmgetstate -a` to accept all SSH connections in order to remount

  - ssh back and forth between all members of cluster to ensure all nodes and perform direct SSH

  - `mmmount bassdata -N <nodename>`

- noticed that not all nodes have the same `/etc/hosts` --> synchronized through the whole cluster again

  - then repeat `mmgetstate -a` once more

  - copy ems' `.ssh/authorized_keys` to problematic nodes

- debug by `mmumount bassdata -a` (from problematic node) ---> shows WHICH OTHER nodes are showing "/endosome target is busy" --> can issue `mmshutdown -a` across cluster to further debug

- restarted the cluster --> didn't fix issue

- `mmcommon showLocks` --> no locks

- `mmdiag --deadlock` --> no deadlocks

- `mmdelnode -N 21` --> deleted lamella01-ib

  - `mmaddnode -N lamella01-ib`

- `dmesg -T` --> shows mmfs memory allocation error

```
[D] GPFS cxiInitFastCondvar: free fcThrP at 0xFFFF8873FFA00000

```

- pagepool may be too small for lamella01 --> `/usr/lpp/mmfs/bin/mmdiag --config | grep -i page` --> gave 4GB, increasing to 16GB

- `mmchconfig pagepool=16GB -i -N <Nucleus005/lamella01-ib>` (40GB) for lamella

  - if fails, shutdown node, and implement same command w/o `-i` --> restart GPFS again

- ISSUE fixed

```
lsof | grep -w '/archive\|/work' --> kill any straggling processes
```

\
\

### Configuring /work for AFMDR

- Isiah Forwards AFM template

  - to fill as proceed

- Need to remote mount Bass on CUH

```
# check if old ssl keys are still there

# (ems1 - CUH)
/root/bass_key

# (ems1 - Bass)
/root/cuh_key

# bass is the owning cluster, on bass - given that the keys have already been exchanged

mmauth update . -l AUTHONLY

  # repeat on CUH

# on bass:

mmauth add biohpc-ess.gssio1-ib -k ~/cuh_key/id_rsa.pub

mmauth grant biohpc-ess.gssio1-ib -f bassdata

# on cuh:

  # specify bass cluster name and contact nodes

mmremotecluster add biohpc-ess.gssio1-hs -n gssio1-hs,gssio2-hs,gssio3-hs,gssio4-hs,ems1-hs  -k ~/bass_key/id_rsa.pib

mmremotefs add Endosome -f bassdata -C biohpc-ess.gssio1-hs -T /Endosome

  # filesystem name cannot be any existing name from both sides

mmremotefs update Endosome -f bassdata -C biohpc-ess.gssio1-hs -T /endosome

# add afm nodes to CUH

mmaddnode -N afm03-ib, afm04-ib

mmchlicense server --accept -N afm03-ib,afm04-ib

mmchnode --gateway -N afm03-ib,afm04-ib 

mmchconfig verbsPorts="mlx5_0 mlx5_1",verbsRdma="enable" -N afm03-ib,afm04-ib

mmstartup -N afm03-ib,afm04-ib

# remote mount

mmmount Endosome -a

ls /endosome  # check

```






\

- Remote mount CUH on Bass, this is for disaster recovery FAILOVER

```
# on cuh (ems1)

mmauth update biohpc-ess.gssio1-hs -k ~/bass_key/id_rsa.pub   # use `add` if it's the first time

mmauth grant biohpc-ess.gssio1-hs -f cuhdata 

# on bass (ems1)

mmremotecluster update biohpc-ess.gssio1-ib -n gssio1-ib,gssio2-ib,gssio3-ib,gssio4-ib,ems1-ib,afm03-ib,afm04-ib  -k ~/cuh_key/id_rsa.pub

mmremotefs add Exosome -f cuhdata -C biohpc-ess.gssio1-ib -T /Endosome

mmmount Exosome -a

```

\


- Need to create /work fileset

\
\

- convert to AFM-DR, on afm nodes:

```
# bass, afm01

mmlscluster to check

mmchconfig afmEnableADR=yes  # repeat on CUH side as well (ems1 or afm nodes)

mmlsconfig | grep -i afmdr  # to confirm

mmshutdown --> mmstartup  # on all afm nodes from each side

mmmount Exosome # make sure remote mounts are on afm nodes

# no need to do `showmount -e` or do nfs related ops bc we are using remote mount

# on cuh

# enable ACL/EA transport 

mmafmconfig enable /exosome

ls -la /exosome # we should see `.afm`


# convert fileset on bass as "primary AFM fileset" (work in this case)

mmafmctl bassdata getprimaryid -j work

  # pull primiary id for work fileset on bass


# back on bass (afm01)

mmafmctl bassdata converttoprimary -j work --afmtarget gpfs:///exosome/work --inband

  # on Bass; we have to first convert work as primary, `--afmtarget` defines which secondary fileset to push to

  #  --nocheck-metadata

  # `--inband` specifies the network trucking 

  # we ditched `check-metadata` due to errors

  # NOTE: the step above requires Bass to REMOTE MOUNT CUH

  # this KEEPS failing --> SHOULD WE BE ISSUING FROM BASS???

# on cuh:

  # this creates a new fileset

mmcrfileset cuhdata work --inode-space new -p afmMode=drs -p afmPrimaryId=<ID pulled from `getprimaryid`>

\
\

# Cont'd

# current status: create fileset work on CUH

  # cuh - ems1

mmcrfileset cuhdata work --inode-space new

# check fileset afm status

mmlsfileset cuhdata work --afm -L

  # if `afm=-associated No` --<> we will link it to a junction point or mount point

mmlinkfileset cuhdata work -J /exosome/work

# on bass

# try to get Bass to define CUH work as afmtarget again (failed previously due to work fileset junctioned to /exosome/work not having created the day before)

mmafmctl bassdata converttoprimary -j work --afmtarget gpfs:///exosome/work --inband --check-metadata

  # failed

# on CUH

mmafmconfig enable /exosome/work

  # `.afm` was missing in there

 # disable afm on /exosome

mmafmconfig disable /exosome

unmount and mount

 # after this, the `.afm` should disappear from `/exosome/`

# re-create work fileset on CUH

 # delete old work

mmunlinkfileset cuhdata work

mmdelfileset cuhdata work -f

 # recreate fileset



  # recreate, specify that this fileset is secondary to a primary site

mmcrfileset cuhdata work --inode-space new

mmlinkfileset cuhdata work -J /exosome/work

mmafmconfig enable /exosome/work


# troubleshoot: restarted CUH cluster and found out that AFM03 and AFMO4 cannot ssh back into the 4 gssio[1-4]-ib nodes; performed `ssh-copy-id -i ~/.ssh/id_rsa.pub root@gssio[1-4]-ub` as solution

# cannot mmstartup afm03-4 --> disabled AFMDR mode on CUH cluster `mmchconfig afmEnableADR=yes`

# errors `ndsRAIDFlusher...` on afm nodes in attempting to start

# on afm03 --> /var/mmfs/ --> rm -rf * # repeated on afm04

# reinstalled binaries for gpfs for each afm node, `rpm -ivh <location of all gpfs rpms>`

# STOPPED and DISABLED firewalld --> added nodes back to CUH cluster

# ensure /exosome is mounted and remote mounted

# re-enabled AFMDR on cluster

# enalbled /exosome with afm
mmchconfig afmEnableADR=no
mmafmconfig enable /exosome (?)
mmafmconfig disable /exosome (?)
mmchconfig afmEnableADR=yes
mmafmconfig enable /exosome

# shutdown cluster and restart it(on secondary) --> before we didn't do that after enabling afm, this time it worked

# SUCCESS!

# recreate filesystem, this time it's a success
mmcrfileset cuhdata work --inode-space new -p afmMode=drs -p afmPrimaryId=<ID pulled from `getprimaryid`>

# link work fileset to its junction
mmlinkfileset cuhdata work -J /exosome/work

# verify that work is really a secondary fileset
mmlsfileset cuhdata work --afm -L

.
.
.

# enable afmDR instantaneously on the cluster, previously didn't add `-i` option, which would have required a gpfs restart

mmchconfig afmEnableADR=yes -i

# back up bass, re-convert work to primary again
mmafmctl bassdata converttoPrimary -j work --afmtarget gpfs://exosome/work --inband

  # worked this time, except quit with error 'remote I/O error' and 'PrimInitFail' when `mmafmctl bassdata getstate -j work`

# confirm configuration 
mmlsfileset bassdata work --afm -L

.
.
.


### end of build day 02 ###

\
\

### build day 03 ### - AFM-DR with Venkat

- Info to queue each file, it takes around 700 bytes, if a fileset has a lot of files --> gateway nodes will run out of memory

# attempt to revert the converttoPrimary on Bass

mmafmctl bassdata changeSecondary -j work --new-target gpfs:///exosome/work --inband

  # this starts the resync procress (queueing data in memory) for afm nodes on Bass

  # initially failed because the files `policyData.mmafmctl.bassdata.work.#####.list.files` fill up the /tmp dir on afm nodes`

# Venkat recommends that AFM-DR needs to be less than 100 million files, and to stop afm queing (but we have 179 mil)

  # shared a document that specify the requirement, which UTSW may not meet, too resource extensive; may not be able to queue the changes in data on time

  # typically AFM-DR requires a special approval/review process from IBM board 

mmafmctl bassdata stop -j work

# ALTERNATIVE SOLUTION (not-dr), do prefetch, and run policy files to pull the differences
 
# we switched to AFM-DR RPO license due to initial AFM RPO 0 license expiring -- we may not necessarily have to do DR

# what's different between AFM/AFM-DR: AFM-DR queues the differences at gateway nodes, then dumps it --> regular AFM queues differences instantaneously



.

mmfsadm dump afm   # show if there are files being queued

# ERROR CAUGHT:

- when issuing `changeSecondary` to assign CUH work as afmtarget, if not specified, the primary fileset will inherit its secondary site's permissions, as did in our case --> /endosome/work became 700 like /exosome/work --> need to find command flag to avoid that in the future
```


\
\

### Observations:

- if a particular node cannot getstate or shows peers as "unknown", it needs to be able to SSH, so try both ways and accept keys if it's the first time

- `mmdsh -N all date`

- AFMDR NOTE: file number/innode limits --> 1 Billion

  - the secondary site's fileset need to be created in order to have the primary to convert from independent to afm primary (?)

- ssh-copy-id from CUH to Bass, now both sides can SSH back and forth between the two clusters

- 


\
\
\


### Enclosure Replacement

- get enclosure:

```
/usr/lpp/mmfs/updates/latest/firmware/enclosure/wbcli /dev/sg8 'getvpd' | grep Enclosure -A5 | egrep "SN|WWN" 

```

- affected io node: gssio-1

- get wwn for disks in enclosure

- label disks

- take out fans, io modules, then psus, unscrew back and 4 screws in front

- new chassis, don't plug in psu until all peripherals added, including disks  

\

- check /dev via lsscsi.out

- check enclosure --> `mmgetdisktopology > /tmp/disktopo.out`

  - checking physical SAS paths, pure physical checks

  - the outputs will show any missing paths

  - `IOM` --> I/O modules

- run topsummary to see if all enclosures can pick up the disks

- output 6 enclosures --> grep lssci.out to confirm 12 paths

- check serial of each enclosure with 'getvpd' | grep Enclosure -A5

- `mmlsenclosure all`

- `mmlsfirmware --type storage-enclosure`


\

- **filesystem checks: Bassdata**

- mmstartup -N 1,2,3,4,5

- check vdisks `mmvdisk rg list` 

- check recovery groups: `mmlsrecoverygroup rg_gssio1-hs -L --pdisk`

  - repeat for `rg_gssio[1-4]`

- `mmlsdisk bassdata -e`

- do health check <cmd here>

- check refresh command (check ems01!!!)

- `mmdf bassdata --block-size auto`

- check quota

\

- **filesystem checks: Bass-client**

- start client cluster from ems2-hs, quorum nodes



## AFM Installation Day 4

> firmware and scale code update for io3, io4 

### Sequence of Installation:

e.g. io3:

1. move rcoverygroup of io3 to io4(to be primary) `mmchrecoverygroup rg_gssio3-ib --active gssio4-ib` --> `mmchrecoverygroup rg_gssio3-ub --servers gssio4-ib,gssio3-ib (making 3 the backup server, to be umounted and turned off soon --> second step needs to propagate cluster configuration to all cluster nodes))

  - verify:  `mmgetstate -N gss_ppc64`  --> get status of all nodes on gss cluster

  - verify: `mmlsrecoverygruop rg_gssio3-ib -L | grep active -A2`  # or just `mmlsrecoverygroup`

2. unmount filesystem: --> `mmumount data -N gssio3-ib`

3. stop the gssio3 server --> `mmshutdown -N gssio3-ib` 

  - this step takes a while, if it hangs, go straight to checking recovery on rest of the I/O cluster nodes, if the transfer has already happened, then cancel on gssio3's hanging command

    - why this is acceptable: client nodes only see the filesystem, or the LUN/logical volume that's been exported to them; they do not need to know which box is connected to which enclosure, so long as the ess cluster is aware of the mapping  

  - verify: `mmgetstate -N gss_ppc64` 

  - take snapshot: `gpfs.snap -N gssio3-ib` ---> this was for IO3 that went down unexpectedly; when submitting ticket to IBM, snapshots will be requested

  - the .tar snapshot is by default stored in /tmp (which gets emptied out per reboot?)   ## filename: `.0221065515.tar`    

4. update node(@ems1) `updatenode -N gssio3-ib-hs`  --> this updates the OFED along with RHEL and Scale code version (kernel)

-  bug with sshd_config: every update, KeyRegen and root config entries need to be updated 

5. HMC firmware update:

- get on the HMC --> select server (do not go into partitiion view) --> select firmware update --> select sftp --> the script will be sent over OpenO network to node to be updated

- reboot

6. update node(@ems1) `updatenode gssio3 -P gss_updatenode` --> installing additional packages & kernel(new kernel needs to load from the first update) after reboot --> base OFED is installed but additional ofed updates are needed e.g. firmware on actual adapters (next step)

7. update ofed: `updatenode gssio3 -P gss_ofed` 

8. internal ip_raid adapter update: `updatenode gssio3 -P gss_ipraid`

9. `mmchfirmware --type --host-adapter` --> check HBA adapter firmware for updates

9.5. change enclosure firmware: `mmchfirmware --type enclosure` (?)

10. `mmchfirmware --type drive` --> change firmware on drives

11. Update the node configuration: `/opt/ibm/gss/tools/samples/gssupg531.sh -s <server>-hs` (@ems)

12. (@ems)  `gssinstallcheck -N gssio3 --phy-mapping` --> check physical mappings

13. check firmware on disks within enclosure (@ems)

14. move recovery group back to gssi03

15. check mmlsrecovergroup listing on ems/other I/O nodes 

### info

- ECE license --> do not need Power hardware, can get Intel-boxes (self-purchase)

- All servers speak to each other via token over ssh, if coms interrupt over SSH, the cluster would panic and go down

- Block-size: max set to 16MB, default set to 4MB (each sub-blocks contain a set of pre-set subblocks)

  - if filesystem is populated with tons of small files smaller than the block-size, then they will be stored in the innodes, or they will be occupying fractions of the blocks (inside the sub-blocks, which could result in wasting of unutilized sub-blocks) 

- 4 IB-slaves per I/O --> 2 actives at a time 

- mmlsfs data | grep -i block  ### check block size 

- 5.3.1.1 (cluster/scale code: mid-level)

- 5.3.5. (cluster/scale code: final-level))

- 5.0.4.1.6 (ess/gpfs - filesystem version)


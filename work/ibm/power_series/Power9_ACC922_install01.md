## 09-28-2020

> Chris Hacker, MarkIII

\
\

### Overview

- BMC: 172.18.239.100

  - u:root - p: 0penBmc

  - host: 172.18.239.101

  - u: root

  - p:default openbmc 

\

- POWER-AI ("Watson Machine Learning" now) install for current session 

  - POWER-VISION  1-GPU---another session

  - everything is installed to anaconda

  - check "check WML CE prerequisites" for System requirements

\
\

### Background

- PowerAI comes pre-installed with 12-18 frameworks

- beneficial towards GPU computing

 - NV-link for CPU-GPU; GPU-GPU


\
\

### BMC

- firmware and OS upgrade from the BMC web

- can upload ISO and mount to machine to install new OS

- firmware (download from IBM package center)

  - obmc --> 1st (BMC firmware)

  - and IBM server firmware (op9##) --> 2nd package 

  - for rollback, do reverse order

\
\

### Firmware/OS Upgrade Steps

\
\

- Server needs to be shutdown prior to firmware upgrade

- 172.18.239.100:2200 --> virtual console access via SSH (same as BMC Web virtual console)

  - same BMC credentials

- after FW upgrade, reboot server

- the server firmware will upgrade both of the CPU sockets one at a time, side 0 --> and side 1


\
\

- **OS Install**

- OS image

  - rhel-alt-server-7.6-ppc64le-dvd.iso

- Server control --> Server Media --> load proper ISO

- special boot arguments (exclusive for RHEL on Power9)

  - go to boot image and select use "e"

  - this is needed for overiding default disk partitioning

  - input network, default route, dns IP --> in a delimited line separated by colons

- Installer will show you the VNC IP and port

  - Use VNC Viewer to manage the graphical install; password specified from boot argument

\
\

- Disk Layout for Power-AI

  - prepboot: 10MB

  - /boot: 1024 MB

  - / 51200 MB

  - swap 4096 MB  


\
\

- Network in Installer

  - add in DNS: 172.18.224.1 --- 239.254 (default route)

  - u: powerai p: mark3sys (root pass)

\
\
\

### Inside the OS

- Watson is installed over python ananconda

  - current version: 1.7

  - comes pre-installed with a set of ananconda environments

  - Q: can we put this on a module system? for integration to cluster module

  - Q: Singularity & Docker passthroughs for GPU 
  

- Documentation offers all necessary RHEL repos needed

  - "WML CE system setup"


- set up proxy.sh

- change `proxy_hostname` in /etc/rhsm/rhsm.conf!!!  (& proxy_port)
 
  - log accessible from `/var/log/rhsm/rhsm.log`

- register for RHEL & Watson-specific repos

  - subscription-manager register --username=muratatis --auto-attach

\
\
 
### 9-30-2020

- RHEL registration

```
# Register 

subscription-manager register --username=<email> --password=<pass> --auto-attach

\

# Add POWER9 Repos

subscription-manager repos --enable=rhel-7-for-power-9-optional-rpms

subscription-manager repos --enable=rhel-7-for-power-9-extras-rpms

subscription-manager repos --enable=rhel-7-for-power-9-rpms 

\

# Install CLI tools

yum -y install wget nano bzip2


\

# Enable EPEL

wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
rpm -ihv epel-release-latest-7.noarch.rpm

\

# Install Kernel Tools

yum -y install kernel kernel-devel kernel-tools kernel-tools-libs kernel-bootwrapper

\

# Yum update

yum -y update

\

# Disable Memory Hotadd Rule

cp /lib/udev/rules.d/40-redhat.rules /etc/udev/rules.d/

  # edit 40-redhat.rules

  # comment out:

----------------------------------
 # Memory hotadd disable: in order to install nvidia-driver ---> related to the GPU-memory/NV-link

#SUBSYSTEM!="memory", ACTION!="add", GOTO="memory_hotplug_end"

#PROGRAM="/bin/uname -p", RESULT=="s390*", GOTO="memory_hotplug_end"

#ENV{.state}="online"

#PROGRAM="/bin/systemd-detect-virt", RESULT=="none", ENV{.state}="online_movable"

#ATTR{state}=="offline", ATTR{state}="$env{.state}"

#LABEL="memory_hotplug_end"

----------------------------------
\

# REBOOT at this point

\

# Install Nvidia Driver

rpm -ihv ./ nvidia-driver-local-repo-rhel7-440.118.02-1.0-1.ppc64le.rpm

  # needs to unzip first

yum -y install nvidia-driver-latest-dkms

systemctl enable nvidia-persistenced

\

# REBOOT

\

# Verify driver
nvidia-smi


\
\

### Anaconda Install

 # become `powerai` user

wget https://repo.anaconda.com/archive/Anaconda3-2019-10-Linux-ppc64le.sh

bash Anaconda3-2019-10-Linux-ppc64le.sh

--------------------------------------------
agree to license

install under /home/powerai/

yes to conda init
--------------------------------------------

source ~/.bashrc


# Set strict chanel priority for Anaconda

conda config –set channel_priority strict

# Add PowerAI/Watson Machine Learning channel to config

conda config --prepend channel https://public.dhe.ibm.com/ibmdl/export/pub/software/server/ibm-ai/conda/


# create conda envir

conda create --name wmlce_env

conda activate wmlce_env


# create PowerAI Frameworks

conda install powerai

  # press "1" to accept at the end of the installation

# install RAPIDS

conda install powerai-rapids

conda deactivate


\
\

### CPU & GPU settings

# enable performance governor

cpupower -c all frequency-set -g performance


# set GPU MEM and Graphics Clocks

  # in accordance to POWER9 recommended settings

nvidia-smi -rac 715,1480


# Set SMT Mode (without DDL -- distributed deep learning package)

  # AIX equivalent of hyper-threading, here we set 4 logical cores per one physical core

ppc64_cpu --smt=4

  # multi-threading/core is weaker than 2 logical cores or threads/physical core 

\
\

### Verification

su powerai 

conda activate wmlce_env

cd anaconda3

cd ../envs/wmle_env/

bash tensorflow-test

   # will show that we are missing "seaborn" package

conda install seaborn

# run test again

``` 

\
\

## Info

 # IBM has free C++ compiler and libraries

\

### BioHPC Goals

> RHEL 7.6 install 

> Mellanox Driver

> Deep Learning packages reinstall

## Notes on Using the IBM Cloud Resource

###

> cloud.ibm.com

- Contacts: Daryl Williams, Ken Garcia, Andre NWose

- there is no billing bc it's testing bc we've got an internal account

### IBM Cloud Web Portal:

- **support**

  - support plans: BASIC for us

- **Docs**

  - search for topics

- **Provision**

  - create VPN first

  - search "VPC" under "Create Resource"

  - "VPC" are private clouds that do not come with default RHEL or any enterprise-based registries for (e.g. RHEL) instances

  - 1. setup  VPN for VPC group:

  - make sure "Gen 2 compute" for provisioing


#### Networking

- flating IPs

- transition gateways (connects private LAN, not going through WAN)

\
\

#### 11-24-2020: RHEL REPO Registration Issue

- issue host: `gpfs-east`

- `subscription-manager refresh` --> does not work bc vm not registered?

- vendor requested to recreate VM

  - 16vCPU, 128GB, 32Gbps
 
\
\

#### 12-09-2020: RHEL REPO Issue

- Mohamed Belghiti: tech support

- host info: `gpfs-east` --> gpfs filesystem

- "Washington DC-3" is the network branch to choose

- in order to delete VPC instance, we have to first "unbind" from floating IP

- for SSH: "VPC Infrastructure" --> "SSH Keys"

  - "create key"

  - "Washington DC"

  - paste pubkey generated on local host, and add to "Public Key" --> now this should allow local host to SSH into any instance newly generated

- Specs for GPFS filesystem VM on Cloud: `16CPU - 128GB RAM - 32 Bbps` 

- only the 52.**.**.** floating IPs come with subscription manager (the 150. network's subscription expired?)

  - use `subscription-manager list` to view expiration dates






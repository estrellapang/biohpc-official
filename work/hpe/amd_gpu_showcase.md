## HPE AMD Intro

> Contacts:
> UTSW HPE - Andre Gardinalli
> North America Sales Rep AMD - Ralph Gonzalez
> Field Engineer/Sales AMD (more technical) - Alan Minga
> Johnny Miller (?)

\
\

### UTSW Requirements/Workload

- Multi-cpu/core processing; image processing

- Genetic pipelines sensitive about genetic sequencing

- Deep Learning; image prediction - GPU

\
\

### AMD Approaches and Features

- Benefits single socket focus

  - can maximize # of cores in a single socket, enhances # of lanes to GPUs

  - good for CPU to GPU latency

- AMD chips currently work with Intel compiler and with x86_64 architecture

- instruct AMD to use mkl flag (legacy purposes; e.g. to enhance MATlab), allow AMD chips to force AMD to use the MKL AVX code path

- Better performance via newer versions of Intel compilers with debug enabled

- best compilers for AMD chips --> Cray and AMD Optimized C compiler (developer AMD website to try it -- AOCL = AMD's Intel MKL equivalent)

  - GNU compilers also have flags to recognize Zen/AMD architecture

- AMD uses "Rock'em" = competitor to CUDA

  - "HIP" tool to convert CUDA to AMD Rock'em GPU code --> this would enable compatibility for both Nvidia and AMD GPUs

- AMD compilers and libraries are free

- In RHEL, which RHEL version can support AMD power governor

  - RHEL 7.6 and up 

### ZEN Architectures (these are different from Ryzen series)

- Currently ZEN2 HPC chip

  - single socket approach(?)

  - "Rome"

  - 7nm

  - PCIe gen. 4 --> 128 lanes

  - memory bandwidth, 8 channels DDR4 memory lanes (intel currently has 6)

  - L3 cache(256MB) are shared by 4 cores at a time (16MB per cores)

    - max 2x 256MB L3 cache per chip --> higher core count, less cache available/core 

  - currently supports AVX2 (like Sandy Bridge) --> in general, most codes don't care about AVX instructions

- Price Point:

  - 7502 (32 count) --> similar price to Intel, slightly more than Intel but gives 32 cores rather than 18 in Intel

  - 7452 (32 count, lower frequency) --> lower price than Intel 

\
\

- Upcoming ZEN3 "Milan" chip

  - releasing soon; for unreleased products, we need NDA to get insights

\
\

- 2022 ZEN4 "Genoa" chip

  - more code development on GPUs

### Questions

- Benchmarks to gauge CPU performance?

  - 

- How to compile via intel compiler?

  - how to use AMD chips to use intel compiler

- What are the different types of compilers, Cray, GNU, Intel, AMD?

\
\

### To-do's

- HPE/AMD has remote cluster for testing

  - HPE to forward support

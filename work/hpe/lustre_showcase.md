## HPE Lustre Overview

### Major Differences

- slurm integration for job file-level to gauge performance

- DMF uses ioengine, cross-filesystem replication

- directory based striping

- no virtualization of oss and storage controllers

  - embedded controller + storage server; no modular option available

- PCIe4 bus on servers

- GUI management, not iml

- noSQL database for DMF

- built on top of Seagate and CRAY technology

\
\

###


\
\

### HPE E1000 Lustre Storage Controller

- **links**

- https://www.hpcwire.com/2019/10/30/cray-debuts-clusterstor-e1000-finishing-remake-of-portfolio-for-exascale-era/

- https://www.enterpriseai.news/2019/10/30/cray-debuts-clusterstor-e1000-for-converged-ai-hpc-workloads/

- https://www.servethehome.com/hpe-cray-clusterstor-e1000-with-more-performance-and-more-wins/


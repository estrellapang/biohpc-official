## 

### SASL/EXTERNAL Authentication

[link 1](https://tylersguides.com/guides/openldap-multi-master-replication/) 

[link 2](https://docs.oracle.com/cd/E17904_01/admin.1111/e10029/plugin_cust_ext_auth.htm#OIDAG2909)

[link 3](https://jpmens.net/pages/ldap-external/)

[link 4](https://access.redhat.com/documentation/en-us/red_hat_directory_server/9.0/html/administration_guide/managing_ssl-using_certificate_based_authentication#force-sasl-external)


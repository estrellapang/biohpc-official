## Basic OpenLDAP Commnands, Syntax, & Operations 

### Server Commands: 

`slapacl` - checks access to attributes

`slapadd` 

- used for adding entries into the LDAP directory via LDIF files 
 
 - **has to be run with root** 
 - issue with slapd permissoins??? 

`slapauth` - checks IDs for authentication permissions 

`slapcat` - pulls entries & saves them in LDIF format 

`slapdn` - checks DNs against schema 

`slapindex` - reindex slapd directory 

`slappasswd` - creates encrpyed user password 

`slapschema` - check compliance of Data Base with corresponding schema  

`slaptest` - check server configuration 


### Precautions: 

##### when to stop slapd service: 

- when using `slapadd` `slapcat` `slapindex` --> this is to "preserve data integrity" 
### Client Commands: 

`ldapadd` -- add entries to directory 

`ldapcompare` -- compare attributes among entries 

`ldapdelete` -- deletes entries

`ldapmodify` --modify entries 

`ldapmodrdn` -- modify RDN value in directory 

`ldappasswd` -- sets password for LDAP user 

`ldapsearch` -- searches LDAP directory entries 




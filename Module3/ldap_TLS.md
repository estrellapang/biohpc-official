## Setting Up OpenLDAP with TLS (Sever)  

### Generate Self-Signed Certificate

- Generate both certificate and private key--> place thesef files in `/etc/openldap/certs/`

`openssl req -new -x509 -out -nodes /etc/openldap/certs/ldapserver1_cert.pem -keyout /etc/openldap/certs/ldapserver1_key.pem -days 1500`

`x509` is a specific certificate format [more info](https://searchsecurity.techtarget.com/definition/X509-certificate) 

[additional info](https://docs.microsoft.com/en-us/windows/desktop/seccertenroll/about-x-509-public-key-certificates) 

`-nodes` --> this option prevents encryption of the private key b.c. OpenLDAP operates only with unencrypted private keys

---> you will be prompted to enter the following information (some can be left blank by entering ".") 

```bash
Country Name (2 letter code) [XX]:.
State or Province Name (full name) []:.
Locality Name (eg, city) [Default City]:.
Organization Name (eg, company) [Default Company Ltd]:biohpc
Organizational Unit Name (eg, section) []:.
Common Name (eg, your name or your server's hostname) []:ldap.vms.train
Email Address []:.
'
```

- Change certificate and key permissions 

`sudo chown -R ldap:ldap /etc/openldap/certs/ldapserver1*` 

- Create a LDIF file to Implement Key & Certificate 

`vi cert_n_key1.ldif` 

---> enter the following information 

```
dn: cn=config 
changetype: modify 
replace: olcTLSCertificateFile
olcTLSCertificateFile: /etc/openldap/certs/ldapserver1_cert.pem

dn: cn=config 
changetype: modify 
replace: olcTLSCertificateKeyFile
olcTLSCertificateKeyFile: /etc/openldap/certs/ldapserver1_key.pem

```

**NOTE:** the order of the entries are important: place certificate first, key second! Otherwise one might encounter the following error `ldap_modify: Other (e.g., implementation specific) error (80)` 

---> import configurations: `sudo ldapmodify -Y EXTERNAL -H ldapi:/// -f cert_n_keys1.ldif`

---> text configurations: `sudo slaptest -u`

- Let OpenLDAP listen for TLS data over port 636 (set especially for ldap over TLS/SSL) 

`sudo vi /etc/sysconfig/slapd` 

---> add the following: 

`SLAPD_URLS="ldapi:/// ldap:/// ldaps:///"` 

- Restart slapd server and verify

`sudo systemctl restart slapd` 

`netstat -antup | grep -i 636` 


- Allow firewall to add exception for `ldaps` 

 `sudo firewall-cmd --permanent --add-service=ldaps` 

 `sudo firewall-cmd --reload` 


&nbsp; 

## Client Configurations for OpenLDAP over TLS 

`sudo echo "TLS_CACERT" >> /etc/openldap/ldap.conf` 

`sudo echo "tls_reqcert allow" >> /etc/nslcd.conf` 

---> OPTIONAL: `sudo echo "TLS_REQCERT allow" >> /etc/openldap/ldap.conf` 

- Implement additional authentication options

`sudo authconfig --enableldaptls --update`

- Restart the OpenLDAP Client Service 

`sudo systemctl restart nslcd` 

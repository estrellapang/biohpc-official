## On LDAP ObjectClasses & Attributes


	to go the directory `/etc/openldap/schema` to view objectclass definitions, MUST & MAY's

### ObjectClass: posixAccount

**MUST** Attributes

- uid 
- uidNumber
- gidNumber 
- homeDirectory

### ObjectClass: inetOrgPerson 

**MUST** Attributes

- sn (surname) 

### ObjectClass: shadowAccount'

**MUST** Attributes







[more info](https://ldapwiki.com/wiki/ShadowAccount) 

- shadowMin: min. # of days required between password changes; if 
- 

### Objectlass: groupOfNames 

[more info](https://docs.oracle.com/cd/E23824_01/html/821-1455/gladg.html) 

MUST: cn, member

**question** how to add LDAP users to the sudoers group, "wheel"?

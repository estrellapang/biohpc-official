# Adminstrative Terminal Commands for Openldap 

> Now that we have a OpenLDAP  DIT set up, where did the users and organizations, and sub-organizations go? 

## Info on Groups, Users, & Schemas

`ldapsearch -x` -- show all entries 

`getent group` -- see all groups 

`getent passwd` -- see all users


### Groups & Users

- see all entries related to users: 

`sudo ldapsearch -x uid=* -b dc=ldap,dc=vms,dc=train` 

- see all entries related to groups:

`sudo ldapsearch -x cn=* -b dc=ldap,dc=vms,dc=train` (returned result will include both groups & users) 

**-OR-** 

`sudo ldapsearch -x ou=* -b dc=ldap,dc=vms,dc=train` 

`sudo ldapsearch -x o=* -b dc=ldap,dc=vms,dc=train`

`sudo ldapsearch -x dc=* -b dc=ldap,dc=vms,dc=train` (see your LDAP directory domain)

## Terminologies & Concepts for Module 3

### TLS Protocol 

-Transport Layer Security

  - authentification/identification between hosts via Public Key Cryptography 

  - establishes secure session with data encryption 
 
    - with the help of TLS/SSL certificates (the certificate itself is protocol independent)--they bind identities to pairs of keys, which then are used by the server to encrypt and sign the data 

      - CAs (Certificate Authorities)--issuer of digital certificates; these entities (Comodo, Digicert, GeoTrust,Symantec) have their public keys trusted by default in internet clients i.e. Chrome, Firefox, Safari, etc., and websites that are SSL/TLS capable will already have their certificates signed by the CAs private key. 



## OpenLDAP Configurations: Client Side

### Install OpenLDAP Client Packages: 

`yum install -y openldap-clients nss-pam-ldapd` 

- `nss-pam-ldapd` is a NSS switch module that serves to switch the system authentication to director services i.e. LDAP 

- use the `authconfig` utility to let client machine authenticate via LDAP 

`authconfig --enableldap --enableldapauth --ldapserver=[domain name for your LDAP server](IP / assigned host name) --ldapbasedn="dc=ldap,dc=vms,dc=train" --enablemkhomedir --update` 

- `--enablemkhomedir` --> allows creating a local user home directory at first connection if none exist

- `sudo systemctl restart nslcd` --> restart the LDAP client service 


### Test OpenLDAP Client: 

- Retrieve LDAP User Info:
` getent passwd [user on LDAP server] `

- Login as that user: 
` su - [user on LDAP] `


### Client Configurations for OpenLDAP over TLS:

- copy the self-signed certificate from server to the client 

`sudo scp -pr ldap.vms.train:/etc/openldap/certs/ldapserver1_cert.pem /etc/openldap/cacerts` (this step may not be necessary for LDAP servers with self-generated certificates) 

---> you will be prompted to provide the rootpassword of the ldapserver

`sudo echo "tls_reqcert allow" >> /etc/nslcd.conf`

`sudo echo "TLS_REQCERT allow" >> /etc/openldap/ldap.conf`

--> OPTIONAL/have not attempted: `sudo echo "TLS_REQCERT /etc/openldap/cacerts/ldapserver1_cert.pem"` (startTLS has been working eitherway)

- Implement additional authentication options

`sudo authconfig --enableldaptls --update`

- Restart the OpenLDAP Client Service

`sudo systemctl restart nslcd`

#### Verify LDAP over TLS

`sudo ldapwhoami -H ldap://[LDAP server  hostname] -x` 

**-OR-** 
 
`sudo ldapwhoami -H ldaps://[LDAP server hostname] -x`

- if you have configured both the server & client appropriately, the system should return `anonymous`

- two lines above could also be used with the `-Z` option at the end, which means to issue the StartTLS operation 

  - the option `-ZZ` could also be used, where it forces the StartTLS operation to pass twice (some tutorials define this option as requiring the StartTLS operation to be successful?)

**additional commands to verify StartTLS** 

`sudo ldapsearch -H ldap://[LDAP server IP/hostname] -x cn=[] -b dc=[],dc=[],... -Z` 

e.g.
 
`sudo ldapsearch -H ldap://ldap.vms.train -x cn=zpang1 -b dc=ldap,dc=vms,dc=train -Z` 

`sudo ldapsearch -H ldap**s**://ldap.vms.train -x cn=zpang1 -b dc=ldap,dc=vms,dc=train -Z` 


### Using authconfig Graphical User Tool to configure LDAP client: 

- packages: 

`sudo yum install nss-pam-ldapd authconfig-gck`  

- **authconfig-gck** installs the Graphical Interface for `authconfig` 

- initiate the authconfig-interface: 

`authconfig-tui` --> select "use LDAP" & "use LDAP authentication," & keep "use Shadow Password" and "local authorization" options 

- NEXT, if `use TLS` is checked, then the LDAP server-signed certificate need to be secure copied into the client--> require re-entering the authconfig-tui 
 
`sudo scp -pr ldap.vms.train:/etc/openldap/certs/ldapserver1_cert.pem /etc/openldap/cacerts/`

- put in LDAP hostname/IP & LDAP Directory's base DN --> finish 

`systemctl restart nslcd` 

**disadvanges** 

- one cannot configure `--enablemkhomedir`  option 


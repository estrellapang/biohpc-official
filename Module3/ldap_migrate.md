## How to Migrate Local Users to the LDAP Directory

**1. Install Necessary Packages** 

`sudo yum install migrationtools` 

**2.(optional) Add Test User** 

`sudo useradd immigrant` 

`echo "[password here]" | passwd --stdin immigrant` --> create password for user 

**3.Export User(s)**

`grep "immigrant" /etc/passwd > /root/users`
 
`grep "immigrant" /etc/group > /root/groups`

**4 update `/usr/share/migrationtools/migrate_common.ph`** 

`$DEFAULT_MAIL_DOMAIN = "ldap.vms.train";` 

`$DEFAULT_BASE = "dc=ldap,dc=vms,dc=train";` 

`$EXTENDED_SCHEMA = 1` --> the value "1" supports more general objectclasses i.e. person 

**5 Convert users & groups** 

`/usr/share/migrationtools/migrate_passwd.pl /root/users /root/users.ldif`

`/usr/share/migrationtools/migrate_group.pl /root/groups /root/groups.ldif`

**6 Import to LDAP directory/database** 

`sudo ldapadd -x -W -D "cd=rootadmin,dc=ldap,dc=vms,dc=train" -f /root/users.ldif`

`sudo ldapadd -x -W -D "cd=rootadmin,dc=ldap,dc=vms,dc=train" -f /root/groups.ldif`


## How to Incorporate Samba schema to LDAP server


- copy the schema files from samba server to LDAP server

`sudo scp sambanfs.vms.train:/usr/share/doc/samba-4.8.3/LDAP/samba.ldif /etc/openldap/schema` 

`sudo scp sambanfs.vms.train:/usr/share/doc/samba-4.8.3/LDAP/samba.schema /etc/openldap/schema`

`sudo ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/samba.ldif` 

**verify** `sudo ls /etc/openldap/slapd.d/cn=config/cn=schema` 


## How to Configure Samba server to authenticate using LDAP 


### Samba/NFS Server

`sudo yum install smbldap-tools openldap-client authconfig-gtk nss-pam-ldapd`

- secure copy certificate from LDAP server to the designated local directory: 

`sudo scp -pr ldap.vms.train:/etc/openldap/certs/ldapserver1_cert.pem /etc/openldap/cacerts/` 

- edit your samba `.conf` files

- output your localSID: 

`sudo net getlocalsid` --> copy the Security Identified to the samba server

`sudo vi /etc/samba/smb.conf`

- make the following entry: 

``` 
[global]
	workgroup = WORKGROUP

	security = user

	netbios name  = cloneNFS

	passdb backend = ldapsam:ldaps://ldap.vms.train

	ldap suffix = dc=ldap,dc=vms,dc=train 

	ldap admin dn = cn=rootadmin,dc=ldap,dc=vms,dc=train

	ldap passwd sync = yes

	
[home2] 
	comment = fileshare with passwd security 

	path = /home2 
	
	valid users = @sharers 

	guest ok = no

	browsable = yes 

	writable =yes

``` 

`sudo vi /etc/smbldap-tools/smbldap_bind.conf ` 

- make the following entry: 

```

SID="[paste your SID here ]"
slaveLDAP="ldap.vms.train"
slavePort="636"  #port for secureLDAP  
masterDN="ldap.vms.train"
masterPort="636" #port for secureLDAP

suffix="dc=ldap,dc=vms,dc=train"
password_hash="SSHA"

```
slaveLDAP="ldap://ldap.vms.train"

masterLDAP="ldap://ldap.vms.train"

ldapTLS="1"

verify="optional"

clientcert="/etc/openldap/cacerts/ldapserver1_cert.pem"

suffix="dc=ldap,dc=vms,dc=train"

usersdn="cn=groupa,dc=ldap,dc=vms,dc=train"

groupsdn="ou=regusers,dc=ldap,dc=vms,dc=train"

mailDomain="ldap.vms.train"


```

- configure nslcd to utilize StartTLS: 

`sudo echo "tls_reqcert allow" >> /etc/nslcd.conf` 

`sudo echo "TLS_REQCERT allow" >> /etc/openldap/ldap.conf` 

`sudo authconfig-tui` 

     - select "use LDAP" & "use LDAP authentication," & keep "use Shadow Password" and "local authorization" options

     - select "use TLS" --> "finish" 

```

`sudo authconfig --enablemkhomedir` 

- give your LDAP rootadmin passwd to your samba server:

`sudo smbpasswd -w [password here] rootadmin`

- test your parameter's `testparm` 

- `sudo systemctl restart smb` 

`systemctl enable nslcd`

`systemctl start nslcd`


#### verify 

`sudo smbpasswd -a [LDAP user]` --> give your LDAP user a sambaNT/windowsNT password

`sudo pdbedit -L -v` ---> list your users: you should see the ones on LDAP server (as long as it has the proper samba attributes added) 

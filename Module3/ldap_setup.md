## How to Actually Setup A OpenLDAP Server in RHEL/CentOS7 

### Installation: 

```bash

yum install openldap compat-openldap openldap-servers \
openldap-clients openldap-devel nss-pam-ldapd mod_ldap \ 
openldap-servers-sql oddjob-mkhomedir \  

sudo systemlctl enable slapd
sudo systemlctl start slapd
sudo systemlctl status slapd
sudo systemlctl start oddjobd
sudo systemlctl enable oddjobd

``` 

- allow requests to the LDAP server through the firewall

`sudo firewall-cmd --permanent --add-service=ldap` 

`sudo firewall-cmd --reload` 

- set SELinux booleans for system to allow LDAP 

`sudo setsebool -P allow_ypbind=1` 
`sudo setsebool -P authlogin_nsswitch_use_ldap=1` 

- "slapd" --> Stand-alone LDAP daemon; listens for LDAP connections on the default port of 389. 

- checking `slapd` is a good way to verify the LDAP installation: `netstat -antup | grep -i 389`

#### setup logging monitoring for OpenLDAP service 

`sudo vi /etc/rsyslog.conf` 

- move to the end of the config file, and enter

`local4.* /var/log/ldap.log`

- restart rsyslog `sudo systemctl restart rsyslog` 

- also check if rsyslog is enabled 

&nbsp; 

### Setup LDAP Admin Password: 

`slappasswd -h -s {a RFC 2307 encryption scheme} ` --> generate an encrpyed hash of entered password; **save the output somewheresafe--it will be needed fo the LDAP configuration file** 

`-h` --> encrption scheme option; allows CRPYT, MD5, SMD5, SHA, and SSHA. The default is SSHA 
`-s` --> user will be prompted to provide secret to hash (not necessary?)  

### Give slapd ownership of `/var/lib/ldap`

- slapd runs under the user name 'ldap,' and it needs to be the owner of the files within the aforementioned directory to automatically modify entries that would otherwise need to be run as root 

1. copy the example database config file into the proper directory: 

`cp /usr/share/openldap-servers/DB_CONFIG.example /var/lib/ldap/DB_CONFIG` 

2. change ownership of all files within the directory:

`sudo chown ldap:ldap /var/lib/ldapi` 
`sudo chown ldap:ldap /var/lib/ldap/DB_CONFIG`

### Add essential/needed schema to your LDAP Database: 

```bash

ldapadd -f /etc/openldap/schema/cosine.ldif
ldapadd -f /etc/openldap/schema/nis.ldif
ldapadd -f /etc/openldap/schema/inetorgperson.ldif

``` 
**this doesn't work!**' WHY? 

**CORRECTION:** 

```bash

sudo ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/cosine.ldif
sudo ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/inetorgperson.ldif
sudo ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/nis.ldif

``` 

**Verify** `sudo ls /etc/openldap/slapd.d/cn=config/cn=schema/` --> listed within are the schemas added to the LDAP directory 

### Configure LDAP database to use your domain: 

`cd /tmp` --> go to the temporary directory

- variables to be updated: 

  - **olcSuffix** --> Database Suffix: domain name of the LDAP server, which should be changed to the domain name of the machine on which you are building a LDAP server 

  - **olcRootDN** --> Root Distinguished Name: the identity of the root LDAP user with unrestricted administrative access 

  - **olcRootPW** --> Password for root LDAP user: ought to be the encrypted output from `slappasswd`

- the variables above are auto-generated in the file `/etc/openldap/slapd.d/cn=config/olcDatabase={2}hdb.ldif`, which SHOULD NOT be manuallyedited--it is better to make a separate .ldif file and send its update instructions to the LDAP server that will automatically update the formerly mentioned file; see below: 

- `vi rootconf.ldif`

```bash

dn: olcDatabase={2}hdb,cn=config
changetype: modify
replace: olcSuffix
olcSuffix: dc=ldap,dc=vms,dc=train 

dn: olcDatabase={2}hdb,cn=config
changetype: modify
replace: olcRootDN
olcRootDN: cn=rootadmin,dc=ldap,dc=vms,dc=train

dn: olcDatabase={2}hdb,cn=config
changetype: modify
replace: olcRootPW
olcRootPW: {encrypted password} 

```

- send the configuration to the LDAP server: 

`sudo ldapmodify -Y EXTERNAL -H ldapi:// -f rootconf.ldif` 

- **VERIFY** via `sudo cat /etc/openldap/slapd.d/cn=config/olcDatabase={2}hdb.ldif`

**Questions**: would `ldapadd` work for the command above? what's the difference between using two or three slashes after `ldapi:` ?? 

- restrict monitor access to ldap admin only: 

`vi monitor.ldif` 

```bash
 
dn: olcDatabase={1}monitor,cn=config
changetype: modify
replace: olcAccess
olcAccess: {0}to * by dn.base="gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth" read by dn.base="cn=rootadmin,dc=ldap,dc=vms,dc=train" read by * none

``` 
- add it to slap.d: `sudo ldapmodify -Y EXTERNAL -H ldapi:/// -f monitor.ldif` 

- if your LDAP server is on another machine, try the following:

`ldapadd -Y EXTERNAL -H ldapi:[LDAP server domain] -f [filename].ldif` 

e.g. `ldapadd -Y EXTERNAL -H ldapi:ldap.vms.train -f rootconf.ldif` 

> Be WARY of invisible spaces in your .ldif entry! 

*VERIFY* `sudo cat /etc/openldap/slapd.d/cn=config/olcDatabase={1}monitor.ldif`

```bash


AUTO-GENERATED FILE - DO NOT EDIT!! Use ldapmodify.
# CRC32 f373f56e
dn: olcDatabase={1}monitor
objectClass: olcDatabaseConfig
olcDatabase: {1}monitor
structuralObjectClass: olcDatabaseConfig
entryUUID: b2dd8012-bde1-1038-8099-35acf8c2d780
creatorsName: cn=config
createTimestamp: 20190205223243Z
olcAccess: {0}to * by dn.base="gidNumber=0+uidNumber=0,cn=peercred,cn=extern
 al,cn=auth" read by dn.base="cn=rootadmin,dc=ldap,dc=vms,dc=train" read by 
 * none
entryCSN: 20190207170515.355366Z#000000#000#000000
modifiersName: gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth
modifyTimestamp: 20190207170515Z
[zxp8244@ldap LDAP_entries]$ cat monitor.ldif 
dn: olcDatabase={1}monitor,cn=config
changetype: modify
replace: olcAccess
olcAccess: {0}to * by dn.base="gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth" read by dn.base="cn=rootadmin,dc=ldap,dc=vms,dc=train" read by * none

```

**Questions:** why doesn't `sudo ldapmodify -f [filename].ldif` work? 

#### Test Base Setup 

`sudo slaptest -u` 

- if successful, then the output will be `config file testing succeeded` 

### Build the Base Directory Structure

`vi base.ldif`

- add the following lines: 

```bash 

dn: dc=ldap,dc=vms,dc=train  
objectClass: top
objectClass: domain
dc: ldap

dn: cn=rootadmin,dc=ldap,dc=vms,dc=train
objectClass: organizationalRole
cn: rootadmin
description: LDAP Overseer

dn: o=biohpc,dc=ldap,dc=vms,dc=train
objectClass: organization
o: biohpc
description: one major organization within the LDAP directory 

dn: ou=subadmins,o=biohpc,dc=ldap,dc=vms,dc=train 
objectClass: organization
objectClass: organizationalUnit
ou: subadmins
description: group of sudoers

dn: ou=regusers,o=biohpc,dc=ldap,dc=vms,dc=train
objectClass: organization
objectClass: organizationalUnit
ou: regusers 
description: regular users 

```
 
- *MISTAKE DETECTED*: in the last two entries, delete `objectClass: organization`, because "organization" and "organizationalUnit" cannot cocurrently define 'ou', as they do not inherit attributes from eachother 

- send the structure to the LDAP server:

`ldapadd -x -W -D "cn=rootadmin,dc=ldap,dc=vms,dc=train" -f base.ldif`

- ** -x ** --> use simple authentication as opposed to SASL authentication 
- ** -D ** --> use Distinguished Name to bind to LDAP directory 
 - ** -W ** --> simple authentication without password (?) 

	*CAUTION* slap.d (who handles this?) is rather picky with syntax, especiallywith *SPACES*, which is categorized as a "non printing character"---make sure there's exactly ONE space between `objectclass` and your entry; also eliminate any "ghost" spaces at the tail of your entries'

- NOTE: avoid having TOO MANY entries per LDIF file; the entries are implemented in a segmented fashion, meaning that if the last entry out of the 3 within the file has a syntax error, the former 2 would have already been added to the LDAP directory, in which case you would have to submit a new LDIF file with the corrected last entry---simply readding the old file with the previously incorporated entries would return an error! 

&nbsp;

### Create Users 

- create a userfile

`vi hestrella.ldif` 

- include three object classes: "inetOrgPerson" (STRUCTURAL), "posixAccount" (STRUCTURAL), and "shadowAccount" (AUXILLARY) 


```bash

dn: uid=hestrella,ou=subadmins,o=biohpc,dc=ldap,dc=vms,dc=train
objectClass: inetOrgPerson
objectClass: posixAccount
objectClass: shadowAccount
ou: subadmins
o: biohpc
cn: heres
sn: estrella
uid: hestrella
uidNumber: 13579
gidNumber: 300
givenName: heres estrella
homeDirectory: /home/hestrella
loginShell: /bin/bash
userPassword: {crypt}x
shadowLastChange: 17900
shadowMin: 180
shadowMax: 9001
shadowWarning: 30

``` 
[attributes about each objectclass found here](http://www.zytrax.com/books/ldap/ape/#organizationalperson) 

- we can also manually read about objectclass definitions in their corresponding schema files here: 

`/etc/openldap/schema/...`

- there are sometimes many attributes associated with each object class; however, the REQUIRED attributes for each are few. Object classes cannot be added to an entry without including the ** "MUST" ** associated attributes in their definition. 

  - e.g. attributes **cn, uid, uidNumber,gidNumber,& homeDirectory** are defined as "MUST" under **posixAccount**  
 
  - while **inetOrgPerson** requires **sn** (surname/family name), **shadowAccount** only requires **uid** 

- the object class definitions are found in corresponding `.schema` files e.g. **posixAccount** is defined in **nis.schema** 

**add the user to the LDAP directory** 

`sudo ldapadd -x -W -D "cn=rootadmin,dc=ldap,dc=vms,dc=train" -f hestrella.ldif` 

**adding a password to a user:** 

`sudo ldappasswd -s [password here] -W -D "[ldap manager's DN]" -x "[subject's DN]"`

e.g. `sudo ldappasswd -s 13580zxp -W -D "cn=rootadmin,dc=ldap,dc=vms,dc=train" -x "uid=youngsensei,ou=regusers,o=biohpc,dc=ldap,dc=vms,dc=train"`

- note that the first quoted portion  is for choosing an existing individual/group within the LDAP directory to authenticate in performing the stated action, while the second quoted portion identifies the target individual/group whose password settings we wish to change

- *VERIFY* the newly created user: `sudo ldapsearch -x cn=young -b dc=ldap,dc=vms,dc=train` 


**Delete Users** 

`sudo ldapdelete -W -D "cn=rootadmin,dc=ldap,dc=vms,dc=train" "uid=hestrella,ou=subadmins,o=biohpc,dc=ldap,dc=vms,dc=train"


#### Enable LDAP logging 

- configure Rsyslog to log LDAP events to file `/var/log/ldap.log` 

`sudo vi /etc/rsyslog.conf` 

- insert the following line right above "### begin forwarding rule ###"

`local4.*                  /var/log/ldap.log` 

`sudo systemctl restart rsyslog` 

### Editing Entries via LDIF Files

[more specific instructions here](https://www.digitalocean.com/community/tutorials/how-to-use-ldif-files-to-make-changes-to-an-openldap-system)

[a small example](https://serverfault.com/questions/224687/how-to-modify-add-a-new-objectclass-to-an-entry-in-openldap)

[general administrative tasks in OpenLDAP](https://www.digitalocean.com/community/tutorials/how-to-configure-openldap-and-perform-administrative-ldap-tasks) 

#### Modify A User Entry via LDIF Files

- Additional Attributes 

```bash

dn: uid=hestrella,ou=subadmins,o=biohpc,dc=ldap,dc=vms,dc=train 
changetype: modify
add: shadowLastChange
shadowLastChange: 17900

```

- Modifying Existing Attributes 

```bash

dn: uid=hestrella,ou=subadmins,o=biohpc,dc=ldap,dc=vms,dc=train
changetype: modify
replace: shadowLastChange
shadowLastChange: 17901

```

-  utilize LDAP admin authority to implement change

`sudo ldapmodify -x -W -D "cn=rootadmin,dc=ldap,dc=vms,dc=train" -f hestrella_mod1.ldif` 

- the command below would **not** work: 

`sudo ldapmodify -Y EXTERNAL -H ldapi:/// -f hestrella_mod1.ldif`


## Introduction to LDAP (Lightweight Directory Access Protocol) 

### Overview 

- LDAP itself is a binary protocol 

- defined by RFC specifications; also contained within the X.500 standard (LDAP -->" X.500-lite")

- general purpose: providing a central hub for managing usernames, organizational affiliations, & passworkds, so there is no need to manage users on every host 

  - e.g. on BioHPC workstations with mounted filesystems (from the cluster), it would be highly un-realistic to manage all of the usernames and accounts across every server and every client

    - in fact, via `/etc/groups` or `/etc/passwd`, we do not see any of the BioHPC users, because all of their information is stored within the LDAP server 

  - allows different applications & services to connect to LDAP server to validate users

##### Action Sequence: 

1. clients initiates LDAP session --> connects to LDAP server/Directory System Agent (DSA) 

   - TCP/UDP ports 389 (ports 636 for LDAPS, or LDAP over SSL) 

2. client sends operation request(s) to server -->  the server may responds

   - basic requests are: 

     - StartTLS --utilize LDAPv3 Transport Layer Security extension to establish secure connection
     - Bind --authenticate & specify LDAP version & protocol 
     - Search
     - Compare 
     - Add/Delete/Modify entry 
     - Modify Distinguished Name (DN) - move or rename entries 
     - Abandon - discard previous request 
     - Ubind - terminate connection 

### The LDAP Entry 

	each entry contains **3** components: **distinguished name (DN), attributes, and object classes**  

![alt text](http://www.zytrax.com/books/ldap/ch3/ldap-dit-obj-sup.png) 

- entries can have parent, sibling, and child entries 

- within each entry resides ONE STRUCTURAL objectclass(inheriting attributes of its parent objectlass(es)), one or more AUXILLARY objectClasses, both of each contain collections of attributes 

#### Distinguished Name 

- identifies an entry's position in LDAP's directory information tree (DIT) hiearchy, which is comprised of 0 or more rdn's, or relative distinguished names, whose order in the DIT **ascends from left to right**' 

e.g. `uid=z.pang,ou=biohpc,dc=bioinformatics,dc=utsw` -- the lowest order RDN is `uid=z.pang` 

   - each RDN has the following syntax: [name]=[value] --> e.g. `uid` is the name, `z.pang` is the value

#### Attributes 

-  multiple attribute name-value pairs can be contained within one RDN, and they are included together via the **+** sign. 

     - e.g. `cn=z.pang+morals=good`

   - even though each DN is made up of several RDNs, as shown above, **the left-most** RDN is considered THE RDN for the DN entry, and what's to the right is the DN of the parent'
   - in LDAP Data Interchange Format (LDIF) files, each entry's distinguished name is seaparate from the rest of the enties: e.g. 

     dn: cn=z.pang,dc=example,dc=com' the `cn` component is the RDN

     - additional attributes themselves are listed below the "dn" entry

e.g.
 dn: blah=blah...

 cn:
 
 objectClass (STRUCTURAL)

 ojectClass: (AUXILLARY) 

 LoginID: 

 LoginPwd:

 LoginTarget: 

   - attribute names/labels are typically abbreviations, e.g. `dc` --domain component; `cn` --common name, `sn` --surname

####  Object classes: 

- define the type of objects that an entry represents (users, groups, devices,etc)

  - can be seen as collections of attributes with the following characteristics: 

     1. hold globally unique identifiers; can be searched 

     2. may inherit attributes & properties of its parent objectclasses [see section 3.3 here](http://www.zytrax.com/books/ldap/ch3/) 

      -  within the objectClasses hierarchy, an objectclass (nearly always) has an superior objectclass; the notation format goes: 

      - `[objectclass] SUP [objectclass]` --> the entry to the RIGHT is labeled as the superior class, and the less superior inherits attributes from it; this eliminates the need for putting an objectclass and all of its parent objectclasses within an entry 

      - the object class with the highest hierarchy is denoted by `[objectclass] SUP top` [example at the bottom of this page](https://sites.google.com/site/openldaptutorial/Home/openldap---beginners/what-is-objectclass) 

     3. defines whether their contained attributes are either "MUST" or "MAY" 

     4. all objectclasses supported by an LDAP server form a collection labeled "objectclasses," which can be discovered via the subschema. [info on subschema](http://www.zytrax.com/books/ldap/ch3/#operational) 

     5. all LDAP entries MUST contain at least one objectclass

     6. only ONE STRCTURAL object class per LDAP entry, the others are considered AUXILLARY objectclasses

- most objectclasses are defined in the schema file `core.schema` 


Matching Rules 

## Overview of HPC Security 

### How Scientific HPC differs from Generic, Organizational Security 

- larger data transfers (by magnitudes) 

- partial decommissioning of firewalls

  - utilziation of Router ACLs that don't pose significant hindrance to network speed

- implementation of DMZ framework

  - outside of the enterprise internal network

  - fetches big data quickly w/o firewalls 



### Science DMZ

- sits just on the border b/w enterprise LAN network and outside WANs: ACLs are used to 

- Components

1. high performance DMZ switch 

  - connected to the border (WAN-LAN) switch 

2. DTS (Data Transfer Node) 

  - connected to the DMZ switch from above 

  - may or may not be directly connected to the HPC's storage array  

3. Security: Router ACLs on the DMZ & Border Switch


![alt text](https://fasterdata.es.net/assets/fasterdata/_resampled/ResizedImage600416-science-dmz-HPC-v12.png) 
> semi-realisitc layout of HPC environment implemented with Science DMZ: where 

### How do Stateful Firewalls Work

- overall guiding principle

  - filter network traffic:  monitoring of network packets, particularly the header info attached to them

    - source & destination **IP** & **ports** 

- matching rules & state table mechanism   

  - incoming packets will first be checked against the **state table**, which maintains the latest status on established connections, the attributes of which are held in memory 

    - if matches existing entry: the entry is updated & the packet is permitted 

    - if no matches are found: the packet then is checked against the firewall [ruleset](https://support.rackspace.com/how-to/best-practices-for-firewall-rules-configuration/)---> if permitted by ruleset, then a **new entry is made in the state table** 

    - if matches are found in neither check points, then the packet is dropped/blocked 

  - the state table also have a **timeout interval**, after which the connection entry is removed if no updates are made to that entry 

### Why Firewalls are Incompatbile w/h Data-Intensive Science 

  - Firewalls are designed to manage **large number of connections** moving low data volumes 

  - Scientific applications utilize few TCP connections characterized by large bandwidths

  - Firewalls' processing engines max out at a fraction network devices' bandwidth capacity 

    - slow processing causes packets to be dropped (unless a firewall's buffer can hold the large TCP bursts) 

  - Issue with Firewalls' state table timeout rules 

    - applications processing large data sets may not communicate as frequently, and therefore tends to get removed due to apparent idle network activities [more explanation here](https://fasterdata.es.net/science-dmz/science-dmz-security/) 
- What are Firewall Pinholes?

### Science DMZS Security

- Router ACLs 

  - implemented in the router/switches forwarding hardware

  - allow traffic for specific ports & subnets to the Data Transfer Nodes

- Host port control, e.g. IPTables

- Network & Host Intrusion 

- [more measures & their examples here](https://fasterdata.es.net/science-dmz/science-dmz-security/best-practices-for-science-dmz-security/) 

> at BioHPC, the Science DMZ architecture isn't exactly implemented. Instead, a traditional DMZ architecture is in place, where the DMZ servers aren't in direct connection with the storage array (this is due to limitations in the interal LAN not having the big internal bandwidth as required by Science DMZ?) 


### Measures & Practices for HPC Security 

- **Confidentiality, Integrity, and Availability** 

- Incremental backups---for raw data that are not supposed to be tempered with---compare with backed up data with copied data; if a difference is detected, then data integrity is broken 

- Utilizing workflows, containers, and web interfaces as service platforms so that HPC users do not directly exert actions on the clsuter
 

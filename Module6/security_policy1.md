## Security Conducts & Policies

- definition of "Information Resources"---`all networks, software,quipment,  facilities,  and  devices  that  are  designed,  built,  operated  or 
maintained to collect, process, store, retrieve, display, or transmit UT Southwestern information` 

### ISR-104: "Acceptable Use of Information Resources" 

`users must store confidential info or other info essential to the mission of UTSW on a centrally managed server` (line 5)

`users will be held responsible for consequences resulting from neglience of security measures on sensitive information` (line 6) 

  - copy rights violations will also be punished on an individual basis 

`disclose only the minimum necessary protected information to accomplish authorized functions of UTSW` (line 7)

`UT Southwestern information resources must not be used for private commercial purposes or for the exclusive benefit 
of individuals or organizations that are not associated with UT Southwestern` (line 2 of Unacceptable Uses)

`Files not related to UT Southwestern business may not be stored on network file servers` (line 4 of unacceptable uses)`

` Any email communication containing protected information to be sent externally must be sent in a secure method by entering “securemail” in the subject line. When responding to external email containing protected information, users must  not  reply  or  resend  protected  information  in  an  insecure  ma
nner.  Email  messages  must  be  edited  to  remove references to protected information or “securemail” must be used in the subject line.

### ISR-108 Password Management

`Passwords must be unique to UT Southwestern, and must not be used for any other online services, such as shopping, 
financial accounts, or other personal email accounts. These types of personal accounts are frequently compromised, 
and password re-use exposes UT Southwestern to fraudulent access` (line 1 of Password Requirements) 


`implementing lock-outs after too many failed log in attempts` (Admins Responsibility) 

`system admin's accounts are disabled immediately after they leave/are terminated` (Password Security/Responsibilities section) 

### ISR-110 Network Security Management 

`Devices offering  services on the network,  including without limitation all servers, must  be registered with IR. Non-registered servers are subject to quarantine from the network` 

`All  extensions  of  network  services require  management  by  Information  Resources  or  prior  written  approval  of  the CISO. This includes the installation of routers, switches, hubs, wireless access devices, and mobile hotspots` 

`no protected data should reside on the DMZ` 

`Operating and maintaining a reliable network with appropriate redundancies to meet quality of service goals` 

### ISR-111 Systems Access Management

`Users must be granted access to UT Southwestern information resources using the principle of least privilege` 

- BioHPC users, me as a sysadmin in the datahall 

`An employeeuser’s access authorization must be modified or removed when the user’s employment or job responsibilities change or terminate. If an employee account is found to have been inactive for 90 days or more, it will be deactivated.`

- auto deactivation in our LDAP rules? 




















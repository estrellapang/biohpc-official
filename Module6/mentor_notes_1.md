## 

### 12-10-2020: One-on-One with Xiaochu

- all data and intellectual property belongs to UTSW

  - code

  - password, never, never write it down --> send it over a message or email --> or scripts

  - encrypt it!

  - log in - always lock the screen

- data-breach is punishable on an individual basis

- loss of data is on users, but we need to ensure that the data is properly backedup & integrity is maintained

\
\

- least privilege

- REJECT vs. DROP
 

\
\

- overall philosophy, don't be brave and try to work in pairs or inform the team if you ever make any configuration changes

- warn users when they will share PHI data 

\
\

- avoid vague language 

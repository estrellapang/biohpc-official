## Linux Security Part 4: Securing SSH 

### SSH Overview 

- SSH = Secure Shell 

- Can authenticate via both user-password & public-private key pair 

- to enable/disable key-based authentication, go to `/etc/ssh/sshd_config` and change the line `PubkeyAuthentication yes/no` 

- `ssh-keygen` creates the key, and if the user hits `Enter` for passphrase, it will not have any passcodes

  - note; 2 keys will be generated, 1 public & 1 private---the latter is NOT to be shared, and the **public key needs to be stored in the remote system** that one wishes to connect to

- Sending the Pub-Key to remote host

- `ssh-copy-id <user>@<host/IP>`   # specify which key? 

  - this will add YOUR pub key to the remote host's `~/.ssh/authorized_keys` 

### SSH Ops in `sshd_config`

- **enforcing key authentication**

  - `PasswordAuthentication no`

  - this prevents brute force attacks where even if the username/password is attained, the attacker cannot connect b.c. of absence of proper key 

- **disable root logins** 

  - set `PermitRootLogin no` 

  - allow only root login with a key: `PermitRootLogin without-password` 

- **control user & group access** 

  - `AllowUsers <user1> <user2> ...`   # allow specific users

  - `AllowGroups <group1> <group2> ...`   # allow specific groups 

  - `DenyUsers ... ...` 

  - `DenyGroups ... ...`    # OR deny specific users

### Use SSH for Port Forwarding: in `sshd_config`

[Local] 

[Remote] 

[Dynamic] 

- **port forwarding is a security threat**: better to install a **local firewall** 

- **disable SSH TCP Port Forwarding** 

  - `AllowTcpForwarding no` 

  - `GatewayPorts no` --> deny remote connections to the forwarded, local port

### additional concerns

- Use SSHv2 protocol: 

  - this new protocol has improvements in encryption & authentication algorithms

  - `Protocol 2` --> implement this directive entry in `sshd_config`

- Control which Network Interface/Network Address sshd binds to:

  - by default, ssh will listen to ALL addresses on the system

  - in the case that a server has BOTH public & private network connections, one can reduce the attack surface & let sshd bind only to the private network 

  - `ListenAddress [hostname/IP address]`   # this can be multiple lines  

- Customize SSH Port 

  - `Port <customized port>` 

  - **note**: changes to the SSH port will require **updates in the SELinux security policy** 

  - `semanage port -a -t ssh_port_t -p tcp <new port>`

  - `semanpage port -l | grep ssh`   # Verify the port change

- SSH login Banners: avoid info leakage

  - some users may reveal too much information on their pre-login banner messages; it is better to not set a path to banner files in sshd config 

  - `Banner none` ---> could be `Banner <directory of Banner file>`

- RELOAD the sshd configuration after changes! 

  - `sudo systemctl restart/reload sshd` 

## Overview of PAM (Pluggable Authentication Modules)

	Provides a library of functions for applications to use when requesting user authentications, eliminating the need to recompile individual applications when user authenication schemes are changed; PAM can be configured to deny certain programs the right to authenticate users, to only allow certain users to be authenticated, to warn when certain programs attempt to authenticate, or even to deprive all users of login privileges. PAM's modular design gives you complete control over how users are authenticated.  This allows developers to write applications that require authentication, independently of the underlying authentication system 



### Essential Attributes 

- files using PAM for authentication have PAM config files in `/etc/pam.d/` directory 

  - within each, a series of `.so`'s, or shared objects return either success or failure messages after checking the user credentials

  - PAM looks for authentication info in a system's `passwd` or `shadow` files, or within a **LDAP** directory or a Cerberus Server 

### Detailed Components of PAM Library


- an API that acts as the interface between the application layer and the transport layer 

  - handles requests & instructions via sockets

- config files for PAM-aware applications 

  - located in `/etc/pam.d` 

  - each of which uniquely references modules listed that performs specific authentication tasks 

    - each module is a [**dynamically loaded library**](https://www.tecmint.com/understanding-shared-libraries-in-linux/) 

    - within each library, functions are called to perform authentication related tasks 


- `/lib64/security` --> Location of all of the PAM modules (collection is expandable)


![alt-text](https://cdn-images-1.medium.com/max/533/0*ltCYgE0BcsaNw16b.png)
> Toplogy of PAM's interactions with aware-applications


### PAM Config. File Syntax

`#` --> comments 

`\` --> extend the line 


- directives for each entry

	module_interface/type   control_flag   module_name   module_args 

- **module type**: indicates the kind of authorization process 

  - account: account verification e.g. is access allowed? Has user's password expired? Account restrictions can be applied; e.g. an account can only be used for authenitication during normal business hours 

  - auth: authenticates users e.g. requesting and verifying passwords 

  - password: utilized facilitating password changes to work with authentication modules 

  - session: actions performed at the beginning and end of a session e.g. mounting user's home directory 

- **control flags**: all PAM modules generate success or failure result upon being called; the following flags dictate how importatnt the result will be w.r.t. the authentication if a user to the service

  - **_requisite_**: the strongest flag; if  module specified fails, other modules will stop loading, failure returns

  - **_required_**: identical to "requisite", also returns failure at end of execution if one module is failed--however, it is more secure than "requisite" in that it **does not return the error message**, although it will proceed to additional rules upon first failure. Nonetheless, it returns unsuccessful authorization at the end

  - **_sufficient_**: given that all preceding modules have succeeded, the success of this module leads to an immediate and successful return to the application (failure of this module is ignored)--particularly useful when listed at **top** of the module list [example here](https://medium.com/information-and-technology/wtf-is-pam-99a16c80ac57) 

  - **_include_**: pulls modules & their arguments from other PAM config files

  - **_optional_**: referenced when no other modules reference the interface; success or failure status are in general not recorded 

- **more reasons why the order of entries matter!** [see why](https://likegeeks.com/linux-pam-easy-guide/)


### Commands Involving PAM

- [restrict user access to `su` command](https://www.thegeekdiary.com/how-to-restrict-su-access-to-a-user-only-by-pam/) 

- **is a program PAM aware** 

e.g. 

`ldd /usr/bin/[program name] | grep -i 'libpam.so'` 

`ldd /usr/sbin/lsof` | grep -i 'libpam.so'` 

- if the `libpam.so` is found, then the program is "PAM aware" 

### Extra Background Knowledge 


[what are sockets/socket APIs](https://whatis.techtarget.com/definition/sockets) 


 

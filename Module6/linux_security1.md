## Linux Security: Part I---General Security 

### Strong Points in Linux Security 

- it is simply not a common target in terms how many Linux computers there are versus Windows 

- multi-user system 

  - highly specialization of roles for each user 

  - root privilege required for users to install software and modify configurations 

    - whose permission is required to install system-wide software; configure networking, manager users, etc. 

    - other accounts are normal or service accounts

  - point of failure: root access grants totatl ownership of the system 

- file permissions 

  - root owns files related to the operating system
  - dedicated users are allocated for specific applications & services

- each process is run by an account 

  - each account controls its own processes e.g. start/kill 

  - except root 

  - e.g. of security layer: if attacker breaks ino an application user or a normal user, the attacker would only gain the permissions of those specific users---privilege escalation would require a little more hacking 

  - for example above, root user should not be the user running applications

- Linux is open-source: not a black box 

  - open-source community is much faster at noticing flaws in source code 

  - proprietary OS's are tied down with corporate/bureaucracy/secrecy around their product---only the insiders can release patches; takes longer to notice bugs 

- utilization of repositories 

  - available programs and packages are sought out and installed via a package manager 

  - distribution specific repositories are vetted by their official teams, and are almost always safe---not only are distro repositories thoroughly tested, they are also **signed with a public key** which helps to prove that the package had not been changed since its release 

  - installing software without 3rd party add-ons, which could be malicious 

### Guidelines 

- minimize the amount of services & applications installed on the system---they increase the **attack surface** of the system. 

- compartmentalize web services 

  - e.g. file sharing & web servers should not be installed on the same system. If the web server was compromised, the file sharing service on the same system will be susceptible to attack as well. 

- encrypt all over-the-network data transmission 

  - avoids man-in-the-middle attacks 

  - switch to the secure protocols: 

    - SFTP over FTP 

    - SSH over Telnet 

    - SNMP3 over SNMP 1 & 2 

    - HTTPS over HTTPS 

- do not allow more than one service or person to utilize a single account; reduce the complexity of security auditing 

- in the case when shared accounts have to be accessed, e.g. `root` or any application account like the web server user 

  - avoid direct logins into shared accounts---always require logging into individual accounts first 

  - restrict switching to shared accounts with sudo privilege (this also creates a **log of who ran the command!!!** in terms of who used `sudo` along with what the other executed command was and at what time.) 

- double factor authentication: biometrics/email/cell phone code + password 

- principle of least privilege

  - only use escalated privileges when necessary i.e. performing system admin tasks 

  - try not to run services with root privileges 

- review system & application logs 

  - better to send logs to dedicated log server in case of attackers who attempt to delete logs 

- firewalls 

  - Linux kernel comes with a built-in packet filtering framework (Netfilters) 

  - **iptables** tool: communicates with Netfilters in the kernel---iptables in tandem with Netfilters can be used to create firewall rules from the command line 

  - Linux allow the easy creation of **local firewalls** 

  - **least prvilege, again**: only allow network connections from sources that require it. 

- encryption of data over the network & at rest 






## Linux Security Part 3: Network Security 

> Topics: ** network services**, **Linux firewalls**, **info leakage prevention**, **port scanning**, **Xinetd**, & **SSH** 


- **Network Services** 

	anything that listens on network ports for incoming connections: daemons & services, etc. 

- network services are often targets for attacks

- the servers & daemons output their activities into log files either in `/var/log/` or in other directories outside (depends on the type of daemon/service) 

- Nature of Network Services:

  - each performs very specific tasks, e.g. `sshd` manages SSH connections; `httpd` responds to HTTP connections 

  - each has a dedicated account: prevents attacker to access more than what the service account is allowed to 

  - if the services run on **ports < 1024**, they may elevate to root temporarily to open the port and then drop the privilege or transfer it to child processes 

    - **NOTE**: ports < 1024 are _privileged ports_ and require root privilege to open 

    - one could attempt to configure network services to utilize non-root users to perform their tasks 

  - many services by default **bind to/listen from ALL network interfaces**, in order to reduce the attack surface, **limit** the interface/IP address network services bind to.

    - e.g. allow SSH from internal networks & reject all others from the public internet

    - e.g. do not let the database server permit addresses other than that of its associated web server 

      - if both the database & web server are on the same machine, simply bind the former's to the local IP address: `127.0.0.1` 

      - fun fact: MySQL listens on port 3306

    - good pratice to KEEP UNENCRYPTED data transfer in the organizational LAN rather than on the public net as well


- Tools to manage incoming packets to Net Services: 

  - Firewall 

  - TCP Wrappers

  - TCP Wrap services i.e. `xinetd`

    - see xinetd controlled services --> `/etc/xinet.d/ ` --> each service will have a config file 

    - if need to be disabled, insert `disable = yes` somewhere in the file

  - the flow control of incoming requests goes from top to bottom in the above mentioned mechanisms 

![alt text](firewall_tcpwrappers_xinetd.png "Fate of Packets to Net Services")  

- **Which Services are Using Which Port?** Try `netstat` 

- good command options: 

  - `-n`: displays addresses in numerical format 

  - `-u`: include the UDP protocol 

  - `-t`: include TCP 

  - `-l`: display only listening sockets 

  - `-p`: **need root privilege**: display process PID & Name 

e.g. sample output of `sudo netstat -nutlp` 

```
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 0.0.0.0:111             0.0.0.0:*               LISTEN      1/systemd           
tcp        0      0 0.0.0.0:20048           0.0.0.0:*               LISTEN      3433/rpc.mountd     
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      3358/sshd           
tcp        0      0 127.0.0.1:25            0.0.0.0:*               LISTEN      3789/master         
tcp        0      0 0.0.0.0:2049            0.0.0.0:*               LISTEN      -                   
tcp        0      0 0.0.0.0:43810           0.0.0.0:*               LISTEN      -                   
tcp        0      0 0.0.0.0:54562           0.0.0.0:*               LISTEN      3375/rpc.statd

``` 
- explanation 

  - "Foreign Address" of 0.0.0.0:* means the program(s) listed in lsitening from all IP addresses 

  - the `master` program is a component of the post-fix mail server listening on the SMTP port 25 on the interface `127.0.0.1`, this is the "local" loop-back IP address ---> it is good practice to let strictly local programs listen ONLY on the loopback IP as opposed to across ALL interfaces on the system 


- **_scan them ports!_**: USEFUL COMMANDS

- 1.`nmap` 

  - can be utilized to listen to local & remote machines

  - not installed by default: `sudo yum install nmap` 

- syntax: `nmap <hostname/IP>` 

- e.g. scanning **local network address**

```
[zxp8244@linux1 ~]$ nmap 192.168.56.4

Starting Nmap 6.40 ( http://nmap.org ) at 2019-05-30 17:33 CDT
Nmap scan report for linux1.vms.train (192.168.56.4)
Host is up (0.00074s latency).
Not shown: 996 closed ports
PORT     STATE SERVICE
22/tcp   open  ssh
111/tcp  open  rpcbind
2049/tcp open  nfs
3306/tcp open  mysql

``` 

- e.g. scanning local loop-back address: notice extra open ports? 

```
[zxp8244@linux1 ~]$ nmap 127.0.0.1

Starting Nmap 6.40 ( http://nmap.org ) at 2019-05-30 17:48 CDT
Nmap scan report for localhost (127.0.0.1)
Host is up (0.00083s latency).
Not shown: 994 closed ports
PORT      STATE SERVICE
22/tcp    open  ssh
25/tcp    open  smtp
111/tcp   open  rpcbind
2049/tcp  open  nfs
3306/tcp  open  mysql
49158/tcp open  unknown

``` 

- e.g. using `nmap` with `-Pn` option: which skips the host discovery process by pretending that all all hosts are online, whichcan **_sometimes_** get past certain REJECT policies 

```
[zxp8244@linux1 xinetd.d]$ nmap 198.215.56.34      # first attempt to no avail!

Starting Nmap 6.40 ( http://nmap.org ) at 2019-05-31 11:26 CDT
Note: Host seems down. If it is really up, but blocking our ping probes, try -Pn
Nmap done: 1 IP address (0 hosts up) scanned in 0.02 seconds
[zxp8244@linux1 xinetd.d]$ 
[zxp8244@linux1 xinetd.d]$ 
[zxp8244@linux1 xinetd.d]$ 
[zxp8244@linux1 xinetd.d]$ 
[zxp8244@linux1 xinetd.d]$ nmap -Pn 198.215.56.34

Starting Nmap 6.40 ( http://nmap.org ) at 2019-05-31 11:26 CDT
Nmap scan report for 198.215.56.34
Host is up (0.0015s latency).
Not shown: 999 filtered ports
PORT   STATE SERVICE
22/tcp open  ssh                                 # Oh looky here! An open port! 

``` 

- e.g. where `nmap -Pn` cannot detect any unfiltered ports b.c. of remote host's strong iptables ruleset

  - get on 192.168.56.4 and execute ` sudo iptables -L --line-numbers -n -v ` to check its strong policies (DROP on both INPUT & FORWARDchains)

```
[zxp8244@sambanfs ~]$ nmap 192.168.56.4

Starting Nmap 6.40 ( http://nmap.org ) at 2019-05-31 12:08 CDT
Note: Host seems down. If it is really up, but blocking our ping probes, try -Pn
Nmap done: 1 IP address (0 hosts up) scanned in 0.02 seconds
[zxp8244@sambanfs ~]$ 
[zxp8244@sambanfs ~]$ 
[zxp8244@sambanfs ~]$ 
[zxp8244@sambanfs ~]$ nmap -Pn 192.168.56.4

Starting Nmap 6.40 ( http://nmap.org ) at 2019-05-31 12:08 CDT
Nmap scan report for linux1.vms.train (192.168.56.4)
Host is up (0.91s latency).
All 1000 scanned ports on linux1.vms.train (192.168.56.4) are filtered

Nmap done: 1 IP address (1 host up) scanned in 55.92 seconds

```
 
- 2.`lsof` --> show all listening & established connections (LOCALLY)

  - does not show port numbers by default? 

  - execute with **root privilege** 

  - **practical option**: `lsof -i` 

- 3.`telnet` --> modern networking purpose: see if specific ports are reachable 

  - text-based and incompatible with protocols like SSH & SSL: being phased out by OS's like MacOS & RHEL  

  - syntax: `telnet <Hostname/IP> <port>` 

e.g. using telnet to check if port 22 is open (but cannot use telnet's supported protocol to send instructions to remote host's SSH port) 

```
[zxp8244@linux1 xinetd.d]$ telnet 198.215.56.34 22
Trying 198.215.56.34...
Connected to 198.215.56.34.
Escape character is '^]'.
SSH-2.0-OpenSSH_7.4
quit
Protocol mismatch.

```

- 4.`nc` --> or "netcat" 

  - an alternative telnet, and is installed by default in RedHat & Centos as a part of the `nmap` bundle

  - syntax: `nc <Hostname/IP> <port>` 

  - useful options: `-vz` (verbose and ONLY seeing if the specified port is open) 

    - `-u` --> test connection for UDP ports

e.g. one host has port 80 open, while the other doesn't 

```

[zxp8244@linux1 xinetd.d]$ nc -vz 192.168.56.7 80
Ncat: Version 7.50 ( https://nmap.org/ncat )
Ncat: Connected to 192.168.56.7:80.
Ncat: 0 bytes sent, 0 bytes received in 0.01 seconds.
[zxp8244@linux1 xinetd.d]$ nc -vz 192.168.56.4 80
Ncat: Version 7.50 ( https://nmap.org/ncat )
Ncat: Connection refused.

```

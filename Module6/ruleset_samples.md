## Security Training Module: Exercise Examples 

### PAM: Restrict SSH Login to One Local User


- **create a user with sudo rights**

```
sudo useradd -aG wheel zinger  # create user and add it to "wheel" the sudoers' group (secondary/supplementary) 

passwd zinger   

id zinger   # confirm the user's credentials; `groups zinger` would do as well 

##### sample output #####

[zxp8244@linux1 ~]$ id zinger
uid=1002(zinger) gid=1002(zinger) groups=1002(zinger),1014(wheel)

######################### 

sudo su - zinger   # login to zinger's account 

sudo whoami   # confirm user's sudo privilege

##### sample output #####

[zinger@linux1 ~]$ sudo whoami
[sudo] password for zinger: 
root

######################### 

```

- **use PAM to restrict SSH login to user 'zinger'**

1.`sudo vi /etc/pam.d/sshd` 

2.add the following line under the `auth` module stack: 

``` 
auth	   required	pam_listfile.so onerr=fail item=user sense=allow \
file=/etc/ssh/allowed_users 

```  

3.explanation: 

`required` ---> control flag: the module & module arguments specified in the line
are compulsory for the entire authenication process to succeed 

`pam_listfile.so` ---> a PAM module authenticates via items listed in files 

`onerr=fail` ---> module argument: upon errors i.e. no reference file found or unable to be
opened, the authentication will fail 

`item=user` ---> module argument: tell module what credentials are listed in 
the file specified and should be checked for (username in this case) 

`sense=allow` ---> module argument: what to do when a match is found in the file; here, 
if the username 'zinger' matches that of the account attempting to SSH in, access 
will be granted 

&nbsp; 

- Verification: 

``` 

$ ssh zxp8244@192.168.56.4
zxp8244@192.168.56.4's password: 
Permission denied, please try again.   # SSH login denied 

$ ssh root@192.168.56.4
root@192.168.56.4's password: 
Permission denied, please try again   # SSH login denied 

$ ssh estrella@192.168.56.4
estrella@192.168.56.4's password: 
Permission denied, please try again.   # SSH login denied 

$ ssh zinger@192.168.56.4
zinger@192.168.56.4's password: 
Last login: Thu May 23 10:29:38 2019    # Yay


```

### iptables rulesets 


- **restrict SSH connection to a select number of machines** 

```
$ sudo iptables -L --line-numbers -n -v

Chain INPUT (policy DROP 0 packets, 0 bytes)
num   pkts bytes target     prot opt in     out     source               destination         
1     3801  298K ACCEPT     all  --  *      *       0.0.0.0/0            0.0.0.0/0            state RELATED,ESTABLISHED
2        0     0 ACCEPT     icmp --  *      *       0.0.0.0/0            0.0.0.0/0           
3        2   320 ACCEPT     all  --  lo     *       0.0.0.0/0            0.0.0.0/0           
4        0     0 ACCEPT     tcp  --  *      *       192.168.56.12        0.0.0.0/0            tcp dpt:22
5        7   420 ACCEPT     tcp  --  *      *       192.168.56.1         0.0.0.0/0            tcp dpt:22
6        0     0 REJECT     all  --  *      *       0.0.0.0/0            0.0.0.0/0            reject-with icmp-host-prohibited

Chain FORWARD (policy DROP 0 packets, 0 bytes)
num   pkts bytes target     prot opt in     out     source               destination         
1        0     0 REJECT     all  --  *      *       0.0.0.0/0            0.0.0.0/0            reject-with icmp-host-prohibited

Chain OUTPUT (policy ACCEPT 2544 packets, 286K bytes)
num   pkts bytes target     prot opt in     out     source               destination   

```

- `iptables` command for rules above: 

```

sudo iptables 

sudo iptables -P INPUT DROP   # if configuring remotely, do this LAST!!!
sudo iptables -P FORWARD DROP   # when one doesn't want the machine to act a router
sudo iptables -P OUTPUT ACCEPT
sudo iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
sudo iptables -A INPUT -p icmp -j ACCEPT
sudo iptables -A INPUT -i lo -j ACCEPT
sudo iptables -A INPUT -s 192.168.56.12/32 -p tcp -m tcp --dport 22 -j ACCEPT
sudo iptables -A INPUT -s 192.168.56.1/32 -p tcp -m tcp --dport 22 -j ACCEPT
sudo iptables -A INPUT -j REJECT --reject-with icmp-host-prohibited
sudo iptables -A FORWARD -j REJECT --reject-with icmp-host-prohibited

``` 

- NOTE: INPUT chain rules #1-3 are bare essentials for local comms, established connections, & ICMP protocol (for ping & stuff like traceroute); ssh port opened for addresses 56.1 & 56.12. DROP policy for for INPUT chain + "reject all" rule terminating the ruleset blocks all other connections 

&nbsp; 

- **block connections from an IP range**: 

```
sudo iptables -I INPUT 5 -p tcp --dport 22 -m iprange --src-range 192.168.56.10-192.168.56.12 -j REJECT
sudo iptables -I INPUT 6 -p tcp --dport 80 -m iprange --src-range 192.168.56.10-192.168.56.12 -j REJECT

``` 

- `-I` option lets one specify the line number for the rule being added 

- use `-m` module option to select `iprange` module and specify range via `--src-range`

- `sudo iptables-save` to make rules persistent

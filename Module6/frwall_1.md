## Configuring Firewall via `iptables`, Part I 

### Introduction & Structure

- **_Netfilter_** framework: 

  - a framework within the Linux kernel that provides a series of hooks in various points in a protocol stack 

  - the function in the Linux kernel that filters packets based on:

    - Source/Destination IP address 

    - Source/Destination port number

    - Source/Destination MAC address (specifying interfaces)

    - Network Protocol

- Netfilter applies the following actions for each network packet 

  - accept, reject, ignore, and pass 

- `iptables` command manages the Netfilter feature 


- 5 Firewall/Network Traffic Filters/Tables (of Netfilters)

  - **Filter**: main traffic processing mechanism that polices incoming/outgoing connections 

  - NAT: lists NAT rules

    - **N**etwork **A**ddress **T**ranslation: allows one IP address to be shared

    - what allows VMs to send/receive traffic with the interface used on the physical host 

  - Mangle: rules for mangling/altering attributes of packets 

    - e.g. modifying TTL, or "time to live" for packets

  - Security: use for "Mandatory Access Control" (?) which are implemented by Linux security modules such as SELinux

  - Raw: rarely used; but primarily for allowing exemptions from "connection tracking" (?) 

- Filters/Tables are organized & compartmentalized by "Chains" 

![alt text](tables_chains.png) 
> Chains utilized by each Filter/Table 

- Chains are the "containers" of the categorized rules in the firewall tables 

  - List of the Chains: **remember**--their names are CASE-SENSITIVE!

    - **_INPUT_**: packets coming into the host 

    - **_OUTPUT_**: packets sent to another host 

    - **_FORWARD_**: packets received but to be relayed to other hosts 

    - **_PREROUTING_**: used by tables like NAT, where packets go to prior to INPUT or FORWARD chains 

    - **_POSTROUTING_**: also used by tables like NAT; where packets go after OUTPUT chains, right before they leave the system

- Different Combinations of Chains are utilized by Different Tables

![alt text](chains_list.png "Tables & Chains") 

- Graphical Concepts on Packet Processing: 

![alt text](packet_flow_simple.png) 
> the general sequence of packet flow through the chains 

![alt text](packet_flow_detailed.png) 
> overall view of packets traversing through chains

![alt text](http://shorewall.net/images/Netfilter.png)
> even more detailed flow diagram of packet flow

- NOTE: remember, one can customize and build his/her own chains 

### Chain Policy

  - once again, each packet going through a chain could be evaluated by **IP, MAC, Port, Protocol**, and a packet will **go down the list of a chain's rules until they match one, after which the packet is sent to a "Target", which determines what will happen to the packet**

  - there are **5** targets/actions upon a matched packet:

    - **_DROP_**: drop packet w/o informing the client 

    - **_ACCEPT_**: allow packet past the firewall 

    - **_REJECT_**: drop packet but informs the sender; not as secure as DROP

    - **_RETURN_**: stops the packet from further rule matching; if the chain is a main chain i.e. INPUT; then the **Default Chain Policy/Target** is implemented: (ACCEPT or DROP)

    - **_LOG_**: logs the packet via system loggers and **lets it continue to be processed**

### Listing & Viewing Rules  

`iptables -L` --> this command lists rules for all chains in the main filter table

- Unless otherwise stated, the filter table will by default be the table whose rules are being listed, and to which the new rules will go to 

- More options: 

`iptables -L [INPUT/OUTPUT/FORWARD] -n --line-numbers -v` 

`-n` --> show only IP address & port numbers (no resolved DNS hostnames) 

`-v` --> verbose:show number of pacjets processed & the cumulative size of all packets in bytes 

`-t` --> specify table, e.g. `-t nat`   

`--line-numbers` --> display rules of the specified chain with line numbers, yay~! 


- **Format**:

```
[target] [prot] [opt] [source] [destination] rule options 

``` 

- **_target_**: If a packet matches the rule, the target specifies what should be done with it. For example, a packet can be accepted, dropped, logged, or sent to another chain to be compared against more rules 

- **_prot_**: specific protocol example values: `tcp` `udp` `icmp` `all` 

- **_opt_**: IP options (rarely used)

- **_source_**: source IP or subnet, or input `anywhere`

- **_destination_**: destination ~ ~ 


### Implementing Em' Rules!

- in CentOS & RHEL, `firewalld` is installed by default to talk to the netfilter framework, and `iptables-services` is not installed by default; however, one can also directly interact with the system-built iptables/Netfilters via the `iptables` command; see below for more:  

  - `iptables-services` (which only pulls iptable configurations from a saved file), one can opt to disable firewalld and enable iptables-services (both services conflict). This is **option 1**

    - `sudo yum install iptables-services` 

    - `systemctl start iptables` 

    - `systemctl enable iptables` 

    - `systemctl stop firewalld` 

    - `systemctl disable firewalld` 

    - `systemctl mask firewalld` 

  - **option 2**: use `firewalld` to customize filter ruleset via its own distinct syntax

  - **option 3**: disable both `firewalld` & `iptables-services` and interact with the kernel integrated iptables/Netfilters alone (not recommended) 

    - difference between daemons & services: the former actively performs complex actions, while the latter resorts to static configurations and applies upon system boot.

- **Looking at Default Rules**: 

  - standard rules generated firewalld (could be different if it is not present or by iptables-service) 

```

$ sudo iptables -L
Chain INPUT (policy ACCEPT)
target     prot opt source               destination         
ACCEPT     all  --  anywhere             anywhere            state RELATED,ESTABLISHED 
ACCEPT     icmp --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     tcp  --  anywhere             anywhere            state NEW tcp dpt:ssh 
REJECT     all  --  anywhere             anywhere            reject-with icmp-host-prohibited 

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination         
REJECT     all  --  anywhere             anywhere            reject-with icmp-host-prohibited 

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination      


```

- **Explanation** on important rules from above: [more thorough explanations here](https://wiki.centos.org/HowTos/Network/IPTables) (how to build from ground up!)

  - `Chain INPUT (policy ACCEPT)`: **default policy**: what to do if packets do not match any of the rules listed. 

    - **command for setting general policies**: `sudo iptables -P [chain] [policy: only ACCEPT/DROP]` 

    - `DROP` is generally considered to be a bit more safe, as incoming packets that don't match the any of the rules will be dropped 

    - it is actually good practice to enforce `DROP` policies on the FORWARD chain, so that the machine does not by default serve as a router that allows packets to pass through it; however, `ACCEPT` is generally a good policies on the OUTPUT chain, unless one does not trust all users of the machine

  - `ACCEPT   tcp  --  anywhere    anywhere    state NEW tcp dpt:ssh`: SSH connections are permitted from all source and destination IPs by default 

    - **important to delete the following rule if the sysadmin wishes to set restrictions on SSH access!** 

  - `ACCEPT   all  --  anywhere   anywhere   state RELATED,ESTABLISHED` 

    - command: `iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT`

    - **"the rule that does the most work"**: this rule utilizes `-m` flag to load the "state" module, which checks the state of packets---whether they are "NEW", "ESTABLISHED", or "NEW." The options specified lets the module check "RELATED" and "ESTABLISHED" modules, which are packets from already established connections or related to already established connections. Without this  rule, the SSH connections would not persist.

  - `ACCEPT   all  --  anywhere   anywhere`:  this rule is created by the command `iptables -A INPUT -i lo -j ACCEPT`, which states uses the `-i` flag to specify that packets received by `lo`, or localhost interface, are accepted---this rule is **crucial**, for it lets the local processes to access information installed locally, e.g. the local web server accessing a locally installed database 

  - `REJECT   all -- anywhere  anywhere reject-with icmp-host-prohibited`: this is an essential good practice: **end all rulesets with a "reject-all" rule** that which rejects any packet that does not match all of the other rules---this is a useful extra layer of security in addition to the CHAIN policy!

    - iptable command: `$ sudo iptables -A INPUT -j REJECT --reject-with icmp-host-prohibited` 

    - **iptables returns a rejection message** via the ICMP protocol, the contents of which can **be somewhat customized**:

    - **REJECT syntax** `REJECT --reject-with imcp-[message option]` [see list here](https://unix.stackexchange.com/questions/124624/what-a-input-j-reject-reject-with-icmp-host-prohibited-iptables-line-does-ex) 

    - if not specified, the default will be `icmp-port-unreachable`

    - for best security measures, **DROP ALL with no icmp message is more secure**, as it lets scanners packets hang w/o reply; attempts to scan for open ports would take much longer as attackers would have to wait until each dropped packet times out. 

- **Adding Rules** 
 
e.g. `sudo iptables -A INPUT -i eth1 -p tcp --dport 22 -d 192.168.56.1 -j ACCEPT` 

- options explained: 

  - `-A` --> **append** new rule to the **bottom of the ruleset** 

  - `-I [order #]` --> **insert** new rule and specify **where** it will be located

  - `-i` --> "interface" or device: to which physical port (e.g. "enp0s8" or "lo") will the traffic enter or leave the host 

  - `-p` --> "protocol": specifies the packet's protcol 

    - e.g.  `-p tcp` `-p udp` `-p icmp` 

    - `--dport` --> specifies the destination port 

    - `--sport` --> specifies the source port

    - although these follow after the protocol option, but they can be specified together with IP & many other options 

    - e.g. `sudo iptables -A INPUT -s 192.168.56.12/32 -p tcp --dport 22 -j ACCEPT`   # accept TCP connections coming to port 22 from 56.12 

    - `icmp` protocol can be used to match "ping" & "pong" connections, and more 

    - e.g. `sudo iptables -A INPUT -p icmp --icmp-type echo-request` # blocks ping requests

    - see list of all `--icmp-types` execute `iptables -p icmp -h`

  - `-P` --> or `--policy`: specifies the general policy for each chain

    - `sudo iptables -P INPUT DROP`

  - `-s` --> source IP

    - `-s 192.168.56.0/24`   # one can set the entire subnet or individual addresses---the format can either be the shorthand or the full mask e.g. "255.255.255.0"  

  - `-d` --> destination IP

  - `-j` --> specifies the action: ACCEPT,DROP, or REJECT 

  - `-v` --> verbose option, which will show packet stats i.e. size & number (culmulative),
as well as `in` & `out` interfaces

  - `-S` --> show iptables in terms of **executed iptables commands** (useful!) 

  - `-F` --> flush rules

    - `sudo iptables -t filter -F INPUT`

  - `-m` --> utilize iptables modules e.g. limit an entire range of IP's without having to specify via the subnet mask (see example in "ruleset_samples.md" within this directory)    #  `man iptables-extensions` 

    - example on `-m limit` --> which can match packets based on their rate

    - by default settings, all the packets from a certain destination  that DO NOT exceed a given rate are matched -OR- packets that exceed a speicifiedrate will be matched 

![alt text](limit_rate.png "syntax for -m limit") 


- example 1: **block the ssh connection** from a specific IP address 

`sudo iptables -A INPUT -p tcp --dport 22 -s 192.168.56.10 -d 192.168.56.4 -j REJECT`

or simply: `sudo iptables -A INPUT -p tcp --dport 22 -s 192.168.56.10 -j REJECT` (only need to specify source) 
 
```
[zxp8244@linux1 ~]$ sudo iptables -L INPUT --line-numbers -n 
Chain INPUT (policy ACCEPT) 
num  target     prot opt source               destination  
2    REJECT     tcp  --  192.168.56.10        192.168.56.4         tcp dpt:22 reject-with icmp-port-unreachable

# option `-v` can also be added to reveal number of packets and cumulative size, in addition to **interfaces**!!! 

```

- example 2: restrict SSH connections from **a range of IPs** 

  - for resultant rules, see ruleset example 2 

  - use of `iprange` module & specifying with `--src-range` option 

- example 3: **prevent a common DoS Attack** via `-m limit` & `-m state`

  - `sudo iptables -A INPUT -p tcp --dport 80 -m limit --limit 50/min --limit-burst 200 -j REJECT` 

  -  `sudo iptables -A INPUT -p tcp --dport 80 -m limit --limit 50/min --limit-burst 200 --state NEW -j REJECT`  

`sudo iptables -I INPUT 5 -p tcp --dport 22 -m iprange --src-range 192.168.56.10-192.168.56.12 -j REJECT` 


- **Saving Rules** 

- by type/table: 

`sudo iptables-save -t filter`   # `-t` type/table (depending on how one perceives the iptables structure). `filter` table/type 

- save to specific file (when iptables-service is installed) 

`sudo iptables-save > /etc/sysconfig/iptables` 


- **Deleting Rules** 

- by chain & rule number 

`sudo iptables -D [chain] [rule number]` 

- rid of all rules 

`sudo iptables -F [chain or do not specify]`

 

### Ruleset Examples 

- Ruleset sample 1 

  - redundantly secure: DROP policy + Reject all rule on INPUT

  - FORWARD chain drop policy: when one doesn't want to use his/her machine as a router

```
[zxp8244@linux1 ~]$ sudo iptables -L --line-numbers -n
Chain INPUT (policy DROP)
num  target     prot opt source               destination         
1    ACCEPT     all  --  0.0.0.0/0            0.0.0.0/0            state RELATED,ESTABLISHED
2    ACCEPT     icmp --  0.0.0.0/0            0.0.0.0/0           
3    ACCEPT     all  --  0.0.0.0/0            0.0.0.0/0           
4    ACCEPT     tcp  --  192.168.56.12        0.0.0.0/0            tcp dpt:22
5    ACCEPT     tcp  --  192.168.56.1         0.0.0.0/0            tcp dpt:22
6    REJECT     all  --  0.0.0.0/0            0.0.0.0/0            reject-with icmp-host-prohibited

Chain FORWARD (policy DROP)
num  target     prot opt source               destination         
1    REJECT     all  --  0.0.0.0/0            0.0.0.0/0            reject-with icmp-host-prohibited

Chain OUTPUT (policy ACCEPT)
num  target     prot opt source               destination 
 
```

- Ruleset sample 2 

  - 192.168.56.12 has SSH connection, listed on top of the IP range SSH restrict at the bottom of ruleset makes non-applicable (once again, order matters!)

  - security hole: missing `reject rest/all` rule & INPUT policy being "ACCEPT" 

```

[zxp8244@linux1 ~]$ sudo iptables -L --line-numbers -n
Chain INPUT (policy ACCEPT)
num  target     prot opt source               destination         
1    ACCEPT     all  --  0.0.0.0/0            0.0.0.0/0            state RELATED,ESTABLISHED
2    ACCEPT     icmp --  0.0.0.0/0            0.0.0.0/0           
3    ACCEPT     all  --  0.0.0.0/0            0.0.0.0/0           
4    ACCEPT     tcp  --  192.168.56.12        0.0.0.0/0            tcp dpt:22
5    ACCEPT     tcp  --  192.168.56.1         0.0.0.0/0            tcp dpt:22
6    REJECT     tcp  --  0.0.0.0/0            0.0.0.0/0            tcp dpt:22 source IP range 192.168.56.10-192.168.56.12 reject-with icmp-port-unreachable

Chain FORWARD (policy ACCEPT)
num  target     prot opt source               destination         
1    REJECT     all  --  0.0.0.0/0            0.0.0.0/0            reject-with icmp-host-prohibited

Chain OUTPUT (policy ACCEPT)
num  target     prot opt source               destination

```

#### resources 

 [firewalld & iptables](https://www.tecmint.com/firewalld-vs-iptables-and-control-network-traffic-in-firewall/) 

[delete rules by rule #](https://www.hostinger.com/tutorials/iptables-tutorial) 

[TCP Wrappers approach to SSH](https://www.agix.com.au/limiting-access-by-ip-to-ssh-on-centos7-and-rhel7/) 

[good explanation of base table rules](https://wiki.centos.org/HowTos/Network/IPTables) 

[essential iptable 1](https://likegeeks.com/linux-iptables-firewall-examples/) 

[essential iptable 2](https://www.digitalocean.com/community/tutorials/how-to-list-and-delete-iptables-firewall-rules) 

[secure overall 1](https://likegeeks.com/secure-linux-server-hardening-best-practices/) 

[secure overall 2](https://wiki.centos.org/HowTos/Network/SecuringSSH#head-a296ec93e31637aa349538be07b37f67d836688a) 









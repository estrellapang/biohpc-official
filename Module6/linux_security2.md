## Linux Security Part II: PAM & Account Security 

### Account Security General Guidelines 

- it is critical to protect the system from the inside by setting restrictions on its accounts 

- elements that are prone to privilege escalation attacks 

  - bug in the software running on system 

  - insecure configurations of software accounts  

  - users with unrestricted `su` & `sudo` permissions 

- Essential Measures 

  - prevent unwanted users from gaining access to accounts that are security threats

  - secure the critical accounts themselves, use the principle of Least Privilege in what they can access 


### Few Notes on PAM 

- login program checks `/etc/passwd` & `/etc/shadow` for authenticating usernames & passwords 

  - PAM can intercept that task and choose to authenticate user credentials in a wider variety of ways (based on a range of available PAM modules), e.g.let the login program authenticate via (via a PAM module) finger prints of users 

  - this is advantageous in that the programs themselves do not have to be rewritten in order to utilize a new authentication method 

### Accounts & Password Security 

- `root` --> UID of 0; if another user's UID is set to 0, then it would be granted superuser privilege as well 

- **UID < 1000** --> system accounts (typically by services & apps)

  - configure the range in `/etc/login.defs`

  - `useradd -r` --> lets the new user belong in in the system account range


- **enforce strong passwords** via `pam_pwquality.so` 

  - configure in `/etc/security/pwquality.conf` 

- **shadow password**

  - hashed password are stored in `/etc/shadow`, which only `root` can access

  - /etc/shadow structure:

  - fields separated by colons: username, hashed password, days since epoch of last password change (epoch of for Linux: Jan. 1st, 1970), days until change allowed, days before change required, days warning expiration, days before account becomes inactive, and days since epoch when account expires, and reserved (for future use) 


- `chage`: account expiry management 

  - `chage -l <accountname>` --> display account aging information

  - `chage -M <max_days>` --> set max number of days for when the password is valid (can let user change their password every however many days) 

  - `chage -E <expiration date>` --> date when account will be no longer accessible 

  - `chage -d <last day>` --> set the last day when password was changed 

  - default is set in `/etc/login.defs` 

    - PASS_MIN_DAYS = minimum number of days needed before next password change 

    - PASS_MIN_LEN = minimum length of password allowed 

    - PASS_WARN_AGE = number of days a warning is given before password expires 

- Prevent Users from Using Same Passwords 

  - `password required pam_pwhistory.so` --> this modules stores the previous 10 passwords used by each account in a /etc/< > file 

    - `remember=<#>` option --> tell module how many passwords to remember 

- **Account Access Control** 


- Lock accounts via `passwd`

  - `passwd -l <account name>` 

  - `passwd -u <account name>`  # unlock an account 

- Lock accounts via `/etc/passwd`

  - use `nologin` as shell option, which switches the accounts shell to non-interactive

  - at the end of each file entry, type  `/sbin/nologin` 

  - this is commonly applied to system/application accounts e.g. `apache` 

- Lock accounts via `chsh` 

  - this command changes accounts' shells without having to go into /etc/passwd 

  - `chsh -s <shell> <account>` 

  - e.g. `chsh -s /sbin/nologin <account>` 

- Lock accounts via `pam_nologin.so`

  - this module looks for `/etc/nologin` -OR- `/var/run/nologin` 

  - if user is listed in either of which, he/she will be prevented from logging in 

- Authentication Logs 

  - `last` command --> displays the list of last-logged in users

    - gets information from `/var/log/wtmp` file 

  - `/var/log/btmp` --> records **failed** login attempts 

    - `lastb` --> command to show its contents 

  - `/var/log/lastlog` --> stores **when** the a user last logged in 

    - `lastlog` --> its corresponding command 

  - other places to check for authentication logs: 

    - `/var/log/messages` 

    - `/~/~/syslog` 

    - `/~/~/secure` 

    - `/~/~/auth.log` 

    - where one may find relevant authentication info depends on the distribution  

- Intrusion Prevention System 

  - fail2ban; monitors log files, and blocks IP addresses with repeated failed attempts; and automatically unlocks the account after a set time 


- Mulitifactor Authentication 

  - Popular ones all use PAM modules

  - Google Authenticator

  - DuoSecurity 

  - RSA SecurID 

 
- **Root Account & Security by Account Type** 

- Root common practice: 

  - prevent direct log in to root

  - use `sudo`, as the system keeps a more detailed log of:

     - who executed

     - what commands 
 
     - by contrast, `su` can only be traced to at what time a user had switched to root account

  - make sure no other account has an UID of '0' 

     - `awk -F: '($3 == "0") {print}' /etc/passwd` --> **check if root is the only account that has UID of 0**

``` 
$ awk -F: '($3 == "0") {print}' /etc/passwd
root:x:0:0:root:/root:/bin/bash

``` 

- How to Disable Root Logins 

  - **_local root logins_**: edit entries in `/etc/securetty` file: by default, it affects the **login** program whose PAM config is located in the `/etc/pam.d` directory. The corresponding PAM module is configured so that whichever interface that is **listed** in `/etc/securetty` will be able to login directly to root

  - `w` --> see which device you are logged in as

e.g.

```
[zxp8244@linux1 ~]$ w
 13:06:48 up  3:31,  1 user,  load average: 0.00, 0.01, 0.05
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
zxp8244  pts/0    zengmaster       09:38    0.00s  0.05s  0.02s w

```

  - **_remote root logins_**: edit `/etc/pam.d/sshd` (refer to `pam_1.md` or `pam_2.md`) 

  - **_remote root logins_2**: 

     - add `PermitRootLogin no` to `/etc/ssh/sshd_config`  ---> `sudo systemctl restart sshd` 


- Application Accounts (e.g. httpd or apache) 

  - either implement `nologin` to them, or 

  - restrict SSH direct logins to those app accounts 

    - e.g. add following line to `sshd_config` ---> `DenyUsers <app. account 1>  <app. account 2>` 

  - **_alternative_**: use `sudo` to perform actions via the apps' accounts 

    - `sudo -u <app account> <app commands>` 

e.g. 

```
[zxp8244@linux1 ~]$ sudo -u estrella whoami
[sudo] password for zxp8244: 
estrella
[zxp8244@linux1 ~]$ 
[zxp8244@linux1 ~]$ 
[zxp8244@linux1 ~]$ 
[zxp8244@linux1 ~]$ sudo -u zinger whoami
zinger

``` 

- Deleting Inactive Accounts: **3 Essential Steps** 

  - 1.Determine & Note Down Account ID: `id account` 

  - 2.Delete account: `# userdel -r <account name>` 

    - the `-r` option deletes the account along with its **home directory** 

  - 3.find files that belong to the deleted user and either delete them or change the owner

    - `find / -user <deleted UID>` 

    - `find / -nouser` 

- **Sudoers File**

- edit directly by `sudo visudo` or `# vi /etc/sudoers` (LATTER is NOT Recommended) 

  - b.c. `visudo` actually **validates the syntax** of the sudoers file prior to saving---this can save someone from generating errors that inhibits sudo operations

- `vi` editor will be used to edit the file by default, if other editors are preferred, change it in the environment variable: `EDITOR` 

  e.g. `export EDITOR=nano`  # this sets default editor to NANO (for the current login/session) 


- e.g. control what commands a user with sudo privilege can execute:

- `<user> ALL=(root) /usr/bin/yum` 

  - this allows <user> on all hostnames/interfaces (typically ALL is the most frequently used option), to only be able to `sudo yum` 

  - the additional line is typically added to the bottom of the page

- **view what an user can sudo execute** 

  - `sudo -l` --> view what sudo commands are allowed yourself

  - `sudo -l -U <username>`  # `-ll` can be used in place of `-l` for slightly more verbose listing

e.g. 

```
Matching Defaults entries for estrella on linux1:
    !visiblepw, always_set_home, match_group_by_gid, always_query_group_plugin, env_reset, env_keep="COLORS DISPLAY HOSTNAME HISTSIZE
    KDEDIR LS_COLORS", env_keep+="MAIL PS1 PS2 QTDIR USERNAME LANG LC_ADDRESS LC_CTYPE", env_keep+="LC_COLLATE LC_IDENTIFICATION
    LC_MEASUREMENT LC_MESSAGES", env_keep+="LC_MONETARY LC_NAME LC_NUMERIC LC_PAPER LC_TELEPHONE", env_keep+="LC_TIME LC_ALL LANGUAGE
    LINGUAS _XKB_CHARSET XAUTHORITY", secure_path=/sbin\:/bin\:/usr/sbin\:/usr/bin

User estrella may run the following commands on linux1:
    (ALL) ALL

```

- user-specific sudo configurations can also be saved in `/etc/sudoers.d/` 

  - `visudo` can be prompted to create & write to a specific file

  - ` # visudo -f /etc/sudoers.d/<user> `

  - it is good to set certain commands as being able to sudo & execute from anyone  `(ALL=ALL)`, while reserving others strictly to `root` i.e. `(ALL=root)` 

- **_sudo log_**: located in `/var/log/secure`   # what's in /var/log/messages? 

  - reveals who, what, and where (directories)









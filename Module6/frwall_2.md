## Linux Security: The Linux Firewall: Part 2 


### Additional Commands 

- Create Chains 

  - `iptables -t <table name> -N <chain name>`

- Delete Chains 

  - `iptables -t <table name> -X <chain name>`

### iptables/Netfilters Front-Ends 

- CentOS/RHEL

  - firewalld 

  - system-configure-firewall

  - **archaic/legacy**: iptables-services(now replaced by firewalld)  

- Unbuntu 

  - UFW (Uncomplicated FireWall) --> has a graphical interface 

### Example: Creating a "LOGDROP" Chain 

	using the `-m` option to utilize the `limit` module 

- called by the option handle/flag 

 ... to be continued ... 


### FirewallD!

> the default **dynamic firewall daemon** in RHEL7 & CentOS7 

#### Similarities with `iptables-services` 

- both utilize the **netfilter** kernel framework/module to operate

- both use the `iptables` command (native to system) to perform a large degree of their tasks

#### Differences b/w `firewalld` & `iptables-services` 

- firewalld can change settings during normal operations without losing existing connections, and does not need to restart after making each change.

- iptables-services store config files in `/etc/sysconfig/iptables`

- firewalld store config files in:

  - `/usr/lib/firewalld` 

  - `/etc/firewalld`

#### Operations 

- if not installed, install via 

`sudo yum install firewalld firewall-config` 

- check services 

`sudo systemctl status firewalld` 

- **Zones** 

- `firewall-cmd --get-active-zones`

```
[estrella@headnode ~]$ sudo firewall-cmd --get-active-zones
[sudo] password for estrella:
public
  interfaces: enp0s3 enp0s8
```

- list all info on specific zones

 -`firewall-cmd --get-active-zones`

```
[estrella@headnode ~]$ sudo firewall-cmd --zone=public --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh dhcpv6-client
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

``` 

- see available services

 - `firewall-cmd --get-services` 

```
# there are lots of default services whose reserved ports save the user from having to type in 
### Resources

[estrella@headnode ~]$ firewall-cmd --get-services
RH-Satellite-6 amanda-client amanda-k5-client bacula bacula-client bgp bitcoin bitcoin-rpc bitcoin-testnet bitcoin-testnet-rpc ceph ceph-mon cfengine condor-collector ctdb dhcp dhcpv6 dhcpv6-client dns docker-registry docker-swarm dropbox-lansync elasticsearch freeipa-ldap freeipa-ldaps freeipa-replication freeipa-trust ftp ganglia-client ganglia-master git gre high-availability http https imap imaps ipp ipp-client ipsec irc ircs iscsi-target jenkins kadmin kerberos kibana klogin kpasswd kprop kshell ldap ldaps libvirt libvirt-tls managesieve mdns minidlna mongodb mosh mountd ms-wbt mssql murmur mysql nfs nfs3 nmea-0183 nrpe ntp openvpn ovirt-imageio ovirt-storageconsole ovirt-vmconsole pmcd pmproxy pmwebapi pmwebapis pop3 pop3s postgresql privoxy proxy-dhcp ptp pulseaudio puppetmaster quassel radius redis rpc-bind rsh rsyncd samba samba-client sane sip sips smtp smtp-submission smtps snmp snmptrap spideroak-lansync squid ssh syncthing syncthing-gui synergy syslog syslog-tls telnet tftp tftp-client tinc tor-socks transmission-client upnp-client vdsm vnc-server wbem-https xmpp-bosh xmpp-client xmpp-local xmpp-server zabbix-agent zabbix-server 

# good practice: `firewall-cmd --get-services | grep "servicename*"

``` 

- [RHEL's firewalld docs & structural diagram on firewalld & iptables relations to netfilter](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/sec-using_firewalls) 




## PAM Configuration examples

### Restricting SSH-Sessions from Users

[tutorial link](https://www.tecmint.com/configure-pam-in-centos-ubuntu-linux/) 

[explanation module used](http://www.linux-pam.org/Linux-PAM-html/sag-pam_listfile.html) 

1.sudo modify `/etc/pam.d/sshd` 

2.add the following rules: 

```
auth	   required	pam_listfile.so onerr=succeed item=user sense=deny \
file=/etc/ssh/deniedusers

```

**_pam_listfile.so_**: a module designed to limit the privileges of specific users/accounts **based on the contents of specified files** 

**_onerr=_**: module (above) argument: what to do if an error is encountered: 

`onerr=succeed` --> if an error is encountered, PAM will return the authenication status of "PAM_SUCCESS." In this case, if no `deniedusers` were found, then the authenication process will proceed onto the next rule in the configuration file (user-restriction will be disabled) 

**_item=user_**: the item checked for authentication in the specified file will be username 

**_sense_**: action to take **if matching entries were found.** `sense=deny` --> if item (name in this case) found, then authentication will be denied 

**_file_**: absolute path to the reference file 

3.traverse to `/etc/ssh/` --> touch file `deniedusers`, and enter the names of accounts to be restricted.


[more detailed tutorial of pam_listfile.so](https://www.cyberciti.biz/tips/linux-pam-configuration-that-allows-or-deny-login-via-the-sshd-server.html) 
 

![alt-text](https://cdn-images-1.medium.com/max/533/0*ltCYgE0BcsaNw16b.png)
> Toplogy of PAM's interactions with aware-applications


### Detailed Components of PAM Library


- an API that acts as the interface between the application layer and the transport layer 

  - handles requests & instructions via sockets

- config files for PAM-aware applications 

  - located in `/etc/pam.d` 

  - each of which uniquely references modules listed that performs specific authentication tasks 

    - each module is a [**dynamically loaded library**](https://www.tecmint.com/understanding-shared-libraries-in-linux/) 

    - within each library, functions are called to perform authentication related tasks 


- `/lib64/security` --> Location of all of the PAM modules (collection is expandable)

 
### Essential PAM Modules

- `pam_securetty.so`

  - `auth` type module  

  - if user is attempting to login as `root`, the **tty** from which the user is logging in should be listed in the `/etc/securetty` 

- `pam_unix.so` 

  - `auth` type module 

  - `checks users username & password in `/etc/password` & `/etc/shadow` 

  - `nullok` option --> allows blank passwords 

- `pam_nologin.so` 

  - `auth` type module 

  - non-root users cannot login if `/etc/nologin` or `/var/run/nologin` files are present 

- `pam_unix.so` 

  - as an `account` module type 

  - account verification --> whether the account has expired or not, or has the password expired or not 

- `pam_pwquality.so` 

  - used to be `pam_cracklib` 

  - `account` module interface 

  - in the case that the old password has expired, the user will be prompted by this module to enter a new password; the new password's quality will be checked, e.g. if it is a dictionary word 

  - argument: `retry=3` --> allows the user to input his/her new password 3 times in case the initial attempt fails e.g. bad password quality 

- `pam_unix.so` 

  - as a `password` type module

  - arguments: `shadow` `nullok` `use_authtok` 

  - `shadow` use passwords in `/etc/shadow`, `nullok` allows users to change the password from a blank password (without this, the account with a blank password will be locked. `use_authtok` commands the module to NOT prompt for the password again (b.c. it'd been prompted by previous modules), and use the password gathered by modules above it. 


- `pam_unix.so` 

  - as a `session` type module 

  - keeps logs of when a user logs in or out of the system. 

- **info on pam modules**: `man pam_nologin` --> take away the `.so` extension when executing the `man` command.


- `pam_pwhistory.so` 

- `password required pam_pwhistory.so` --> this modules stores the previou    s 10 passwords used by each account in a /etc/< > file

- `remember=<#>` option --> tell module how many passwords to remember

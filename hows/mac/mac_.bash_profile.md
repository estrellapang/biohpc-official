## Configure .bash_profile Settings

- Typically located in /~  

### Command History 

`SHELL_SESSION_HISTORY=0`
 
`HISTFILESIZE` --> number of lines are stored at the startup time and in the history file at the end of your session (typically this should be large, i.e. 10000) )

`HISTSIZE` --> number of lines stored in memory while bash session is ongoing (this could be a smaller number, i.e. 1000-5000) 

--> the parameters above will let your bash shell save the command history 

- sample of current macbook air .bash_profile

```
# enable command history and specify number of commands to store
SHELL_SESSION_HISTORY=0
HISTFILESIZE=10000
HISTSIZE=5000

# enable coloring of the Terminal session
export CLICOLOR=1 

# specify colorscheme for the files in Terminal
export LSCOLORS=ExFxCxDxBxegedabagacad

# aliases
alias git_repo='cd /Users/zeng/Documents/practice/pushy/Learning_Linux/'

alias nucleus005='ssh zpang1@198.215.54.6'

alias nucleus006='ssh zpang1@198.215.54.58'

alias ldap002='ssh biohpcadmin@198.215.54.45'

alias lamella01='ssh biohpcadmin@198.215.54.118'

alias lamella02='ssh biohpcadmin@198.215.54.119'

alias wsc037='ssh biohpcadmin@198.215.56.37'

alias xymon='ssh biohpcadmin@198.215.49.64'

alias cuh_ems01='ssh gpfsadmin@10.10.137.20'

alias afm03='ssh root@10.10.137.8'

alias afm04='ssh root@10.10.137.9'

alias mds00='ssh biohpcadmin@198.215.54.49'

alias cloud017='ssh biohpcadmin@129.112.9.42'

```

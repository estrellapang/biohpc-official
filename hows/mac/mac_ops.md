## Good-to-Know Commands & Operations for MAC users 

### General & GUI Ops: 

`CMD + CTRL + q` --> lock screen

`CTRL ->/<-` --> switch desktops 

### Connecting to Servers/Shared Storage: 

- Mount shared storage on MACs (COMMAND Key + K —> “connect to server”) 

  - to connect to BioHPC’s shared storage:  `smb://lamella.biohpc.swmed.edu/project/biohpcadmin/shared/  (you will be prompted to log in as zpang1) 

  - we can also do smb://~/work/biohpcadmin/shared/ (or home2) 

	under `../project/biohpc/shared/`, you can find .ISO images of various operating systems!  

- Set up auto-mapping upon log in/boot 

  - [link1](https://setapp.com/how-to/map-a-network-drive-on-mac) 

  - [link2](https://blog.pcrisk.com/mac/12234-how-to-map-a-network-drive-on-mac) 

  - [keychain app](https://support.apple.com/guide/keychain-access/view-the-information-stored-in-a-keychain-kyca1085/10.5/mac/10.14) 

```
1. Manually map your network drive via "Finders" --> "Go" --> "Connect to Server" --> "smb://lamella.biohpc.swmed.edu/<user id>"  --> enter BioHPC password --> select "Remember this password in my keychain" 

2. "Settings" --> "Users & Groups" --> "Login Items": this will open a window for you to select an app or a directory --> select "lamella.biohpc.swmed.edu" under "Locations" --> select your home directory "s######" --> click "Add" 

  - if you do not see the new login item added to the list, close "Settings", reopen it & navigate to the login items window once more; it should then be listed as a "Volume" 

```

### Turn Off Terminal App's Flashes 

- go to "Terminal" --> "Advanced" --> Uncheck "Visual Bell"


### Remap Keyboard Layout via `hidutil`

- [useful mac developer link](https://developer.apple.com/library/archive/technotes/tn2450/_index.html)

- NOTE: all remapping have to be specified in one command (at least with `property --set` handles), repeating it with different mappings wipe the previous configurations

- e.g.: right SHIFT to ENTER and right ALT to POWER, respectively

```
hidutil property --set '{"UserKeyMapping": [{"HIDKeyboardModifierMappingSrc":0x7000000e5,"HIDKeyboardModifierMappingDst":0x700000028},{"HIDKeyboardModifierMappingSrc":0x7000000e6,"HIDKeyboardModifierMappingDst":0x700000066}]}'
UserKeyMapping:(
        {
        HIDKeyboardModifierMappingDst = 30064771112;
        HIDKeyboardModifierMappingSrc = 30064771301;
    },
        {
        HIDKeyboardModifierMappingDst = 30064771174;
        HIDKeyboardModifierMappingSrc = 30064771302;
    }
)
```

- the customized mapping will disappear upon reboot; made cron job to implement at reboot

```
# "@reboot" means to run upon reinitialization of system

crontab -l
@reboot /bin/bash /Users/zeng/Documents/key_remap.sh

  # the `key_remap.sh` contains the mapping of ENTER to right SHIFT, and right SHIFT to ENTER, and right OPTION to POWER

```

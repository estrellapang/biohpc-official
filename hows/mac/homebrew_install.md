## Get your Mittens on a MAC OS-based Package Manager 


### Installation Command:

`/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"` 


### If Installation not Successful: 

- in case of error message `/usr/local/Homebrew/.git: Permission denied Failed during: git init -q`

- solution 1. change permission for the directory for the currently logged in user

`sudo chown -R $USER:admin /usr/local`

- solution 2. remove previously installed git 

`rm -rf /usr/local/.git` 
`rm -r /usr/local/bin/git`
`rm -r /usr/bin/git` 

`which git` --> verify deletion

### Post Installation:

- for error `/usr/bin/git: No such file or directory`

- restart Terminal App 


### GCC Alreay Installed on Newer MACs? 

```
biohpc-air:~ zeng$ gcc --version
Configured with: --prefix=/Library/Developer/CommandLineTools/usr --with-gxx-include-dir=/Library/Developer/CommandLineTools/SDKs/MacOSX10.14.sdk/usr/include/c++/4.2.1

```

- however, when checked with `brew info gcc`

  - the output indicates that gcc is not installed! 

  - this is because Homebrew installs to a separate location from the Mac OS



### Homebrew locations ### 

- `/usr/local/Cellar/..` --> location of installed packages

- `/usr/local/Homebrew/..` --> configs


### No Paths to Installed Programs? ### 

- `brew unlink <program>`

- `brew link <program>`

  - e.g. 

```
 brew unlink node
Unlinking /usr/local/Cellar/node/7.10.0... 1 symlinks removed

sw500886:bin zeng$ brew link node
Linking /usr/local/Cellar/node/7.10.0... 7 symlinks created

```

### 03-11-2020: Installed `inetutils` (which includes FTP client)

```
brew search inetutils

brew install inetutils

---------------------------------------------
==> Installing dependencies for inetutils: libidn
==> Installing inetutils dependency: libidn
==> Downloading https://homebrew.bintray.com/bottles/libidn-1.35.mojave.bottle.tar.gz
==> Downloading from https://akamai.bintray.com/d3/d3741facdecc039b53d64392b6f8f4377a01d38bd0ce388db
######################################################################## 100.0%
==> Pouring libidn-1.35.mojave.bottle.tar.gz
==> Caveats
Emacs Lisp files have been installed to:
  /usr/local/share/emacs/site-lisp/libidn
==> Summary
🍺  /usr/local/Cellar/libidn/1.35: 72 files, 1.4MB
==> Installing inetutils
==> Downloading https://homebrew.bintray.com/bottles/inetutils-1.9.4_2.mojave.bottle.tar.gz
==> Downloading from https://akamai.bintray.com/cd/cd8d9c2d67518442b03bd4c6573a22408136fbfa54822db89
######################################################################## 100.0%
==> Pouring inetutils-1.9.4_2.mojave.bottle.tar.gz
==> Caveats
The following commands have been installed with the prefix 'g'.

    dnsdomainname
    ftp
    rcp
    rexec
    rlogin
    rsh
    telnet

If you really need to use these commands with their normal names, you
can add a "gnubin" directory to your PATH from your bashrc like:

    PATH="/usr/local/opt/inetutils/libexec/gnubin:$PATH"
==> Summary
🍺  /usr/local/Cellar/inetutils/1.9.4_2: 107 files, 2.6MB
==> `brew cleanup` has not been run in 30 days, running now...
Removing: /Users/zeng/Library/Caches/Homebrew/Cask/unetbootin--675.dmg... (9.9MB)
==> Caveats
==> libidn
Emacs Lisp files have been installed to:
  /usr/local/share/emacs/site-lisp/libidn
==> inetutils
The following commands have been installed with the prefix 'g'.

    dnsdomainname
    ftp
    rcp
    rexec
    rlogin
    rsh
    telnet

If you really need to use these commands with their normal names, you
can add a "gnubin" directory to your PATH from your bashrc like:

    PATH="/usr/local/opt/inetutils/libexec/gnubin:$PATH"

---------------------------------------------

```

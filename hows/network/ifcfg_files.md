## Interface Configuration(ifcfg-) Files

> The files that control software software interfaces for network devices 

### Ethernet Ifcfg Files: 

- Location: `/etc/sysconfig/network-scripts/ifcfg-eth#` 

  - " # " corresponds to the number associate with each file

  - each ' ifcfg-eth# ' file controls a **Network Interface Card**, or NIC, which is a network adapter 


- Depends on whether the IP address is static or DHCP assigned, the file parameters cold look different, especially in "BOOTPROTO" 

	` BOOTPROTO=static `
	` BOOTPROTO=dhcp ` 

### Configuration Parameters

`BOOTPROTO` --> boot protocols for this interface

	- OPTIONS: 1. static/none --> the adapter uses a fixed address
 
		   2. bootp/dhcp --> adapter sends request to DHCP (Dynamic Host Configuration Protocl) server

 
`BROADCAST` --> Broadcast IP address: with all 1 bits in the host identifer, or " .255 " Packets sent to this IP address will be sent to ALL devices on the subnet 

`DEVICE` -OR- `NAME` --> logical name of the device, i.e. "eth0" or "enp0s2" 

`DHCPV6C` --> whether to use DHCP to obtain an IPv6 address for the interfaceddevice 

	- OPTIONS: 1. yes 
		   2. no 

`DNS1` & `DNS2` --> primary & secondary DNS (domain name servers) 

	- typically primary = local DNS server address 
	- secondary = world-wide DNS server, e.g. 8.8.8.8 (Google's free DNS server)


	- NOTE: a tertiary is not supported in the ifcfg files
 
	- If parameter ignored --> DNS servers must be manually entered in `/etc/resolv.conf` in which 2+ DNS entries are allowed   

		- any DNS server listed in the ifcfg file will be added to the resolv.conf file 

`HWADRR` -OR- `MACADDR` --> Hardware/MAC-address: ONLY USE ONE OF THESE! 

	- format: > AA:BB:CC:DD:EE:FF  

	- every network device is assigned a MAC-address (operates at which OSI layer? ) 

	- typically automatically assigned, BUT WHAT IF NOT? HOW TO MANUALLY CHECK? --> a common way is `ifconfig` 

`TYPE` --> "Ethernet" this entry could actually hinder connectivity if not specified  

`ONBOOT` --> does the network on this device start upon booting device

	- `yes` network starts without a user logging in 
	- `no ` (typically default), ntwk only starts after a user logs in 

`GATEWAY` --> address of network router/ default gateway for subnet  

	- in application, gateways are what connects one subnet work to another; and physically it could be a router, firewall, or a proxy server, all of which while acting as a gate for a subnetwork, actually is considered a node itself. 

	- e.g.??? 

`IPADDR` --> the IPv4 address assigned to the adapter/NIC 

`NETMASK` --> the subnet mask, which is what the the router relies to locate which subnet a specific host is in (however, the info needs to be added to the routing table at some point in time to the router) 

`USERCTL` --> can non-sudoers start/stop this interface/adapter? (yes/no) 

`NM_CONTROLLED` --> is NetworkManager permiited to configure this device? (default yes, option to set to no) 

`MTU` --> **Maximum Transmission Unit**, which is the largest packet/frame that can be transmitted over a network; older standards included 1500 or 576 octets, or bytes, but for networks such as the one in BioHPC with 1G+ connections,MTU can be set to 8192 with jumbo frames enabled (thus network transmissions being improved) 

[read more about MTU here](https://searchnetworking.techtarget.com/definition/maximum-transmission-unit)

[read more about Jumbo frames here](https://searchnetworking.techtarget.com/definition/jumbo-frames) 

\
\
\
\
\
### DHCP CLIENT CONFIGS

- If your interface is utilizing DHCP for network configurations, then the **only parameters** that are required in the ifcfg file are: 

`BOOTPROTO ONBOOT HWADDR`

- Example of 1G Port on Workstation:

```
DEVICE="eth2"
BOOTPROTO="dhcp"
ONBOOT="no"
TYPE="Ethernet"
HWADDR="00:1b:21:dd:20:4e"

```

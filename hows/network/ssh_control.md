## How to Control SSH Access via TCP Wrappers & iptables 

- 

``` 
### DO NOT REMOVE `ssh` from allowed services in firewall-cmd--it will DROP ALL ssh connections REGARDLESS of any existing TCP wrapper rules ### 

# modify `/etc/ssh/sshd_config`

  # add/modified lines so root ssh access is blocked: 

`PermitRootLogin no`
`DenyUsers root`
`DenyGroups root`

# modify `/etc/hosts.allow` 

  # only allow one IP SSH access (could also do entire subnets!) 

`sshd,vsftpd : 198.215.56.1`

# modify `/etc/hosts.deny` 

  # deny all other IPs except ones listed above 

`sshd,vsftpd : 198.215.56.1` 

# modify `/etc/pam.d/sshd` to restrict allowed users

  # BEFOREHAND: create file `/etc/ssh/sshd.allow` --> enter allowed users 

  # add this line at the end of the `auth` stack in the SSH PAM config file 

`auth  required  pam_listfile.so item=user sense=allow file=/etc/ssh/sshd.allow onerr=fail`
```

- references:

  - [pam & sshconfig](https://www.thegeekdiary.com/centos-rhel-how-to-disable-ssh-for-non-root-users-allowing-ssh-only-for-root-user/)

  - [using iptables/firewalld](https://www.tecmint.com/block-ssh-and-ftp-access-to-specific-ip-and-network-range/) 

  - [details on TCP wrapper options](https://www.tecmint.com/secure-linux-tcp-wrappers-hosts-allow-deny-restrict-access/) 


### July 2019: Proposal for DMZ Security 


- **via `/etc/ssh/ssh_config`**


```

# no root login 

PermitRootLogin no


# only allow `biohpcadmin` to login

AllowUsers biohpcadmin 


# match for SSH connections from BioHPC networks

Match Address 127.0.0.1,198.215.49.*,198.215.51.*,198.215.54.*,198.215.56.*,198.215.60.*,172.18.224,*


# change SSH port [MAYBE] 

Port 1984 

```


- **Re-enforcement for VMs running RHEL6** (via iptables-services)


```
# check current iptables rules

sudo iptables -L INPUT --line-numbers -n

# New Rules:  drop all connections that are NOT coming from our internal networks 

iptables -A INPUT -i <interface name> -p tcp --dport <port of choice> ! -s 198.215.49.0/24 -j DROP
iptables -A INPUT -i <interface name> -p tcp --dport <port of choice> ! -s 198.215.51.0/24 -j DROP
iptables -A INPUT -i <interface name> -p tcp --dport <port of choice> ! -s 198.215.54.0/24 -j DROP
iptables -A INPUT -i <interface name> -p tcp --dport <port of choice> ! -s 198.215.56.0/24 -j DROP
iptables -A INPUT -i <interface name> -p tcp --dport <port of choice> ! -s 198.215.60.0/24 -j DROP
iptables -A INPUT -i <interface name> -p tcp --dport <port of choice> ! -s 172.18.224.0/24 -j DROP
iptables -A INPUT -i <interface name> -p tcp --dport <port of choice> ! -s 127.0.0.1/24 -j DROP


# drop all other port 22 SSH connections 

iptables -A INPUT -i <interface name> -p tcp --dport 22 -j DROP

sudo iptables -D INPUT [rule number]  ## Delete previous rule allowing all connections to port 22


# save new iptables rules 

sudo iptables-save -t filter 


# verify:  

sudo iptables -L INPUT --line-numbers -n

```


- Re-enforcement for VMs running **RHEL7** (via firewalld)


```
### use rich rules to whitelist subnets for connections to port 22 (or whatever port we decide SSH will use) 

sudo firewall-cmd --permanent --add-rich-rule 'rule family="ipv4" source address="198.215.56.0/24" port port=22 protocol="tcp" limit value=21/m accept'

sudo firewall-cmd --permanent --add-rich-rule 'rule family="ipv4" source address="198.215.54.0/24" port port=22 protocol="tcp" limit value=21/m accept'

sudo firewall-cmd --permanent --add-rich-rule 'rule family="ipv4" source address="198.215.51.0/24" port port=22 protocol="tcp" limit value=21/m accept'

sudo firewall-cmd --permanent --add-rich-rule 'rule family="ipv4" source address="198.215.49.0/24" port port=22 protocol="tcp" limit value=21/m accept'

sudo firewall-cmd --permanent --add-rich-rule 'rule family="ipv4" source address="198.215.60.0/24" port port=22 protocol="tcp" limit value=21/m accept'

sudo firewall-cmd --permanent --add-rich-rule 'rule family="ipv4" source address="172.18.224.0/24" port port=22 protocol="tcp" limit value=21/m accept'
 
 # protocol must be included in the command options

 # `limit value=21/m` --> allow no more than 21 connections per minute

 # be sure to include the subnet from which you've SSHed into the server, otherwise your session could be interrupted once the new rules have been implemented



# Remove firewall-cmd rule allowing all SSH connections

sudo firewall-cmd --permanent --remove-service=ssh


# Restart firewalld

sudo firewall-cmd --reload

# Verify

sudo firewall-cmd --list-all --zone=public

 # Attempt to SSH from allowed subnets via `biohpcadmin` & `root` 

 # SSH from blocked subnets  


```

- NOTE on SSH errors: 

  - if `ssh_exchange_identification`: read: Connection reset by peer` --> denied due to SSHD or TCP wrappers 

  - if `Connection to ###.###.###.### closed` --> indicates iptables/firewall blocking the port 

  - if no response for > 20 seconds --> connection to port dropped 

- Additional References:

  - [firewalld_rich_rules_1](https://linuxcluster.wordpress.com/2019/07/23/using-firewall-cmd-rich-rules-to-whitelist-ip-address-range/) 

  - [firewalld_rich_rules_2](https://www.linode.com/docs/security/firewalls/introduction-to-firewalld-on-centos/) 

  - [firewalld_rich_rules_3](https://www.rootusers.com/how-to-use-firewalld-rich-rules-and-zones-for-filtering-and-nat/) 

  - [firewall_rich_rules_4](https://www.computernetworkingnotes.com/rhce-study-guide/firewalld-rich-rules-explained-with-examples.html) 

  - [firewall_rich_rules_5](https://major.io/2014/11/24/trust-ip-address-firewallds-rich-rules/) 

  - [general SSH security measures](https://unix.stackexchange.com/questions/406245/limit-ssh-access-to-specific-clients-by-ip-address) 



- Note on DMZ VMs' **Firewall Types**: 

  - firewalld 

    - flash

    - lce 

    - portal 

  - iptables  

    - ansir 

    - SAP 

    - service

    - cloud

    - git 


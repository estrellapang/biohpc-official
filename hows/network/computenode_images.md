## Installing RHEL Images onto Compute Nodes (Data Hall Task) 


### Pulling BiohHPC Modified RHEL Image: 

- Turn on the server (button typically located on the far left side of the back panel, and it is commonly a tiny plastic tactile button) 

  - if server already turned on, "CTRL + ALT + DEL"  to reboot 

- Select "Manually Select Node" --> naviage to the desired un-assigned node name  --> Confirm --> Confirm --> "Full install Mode" 

### Installing a clean Image via External Optical Drive: 

- Before Power on --> go to the front panel of node, and connect the optical drive containg the DVD on which the clean RHEL .iso file is burned 

- Power ON/Reboot --> "F11" to go into Boot Manager --> select "One shot" --> "Optical Drive" 


- If system wants to to PXE (over-the-network) boot after image installation: 


- Reboot --> "F2" on boot screen --> "Sys Bio" --> "Boot settings" --> place "Drive" at the top of the options list --> Uncheck the "NIC adapter" box --> leave "hardware" box checked  

- Once the proper (base-install) image loads,

```bash

vi etc/sysconfig/network 

GATEWAY=172.18.239.254
HOSTNAME=arbitrary 
NETWORKING=yes 

:wq 


vi etc/sysconfig/network-scripts/ifcfg-en0ps0

ONBOOT=yes
TYPE=Ethernet
DEVICE=eth0 
IPADDR=172.18.239.[] 
NETMASK=255.255.255.0
BOOTPROTO=static
GATEWAY=172.18.239.254

```

### Configuring iDRAC: 

**root password required**

- As the boot screen is loading "F2" --> "iDRAC" setting --> "Network" -->  "IP"  

- "static ip" --> "192.168.0.250" & "gateway" --> "192.168.0.250" 

- "userconfiguration" --> "biohpcadmin" 

 

## General Bash Commands used in Dianosing & Configuring Networking 

`traceroute` 

`tracepath` 

`ethtool [NIC name]` --> shows connection speed of NIC ports

`nmap -sP ###.###.###.0/24` --> run as root to sniff out MAC addresses of all IP addresses on the subnet

  - [reference](https://serverfault.com/questions/245136/how-to-find-out-mac-addresses-of-all-machines-on-network)

[common networking commands](http://techgenix.com/top-11-networking-commands/) 

[ip, route, and netstat](https://www.cyberciti.biz/faq/how-to-find-gateway-ip-address/)

[tecmint on networking commands](https://www.tecmint.com/linux-network-configuration-and-troubleshooting-commands/)

[snpwalk - scan ARP and Mac addr tables](https://networkengineering.stackexchange.com/questions/2900/using-snmp-to-retrieve-the-arp-and-mac-address-tables-from-a-switch)
\
\

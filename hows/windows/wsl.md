## Setting Up WSL and What Not

### Setting Up WSL 2 

- **look for Digital Ocean tutorial**

### Troubleshot

- How to limit the RAM and CPU resources of VM

  - otherwise a process "Vmmem" will eat up all the RAM

  - [reference](https://medium.com/@lewwybogus/how-to-stop-wsl2-from-hogging-all-your-ram-with-docker-d7846b9c5b37)

## Useful CLI Tools to Use to Windows10 cmd

### Networking cmds

- [view arp table](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/arp)

- [ipconfig /all](https://www.digitalcitizen.life/command-prompt-advanced-networking-commands/)


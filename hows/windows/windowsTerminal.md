## Guide to Customizing The Windows Terminal Emulator

### Key-bindings for Navigation Actions

- [official guide](https://docs.microsoft.com/en-us/windows/terminal/customize-settings/actions)

\

#### Standard Keys

- **PROTIP**: Avoid Mouse Scrolling--the backscroll buffer gets weird with this app!

- open new tab: `SHIFT + CTRL + T`

- switch tabs: `CTRL + TAB`

- open new pane to the right: `SHIFT + ALT + =`

- open new pane to the left: `SHIFT + ALT + -`

- close pane/current window: `SHITFT + CTRL + W`

- switch between panes: `ALT + ARROWS`

- change pane sizing: `SHIFT + ALT + ARROWS`

- ZOOM: `CTRL +/-`

- UP and DOWN pages: `SHIFT + CTRL + PGUP/PGDOWN`

- CLOSE TAB: `CTRL + D`

\
\

### Theming

- [very thorough and fun guide](https://medium.com/@bhavsec/getting-started-with-windows-new-terminal-and-wsl-6b8fbd10ce17)

- [where to save custom backgrounds](https://www.howtogeek.com/426346/how-to-customize-the-new-windows-terminal-app/)

- [fetch HEX color codes](https://www.colorschemer.com/hex-color-codes/)

- [apply retro terminal effect](https://www.freecodecamp.org/news/windows-terminal-themes-color-schemes-powershell-customize/)

- [disable ls directory highlighting](https://blog.jongallant.com/2020/06/wsl-ls-folder-highlight/)

\
\

#### Cursor Color and Shape

```
"cursorColor": "#98FF00",
"cursorShape": "vintage",

  # the comma character is important for JSON object to be parsed
  # cursorColor only understand RGB color in HEX Color codes
```

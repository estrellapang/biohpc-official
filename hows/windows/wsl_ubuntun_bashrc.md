# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=100000
HISTFILESIZE=200000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    #PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;36m\]\w\[\033[00m\]\$ '
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\D{%m-%d-%I:%M%p}\[\033[00m\]:\[\033[01;36m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/estrella/miniconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/estrella/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/home/estrella/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/estrella/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<
#
#
# Custom Aliases
alias gitgud='cd /mnt/c/Users/estrella/Documents/HPC_Journey'
alias pyup='cd /mnt/c/Users/estrella/Documents/HPC_Journey/scripts/python'
alias outworld='cd /mnt/c/Users/estrella/Desktop/work'
alias lustre_utils='cd /mnt/c/Users/estrella/Documents/BioHPC_gitDocs/lustre_utils'
alias nucleus006='ssh zpang1@198.215.54.58'
alias nucleus005='ssh zpang1@198.215.54.6'
alias ldap002='ssh biohpcadmin@198.215.54.45'
alias wsc037='ssh biohpcadmin@198.215.56.37'
alias testcloud='ssh estrella@198.215.56.120'
alias lamella01='ssh biohpcadmin@198.215.54.118'
alias lamella02='ssh biohpcadmin@198.215.54.119'
alias lamellaSE='ssh biohpcadmin@198.215.54.13'
alias lsfmaster='ssh root@172.18.239.214'
alias ems01='ssh root@172.18.239.50'
alias afm01='ssh root@172.18.239.51'
alias afm02='ssh root@172.18.239.52'
alias afm03='ssh root@10.10.137.8'
alias mds00='ssh biohpcadmin@198.215.54.49'
alias sfa00='ssh user@192.168.54.196'
alias sfa01='ssh user@192.168.54.197'
alias bastion='ssh root@172.18.239.212'
alias starkist='ssh root@172.18.239.230'
alias starkist2='ssh root@172.18.239.231'

########################################################
#
# Custom Environment Variables & BASH Configs

# limit prompt to show the last 1 dirs
PROMPT_DIRTRIM=1

# disable directory highlight for `ls`
LS_COLORS=$LS_COLORS:'ow=1;34:' ; export LS_COLORS

#### REFERENCES ####

# [customize prompt to show time stamp](https://alexanderallen.medium.com/customizing-my-wsl-bash-prompt-374daf85a7ae)

# [all color codes for BASH](https://www.cyberciti.biz/faq/bash-shell-change-the-color-of-my-shell-prompt-under-linux-or-unix/)

## Windows Tricks

### Recover/Change Lost Password

- burn Hiren's BootCD PE into a bootable flash drive

  - [download link](https://www.hirensbootcd.org/download/)

\

- to boot windows via a boot device

  - on login screen, "Power" --> select "Restart" WHILE holding down SHIFT key

  - once in the "Advanced Options" menu, choose "Select a Device" --> "flash drive" --> "UEFI" (this will reboot into the Hiren's bootable OS)

\

- inside Hiren's OS:

  - open "utilities" folder --> "Security" --> "Passwords" --> "NT Password Edit"

  - unlock users, change password

  - shutdown OS, and you will reboot into the regular windows os installed on the harddrive

\

- extra links on resetting Windows passwords

  - [for win 8](https://www.lifewire.com/how-to-reset-a-windows-8-password-2626230) 

  - [older version of Hiren's disk](https://www.youtube.com/watch?v=J9b0EYdSKTs)

  - [for win 10](https://adamtheautomator.com/reset-windows-10-password-command-prompt/)

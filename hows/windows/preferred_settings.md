## Win 10 Setup

### Optimize Performance

#### Reduce GUI Overhead

"Settings" --> search "Adjust the appearance of Windows" --> "Visual Effects" --> "Custom" (uncheck everything except "Smooth edges of screen fonts")

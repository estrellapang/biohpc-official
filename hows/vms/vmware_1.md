## Intro to VMware


### Relevant Links 

[what is?](https://www.techopedia.com/definition/25979/vmware-esxi-server) 


### Storage Concepts and Ops 

[different virtual harddisk types explained](https://docs.vmware.com/en/VMware-vSphere/7.0/com.vmware.vsphere.vm_admin.doc/GUID-F4917C61-3D24-4DB9-B347-B5722A84368C.html)

[parted utility details](https://www.digitalocean.com/community/tutorials/how-to-partition-and-format-storage-devices-in-linux)

[LVM essentials](https://www.tecmint.com/extend-and-reduce-lvms-in-linux/) 

[expand vHD in VMware 1](https://pubs.vmware.com/workstation-9/index.jsp?topic=%2Fcom.vmware.ws.using.doc%2FGUID-73BEB4E6-A1B9-41F4-BA37-364C4B067AA8.html) 

[expand vHD in VMware 2](https://www.thomas-krenn.com/en/wiki/Increasing_the_size_of_a_virtual_disk_in_VMware) 

[add new vHD 1](https://pubs.vmware.com/vsphere-51/index.jsp?topic=%2Fcom.vmware.vsphere.vm_admin.doc%2FGUID-2CFBCCD5-B53D-42CD-AECD-FBC46AAC4AC5.html) 

[add new vHD 2](https://pubs.vmware.com/vsphere-51/index.jsp?topic=%2Fcom.vmware.vsphere.vm_admin.doc%2FGUID-F4917C61-3D24-4DB9-B347-B5722A84368C.html) 



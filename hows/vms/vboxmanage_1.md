## Managing VirtualBox VMs via CLI

### Resources

- [Oracle's VBOX 6  VBoxManage Encyclopedia](https://docs.oracle.com/cd/E97728_01/E97727/html/vboxmanage-intro.html)

- [Oracle VBoxManage Documentation for VBox v6.1](https://docs.oracle.com/en/virtualization/virtualbox/6.1/user/vboxmanage.html)

- [VBoxManage modify VM NIC properties](https://docs.oracle.com/en/virtualization/virtualbox/6.0/user/network_performance.html)

### Basic Ops

- check running vms:

```
VBoxManage list runningvms
```

\

- launch headless:

```
VBoxManage startvm --type=headless <vm name>
```

\

- shutdown vms:

```
VBoxManage controlvm cloud_18 poweroff
```

\

- fetch vminfo:

```
vboxmanage showvminfo <vm name>
```

\

### Snapshots 

- list all snapshots for a VM:

```
VBoxManage snapshot <VM Name> list

```

### Register/Add/Delete VMs

- [register cmd](https://superuser.com/questions/745844/how-can-i-import-an-existing-vbox-virtual-machine-in-virtualbox)

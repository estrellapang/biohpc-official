## New VMs & Local Networking


### Repeat Steps for EVERY New VM 

- **On Physical Host**

  - BIOS --> Enable Virtualization in CPU's settings

- New VM Itself:

1. assign static IP 

2. assign the same hostname in both `/etc/hosts` and `/etc/hostname` 

3. reboot 

4. ping other vms & vm host

5. test if can mount shared storage from file server vms

6. If GUI Installed: install GUEST ADDITIONS via command line! (or sometimes the dynamically allocated storage may fail!!!) 

7. Install EPEL repo, install Vim, net-tools 

  - for VMs with tons of storage, try fixed-size disks! 

&nbsp;

- Existing VMs: 

1. add new VM static address in `/etc/hosts` 

2. add new host to the `/etc/exports` (if the VM is a file server)

3. ping the new vm 


### How to move VMs to `/project` (or anywhere shared directory) 


``` 

#### first copy all relevant files from home2 ####

- change into the shared fs directory where one wishes to store his/her VMs

-  create a designated machine folder & `cd` into it

`cd /project/biohpcadmin/zpang1/VirtualBox_VMs/<machine folder>`

- copy all relevant files from original VM directory into the current one 

  - `cp -rpv ~/VirtualBox\ VMs/data1/* ./` 

  -  `-rpv` --> recursive copy, preserve permissions and access dates, and verbose 

``` 

####  Get on VirtualBox, remove the previously copied virtual machine (on VBox GUI) ####

  - then reinforce the delete (if VirtualBox doesn't delete everything) in CLI

  - `rm -r ~/VirtualBox\ VMs\data1`

  - verify `du -sh ~/VirtualBox\ VMs\` --> the amount of space used should be less now

#### Reincorporate moved VM back to VirtualBox #### 

 "Add" --> browse into the new directory --> open the `.vbox` file 

``` 

### Deleting Virtual Drives 


``` 
1. delete everything in Vbox GUI 

2. go to VM's directory, and remove disk related files

data1.vbox  data1.vbox-prev  data1.vdi  drive_base.vdi  Logs  mdd1.vdi  mdd2.vdi  mdd3.vdi  mdd4.vdi
[zpang1@biohpcwsc037 data1]$ 
[zpang1@biohpcwsc037 data1]$ 
[zpang1@biohpcwsc037 data1]$ 
[zpang1@biohpcwsc037 data1]$ 
[zpang1@biohpcwsc037 data1]$ rm mdd1.vdi mdd2.vdi mdd3.vdi mdd4.vdi 
[zpang1@biohpcwsc037 data1]$ ls
data1.vbox  data1.vbox-prev  data1.vdi  drive_base.vdi  Logs

```

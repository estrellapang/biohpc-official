## Good Ol' LAMP Stack in Web Dev: PHP 

### PHP: At a Glance

- `P` portion of the LAMP stack --> primarily used to deliver "dynamic content"(specifically HTML)

  - `P` can also be "Python" or "Perl" (older) 

  - html is static and needs to be manually changed

- PHP is a scripting language (relies on other language to output) that & works on the **SERVER side**

  - any code/work is FIRST done on the server, and the output is downloaded by the client/browser 

  - if it was pure HTML, the page downloaded would really be the originally written script

- **Client Side** scripting languages runs on the **CLIENT/BROSWER side**

  - primarily used to enable user interaction with downloaded HTML elements


### More Details on PHP in Web Design 

- Designed specifically to script for web pages
 
  - code is written embedded in the HTML page, & the **PHP Processor** scans the page for "<?php" & "?>" characters to execute and finish the PHP script

  - can deliver other outputs e.g.charts & pictures by querying databases (commonly MYSQL)

  - PHP can be embedded in HTML, CSS Javascript, and XML, etc.

  - so long as PHP is embedded, the file extension will have to be `.php`

- How PHP queries Databases

  - Starts with HTTP Requests---the Apache Server receives a request, and if the request involves a PHP file:

    - the PHP script within the file is executed--ouput is generally HTML

    - any HTTP requests the PHP processor will handle

    - PHP handles the following HTTP Request verbs: CREATE, READ, UPDATE, and DESTROY 

  - How does PHP know which database?

    - in the PHP SCRIPT, the SQL database must be specified!

      - `mysqli_connect` is an example of the API/PHP function used to access the SQL database 

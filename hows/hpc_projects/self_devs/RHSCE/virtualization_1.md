## 

### RHEL Uses KVM

- Kernel-based Virtual Machine --> **Hypervisor** 

- Quick Emulator (QEMU) --> **device emulator** 

- Atrributes of KVM

  - Overcommitting --> allowing more than what exists on the host: "dynamica allocation" concept 

  - disk I/O throttling --> limit amount of disk resources used by VM

  - CPU hot-add 

  - Nest-Virtualization 

  - Management via "libert API" --> management tools i.e. below that comms to KVM hypervisor via this API

    - GUI: "virt-manager" 

    - CLI: "virsh"

  - Types of Virtualization in KVM

```
## Fully Hardware-Virtualized CPU

 # instructions from guest to host CPU pass through directly, no significant slow down

 # guest sees same type of CPU as host

\

## Paravirtualized

 # hypervisor relays instructions from guest to host

 # faster than full emulation b.c. no translation by hypervisor from not emulating pieces of hardware

 # requires paravirtualized drivers installed on guest

 # e.g. Network Cards (virtio-net), Block devices (virtio-blk), controllor devices (virtio-scsi), serial devices (virtio-serial), graphics cards (QXL) 

\

## Emulated 

  # SLOWEST, guest will load the respective drivers for the hardware being virutalized

  # guest driver --> QEMU --> Host Drivers --> Host Hardware  (extra translation step)

  # e.g. PCI bridges, mouse & keyboard, USB connections, serial ports, IDE block devices, sound devices, video cards, network adapters

\

## Shared/Passthrough

  # appears native to VM, as if it we real hardware, but passed through via hypervisor

    # e.g. a PCI card device passed to one VM, but the actual device will NOT be available to host & other VMs

  # devices run at native speed, but less flexibility

  # e.g. USB, PCI cards, PCIe selective function pass through, SCSI cards


```

\

- **install kvm**

```

yum install qemu-kvm libvirt(library needed to communicate with hypervisors) virt-manager libvirt-client (includes virsh CLI tool)

-OR-

yum group install "Virtualization Client" 

systemctl start/enable libvirtd 

reboot

```







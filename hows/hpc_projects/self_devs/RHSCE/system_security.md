## Security Configurations Enterprise Linux 

### Authentications & Credentials
 



### Logs

`w` --> show login since machine has been on 

`last` --> show all logins in history 

`/var/log/messages` --> root/sudo view-rights: contains records of a variety of events, such as the system error messages, **system startups and shutdowns**, change in the network configuration, etc. 

[more logs here](https://www.thegeekstuff.com/2011/08/linux-var-log-files) 

[difference b/w major logs](https://askubuntu.com/questions/26237/difference-between-var-log-messages-var-log-syslog-and-var-log-kern-log) 


### Changing Passwords using SSH 

`ssh <user whose passwd will be changed>@<IP/hostname> passwd` 

- old password will be requested & new password will be entered twice 

- a nice way to change password without logging in

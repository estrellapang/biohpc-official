## Basic Maneuvers in CentOS / RHEL's GNOME desktop

> plus some tips in bash

### Display 

- Cursor unable navigate across the screens (and if multiple desktop feature does not work)

  - "Settings" --> "Displays" --> "Arrange Combined Displays" --> Switch the physical placement of screens

  - clicking each screen icon, more options are available: 

     -  you can also rotate the screen by 90 or 180 degrees 


- `F11` --> enable/disable overhead menu bar

  - in some apps it's to go into full screen


### Logging In & Stuff

`startx` --> go into GUI mode 

`CTRL + ALT + F2` --> go into one of the tty console windows (CLI mode) --- `~ + ~ + F7` to exit (need to confirm)? 

`yum list installed | grep -i "[ ]"` --> search among installed packages

`super/windows key + l` --> lock screen (for RHEL 7) 


### In Bash Terminal 

`CTRL + R` --> search recursively through command history 

- `CTRL + R` repeatedly --> toggle among results

- `SHIFT + CTRL + R` repeatedly --> reverse toggle among results

- `CTRL + G` / `CTRL + C` --> quit search

- `SHIFT + ALT + >` --> **end of bash history**

### Text Editing 

- compare differences in text files: 

 - `diff [file 1] [file 2]`

- open a new tab in "Terminal" 

 - `SHIFT+CTRL+T`

 - `CTRL+PageUp/PageDown` --> switch b/w tabs


### List Hardware Info

-**GPUs** 

  - [link1](https://www.binarytides.com/linux-get-gpu-information/) 

  - [link2](https://stackoverflow.com/questions/10310250/how-to-check-for-gpu-on-centos-linux)


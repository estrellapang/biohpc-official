## Essential Provisioning Settings

### Basic Server Drive Partitioning

- `/boot` --> location of kernel images

- `/` --> OS 

- `SWAP` --> 2x or = DRAM



### Kickstart Basics

- created by RHEL --> automated installation systems

  - Debian & Unbuntu support it

- Overview of functions:

  - create partitions, users, network settings, & software

- **NOTE**: inside every RHEL + CentOS installation, a kickstart file is stored in `/root` in case we wish to repeat installation quickly --**/root/anaconda.ks.cfg** 

- **Requirements for Kickstart Script to Work**

  - ISO Image (CD or shared over network via NFS,FTP,HTTP)

  - (for VMs) --> a VM disk image

  - kickstart file

  - delivery method for kickstart script

    - hosted on http/ftp server, whose URL passes to installer    

    - `--extra-args="ks=<http/ftp>://192.168.54.3/kc.cfg" (virt-install handle) 
 
    - inject kickstart file into RAM disk (small disk in RAM holds essential files and drivers for systems to boot), this avoids over-the-network kickstart file sharing

    - `initrd-inject=centos7-ks.cfg --extra-args="ks=file:/centos7-ks.cfg"  (virt-install handle)


- e.g. kickstart file

```
## System authorization information
auth --enableshadow --passalgo=sha512   # password hashing algorithm

## Use CDROM installation media 
cdrom

## Use graphical/text install
#graphical
text   # will still need to accept license at the end of installation

## Run the Setup Agent on first boot
firstboot --enable
ignoredisk --only-use=vda   # tells installer to ignore all drives except "vda" which was a virtual drive

## Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'

## System language
lang en_US.UTF-8

## Network information
network  --bootproto=dhcp --device=enp0s25 --onboot=off --ipv6=auto --no-activate
network  --hostname=rhcsa

## Root password   username: root, password:password - use grub-crypt --sha-512 to create a new password
rootpw --iscrypted $6$ECqZP9cuaLnmEKgw$YVwZ2MgqIgw1Zb5SSDqIykc9469sH2qsYG9qARPs2jTFC3YsCx5E5F.nP3D4gke4xGlwnngKjSxO/vRoFpxlT1

  # the encrypted hash can be generated using: `echo -n 'password' | openssl sha512 | awk '{print $2}'`


## System services
services --enabled="chronyd"
services --enabled="acpid"

## System timezone
timezone America/Vancouver

## Create first username: user1, password:password - use grub-crypt --sha-512 to create a new password
user --groups=wheel --name=user1 --password=$6$mz8E2/m5Xun3bur0$ezkuRC0ClDZWqU5C0581r/Sjq0EtMUxnojFJJPlJ.ayLirPVzSP3uqfR63R3JQkzG49WSrm4.TsPvj6JBThaa0 --iscrypted --gecos="User One"

## X Window System configuration information
xconfig  --startxonboot

## System bootloader configuration
bootloader --append=" crashkernel=auto" --location=mbr --boot-drive=vda "console=tty0 console=ttyS0,115200"

## Clears all partitions on drive
clearpart --all --initlabel --drives=vda

## Manual Disk partitioning information
#part /boot --fstype="xfs" --ondisk=vda --size=1024
#part pv.247 --fstype="lvmpv" --ondisk=vda --size=121079
#volgroup studio --pesize=4096 pv.247
#logvol /  --fstype="xfs" --size=20480 --name=root --vgname=rhcsa
#logvol swap  --fstype="swap" --size=1996 --name=swap --vgname=rhcsa
#logvol /home  --fstype="xfs" --size=98600 --name=home --vgname=rhcsa

## Auto partitioning
autopart


%packages
@^graphical-server-environment
@base
@core
@desktop-debugging
@dial-up
@fonts
@gnome-desktop
@guest-agents
@guest-desktop-agents
@hardware-monitoring
@input-methods
@internet-browser
@multimedia
@print-client
@x11
acpid
chrony
kexec-tools

%end

%addon com_redhat_kdump --enable --reserve-mb='auto'

%end

%anaconda
pwpolicy root --minlen=6 --minquality=50 --notstrict --nochanges --notempty
pwpolicy user --minlen=6 --minquality=50 --notstrict --nochanges --notempty
pwpolicy luks --minlen=6 --minquality=50 --notstrict --nochanges --notempty
%end
```

\
\

### GRUB Basics 

- **change grub settings**

- implement changes to `/etc/default/grub`  --> `sudo grub2-mkconfig -o /boot/grub2/grub.cfg`

\
\

- **configure & choose kernel to boot from** 

- check default boot kernel:

  - `sudo grep -i "menuentry '" /etc/grub2.cfg | cut -d' ' -f2-6` 

  - topmost entry is the default boot kernel, e.g.

```
[estrella@vagrant ~]$ sudo grep -i "menuentry '" /etc/grub2.cfg | cut -d' ' -f2-6

'CentOS Linux (3.10.0-1127.el7.x86_64) 7 (Core)'    
'CentOS Linux (0-rescue-55b41cabce4eaa4f951e7ebb7686a40f) 7 (Core)'

```

- set default boot kernel (persistently)

  - `grub2-set-default <#>`   

\
\


### Systemd & `systemctl` cmd

- **What is...** 

  - systemd is the 1st process started by the kernel for Enterprise Linux OS versions 7 & up

  - manages all other system services, devices, system timers, & boot targets (all considered types of "objects" or "Units" --> for each unit, a unit file exists for configuration)

    - `systemctl` manages system units

  - "run-levels" / "boot targets" --> booting into different OS configurations, with different combinations of SERVICES will be running---can be changed on-the-fly

- COMMAND for: see all unit-files for services

  - `systemctl list-unit-files -at service`

    - this does not check the running status of services, only startup states

  - 3 general statuses: enabled/disabled/static --- the latter indicates that services being described cannot/not meant to start automatically"

- COMMAND: detailed info on system units

  - `systemctl list-units -at service`

  -  `list-units` by default shows **enabled** services

  -  `-a` shows enabled && inactive services   

```
 #NAME#                                  #config file loaded?#    ## status ##    # what is it #
  UNIT                                                  LOAD      ACTIVE   SUB     DESCRIPTION
  auditd.service                                        loaded    active   running Security Auditing Service
  cpupower.service                                      loaded    inactive dead    Configure CPU power related settings
  crond.service                                         loaded    active   running Command Scheduler

```

- COMMAND: show ONLY running services

  - `systemctl list-units -t service --state running` 


- COMMAND: show each service's configuration

  - `systemctl cat <service name>`

  - useful for showin behavior and attributes of services

```
sudo systemctl cat tuned
# /usr/lib/systemd/system/tuned.service
[Unit]
Description=Dynamic System Tuning Daemon
After=systemd-sysctl.service network.target dbus.service
Requires=dbus.service polkit.service    ## DEPENDENCIES ##
Conflicts=cpupower.service          
Documentation=man:tuned(8) man:tuned.conf(5) man:tuned-adm(8)

[Service]
Type=dbus
PIDFile=/run/tuned/tuned.pid
BusName=com.redhat.tuned
ExecStart=/usr/sbin/tuned -l -P   ## LAUNCH COMMAND ##

[Install]
WantedBy=multi-user.target   ## Operates on which target##

```

- COMMAND: PREVENT services from BEING STARTED

  - `systemctl mask <service>` 

  - `systemctl unmask <service>` 

  - additional layer of control ontop of `start/stop/enable/disable`


- COMMAND: check default systemd target 

  - `systemctl get-default`

- change default target../hpc_projects/self_devs/RHSCE/

  - `systemctl set-default <graphical/multi-user>`

  - "multi-user" target --> no GUI, CLI only

\
\
\

### Base Networking 

- Interface Naming Convention:

```
Firmware/BIOS supplied		eno<#>
PCIe slots			ens<#>
PCI slots			enp<#>s<#> 
no firmware info`		eth<#>
```

- `/etc/resolv.cnf`

  - a global configuration

  - different interfaces could use different DNS servers by setting directly in interface config files

\
\

- Essential Network Commands

  - `ip` --> live network interfacce manangement

    - changes are not persistent unless changed via config file or `nmcli` or `nmtui`

  - `nmcli` --> network manager CLI

  - `nmtui` --> visual tool for network manager

  - `nm-connection-editor`  --> GUI based tool
 
\
\

- Time Management

 - `timedatectl` --> displays local time, universal time, and real time clock (RTC) time, which is the hardware clock

   - also shows if NTP is enabled or not

 -  

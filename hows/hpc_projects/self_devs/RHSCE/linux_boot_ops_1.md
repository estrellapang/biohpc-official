## Boot Operations

### Boot into Systemd Emergency Target

- this mode requires root password

```
on boot GRUB menu, "e" on selected kernel --> go to line starting with "/vmlinuz" (specifying kernel parameters)  --> append "system.unit=emergency" --> "CTRL + x" to boot into emergency target

once booted inside emergency target --> "journalctl -xb" to examine boot logs; THERE ARE NO NEW KERNEL MESSAGES for this emergency boot

# to reboot into default target 
CTRL + d/systemctl reboot/systemctl default 

```

\
\

### Boot into Recovery Mode & Reset Root Pass

```
# boot into correct target/kernel configuration

GRUB --> "e" --> "/vmlinuz" --> append "rd.break" --> CTRL + x 

# in this recovery mode, the root filesystem is mounted with /sysroot being read-only 

# to change pass, change /sysroot mount to read & write

`mount -o remount,rw /sysroot` 

# change into chroot shell with /sysroot as root filesystem

`chroot /sysroot`  --> to `ls` before and after to confirm difference

# change root pass --> `passwd` 

# alllow SELinux to re-write security context of all files on next reboot (or else one may not log in as root) --> `touch /.autorelabel`

# exit chroot shell and recovery mode "exit" --> "exit" 

  # this will either auto reboot, or continue booting into default target---in the latter case, we will need to reboot again for SElinux to apply changes to security context 

```

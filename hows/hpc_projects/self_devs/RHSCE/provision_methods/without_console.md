## Provision Nodes Without Consoles via a Laptop

### Using Laptop as DHCP Kickstart Server

- Linux Laptop/VM with Bridged Adapter

- Wifi for packages and making new images

- **Equipment Needed**

  - small 5-port switch

  - 1G Ethernet cables


## Info on RHCE EX200-300 Exams

### Requirements

- RHCE needs RHSCA certificate to be obtained first! 

- Exam: 3.5 hours in length, and costs $400 

  - no internet access during exam 

  - access to RHEL knowledgebase 

  - [Self-Assessment Link](www.redhat.com/rhtapps/assessment)

  - satisfies the DOD 8570 mandate! 

### Bootable USB Drive 

- Download ISO Image --> Download "WIN32 Disk Imager" from SourgeForge

### Base Installs

- "Test Install Media" on Boot menu is mainly for testing the optical disk errors 

### KVM: Kernel Virtual Machines 

> hypervisors, softwares that virtualize physical computing resources i.e. CPUs, memory, device drivers, etc.---can now be embedded in the kernel and steer away from the conventional software based hypervisor solutions i.g. VirtualBox, VMware vSphere, and Xen. 

- **Overview**

  - type-2 hypervisor: runs within a host O.S. 

  - runs ONLy Linux O.S.'s (unlike the multi-platform type-2 hypervisor e.g. VirtualBox)

  - supports virtualization of nearly all Linux distros, Solaris, and Windows 

  - can install on both 32-bit & 64-bit machines that support Intel VT &/ AMD-V extensions

  - 3 major components: `kvm.ko`, QEMU, & libvirt

- kvm.ko: 

  - a kernel module that enables the Host O.S. to utilize Intel/AMD virtualization extensions
  - primarily functions to allow multiple O.S.'s to share scheduling, memory, and CPU without directly interfering with one another 

  - VMs' resource utilization managed by this component 

- QEMU:

  - in KVM, it is modified from the generic versions

  - an open-source machine emulator & virtualizer that provides virtual hardware to i.e. GPU's, Input/Peripheral Devices, NICs, RAM, and CPUs to the VM 

  - Runs in the Hosts's userspace: `/dev/kvm` with each virtualized V.M. as a linux process; when possible, works with KVM to run V.M. processes at near-native speeds; when not possible, falls back on software-only emulations

  - tasks:  

    - Setup VM's address space (a firmware image e.g. ISO image needed)  

    - facilitate I/O to & from guest V.M. 

    - map V.M.s' video display back to Host 

- libvirt: 

  - a software bundle that altogether functions as a "Hypervisor Manager," which lets users manage all VMs conveniently without needing to know phypervisor specific tools/commands 

  - components: an API library, a daemon (libvirtd), and a command line utility (virsh) 

  - supports hypervisors i.e. KVM, LXC, OpenVZ, UML, XEN, ESX, etc. 

  - controls: lifecycle ops--power, hot-plug ops, storage, and network modifications 


- **Requirements to Implement on a CentOS host** 

  - `qemu-kvm` 

  - `libvirt` (server) & `libvirt-client` (client; if remote access needed) 

  - `virt-manager` --> a GUI for interacting with libvirt server 

  - to install all at once: `yum group list hidden` & `yum group install "Virtualization Client" 

  - to set up the guest V.M. ISO images wil be required

## Overview of the Linux Boot Process

### Firmware Stage

- machine executes code in BIOS/UEFI firmware

- conducts self-tests

- finishes by starting boot loader 

- **questions** 

  - difference b/w BIOS & UEFI

\
\

### Boot Loader Stage

- "GRUB2" (Grand Unified Bootloader 2) --> boot loader for Enterprise Linux

- main role --> read kernel related configurations and executes the kernel

- configuration files

  - `/boot/grub2/grub.cfg` (BIOS based systems)

  - `/boot/efi/EFI/redhat/grub.efi` (UEFI based systems)

- **questions**

  - what are some common bootloader configurations e.g. choosing which kernel to boot from

\
\

### Kernel Stage

- kernel loads initial ramdisk into RAM

  - essentially a scheme for loading a "temporary root file system", or simply an "early user space", into memory

    - purpose: to provide user-space helpers that conduct hardware detection, kernel module (hardware drivers) loading, which are required to mount the ACTUAL root filesystem

    - the device drivers, system configurations, even provisioning scripts i.e. kickstart scripts, are pulled from ramdisk

  - before real root fs is mounted, the ramdisk is unmounted

  - not to be mistaken with ["RAM drive"](https://en.wikipedia.org/wiki/RAM_drive)

- [fakipedia's surprisingly good details on this topic](https://en.wikipedia.org/wiki/Initial_ramdisk)

- **questions** 

  - two approaches to initial ramdisk: `initrd` and `initramfs` --> differences?

\
\

### Initialization Stage

- kernel starts "grandfather process"

  - older systems (e.g. RHEL 6 & under) used "initd"

  - newer systems use "systemd"

- main responsibility: launches system services

  - e.g. starts login shells & GUI interface

- systemd can boot system into different "Targets", or simply sets of configurations

  - for "initd", this was called "Run Levels" 

  - default systemd target: "graphical.target" 

  - we can boot into targets e.g. "emergency target" to rescue system after crash 

- EXTRA: refer to "hows/hpc_projects/self_devs/RHCSE/boot_ops_1.md" for tutorial to boot into emergency target



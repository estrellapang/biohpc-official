## Local, User, & System Wide Environment Variables in Linux 

### Good Links 

[thorough guide on TecMint](https://www.tecmint.com/set-unset-environment-variables-in-linux/)

[how to add functions to $PATH](https://www.centoshowtos.org/environment-variables/) 

[profile.d & aliases](https://www.thegeekdiary.com/understanding-shell-profiles-in-centos-rhel/) 

### User & System Environment Variables 

**User** 

- uniquely defined per user, including 

- the `/etc/` directory contains configurations that are applied system-wide to all users

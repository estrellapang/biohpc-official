## Mini-Cluster for SystemAdmin/DevOps Research

### Namespace

- sudo user(s) 

  - `estrella` 

- domain name: `<host>.biohpc.new` 

### Network 

- Hostonly: 

  - subnet: `192.168.56.1` 

    - connected_NIC: `managmt1`

### Nodes 

- **Head Node** 

  - system info: 

```
[estrella@head ~]$ cat /etc/redhat-release
CentOS Linux release 7.6.1810 (Core)
[estrella@head ~]$ uname -r
3.10.0-957.el7.x86_64

```

 

### Debug: 

- deleted an old VM assigned with the same IP addr, now the host PC cannot SSH into the newVM:


```
youngsensei@x1:~$ ssh estrella@192.168.56.2
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that a host key has just been changed.
The fingerprint for the ECDSA key sent by the remote host is
SHA256:rt2dnXlzUVl2vvnH2/gKlBOViz0pFcEkqArY6n2diFw.
Please contact your system administrator.
Add correct host key in /home/youngsensei/.ssh/known_hosts to get rid of this message.
Offending ECDSA key in /home/youngsensei/.ssh/known_hosts:9
  remove with:
  ssh-keygen -f "/home/youngsensei/.ssh/known_hosts" -R "192.168.56.2"
ECDSA host key for 192.168.56.2 has changed and you have requested strict checking.
Host key verification failed.
```

- **solution**

```
# remove old ssh-key entry from `known_hosts` 

youngsensei@x1:~$ sudo ssh-keygen -f "/home/youngsensei/.ssh/known_hosts" -R "192.168.56.2"
[sudo] password for youngsensei:
# Host 192.168.56.2 found: line 9
/home/youngsensei/.ssh/known_hosts updated.
Original contents retained as /home/youngsensei/.ssh/known_hosts.old

# SSH again from WSL: kept seeing this error

youngsensei@x1:~$ ssh estrella@192.168.56.2
The authenticity of host '192.168.56.2 (192.168.56.2)' can't be established.
ECDSA key fingerprint is SHA256:rt2dnXlzUVl2vvnH2/gKlBOViz0pFcEkqArY6n2diFw.
Are you sure you want to continue connecting (yes/no)? yes
Failed to add the host to the list of known hosts (/home/youngsensei/.ssh/known_hosts).

# fixed by `sudo ssh` 

youngsensei@x1:~$ sudo ssh estrella@192.168.56.2
The authenticity of host '192.168.56.2 (192.168.56.2)' can't be established.
ECDSA key fingerprint is SHA256:rt2dnXlzUVl2vvnH2/gKlBOViz0pFcEkqArY6n2diFw.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '192.168.56.2' (ECDSA) to the list of known hosts.
```


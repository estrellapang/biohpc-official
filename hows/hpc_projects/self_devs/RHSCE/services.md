## How to Interact with Daemons/Services on Enterprise Linux 

### Resources

- [fundamental tutorial 1](https://www.cyberciti.biz/faq/check-running-services-in-rhel-redhat-fedora-centoslinux/)

### Platform Independent

- **See Services' Ports** 

  - `netstat -tulpn`

### RHEL 6

- **See all Running Services**

  -`service --status-all | grep -i running`


### RHEL 7 

- **See all Running Services**

  - `systemctl | grep -i 'running'`


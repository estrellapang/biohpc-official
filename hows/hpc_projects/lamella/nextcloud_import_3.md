## Test Nextcloud Deployment Log: Cont'd
 
> coonects from `next_cloud_import_2.md`

### 11-04-2019: No Log-in Issue

- **NOTE**:

  - deleted `httpd.conf` from `../conf.d/..`   # gone now

  - modified entries for `/etc/httpd/conf/http.conf`

```
Listen 80
ServerRoot "/etc/httpd"
DocumentRoot "/var/www/html/nextcloud_15"

```




#### Troubleshoot: Permission Change

```
chown root:apache /var/lib/php/session
chown root:apache /var/lib/php/wsdlcache

systemctl restart httpd

```

- RESULT

```
# Local users, new and with changed passwords -->  CAN LOG IN

e.g. 
------------------------
u:tester_1
p:13579ZxP

u:admin
p:~13579ZxP~ 

u:biohpcadmin
p:~13579ZxP~
-----------------------
```

#### LDAP Integration:

```
# Log in as "admin" --> "Settings" --> under "Administration" --> "LDAP/AD Integration" 


### "Server" Fields 

"+"  # add new config

"198.215.54.45"  --> "389"   # make sure to open up port 389 to VM's I.P. address 

 			     # no need to select "Detect Port" until all of the configurations have finished


"ou=users,dc=biohpc,dc=swmed,dc=edu" --> "Base DN" 

	# do not select "Detect Base DN/Test Base DN" until all of the configurations have finished

# DO NOT CHECK "Manually enter LDAP filters"

\
\
\

### "Users" Fields

# under "Edit LDAP Query" --> "(&(|(objectclass=posixAccount)))" --> click "Edit LDAP Query" link once more 

  # After this, the previously greyed out user search criteria should be made available

# "Only these object classes"  --> "posixAccount"

# DO NOT TOUCH "Only from these groups" 

\
\
\

### "Login Attributes" Fiekds

# CHECK "LDAP/AD Username" 

# "LDAP/AD Email ADdress" 

# "Other Attributes" --> "displayName" 

# the "LDAP Query" filter terms should automatially appear 

\
\
\

### "Groups" 

# select "posixGroup" --> click "Edit LDAP Query" so thast the field becomes static & not editable

\
\
\

### Verify: 

under "Server" --> "Detect Port" & "Test Base DN"  ("Detect Base DN" will return an error) 

under "User" --> "Verify settings and count users" 

under "Groups" --> "Verify settings and count the groups"

```






#### To-do: 

- check email --> httpd config --> configure VirtualHost 

- integrity:check-core

- enable Apache for SSL ---> how to check if httpd server has SSL configured???

- Warnings from admin: Overview

- database optimization

- change to different caching mechanism?



### Logs on Additional Issues

```

The "Strict-Transport-Security" HTTP header is not set to at least "15552000" seconds. For enhanced security, it is recommended to enable HSTS as described in the security tips

    Your web server is not properly set up to resolve "/.well-known/caldav". Further information can be found in the documentation.
    Your web server is not properly set up to resolve "/.well-known/carddav". Further information can be found in the documentation.
    The PHP OPcache is not properly configured. For better performance it is recommended to use the following settings in the php.ini:

    opcache.enable=1
    opcache.enable_cli=1
    opcache.interned_strings_buffer=8
    opcache.max_accelerated_files=10000
    opcache.memory_consumption=128
    opcache.save_comments=1
    opcache.revalidate_freq=1
```

- "INVALID_HASH: Error Messages from Integrity-Check

```
sudo -u apache php occ integrity:check-core
  - INVALID_HASH:
    - .htaccess:
      - expected: 03a32baf7f3a61e407b4a452422c7641594cd7701426039443c2f10f311ca1a0d5e1865bbb8e7a0fba0c2144076f0af5781f95d78bdae4672ec2a6c08c338070
      - current: db4808fd0a9522353eb69caa6b71a52325a62671e47189df275677b078b3bf1d581693d873be5b5889137a5056949d097985ae6de8c70cc5ad4f1dd173ff624a
    - core/css/guest.css:
      - expected: 55c9c7dc8e07e52fff5d047ee1bd19eec45c054fe7449ffb7da2dc35c6a67d63ddc91a2b0115893a182aa629206409e9117dc6f91e689c295b906ad73859da5d
      - current: 234b8f2398bbb1e680a28c13938a536aea6830c653e80247bf1b6f5f6da0dbf9397d046564ea5ffeeb51e7e8ec415fe471096b96b3b1cb1bafcd3c4203446fc1
  - FILE_MISSING:
    - 3rdparty/.drone.yml:
      - expected: b7cba38d64735fea33ab1fdf69fa00c7e6d93d085626bd05acd29f28942d1eb6ef204fbaec221ea113de2ed50ea5011185757b39a4670196fb43e2b670941cf8
      - current: 
    - 3rdparty/bantu/ini-get-wrapper/.scrutinizer.yml:
      - expected: 9aa05974ccb80648f933f6a79f41dfc5641cac6143e8f07df744a6d8fcd3dfee48c78f1eaffa14e356dcf877dfdf13ac94cca95935d531914ff0c1105034507b
      - current: 
    - 3rdparty/doctrine/cache/.coveralls.yml:
      - expected: 35ec67b438e82f32a1a525ea16dcfc1c5e6c8775ba78832bb326d803cb1c2bfa2b7fdfca11b91b62d3c499ddb8458aac951f3c2bbef4e8be0029f37908d63408
      - current: 
    - 3rdparty/justinrainbow/json-schema/.gitattributes:
      - expected: b6b6f2f27ef19adac85717f5c80b3719b56b0884bf246386f28786f6d9d517d1a8749b29291643a9db708efa90a8f5d034fb22ba1a4e0fdec9c2ec56272985f5
      - current: 
    - 3rdparty/justinrainbow/json-schema/.php_cs.dist:
      - expected: 2349d53ba83ff96d34235dc9ea1620a354c529fc64a633ced90149c95ed79b67b9c617b14f89469e5cea59c249a92ea467d963349ac7eac3229921a31b72ebd2
      - current: 
    - 3rdparty/php-opencloud/openstack/.php_cs.dist:
      - expected: f651349235dd54f91d0c2be3a36e70485d2eb54081880bc484ab85f6e38137978fc78cf2bec3f1c1b7e069ff9611fe78ac371c9806e19a981a035cb9ca89f5bd
      - current: 
    - 3rdparty/php-opencloud/openstack/.scrutinizer.yml:
      - expected: cc06b81b5943c36936d7981d30276fb10c7da040675dc57abe5475941aeb4ba46fcad01990bd8648b952ac63d84f7a8ef24bf7fb2815f3db7a7252a1e859f106
      - current: 
    - 3rdparty/swiftmailer/swiftmailer/.github/ISSUE_TEMPLATE.md:
      - expected: 6e29207b4bdc7fb2983a61e6efa0df825a7d8b87625690a817b9475084f686a5ca456816fe71334157c5339e1a6b0d14a10453f4da7764406577b72ba6a7653f
      - current: 
    - 3rdparty/swiftmailer/swiftmailer/.github/PULL_REQUEST_TEMPLATE.md:
      - expected: 6caa153cb8c3e20122870d31b1bd5f76fa25d649a6f0258f022782dc665ab9f3e0c7c6213321eb37017248c63b3ab15c4954aa91df4548d41b9364b37b5d9b8b
      - current: 
    - 3rdparty/swiftmailer/swiftmailer/.php_cs.dist:
      - expected: 8c45e0b01320a3c256232c44a24bddce8a7d790d3ef69e8ee0ff52a6b5bea0b42899525dad7c1a9c50afeba78d6e609003cd24889c420475818c7a56c0f9477b
      - current: 
    - core/vendor/DOMPurify/.bower.json:
      - expected: 8b72b03d635f3db2f2be8316bd0bb4a5dc839b91105996139b433dd45aa85b18faba356abb459858f0cde35cbe8f21a40edd5b3a295c7de8c77d130aae953db9
      - current: 
    - core/vendor/autosize/.bower.json:
      - expected: 5a400d2a7d29c4e8faad12b3c16b0e2d2a9bc04046d692ddd770cf4dde986f32ee7fd5313180fe863cba523df1ac486274b97d026050bebffa34774ad0ce9063
      - current: 
    - core/vendor/backbone/.bower.json:
      - expected: db697afd3f0e53fe12a3ab64fa81de97152f35d7a3970b4a11da12cebdd3c6797ea205d84d268ea8fa4ac4b3143b12e895a4a705afdaa90607f370e3065f80d0
      - current: 
    - core/vendor/base64/.bower.json:
      - expected: 11eb3f94277eb0a614f5ef0d5769d9dbb024fe1a705bf84f13a1679778a1a411425f2c82d89a543388594926ced8235dddc7833b90bc3b097ccfbeca7a8a1e37
      - current: 
    - core/vendor/blueimp-md5/.bower.json:
      - expected: e5f216445f7e4f10e0e9c5604aa6d39233dc343edfd5a09ae33e23100ff75971241c1ead6cc538e2b383d2d500aec17c3899f4eec8e4b38e45e347718964cfd4
      - current: 
    - core/vendor/bootstrap/.bower.json:
      - expected: ba32efa9aa94da111defcf107daed91f6bf48ed8dc624bdba0b0582c1de025498c3c8cdfc2ac1a2c496c41cf27d16b681fa06da703aa5b0af9543b6a57886007
      - current: 
    - core/vendor/clipboard/.bower.json:
      - expected: acec75a9dd0142c5ce0d3723a27ec3216183b774f7159668ddcd509749865fcc48098493d6e500d4e32cdcc467a46bfbdfa1dddb60db2c4758fcbaa2fad18fe9
      - current: 
    - core/vendor/davclient.js/.bower.json:
      - expected: 61859f5dbd46ac09211ed96843ccd4eef14f4df499dc8869b85dd091b85f41054e9f35b057119dfbde1fe71e844eab1a8de67ddc05095c4cdb4d5ec4af570d13
      - current: 
    - core/vendor/es6-promise/.bower.json:
      - expected: c4de3fabe331f6bc356d1ebd1e84f75fa9ca676c7cc37a80bb363b795939f4b0bc7c895372e957d4d06c118b49e60cc03260b69cd22f82d3fbb0d8065445a5da
      - current: 
    - core/vendor/es6-shim/.bower.json:
      - expected: d50a62bcaf0bddb0609c2ac5a65726bf2fec2edf114d68799d8c13cf7c86976748c9a74fd05fb96b638c6ad8f255d318bb29a5090dbd8f68d98f0260cb8654fb
      - current: 
    - core/vendor/handlebars/.bower.json:
      - expected: 6f22b57776f77fd8777c6ad8a12ebaa3a0a9f4cf2e8ef4ced5bb3b1e3fb388c3e99b2ffcfcaa1fda71faeeee778f98d77237281f51f65ec38685080c70bfb26e
      - current: 
    - core/vendor/jcrop/.bower.json:
      - expected: 33070a7645a1551d3bf75eeb75995292c7c5ce170de068daa636095c5ce8925069ea5cb35cd6c86525fac8524d27ecc992f3ed176fb64228f036522e2cc04b30
      - current: 
    - core/vendor/jquery-migrate/.bower.json:
      - expected: a1915ce5830944f2584837972224f9cca576b2491d0e090f871f8d94d7c5a198202c8ec563dd0ba9a4537426eeb213c5ecb0a421ac2bda3b663107997ce83a4b
      - current: 
    - core/vendor/jquery-ui/.bower.json:
      - expected: 073de18b210816b2146a21e0a817ba9fb17a47e9e9600dc04a361e60431fc4a89be33c9d4889658c1b7a3847f3edaa1f34497f01ea0c2a904537302138e14a22
      - current: 
    - core/vendor/jquery/.bower.json:
      - expected: f88f33ba3678ad1f8dfa0e9e39c2a84a730b853ee3df2dc50fd8e06639a5874207edde460239519f83a02c2b5d6a93943039498c5c3f6bfc035debb3b170f449
      - current: 
    - core/vendor/jsTimezoneDetect/.bower.json:
      - expected: e20b143a85d40727e2654384008ac1448d920516c6a9fa1cbaf149a0192ab8ce7873dac8e1ba7611c2f4d9e4e45d2a5c6e2eff4c43751cd874abb9f5b6a522ef
      - current: 
    - core/vendor/marked/.bower.json:
      - expected: 4328cbe1fa4e378347536d452d712666ffbe6ef3b2a8333796fbd45589293df038f776ca99ce0a4638aba269cbf7503ab9ec568294395f74d3b050d562bab92d
      - current: 
    - core/vendor/moment/.bower.json:
      - expected: b06130ae4684fe133a6a19840cd6613a692d3504cca88688583df491d7a3c0d07f59b32d91d3289c350f69d044a685f665e93a747242f0e26b51c432fafc799a
      - current: 
    - core/vendor/select2/.bower.json:
      - expected: 8bcd36df4a023740ab833ef0f2e19e3eafd97d05ea1f94b2ae7414c0886b52d88b2451dcefe68a7938dcd0f12ecb17d0c44507873cfddb57d3d0bb4e8bab224b
      - current: 
    - core/vendor/snapjs/.bower.json:
      - expected: 33e7103f10109ee0ca361b2272b0e3a3dbadfec3143d31005f2abf0180c8d3ed5fb6052b761aa256c3c79e52b3eb61188b06288ed0f986e11812659b97c5966c
      - current: 
    - core/vendor/strengthify/.bower.json:
      - expected: a8d443e4632605de0e7e88a941c40423f347d7f493ea70ccb05b43feaa84ab349e8ba58533d52daf72d33b22d9168efa963f2481c68c4905ef1491bee6c4ad36
      - current: 
    - core/vendor/underscore/.bower.json:
      - expected: cb87f2f144f1b8b5209009220c77f060a4d1d8e3fd3ea2024770f1c42e88197cc08f390876549d303719c9ad82080a994facf4958104a0dca361a663f93994c6
      - current: 
    - core/vendor/zxcvbn/.bower.json:
      - expected: f4ab73984e38266a80aae03923bf1d06ecc6b4c276e7fda906eeae4cca72014882b5d1e7524585f7c878c3703fb23bcbf46fdf411f6c5f7cfa0083f58da394a2
      - current: 
    - settings/.babelrc.js:
      - expected: b9709e9a3289d5ddbbefb86758164cc7f532c5e562015e49d721cc6969074dacb63544909ffe6385aca85a4590746ca1344ea74654196b5cdcc28ffb714e9243
      - current: 
  - EXTRA_FILE:
    - orphan_sweep.sh:
      - expected: 
      - current: 18d436e25e791404060c26282e6346193b70ca7cfa680aea329e9049ac4f0887f24db6094b6f50d88173df9ff4bea260a33da363ad71a8fa308a7996fec8ace7
    - update-log.txt:
      - expected: 
      - current: 109ad87d7a5d74f80bf357d299087ee67b62bb331df921caa59ddab4962cc32430976afa201f6628a3af3cbd5b7c8fc601a78b5537bac834f00909e47387f19d

```

### Resources:

- [grab info from MySQL database](http://g2pc1.bu.edu/~qzpeng/manual/MySQL%20Commands.htm)


## Steps towards Installing & Configuring Samba CTDB

### Overview: virtual IP being shared among the samba cluster 

> CTDB: a cluster implementation of the TDB (?) database used by Samba--runs a cluster stack in parallel to the Linux cluster tat hosts it. Essential tasks are managing node membership, recovery/failover, IP traffic load balancing, and hosting samba services. 

- [official description of CTDB's structure & functions](https://ctdb.samba.org/manpages/ctdb.7.html)

- referenced tutorials: 

  - [doc1](http://opentechrains.blogspot.com/2016/12/how-to-configure-glustersambactdb.html) 

  - [doc2](https://www.golinuxhub.com/2014/10/how-to-configure-clustered-samba-share.html) 

  - [doc3](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/cluster_administration/ch-clustered-samba-ca#s1-about-CTDB-CA) 

  - [doc4_advanced](https://www.sbarjatiya.com/notes_wiki/index.php/CentOS_7.x_ctdb_cluster_with_LDAP_integration_over_gfs2_filesystem) 

  - [doc5_legacy_ctdb.conf options](https://ctdb.samba.org/manpages/4.8/ctdbd.conf.5.html) 

  - [doc6_current_ctdb.conf options](https://ctdb.samba.org/manpages/ctdb.conf.5.html) 

### Pre-configuration Steps

```
######## Set up networking on lamella_t2 server ######### 

- public facing addr: 192.168.56.21 

- private cluster addr: 10.100.180.6

[estrella@lamella_t1 ~]$ sudo genhostid
[estrella@lamella_t1 ~]$ hostid
36107b6c

[estrella@lamella_t1 ~]$ sudo hostnamectl set-hostname lamella_t2.biohpc.new
[estrella@lamella_t1 ~]$ cat /etc/hostname
lamella_t2.biohpc.new

[estrella@lamella_t1 ~]$ lsmod | grep lnet
lnet                  595941  0 
libcfs                421295  1 lnet

[estrella@lamella_t1 ~]$ lsmod | grep zfs
zfs                  3564425  3 
zunicode              331170  1 zfs
zavl                   15236  1 zfs
icp                   270148  1 zfs
zcommon                73440  1 zfs
znvpair                89131  2 zfs,zcommon
spl                   102412  4 icp,zfs,zcommon,znvpair


# lnet not connecting?

[estrella@lamella_t1 ~]$ sudo lnetctl net show
[sudo] password for estrella: 
show:
    - net:
          errno: -100
          descr: "cannot get networks: Network is down"

  # booted up MGS, MDS, & OSS --> `sudo modprobe -v lnet` again 

[estrella@lamella_t2 ~]$ lsmod | grep lnet
lnet                  595941  6 lmv,osc,lustre,obdclass,ptlrpc,ksocklnd
libcfs                421295  11 fid,fld,lmv,mdc,lov,osc,lnet,lustre,obdclass,ptlrpc,ksocklnd

[estrella@lamella_t2 ~]$ sudo lnetctl net show
net:
    - net type: lo
      local NI(s):
        - nid: 0@lo
          status: up
    - net type: tcp
      local NI(s):
        - nid: 10.100.180.6@tcp
          status: up
          interfaces:
              0: eth1

# LNet is up! Could require MGS/MDS to be up


[estrella@lamella_t2 ~]$ sudo cat /etc/sysconfig/modules/lustre.modules 

-----------------------------------------------------------------------
#!/bin/sh

/sbin/lsmod | /bin/grep lustre 1>/dev/null 2>&1
if [ ! $? ] ; then
   /sbin/modprobe lustre >/dev/null 2>&1
fi
-----------------------------------------------------------------------

# auto load lustre module is enabled, however, on `lamella_1,` it seems that lustre module is not loaded by default either; instead, it loads as soon as the lustre fileshare is mounted 

[estrella@lamella_t2 ~]$ lsmod | grep lustre

[estrella@lamella_t2 ~]$ sudo modprobe -v lustre
[sudo] password for estrella: 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/obdclass.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/ptlrpc.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/fld.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/fid.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/lov.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/osc.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/mdc.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/lmv.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/lustre.ko 


[estrella@lamella_t1 ~]$ lsmod | grep vbox   # it's okay; not loaded on `lamella_t1` either

### check LDAP client (via authconfig-tui), & check smb.conf, & smbldap.conf

# temporarily disabled & stopped nslcd:

[estrella@lamella_t2 ~]$ sudo systemctl disable nslcd
Removed symlink /etc/systemd/system/multi-user.target.wants/nslcd.service.
[estrella@lamella_t2 ~]$ sudo systemctl stop nslcd


# `smb.conf`

-----------------------------------------------------------------------

interfaces = 192.168.56.0/24   # changed this line for `lamella_t2` 

-----------------------------------------------------------------------

# `smbldap.conf`

-----------------------------------------------------------------------

SID="S-1-5-21-439045346-4104710651-3900850888"   # um? regenerate? 

-----------------------------------------------------------------------

# `smbldap_bind.conf`

-----------------------------------------------------------------------

SID="S-1-5-21-439045346-4104710651-3900850888"  # need changing?

-----------------------------------------------------------------------

# tested connection to ldap server via 56.21 & hosted samba server --> authentication sucessful 

  # tested while NAT network was set down 

# add additional interface to LDAP server, so it can connect to the private samba cluster network: 10.100.180.0/24 

  # this is because CTDB manages the public facing network, where the clients' requests are listened for; the LDAP server was previously connected to this network 

  # correct LDAP's hostname in /etc/hosts in all servers, then test connection

# disable NAT network interface on the necessary servers (only LDAP is fine) 

# add extra interface for LDAP server, letting it serve on 10.100.180.7

  # [LDAP server] disconnected enp0s8 (192.168.56.6) & enp0s3 (NAT) & deleted their `ifcfg-` files

  # connected enp0s9 & assigned address as 10.100.180.7 

# In order for samba servers to server properly, the new LDAP server address has to be updated on both MGS & MDS!

  # or else the permissions on the lustre file share would be missing!

# both `lamella_t1` & `lamella_t2` can server as Samba servers via LDAP bind @ 10.100.180.7

# take snapshot of LDAP, Lustre Cluster, and Lamella servers 

####### end of pre-CTDB preparations ########

```



### CTDB Configuration 

> BEFORE STARTING: mount parallel FS on both Samba/Lamella servers, stop nslcd.service, smb.service, nmb.service, & ctdb.service


```

##### Installations #####

# check if the following packges have been installed 

"samba-winbind + samba samba-winbind-clients + samba-common"

yum list installed | grep <package names> 

# install on both samba servers 

sudo yum install <package names> 

['lamella_t1'] 

# if `yum install` returns errors, then consider disabling all interfaces except NAT

[estrella@lamella_t1 ~]$ sudo yum install ctdb samba-common samba-winbind-clients

Installed:
  ctdb.x86_64 0:4.8.3-4.el7                                    samba-winbind-clients.x86_64 0:4.8.3-4.el7                                   

Dependency Installed:
  samba-winbind.x86_64 0:4.8.3-4.el7          samba-winbind-modules.x86_64 0:4.8.3-4.el7          tdb-tools.x86_64 0:1.3.15-1.el7         

#  samba-common already installed 

[estrella@lamella_t1 ~]$ yum list installed | grep samba
samba.x86_64                       4.8.3-4.el7                    @base         
samba-client.x86_64                4.8.3-4.el7                    @base         
samba-client-libs.x86_64           4.8.3-4.el7                    @base         
samba-common.noarch                4.8.3-4.el7                    @base         
samba-common-libs.x86_64           4.8.3-4.el7                    @base         
samba-common-tools.x86_64          4.8.3-4.el7                    @base         
samba-libs.x86_64                  4.8.3-4.el7                    @base         
samba-winbind.x86_64               4.8.3-4.el7                    @base         
samba-winbind-clients.x86_64       4.8.3-4.el7                    @base         
samba-winbind-modules.x86_64       4.8.3-4.el7                    @base 


##### Configurations #####

 - main CTDB config file: `/etc/ctdb/ctdbd.conf` 

 - NOTE: the syntax used here belong to legacy samba versions (V4.8 and under), however, they are still valid for newer samba & ctdb releases. For new ctbd.conf syntax, check out `Doc6..` listed at top of page


```
# required fields
-----------------------------------------------------------------------------
CTDB_NODES=/silustre/ctdb/prvt_addrs

  # non-routable, private/internal cluster net addresses for the samba nodes (e.g. infiniBand network IP's), this field points to a file that should be identical on all nodes on the cluster; can be created locally (/etc/sysconfig/<>), but placing iton the parallel filesystem is a better idea. Needs to be IDENTICAL across all nodes. Separating the server and client networks is critical, as the CTDB protocol is unauthenticated & unencrypted 

  # private addresses are configured by the OS and is unique and permanent for each node

CTDB_PUBLIC_ADDRESSES=/silustre/ctdb/pub_addrs

  # client-facing net address of samba nodes, on which SMBD daemons and other services will bind to and which clients will use to connect to the cluster; also points to a file like previous entry, specify which interface will be used in file; can be created locally 

  # public addresses are NOT configured at the operating system level, and are not permanent; they are purely managed by CTDB and assigned to interfaces on physical nodes at runtime. CTDB will reassign these addresses during failover

 
CTDB_RECOVERY_LOCK=/silustre/ctdb/.lock

  # points to a file that MUST be saved on a parallel FS! This parameter specifies a lock file that's used by CTDB daemons to designate recovery nodes in the event of split-brain scenarios--all CTDB daemons running on ALL cluster nodes will need to access/lock the very same file! 

CTDB_MANAGES_SAMBA=yes (must be enabled) 

  # allow CTDB tp start and stop the Samba service for providing migration/failover

CTDB_MANAGES_WINBIND=yes (must be enabled if running on a member server)

  # allow CTDB to start/stop winbind daemon; enable this when CTDB is being used in a Windows domain; MUST disable winbind daemon via systemctl

-----------------------------------------------------------------------------



# optional fields
-----------------------------------------------------------------------------

CTDB_LOGGING=/var/log/ctdb.log 

  # CTDB by defaults uses the syslog API & saves the log file to `/usr/local/var/log/log.ctdb`; however, it is more convenient to save it in a more centralized location such as `/var/log/..` 

CTDB_MAX_OPEN_FILES=<whole number> 

  # maximum number of open files; there are no default limits, but one could alter the threshold depending on system & network capabilities 
 
CTDB_SET_<tunable parameters>=... 

  # refer to this official doc if one truly wishes to customize CTDB operations (https://ctdb.samba.org/manpages/ctdb-tunables.7.html) 	

-----------------------------------------------------------------------------


# mount the parallel file share on BOTH samba servers 

  # add /silustre (lustre file share) to `/etc/fstab` & `sudo mount -a`

  # INCLUDE: `flock` as one of the mount options!  

# make & designate a directory where CTDB daemon to grab configurations from 

  /silustre/ctdb/.lock..   # we want this file to be hidden

  # change permissions on the directory so that only root & ctdbd (does it have a user?) has read & write permissions

  # NOTE: this directory will contain many of the reference files listed in `ctdbd.conf`; it would be advantageous to create them on the parallel file system and create symlinks to each server's local directories :) 

### files inside /silustre/ctdb/ ###

- `prvt_addrs` --> corresponds to "CTDB_NODES"

-------------------------------------------
10.100.180.5
10.100.180.6
-------------------------------------------

  - create symlink to local directory 

  `sudo ln -sv /silustre/ctdb/prvt_addrs /etc/ctdb/nodes`



- `pub_addrs` --> corresponds to "CTDB_PUBLIC_ADDRESSES" 

 - **NOTICE**: public addresses are VIRTUAL IPs!!! Addresses previously assigned as STATIC IPs CANNOT OVERLAP with these! 

    - the listings in this file also need netmask & interface appended to each entry

-------------------------------------------
192.168.56.20/24 enp0s8 # WRONG: these are static IPs assigned to nodes' interfaces192.168.56.21/24 enp0s8
-------------------------------------------

----------------------------------------------------------------

192.168.56.22/24 enp0s8   # CORRECT:  both addresses are unused
192.168.56.23/24 enp0s8

---------------------------------------------------------------

 - create symlink 

  `sudo ln -sv /silustre/ctdb/pub_addrs /etc/ctdb/public_addresses`


# Modify smb.conf on both servers; REQUIRED to be IDENTICAL across all nodes!

  # back up former config file
-----------------------------------------------------------------------------

[global] 

 # REMOVE `interfaces =` option; CTDB auto-assigns ipaddresses to the interfaces listed via `/etc/ctdb/pub_addrs` 

 clustering = yes 

 private dir = /silustre/ctdb/.lock  # is this necessary?

 guest ok = yes          # is this necessary?

[silustre] 

# the following options are recommended by Samba wiki for CTDB clusters with lustre backends 

# the two options below allegedly avoids data corruption
vfs objects = fileid
fileid:algorithm = fsname

use mmap = no
nt acl support = yes  # map POSIX ACL to Windows NT's format
ea support = yes

-----------------------------------------------------------------------------
```

#### Deploying

```

# ensure following services are DISABLED and STOPPED:

 - smb, nmb, winbind

# open port 4379 for both lamella servers

sudo firewall-cmd --permanent --add-port=4379/tcp

sudo firewall-cmd --reload

sudo firewall-cmd --list-ports

# restart ctdb daemon

sudo onnode -p all systemctl restart ctdb

  # `onnode` --> a CTDB utility to run commands on specific nodes or on all nodes  

  # `-p` --> execute commands in parallel as opposed to sequential

  # [more on onnode options](https://ctdb.samba.org/manpages/onnode.1.html)


# test if CTDB is operational

sudo systemctl status ctdb

ctdb status

ctdb ping -n all

# Network check

ctdb ip   # lists all virtual IPs & the local node currently hosting them  

ctdb ip all # show all VIPs hosted by all nodes in Samba cluster

```

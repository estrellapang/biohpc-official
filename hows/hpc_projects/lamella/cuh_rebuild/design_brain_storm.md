## Brain Storming for Re-building Lamella for CUH-QTS

### Samba Fine-Tuning

- [extra 1](https://www.truenas.com/community/threads/slow-samba-performance-with-cache-none-option-on-client.77369/)

- [extra 2](https://bogner.sh/2014/10/how-to-disable-smb-client-side-caching/)

- [extra 3](https://docs.microsoft.com/en-us/windows-server/administration/performance-tuning/role/file-server/smb-file-server)

- [extra_4_BCache_FlashCache](https://askubuntu.com/questions/360257/how-to-cache-more-data-on-ssd-ram-to-avoid-spin-up)

- [comprehensive samba fine tuning parameters](https://www.arm-blog.com/samba-finetuning-for-better-transfer-speeds/)

- [samba conf parameters](https://www.samba.org/samba/docs/current/man-html/smb.conf.5.html#WRITECACHESIZE)

#### HA

#### SQL

- [Percona MySQL clustering](https://www.redhat.com/sysadmin/active-active-active-clusters)

## Additional Topics Yet to be Covered in Project 

### VM Bugs

- **networking** 

  - NAT network (enp0s3) conflicts with Server-Client host-only network (eth2)

    - lag during SSH login (unless enp0s3 is disabled)

    - yum cannot resolve proxy unless `eth2` has been disabled  

  - create separate zones:

    - one for internal 10.100.180.### network

    - one for server-client 192.168.56.### network

    - use firewall-cmd to add appropriate interfaces to their zones on each lamella server (refer to `lamella_7` setup)

### Lustre

- Set up HA 

- ZFS software RAID redundancies? 

- build from **source**---match any server whose kernel version does not match with newest lustre release

  - use alternative to `dkms` distribution (something that doesn't require re-compiling every time the kernel updates/upgrades)

- ERROR: `LustreError: 5426:0:0(obd_config.c:1477:class_process_proc_param()) silustre-MDT0000: can't parse param 'mdt.nosquash_nids' (missing '=')

### Samba-LDAP

- LDAP slave 

  - alternative: HA LDAP cluster with Pacemaker 

- **disabled TLS** during debugging--re-enable it?

### CTDB

- `onnode -p all service ctdb restart` --> this doesn't work

  - need to add node's OWN pub SSH key to `authorized_keys` (to roots home directory)

- newer version (like the ones installed here) has KeepAlive?

  - check ctdb startup events

- from `/var/log/log.ctdb` 

  - `ctdbd[4902]: Keepalive monitoring has been started`

- Benchmark your CIFS/Samba Cluster

  - `smbtorture` command (need: `sudo yum install samba-test`)

  - check link inside `ctdb_live2.md`

  - better understand relationship between smb & cifs protocol 

- Understand relationship between sockets & executables

### Nextcloud 

- standalone/HA database

  - HA MySQL database (standalone) 

    - according to Liqiang--database clusters only sync synchronously, but not asynchronusly, which increases latency in data transfer 

  - HA MySQL database for NextCloud

  - **back up** standard standalone server

    - [link](https://dev.mysql.com/doc/refman/8.0/en/backup-and-recovery.html) 

- using `redis` caching solution as opposed to `opcache` 

  - [redis tutorial 1](https://medium.com/tech-tajawal/introduction-to-caching-redis-node-js-e477eb969eab) 

- deploying `nginx` http server as opposed to `apache` 


  - [nginx 1](https://www.hostingadvice.com/how-to/nginx-vs-apache/)

  - [nginx 2](https://www.hostingadvice.com/how-to/nginx-vs-apache/)

  - [nginx 3](https://www.digitalocean.com/community/tutorials/apache-vs-nginx-practical-considerations)
    
    - mentions use of NGINX in front of Apache as a reverse proxy
 
  - [nginx 4](https://kinsta.com/blog/nginx-vs-apache/)

- **containerize** Nextcloud by installing either via Docker or via Snapcraft (SNAP packages need more investigation)

- **info on `/data` 

  - owned by `apache` & only accessible by users only when logged into the NextCloud via web 

- use firewall-cmd to restrict mysql connections on port 3306

- save user passwd to database? 

- SSL for apache?

- download tuner software

### Long-Term/Host Concerns 

 - How does VirtualBox updates affect VM files? 

   - Guestaddition belong to a specific version, how to remove it or update it to newer versions?

## NOTES from Live Configuration Implementation of CTDB 

### Installations

```

[estrella@lamella_t2 ~]$ sudo ip link set enp0s8 down   # yum has proxy issue when this network is up

[estrella@lamella_t2 ~]$ sudo yum install ctdb samba-common samba-winbind-clients

Installed:
  ctdb.x86_64 0:4.8.3-4.el7                                    samba-winbind-clients.x86_64 0:4.8.3-4.el7                                   

Dependency Installed:
  samba-winbind.x86_64 0:4.8.3-4.el7          samba-winbind-modules.x86_64 0:4.8.3-4.el7          tdb-tools.x86_64 0:1.3.15-1.el7         

Complete!



```


### Main CTDB Config File

```
sudo vim /etc/ctdb/ctdbd.conf

-------------------------------------------------------------------------------

# Options to ctdbd, read by ctdbd_wrapper(1)
#
# See ctdbd.conf(5) for more information about CTDB configuration variables.

# Shared recovery lock file to avoid split brain.  No default.
#
# Do NOT run CTDB without a recovery lock file unless you know exactly
# what you are doing.

CTDB_RECOVERY_LOCK=/silustre/ctdb/.lock

# List of nodes in the cluster.  Default is below.
# CTDB_NODES=/etc/ctdb/nodes

CTDB_NODES=/etc/ctdb/nodes

# List of public addresses for providing NAS services.  No default.
# CTDB_PUBLIC_ADDRESSES=/etc/ctdb/public_addresses

CTDB_PUBLIC_ADDRESSES=/etc/ctdb/public_addresses

# What services should CTDB manage?  Default is none.

CTDB_MANAGES_SAMBA=yes
# CTDB_MANAGES_WINBIND=yes  #previously gave errors, disabled per Doc's instructions
# CTDB_MANAGES_NFS=yes

# Raise the file descriptor limit for CTDB?
# CTDB_MAX_OPEN_FILES=10000

# Default is to use the log file below instead of syslog.
# CTDB_LOGGING=file:/var/log/log.ctdb

# Default log level is NOTICE.  Want less logging?
# CTDB_DEBUGLEVEL=ERR

# Set some CTDB tunable variables during CTDB startup?
# CTDB_SET_TDBMutexEnabled=0

CTDB_SOCKET=/var/run/ctdb/ctdbd.socket

-------------------------------------------------------------------------------

[estrella@lamella_t1 ~]$ su -

[root@lamella_t1 ~]# cd /silustre/

[root@lamella_t1 silustre]# mkdir ctdb

[root@lamella_t1 silustre]# ls -lah
total 28K
drwxr-xr-x.  5 root     oasis  11K Jul  9 10:21 .
dr-xr-xr-x. 18 root     root   240 Jun 21 13:28 ..
drwxr-xr-x.  2 root     root  1.0K Jul  9 10:21 ctdb
drwxr-xr-x.  2 lambella oasis  11K Jul  2 12:07 lambella
-rw-r--r--.  1 root     root    25 Jun 27 13:55 text1.md


[root@lamella_t1 silustre]# logout

[estrella@lamella_t1 ctdb]$ sudo vim prvt_addrs

-------------------------------------------------------------------------------

10.100.180.5
10.100.180.6

-------------------------------------------------------------------------------

[estrella@lamella_t1 ctdb]$ sudo ln -sv /silustre/ctdb/prvt_addrs /etc/ctdb/nodes
[sudo] password for estrella: 
‘/etc/ctdb/nodes’ -> ‘/silustre/ctdb/prvt_addrs’

[estrella@lamella_t1 ctdb]$ ls -lah /etc/ctdb/nodes
lrwxrwxrwx. 1 root root 25 Jul  9 10:40 /etc/ctdb/nodes -> /silustre/ctdb/prvt_addrs



[estrella@lamella_t1 ctdb]$ sudo vim pub_addrs

-------------------------------------------------------------------------------

192.168.56.22/24 eth2   # both addresses are unused
192.168.56.23/24 eth2

-------------------------------------------------------------------------------

[estrella@lamella_t1 ctdb]$ sudo ln -sv /silustre/ctdb/pub_addrs /etc/ctdb/public_addresses
[sudo] password for estrella: 
‘/etc/ctdb/public_addresses’ -> ‘/silustre/ctdb/pub_addrs’

[estrella@lamella_t1 ctdb]$ ls -lah /etc/ctdb/public_addresses
lrwxrwxrwx. 1 root root 24 Jul  9 10:50 /etc/ctdb/public_addresses -> /silustre/ctdb/pub_addrs


[estrella@lamella_t1 ctdb]$ ls -lah /etc/samba/
total 36K
drwxr-xr-x.  2 root root   81 Jul  9 11:12 .
drwxr-xr-x. 95 root root 8.0K Jul  9 09:55 ..
-rw-r--r--.  1 root root   20 Oct 30  2018 lmhosts
-rw-r--r--.  1 root root  650 Jul  5 14:06 smb.conf
-rw-r--r--.  1 root root  706 Jun 24 17:22 smb.conf.bak
-rw-r--r--.  1 root root  12K Oct 30  2018 smb.conf.example


[estrella@lamella_t1 ctdb]$ sudo cp /etc/samba/smb.conf /etc/samba/smb.conf.bak2

[estrella@lamella_t1 ctdb]$ sudo vim /etc/samba/smb.conf 

-------------------------------------------------------------------------------

[global]
        workgroup = WORKGROUP

        security = user

        netbios name = Lamella

        passdb backend = ldapsam:ldap://ldap.vms.train

        ldap suffix = dc=ldap,dc=vms,dc=train

        ldap admin dn = cn=rootadmin,dc=ldap,dc=vms,dc=train

        ldap passwd sync = Yes

        clustering = Yes

        #ldap ssl = start tls

[silustre]
        comment = file share mounted from lustre cluster

        path = /silustre

        valid users = @oasis

        guest ok = No

        browsable = Yes

        writable = Yes

-------------------------------------------------------------------------------


sudo firewall-cmd --permanent --add-port=4379/tcp

sudo firewall-cmd --reload

sudo firewall-cmd --list-ports

4379/tcp

```


### Rebooted lamella_t2 and did status check 

```
-------------------------------------------------------------------------------
[estrella@lamella_t2 ~]$ lsmod | grep zfs
zfs                  3564425  3 
zunicode              331170  1 zfs
zavl                   15236  1 zfs
icp                   270148  1 zfs
zcommon                73440  1 zfs
znvpair                89131  2 zfs,zcommon
spl                   102412  4 icp,zfs,zcommon,znvpair
[estrella@lamella_t2 ~]$ lsmod | grep lnet
lnet                  595941  7 lmv,mgc,osc,lustre,obdclass,ptlrpc,ksocklnd
libcfs                421295  12 fid,fld,lmv,mdc,lov,mgc,osc,lnet,lustre,obdclass,ptlrpc,ksocklnd
[estrella@lamella_t2 ~]$ 
[estrella@lamella_t2 ~]$ lsmod | grep lustre
lustre                758679  4 
lmv                   177987  2 lustre
mdc                   232938  2 lustre
lov                   314581  4 lustre
ptlrpc               2264705  8 fid,fld,lmv,mdc,lov,mgc,osc,lustre
obdclass             1962422  12 fid,fld,lmv,mdc,lov,mgc,osc,lustre,ptlrpc
lnet                  595941  7 lmv,mgc,osc,lustre,obdclass,ptlrpc,ksocklnd
libcfs                421295  12 fid,fld,lmv,mdc,lov,mgc,osc,lnet,lustre,obdclass,ptlrpc,ksocklnd
[estrella@lamella_t2 ~]$ 
[estrella@lamella_t2 ~]$ sudo lctl dl
[sudo] password for estrella: 
  0 UP mgc MGC10.100.180.2@tcp 6816676f-2aa4-3b77-6aaf-f43409a1bbe5 4
  1 UP lov silustre-clilov-ffffa11e16a74800 63b0af17-3410-6a59-c53b-58cbec0259f1 3
  2 UP lmv silustre-clilmv-ffffa11e16a74800 63b0af17-3410-6a59-c53b-58cbec0259f1 4
  3 UP mdc silustre-MDT0000-mdc-ffffa11e16a74800 63b0af17-3410-6a59-c53b-58cbec0259f1 4
  4 UP osc silustre-OST0001-osc-ffffa11e16a74800 63b0af17-3410-6a59-c53b-58cbec0259f1 4
-------------------------------------------------------------------------------
```

### ERRORS ###

```
[estrella@lamella_t2 ~]$ ctdb ip
connect() failed, errno=13
Failed to connect to CTDB daemon (/var/run/ctdb/ctdbd.socket)
```

#### 1st Fix Attempt

```
[estrella@lamella_t2 ~]$ sudo mkdir -p /var/lib/run/ctdb

  # also added socket entry in ctdbd.conf

`CTDB_SOCKET=/var/run/ctdb/ctdbd.socket`


# issue with smbd start-up...

-------------------------------------------------------------------------------

Jul 09 17:47:35 lamella_t2.biohpc.new systemd[1]: Starting Samba SMB Daemon...
-- Subject: Unit smb.service has begun start-up
-- Defined-By: systemd
-- Support: http://lists.freedesktop.org/mailman/listinfo/systemd-devel
-- 
-- Unit smb.service has begun starting up.
Jul 09 17:47:35 lamella_t2.biohpc.new smbd[12623]: [2019/07/09 17:47:35.591110,  0] ../source3/passdb/secrets.c:362(fetch_ldap_pw)
Jul 09 17:47:35 lamella_t2.biohpc.new smbd[12623]:   fetch_ldap_pw: neither ldap secret retrieved!
Jul 09 17:47:35 lamella_t2.biohpc.new smbd[12623]: [2019/07/09 17:47:35.591190,  0] ../source3/passdb/pdb_ldap.c:6542(pdb_init_ldapsam_commo
Jul 09 17:47:35 lamella_t2.biohpc.new smbd[12623]:   pdb_init_ldapsam_common: Failed to retrieve LDAP password from secrets.tdb
Jul 09 17:47:35 lamella_t2.biohpc.new smbd[12623]: [2019/07/09 17:47:35.591217,  0] ../source3/passdb/pdb_interface.c:180(make_pdb_method_na
Jul 09 17:47:35 lamella_t2.biohpc.new smbd[12623]:   pdb backend ldapsam:ldap://ldap.vms.train did not correctly init (error was NT_STATUS_N
Jul 09 17:47:35 lamella_t2.biohpc.new systemd[1]: smb.service: main process exited, code=exited, status=1/FAILURE
Jul 09 17:47:35 lamella_t2.biohpc.new systemd[1]: Failed to start Samba SMB Daemon.
-- Subject: Unit smb.service has failed
-- Defined-By: systemd
-- Support: http://lists.freedesktop.org/mailman/listinfo/systemd-devel
-- 
-- Unit smb.service has failed.
-- 
-------------------------------------------------------------------------------

```


### Debugging 

- create ssh keys for eachother on both servers

  - `ssh-keygen` 

--> then save pub key to `.ssh/authorized_keys`

  - also add local pub key NOTE: NOT DONE!

  - this is so that `sudo onnode -p all service ctdb start` can work 


- inside `smb.conf`, added the following lines


`ctdbd socket = /var/run/ctdb/ctdbd.socket`


- samba server fails to start:

 - CTDB logs: 

```
-------------------------------------------------------------------------------

2019/07/10 18:23:24.957086 ctdbd[10755]: CTDB starting on node
2019/07/10 18:23:24.962253 ctdbd[10756]: Starting CTDBD (Version 4.8.3) as PID: 10756
2019/07/10 18:23:24.962514 ctdbd[10756]: Created PID file /run/ctdb/ctdbd.pid
2019/07/10 18:23:24.962610 ctdbd[10756]: Removed stale socket /var/run/ctdb/ctdbd.socket
2019/07/10 18:23:24.962653 ctdbd[10756]: Listening to ctdb socket /var/run/ctdb/ctdbd.socket
2019/07/10 18:23:24.962672 ctdbd[10756]: Set real-time scheduler priority
2019/07/10 18:23:24.962915 ctdbd[10756]: Starting event daemon /usr/libexec/ctdb/ctdb_eventd -e /etc/ctdb/events.d -s /var/run/ctdb/eventd.sock -P 10756 -l file:/var/log/log.ctdb -d NOTICE
2019/07/10 18:23:24.963188 ctdbd[10756]: connect() failed, errno=2
2019/07/10 18:23:24.966293 ctdb-eventd[10757]: daemon started, pid=10757
2019/07/10 18:23:24.966399 ctdb-eventd[10757]: listening on /var/run/ctdb/eventd.sock
2019/07/10 18:23:25.963446 ctdbd[10756]: Set runstate to INIT (1)
2019/07/10 18:23:26.103801 ctdbd[10756]: PNN is 0
2019/07/10 18:23:26.117624 ctdbd[10756]: Vacuuming is disabled for non-volatile database secrets.tdb
2019/07/10 18:23:26.117653 ctdbd[10756]: Attached to database '/var/lib/ctdb/persistent/secrets.tdb.0' with flags 0x400
2019/07/10 18:23:26.121944 ctdbd[10756]: Vacuuming is disabled for non-volatile database ctdb.tdb
2019/07/10 18:23:26.121962 ctdbd[10756]: Attached to database '/var/lib/ctdb/persistent/ctdb.tdb.0' with flags 0x400
2019/07/10 18:23:26.121981 ctdbd[10756]: Freeze db: ctdb.tdb
2019/07/10 18:23:26.122005 ctdbd[10756]: Set lock helper to "/usr/libexec/ctdb/ctdb_lock_helper"
2019/07/10 18:23:26.125006 ctdbd[10756]: Freeze db: secrets.tdb
2019/07/10 18:23:26.127494 ctdbd[10756]: Set runstate to SETUP (2)
2019/07/10 18:23:26.220926 ctdbd[10756]: Keepalive monitoring has been started
2019/07/10 18:23:26.220985 ctdbd[10756]: Set runstate to FIRST_RECOVERY (3)
2019/07/10 18:23:26.221198 ctdb-recoverd[10845]: monitor_cluster starting
2019/07/10 18:23:26.222278 ctdb-recoverd[10845]: Initial recovery master set - forcing election
2019/07/10 18:23:26.222427 ctdbd[10756]: This node (0) is now the recovery master
2019/07/10 18:23:26.723892 ctdbd[10756]: Remote node (1) is now the recovery master
2019/07/10 18:23:27.222032 ctdbd[10756]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/10 18:23:28.222634 ctdbd[10756]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/10 18:23:29.222985 ctdbd[10756]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/10 18:23:29.679627 ctdbd[10756]: Attached to database '/var/lib/ctdb/g_lock.tdb.0' with flags 0x1c41
2019/07/10 18:23:29.687378 ctdbd[10756]: Recovery has started
2019/07/10 18:23:29.728419 ctdb-recoverd[10845]: Election period ended
2019/07/10 18:23:29.728627 ctdb-recoverd[10845]:  Current recmaster node 1 does not have CAP_RECMASTER, but we (node 0) have - force an election
2019/07/10 18:23:29.728697 ctdbd[10756]: This node (0) is now the recovery master
2019/07/10 18:23:29.784530 ctdbd[10756]: Freeze db: g_lock.tdb
2019/07/10 18:23:29.784742 ctdbd[10756]: Freeze db: secrets.tdb frozen
2019/07/10 18:23:29.787359 ctdbd[10756]: Freeze db: ctdb.tdb frozen
2019/07/10 18:23:29.880766 ctdbd[10756]: Thaw db: secrets.tdb generation 1315133050
2019/07/10 18:23:29.880795 ctdbd[10756]: Release freeze handle for db secrets.tdb
2019/07/10 18:23:29.881027 ctdbd[10756]: Thaw db: ctdb.tdb generation 1315133050
2019/07/10 18:23:29.881041 ctdbd[10756]: Release freeze handle for db ctdb.tdb
2019/07/10 18:23:29.884628 ctdbd[10756]: Thaw db: g_lock.tdb generation 1315133050
2019/07/10 18:23:29.884654 ctdbd[10756]: Release freeze handle for db g_lock.tdb
2019/07/10 18:23:29.885417 ctdbd[10756]: Recovery mode set to NORMAL
2019/07/10 18:23:29.885453 ctdbd[10756]: Set cluster mutex helper to "/usr/libexec/ctdb/ctdb_mutex_fcntl_helper"
2019/07/10 18:23:29.890274 ctdbd[10756]: Recovery has finished
2019/07/10 18:23:29.981026 ctdbd[10756]: Set runstate to STARTUP (4)
2019/07/10 18:23:29.982668 ctdb-recoverd[10845]: Disabling takeover runs for 60 seconds
2019/07/10 18:23:30.076571 ctdb-recoverd[10845]: Reenabling takeover runs
2019/07/10 18:23:30.223760 ctdbd[10756]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/10 18:23:30.223800 ctdbd[10756]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/10 18:23:30.230532 ctdbd[10756]: Remote node (1) is now the recovery master
2019/07/10 18:23:31.221884 ctdbd[10756]: 10.100.180.5:4379: connected to 10.100.180.6:4379 - 1 connected
2019/07/10 18:23:31.224015 ctdbd[10756]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/10 18:23:31.224037 ctdbd[10756]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/10 18:23:32.224578 ctdbd[10756]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/10 18:23:32.224659 ctdbd[10756]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/10 18:23:33.224885 ctdbd[10756]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/10 18:23:33.224953 ctdbd[10756]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/10 18:23:33.233661 ctdb-recoverd[10845]: Election period ended
2019/07/10 18:23:33.234831 ctdb-recoverd[10845]: Initial interface fetched
2019/07/10 18:23:33.235082 ctdb-recoverd[10845]: Trigger takeoverrun
2019/07/10 18:23:33.681687 ctdb-recoverd[10845]: Disabling takeover runs for 60 seconds
2019/07/10 18:23:33.783259 ctdb-recoverd[10845]: Reenabling takeover runs
2019/07/10 18:23:34.225434 ctdbd[10756]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/10 18:23:34.225518 ctdbd[10756]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/10 18:23:35.226242 ctdbd[10756]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/10 18:23:35.226303 ctdbd[10756]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/10 18:23:36.226685 ctdbd[10756]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/10 18:23:36.226772 ctdbd[10756]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/10 18:23:37.227480 ctdbd[10756]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/10 18:23:37.227559 ctdbd[10756]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/10 18:23:38.228071 ctdbd[10756]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/10 18:23:38.228126 ctdbd[10756]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/10 18:23:39.228593 ctdbd[10756]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/10 18:23:39.228693 ctdbd[10756]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/10 18:23:40.228882 ctdbd[10756]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/10 18:23:40.228954 ctdbd[10756]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/10 18:23:41.229497 ctdbd[10756]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/10 18:23:41.229582 ctdbd[10756]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/10 18:23:42.229835 ctdbd[10756]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/10 18:23:42.229915 ctdbd[10756]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/10 18:23:43.230438 ctdbd[10756]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/10 18:23:43.230523 ctdbd[10756]: ctdb_recheck_persistent_health: OK[2] FAIL[0]
2019/07/10 18:23:43.230538 ctdbd[10756]: Running the "startup" event.
2019/07/10 18:23:43.433622 ctdb-eventd[10757]: 50.samba: Redirecting to /bin/systemctl start smb.service
2019/07/10 18:23:43.433730 ctdb-eventd[10757]: 50.samba: Job for smb.service failed because the control process exited with error code. See "systemctl status smb.service" and "journalctl -xe" for details.
2019/07/10 18:23:43.433739 ctdb-eventd[10757]: 50.samba: Failed to start samba
2019/07/10 18:23:43.433747 ctdb-eventd[10757]: startup event failed
2019/07/10 18:23:43.433805 ctdbd[10756]: startup event failed
-------------------------------------------------------------------------------
```

- journalctl -xe logs

```
-- Unit smb.service has begun starting up.
Jul 10 18:36:24 lamella_t1.biohpc.new smbd[20675]: [2019/07/10 18:36:24.197802,  0] ../source3/passdb/secrets.c:362(fetch_ldap_pw)
Jul 10 18:36:24 lamella_t1.biohpc.new smbd[20675]:   fetch_ldap_pw: neither ldap secret retrieved!
Jul 10 18:36:24 lamella_t1.biohpc.new smbd[20675]: [2019/07/10 18:36:24.197880,  0] ../source3/passdb/pdb_ldap.c:6542(pdb_init_ldapsam_common)
Jul 10 18:36:24 lamella_t1.biohpc.new smbd[20675]:   pdb_init_ldapsam_common: Failed to retrieve LDAP password from secrets.tdb
Jul 10 18:36:24 lamella_t1.biohpc.new smbd[20675]: [2019/07/10 18:36:24.197912,  0] ../source3/passdb/pdb_interface.c:180(make_pdb_method_name)
Jul 10 18:36:24 lamella_t1.biohpc.new smbd[20675]:   pdb backend ldapsam:ldap://10.100.180.7 did not correctly init (error was NT_STATUS_NO_MEMORY)
Jul 10 18:36:24 lamella_t1.biohpc.new systemd[1]: smb.service: main process exited, code=exited, status=1/FAILURE
Jul 10 18:36:24 lamella_t1.biohpc.new systemd[1]: Failed to start Samba SMB Daemon.
-- Subject: Unit smb.service has failed
-- Defined-By: systemd
-- Support: http://lists.freedesktop.org/mailman/listinfo/systemd-devel
-- 
-- Unit smb.service has failed.
-- 
-- The result is failed.
Jul 10 18:36:24 lamella_t1.biohpc.new systemd[1]: Unit smb.service entered failed state.
Jul 10 18:36:24 lamella_t1.biohpc.new systemd[1]: smb.service failed.
------------------------------------------------------------------------------

```

- NOTES: something's wrong with smb conf???! clustering? CTDB_Socket?


### July 11, 2019 ###

- CONFIG: when SSH-ing into Lamella servers, disable NAT interfaces (tend to cause LAG when combined with host-only network 2) 

- Samba fix

  - in `smb.conf` --> remove `clustering = yes` entry, or else smb.service will fail with the following errors 

```
-- Unit smb.service has begun starting up.
Jul 11 13:52:56 lamella_t1.biohpc.new systemd[1]: smb.service: main process exited, code=exited, status=1/FAILURE
Jul 11 13:52:56 lamella_t1.biohpc.new systemd[1]: Failed to start Samba SMB Daemon.
-- Subject: Unit smb.service has failed
-- Defined-By: systemd
-- Support: http://lists.freedesktop.org/mailman/listinfo/systemd-devel
-- 
-- Unit smb.service has failed.
-- 
-- The result is failed.
Jul 11 13:52:56 lamella_t1.biohpc.new systemd[1]: Unit smb.service entered failed state.
Jul 11 13:52:56 lamella_t1.biohpc.new systemd[1]: smb.service failed.
Jul 11 13:52:56 lamella_t1.biohpc.new polkitd[3130]: Unregistered Authentication Agent for unix-process:4920:59006 (system bus name :1.52, o
Jul 11 13:52:56 lamella_t1.biohpc.new sudo[4918]: pam_unix(sudo:session): session closed for user root
Jul 11 13:53:09 lamella_t1.biohpc.new sudo[4929]: estrella : TTY=pts/0 ; PWD=/home/estrella ; USER=root ; COMMAND=/bin/systemctl start smb
Jul 11 13:53:09 lamella_t1.biohpc.new sudo[4929]: pam_unix(sudo:session): session opened for user root by estrella(uid=0)
Jul 11 13:53:09 lamella_t1.biohpc.new polkitd[3130]: Registered Authentication Agent for unix-process:4931:60331 (system bus name :1.54 [/us
Jul 11 13:53:09 lamella_t1.biohpc.new systemd[1]: Starting Samba SMB Daemon...
-- Subject: Unit smb.service has begun start-up
-- Defined-By: systemd
-- Support: http://lists.freedesktop.org/mailman/listinfo/systemd-devel
-- 
-- Unit smb.service has begun starting up.
Jul 11 13:53:09 lamella_t1.biohpc.new systemd[1]: smb.service: main process exited, code=exited, status=1/FAILURE
Jul 11 13:53:09 lamella_t1.biohpc.new systemd[1]: Failed to start Samba SMB Daemon.
-- Subject: Unit smb.service has failed
-- Defined-By: systemd
-- Support: http://lists.freedesktop.org/mailman/listinfo/systemd-devel
-- 
-- Unit smb.service has failed.
-- 
-- The result is failed.
Jul 11 13:53:09 lamella_t1.biohpc.new systemd[1]: Unit smb.service entered failed state.
Jul 11 13:53:09 lamella_t1.biohpc.new systemd[1]: smb.service failed.
Jul 11 13:53:09 lamella_t1.biohpc.new sudo[4929]: pam_unix(sudo:session): session closed for user root
Jul 11 13:53:09 lamella_t1.biohpc.new polkitd[3130]: Unregistered Authentication Agent for unix-process:4931:60331 (system bus name :1.54, o
Jul 11 13:58:18 lamella_t1.biohpc.new systemd[1]: Starting Cleanup of Temporary Directories...
-- Subject: Unit systemd-tmpfiles-clean.service has begun start-up
-- Defined-By: systemd

```

- **info on nmb** is not needed 

  - nmbd is only needed for WindowsNT to automatically map samba drives 

  - w/o it, all one has to do is to manually map the network drives 

- `sudo systemctl start ctdb` 

- logs from `lamella_t1` 

```
2019/07/11 14:14:48.185010 ctdbd[5033]: CTDB starting on node
2019/07/11 14:14:48.340453 ctdbd[5034]: Starting CTDBD (Version 4.8.3) as PID: 5034
2019/07/11 14:14:48.340736 ctdbd[5034]: Created PID file /run/ctdb/ctdbd.pid
2019/07/11 14:14:48.340866 ctdbd[5034]: Listening to ctdb socket /var/run/ctdb/ctdbd.socket
2019/07/11 14:14:48.340886 ctdbd[5034]: Set real-time scheduler priority
2019/07/11 14:14:48.341129 ctdbd[5034]: Starting event daemon /usr/libexec/ctdb/ctdb_eventd -e /etc/ctdb/events.d -s /var/run/ctdb/eventd.sock -P 5034 -l file:/var/log/log.ctdb -d NOTICE
2019/07/11 14:14:48.341341 ctdbd[5034]: connect() failed, errno=2
2019/07/11 14:14:48.352657 ctdb-eventd[5036]: daemon started, pid=5036
2019/07/11 14:14:48.352776 ctdb-eventd[5036]: listening on /var/run/ctdb/eventd.sock
2019/07/11 14:14:49.341592 ctdbd[5034]: Set runstate to INIT (1)
2019/07/11 14:14:49.803143 ctdbd[5034]: PNN is 0
2019/07/11 14:14:49.818371 ctdbd[5034]: Vacuuming is disabled for non-volatile database secrets.tdb
2019/07/11 14:14:49.818401 ctdbd[5034]: Attached to database '/var/lib/ctdb/persistent/secrets.tdb.0' with flags 0x400
2019/07/11 14:14:49.822712 ctdbd[5034]: Vacuuming is disabled for non-volatile database ctdb.tdb
2019/07/11 14:14:49.822724 ctdbd[5034]: Attached to database '/var/lib/ctdb/persistent/ctdb.tdb.0' with flags 0x400
2019/07/11 14:14:49.822743 ctdbd[5034]: Freeze db: ctdb.tdb
2019/07/11 14:14:49.822793 ctdbd[5034]: Set lock helper to "/usr/libexec/ctdb/ctdb_lock_helper"
2019/07/11 14:14:49.833715 ctdbd[5034]: Freeze db: secrets.tdb
2019/07/11 14:14:49.836208 ctdbd[5034]: Set runstate to SETUP (2)
2019/07/11 14:14:49.988992 ctdbd[5034]: Keepalive monitoring has been started
2019/07/11 14:14:49.989037 ctdbd[5034]: Set runstate to FIRST_RECOVERY (3)
2019/07/11 14:14:49.989250 ctdb-recoverd[5123]: monitor_cluster starting
2019/07/11 14:14:49.990372 ctdb-recoverd[5123]: Initial recovery master set - forcing election
2019/07/11 14:14:49.990535 ctdbd[5034]: This node (0) is now the recovery master
2019/07/11 14:14:50.989763 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:14:51.990791 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:14:52.991554 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:14:52.991636 ctdb-recoverd[5123]: Election period ended
2019/07/11 14:14:52.991907 ctdb-recoverd[5123]: Node:0 was in recovery mode. Start recovery process
2019/07/11 14:14:52.991925 ctdb-recoverd[5123]: ../ctdb/server/ctdb_recoverd.c:1277 Starting do_recovery
2019/07/11 14:14:52.991933 ctdb-recoverd[5123]: Attempting to take recovery lock (/silustre/ctdb/.lock)
2019/07/11 14:14:52.992060 ctdb-recoverd[5123]: Set cluster mutex helper to "/usr/libexec/ctdb/ctdb_mutex_fcntl_helper"
2019/07/11 14:14:53.046086 ctdb-recoverd[5123]: Recovery lock taken successfully by recovery daemon
2019/07/11 14:14:53.046129 ctdb-recoverd[5123]: ../ctdb/server/ctdb_recoverd.c:1336 Recovery initiated due to problem with node 0
2019/07/11 14:14:53.046175 ctdb-recoverd[5123]: ../ctdb/server/ctdb_recoverd.c:1361 Recovery - created remote databases
2019/07/11 14:14:53.046279 ctdb-recoverd[5123]: ../ctdb/server/ctdb_recoverd.c:1390 Recovery - updated flags
2019/07/11 14:14:53.046353 ctdb-recoverd[5123]: Set recovery_helper to "/usr/libexec/ctdb/ctdb_recovery_helper"
2019/07/11 14:14:53.077933 ctdb-recovery[5126]: Set recovery mode to ACTIVE
2019/07/11 14:14:53.078044 ctdbd[5034]: Recovery has started
2019/07/11 14:14:53.175510 ctdb-recovery[5126]: start_recovery event finished
2019/07/11 14:14:53.175587 ctdb-recovery[5126]: updated VNNMAP
2019/07/11 14:14:53.175601 ctdb-recovery[5126]: recover database 0x6645c6c4
2019/07/11 14:14:53.175615 ctdb-recovery[5126]: recover database 0x7132c184
2019/07/11 14:14:53.175787 ctdbd[5034]: Freeze db: ctdb.tdb frozen
2019/07/11 14:14:53.175879 ctdbd[5034]: Freeze db: secrets.tdb frozen
2019/07/11 14:14:53.207436 ctdbd[5034]: Thaw db: ctdb.tdb generation 1203919775
2019/07/11 14:14:53.207463 ctdbd[5034]: Release freeze handle for db ctdb.tdb
2019/07/11 14:14:53.207572 ctdbd[5034]: Thaw db: secrets.tdb generation 1203919775
2019/07/11 14:14:53.207587 ctdbd[5034]: Release freeze handle for db secrets.tdb
2019/07/11 14:14:53.207702 ctdb-recovery[5126]: 2 of 2 databases recovered
2019/07/11 14:14:53.207739 ctdbd[5034]: Recovery mode set to NORMAL
2019/07/11 14:14:53.207766 ctdbd[5034]: Set cluster mutex helper to "/usr/libexec/ctdb/ctdb_mutex_fcntl_helper"
2019/07/11 14:14:53.212167 ctdb-recovery[5126]: Set recovery mode to NORMAL
2019/07/11 14:14:53.212211 ctdbd[5034]: Recovery has finished
2019/07/11 14:14:53.302326 ctdbd[5034]: Set runstate to STARTUP (4)
2019/07/11 14:14:53.302380 ctdb-recovery[5126]: recovered event finished
2019/07/11 14:14:53.302786 ctdb-recoverd[5123]: Takeover run starting
2019/07/11 14:14:53.302892 ctdb-recoverd[5123]: Set takeover_helper to "/usr/libexec/ctdb/ctdb_takeover_helper"
2019/07/11 14:14:53.307778 ctdb-takeover[5182]: No nodes available to host public IPs yet
2019/07/11 14:14:53.397796 ctdb-recoverd[5123]: Takeover run completed successfully
2019/07/11 14:14:53.397843 ctdb-recoverd[5123]: ../ctdb/server/ctdb_recoverd.c:1408 Recovery complete
2019/07/11 14:14:53.397853 ctdb-recoverd[5123]: Resetting ban count to 0 for all nodes
2019/07/11 14:14:53.397861 ctdb-recoverd[5123]: Just finished a recovery. New recoveries will now be suppressed for the rerecovery timeout (10 seconds)
2019/07/11 14:14:53.397867 ctdb-recoverd[5123]: Disabling recoveries for 10 seconds
2019/07/11 14:14:53.992369 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:14:53.992411 ctdbd[5034]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:14:53.992638 ctdb-recoverd[5123]: Initial interface fetched
2019/07/11 14:14:53.992742 ctdb-recoverd[5123]: Trigger takeoverrun
2019/07/11 14:14:53.992842 ctdb-recoverd[5123]: Takeover run starting
2019/07/11 14:14:53.996225 ctdb-takeover[5209]: No nodes available to host public IPs yet
2019/07/11 14:14:54.087894 ctdb-recoverd[5123]: Takeover run completed successfully
2019/07/11 14:14:54.992750 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:14:54.992828 ctdbd[5034]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:14:55.993759 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:14:55.993808 ctdbd[5034]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:14:56.994269 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:14:56.994317 ctdbd[5034]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:14:57.994648 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:14:57.994722 ctdbd[5034]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:14:58.995446 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:14:58.995509 ctdbd[5034]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:14:59.990574 ctdbd[5034]: 10.100.180.5:4379: connected to 10.100.180.6:4379 - 1 connected
2019/07/11 14:14:59.995795 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:14:59.995847 ctdbd[5034]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:00.592206 ctdb-recoverd[5123]: Election period ended
2019/07/11 14:15:00.996797 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:00.996885 ctdbd[5034]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:01.004371 ctdb-recoverd[5123]: Node:1 was in recovery mode. Start recovery process
2019/07/11 14:15:01.004404 ctdb-recoverd[5123]: ../ctdb/server/ctdb_recoverd.c:1277 Starting do_recovery
2019/07/11 14:15:01.004418 ctdb-recoverd[5123]: Unable to begin - recoveries are disabled
2019/07/11 14:15:01.997233 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:01.997319 ctdbd[5034]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:02.998066 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:02.998160 ctdbd[5034]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:03.398588 ctdb-recoverd[5123]: Reenabling recoveries after timeout
2019/07/11 14:15:03.999206 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:03.999275 ctdbd[5034]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:04.096522 ctdb-recoverd[5123]: Election period ended
2019/07/11 14:15:04.999963 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:05.000021 ctdbd[5034]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:05.009102 ctdb-recoverd[5123]: ../ctdb/server/ctdb_recoverd.c:1277 Starting do_recovery
2019/07/11 14:15:05.009130 ctdb-recoverd[5123]: Already holding recovery lock
2019/07/11 14:15:05.009140 ctdb-recoverd[5123]: ../ctdb/server/ctdb_recoverd.c:1336 Recovery initiated due to problem with node 0
2019/07/11 14:15:05.009624 ctdb-recoverd[5123]: ../ctdb/server/ctdb_recoverd.c:1361 Recovery - created remote databases
2019/07/11 14:15:05.010590 ctdb-recoverd[5123]: ../ctdb/server/ctdb_recoverd.c:1390 Recovery - updated flags
2019/07/11 14:15:05.015117 ctdbd[5034]: Recovery mode set to ACTIVE
2019/07/11 14:15:05.015458 ctdb-recovery[5236]: Set recovery mode to ACTIVE
2019/07/11 14:15:05.015581 ctdbd[5034]: Recovery has started
2019/07/11 14:15:05.112380 ctdb-recovery[5236]: start_recovery event finished
2019/07/11 14:15:05.112726 ctdb-recovery[5236]: updated VNNMAP
2019/07/11 14:15:05.112749 ctdb-recovery[5236]: recover database 0x6645c6c4
2019/07/11 14:15:05.112760 ctdb-recovery[5236]: recover database 0x7132c184
2019/07/11 14:15:05.112913 ctdbd[5034]: Freeze db: ctdb.tdb
2019/07/11 14:15:05.113156 ctdbd[5034]: Freeze db: secrets.tdb
2019/07/11 14:15:05.154861 ctdbd[5034]: Thaw db: ctdb.tdb generation 1103481939
2019/07/11 14:15:05.154892 ctdbd[5034]: Release freeze handle for db ctdb.tdb
2019/07/11 14:15:05.155787 ctdbd[5034]: Thaw db: secrets.tdb generation 1103481939
2019/07/11 14:15:05.155806 ctdbd[5034]: Release freeze handle for db secrets.tdb
2019/07/11 14:15:05.156501 ctdb-recovery[5236]: 2 of 2 databases recovered
2019/07/11 14:15:05.156599 ctdbd[5034]: Recovery mode set to NORMAL
2019/07/11 14:15:05.173780 ctdb-recovery[5236]: Set recovery mode to NORMAL
2019/07/11 14:15:05.173883 ctdbd[5034]: Recovery has finished
2019/07/11 14:15:05.264326 ctdb-recovery[5236]: recovered event finished
2019/07/11 14:15:05.264403 ctdb-recoverd[5123]: Takeover run starting
2019/07/11 14:15:05.268033 ctdb-takeover[5292]: No nodes available to host public IPs yet
2019/07/11 14:15:05.357745 ctdb-recoverd[5123]: Takeover run completed successfully
2019/07/11 14:15:05.357821 ctdb-recoverd[5123]: ../ctdb/server/ctdb_recoverd.c:1408 Recovery complete
2019/07/11 14:15:05.357842 ctdb-recoverd[5123]: Resetting ban count to 0 for all nodes
2019/07/11 14:15:05.357874 ctdb-recoverd[5123]: Just finished a recovery. New recoveries will now be suppressed for the rerecovery timeout (10 seconds)
2019/07/11 14:15:05.357881 ctdb-recoverd[5123]: Disabling recoveries for 10 seconds
2019/07/11 14:15:06.002850 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:06.002925 ctdbd[5034]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:06.010370 ctdb-recoverd[5123]: Takeover run starting
2019/07/11 14:15:06.014377 ctdb-takeover[5319]: No nodes available to host public IPs yet
2019/07/11 14:15:06.107099 ctdb-recoverd[5123]: Takeover run completed successfully
2019/07/11 14:15:07.003026 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:07.003077 ctdbd[5034]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:08.004333 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:08.004406 ctdbd[5034]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:09.005832 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:09.005897 ctdbd[5034]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:10.006817 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:10.006902 ctdbd[5034]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:11.007780 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:11.007840 ctdbd[5034]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:12.008092 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:12.008183 ctdbd[5034]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:13.008472 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:13.008564 ctdbd[5034]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:14.009045 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:14.009135 ctdbd[5034]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:15.009571 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:15.009681 ctdbd[5034]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:15.358694 ctdb-recoverd[5123]: Reenabling recoveries after timeout
2019/07/11 14:15:16.010454 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:16.010516 ctdbd[5034]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:17.011274 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:17.011337 ctdbd[5034]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:18.012413 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:18.012479 ctdbd[5034]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:19.013042 ctdbd[5034]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:19.013162 ctdbd[5034]: ctdb_recheck_persistent_health: OK[2] FAIL[0]
2019/07/11 14:15:19.013191 ctdbd[5034]: Running the "startup" event.
2019/07/11 14:15:20.357932 ctdb-eventd[5036]: 50.samba: Redirecting to /bin/systemctl start smb.service
2019/07/11 14:15:20.379874 ctdbd[5034]: startup event OK - enabling monitoring
2019/07/11 14:15:20.379901 ctdbd[5034]: Set runstate to RUNNING (5)
2019/07/11 14:15:21.884616 ctdb-recoverd[5123]: Node 1 has changed flags - now 0x0  was 0x2
2019/07/11 14:15:22.035029 ctdb-recoverd[5123]: Takeover run starting
2019/07/11 14:15:22.324849 ctdb-recoverd[5123]: Takeover run completed successfully
2019/07/11 14:15:22.584191 ctdbd[5034]: monitor event OK - node re-enabled
2019/07/11 14:15:22.584223 ctdbd[5034]: Node became HEALTHY. Ask recovery master to reallocate IPs
2019/07/11 14:15:22.584587 ctdb-recoverd[5123]: Node 0 has changed flags - now 0x0  was 0x2
2019/07/11 14:15:23.036459 ctdb-recoverd[5123]: Takeover run starting
2019/07/11 14:15:23.158822 ctdbd[5034]: Takeover of IP 192.168.56.23/24 on interface eth2
2019/07/11 14:15:23.355896 ctdb-recoverd[5123]: Takeover run completed successfully

```

- logs from `lamella_t2` 

```
2019/07/11 14:14:55.806614 ctdbd[4893]: CTDB starting on node
2019/07/11 14:14:55.854299 ctdbd[4894]: Starting CTDBD (Version 4.8.3) as PID: 4894
2019/07/11 14:14:55.854524 ctdbd[4894]: Created PID file /run/ctdb/ctdbd.pid
2019/07/11 14:14:55.854642 ctdbd[4894]: Listening to ctdb socket /var/run/ctdb/ctdbd.socket
2019/07/11 14:14:55.854677 ctdbd[4894]: Set real-time scheduler priority
2019/07/11 14:14:55.854909 ctdbd[4894]: Starting event daemon /usr/libexec/ctdb/ctdb_eventd -e /etc/ctdb/events.d -s /var/run/ctdb/eventd.sock -P 4894 -l file:/var/log/log.ctdb -d NOTICE
2019/07/11 14:14:55.855148 ctdbd[4894]: connect() failed, errno=2
2019/07/11 14:14:55.873138 ctdb-eventd[4895]: daemon started, pid=4895
2019/07/11 14:14:55.873248 ctdb-eventd[4895]: listening on /var/run/ctdb/eventd.sock
2019/07/11 14:14:56.855560 ctdbd[4894]: Set runstate to INIT (1)
2019/07/11 14:14:57.242197 ctdbd[4894]: PNN is 1
2019/07/11 14:14:57.259547 ctdbd[4894]: Vacuuming is disabled for non-volatile database ctdb.tdb
2019/07/11 14:14:57.259575 ctdbd[4894]: Attached to database '/var/lib/ctdb/persistent/ctdb.tdb.1' with flags 0x400
2019/07/11 14:14:57.264289 ctdbd[4894]: Vacuuming is disabled for non-volatile database secrets.tdb
2019/07/11 14:14:57.264306 ctdbd[4894]: Attached to database '/var/lib/ctdb/persistent/secrets.tdb.1' with flags 0x400
2019/07/11 14:14:57.264324 ctdbd[4894]: Freeze db: secrets.tdb
2019/07/11 14:14:57.264370 ctdbd[4894]: Set lock helper to "/usr/libexec/ctdb/ctdb_lock_helper"
2019/07/11 14:14:57.286490 ctdbd[4894]: Freeze db: ctdb.tdb
2019/07/11 14:14:57.288914 ctdbd[4894]: Set runstate to SETUP (2)
2019/07/11 14:14:57.407315 ctdbd[4894]: Keepalive monitoring has been started
2019/07/11 14:14:57.407363 ctdbd[4894]: Set runstate to FIRST_RECOVERY (3)
2019/07/11 14:14:57.407564 ctdb-recoverd[4983]: monitor_cluster starting
2019/07/11 14:14:57.408727 ctdb-recoverd[4983]: Initial recovery master set - forcing election
2019/07/11 14:14:57.408912 ctdbd[4894]: This node (1) is now the recovery master
2019/07/11 14:14:57.910702 ctdbd[4894]: Remote node (0) is now the recovery master
2019/07/11 14:14:58.408475 ctdbd[4894]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:14:59.408802 ctdbd[4894]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:00.408983 ctdbd[4894]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:00.913874 ctdb-recoverd[4983]: Election period ended
2019/07/11 14:15:00.914111 ctdb-recoverd[4983]:  Current recmaster node 0 does not have CAP_RECMASTER, but we (node 1) have - force an election
2019/07/11 14:15:00.914181 ctdbd[4894]: This node (1) is now the recovery master
2019/07/11 14:15:01.409384 ctdbd[4894]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:01.415552 ctdbd[4894]: Remote node (0) is now the recovery master
2019/07/11 14:15:02.407918 ctdbd[4894]: 10.100.180.6:4379: connected to 10.100.180.5:4379 - 1 connected
2019/07/11 14:15:02.410077 ctdbd[4894]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:03.410349 ctdbd[4894]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:04.410762 ctdbd[4894]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:04.418694 ctdb-recoverd[4983]: Election period ended
2019/07/11 14:15:04.834067 ctdbd[4894]: Recovery has started
2019/07/11 14:15:04.931426 ctdbd[4894]: Freeze db: ctdb.tdb frozen
2019/07/11 14:15:04.931509 ctdbd[4894]: Freeze db: secrets.tdb frozen
2019/07/11 14:15:04.973862 ctdbd[4894]: Thaw db: ctdb.tdb generation 1103481939
2019/07/11 14:15:04.973937 ctdbd[4894]: Release freeze handle for db ctdb.tdb
2019/07/11 14:15:04.974067 ctdbd[4894]: Thaw db: secrets.tdb generation 1103481939
2019/07/11 14:15:04.974080 ctdbd[4894]: Release freeze handle for db secrets.tdb
2019/07/11 14:15:04.974896 ctdbd[4894]: Recovery mode set to NORMAL
2019/07/11 14:15:04.974992 ctdbd[4894]: Set cluster mutex helper to "/usr/libexec/ctdb/ctdb_mutex_fcntl_helper"
2019/07/11 14:15:04.992169 ctdbd[4894]: Recovery has finished
2019/07/11 14:15:05.082142 ctdbd[4894]: Set runstate to STARTUP (4)
2019/07/11 14:15:05.082873 ctdb-recoverd[4983]: Disabling takeover runs for 60 seconds
2019/07/11 14:15:05.176185 ctdb-recoverd[4983]: Reenabling takeover runs
2019/07/11 14:15:05.411216 ctdbd[4894]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:05.411252 ctdbd[4894]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:05.420251 ctdb-recoverd[4983]: Initial interface fetched
2019/07/11 14:15:05.420368 ctdb-recoverd[4983]: Trigger takeoverrun
2019/07/11 14:15:05.828730 ctdb-recoverd[4983]: Disabling takeover runs for 60 seconds
2019/07/11 14:15:05.925496 ctdb-recoverd[4983]: Reenabling takeover runs
2019/07/11 14:15:06.411648 ctdbd[4894]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:06.411697 ctdbd[4894]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:07.412262 ctdbd[4894]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:07.412311 ctdbd[4894]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:08.412859 ctdbd[4894]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:08.412922 ctdbd[4894]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:09.413984 ctdbd[4894]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:09.414068 ctdbd[4894]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:10.414102 ctdbd[4894]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:10.414164 ctdbd[4894]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:11.414214 ctdbd[4894]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:11.414276 ctdbd[4894]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:12.414519 ctdbd[4894]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:12.414577 ctdbd[4894]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:13.415621 ctdbd[4894]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:13.415681 ctdbd[4894]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:14.416612 ctdbd[4894]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:14.416706 ctdbd[4894]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:15.417468 ctdbd[4894]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:15.417525 ctdbd[4894]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:16.417745 ctdbd[4894]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:16.417868 ctdbd[4894]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:17.418750 ctdbd[4894]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:17.418831 ctdbd[4894]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/11 14:15:18.419741 ctdbd[4894]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/11 14:15:18.419867 ctdbd[4894]: ctdb_recheck_persistent_health: OK[2] FAIL[0]
2019/07/11 14:15:18.419882 ctdbd[4894]: Running the "startup" event.
2019/07/11 14:15:19.413727 ctdb-eventd[4895]: 50.samba: Redirecting to /bin/systemctl start smb.service
2019/07/11 14:15:19.436827 ctdbd[4894]: startup event OK - enabling monitoring
2019/07/11 14:15:19.436851 ctdbd[4894]: Set runstate to RUNNING (5)
2019/07/11 14:15:21.702140 ctdbd[4894]: monitor event OK - node re-enabled
2019/07/11 14:15:21.702172 ctdbd[4894]: Node became HEALTHY. Ask recovery master to reallocate IPs
2019/07/11 14:15:21.702877 ctdb-recoverd[4983]: Node 1 has changed flags - now 0x0  was 0x2
2019/07/11 14:15:21.853432 ctdb-recoverd[4983]: Disabling takeover runs for 60 seconds
2019/07/11 14:15:21.857565 ctdbd[4894]: Takeover of IP 192.168.56.23/24 on interface eth2
2019/07/11 14:15:21.857971 ctdbd[4894]: Takeover of IP 192.168.56.22/24 on interface eth2
2019/07/11 14:15:22.143200 ctdb-recoverd[4983]: Reenabling takeover runs
2019/07/11 14:15:22.402963 ctdb-recoverd[4983]: Node 0 has changed flags - now 0x0  was 0x2
2019/07/11 14:15:22.854848 ctdb-recoverd[4983]: Disabling takeover runs for 60 seconds
2019/07/11 14:15:22.858875 ctdbd[4894]: Release of IP 192.168.56.23/24 on interface eth2  node:0
2019/07/11 14:15:23.174253 ctdb-recoverd[4983]: Reenabling takeover runs

```

- **NOTE**: from log, CTDB seems to be working 

- Verify: Samba share seems to be available across 4 address (2 static, 2 virtual---not normal) 

```
[estrella@lamella_t2 ~]$ smbclient -L 192.168.56.22 -U lambella
Enter WORKGROUP\lambella's password: 

	Sharename       Type      Comment
	---------       ----      -------
	silustre        Disk      file share mounted from lustre cluster
	IPC$            IPC       IPC Service (Samba 4.8.3)
Reconnecting with SMB1 for workgroup listing.

	Server               Comment
	---------            -------

	Workgroup            Master
	---------            -------
	WORKGROUP            LAMELLA
[estrella@lamella_t2 ~]$ 
[estrella@lamella_t2 ~]$ 
[estrella@lamella_t2 ~]$ smbclient -L 192.168.56.23 -U lambella
Enter WORKGROUP\lambella's password: 

	Sharename       Type      Comment
	---------       ----      -------
	silustre        Disk      file share mounted from lustre cluster
	IPC$            IPC       IPC Service (Samba 4.8.3)
Reconnecting with SMB1 for workgroup listing.

	Server               Comment
	---------            -------

	Workgroup            Master
	---------            -------
	WORKGROUP            LAMELLA
[estrella@lamella_t2 ~]$ 
[estrella@lamella_t2 ~]$ smbclient -L 192.168.56.20 -U lambella
Enter WORKGROUP\lambella's password: 

	Sharename       Type      Comment
	---------       ----      -------
	silustre        Disk      file share mounted from lustre cluster
	IPC$            IPC       IPC Service (Samba 4.8.3)
Reconnecting with SMB1 for workgroup listing.

	Server               Comment
	---------            -------

	Workgroup            Master
	---------            -------
	WORKGROUP            LAMELLA

trella@lamella_t2 ~]$ smbclient -L 192.168.56.21 -U lambella
Enter WORKGROUP\lambella's password: 

	Sharename       Type      Comment
	---------       ----      -------
	silustre        Disk      file share mounted from lustre cluster
	IPC$            IPC       IPC Service (Samba 4.8.3)
Reconnecting with SMB1 for workgroup listing.

	Server               Comment
	---------            -------

	Workgroup            Master
	---------            -------
	WORKGROUP            LAMELLA

```

- on Windows: 

  - "network" --> "map network drive" --> "192.168.56.2[0-4]/silustre" 

  - all 4 samba drives can be mapped and accessed with user "lambella"

- new perspective on the old error:

```
connect() failed, errno=13
Failed to connect to CTDB daemon (/var/run/ctdb/ctdbd.socket)

``` 

  - perhaps the `ctdb` command cannot talk to CTDB daemon? 



- **for learning** 

  - stopped `ctdb` on `lamella_t1` ----> `lamella_t2` enters recovery process & takes over the lost public IP, see logs below 


```
2019/07/11 17:49:42.270350 ctdbd[4894]: 10.100.180.6:4379: node 10.100.180.5:4379 is dead: 0 connected
2019/07/11 17:49:42.270399 ctdbd[4894]: Tearing down connection to dead node :0
2019/07/11 17:49:43.093772 ctdb-recoverd[4983]:  Current recmaster node 0 does not have CAP_RECMASTER, but we (node 1) have - force an election
2019/07/11 17:49:43.093826 ctdbd[4894]: Recovery mode set to ACTIVE
2019/07/11 17:49:43.093881 ctdbd[4894]: This node (1) is now the recovery master
2019/07/11 17:49:46.097035 ctdb-recoverd[4983]: Election period ended
2019/07/11 17:49:46.097388 ctdb-recoverd[4983]: Node:1 was in recovery mode. Start recovery process
2019/07/11 17:49:46.097408 ctdb-recoverd[4983]: ../ctdb/server/ctdb_recoverd.c:1277 Starting do_recovery
2019/07/11 17:49:46.097416 ctdb-recoverd[4983]: Attempting to take recovery lock (/silustre/ctdb/.lock)
2019/07/11 17:49:46.097489 ctdb-recoverd[4983]: Set cluster mutex helper to "/usr/libexec/ctdb/ctdb_mutex_fcntl_helper"
2019/07/11 17:49:46.102466 ctdb-recoverd[4983]: Recovery lock taken successfully by recovery daemon
2019/07/11 17:49:46.102496 ctdb-recoverd[4983]: ../ctdb/server/ctdb_recoverd.c:1336 Recovery initiated due to problem with node 0
2019/07/11 17:49:46.102579 ctdb-recoverd[4983]: ../ctdb/server/ctdb_recoverd.c:1361 Recovery - created remote databases
2019/07/11 17:49:46.102694 ctdb-recoverd[4983]: ../ctdb/server/ctdb_recoverd.c:1390 Recovery - updated flags
2019/07/11 17:49:46.102799 ctdb-recoverd[4983]: Set recovery_helper to "/usr/libexec/ctdb/ctdb_recovery_helper"
2019/07/11 17:49:46.174925 ctdb-recovery[17361]: Set recovery mode to ACTIVE
2019/07/11 17:49:46.175062 ctdbd[4894]: Recovery has started
2019/07/11 17:49:46.271959 ctdb-recovery[17361]: start_recovery event finished
2019/07/11 17:49:46.272034 ctdb-recovery[17361]: updated VNNMAP
2019/07/11 17:49:46.272050 ctdb-recovery[17361]: recover database 0x7132c184
2019/07/11 17:49:46.272075 ctdb-recovery[17361]: recover database 0x6645c6c4
2019/07/11 17:49:46.272480 ctdbd[4894]: Freeze db: secrets.tdb
2019/07/11 17:49:46.272684 ctdbd[4894]: Freeze db: ctdb.tdb
2019/07/11 17:49:46.323628 ctdbd[4894]: Thaw db: secrets.tdb generation 1862276808
2019/07/11 17:49:46.323684 ctdbd[4894]: Release freeze handle for db secrets.tdb
2019/07/11 17:49:46.323792 ctdbd[4894]: Thaw db: ctdb.tdb generation 1862276808
2019/07/11 17:49:46.323801 ctdbd[4894]: Release freeze handle for db ctdb.tdb
2019/07/11 17:49:46.324295 ctdb-recovery[17361]: 2 of 2 databases recovered
2019/07/11 17:49:46.324376 ctdbd[4894]: Recovery mode set to NORMAL
2019/07/11 17:49:46.328415 ctdb-recovery[17361]: Set recovery mode to NORMAL
2019/07/11 17:49:46.328473 ctdbd[4894]: Recovery has finished
2019/07/11 17:49:46.418497 ctdb-recovery[17361]: recovered event finished
2019/07/11 17:49:46.418567 ctdb-recoverd[4983]: Takeover run starting
2019/07/11 17:49:46.418650 ctdb-recoverd[4983]: Set takeover_helper to "/usr/libexec/ctdb/ctdb_takeover_helper"
2019/07/11 17:49:46.496414 ctdbd[4894]: Takeover of IP 192.168.56.23/24 on interface eth2
2019/07/11 17:49:46.688193 ctdb-recoverd[4983]: Takeover run completed successfully
2019/07/11 17:49:46.688472 ctdb-recoverd[4983]: ../ctdb/server/ctdb_recoverd.c:1408 Recovery complete
2019/07/11 17:49:46.688484 ctdb-recoverd[4983]: Resetting ban count to 0 for all nodes
2019/07/11 17:49:46.688493 ctdb-recoverd[4983]: Just finished a recovery. New recoveries will now be suppressed for the rerecovery timeout (10 seconds)
2019/07/11 17:49:46.688500 ctdb-recoverd[4983]: Disabling recoveries for 10 seconds
2019/07/11 17:49:56.689246 ctdb-recoverd[4983]: Reenabling recoveries after timeout

```

- Verify if `lamella_t2` is still hosting ctdb-smb services 

  - appears that it has taken over BOTH public IP addresses

```
[estrella@lamella_t2 ~]$ smbclient -L 192.168.56.22 -U lambella
Enter WORKGROUP\lambella's password: 

	Sharename       Type      Comment
	---------       ----      -------
	silustre        Disk      file share mounted from lustre cluster
	IPC$            IPC       IPC Service (Samba 4.8.3)
Reconnecting with SMB1 for workgroup listing.

	Server               Comment
	---------            -------

	Workgroup            Master
	---------            -------
	WORKGROUP            LAMELLA
[estrella@lamella_t2 ~]$ smbclient -L 192.168.56.23 -U lambella
Enter WORKGROUP\lambella's password: 

	Sharename       Type      Comment
	---------       ----      -------
	silustre        Disk      file share mounted from lustre cluster
	IPC$            IPC       IPC Service (Samba 4.8.3)
Reconnecting with SMB1 for workgroup listing.

	Server               Comment
	---------            -------

	Workgroup            Master
	---------            -------
	WORKGROUP            LAMELLA

# not shown here: but it appears that the samba share tied to static IP of ##.##.56.20 has now closed its connections--which makes sense

``` 


### 2nd Debug Attempt

- creating file `/etc/default/ctdb`

-----------------------------------------------------------------------
#CTDB_SOCKET=/var/run/ctdb/ctdbd.socket
export CTDB_SOCKET
-----------------------------------------------------------------------


### July 12,19: 3rd Debug Attempt 

- Observations: 

 - the socket issue seems to be permission issue (diagnosed by `ps aux | grep -i "ctdb*"`) 

 - however, with `sudo ctdb status`, we get the following results: 

```
[estrella@lamella_t1 ~]$ sudo ctdb status
Number of nodes:2
pnn:0 10.100.180.5     UNHEALTHY (THIS NODE)
pnn:1 10.100.180.6     UNHEALTHY
Generation:945282163
Size:2
hash:0 lmaster:0
hash:1 lmaster:1
Recovery mode:NORMAL (0)
Recovery master:0

```

  - which also causes ctdbd to fail to stop, a sample of `systemd` messages below: 

```
           ├─12361 sed -e s/^[^\t ]*[\t ]*// -e s/,/ /g -e s/[\t ]*$// /etc/ctdb/public_addresses
           ├─12407 sed -e s/^[^\t ]*[\t ]*// -e s/,/ /g -e s/[\t ]*$// /etc/ctdb/public_addresses
           ├─12453 sed -e s/^[^\t ]*[\t ]*// -e s/,/ /g -e s/[\t ]*$// /etc/ctdb/public_addresses
           ├─12509 sed -e s/^[^\t ]*[\t ]*// -e s/,/ /g -e s/[\t ]*$// /etc/ctdb/public_addresses
           ├─12555 sed -e s/^[^\t ]*[\t ]*// -e s/,/ /g -e s/[\t ]*$// /etc/ctdb/public_addresses
           ├─12601 sed -e s/^[^\t ]*[\t ]*// -e s/,/ /g -e s/[\t ]*$// /etc/ctdb/public_addresses
           ├─12648 sed -e s/^[^\t ]*[\t ]*// -e s/,/ /g -e s/[\t ]*$// /etc/ctdb/public_addresses
           ├─12694 sed -e s/^[^\t ]*[\t ]*// -e s/,/ /g -e s/[\t ]*$// /etc/ctdb/public_addresses
           ├─12740 sed -e s/^[^\t ]*[\t ]*// -e s/,/ /g -e s/[\t ]*$// /etc/ctdb/public_addresses
           ├─12786 sed -e s/^[^\t ]*[\t ]*// -e s/,/ /g -e s/[\t ]*$// /etc/ctdb/public_addresses
           └─control
             └─12798 /bin/sh /usr/sbin/ctdbd_wrapper /run/ctdb/ctdbd.pid stop

Jul 12 15:35:13 lamella_t1.biohpc.new systemd[1]: Stopping CTDB...
Jul 12 15:35:44 lamella_t1.biohpc.new ctdbd_wrapper[12798]: Timed out waiting for CTDB to shutdown.  Killing CTDB processes.
Jul 12 15:36:44 lamella_t1.biohpc.new systemd[1]: ctdb.service stopping timed out. Terminating.
Jul 12 15:38:14 lamella_t1.biohpc.new systemd[1]: ctdb.service stop-sigterm timed out. Killing.
Jul 12 15:39:44 lamella_t1.biohpc.new systemd[1]: ctdb.service still around after SIGKILL. Ignoring.
Jul 12 15:41:14 lamella_t1.biohpc.new systemd[1]: ctdb.service stop-final-sigterm timed out. Killing.

# the issue caused lustre client errors

```

- Estimated cause:

  - `smb.conf` needs the following entries: 

----------------------------------------------------------------------------------------------------

clustering = yes
idmap config * : backend = autorid
idmap config * : range = 1000000-1999999

----------------------------------------------------------------------------------------------------

  - `ctdb event script enable legacy 50.samba` 


### Possible Troubleshooting Links:


https://bugs.launchpad.net/ubuntu/+source/samba/+bug/1335540

http://lists-archives.com/samba/81001-ctdb-failed-to-connect-client-socket-to-daemon.html

https://samba.samba.narkive.com/AvEgF5Jp/ctdb-failed-to-connect-client-socket-to-daemon#post1 

https://lists.samba.org/archive/samba-technical/2014-August/101748.html





### July 15th, 2019 

- kernel log messages for lustre errors (on `lamella_t1`)

```
Jul 12 14:32:25 lamella_t1 kernel: Lustre: 8134:0:(client.c:2132:ptlrpc_expire_one_request()) @@@ Request sent has timed out for slow reply: [sent 1562959343/real 1562959343]  req@ffff9b23cd803300 x1638879740302128/t0(0) o101->silustre-MDT0000-mdc-ffff9b23d483f000@10.100.180.3@tcp:12/10 lens 640/33488 e 24 to 1 dl 1562959945 ref 2 fl Rpc:IXP/0/ffffffff rc 0/-1
Jul 12 14:32:25 lamella_t1 kernel: Lustre: silustre-MDT0000-mdc-ffff9b23d483f000: Connection to silustre-MDT0000 (at 10.100.180.3@tcp) was lost; in progress operations using this service will wait for recovery to complete
Jul 12 14:32:25 lamella_t1 kernel: Lustre: silustre-MDT0000-mdc-ffff9b23d483f000: Connection restored to 10.100.180.3@tcp (at 10.100.180.3@tcp)
Jul 12 14:38:22 lamella_t1 kernel: Lustre: 8412:0:(client.c:2132:ptlrpc_expire_one_request()) @@@ Request sent has timed out for slow reply: [sent 1562959945/real 1562959945]  req@ffff9b23c10a0c00 x1638879740302672/t0(0) o101->silustre-MDT0000-mdc-ffff9b23d483f000@10.100.180.3@tcp:12/10 lens 640/33488 e 0 to 1 dl 1562960302 ref 2 fl Rpc:IXP/2/ffffffff rc -11/-1
Jul 12 14:38:22 lamella_t1 kernel: Lustre: silustre-MDT0000-mdc-ffff9b23d483f000: Connection to silustre-MDT0000 (at 10.100.180.3@tcp) was lost; in progress operations using this service will wait for recovery to complete
Jul 12 14:38:22 lamella_t1 kernel: Lustre: silustre-MDT0000-mdc-ffff9b23d483f000: Connection restored to 10.100.180.3@tcp (at 10.100.180.3@tcp)
Jul 12 14:40:01 lamella_t1 systemd: Created slice User Slice of root.
Jul 12 14:40:01 lamella_t1 systemd: Started Session 8 of user root.
Jul 12 14:40:01 lamella_t1 systemd: Removed slice User Slice of root.
Jul 12 14:45:21 lamella_t1 kernel: Lustre: 8458:0:(client.c:2132:ptlrpc_expire_one_request()) @@@ Request sent has timed out for slow reply: [sent 1562960302/real 1562960302]  req@ffff9b23c10a0f00 x1638879740302768/t0(0) o101->silustre-MDT0000-mdc-ffff9b23d483f000@10.100.180.3@tcp:12/10 lens 640/33488 e 0 to 1 dl 1562960721 ref 2 fl Rpc:IXP/2/ffffffff rc -11/-1
Jul 12 14:45:21 lamella_t1 kernel: Lustre: silustre-MDT0000-mdc-ffff9b23d483f000: Connection to silustre-MDT0000 (at 10.100.180.3@tcp) was lost; in progress operations using this service will wait for recovery to complete
Jul 12 14:45:21 lamella_t1 kernel: Lustre: silustre-MDT0000-mdc-ffff9b23d483f000: Connection restored to 10.100.180.3@tcp (at 10.100.180.3@tcp)
Jul 12 14:50:01 lamella_t1 systemd: Created slice User Slice of root.
Jul 12 14:50:01 lamella_t1 systemd: Started Session 9 of user root.
Jul 12 14:50:01 lamella_t1 systemd: Removed slice User Slice of root.
Jul 12 14:52:33 lamella_t1 kernel: Lustre: 8319:0:(client.c:2132:ptlrpc_expire_one_request()) @@@ Request sent has timed out for slow reply: [sent 1562960721/real 1562960721]  req@ffff9b23c10a0000 x1638879740302480/t0(0) o101->silustre-MDT0000-mdc-ffff9b23d483f000@10.100.180.3@tcp:12/10 lens 640/33488 e 1 to 1 dl 1562961153 ref 2 fl Rpc:IXP/2/ffffffff rc -11/-1
Jul 12 14:52:33 lamella_t1 kernel: Lustre: silustre-MDT0000-mdc-ffff9b23d483f000: Connection to silustre-MDT0000 (at 10.100.180.3@tcp) was lost; in progress operations using this service will wait for recovery to complete
Jul 12 14:52:33 lamella_t1 kernel: Lustre: silustre-MDT0000-mdc-ffff9b23d483f000: Connection restored to 10.100.180.3@tcp (at 10.100.180.3@tcp)
Jul 12 15:00:01 lamella_t1 systemd: Created slice User Slice of root.
Jul 12 15:00:01 lamella_t1 systemd: Started Session 10 of user root.
Jul 12 15:00:01 lamella_t1 systemd: Removed slice User Slice of root.
Jul 12 15:00:35 lamella_t1 kernel: Lustre: 8505:0:(client.c:2132:ptlrpc_expire_one_request()) @@@ Request sent has timed out for slow reply: [sent 1562961153/real 1562961153]  req@ffff9b23c10a0600 x1638879740302880/t0(0) o101->silustre-MDT0000-mdc-ffff9b23d483f000@10.100.180.3@tcp:12/10 lens 640/33488 e 0 to 1 dl 1562961635 ref 2 fl Rpc:IXP/2/ffffffff rc -11/-1
Jul 12 15:00:35 lamella_t1 kernel: Lustre: silustre-MDT0000-mdc-ffff9b23d483f000: Connection to silustre-MDT0000 (at 10.100.180.3@tcp) was lost; in progress operations using this service will wait for recovery to complete
Jul 12 15:00:35 lamella_t1 kernel: Lustre: silustre-MDT0000-mdc-ffff9b23d483f000: Connection restored to 10.100.180.3@tcp (at 10.100.180.3@tcp)


# need to test whether this is caused by NAT network or a result of auto-repair

# comment: recovered; no similar messages on `lamella_t2`

``` 


- error while enabling script:

```
[estrella@lamella_t1 ~]$ ctdb event script enable legacy 50.samba
socket connect failed - /var/run/ctdb/eventd.sock
ctdb_event_init() failed, ret=5

```

- error while starting ctdb: 

```
2019/07/15 17:34:28.335609 ctdb-recoverd[5247]: ERROR: when taking recovery lock
2019/07/15 17:34:28.335634 ctdb-recoverd[5247]: Unable to get recovery lock - retrying recovery
2019/07/15 17:34:29.333200 ctdb-recoverd[5247]: ../ctdb/server/ctdb_recoverd.c:1277 Starting do_recovery
2019/07/15 17:34:29.333238 ctdb-recoverd[5247]: Attempting to take recovery lock (/silustre/ctdb/.lock)
2019/07/15 17:34:29.337698 ctdbd[5158]: /usr/libexec/ctdb/ctdb_mutex_fcntl_helper: Failed to get lock on '/silustre/ctdb/.lock' - (Function not implemented)

```

- retry: starting lamella_t2 first 

```
# lamella_t1 still has .lock file errors

2019/07/15 17:50:13.133920 ctdbd[5844]: Recovery has started
2019/07/15 17:50:13.230420 ctdbd[5844]: Freeze db: g_lock.tdb
2019/07/15 17:50:13.230634 ctdbd[5844]: Freeze db: secrets.tdb
2019/07/15 17:50:13.232456 ctdbd[5844]: Freeze db: ctdb.tdb
2019/07/15 17:50:13.463852 ctdbd[5844]: Thaw db: secrets.tdb generation 360789201
2019/07/15 17:50:13.463883 ctdbd[5844]: Release freeze handle for db secrets.tdb
2019/07/15 17:50:13.482124 ctdbd[5844]: Thaw db: ctdb.tdb generation 360789201
2019/07/15 17:50:13.482155 ctdbd[5844]: Release freeze handle for db ctdb.tdb
2019/07/15 17:50:13.484638 ctdbd[5844]: Thaw db: g_lock.tdb generation 360789201
2019/07/15 17:50:13.484656 ctdbd[5844]: Release freeze handle for db g_lock.tdb
2019/07/15 17:50:13.484946 ctdbd[5844]: Recovery mode set to NORMAL
2019/07/15 17:50:13.488973 ctdbd[5844]: /usr/libexec/ctdb/ctdb_mutex_fcntl_helper: Failed to get lock on '/silustre/ctdb/.lock' - (Function not implemented)

```

### July 17th, 2019 


```
## good sign: the previous recovery issues have been resolved

  # check `/var/log/log.ctdb` to see recovery details

## Error: samba still cannot start after `clustering = yes` option has been re-entered back into `smb.conf` 


## Fix Attempt: 

from the following thread, got advice to change permissions on `/var/lib/samba` 

https://www.linuxquestions.org/questions/linux-server-73/cannot-start-samba-4175625224/ 

--------------------------------------------------

cd /var/lib/samba 

drwx------.  3 root root         41 Jun 24 17:35 private

sudo chmod 755 private  

sudo chmod 755 private/msg.sock/

# check lock/msg.lock    # not msg.sock!!! 

drwxr-xr-x.  3 root root       4.0K Jun 28 15:20 lock   # good

### DO NOT DO this! Samba will not EVEN start!
--------------------------------------------------


### Progress Summary:

  # reverted permissions in /var/lib/samba back to default 

  # removed `clustering = yes` option in `smb.conf` 

  # ctdb starts normally again, with ctdb node status as "healthy" 

# Sample of `smb.conf` entries 


-------------------------------------------------------------------------------

[global]
        workgroup = WORKGROUP

        security = user

        netbios name = Lamella

        passdb backend = ldapsam:ldap://10.100.180.7

        ldap suffix = dc=ldap,dc=vms,dc=train

        ldap admin dn = cn=rootadmin,dc=ldap,dc=vms,dc=train

        ldap passwd sync = Yes

        ctdbd socket = /var/run/ctdb/ctdbd.socket

        #ldap ssl = start tls

[silustre]
        comment = file share mounted from lustre cluster

        path = /silustre

        valid users = @oasis

        guest ok = No

        browsable = Yes

        writable = Yes

-------------------------------------------------------------------------------



# Sample of `/etc/ctdb/ctdbd.conf` 

-------------------------------------------------------------------------------

CTDB_RECOVERY_LOCK=/silustre/ctdb/.lock
CTDB_NODES=/etc/ctdb/nodes
CTDB_PUBLIC_ADDRESSES=/etc/ctdb/public_addresses
CTDB_MANAGES_SAMBA=yes
CTDB_SOCKET=/var/run/ctdb/ctdbd.socket

-------------------------------------------------------------------------------



# `log.ctdb` from `lamella_t1` 

-------------------------------------------------------------------------------

2019/07/17 16:20:21.633056 ctdbd[3528]: CTDB starting on node
2019/07/17 16:20:21.644027 ctdbd[3529]: Starting CTDBD (Version 4.8.3) as PID: 3529
2019/07/17 16:20:21.644258 ctdbd[3529]: Created PID file /run/ctdb/ctdbd.pid
2019/07/17 16:20:21.644355 ctdbd[3529]: Removed stale socket /var/run/ctdb/ctdbd.socket
2019/07/17 16:20:21.644537 ctdbd[3529]: Listening to ctdb socket /var/run/ctdb/ctdbd.socket
2019/07/17 16:20:21.644559 ctdbd[3529]: Set real-time scheduler priority
2019/07/17 16:20:21.644791 ctdbd[3529]: Starting event daemon /usr/libexec/ctdb/ctdb_eventd -e /etc/ctdb/events.d -s /var/run/ctdb/eventd.sock -P 3529 -l file:/var/log/log.ctdb -d NOTICE
2019/07/17 16:20:21.645020 ctdbd[3529]: connect() failed, errno=2
2019/07/17 16:20:21.650622 ctdb-eventd[3531]: daemon started, pid=3531
2019/07/17 16:20:21.650723 ctdb-eventd[3531]: listening on /var/run/ctdb/eventd.sock
2019/07/17 16:20:22.645446 ctdbd[3529]: Set runstate to INIT (1)
2019/07/17 16:20:22.780260 ctdbd[3529]: PNN is 0
2019/07/17 16:20:22.797642 ctdbd[3529]: Vacuuming is disabled for non-volatile database secrets.tdb
2019/07/17 16:20:22.797674 ctdbd[3529]: Attached to database '/var/lib/ctdb/persistent/secrets.tdb.0' with flags 0x400
2019/07/17 16:20:22.802023 ctdbd[3529]: Vacuuming is disabled for non-volatile database ctdb.tdb
2019/07/17 16:20:22.802039 ctdbd[3529]: Attached to database '/var/lib/ctdb/persistent/ctdb.tdb.0' with flags 0x400
2019/07/17 16:20:22.802058 ctdbd[3529]: Freeze db: ctdb.tdb
2019/07/17 16:20:22.802082 ctdbd[3529]: Set lock helper to "/usr/libexec/ctdb/ctdb_lock_helper"
2019/07/17 16:20:22.804942 ctdbd[3529]: Freeze db: secrets.tdb
2019/07/17 16:20:22.807404 ctdbd[3529]: Set runstate to SETUP (2)
2019/07/17 16:20:22.901992 ctdbd[3529]: Keepalive monitoring has been started
2019/07/17 16:20:22.902037 ctdbd[3529]: Set runstate to FIRST_RECOVERY (3)
2019/07/17 16:20:22.902274 ctdb-recoverd[3619]: monitor_cluster starting
2019/07/17 16:20:22.902843 ctdb-recoverd[3619]: Initial recovery master set - forcing election
2019/07/17 16:20:22.902937 ctdbd[3529]: This node (0) is now the recovery master
2019/07/17 16:20:23.903107 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:24.904043 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:25.905100 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:25.905191 ctdb-recoverd[3619]: Election period ended
2019/07/17 16:20:25.905477 ctdb-recoverd[3619]: Node:0 was in recovery mode. Start recovery process
2019/07/17 16:20:25.905497 ctdb-recoverd[3619]: ../ctdb/server/ctdb_recoverd.c:1277 Starting do_recovery
2019/07/17 16:20:25.905505 ctdb-recoverd[3619]: Attempting to take recovery lock (/silustre/ctdb/.lock)
2019/07/17 16:20:25.905554 ctdb-recoverd[3619]: Set cluster mutex helper to "/usr/libexec/ctdb/ctdb_mutex_fcntl_helper"
2019/07/17 16:20:25.910979 ctdb-recoverd[3619]: Recovery lock taken successfully by recovery daemon
2019/07/17 16:20:25.911009 ctdb-recoverd[3619]: ../ctdb/server/ctdb_recoverd.c:1336 Recovery initiated due to problem with node 0
2019/07/17 16:20:25.911057 ctdb-recoverd[3619]: ../ctdb/server/ctdb_recoverd.c:1361 Recovery - created remote databases
2019/07/17 16:20:25.911171 ctdb-recoverd[3619]: ../ctdb/server/ctdb_recoverd.c:1390 Recovery - updated flags
2019/07/17 16:20:25.911194 ctdb-recoverd[3619]: Set recovery_helper to "/usr/libexec/ctdb/ctdb_recovery_helper"
2019/07/17 16:20:25.915179 ctdb-recovery[3622]: Set recovery mode to ACTIVE
2019/07/17 16:20:25.915303 ctdbd[3529]: Recovery has started
2019/07/17 16:20:26.012141 ctdb-recovery[3622]: start_recovery event finished
2019/07/17 16:20:26.012231 ctdb-recovery[3622]: updated VNNMAP
2019/07/17 16:20:26.012247 ctdb-recovery[3622]: recover database 0x6645c6c4
2019/07/17 16:20:26.012261 ctdb-recovery[3622]: recover database 0x7132c184
2019/07/17 16:20:26.012414 ctdbd[3529]: Freeze db: ctdb.tdb frozen
2019/07/17 16:20:26.012466 ctdbd[3529]: Freeze db: secrets.tdb frozen
2019/07/17 16:20:26.431020 ctdbd[3529]: Thaw db: ctdb.tdb generation 116089860
2019/07/17 16:20:26.431049 ctdbd[3529]: Release freeze handle for db ctdb.tdb
2019/07/17 16:20:26.431140 ctdbd[3529]: Thaw db: secrets.tdb generation 116089860
2019/07/17 16:20:26.431156 ctdbd[3529]: Release freeze handle for db secrets.tdb
2019/07/17 16:20:26.431277 ctdb-recovery[3622]: 2 of 2 databases recovered
2019/07/17 16:20:26.431314 ctdbd[3529]: Recovery mode set to NORMAL
2019/07/17 16:20:26.431344 ctdbd[3529]: Set cluster mutex helper to "/usr/libexec/ctdb/ctdb_mutex_fcntl_helper"
2019/07/17 16:20:26.436011 ctdb-recovery[3622]: Set recovery mode to NORMAL
2019/07/17 16:20:26.436060 ctdbd[3529]: Recovery has finished
2019/07/17 16:20:26.527461 ctdbd[3529]: Set runstate to STARTUP (4)
2019/07/17 16:20:26.527516 ctdb-recovery[3622]: recovered event finished
2019/07/17 16:20:26.527626 ctdb-recoverd[3619]: Takeover run starting
2019/07/17 16:20:26.527658 ctdb-recoverd[3619]: Set takeover_helper to "/usr/libexec/ctdb/ctdb_takeover_helper"
2019/07/17 16:20:26.530983 ctdb-takeover[3678]: No nodes available to host public IPs yet
2019/07/17 16:20:26.622224 ctdb-recoverd[3619]: Takeover run completed successfully
2019/07/17 16:20:26.622268 ctdb-recoverd[3619]: ../ctdb/server/ctdb_recoverd.c:1408 Recovery complete
2019/07/17 16:20:26.622279 ctdb-recoverd[3619]: Resetting ban count to 0 for all nodes
2019/07/17 16:20:26.622286 ctdb-recoverd[3619]: Just finished a recovery. New recoveries will now be suppressed for the rerecovery timeout (10 seconds)
2019/07/17 16:20:26.622293 ctdb-recoverd[3619]: Disabling recoveries for 10 seconds
2019/07/17 16:20:26.905844 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:26.905886 ctdbd[3529]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:26.906089 ctdb-recoverd[3619]: Initial interface fetched
2019/07/17 16:20:26.906191 ctdb-recoverd[3619]: Trigger takeoverrun
2019/07/17 16:20:26.906280 ctdb-recoverd[3619]: Takeover run starting
2019/07/17 16:20:26.909630 ctdb-takeover[3705]: No nodes available to host public IPs yet
2019/07/17 16:20:27.000163 ctdb-recoverd[3619]: Takeover run completed successfully
2019/07/17 16:20:27.906153 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:27.906234 ctdbd[3529]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:28.906785 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:28.906872 ctdbd[3529]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:29.907354 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:29.907432 ctdbd[3529]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:30.908307 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:30.908380 ctdbd[3529]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:31.909422 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:31.909502 ctdbd[3529]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:32.903310 ctdbd[3529]: 10.100.180.5:4379: connected to 10.100.180.6:4379 - 1 connected
2019/07/17 16:20:32.910467 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:32.910528 ctdbd[3529]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:33.716179 ctdb-recoverd[3619]: Election period ended
2019/07/17 16:20:33.910646 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:33.910715 ctdbd[3529]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:33.915985 ctdb-recoverd[3619]: Node:1 was in recovery mode. Start recovery process
2019/07/17 16:20:33.916006 ctdb-recoverd[3619]: ../ctdb/server/ctdb_recoverd.c:1277 Starting do_recovery
2019/07/17 16:20:33.916019 ctdb-recoverd[3619]: Unable to begin - recoveries are disabled
2019/07/17 16:20:34.911637 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:34.911685 ctdbd[3529]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:35.911864 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:35.911933 ctdbd[3529]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:36.623830 ctdb-recoverd[3619]: Reenabling recoveries after timeout
2019/07/17 16:20:36.912139 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:36.912181 ctdbd[3529]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:37.221784 ctdb-recoverd[3619]: Election period ended
2019/07/17 16:20:37.912764 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:37.912807 ctdbd[3529]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:37.919771 ctdb-recoverd[3619]: ../ctdb/server/ctdb_recoverd.c:1277 Starting do_recovery
2019/07/17 16:20:37.919791 ctdb-recoverd[3619]: Already holding recovery lock
2019/07/17 16:20:37.919799 ctdb-recoverd[3619]: ../ctdb/server/ctdb_recoverd.c:1336 Recovery initiated due to problem with node 0
2019/07/17 16:20:37.920320 ctdb-recoverd[3619]: ../ctdb/server/ctdb_recoverd.c:1361 Recovery - created remote databases
2019/07/17 16:20:37.921012 ctdb-recoverd[3619]: ../ctdb/server/ctdb_recoverd.c:1390 Recovery - updated flags
2019/07/17 16:20:37.924736 ctdbd[3529]: Recovery mode set to ACTIVE
2019/07/17 16:20:37.924979 ctdb-recovery[3732]: Set recovery mode to ACTIVE
2019/07/17 16:20:37.925077 ctdbd[3529]: Recovery has started
2019/07/17 16:20:38.017140 ctdb-recovery[3732]: start_recovery event finished
2019/07/17 16:20:38.017445 ctdb-recovery[3732]: updated VNNMAP
2019/07/17 16:20:38.017462 ctdb-recovery[3732]: recover database 0x6645c6c4
2019/07/17 16:20:38.017473 ctdb-recovery[3732]: recover database 0x7132c184
2019/07/17 16:20:38.017624 ctdbd[3529]: Freeze db: ctdb.tdb
2019/07/17 16:20:38.017831 ctdbd[3529]: Freeze db: secrets.tdb
2019/07/17 16:20:38.608837 ctdbd[3529]: Thaw db: ctdb.tdb generation 1232096721
2019/07/17 16:20:38.608881 ctdbd[3529]: Release freeze handle for db ctdb.tdb
2019/07/17 16:20:38.811567 ctdbd[3529]: Thaw db: secrets.tdb generation 1232096721
2019/07/17 16:20:38.811598 ctdbd[3529]: Release freeze handle for db secrets.tdb
2019/07/17 16:20:38.812301 ctdb-recovery[3732]: 2 of 2 databases recovered
2019/07/17 16:20:38.812393 ctdbd[3529]: Recovery mode set to NORMAL
2019/07/17 16:20:38.817415 ctdb-recovery[3732]: Set recovery mode to NORMAL
2019/07/17 16:20:38.817510 ctdbd[3529]: Recovery has finished
2019/07/17 16:20:38.909127 ctdb-recovery[3732]: recovered event finished
2019/07/17 16:20:38.909204 ctdb-recoverd[3619]: Takeover run starting
2019/07/17 16:20:38.912851 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:38.912871 ctdbd[3529]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:38.912919 ctdb-takeover[3788]: No nodes available to host public IPs yet
2019/07/17 16:20:39.003726 ctdb-recoverd[3619]: Takeover run completed successfully
2019/07/17 16:20:39.003759 ctdb-recoverd[3619]: ../ctdb/server/ctdb_recoverd.c:1408 Recovery complete
2019/07/17 16:20:39.003767 ctdb-recoverd[3619]: Resetting ban count to 0 for all nodes
2019/07/17 16:20:39.003823 ctdb-recoverd[3619]: Just finished a recovery. New recoveries will now be suppressed for the rerecovery timeout (10 seconds)
2019/07/17 16:20:39.003832 ctdb-recoverd[3619]: Disabling recoveries for 10 seconds
2019/07/17 16:20:39.914803 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:39.914880 ctdbd[3529]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:40.006449 ctdb-recoverd[3619]: Takeover run starting
2019/07/17 16:20:40.010744 ctdb-takeover[3815]: No nodes available to host public IPs yet
2019/07/17 16:20:40.105394 ctdb-recoverd[3619]: Takeover run completed successfully
2019/07/17 16:20:40.919956 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:40.920002 ctdbd[3529]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:41.923099 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:41.923176 ctdbd[3529]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:42.923353 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:42.923426 ctdbd[3529]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:43.924154 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:43.924221 ctdbd[3529]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:44.924280 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:44.924462 ctdbd[3529]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:45.924913 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:45.924966 ctdbd[3529]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:46.925104 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:46.925182 ctdbd[3529]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:47.925680 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:47.925740 ctdbd[3529]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:48.926235 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:48.926297 ctdbd[3529]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:49.004719 ctdb-recoverd[3619]: Reenabling recoveries after timeout
2019/07/17 16:20:49.926575 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:49.926659 ctdbd[3529]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:50.926884 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:50.926925 ctdbd[3529]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:51.927463 ctdbd[3529]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:51.927544 ctdbd[3529]: ctdb_recheck_persistent_health: OK[2] FAIL[0]
2019/07/17 16:20:51.927556 ctdbd[3529]: Running the "startup" event.
2019/07/17 16:20:53.080902 ctdb-eventd[3531]: 50.samba: Redirecting to /bin/systemctl start smb.service
2019/07/17 16:20:53.103147 ctdbd[3529]: startup event OK - enabling monitoring
2019/07/17 16:20:53.103171 ctdbd[3529]: Set runstate to RUNNING (5)
2019/07/17 16:20:55.573132 ctdbd[3529]: monitor event OK - node re-enabled
2019/07/17 16:20:55.573164 ctdbd[3529]: Node became HEALTHY. Ask recovery master to reallocate IPs
2019/07/17 16:20:55.573450 ctdb-recoverd[3619]: Node 0 has changed flags - now 0x0  was 0x2
2019/07/17 16:20:56.030893 ctdb-recoverd[3619]: Unassigned IP 192.168.56.23 can be served by this node
2019/07/17 16:20:56.030932 ctdb-recoverd[3619]: Unassigned IP 192.168.56.22 can be served by this node
2019/07/17 16:20:56.031036 ctdb-recoverd[3619]: Trigger takeoverrun
2019/07/17 16:20:56.032310 ctdb-recoverd[3619]: Takeover run starting
2019/07/17 16:20:56.036718 ctdbd[3529]: Takeover of IP 192.168.56.23/24 on interface eth2
2019/07/17 16:20:56.037084 ctdbd[3529]: Takeover of IP 192.168.56.22/24 on interface eth2
2019/07/17 16:20:56.326322 ctdb-recoverd[3619]: Takeover run completed successfully
2019/07/17 16:20:58.441140 ctdb-recoverd[3619]: Node 1 has changed flags - now 0x0  was 0x2
2019/07/17 16:20:59.036209 ctdb-recoverd[3619]: Takeover run starting
2019/07/17 16:20:59.040393 ctdbd[3529]: Release of IP 192.168.56.23/24 on interface eth2  node:1
2019/07/17 16:20:59.592703 ctdb-recoverd[3619]: Takeover run completed successfully

-------------------------------------------------------------------------------



# `log.ctdb` from `lamella_t2` 

-------------------------------------------------------------------------------

2019/07/17 16:20:28.351194 ctdbd[7062]: CTDB starting on node
2019/07/17 16:20:28.358907 ctdbd[7063]: Starting CTDBD (Version 4.8.3) as PID: 7063
2019/07/17 16:20:28.359165 ctdbd[7063]: Created PID file /run/ctdb/ctdbd.pid
2019/07/17 16:20:28.359275 ctdbd[7063]: Removed stale socket /var/run/ctdb/ctdbd.socket
2019/07/17 16:20:28.359317 ctdbd[7063]: Listening to ctdb socket /var/run/ctdb/ctdbd.socket
2019/07/17 16:20:28.359335 ctdbd[7063]: Set real-time scheduler priority
2019/07/17 16:20:28.359643 ctdbd[7063]: Starting event daemon /usr/libexec/ctdb/ctdb_eventd -e /etc/ctdb/events.d -s /var/run/ctdb/eventd.sock -P 7063 -l file:/var/log/log.ctdb -d NOTICE
2019/07/17 16:20:28.359894 ctdbd[7063]: connect() failed, errno=2
2019/07/17 16:20:28.363006 ctdb-eventd[7064]: daemon started, pid=7064
2019/07/17 16:20:28.363111 ctdb-eventd[7064]: listening on /var/run/ctdb/eventd.sock
2019/07/17 16:20:29.360124 ctdbd[7063]: Set runstate to INIT (1)
2019/07/17 16:20:29.501953 ctdbd[7063]: PNN is 1
2019/07/17 16:20:29.518305 ctdbd[7063]: Vacuuming is disabled for non-volatile database ctdb.tdb
2019/07/17 16:20:29.518334 ctdbd[7063]: Attached to database '/var/lib/ctdb/persistent/ctdb.tdb.1' with flags 0x400
2019/07/17 16:20:29.522648 ctdbd[7063]: Vacuuming is disabled for non-volatile database secrets.tdb
2019/07/17 16:20:29.522666 ctdbd[7063]: Attached to database '/var/lib/ctdb/persistent/secrets.tdb.1' with flags 0x400
2019/07/17 16:20:29.522685 ctdbd[7063]: Freeze db: secrets.tdb
2019/07/17 16:20:29.522710 ctdbd[7063]: Set lock helper to "/usr/libexec/ctdb/ctdb_lock_helper"
2019/07/17 16:20:29.525698 ctdbd[7063]: Freeze db: ctdb.tdb
2019/07/17 16:20:29.528152 ctdbd[7063]: Set runstate to SETUP (2)
2019/07/17 16:20:29.621721 ctdbd[7063]: Keepalive monitoring has been started
2019/07/17 16:20:29.621764 ctdbd[7063]: Set runstate to FIRST_RECOVERY (3)
2019/07/17 16:20:29.622016 ctdb-recoverd[7152]: monitor_cluster starting
2019/07/17 16:20:29.623133 ctdb-recoverd[7152]: Initial recovery master set - forcing election
2019/07/17 16:20:29.623272 ctdbd[7063]: This node (1) is now the recovery master
2019/07/17 16:20:30.125148 ctdbd[7063]: Remote node (0) is now the recovery master
2019/07/17 16:20:30.623080 ctdbd[7063]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:31.626675 ctdbd[7063]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:32.627198 ctdbd[7063]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:33.128323 ctdb-recoverd[7152]: Election period ended
2019/07/17 16:20:33.128813 ctdb-recoverd[7152]:  Current recmaster node 0 does not have CAP_RECMASTER, but we (node 1) have - force an election
2019/07/17 16:20:33.128890 ctdbd[7063]: This node (1) is now the recovery master
2019/07/17 16:20:33.628365 ctdbd[7063]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:33.630497 ctdbd[7063]: Remote node (0) is now the recovery master
2019/07/17 16:20:34.623190 ctdbd[7063]: 10.100.180.6:4379: connected to 10.100.180.5:4379 - 1 connected
2019/07/17 16:20:34.629387 ctdbd[7063]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:35.630350 ctdbd[7063]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:36.631414 ctdbd[7063]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:36.631515 ctdb-recoverd[7152]: Election period ended
2019/07/17 16:20:36.834360 ctdbd[7063]: Recovery has started
2019/07/17 16:20:36.926711 ctdbd[7063]: Freeze db: ctdb.tdb frozen
2019/07/17 16:20:36.926764 ctdbd[7063]: Freeze db: secrets.tdb frozen
2019/07/17 16:20:37.517861 ctdbd[7063]: Thaw db: ctdb.tdb generation 1232096721
2019/07/17 16:20:37.517894 ctdbd[7063]: Release freeze handle for db ctdb.tdb
2019/07/17 16:20:37.718735 ctdbd[7063]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:37.718769 ctdbd[7063]: ../ctdb/server/ctdb_monitor.c:323 in recovery. Wait one more second
2019/07/17 16:20:37.720591 ctdbd[7063]: Thaw db: secrets.tdb generation 1232096721
2019/07/17 16:20:37.720618 ctdbd[7063]: Release freeze handle for db secrets.tdb
2019/07/17 16:20:37.721284 ctdbd[7063]: Recovery mode set to NORMAL
2019/07/17 16:20:37.721327 ctdbd[7063]: Set cluster mutex helper to "/usr/libexec/ctdb/ctdb_mutex_fcntl_helper"
2019/07/17 16:20:37.726376 ctdbd[7063]: Recovery has finished
2019/07/17 16:20:37.816418 ctdbd[7063]: Set runstate to STARTUP (4)
2019/07/17 16:20:37.818213 ctdb-recoverd[7152]: Disabling takeover runs for 60 seconds
2019/07/17 16:20:37.912742 ctdb-recoverd[7152]: Reenabling takeover runs
2019/07/17 16:20:38.635807 ctdb-recoverd[7152]: Initial interface fetched
2019/07/17 16:20:38.635972 ctdb-recoverd[7152]: Trigger takeoverrun
2019/07/17 16:20:38.719221 ctdbd[7063]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:38.719285 ctdbd[7063]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:38.915472 ctdb-recoverd[7152]: Disabling takeover runs for 60 seconds
2019/07/17 16:20:39.014417 ctdb-recoverd[7152]: Reenabling takeover runs
2019/07/17 16:20:39.720203 ctdbd[7063]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:39.720246 ctdbd[7063]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:40.720646 ctdbd[7063]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:40.720702 ctdbd[7063]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:41.720928 ctdbd[7063]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:41.721002 ctdbd[7063]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:42.722029 ctdbd[7063]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:42.722103 ctdbd[7063]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:43.722286 ctdbd[7063]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:43.722331 ctdbd[7063]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:44.722542 ctdbd[7063]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:44.722592 ctdbd[7063]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:45.723036 ctdbd[7063]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:45.723092 ctdbd[7063]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:46.723493 ctdbd[7063]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:46.723574 ctdbd[7063]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:47.724062 ctdbd[7063]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:47.724119 ctdbd[7063]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:48.724388 ctdbd[7063]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:48.724475 ctdbd[7063]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:49.724743 ctdbd[7063]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:49.724801 ctdbd[7063]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:50.725283 ctdbd[7063]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:50.725366 ctdbd[7063]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/17 16:20:51.725704 ctdbd[7063]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/17 16:20:51.725815 ctdbd[7063]: ctdb_recheck_persistent_health: OK[2] FAIL[0]
2019/07/17 16:20:51.725843 ctdbd[7063]: Running the "startup" event.
2019/07/17 16:20:52.636301 ctdb-eventd[7064]: 50.samba: Redirecting to /bin/systemctl start smb.service
2019/07/17 16:20:52.658460 ctdbd[7063]: startup event OK - enabling monitoring
2019/07/17 16:20:52.658482 ctdbd[7063]: Set runstate to RUNNING (5)
2019/07/17 16:20:54.482387 ctdb-recoverd[7152]: Node 0 has changed flags - now 0x0  was 0x2
2019/07/17 16:20:54.941290 ctdb-recoverd[7152]: Disabling takeover runs for 60 seconds
2019/07/17 16:20:55.144670 ctdbd[7063]: Monitoring event was cancelled
2019/07/17 16:20:55.235370 ctdb-recoverd[7152]: Reenabling takeover runs
2019/07/17 16:20:57.348979 ctdbd[7063]: monitor event OK - node re-enabled
2019/07/17 16:20:57.349010 ctdbd[7063]: Node became HEALTHY. Ask recovery master to reallocate IPs
2019/07/17 16:20:57.349874 ctdb-recoverd[7152]: Node 1 has changed flags - now 0x0  was 0x2
2019/07/17 16:20:57.945203 ctdb-recoverd[7152]: Disabling takeover runs for 60 seconds
2019/07/17 16:20:58.314111 ctdbd[7063]: Takeover of IP 192.168.56.23/24 on interface eth2
2019/07/17 16:20:58.501679 ctdb-recoverd[7152]: Reenabling takeover runs

-------------------------------------------------------------------------------

### End of progress summary 

```

**refer to ctdb_live2.md** for additional documentation!

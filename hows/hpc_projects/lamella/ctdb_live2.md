## CTDB Live: Part II 

### July 19th, 2019 

- Bugs:

  - core dumps on `lamella_t1`

  - issues with udevs on `lamella_t2`

  - as always, we need to disable `enp0s3` for things to run smoothly

- Current **CTDB Start Procedure** 

 - check if lustre parallel file system is mounted correctly 

 - check ldap client `systemctl status nslcd` 

 - check firewall port: `sudo firewall-cmd --list-ports`

   - port 4379 should be listed 

 - check allowed firewall services: `sudo firewall-cmd --list-services`

   - ldap, samba should be listed 

- Current **CTDB Status** 

  - Only root can talk to the ctdbd socket, so reg users cannot execute the `ctdb` command 

    - `connect() failed, errno=13 Failed to connect to CTDB daemon (/var/run/ctdb/ctdbd.socket)` 
 
  - Current status: healthy

```
[estrella@lamella_t1 ~]$ sudo ctdb ip
Public IPs on node 0
192.168.56.22 1
192.168.56.23 0

[estrella@lamella_t1 ~]$ sudo ctdb ping all
response from 0 time=0.000053 sec  (2 clients)

[estrella@lamella_t1 ~]$ sudo ctdb status
Number of nodes:2
pnn:0 10.100.180.5     OK (THIS NODE)
pnn:1 10.100.180.6     OK
Generation:1881472308
Size:2
hash:0 lmaster:0
hash:1 lmaster:1
Recovery mode:NORMAL (0)
Recovery master:1

```

- **Testing** 

1. created files from client, files visible from both samba share IPs

 - ctdb status remained healthy 

2. stop ctdb on node 0 (10.100.180.5), & see if recovery master (node 1: 10.100.180.6) can take over 

  - node 0 hosts 192.168.56.23 (client connected to at time of testing) 

  - node 1 hosts 192.168.56.22 

  - samba & ctdb systemd status'

```
[estrella@lamella_t1 lambella]$ sudo systemctl status ctdb
● ctdb.service - CTDB
   Loaded: loaded (/usr/lib/systemd/system/ctdb.service; disabled; vendor preset: disabled)
   Active: inactive (dead)
     Docs: man:ctdbd(1)
           man:ctdb(7)

Jul 19 10:44:22 lamella_t1.biohpc.new systemd[1]: Starting CTDB...
Jul 19 10:44:29 lamella_t1.biohpc.new systemd[1]: Started CTDB.
Jul 19 15:03:10 lamella_t1.biohpc.new systemd[1]: Stopping CTDB...
Jul 19 15:03:12 lamella_t1.biohpc.new systemd[1]: Stopped CTDB.
[estrella@lamella_t1 lambella]$ 
[estrella@lamella_t1 lambella]$ 
[estrella@lamella_t1 lambella]$ 
[estrella@lamella_t1 lambella]$ 
[estrella@lamella_t1 lambella]$ sudo systemctl status smb
● smb.service - Samba SMB Daemon
   Loaded: loaded (/usr/lib/systemd/system/smb.service; disabled; vendor preset: disabled)
   Active: inactive (dead) since Fri 2019-07-19 15:03:11 CDT; 19s ago
     Docs: man:smbd(8)
           man:samba(7)
           man:smb.conf(5)
  Process: 5398 ExecStart=/usr/sbin/smbd --foreground --no-process-group $SMBDOPTIONS (code=killed, signal=TERM)
 Main PID: 5398 (code=killed, signal=TERM)
   Status: "smbd: ready to serve connections..."

Jul 19 10:44:56 lamella_t1.biohpc.new systemd[1]: Starting Samba SMB Daemon...
Jul 19 10:45:00 lamella_t1.biohpc.new smbd[5398]: [2019/07/19 10:45:00.646378,  0] ../lib/util/become_daemon.c:138(daemon_ready)
Jul 19 10:45:00 lamella_t1.biohpc.new smbd[5398]:   daemon_ready: STATUS=daemon 'smbd' finished starting up and ready to serve connections
Jul 19 10:45:00 lamella_t1.biohpc.new systemd[1]: Started Samba SMB Daemon.
Jul 19 15:03:11 lamella_t1.biohpc.new systemd[1]: Stopping Samba SMB Daemon...
Jul 19 15:03:11 lamella_t1.biohpc.new systemd[1]: Stopped Samba SMB Daemon.


# NOTE: no changes in recovery node's smb & ctdb daemons

```

  - `log.ctdb` logs on node 1 (`lamella_t2`) 

```

2019/07/19 15:02:56.577765 ctdbd[4865]: 10.100.180.6:4379: node 10.100.180.5:4379 is dead: 0 connected
2019/07/19 15:02:56.577813 ctdbd[4865]: Tearing down connection to dead node :0
2019/07/19 15:02:57.208695 ctdb-recoverd[4954]: ../ctdb/server/ctdb_recoverd.c:2897 The vnnmap count is different from the number of active lmaster nodes: 2 vs 1
2019/07/19 15:02:57.208732 ctdb-recoverd[4954]: ../ctdb/server/ctdb_recoverd.c:1277 Starting do_recovery
2019/07/19 15:02:57.208740 ctdb-recoverd[4954]: Already holding recovery lock
2019/07/19 15:02:57.208746 ctdb-recoverd[4954]: ../ctdb/server/ctdb_recoverd.c:1336 Recovery initiated due to problem with node 1
2019/07/19 15:02:57.208778 ctdb-recoverd[4954]: ../ctdb/server/ctdb_recoverd.c:1361 Recovery - created remote databases
2019/07/19 15:02:57.208865 ctdb-recoverd[4954]: ../ctdb/server/ctdb_recoverd.c:1390 Recovery - updated flags
2019/07/19 15:02:57.212471 ctdbd[4865]: Recovery mode set to ACTIVE
2019/07/19 15:02:57.212519 ctdb-recovery[471]: Set recovery mode to ACTIVE
2019/07/19 15:02:57.212610 ctdbd[4865]: Recovery has started
2019/07/19 15:02:57.303672 ctdb-recovery[471]: start_recovery event finished
2019/07/19 15:02:57.303747 ctdb-recovery[471]: updated VNNMAP
2019/07/19 15:02:57.303761 ctdb-recovery[471]: recover database 0x7132c184
2019/07/19 15:02:57.303775 ctdb-recovery[471]: recover database 0x6645c6c4
2019/07/19 15:02:57.303913 ctdbd[4865]: Freeze db: secrets.tdb
2019/07/19 15:02:57.304045 ctdbd[4865]: Freeze db: ctdb.tdb
2019/07/19 15:02:59.418469 ctdbd[4865]: Thaw db: secrets.tdb generation 1935425737
2019/07/19 15:02:59.418500 ctdbd[4865]: Release freeze handle for db secrets.tdb
2019/07/19 15:02:59.418582 ctdbd[4865]: Thaw db: ctdb.tdb generation 1935425737
2019/07/19 15:02:59.418593 ctdbd[4865]: Release freeze handle for db ctdb.tdb
2019/07/19 15:02:59.418720 ctdb-recovery[471]: 2 of 2 databases recovered
2019/07/19 15:02:59.418756 ctdbd[4865]: Recovery mode set to NORMAL
2019/07/19 15:02:59.422844 ctdb-recovery[471]: Set recovery mode to NORMAL
2019/07/19 15:02:59.422893 ctdbd[4865]: Recovery has finished
2019/07/19 15:02:59.514763 ctdb-recovery[471]: recovered event finished
2019/07/19 15:02:59.515176 ctdb-recoverd[4954]: Takeover run starting
2019/07/19 15:02:59.518374 ctdbd[4865]: Takeover of IP 192.168.56.23/24 on interface eth2
2019/07/19 15:02:59.712593 ctdb-recoverd[4954]: Takeover run completed successfully
2019/07/19 15:02:59.712920 ctdb-recoverd[4954]: ../ctdb/server/ctdb_recoverd.c:1408 Recovery complete
2019/07/19 15:02:59.712935 ctdb-recoverd[4954]: Resetting ban count to 0 for all nodes
2019/07/19 15:02:59.712943 ctdb-recoverd[4954]: Just finished a recovery. New recoveries will now be suppressed for the rerecovery timeout (10 seconds)
2019/07/19 15:02:59.712951 ctdb-recoverd[4954]: Disabling recoveries for 10 seconds
2019/07/19 15:03:09.714950 ctdb-recoverd[4954]: Reenabling recoveries after timeout

# NOTE: take over successful: client still able to access samba share from BOTH virtual IPs! 

```

  - `log.ctdb` logs on node 0 (`lamella_t1`) 

```

2019/07/19 15:03:10.542836 ctdbd[5110]: Received SHUTDOWN command.
2019/07/19 15:03:10.542866 ctdbd[5110]: Shutdown sequence commencing.
2019/07/19 15:03:10.542874 ctdbd[5110]: Set runstate to SHUTDOWN (6)
2019/07/19 15:03:10.542881 ctdbd[5110]: Shutting down recovery daemon
2019/07/19 15:03:10.542893 ctdbd[5110]: Monitoring has been stopped
2019/07/19 15:03:10.542955 ctdb-recoverd[5199]: Received SIGTERM, exiting
2019/07/19 15:03:11.523175 ctdb-eventd[5112]: 10.interface: Killed 1/1 TCP connections to released IP 192.168.56.23
2019/07/19 15:03:11.588514 ctdbd[5110]: ../ctdb/server/ctdb_takeover.c:1605 Released 1 public IPs
2019/07/19 15:03:11.825195 ctdb-eventd[5112]: 50.samba: Redirecting to /bin/systemctl stop smb.service
2019/07/19 15:03:11.846582 ctdbd[5110]: Shutdown sequence complete, exiting.
2019/07/19 15:03:11.846606 ctdbd[5110]: CTDB daemon shutting down
2019/07/19 15:03:11.846637 ctdb-eventd[5112]: Received signal 15
2019/07/19 15:03:11.846652 ctdb-eventd[5112]: Shutting down


```

  - `sudo ctdb status` 

```
Number of nodes:2
pnn:0 10.100.180.5     DISCONNECTED|UNHEALTHY|INACTIVE
pnn:1 10.100.180.6     OK (THIS NODE)
Generation:1935425737
Size:1
hash:0 lmaster:1
Recovery mode:NORMAL (0)
Recovery master:1

```

- **Further Testing**: Restart CTDB on node 0 after shutdown


  - `log.ctdb` logs on node 1 (`lamella_t2`)

```
2019/07/19 15:18:50.443367 ctdbd[4865]: 10.100.180.6:4379: connected to 10.100.180.5:4379 - 1 connected
2019/07/19 15:18:53.329077 ctdb-recoverd[4954]: Election period ended
2019/07/19 15:18:56.831876 ctdb-recoverd[4954]: Election period ended
2019/07/19 15:18:57.196905 ctdb-recoverd[4954]: Node:0 was in recovery mode. Start recovery process
2019/07/19 15:18:57.196938 ctdb-recoverd[4954]: ../ctdb/server/ctdb_recoverd.c:1277 Starting do_recovery
2019/07/19 15:18:57.196947 ctdb-recoverd[4954]: Already holding recovery lock
2019/07/19 15:18:57.196954 ctdb-recoverd[4954]: ../ctdb/server/ctdb_recoverd.c:1336 Recovery initiated due to problem with node 1
2019/07/19 15:18:57.197453 ctdb-recoverd[4954]: ../ctdb/server/ctdb_recoverd.c:1361 Recovery - created remote databases
2019/07/19 15:18:57.198364 ctdb-recoverd[4954]: ../ctdb/server/ctdb_recoverd.c:1390 Recovery - updated flags
2019/07/19 15:18:57.202158 ctdbd[4865]: Recovery mode set to ACTIVE
2019/07/19 15:18:57.202326 ctdb-recovery[6427]: Set recovery mode to ACTIVE
2019/07/19 15:18:57.202468 ctdbd[4865]: Recovery has started
2019/07/19 15:18:57.305365 ctdb-recovery[6427]: start_recovery event finished
2019/07/19 15:18:57.305746 ctdb-recovery[6427]: updated VNNMAP
2019/07/19 15:18:57.305767 ctdb-recovery[6427]: recover database 0x7132c184
2019/07/19 15:18:57.305778 ctdb-recovery[6427]: recover database 0x6645c6c4
2019/07/19 15:18:57.305994 ctdbd[4865]: Freeze db: secrets.tdb
2019/07/19 15:18:57.306201 ctdbd[4865]: Freeze db: ctdb.tdb
2019/07/19 15:18:57.877179 ctdbd[4865]: Thaw db: ctdb.tdb generation 290515961
2019/07/19 15:18:57.877208 ctdbd[4865]: Release freeze handle for db ctdb.tdb
2019/07/19 15:18:58.250774 ctdbd[4865]: Thaw db: secrets.tdb generation 290515961
2019/07/19 15:18:58.250812 ctdbd[4865]: Release freeze handle for db secrets.tdb
2019/07/19 15:18:58.251177 ctdb-recovery[6427]: 2 of 2 databases recovered
2019/07/19 15:18:58.251441 ctdbd[4865]: Recovery mode set to NORMAL
2019/07/19 15:18:58.255875 ctdb-recovery[6427]: Set recovery mode to NORMAL
2019/07/19 15:18:58.256067 ctdbd[4865]: Recovery has finished
2019/07/19 15:18:58.348490 ctdb-recovery[6427]: recovered event finished
2019/07/19 15:18:58.348888 ctdb-recoverd[4954]: Takeover run starting
2019/07/19 15:18:58.443606 ctdb-recoverd[4954]: Takeover run completed successfully
2019/07/19 15:18:58.444020 ctdb-recoverd[4954]: ../ctdb/server/ctdb_recoverd.c:1408 Recovery complete
2019/07/19 15:18:58.444037 ctdb-recoverd[4954]: Resetting ban count to 0 for all nodes
2019/07/19 15:18:58.444045 ctdb-recoverd[4954]: Just finished a recovery. New recoveries will now be suppressed for the rerecovery timeout (10 seconds)
2019/07/19 15:18:58.444053 ctdb-recoverd[4954]: Disabling recoveries for 10 seconds
2019/07/19 15:18:58.445283 ctdb-recoverd[4954]: Takeover run starting
2019/07/19 15:18:58.542213 ctdb-recoverd[4954]: Takeover run completed successfully
2019/07/19 15:19:08.445889 ctdb-recoverd[4954]: Reenabling recoveries after timeout
2019/07/19 15:19:14.283780 ctdb-recoverd[4954]: Node 0 has changed flags - now 0x0  was 0x2
2019/07/19 15:19:14.467786 ctdb-recoverd[4954]: Takeover run starting
2019/07/19 15:19:14.471932 ctdbd[4865]: Release of IP 192.168.56.23/24 on interface eth2  node:0
2019/07/19 15:19:14.734648 ctdb-eventd[4866]: 10.interface: Killed 1/1 TCP connections to released IP 192.168.56.23
2019/07/19 15:19:14.988327 ctdb-recoverd[4954]: Takeover run completed successfully

```

  - `log.ctdb` logs on node 0 (`lamella_t1`)


```
2019/07/19 15:19:04.331974 ctdbd[787]: CTDB starting on node
2019/07/19 15:19:04.337937 ctdbd[788]: Starting CTDBD (Version 4.8.3) as PID: 788
2019/07/19 15:19:04.338113 ctdbd[788]: Created PID file /run/ctdb/ctdbd.pid
2019/07/19 15:19:04.338193 ctdbd[788]: Removed stale socket /var/run/ctdb/ctdbd.socket
2019/07/19 15:19:04.338232 ctdbd[788]: Listening to ctdb socket /var/run/ctdb/ctdbd.socket
2019/07/19 15:19:04.338249 ctdbd[788]: Set real-time scheduler priority
2019/07/19 15:19:04.338468 ctdbd[788]: Starting event daemon /usr/libexec/ctdb/ctdb_eventd -e /etc/ctdb/events.d -s /var/run/ctdb/eventd.sock -P 788 -l file:/var/log/log.ctdb -d NOTICE
2019/07/19 15:19:04.338695 ctdbd[788]: connect() failed, errno=2
2019/07/19 15:19:04.341737 ctdb-eventd[790]: daemon started, pid=790
2019/07/19 15:19:04.341837 ctdb-eventd[790]: listening on /var/run/ctdb/eventd.sock
2019/07/19 15:19:05.338946 ctdbd[788]: Set runstate to INIT (1)
2019/07/19 15:19:05.470781 ctdbd[788]: PNN is 0
2019/07/19 15:19:05.491532 ctdbd[788]: Vacuuming is disabled for non-volatile database secrets.tdb
2019/07/19 15:19:05.491567 ctdbd[788]: Attached to database '/var/lib/ctdb/persistent/secrets.tdb.0' with flags 0x400
2019/07/19 15:19:05.495757 ctdbd[788]: Vacuuming is disabled for non-volatile database ctdb.tdb
2019/07/19 15:19:05.495773 ctdbd[788]: Attached to database '/var/lib/ctdb/persistent/ctdb.tdb.0' with flags 0x400
2019/07/19 15:19:05.495792 ctdbd[788]: Freeze db: ctdb.tdb
2019/07/19 15:19:05.495816 ctdbd[788]: Set lock helper to "/usr/libexec/ctdb/ctdb_lock_helper"
2019/07/19 15:19:05.498765 ctdbd[788]: Freeze db: secrets.tdb
2019/07/19 15:19:05.501402 ctdbd[788]: Set runstate to SETUP (2)
2019/07/19 15:19:05.595810 ctdbd[788]: Keepalive monitoring has been started
2019/07/19 15:19:05.595850 ctdbd[788]: Set runstate to FIRST_RECOVERY (3)
2019/07/19 15:19:05.596074 ctdb-recoverd[877]: monitor_cluster starting
2019/07/19 15:19:05.596658 ctdb-recoverd[877]: Initial recovery master set - forcing election
2019/07/19 15:19:05.596731 ctdbd[788]: This node (0) is now the recovery master
2019/07/19 15:19:06.097938 ctdbd[788]: Remote node (1) is now the recovery master
2019/07/19 15:19:06.596842 ctdbd[788]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/19 15:19:07.597003 ctdbd[788]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/19 15:19:08.597284 ctdbd[788]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/19 15:19:09.099912 ctdb-recoverd[877]: Election period ended
2019/07/19 15:19:09.100152 ctdb-recoverd[877]:  Current recmaster node 1 does not have CAP_RECMASTER, but we (node 0) have - force an election
2019/07/19 15:19:09.100214 ctdbd[788]: This node (0) is now the recovery master
2019/07/19 15:19:09.597497 ctdbd[788]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/19 15:19:09.601590 ctdbd[788]: Remote node (1) is now the recovery master
2019/07/19 15:19:10.596837 ctdbd[788]: 10.100.180.5:4379: connected to 10.100.180.6:4379 - 1 connected
2019/07/19 15:19:10.600723 ctdbd[788]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/19 15:19:11.601230 ctdbd[788]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/19 15:19:12.467100 ctdbd[788]: ../ctdb/server/ctdb_monitor.c:502 Node 1 became healthy - force recovery for startup
2019/07/19 15:19:12.467251 ctdb-recoverd[877]: Node 1 has changed flags - now 0x0  was 0x2
2019/07/19 15:19:12.471421 ctdbd[788]: Recovery has started
2019/07/19 15:19:12.574920 ctdbd[788]: Freeze db: secrets.tdb frozen
2019/07/19 15:19:12.575182 ctdbd[788]: Freeze db: ctdb.tdb frozen
2019/07/19 15:19:12.602399 ctdb-recoverd[877]: Election period ended
2019/07/19 15:19:13.058315 ctdbd[788]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/19 15:19:13.058346 ctdbd[788]: ../ctdb/server/ctdb_monitor.c:323 in recovery. Wait one more second
2019/07/19 15:19:13.519285 ctdbd[788]: Thaw db: ctdb.tdb generation 290515961
2019/07/19 15:19:13.519315 ctdbd[788]: Release freeze handle for db ctdb.tdb
2019/07/19 15:19:13.519613 ctdbd[788]: Thaw db: secrets.tdb generation 290515961
2019/07/19 15:19:13.519627 ctdbd[788]: Release freeze handle for db secrets.tdb
2019/07/19 15:19:13.520347 ctdbd[788]: Recovery mode set to NORMAL
2019/07/19 15:19:13.520380 ctdbd[788]: Set cluster mutex helper to "/usr/libexec/ctdb/ctdb_mutex_fcntl_helper"
2019/07/19 15:19:13.524937 ctdbd[788]: Recovery has finished
2019/07/19 15:19:13.604493 ctdb-recoverd[877]: Initial interface fetched
2019/07/19 15:19:13.604611 ctdb-recoverd[877]: Trigger takeoverrun
2019/07/19 15:19:13.617228 ctdbd[788]: Set runstate to STARTUP (4)
2019/07/19 15:19:13.617972 ctdb-recoverd[877]: Disabling takeover runs for 60 seconds
2019/07/19 15:19:13.712586 ctdb-recoverd[877]: Reenabling takeover runs
2019/07/19 15:19:13.714361 ctdb-recoverd[877]: Disabling takeover runs for 60 seconds
2019/07/19 15:19:13.811259 ctdb-recoverd[877]: Reenabling takeover runs
2019/07/19 15:19:14.059348 ctdbd[788]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/19 15:19:14.059376 ctdbd[788]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/19 15:19:15.059902 ctdbd[788]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/19 15:19:15.059941 ctdbd[788]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/19 15:19:16.060173 ctdbd[788]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/19 15:19:16.060219 ctdbd[788]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/19 15:19:17.061038 ctdbd[788]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/19 15:19:17.061082 ctdbd[788]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/19 15:19:18.061581 ctdbd[788]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/19 15:19:18.061628 ctdbd[788]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/19 15:19:19.062015 ctdbd[788]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/19 15:19:19.062060 ctdbd[788]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/19 15:19:20.062652 ctdbd[788]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/19 15:19:20.062699 ctdbd[788]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/19 15:19:21.062768 ctdbd[788]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/19 15:19:21.062814 ctdbd[788]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/19 15:19:22.063304 ctdbd[788]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/19 15:19:22.063353 ctdbd[788]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/19 15:19:23.063660 ctdbd[788]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/19 15:19:23.063705 ctdbd[788]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/19 15:19:24.064694 ctdbd[788]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/19 15:19:24.064747 ctdbd[788]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/19 15:19:25.066705 ctdbd[788]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/19 15:19:25.066745 ctdbd[788]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/19 15:19:26.067000 ctdbd[788]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/19 15:19:26.067044 ctdbd[788]: ../ctdb/server/ctdb_monitor.c:334 wait for pending recoveries to end. Wait one more second.
2019/07/19 15:19:27.067994 ctdbd[788]: CTDB_WAIT_UNTIL_RECOVERED
2019/07/19 15:19:27.068055 ctdbd[788]: ctdb_recheck_persistent_health: OK[2] FAIL[0]
2019/07/19 15:19:27.068066 ctdbd[788]: Running the "startup" event.
2019/07/19 15:19:27.323631 ctdb-eventd[790]: 50.samba: Redirecting to /bin/systemctl start smb.service
2019/07/19 15:19:27.344924 ctdbd[788]: startup event OK - enabling monitoring
2019/07/19 15:19:27.344948 ctdbd[788]: Set runstate to RUNNING (5)
2019/07/19 15:19:29.551929 ctdbd[788]: monitor event OK - node re-enabled
2019/07/19 15:19:29.551976 ctdbd[788]: Node became HEALTHY. Ask recovery master to reallocate IPs
2019/07/19 15:19:29.552511 ctdb-recoverd[877]: Node 0 has changed flags - now 0x0  was 0x2
2019/07/19 15:19:29.736900 ctdb-recoverd[877]: Disabling takeover runs for 60 seconds
2019/07/19 15:19:30.067598 ctdbd[788]: Takeover of IP 192.168.56.23/24 on interface eth2
2019/07/19 15:19:30.257374 ctdb-recoverd[877]: Reenabling takeover runs


```

- `sudo ctdb status` 

```
[estrella@lamella_t2 lambella]$ sudo ctdb status
[sudo] password for estrella: 
Number of nodes:2
pnn:0 10.100.180.5     OK
pnn:1 10.100.180.6     OK (THIS NODE)
Generation:290515961
Size:2
hash:0 lmaster:0
hash:1 lmaster:1
Recovery mode:NORMAL (0)
Recovery master:1


# Looks good!
 
```

-----------------------------END of TESTING------------------------------


- **Links**

 - [smb.conf man page: samba 4.10](https://www.samba.org/samba/docs/current/man-html/smb.conf.5.html) 

 - [leads on Samba4 smbtorture](https://download.samba.org/pub/unpacked/ctdb/web/samba.html)

 - [more lead on why CTDB socket is in the wrong place](https://bugzilla.samba.org/show_bug.cgi?id=13700) 


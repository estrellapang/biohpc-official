## Replicating cloud.biohpc.swmed.edu: Essential Setup 

- **this piece of doc continues off of** `nextcloud_import_1.md`, in which a 10T HDD is partitioned & configured with a LVM filesystem---this was to accomodate the 2.4T of user data + SQL data from the production server; the VM that will be hosting the 


### lftp ops--migrating local cloud data to VM 

#### Host Server (phyiscal)


```
# download and configure default directory 

yum install vsftpd.x86_64 

vim /etc/vsftpd/vsftpd.conf

# add desired directory
------------------------
local_root=/mnt/box/gets
------------------------

systemctl restart vsftpd

```


#### Client Download 


```
# download client software
`sudo yum install lftp.x86_64`

# `estrella` may not be able to write into /acloud; could also add an ACL 
`chmod 777 /acloud` 


# make sure the remote permissions are also readable
`chmod -R 755 /mnt/gets/data` 

# parallel download multiple files while breaking files into segments

`lftp -u biohpcadmin -e 'mirror -P=21 --use-pget-n=9 /mnt/box/gets/data /acloud/cloud_data' ftp://198.215.56.37` 

  ## may not be a good option --> seem to slow file transfer when combined

  ## difference between options `-P` & `--use-pget-n` ???

# mirror only new files (`n` option) 

`lftp -u biohpcadmin -e 'mirror -n -P=12 /mnt/box/gets/model_cloud /acloud' ftp://198.215.56.37` 

# Remote Login to FTP server using lftp command prompt

`lftp ftp://biohpcadmin@198.215.56.37`

`lpwd` to list local directories (where files will be downloaded to) 

  ## one can use `cd` `ls` as they could in a normal shell 


## 2nd Day Update: VM crashed from Parallel transfer? 

---------------------------------------------------------------------------------------------------------------
[estrella@data1 acloud]$ lftp -u biohpcadmin -e 'mirror -P=21 /mnt/box/gets/data /acloud/cloud_data' ftp://198.215.56.37
Password: 
Bus errorcrop.png' at 0 (0%) [Receiving data]                                                                                              
[estrella@data1 acloud]$ packet_write_wait: Connection to 10.100.180.8 port 22: Broken pipe
---------------------------------------------------------------------------------------------------------------

 ##  attempt complete transfer by downloading ONLY NEW files from source

`lftp -u biohpcadmin -e 'mirror -n -P=9 /mnt/box/gets/data /acloud/cloud_data' ftp://198.215.56.37`

 ## file system status @ beginning of transfer: 

---------------------------------------------------------------------------------------------------------------
Filesystem                     Type      Size  Used Avail Use% Mounted on
/dev/mapper/centos-root        xfs        31G  4.4G   27G  15% /
devtmpfs                       devtmpfs  4.1G     0  4.1G   0% /dev
tmpfs                          tmpfs     4.2G     0  4.2G   0% /dev/shm
tmpfs                          tmpfs     4.2G  9.0M  4.1G   1% /run
tmpfs                          tmpfs     4.2G     0  4.2G   0% /sys/fs/cgroup
/dev/sda1                      xfs       1.1G  196M  868M  19% /boot
/dev/mapper/condensation-sky_1 xfs       3.3T  1.6T  1.8T  47% /acloud
tmpfs                          tmpfs     821M     0  821M   0% /run/user/1000
---------------------------------------------------------------------------------------------------------------

```

### Dependencies Setup 

- **MySQL Server** 

- according to  [NC v.15 Link](https://docs.nextcloud.com/server/15/admin_manual/installation/system_requirements.html) 

 - MYSQL 5.x is recommended, but the VM has a newer version: 

```
[estrella@data1 ~]$ mysqld --version
/usr/sbin/mysqld  Ver 8.0.17 for Linux on x86_64 (MySQL Community Server - GPL) 

trella@data1 ~]$ yum list installed | grep -i mysql
mysql-community-client.x86_64        8.0.17-1.el7                   @mysql80-community
mysql-community-common.x86_64        8.0.17-1.el7                   @mysql80-community
mysql-community-libs.x86_64          8.0.17-1.el7                   @mysql80-community
mysql-community-libs-compat.x86_64   8.0.17-1.el7                   @mysql80-community
mysql-community-server.x86_64        8.0.17-1.el7                   @mysql80-community
mysql80-community-release.noarch     el7-3                          installed           ### DO NOT REMOVE!
php72w-mysql.x86_64                  7.2.19-1.w7                    @webtatic  

## remove MYSQL v.8 related packages:

[estrella@data1 ~]$ sudo yum remove mysql-community-server-8.0.17-1.el7.x86_64 mysql-community-client-8.0.17-1.el7.x86_64 mysql-community-libs-compat-8.0.17-1.el7.x86_64 mysql-community-libs-8.0.17-1.el7.x86_64 mysql-community-common-8.0.17-1.el7.x86_64

Dependencies Resolved

============================================================================================================================================
 Package                                     Arch                   Version                        Repository                          Size
============================================================================================================================================
Removing:
 mysql-community-client                      x86_64                 8.0.17-1.el7                   @mysql80-community                 146 M
 mysql-community-common                      x86_64                 8.0.17-1.el7                   @mysql80-community                 8.3 M
 mysql-community-libs                        x86_64                 8.0.17-1.el7                   @mysql80-community                  14 M
 mysql-community-libs-compat                 x86_64                 8.0.17-1.el7                   @mysql80-community                 9.5 M
 mysql-community-server                      x86_64                 8.0.17-1.el7                   @mysql80-community                 1.8 G
Removing for dependencies:
 php72w-mysql                                x86_64                 7.2.19-1.w7                    @webtatic                          347 k
 postfix                                     x86_64                 2:2.10.1-7.el7                 @anaconda                           12 M

Transaction Summary
============================================================================================================================================
Remove  5 Packages (+2 Dependent packages)


## ENABLE MYSQL 5.x repo

sudo yum vim /etc/yum.repos.d/mysql-community.repo
---------------------------------------------------------------------------------------------------------------
# Enable to use MySQL 5.7
[mysql57-community]
name=MySQL 5.7 Community Server
baseurl=http://repo.mysql.com/yum/mysql-5.7-community/el/7/$basearch/
enabled=1   								### ENABLE!!! 
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-mysql

[mysql80-community]
name=MySQL 8.0 Community Server
baseurl=http://repo.mysql.com/yum/mysql-8.0-community/el/7/$basearch/
enabled=0   								### DISABLE!!!
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-mysql
---------------------------------------------------------------------------------------------------------------

# VERIFY Enabled Repos 

[estrella@data1 ~]$ yum repolist

repo id                                                     repo name                                                                 status
base/7/x86_64                                               CentOS-7 - Base                                                           10,097
*epel/x86_64                                                Extra Packages for Enterprise Linux 7 - x86_64                            13,415
extras/7/x86_64                                             CentOS-7 - Extras                                                            304
mysql-connectors-community/x86_64                           MySQL Connectors Community                                                   118
mysql-tools-community/x86_64                                MySQL Tools Community                                                         95
mysql57-community/x86_64          ### Good ###              MySQL 5.7 Community Server                                                   364
updates/7/x86_64                                            CentOS-7 - Updates                                                           332
webtatic/x86_64                                             Webtatic Repository EL7 - x86_64                                             587
repolist: 25,312

```

- **Reinstall MYSQL 5.7 Server:** 

```
----------------------------------------------------------------------------------------------------------------
sudo yum install mysql-community-server.x86_64

Installed:
  mysql-community-server.x86_64 0:5.7.27-1.el7                                                                                              

Dependency Installed:
  mysql-community-client.x86_64 0:5.7.27-1.el7   mysql-community-common.x86_64 0:5.7.27-1.el7   mysql-community-libs.x86_64 0:5.7.27-1.el7  

-----------------------------------------------------------------------------------------------------------------

# install deleted packages: 

-----------------------------------------------------------------------------------------------------------------
sudo yum install php72w-mysql

php72w-mysql.x86_64 0:7.2.22-1.w7
mysql-community-libs-compat.x86_64 0:5.7.27-1.el7

sudo yum install postfix

postfix.x86_64 2:2.10.1-7.el7 
-----------------------------------------------------------------------------------------------------------------

### CORRECTION NEEDED ### 
#
# 1st attempt removal attempt missed certain steps, which caused new mysql to fail upon start 

#Error:

-----------------------------------------------------------------------------------------------------------------
Oct 10 19:05:33 data1.biohpc.new mysqld[4917]: Unable to determine if daemon is running: No such file or directory
Oct 10 19:05:33 data1.biohpc.new systemd[1]: mysqld.service: control process exited, code=exited status=1
Oct 10 19:05:33 data1.biohpc.new systemd[1]: Failed to start MySQL Server.
-----------------------------------------------------------------------------------------------------------------

# IF WE DO NOT REMOVE /var/lib/mysql & /etc/my.cnf/ --> the newly installed server will access the old data!

[here in this link](https://orenjibaka.wordpress.com/2016/02/09/how-to-completely-uninstall-remove-mysql-server-on-centos-linux/)


### Remove and Reinstall Once More ###

sudo yum remove mysql-community-server.x86_64 mysql-community-client.x86_64 mysql-community-common.x86_64 mysql-community-libs.x86_64  

============================================================================================================================================
 Package                                     Arch                   Version                        Repository                          Size
============================================================================================================================================
Removing:
 mysql-community-client                      x86_64                 5.7.27-1.el7                   @mysql57-community                 107 M
 mysql-community-common                      x86_64                 5.7.27-1.el7                   @mysql57-community                 2.6 M
 mysql-community-libs                        x86_64                 5.7.27-1.el7                   @mysql57-community                 9.5 M
 mysql-community-server                      x86_64                 5.7.27-1.el7                   @mysql57-community                 746 M
Removing for dependencies:
 mysql-community-libs-compat                 x86_64                 5.7.27-1.el7                   @mysql57-community                 9.2 M
 php72w-mysql                                x86_64                 7.2.22-1.w7                    @webtatic                          347 k
 postfix                                     x86_64                 2:2.10.1-7.el7                 @base                               12 M

Transaction Summary
============================================================================================================================================
Remove  4 Packages (+3 Dependent packages)

## IMPORTANT ADDITIONAL REMOVAL STEPS

sudo rm -rf /var/lib/mysql/

sudo rm -rf /etc/my.cnf.rpmsave


## Next, reinstall as before

# MYSQL SERVER RESTART LOG: (grab temporary password HERE--> look, TIMESTAMP is ALSO OFF?)

----------------------------------------------------------------------------------------------------------------
2019-10-11T00:43:13.363839Z 0 [Warning] TIMESTAMP with implicit DEFAULT value is deprecated. Please use --explicit_defaults_for_timestamp server option (see documentation for more details).
2019-10-11T00:43:15.584859Z 0 [Warning] InnoDB: New log files created, LSN=45790
2019-10-11T00:43:16.223437Z 0 [Warning] InnoDB: Creating foreign key constraint system tables.
2019-10-11T00:43:16.488402Z 0 [Warning] No existing UUID has been found, so we assume that this is the first time that this server has been started. Generating a new UUID: 1d08ef95-ebc0-11e9-832f-0800275798e9.
2019-10-11T00:43:16.519497Z 0 [Warning] Gtid table is not ready to be used. Table 'mysql.gtid_executed' cannot be opened.
2019-10-11T00:43:16.520031Z 1 [Note] A temporary password is generated for root@localhost: Sjilw9tm.mss
2019-10-11T00:43:56.495401Z 0 [Warning] TIMESTAMP with implicit DEFAULT value is deprecated. Please use --explicit_defaults_for_timestamp server option (see documentation for more details).
2019-10-11T00:43:56.497172Z 0 [Note] /usr/sbin/mysqld (mysqld 5.7.27) starting as process 5708 ...
2019-10-11T00:43:56.500389Z 0 [Note] InnoDB: PUNCH HOLE support available
2019-10-11T00:43:56.500422Z 0 [Note] InnoDB: Mutexes and rw_locks use GCC atomic builtins
2019-10-11T00:43:56.500428Z 0 [Note] InnoDB: Uses event mutexes
2019-10-11T00:43:56.500432Z 0 [Note] InnoDB: GCC builtin __atomic_thread_fence() is used for memory barrier
2019-10-11T00:43:56.500437Z 0 [Note] InnoDB: Compressed tables use zlib 1.2.11
2019-10-11T00:43:56.500448Z 0 [Note] InnoDB: Using Linux native AIO
2019-10-11T00:43:56.500730Z 0 [Note] InnoDB: Number of pools: 1
2019-10-11T00:43:56.500881Z 0 [Note] InnoDB: Using CPU crc32 instructions
2019-10-11T00:43:56.502771Z 0 [Note] InnoDB: Initializing buffer pool, total size = 128M, instances = 1, chunk size = 128M
2019-10-11T00:43:56.511162Z 0 [Note] InnoDB: Completed initialization of buffer pool
2019-10-11T00:43:56.513145Z 0 [Note] InnoDB: If the mysqld execution user is authorized, page cleaner thread priority can be changed. See the man page of setpriority().
2019-10-11T00:43:56.525864Z 0 [Note] InnoDB: Highest supported file format is Barracuda.
2019-10-11T00:43:56.594836Z 0 [Note] InnoDB: Creating shared tablespace for temporary tables
2019-10-11T00:43:56.595000Z 0 [Note] InnoDB: Setting file './ibtmp1' size to 12 MB. Physically writing the file full; Please wait ...
2019-10-11T00:43:56.855067Z 0 [Note] InnoDB: File './ibtmp1' size is now 12 MB.
2019-10-11T00:43:56.855945Z 0 [Note] InnoDB: 96 redo rollback segment(s) found. 96 redo rollback segment(s) are active.
2019-10-11T00:43:56.855956Z 0 [Note] InnoDB: 32 non-redo rollback segment(s) are active.
2019-10-11T00:43:56.856356Z 0 [Note] InnoDB: Waiting for purge to start
2019-10-11T00:43:56.906783Z 0 [Note] InnoDB: 5.7.27 started; log sequence number 2626959
2019-10-11T00:43:56.907036Z 0 [Note] InnoDB: Loading buffer pool(s) from /var/lib/mysql/ib_buffer_pool
2019-10-11T00:43:56.907202Z 0 [Note] Plugin 'FEDERATED' is disabled.
2019-10-11T00:43:56.908652Z 0 [Note] InnoDB: Buffer pool(s) load completed at 191010 19:43:56
2019-10-11T00:43:57.001369Z 0 [Note] Found ca.pem, server-cert.pem and server-key.pem in data directory. Trying to enable SSL support using them.
2019-10-11T00:43:57.001662Z 0 [Warning] CA certificate ca.pem is self signed.
2019-10-11T00:43:57.003588Z 0 [Note] Server hostname (bind-address): '*'; port: 3306
2019-10-11T00:43:57.003795Z 0 [Note] IPv6 is available.
2019-10-11T00:43:57.003810Z 0 [Note]   - '::' resolves to '::';
2019-10-11T00:43:57.003843Z 0 [Note] Server socket created on IP: '::'.
2019-10-11T00:43:57.084591Z 0 [Note] Event Scheduler: Loaded 0 events
2019-10-11T00:43:57.084819Z 0 [Note] /usr/sbin/mysqld: ready for connections.
Version: '5.7.27'  socket: '/var/lib/mysql/mysql.sock'  port: 3306  MySQL Community Server (GPL)
----------------------------------------------------------------------------------------------------------------

```

- MYSQL SERVER SETUP

```
# setup root password using temp pass from log file

  # [/var/log/mysqld.log] --> look for line: `[Note] A temporary password is generated for root@localhost: #####`

----------------------------------------------------------------------------------------------------------------
[estrella@data1 ~]$ mysql_secure_installation

Securing the MySQL server deployment.

Enter password for user root: 

The existing password for the user account root has expired. Please set a new password.

New password: 

Re-enter new password: 
 ... Failed! Error: Your password does not satisfy the current policy requirements

New password: 

Re-enter new password: 
The 'validate_password' plugin is installed on the server.
The subsequent steps will run with the existing configuration
of the plugin.
Using existing password for root.

Estimated strength of the password: 100 
Change the password for root ? ((Press y|Y for Yes, any other key for No) : y

New password: 

Re-enter new password: 

Estimated strength of the password: 100 
Do you wish to continue with the password provided?(Press y|Y for Yes, any other key for No) : Y
By default, a MySQL installation has an anonymous user,
allowing anyone to log into MySQL without having to have
a user account created for them. This is intended only for
testing, and to make the installation go a bit smoother.
You should remove them before moving into a production
environment.

Remove anonymous users? (Press y|Y for Yes, any other key for No) : Y
Success.


Normally, root should only be allowed to connect from
'localhost'. This ensures that someone cannot guess at
the root password from the network.

Disallow root login remotely? (Press y|Y for Yes, any other key for No) : 

 ... skipping.
By default, MySQL comes with a database named 'test' that
anyone can access. This is also intended only for testing,
and should be removed before moving into a production
environment.


Remove test database and access to it? (Press y|Y for Yes, any other key for No) : 

 ... skipping.
Reloading the privilege tables will ensure that all changes
made so far will take effect immediately.

Reload privilege tables now? (Press y|Y for Yes, any other key for No) : Y
Success.

All done!
----------------------------------------------------------------------------------------------------------------

# setup blank database

mysql -u root -p

mysql> CREATE DATABASE cloud_import;
Query OK, 1 row affected (0.01 sec)

mysql> CREATE USER 'cloud_overseer'@'localhost' IDENTIFIED BY '~13579ZxP~';
Query OK, 0 rows affected (0.00 sec)

mysql> CREATE USER 'cloud_overseer'@'10.100.180.%' IDENTIFIED BY '~13579ZxP~';
Query OK, 0 rows affected (0.00 sec)

mysql> GRANT ALL PRIVILEGES ON cloud_import.* TO 'cloud_overseer'@'10.100.180.%';
Query OK, 0 rows affected (0.00 sec)

mysql> GRANT ALL PRIVILEGES ON cloud_import.* TO 'cloud_overseer'@'localhost';
Query OK, 0 rows affected (0.00 sec)

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| cloud_import       |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
5 rows in set (0.28 sec)

mysql> SELECT User, Host FROM mysql.user;
+----------------+--------------+
| User           | Host         |
+----------------+--------------+
| cloud_overseer | 10.100.180.% |
| cloud_overseer | localhost    |
| mysql.session  | localhost    |
| mysql.sys      | localhost    |
| root           | localhost    |
+----------------+--------------+
5 rows in set (0.00 sec)


### SQL Database Import 

mysql -u cloud_overseer -p

mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| cloud_import       |
+--------------------+
2 rows in set (0.00 sec)

head -n 5 /var/www/html/nextcloud_15/db_20191009033002.sql
-- MySQL dump 10.13  Distrib 5.7.16, for Linux (x86_64)
--
-- Host: localhost    Database: cloud
-- ------------------------------------------------------
-- Server version	5.7.16

[estrella@data1 acloud]$ mysql -u cloud_overseer -p cloud_import < /var/www/html/nextcloud_15/db_20191009033002.sql
Enter password: 

mysql -u cloud_overseer -p

mysql> USE cloud_import

mysql> USE cloud_import
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
mysql> SHOW TABLES;
+-----------------------------+
| Tables_in_cloud_import      |
+-----------------------------+
| oc_accounts                 |
| oc_activity                 |
| oc_activity_mq              |
| oc_addressbookchanges       |
| oc_addressbooks             |
| oc_appconfig                |
| oc_authtoken                |
| oc_bruteforce_attempts      |
| oc_calendar_invitations     |
| oc_calendar_resources       |
| oc_calendar_rooms           |
| oc_calendarchanges          |
| oc_calendarobjects          |
| oc_calendarobjects_props    |
| oc_calendars                |
| oc_calendarsubscriptions    |
| oc_cards                    |
| oc_cards_properties         |
| oc_comments                 |
| oc_comments_read_markers    |
| oc_conversations            |
| oc_credentials              |
| oc_dav_shares               |
| oc_directlink               |
| oc_documents_invite         |
| oc_documents_member         |
| oc_documents_op             |
| oc_documents_revisions      |
| oc_documents_session        |
| oc_external_applicable      |
| oc_external_config          |
| oc_external_mounts          |
| oc_external_options         |
| oc_federated_reshares       |
| oc_file_locks               |
| oc_filecache                |
| oc_files_trash              |
| oc_files_trashsize          |
| oc_flow_checks              |
| oc_flow_operations          |
| oc_group_admin              |
| oc_group_user               |
| oc_groups                   |
| oc_jobs                     |
| oc_ldap_group_mapping       |
| oc_ldap_group_members       |
| oc_ldap_user_mapping        |
| oc_lucene_status            |
| oc_migrations               |
| oc_mimetypes                |
| oc_mounts                   |
| oc_notifications            |
| oc_oauth2_access_tokens     |
| oc_oauth2_clients           |
| oc_preferences              |
| oc_privatedata              |
| oc_properties               |
| oc_schedulingobjects        |
| oc_share                    |
| oc_share_external           |
| oc_storages                 |
| oc_systemtag                |
| oc_systemtag_group          |
| oc_systemtag_object_mapping |
| oc_trusted_servers          |
| oc_twofactor_backupcodes    |
| oc_twofactor_providers      |
| oc_users                    |
| oc_vcategory                |
| oc_vcategory_to_object      |
| oc_whats_new                |
+-----------------------------+
71 rows in set (0.00 sec)

```
@
@
@
@
@

- **Apache Server Re-install** 

```
sudo yum install httpd.x86_64

-----------------------------------------------------------------------------------------------------------------
Installed:
  httpd.x86_64 0:2.4.6-90.el7.centos                                                                                                        

Dependency Installed:
  httpd-tools.x86_64 0:2.4.6-90.el7.centos                                   mailcap.noarch 0:2.1.41-2.el7
-----------------------------------------------------------------------------------------------------------------

# install SSL/TLS module for Apache Server

[estrella@data1 ~]$ sudo yum install mod_ssl

Installed:
  mod_ssl.x86_64 1:2.4.6-90.el7.centos 



### give `apache` ownership of nextcloud app & proper permissions 

sudo chown -R apache:apache nextcloud_15/ cloud_data/

sudo chmod -R 755 nextcloud_15/ cloud_data/


### rsync nextcloud web app to default directory 

sudo mkdir /var/www/html/nextcloud_15 

sudo rsync -a /acloud/nextcloud_15/ /var/www/html/nextcloud_15 

mv /acloud/nextcloud_15/ /acloud/nextcloud_15_bak


### Trivial: Locate HTTPD Main Config File

sudo find / -name 'httpd.conf'
---------------------------------
[sudo] password for estrella: 
/etc/httpd/conf/httpd.conf
/usr/lib/tmpfiles.d/httpd.conf
---------------------------------

```

###  Additional Configs: 

- Bridged Network: 

  - [link 1](https://superuser.com/questions/510662/network-access-to-virtualbox-vm-from-network-pcs) 

  - [link 2](https://superuser.com/questions/1162055/virtualbox-vm-with-bridged-adapter-cant-access-internet-while-host-can)

  - [link 3](https://blogs.oracle.com/scoter/networking-in-virtualbox-v2) 

  - [link 4](https://superuser.com/questions/1155734/virtualbox-and-bridged-networking)

  - [link 5](https://www.linuxnix.com/important-port-numbers-linux-system-administrator/) 

  - [link 6](https://superuser.com/questions/1313692/how-to-setup-a-bridge-connection-for-enp0s3-centos7-on-oracle-virtualbox)

  - [link 7](https://www.simplified.guide/virtualbox/port-forwarding) 

  - [link 8](https://www.howtogeek.com/122641/how-to-forward-ports-to-a-virtual-machine-and-use-it-as-a-server/) 

  - [link 9](https://serverfault.com/questions/225155/virtualbox-how-to-set-up-networking-so-both-host-and-guest-can-access-internet)

- php.ini 

```
 vim /etc/php.ini

  # change `memory_limit` to `1028M`

# had to manually copy `.htaccess` & `.user.ini` from production server

```

- After changing to Bridged Adapter, Deleted database user on 10.100.180.%

```
mysql> SHOW GRANTS FOR cloud_overseer@'10.100.180.%';
+-----------------------------------------------------------------------------+
| Grants for cloud_overseer@10.100.180.%                                      |
+-----------------------------------------------------------------------------+
| GRANT USAGE ON *.* TO 'cloud_overseer'@'10.100.180.%'                       |
| GRANT ALL PRIVILEGES ON `cloud_import`.* TO 'cloud_overseer'@'10.100.180.%' |
+-----------------------------------------------------------------------------+
2 rows in set (0.00 sec)


mysql> REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'cloud_overseer'@'10.100.180.%';
Query OK, 0 rows affected (0.00 sec)

mysql> DROP USER 'cloud_overseer'@'10.100.180.%';
Query OK, 0 rows affected (0.00 sec)

mysql> CREATE USER 'cloud_overseer'@'198.215.56.%' IDENTIFIED BY '~13579ZxP~';
Query OK, 0 rows affected (0.00 sec)

mysql> GRANT ALL PRIVILEGES ON cloud_import.* TO 'cloud_overseer'@'198.215.56.%';
Query OK, 0 rows affected (0.00 sec)

mysql> SELECT User, Host FROM mysql.user;
+----------------+--------------+
| User           | Host         |
+----------------+--------------+
| cloud_overseer | 198.215.56.% |
| cloud_overseer | localhost    |
| mysql.session  | localhost    |
| mysql.sys      | localhost    |
| root           | localhost    |
+----------------+--------------+
5 rows in set (0.00 sec)

# resource link here: https://www.cyberciti.biz/faq/how-to-delete-remove-user-account-in-mysql-mariadb/ 

```



- **config.php**

```
<?php
$CONFIG = array (
  'instanceid' => 'oc5512891707',
  'passwordsalt' => 'a0b08fcadf85b64eb589ba02d9da2b',
  'trusted_domains' =>
  array (
    0 => '198.215.56.120/test_cloud',
    1 => '198.215.56.120',
  ),
  'datadirectory' => '/acloud/cloud_data',
  'dbtype' => 'mysql',
  'version' => '15.0.7.0',
  'dbname' => 'cloud_import',
  'dbhost' => 'localhost',
  'dbtableprefix' => 'oc_',
  'dbuser' => 'cloud_overseer',
  'dbpassword' => '~13579ZxP~',
  'installed' => true,
  'updatechecker' => false,
  'has_internet_connection' => false,
  'log_rotate_size' => 26214400,
  'appstoreenabled' => false,
  'theme' => 'biohpc',
  'forcessl' => true,
  'enable_previews' => true,
  'ldapIgnoreNamingRules' => false,
  'loglevel' => 0,
  'secret' => '2358593fda0dfbd19ec8af844b286e6dd6c419da7f08e6ffec16d8f3ad1c885af61f254976a1bd700c52a61a346c8f13',
  'maintenance' => false,
  'trashbin_retention_obligation' => 'auto',
  'memcache.local' => '\\OC\\Memcache\\APCu',
  'lost_password_link' => 'https://fileshare.biohpc.new/accounts/password-reset/',
  'check_for_working_webdav' => true,
  'check_for_working_htaccess' => true,
  'ldapUserCleanupInterval' => 0,
  'appstore.experimental.enabled' => true,
  'allow_user_to_change_display_name' => false,
  'remember_login_cookie_lifetime' => 172800,
  'session_lifetime' => 86400,
  'session_keepalive' => true,
  'logtimezone' => 'America/Chicago',
  'ldapProviderFactory' => '\\OCA\\User_LDAP\\LDAPProviderFactory',
  'overwrite.cli.url' => 'https://198.215.56.120',
);

```

- **manually copied missing config files**

  - `.htaccess` to `/var/www/html/nextcloud_15/`  & `/var/www/html/nextcloud_15/config` 

  - `.user.ini` to `/var/www/html/nextcloud_15/` 

  - `.ocdata` to `/acloud/cloud_data` 


- **changed httpd configuration**

```
sudo vim /etc/httpd/conf/httpd.conf 

# following line:
----------------------------------------
DocumentRoot "/var/www/html/nextcloud_15" 
----------------------------------------

```

-**created config.d file for nextcloud**

```
sudo vim /etc/httpd/conf.d/nextcloud_15.conf

------------------------------------------------------------
Alias /nextcloud "/var/www/html/nextcloud_15/"

<Directory /var/www/html/nextcloud_15/>
   Options +FollowSymlinks
   AllowOverride All


 <IfModule mod_dav.c>
   Dav off
 </IfModule>


 SetEnv HOME /var/www/html/nextcloud_15
 SetEnv HTTP_HOME /var/www/html/nextcloud_15

</Directory>
------------------------------------------------------------


- **Disabled SElinux**


- **LDAP Client Config: ldap002**

```
yum install openldap-clients

yum install nss-pam-ldapd

sudo authconfig --enableldap --enableldapauth --enableldaptls --ldapserver=198.215.54.45 --ldapbasedn="dc=biohpc,dc=swmed,dc=edu" --update

# from production clients/ldap server: 
scp -v /etc/ssl/certs/ldap002-cert.pem root@198.215.56.120:/etc/ssl/certs/

  # LDAPTLS wouldn't work without the CACERT

sudo vim /etc/openldap/ldap.conf

-------------------------------------------------------------------------------
uid nslcd
gid ldap
uri ldaps://198.215.54.45
base dc=biohpc,dc=swmed,dc=edu
tls_reqcert allow
tls_ciphers HIGH:MEDIUM:+TLSv1:!SSLv2:+SSLv3
tls_cacertfile /etc/ssl/certs/ldap002-cert.pem
# ssl start_tls  ### DO NOT Enable this option!!! Or else the following error will occurm for nslcd service(while direct queries to the LDAP database will still work) 

#####

Oct 24 15:06:57 data1 nslcd[3312]: [5558ec] <group/member="root"> no available LDAP server found: Operations error: Bad file descriptor
Oct 24 15:06:57 data1 nslcd[3312]: [5558ec] <group/member="root"> no available LDAP server found: Server is unavailable: Bad file descriptor
Oct 24 15:07:11 data1 nslcd[3312]: [1b58ba] <group/member="root"> ldap_start_tls_s() failed (uri=ldaps://198.215.54.45): Operations error: TLS already started

####
-------------------------------------------------------------------------------


sudo vim /etc/nslcd.conf

-------------------------------------------------------------------------------
URI ldaps://198.215.54.45
BASE dc=biohpc,dc=swmed,dc=edu
TLS_REQCERT allow
TLS_CACERT /etc/ssl/certs/ldap002-cert.pem
-------------------------------------------------------------------------------


### Verify TLS:

sudo ldapwhoami -H ldaps://198.215.54.45 -x

ldapsearch -x uid=<username> -ZZ   # should return "additional info:TLS already started"


### Verify nslcd 

getent passwd <uid>

getent group <gid>

### Network Troubleshoot 

netstat -tuan | grep -i 636   # if nslcd is running properly, then one should see "ESTABLISHED" 

  # "TIME_WAIT" indicates local endpoint/application has closed connection, & that the connection is temporaily being kept around to receive delayed packets 

  # "CLOSE_WAIT" --> remote endpoint closed connection

  # [documentation here](https://superuser.com/questions/173535/what-are-close-wait-and-time-wait-states/173543)


telnet <IP> <port>     # client should be able to establish connections with the server (if not, the server's iptables may be up to something)


sudo semanage port -l | grep ldap_port     # use SELinux to determine what ports have been designated to ldap packets
[sudo] password for estrella: 
ldap_port_t                    tcp      389, 636, 3268, 3269, 7389
ldap_port_t                    udp      389, 636

[detailed documentations here](https://access.redhat.com/solutions/46846)
 
```


### Resources

[lftp 1](https://whatbox.ca/wiki/lftp) 

[find out how many threads system CPU has](https://unix.stackexchange.com/questions/218074/how-to-know-number-of-cores-of-a-system-in-linux) 

[system requirements](https://docs.nextcloud.com/server/15/admin_manual/installation/system_requirements.html)

[complete mysql removal_1](https://www.itsmearunchandel.co.in/linux/uninstall-mysql-completely-from-centos-7-server.html) 

[complete mysql removal_2](https://orenjibaka.wordpress.com/2016/02/09/how-to-completely-uninstall-remove-mysql-server-on-centos-linux/) 

[complete mysql removal_2](https://idroot.us/completely-removing-mysql-server-centos/)

[SQL database import 1](https://www.digitalocean.com/community/tutorials/how-to-import-and-export-databases-in-mysql-or-mariadb)

[SQL database import 2](https://www.techrepublic.com/article/how-to-export-and-import-mysql-databases/)

[SQL database import 3](https://stackoverflow.com/questions/4546778/how-can-i-import-a-database-with-mysql-from-terminal)

[SQL database interface commands](http://g2pc1.bu.edu/~qzpeng/manual/MySQL%20Commands.htm)

[re-locate-SQL database + rsync](https://www.digitalocean.com/community/tutorials/how-to-move-a-mysql-data-directory-to-a-new-location-on-ubuntu-16-04) 

[quick guide to finding config files](https://www.cyberciti.biz/tips/where-does-apache-server-store-its-configuration-files.html)

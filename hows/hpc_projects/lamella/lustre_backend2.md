## Setting Up Lustre Parallel FS Backend-Lamella Test Deployment-Part II 

### Creating the Lustre Metadata Service (MDS) 

	The MDS & MDT (metadata target) will be created on the same node as MGS, for MGS setup, refer to `lustre_backend1.md`

[main reference 1](http://wiki.lustre.org/Creating_the_Lustre_Management_Service_(MGS))

[main reference 2](http://kjtanaka.github.io/lustre/2018/02/27/lustre-on-centos72.html)

[oracle zfs examples; check cache devices](https://docs.oracle.com/cd/E53394_01/html/E54801/gayrd.html#SVZFSgfxrx)---> this link also talks about how to adevices to storage pools 

[zfs man page; check cachefile=](http://zfs.datto.com/man/man8/zpool.8.html)
``` 
# Tried to add more virtual harddrives to the virtual machine, but on VirtualBox Version 5.1.22, only 4 open ports are allowed per virtual controller.. 


# Making a separate pool for MDS/MDT, which will use the SAME cache file as `mgspool` 


- this is allowed b.c. according to the zfs manpage: 

" cachefile=<filename> | path

    Controls the location of where the pool configuration is cached. Discovering all pools on system startup requires a cached copy of the configuration data that is stored on the root file system. All pools in this cache are automatically imported when the system boots. Some environments, such as install and clustering, need to cache this information in a different location so that pools are not automatically imported. Setting this property caches the pool configuration in a different location that can later be imported with "zpool import -c". Setting it to the special value "none" creates a temporary pool that is never cached, and the special value '' (empty string) uses the default location.

    Multiple pools can share the same cache file. Because the kernel destroys and recreates this file when pools are added and removed, care should be taken when attempting to access this file. When the last pool using a cachefile is exported or destroyed, the file is removed. "


# Also learned that without the Cachefile, the pools will not be discovered upon system boot! Hence why the pools were getting lost previously; this also indicates that the pool itself DOES NOT NEED TO BE MOUNTED! Good news! 


# list block devices 

[estrella@data1 ~]$ lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0   12G  0 disk 
├─sda1            8:1    0    1G  0 part /boot
└─sda2            8:2    0   11G  0 part 
  ├─centos-root 253:0    0  9.8G  0 lvm  /
  └─centos-swap 253:1    0  1.2G  0 lvm  [SWAP]
sr0              11:0    1 1024M  0 rom  
nvme0n1         259:0    0    4G  0 disk 
├─nvme0n1p1     259:1    0    4G  0 part 
└─nvme0n1p9     259:2    0    8M  0 part 
nvme0n2         259:3    0    4G  0 disk 
├─nvme0n2p1     259:4    0    4G  0 part 
└─nvme0n2p9     259:5    0    8M  0 part 
nvme0n3         259:6    0    4G  0 disk 
nvme0n4         259:7    0    4G  0 disk 

# create new `mdspool` 

[estrella@data1 ~]$ sudo zpool create -O canmount=off -o multihost=on -o cachefile=/etc/zfs/zpool.cache mdspool mirror nvme0n3 nvme0n4 

# verify:

[estrella@data1 ~]$ lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0   12G  0 disk 
├─sda1            8:1    0    1G  0 part /boot
└─sda2            8:2    0   11G  0 part 
  ├─centos-root 253:0    0  9.8G  0 lvm  /
  └─centos-swap 253:1    0  1.2G  0 lvm  [SWAP]
sr0              11:0    1 1024M  0 rom  
nvme0n1         259:0    0    4G  0 disk 
├─nvme0n1p1     259:1    0    4G  0 part 
└─nvme0n1p9     259:2    0    8M  0 part 
nvme0n2         259:3    0    4G  0 disk 
├─nvme0n2p1     259:4    0    4G  0 part 
└─nvme0n2p9     259:5    0    8M  0 part 
nvme0n3         259:6    0    4G  0 disk 
├─nvme0n3p1     259:8    0    4G  0 part 
└─nvme0n3p9     259:9    0    8M  0 part 
nvme0n4         259:7    0    4G  0 disk 
├─nvme0n4p1     259:10   0    4G  0 part 
└─nvme0n4p9     259:11   0    8M  0 part 
[estrella@data1 ~]$ 
[estrella@data1 ~]$ zpool list
NAME      SIZE  ALLOC   FREE  EXPANDSZ   FRAG    CAP  DEDUP  HEALTH  ALTROOT
mdspool  3.97G   318K  3.97G         -     0%     0%  1.00x  ONLINE  -
mgspool  3.97G  2.82M  3.97G         -     0%     0%  1.00x  ONLINE  -


trella@data1 ~]$ zpool status
  pool: mdspool
 state: ONLINE
  scan: none requested
config:

	NAME         STATE     READ WRITE CKSUM
	mdspool      ONLINE       0     0     0
	  mirror-0   ONLINE       0     0     0
	    nvme0n3  ONLINE       0     0     0
	    nvme0n4  ONLINE       0     0     0

errors: No known data errors

  pool: mgspool
 state: ONLINE
  scan: none requested
config:

	NAME         STATE     READ WRITE CKSUM
	mgspool      ONLINE       0     0     0
	  mirror-0   ONLINE       0     0     0
	    nvme0n1  ONLINE       0     0     0
	    nvme0n2  ONLINE       0     0     0

errors: No known data errors


[estrella@data1 ~]$ zfs list
NAME          USED  AVAIL  REFER  MOUNTPOINT
mdspool       177K  3.84G    24K  /mdspool
mdspool/mdt    24K  3.84G    24K  /mdspool/mdt
mgspool      2.81M  3.84G  25.5K  /mgspool
mgspool/mgt  2.67M  3.84G  2.67M  /mgspool/mgt


### MAKE MDS LUSTRE FS ON `MDSPOOL` ### 

[estrella@data1 ~]$ sudo mkfs.lustre --mdt --index 0 --mgsnode 10.100.180.2@tcp0 --backfstype=zfs mdspool/mdt
[sudo] password for estrella: 

mkfs.lustre FATAL: Must specify --fsname for MDT/OST device
mkfs.lustre: exiting with 22 (Invalid argument)


[estrella@data1 ~]$ sudo mkfs.lustre --mdt --index=0 --fsname=zengs_lustre --mgsnode 10.100.180.2@tcp0 --backfstype=zfs mdspool/mdt
mkfs.lustre: filesystem name must be 1-8 chars
mkfs.lustre: exiting with 1 (Operation not permitted)

# Success: See below

[estrella@data1 ~]$ sudo mkfs.lustre --mdt --index=0 --fsname=silustre --mgsnode 10.100.180.2@tcp0 --backfstype=zfs mdspool/mdt

   Permanent disk data:
Target:     silustre:MDT0000
Index:      0
Lustre FS:  silustre
Mount type: zfs
Flags:      0x61
              (MDT first_time update )
Persistent mount opts: 
Parameters: mgsnode=10.100.180.2@tcp
checking for existing Lustre data: not found
mkfs_cmd = zfs create -o canmount=off  mdspool/mdt
  xattr=sa
  dnodesize=auto
Writing mdspool/mdt properties
  lustre:mgsnode=10.100.180.2@tcp
  lustre:version=1
  lustre:flags=97
  lustre:index=0
  lustre:fsname=silustre
  lustre:svname=silustre:MDT0000



# Verify:

[estrella@data1 ~]$ zfs get all -s local
NAME         PROPERTY              VALUE                  SOURCE
mdspool      canmount              off                    local
mdspool/mdt  canmount              off                    local
mdspool/mdt  xattr                 sa                     local
mdspool/mdt  dnodesize             auto                   local
mdspool/mdt  lustre:svname         silustre:MDT0000       local
mdspool/mdt  lustre:flags          97                     local
mdspool/mdt  lustre:mgsnode        10.100.180.2@tcp       local
mdspool/mdt  lustre:index          0                      local
mdspool/mdt  lustre:fsname         silustre               local
mdspool/mdt  lustre:version        1                      local
mgspool      mountpoint            /mgspool               local
mgspool      canmount              on                     local
mgspool/mgt  mountpoint            /mgspool/mgt           local
mgspool/mgt  canmount              off                    local
mgspool/mgt  xattr                 sa                     local
mgspool/mgt  dnodesize             auto                   local
mgspool/mgt  lustre:svname         MGS                    local
mgspool/mgt  lustre:flags          36                     local
mgspool/mgt  lustre:index          65535                  local
mgspool/mgt  lustre:version        1                      local
[estrella@data1 ~]$ 
[estrella@data1 ~]$ 
[estrella@data1 ~]$ zpool list
NAME      SIZE  ALLOC   FREE  EXPANDSZ   FRAG    CAP  DEDUP  HEALTH  ALTROOT
mdspool  3.97G   770K  3.97G         -     0%     0%  1.00x  ONLINE  -
mgspool  3.97G  2.82M  3.97G         -     0%     0%  1.00x  ONLINE  -

# Starting the MDS Device via `mount.lustre` (or simply `mount -t lustre`)

[estrella@data1 ~]$ sudo lctl dl
[sudo] password for estrella:  
  0 UP osd-zfs MGS-osd MGS-osd_UUID 4    # New MDS Device Not Yet Available
  1 UP mgs MGS MGS 4
  2 UP mgc MGC10.100.180.2@tcp 4953df8a-879e-4297-f6d9-598404f9f94a 4


# Sudo Make Mount Point 

sudo mkdir -p /lustre/mdt 

[estrella@data1 ~]$ ls /lustre
mdt  mgt

# mount.lustre 

[estrella@data1 ~]$ sudo mount.lustre mdspool/mdt /lustre/mdt


[estrella@data1 ~]$ df -Th                                        #VERIFY MOUNT!
Filesystem              Type      Size  Used Avail Use% Mounted on
/dev/mapper/centos-root xfs       9.8G  2.4G  7.4G  25% /
devtmpfs                devtmpfs  1.9G     0  1.9G   0% /dev
tmpfs                   tmpfs     1.9G     0  1.9G   0% /dev/shm
tmpfs                   tmpfs     1.9G  8.6M  1.9G   1% /run
tmpfs                   tmpfs     1.9G     0  1.9G   0% /sys/fs/cgroup
/dev/sda1               xfs      1014M  232M  783M  23% /boot
mgspool                 zfs       3.9G     0  3.9G   0% /mgspool
mgspool/mgt             lustre    3.9G  2.7M  3.9G   1% /lustre/mgt
tmpfs                   tmpfs     379M     0  379M   0% /run/user/1000
mdspool/mdt             lustre    3.9G  3.0M  3.9G   1% /lustre/mdt

trella@data1 ~]$ sudo lctl dl
  0 UP osd-zfs MGS-osd MGS-osd_UUID 4
  1 UP mgs MGS MGS 4
  2 UP mgc MGC10.100.180.2@tcp 4953df8a-879e-4297-f6d9-598404f9f94a 4
  3 UP osd-zfs silustre-MDT0000-osd silustre-MDT0000-osd_UUID 6
  4 UP mds MDS MDS_uuid 2
  5 UP lod silustre-MDT0000-mdtlov silustre-MDT0000-mdtlov_UUID 3
  6 UP mdt silustre-MDT0000 silustre-MDT0000_UUID 4
  7 UP mdd silustre-MDD0000 silustre-MDD0000_UUID 3
  8 UP qmt silustre-QMT0000 silustre-QMT0000_UUID 3
  9 UP lwp silustre-MDT0000-lwp-MDT0000 silustre-MDT0000-lwp-MDT0000_UUID 4

# remember, even though the lustre metadata base is mounted on local file system, it is not equivalent to a zfs mount: see below

[estrella@data1 ~]$ sudo zfs get mounted mdspool/mdt
NAME         PROPERTY  VALUE    SOURCE
mdspool/mdt  mounted   no       -
[estrella@data1 ~]$ 
[estrella@data1 ~]$ sudo zfs get mounted mdspool
NAME     PROPERTY  VALUE    SOURCE
mdspool  mounted   no       -

####### Testing `mdspool` Integrity Post Reboot ########

### GOOD ### 

[estrella@data1 ~]$ zpool list
NAME      SIZE  ALLOC   FREE  EXPANDSZ   FRAG    CAP  DEDUP  HEALTH  ALTROOT
mdspool  3.97G  3.14M  3.97G         -     0%     0%  1.00x  ONLINE  -
mgspool  3.97G  2.86M  3.97G         -     0%     0%  1.00x  ONLINE  -
[estrella@data1 ~]$ 
[estrella@data1 ~]$ 
[estrella@data1 ~]$ zfs list
NAME          USED  AVAIL  REFER  MOUNTPOINT
mdspool      3.11M  3.84G    24K  /mdspool
mdspool/mdt  3.00M  3.84G  3.00M  /mdspool/mdt
mgspool      2.83M  3.84G  25.5K  /mgspool
mgspool/mgt  2.69M  3.84G  2.69M  /mgspool/mgt

[estrella@data1 ~]$ zfs get all -s local
NAME         PROPERTY              VALUE                  SOURCE
mdspool      canmount              off                    local
mdspool/mdt  canmount              off                    local
mdspool/mdt  xattr                 sa                     local
mdspool/mdt  dnodesize             auto                   local
mdspool/mdt  lustre:svname         silustre-MDT0000       local
mdspool/mdt  lustre:flags          33                     local
mdspool/mdt  lustre:mgsnode        10.100.180.2@tcp       local
mdspool/mdt  lustre:index          0                      local
mdspool/mdt  lustre:fsname         silustre               local
mdspool/mdt  lustre:version        1                      local
mgspool      mountpoint            /mgspool               local
mgspool      canmount              on                     local
mgspool/mgt  mountpoint            /mgspool/mgt           local
mgspool/mgt  canmount              off                    local
mgspool/mgt  xattr                 sa                     local
mgspool/mgt  dnodesize             auto                   local
mgspool/mgt  lustre:svname         MGS                    local
mgspool/mgt  lustre:flags          36                     local
mgspool/mgt  lustre:index          65535                  local
mgspool/mgt  lustre:version        1                      local

[estrella@data1 ~]$ sudo mount.lustre mgspool/mgt /lustre/mgt
[estrella@data1 ~]$ sudo mount.lustre mdspool/mdt /lustre/mdt
[estrella@data1 ~]$ 
[estrella@data1 ~]$ 
[estrella@data1 ~]$ df -Th
Filesystem              Type      Size  Used Avail Use% Mounted on
/dev/mapper/centos-root xfs       9.8G  2.4G  7.4G  25% /
devtmpfs                devtmpfs  1.9G     0  1.9G   0% /dev
tmpfs                   tmpfs     1.9G     0  1.9G   0% /dev/shm
tmpfs                   tmpfs     1.9G  8.6M  1.9G   1% /run
tmpfs                   tmpfs     1.9G     0  1.9G   0% /sys/fs/cgroup
/dev/sda1               xfs      1014M  232M  783M  23% /boot
mgspool                 zfs       3.9G     0  3.9G   0% /mgspool
tmpfs                   tmpfs     379M     0  379M   0% /run/user/1000
mgspool/mgt             lustre    3.9G  2.7M  3.9G   1% /lustre/mgt
mdspool/mdt             lustre    3.9G  3.0M  3.9G   1% /lustre/mdt
[estrella@data1 ~]$ 
[estrella@data1 ~]$ sudo lctl dl
  0 UP osd-zfs MGS-osd MGS-osd_UUID 4
  1 UP mgs MGS MGS 4
  2 UP mgc MGC10.100.180.2@tcp 98358a4b-0b48-c760-6345-3fb9d77e5da4 4
  3 UP osd-zfs silustre-MDT0000-osd silustre-MDT0000-osd_UUID 6
  4 UP mds MDS MDS_uuid 2
  5 UP lod silustre-MDT0000-mdtlov silustre-MDT0000-mdtlov_UUID 3
  6 UP mdt silustre-MDT0000 silustre-MDT0000_UUID 2
  7 UP mdd silustre-MDD0000 silustre-MDD0000_UUID 3
  8 UP qmt silustre-QMT0000 silustre-QMT0000_UUID 3
  9 UP lwp silustre-MDT0000-lwp-MDT0000 silustre-MDT0000-lwp-MDT0000_UUID 4


### GOOD ###

-------------------------------------------------------------------------------

### 2nd Iteration of MDS Setup 

- differences from previous setup:

  - ntpd server setup, udp port 123 opened

     - MGS@tcp0:10.100.180.2 is added to the ntp.conf file 

  - vbox guest additions installed as a kernel module


```

# generate unique hostid:

hostid 

sudo genhostid   # generate new hostID 

od -An -tx /etc/hostid   # verify the new hostID 

hostID   # verify: output should be different from the very beginning

# Add MGS to the NTP config file

sudo vi /etc/ntp.conf

server 0.centos.pool.ntp.org iburst
server 1.centos.pool.ntp.org iburst
server 2.centos.pool.ntp.org iburst
server 3.centos.pool.ntp.org iburst
server 10.100.180.2    # added entry listing MGS node

# restart NTP service & verify connection to MGS's NTP server

   # note: make sure the MGS node is up & running prior to this step!!! 

[estrella@mds1 ~]$ sudo systemctl restart ntpd
[estrella@mds1 ~]$ sudo systemctl status ntpd
● ntpd.service - Network Time Service
   Loaded: loaded (/usr/lib/systemd/system/ntpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2019-06-26 18:16:21 CDT; 18s ago
  Process: 6512 ExecStart=/usr/sbin/ntpd -u ntp:ntp $OPTIONS (code=exited, status=0/SUCCESS)
 Main PID: 6513 (ntpd)
   CGroup: /system.slice/ntpd.service
           └─6513 /usr/sbin/ntpd -u ntp:ntp -g

Jun 26 18:16:21 mds1.biohpc.new ntpd[6513]: Listen normally on 3 enp0s3 10.0.2.15 UDP 123
Jun 26 18:16:21 mds1.biohpc.new ntpd[6513]: Listen normally on 4 eth1 10.100.180.3 UDP 123
Jun 26 18:16:21 mds1.biohpc.new ntpd[6513]: Listen normally on 5 lo ::1 UDP 123
Jun 26 18:16:21 mds1.biohpc.new ntpd[6513]: Listen normally on 6 enp0s3 fe80::fa4c:234c:6373:61cb UDP 123
Jun 26 18:16:21 mds1.biohpc.new ntpd[6513]: Listening on routing socket on fd #23 for interface updates
Jun 26 18:16:21 mds1.biohpc.new ntpd[6513]: 0.0.0.0 c016 06 restart
Jun 26 18:16:21 mds1.biohpc.new ntpd[6513]: 0.0.0.0 c012 02 freq_set kernel 0.000 PPM
Jun 26 18:16:21 mds1.biohpc.new ntpd[6513]: 0.0.0.0 c011 01 freq_not_set
Jun 26 18:16:21 mds1.biohpc.new systemd[1]: Started Network Time Service.
Jun 26 18:16:28 mds1.biohpc.new ntpd[6513]: 0.0.0.0 c614 04 freq_mode


# Check time offset with MGS server

[estrella@mds001 ~]$ sudo ntpq -p
     remote           refid      st t when poll reach   delay   offset  jitter
==============================================================================
+hydrogen.consta 209.51.161.238   2 u   26   64  377   34.973   -2.733  15.881
*time1.plumdev.n .GPS.            1 u   21   64  377   41.918   -8.299  10.740
+helium.constant 128.59.0.245     2 u   29   64  377   35.249   -9.384  10.602
-server.nanoslim 140.142.234.133  3 u   36   64  377   50.309   -3.324  10.263
-mgs001          108.61.73.243    3 u   26   64  377    0.210  -18.042   9.048

# Check firewall-cmd for LNET & NTP ports

[estrella@mds001 ~]$ sudo firewall-cmd --list-services
ssh dhcpv6-client ntp         # good 

[estrella@mds001 ~]$ sudo firewall-cmd --permanent --add-port=988/tcp
success

[estrella@mds001 ~]$ sudo firewall-cmd --reload
success
[estrella@mds001 ~]$ sudo firewall-cmd --list-ports
988/tcp


# ZFS Pool & Lustre Metadata File System Steps Same as Before # 

### Documentation 

[estrella@mds001 ~]$ sudo mkfs.lustre --mdt --index=0 --fsname=silustre --mgsnode 10.100.180.2@tcp0 --backfstype=zfs mds1pool/mdt1
[sudo] password for estrella: 

   Permanent disk data:
Target:     silustre:MDT0000
Index:      0
Lustre FS:  silustre
Mount type: zfs
Flags:      0x61
              (MDT first_time update )
Persistent mount opts: 
Parameters: mgsnode=10.100.180.2@tcp
checking for existing Lustre data: not found
mkfs_cmd = zfs create -o canmount=off  mds1pool/mdt1
  xattr=sa
  dnodesize=auto
Writing mds1pool/mdt1 properties
  lustre:mgsnode=10.100.180.2@tcp
  lustre:version=1
  lustre:flags=97
  lustre:index=0
  lustre:fsname=silustre
  lustre:svname=silustre:MDT0000


[estrella@mds001 ~]$ zfs get all -s local
NAME           PROPERTY              VALUE                  SOURCE
mds1pool       canmount              off                    local
mds1pool/mdt1  canmount              off                    local
mds1pool/mdt1  xattr                 sa                     local
mds1pool/mdt1  dnodesize             auto                   local
mds1pool/mdt1  lustre:version        1                      local
mds1pool/mdt1  lustre:flags          97                     local
mds1pool/mdt1  lustre:fsname         silustre               local
mds1pool/mdt1  lustre:index          0                      local
mds1pool/mdt1  lustre:svname         silustre:MDT0000       local
mds1pool/mdt1  lustre:mgsnode        10.100.180.2@tcp       local


trella@mds001 ~]$ sudo mount.lustre mds1pool/mdt1 /lustre/mdt1/
[estrella@mds001 ~]$ 
[estrella@mds001 ~]$ 
[estrella@mds001 ~]$ sudo lctl dl
  0 UP osd-zfs silustre-MDT0000-osd silustre-MDT0000-osd_UUID 6
  1 UP mgc MGC10.100.180.2@tcp 8a8f5e6c-8612-43d0-249d-99e9a0a2ef87 4
  2 UP mds MDS MDS_uuid 2
  3 UP lod silustre-MDT0000-mdtlov silustre-MDT0000-mdtlov_UUID 3
  4 UP mdt silustre-MDT0000 silustre-MDT0000_UUID 2
  5 UP mdd silustre-MDD0000 silustre-MDD0000_UUID 3
  6 UP qmt silustre-QMT0000 silustre-QMT0000_UUID 3
  7 UP lwp silustre-MDT0000-lwp-MDT0000 silustre-MDT0000-lwp-MDT0000_UUID 4






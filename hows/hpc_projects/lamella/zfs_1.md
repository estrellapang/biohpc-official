## Managing the ZFS filesystem! 


### Diagnosis 

`sudo systemctl status zfs-import-cache` 

  - if `/etc/zfs/zpool.cache` is not present, then this service will fail

`sudo systemctl status zfs-mount.service`

`sudo systemctl status zfs.target`

- locations: 

  - `/etc/zfs/`  



### Creating a RAID10 volume/"Pool" 

> in ZFS lingo, a RAID10 is a striped-mirrored Vdev (Virtuak Device/Logical Volume) 

``` 

# list connected block devices: a minimum of 4 disks needed

[estrella@data1 ~]$ lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0   12G  0 disk 
├─sda1            8:1    0    1G  0 part /boot
└─sda2            8:2    0   11G  0 part 
  ├─centos-root 253:0    0  9.8G  0 lvm  /
  └─centos-swap 253:1    0  1.2G  0 lvm  [SWAP]
sr0              11:0    1 1024M  0 rom  
nvme0n1         259:0    0    4G  0 disk 
nvme0n2         259:1    0    4G  0 disk 
nvme0n3         259:2    0    4G  0 disk 
nvme0n4         259:3    0    4G  0 disk 


# list existing pools 

[estrella@data1 ~]$ zpool list
no pools available



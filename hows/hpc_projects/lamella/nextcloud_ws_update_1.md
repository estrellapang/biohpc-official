## Nextcloud MOCK Update from WS: Part I, Migrating to Host of Later CentOS Release

### Creating New VM Template

- **Relevant Info**

  - VBox Name: `cloud17`  

  - I.P: `56.124` 

  - MySQL root pass: estrella + tilda
  
  - NC Admins: admin/biohpcadmin + estrella pass on server 

  - kernel + release: 

    - `3.10.0-957.el7.x86_64` 

    - `CentOS Linux release 7.6.1810 (Core)`


 
- **BASICS**

```
hostnamectl
-------------------------------------------------------------------------------
vi /etc/profile.d/proxy.sh

vi /etc/yum.conf

ip link

cd /etc/sysconfig/network-scripts/

yum search epel

yum install net-tools

yum install vim
-------------------------------------------------------------------------------
```

\
\
\

### MySQL Base Setup

```
-------------------------------------------------------------------------------
# download .rpm file from mysql site

yum localinstall mysql80-community-release-el7-3.noarch.rpm

# disable/enable appropriate SQL version from `mysql-community.repo`

# install mysql server

yum install mysql-community-server.x86_64

  # check version

mysqld --version

# find default database root pass
------------------------------------------
systemctl start mysqld

vim /var/log/mysqld   --> search term: " temporary password " 

/*
2020-01-05T23:35:54.066434Z 1 [Note] A temporary password is generated for root@localhost: .4teQJF:37t9
*/
------------------------------------------

# mysql_secure_installation
------------------------------------------
mysql_secure_installation

/*

New password: 

Re-enter new password: 
 ... Failed! Error: Your password does not satisfy the current policy requirements

New password: 

Re-enter new password: 
The 'validate_password' plugin is installed on the server.
The subsequent steps will run with the existing configuration
of the plugin.
Using existing password for root.

Estimated strength of the password: 100 
Change the password for root ? ((Press y|Y for Yes, any other key for No) : Y

	### NOTE!!! Here we could have JUST PRESSED `ENTER`!! ###

New password: 

Re-enter new password: 

Estimated strength of the password: 100 
Do you wish to continue with the password provided?(Press y|Y for Yes, any other key for No) : y

*/


`y` / `Y` to:

1. remove anonymous user 

2. disable remote root login

3. remove 'test' database

4. reload privilege table
------------------------------------------


# create new database:
------------------------------------------
mysql -u root -p

CREATE DATABASE biohpc_cloud;

CREATE USER 'cloud_overseer'@'198.215.56.%' IDENTIFIED by '~13579ZxP~';

CREATE USER 'cloud_overseer'@'localhost' IDENTIFIED by '~13579ZxP~';

GRANT ALL PRIVILEGES ON biohpc_cloud.* TO 'cloud_overseer'@'198.215.56.%';

GRANT ALL PRIVILEGES ON biohpc_cloud.* TO 'cloud_overseer'@'localhost';

SELECT User, Host FROM mysql.user;   # show users

show databases; 

`CTRL + D` --> log out


### Confirm

mysql -u cloud_overseer -p

mysql> show databases

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| biohpc_cloud       |
+--------------------+
2 rows in set (0.00 sec)
------------------------------------------

### SQL DUMP from Original Server
------------------------------------------
sudo mysqldump --single-transaction --quick --lock-tables=false -u cloud_overseer -p cloud_import > /acloud/sql_dumps/inject-$(date +%F).sql

[estrella@data1 acloud]$ ls -lah
total 52K
drwxr-xr-x    6 apache apache   107 Jan  7 02:44 .
dr-xr-xr-x.  18 root   root     238 Sep 17 00:30 ..
drwxrwx---. 679 apache apache   36K Nov 26 12:11 cloud_data
drwxrwx---    5 apache apache   144 Dec 27 19:05 cloud_data_new
drwxr-xr-x.   4 apache apache   120 Oct  9 17:14 nextcloud_15_bak
drwxrwxr-x    2 root   estrella  43 Jan  7 02:53 sql_dumps
------------------------------------------

### Import SQL dump to new server + SQL Inject
------------------------------------------
sudo rsync -a root@198.215.56.120:/acloud/sql_dumps/inject-2020-01-07.sql /tmp/

# turn on mysqld 

# SQL Inject: only works if existing database is an empty template

mysql -u cloud_overseer -p biohpc_cloud < /tmp/inject-2020-01-07.sql

mysql -u cloud_overseer -p

USE biohpc_cloud;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed

show databases;

SHOW TABLES;   # confirm whehther this will work
------------------------------------------
-------------------------------------------------------------------------------
```

\
\
\

### RSYNCing from Original Server

```
# count number of files & directories on original server

[estrella@data1 nextcloud_15]$ sudo find . -type f | wc -l
13479

[estrella@data1 nextcloud_15]$ sudo find . -type d | wc -l
2277

# INSTALL Apache HTTP Server 

  ## without it there is no `/var/www/html` directory

sudo yum install httpd

yum info httpd 

# rsync NC Server

sudo rsync -a root@198.215.56.120:/var/www/html/nextcloud_15 /var/www/html 

  ## after files & directory count, numbers seem to match

  ## check permissions against that of original server

# change to correct permissions

  ## both the owner and group are to be `apache`

# rsync NC Data Directory

 ## make a data directory 

sudo mkdir /acloud

sudo chown apache:apache /acloud

sudo chmod 770 /acloud/


 ## actual rsync (in production: you would want to put server into maintenance mode)

sudo rsync -a root@198.215.56.120:/acloud/cloud_data_new/ /acloud/

 ## also to a count via `find` before and after

```


\
\
\

### PHP Install: 7.1

```
# WebTactic Repository

sudo yum install https://mirror.webtatic.com/yum/el7/webtatic-release.rpm

# All related packages 

sudo yum install mod_php71w php71w-common php71w-opcache php71w-dom php71w-mbstring php71w-gd php71w-json php71w-xml php71w-zip php71w-curl php71w-pear php71w-intl php71w-mysql php71w-ldap php71w-pecl-imagick php71w-pecl-apcu  php71w-posix setroubleshoot-server php71w php71w-pdo bzip2

# Installed Packages

-----------------------------------------------
  bzip2.x86_64 0:1.0.6-13.el7                        mod_php71w.x86_64 0:7.1.33-1.w7                       
  php71w-common.x86_64 0:7.1.33-1.w7                 php71w-gd.x86_64 0:7.1.33-1.w7                        
  php71w-intl.x86_64 0:7.1.33-1.w7                   php71w-ldap.x86_64 0:7.1.33-1.w7                      
  php71w-mbstring.x86_64 0:7.1.33-1.w7               php71w-mysql.x86_64 0:7.1.33-1.w7                     
  php71w-opcache.x86_64 0:7.1.33-1.w7                php71w-pdo.x86_64 0:7.1.33-1.w7                       
  php71w-pear.noarch 1:1.10.4-1.w7                   php71w-pecl-apcu.x86_64 0:5.1.9-1.w7                  
  php71w-pecl-imagick.x86_64 0:3.4.3-1.w7            php71w-process.x86_64 0:7.1.33-1.w7                   
  php71w-xml.x86_64 0:7.1.33-1.w7                    setroubleshoot-server.x86_64 0:3.2.30-7.el7
-----------------------------------------------

sudo yum install mod_ssl

```

\
\
\

### NC App + HTTPD Config

```
#config.php
-----------------------------------------
<?php
$CONFIG = array (
  'instanceid' => 'oc5512891707',
  'passwordsalt' => 'a0b08fcadf85b64eb589ba02d9da2b',
  'trusted_domains' =>
  array (
    0 => '198.215.56.124/test_cloud',
    1 => '198.215.56.124',
  ),
  'datadirectory' => '/acloud',
  'dbtype' => 'mysql',
  'version' => '15.0.7.0',
  'dbname' => 'biohpc_cloud',
  'dbhost' => 'localhost',
  'dbtableprefix' => 'oc_',
  'dbuser' => 'cloud_overseer',
  'dbpassword' => '~13579ZxP~',
  'installed' => true,
  'updatechecker' => false,
  'has_internet_connection' => false,
  'log_rotate_size' => 26214400,
  'appstoreenabled' => false,
  'theme' => 'biohpc',
  'forcessl' => true,
  'enable_previews' => true,
  'ldapIgnoreNamingRules' => false,
  'loglevel' => 0,
  'secret' => '2358593fda0dfbd19ec8af844b286e6dd6c419da7f08e6ffec16d8f3ad1c885af61f254976a1bd700c52a61a346c8f13',
  'maintenance' => false,
  'trashbin_retention_obligation' => 'auto',
  'memcache.local' => '\\OC\\Memcache\\APCu',
  'lost_password_link' => 'https://fileshare.biohpc.new/accounts/password-reset/',
  'check_for_working_webdav' => true,
  'check_for_working_htaccess' => true,
  'ldapUserCleanupInterval' => 0,
  'appstore.experimental.enabled' => true,
  'allow_user_to_change_display_name' => false,
  'remember_login_cookie_lifetime' => 172800,
  'session_lifetime' => 86400,
  'session_keepalive' => true,
  'logtimezone' => 'America/Chicago',
  'ldapProviderFactory' => '\\OCA\\User_LDAP\\LDAPProviderFactory',
  'overwrite.cli.url' => 'https://198.215.56.124',
);
-----------------------------------------

sudo vim /etc/php.ini

  # change `memory_limit` to `1028M`

sudo vim /etc/httpd/conf/httpd.conf

# following line:
----------------------------------------
DocumentRoot "/var/www/html/nextcloud_15" 
----------------------------------------

```

\
\
\

### Server Side Touch-ups

```
# disable SELINUX

sudo firewall-cmd --permanent --add-service=ldap
sudo firewall-cmd --permanent --add-service=http
sudo firewall-cmd --permanent --add-service=https
sudo firewall-cmd --reload

sudo systemctl stop mysqld

# REBOOT for disable SELINUX to take effect

chown root:apache /var/lib/php/session
chown root:apache /var/lib/php/wsdlcache

```


\
\
\

### NC Admin Configs

```
### Issue: Database Users CANNOT log in until password reset!!!

  ## guess: secret hash?

cd /var/www/html/nextcloud_1/

  ## check if NC can query database:

sudo -u apache php occ user:info admin
  - user_id: admin
  - display_name: admin
  - email: 
  - cloud_id: admin@198.215.56.124
  - enabled: true
  - groups:
    - admin
  - quota: 50 GB
  - last_seen: 2019-12-28T01:39:35+00:00
  - user_directory: /acloud/admin
  - backend: Database

sudo -u apache php occ user:info biohpcadmin
  - user_id: biohpcadmin
  - display_name: biohpcadmin
  - email: 
  - cloud_id: biohpcadmin@198.215.56.124
  - enabled: true
  - groups:
    - admin
  - quota: 50 GB
  - last_seen: 2019-11-20T00:22:14+00:00
  - user_directory: /acloud/biohpcadmin
  - backend: Database

  ## reset password

sudo -u apache php occ user:resetpassword admin
Enter a new password: 
Confirm the new password: 
Successfully reset password for admin

### Opened Port 389 on LDAP002 to New NC server

### In Admin LDAP Integrations

 ## currently 368 groups

### verify LDAP is working

sudo -u apache php occ ldap:search zengxing
Zengxing Pang (e57019bc-0d42-460c-a6ec-9b231a6dcb14)

### Logged in, and checked photo previously uploaded

```

\
\
\

### Take SNAPSHOT Prior to NC Update

```
VBoxManage list runningvms
"test_cloud" {eb4c595c-4dd6-4740-a2f4-68bed1990a67}
"cloud17" {435a999b-b008-4f8e-85ed-20a2421ab158}

 VBoxManage snapshot cloud17 take "pre_update" --description "fully replicated php 7_1"
0%...10%...20%...30%...40%...50%...60%...70%...80%...90%...100%
Snapshot taken. UUID: 2b02e9a1-4c26-4cfd-99be-9af12aea1cb9
```


### Mods on Initial NC Server

- **Moving to Empty Data Directory**

```
-------------------------------------------------------------------------------

### OLD

/acloud/cloud_data

### NEW (empty)

/acloud/cloud_data_new

### IMPORTANT!!! ####

COPY `.ocdata` from original data directory to new location!!!

  ## or else error would prevent web app from being accessed


/
/
/

### THEME LOSS:

- Logo Location on Server:

`/var/www/html/nextcloud_15/themes/biohpc/core/img/`

- Location of Background Image

`/home2/s167891/cloud_brand/` -OR- `/home2/s167891/lamella_brand/`

  - they are also backed up on /work

-------------------------------------------------------------------------------

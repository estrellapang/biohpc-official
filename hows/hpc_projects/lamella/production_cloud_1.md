## 

### Base System Setup

- Passes:

  - mysql root: DMZ pass

  - root: new root   

  - `.my.cnf` --> biohpcadmin's pass for `cloud_overseer`


```
sudo systemctl status disable firewalld

sudo vi /etc/selinux/config
----------------
SELINUX=permissive
----------------

# Install Basic Utilities

sudo yum install vim net-tools nc telnet wget

# Apache Installation

sudo yum install httpd (V2.4.6)

sudo yum install httpd24-mod_ssl.x86_64

sudo yum install mod_ssl
```

\
\

### PHP Installation

- [NC 15 Admin Guide: RHEL 7 PHP Install Instructions](https://docs.nextcloud.com/server/15/admin_manual/installation/php_72_installation.html)
```
sudo yum install rh-php73 rh-php73-php rh-php73-php-cli rh-php73-php-common rh-php73-php-gd rh-php73-php-intl rh-php73-php-ldap rh-php73-php-mbstring rh-php73-php-mysqlnd rh-php73-php-opcache rh-php73-php-pdo rh-php73-php-pear rh-php73-php-pecl-apcu rh-php73-php-process rh-php73-php-xml

-------------------------------------------------------------------------
Installed:
  rh-php73.x86_64 0:1-1.el7                           rh-php73-php.x86_64 0:7.3.11-1.el7                
  rh-php73-php-cli.x86_64 0:7.3.11-1.el7              rh-php73-php-common.x86_64 0:7.3.11-1.el7         
  rh-php73-php-gd.x86_64 0:7.3.11-1.el7               rh-php73-php-intl.x86_64 0:7.3.11-1.el7           
  rh-php73-php-ldap.x86_64 0:7.3.11-1.el7             rh-php73-php-mbstring.x86_64 0:7.3.11-1.el7       
  rh-php73-php-mysqlnd.x86_64 0:7.3.11-1.el7          rh-php73-php-opcache.x86_64 0:7.3.11-1.el7        
  rh-php73-php-pdo.x86_64 0:7.3.11-1.el7              rh-php73-php-pear.noarch 1:1.10.9-1.el7           
  rh-php73-php-pecl-apcu.x86_64 0:5.1.17-1.el7        rh-php73-php-process.x86_64 0:7.3.11-1.el7        
  rh-php73-php-xml.x86_64 0:7.3.11-1.el7             

Dependency Installed:
  audit-libs-python.x86_64 0:2.8.5-4.el7             checkpolicy.x86_64 0:2.5-8.el7                    
  httpd24-httpd.x86_64 0:2.4.34-15.el7               httpd24-httpd-tools.x86_64 0:2.4.34-15.el7        
  httpd24-libcurl.x86_64 0:7.61.1-2.el7              httpd24-libnghttp2.x86_64 0:1.7.1-8.el7           
  httpd24-runtime.x86_64 0:1.1-19.el7                libX11.x86_64 0:1.6.7-2.el7                       
  libX11-common.noarch 0:1.6.7-2.el7                 libXau.x86_64 0:1.0.8-2.1.el7                     
  libXpm.x86_64 0:3.5.12-1.el7                       libcgroup.x86_64 0:0.41-21.el7                    
  libsemanage-python.x86_64 0:2.5-14.el7             libwebp.x86_64 0:0.3.0-7.el7                      
  libxcb.x86_64 0:1.13-1.el7                         pcre2.x86_64 0:10.23-2.el7                        
  policycoreutils-python.x86_64 0:2.5-33.el7         python-IPy.noarch 0:0.75-6.el7                    
  rh-php73-php-json.x86_64 0:7.3.11-1.el7            rh-php73-php-zip.x86_64 0:7.3.11-1.el7            
  rh-php73-runtime.x86_64 0:1-1.el7                  scl-utils.x86_64 0:20130529-19.el7                
  setools-libs.x86_64 0:3.3.8-4.el7
-------------------------------------------------------------------------


# symlink PHP 7.3 Apache modules in proper directories:

-------------------------------------------------------------------------
sudo ln -s /opt/rh/httpd24/root/etc/httpd/conf.d/rh-php73-php.conf /etc/httpd/conf.d/

  # in this config file, "session", "opcache", and "wsdlcache" locations are listed

  # permissions on them:

-----------------------------
sudo ls -l /var/opt/rh/rh-php73/lib/php/
total 0
drwxrwx---. 2 root apache 6 Oct 31 07:33 opcache
drwxrwx---. 2 root apache 6 Oct 31 07:33 session
drwxrwx---. 2 root apache 6 Oct 31 07:33 wsdlcache
-----------------------------

sudo ln -s /opt/rh/httpd24/root/etc/httpd/conf.modules.d/15-rh-php73-php.conf /etc/httpd/conf.modules.d/

sudo ln -s /opt/rh/httpd24/root/etc/httpd/modules/librh-php73-php7.so /etc/httpd/modules/
-------------------------------------------------------------------------

# Verify Apache does indeed use PHP 7/.3 and loads correct modules
-------------------------------------------------------------------------
sudo vi /var/www/html/phpinfo.php

--------------------
<?php phpinfo(); ?>
--------------------

# get on browser and visit: `http://129.112.9.42/phpinfo.php`

  # confirms PHP 7.3.11 is being utilized by Apache, page also shows useful configs
-------------------------------------------------------------------------



# symlink  PHP 7.3 binary to use to short path /usr/bin/php

sudo php --version
sudo: php: command not found

-------------------------------------------------------------------------
sudo ln -s /opt/rh/rh-php73/root/usr/bin/php /usr/bin/php

php --version
PHP 7.3.11 (cli) (built: Oct 31 2019 08:30:29) ( NTS )
Copyright (c) 1997-2018 The PHP Group
Zend Engine v3.3.11, Copyright (c) 1998-2018 Zend Technologies
    with Zend OPcache v7.3.11, Copyright (c) 1999-2018, by Zend Technologies
-------------------------------------------------------------------------

# location of RHEL PHP Config Files
-------------------------------------------------------------------------
/etc/opt/rh/rh-php73/ 

 # here and in the sub-directories under here, you will find config files such as:

php.ini 10-opcache.ini 40-apcu.ini 
-------------------------------------------------------------------------
```

\
\

### MySQL Server Installation

```
cd /tmp/

# [link for various platform repo downloads](https://dev.mysql.com/downloads/repo/yum/)

wget https://dev.mysql.com/get/mysql80-community-release-el7-3.noarch.rpm

sudo yum localinstall mysql80-community-release-el7-3.noarch.rpm

sudo yum repolist | grep -i mysql
--------------------------------
mysql-connectors-community/x86_64    MySQL Connectors Community                                      141
mysql-tools-community/x86_64         MySQL Tools Community                                           105
mysql80-community/x86_64             MySQL 8.0 Community Server                                      161
--------------------------------

# change repo to MySQL 5.7 

sudo vim /etc/yum.repos.d/mysql-community.repo
--------------------------------
sudo yum repolist | grep -i mysql
mysql-connectors-community/x86_64 MySQL Connectors Community                 141
mysql-tools-community/x86_64      MySQL Tools Community                      105
mysql57-community/x86_64          MySQL 5.7 Community Server                 404
--------------------------------

# install mysql server

sudo yum install mysql-server
--------------------------------
Installed:
  mysql-community-libs.x86_64 0:5.7.29-1.el7       mysql-community-libs-compat.x86_64 0:5.7.29-1.el7    
  mysql-community-server.x86_64 0:5.7.29-1.el7    

Dependency Installed:
  mysql-community-client.x86_64 0:5.7.29-1.el7       mysql-community-common.x86_64 0:5.7.29-1.el7      
Replaced:
  mariadb-libs.x86_64 1:5.5.64-1.el7 
--------------------------------

mysqld --version
mysqld  Ver 5.7.29 for Linux on x86_64 (MySQL Community Server (GPL))

```

\
\


### Security Update

```
# ERROR w/h missiing GPG key
--------------------------------
warning: /var/cache/yum/x86_64/7Server/epel7-x86_64/packages/libmspack-0.7-0.1.alpha.el7.x86_64.rpm: Header V3 RSA/SHA256 Signature, key ID 352c64e5: NOKEY
Retrieving key from file:///etc/pki/rpm-gpg/RPM-GPG-KEY-utsw_satellite-EPEL-7


GPG key retrieval failed: [Errno 14] curl#37 - "Couldn't open file /etc/pki/rpm-gpg/RPM-GPG-KEY-utsw_satellite-EPEL-7"
[biohpcadmin@cloud7 tmp]$ ls -l /etc/pki/rpm-gpg/
total 48
-rw-r--r--. 1 root root 27824 Apr 24  2019 RPM-GPG-KEY-mysql
-rw-r--r--. 1 root root  3375 Jul  3  2019 RPM-GPG-KEY-redhat-beta
-rw-r--r--. 1 root root  1990 Jul  3  2019 RPM-GPG-KEY-redhat-legacy-former
-rw-r--r--. 1 root root  1164 Jul  3  2019 RPM-GPG-KEY-redhat-legacy-release
-rw-r--r--. 1 root root   885 Jul  3  2019 RPM-GPG-KEY-redhat-legacy-rhx
-rw-r--r--. 1 root root  3211 Jul  3  2019 RPM-GPG-KEY-redhat-release
--------------------------------

  # resolve: disable `gpgcheck`

sudo vim /etc/yum/pluginconf.d/rhnplugin.conf
--------------------------------
gpgcheck = 0
--------------------------------


# tmp workaround:
--------------------------------
sudo yum --disablerepo=epel* --security update-minimal

  # the difference is only one package missing: "libmspack" v0.7 (from v0.5)

Installed:
  kernel.x86_64 0:3.10.0-1062.18.1.el7                         kernel-devel.x86_64 0:3.10.0-1062.18.1.el7                        

Updated:
  git.x86_64 0:1.8.3.1-21.el7_7                                 kernel-headers.x86_64 0:3.10.0-1062.18.1.el7                      
  kernel-tools.x86_64 0:3.10.0-1062.18.1.el7                    kernel-tools-libs.x86_64 0:3.10.0-1062.18.1.el7                   
  libicu.x86_64 0:50.2-4.el7_7                                  nss.x86_64 0:3.44.0-7.el7_7                                       
  nss-softokn.x86_64 0:3.44.0-8.el7_7                           nss-softokn-freebl.x86_64 0:3.44.0-8.el7_7                        
  nss-sysinit.x86_64 0:3.44.0-7.el7_7                           nss-tools.x86_64 0:3.44.0-7.el7_7                                 
  nss-util.x86_64 0:3.44.0-4.el7_7                              patch.x86_64 0:2.7.1-12.el7_7                                     
  perl-Git.noarch 0:1.8.3.1-21.el7_7                            python-perf.x86_64 0:3.10.0-1062.18.1.el7                         
  sqlite.x86_64 0:3.7.17-8.el7_7.1                              sudo.x86_64 0:1.8.23-4.el7_7.2  
--------------------------------


# disable mysqld

# reboot

# security update once more:
--------------------------------
sudo yum --disablerepo=epel* --security update-minimal

Installed:
  kernel.x86_64 0:3.10.0-1127.el7              kernel-devel.x86_64 0:3.10.0-1127.el7             

Updated:
  avahi-libs.x86_64 0:0.6.31-20.el7                bash.x86_64 0:4.2.46-34.el7                   
  bind-export-libs.x86_64 32:9.11.4-16.P2.el7      curl.x86_64 0:7.29.0-57.el7                   
  doxygen.x86_64 1:1.8.5-4.el7                     emacs-filesystem.noarch 1:24.3-23.el7         
  expat.x86_64 0:2.1.0-11.el7                      file.x86_64 0:5.11-36.el7                     
  file-libs.x86_64 0:5.11-36.el7                   gettext.x86_64 0:0.19.8.1-3.el7               
  gettext-common-devel.noarch 0:0.19.8.1-3.el7     gettext-devel.x86_64 0:0.19.8.1-3.el7         
  gettext-libs.x86_64 0:0.19.8.1-3.el7             httpd.x86_64 0:2.4.6-93.el7                   
  httpd-tools.x86_64 0:2.4.6-93.el7                kernel-headers.x86_64 0:3.10.0-1127.el7       
  kernel-tools.x86_64 0:3.10.0-1127.el7            kernel-tools-libs.x86_64 0:3.10.0-1127.el7    
  libcurl.x86_64 0:7.29.0-57.el7                   libxml2.x86_64 0:2.9.1-6.el7.4                
  libxml2-python.x86_64 0:2.9.1-6.el7.4            polkit.x86_64 0:0.112-26.el7                  
  python.x86_64 0:2.7.5-88.el7                     python-libs.x86_64 0:2.7.5-88.el7             
  python-magic.noarch 0:5.11-36.el7                python-perf.x86_64 0:3.10.0-1127.el7          
  rsyslog.x86_64 0:8.24.0-52.el7                   shared-mime-info.x86_64 0:1.8-5.el7           
  unzip.x86_64 0:6.0-21.el7 
--------------------------------


# decided to upgrade to RHEL 7.8 since the 3.10.0-1127 kernel is default for for 

sudo yum check-update

sudo yum update

uname -a
Linux cloud7.biohpc.swmed.edu 3.10.0-1127.el7.x86_64  

cat /etc/redhat-release
Red Hat Enterprise Linux Server release 7.8 (Maipo)

```

\
\

### MySQL Initial Setup 

```
sudo systemctl start htppd
sudo systemctl start mysqld

# fetch temporary root password:

sudo vim /var/log/mysqld.log
--------------------------------
2020-04-03T03:04:06.526044Z 1 [Note] A temporary password is generated for root@localhost: UXovdhPs*4Xw
--------------------------------

# perform mysql secure installation
--------------------------------
Securing the MySQL server deployment.

Enter password for user root: 

The existing password for the user account root has expired. Please set a new password.

New password: 

Re-enter new password: 
The 'validate_password' plugin is installed on the server.
The subsequent steps will run with the existing configuration
of the plugin.
Using existing password for root.

Estimated strength of the password: 100 
Change the password for root ? ((Press y|Y for Yes, any other key for No) : 

 ... skipping.
By default, a MySQL installation has an anonymous user,
allowing anyone to log into MySQL without having to have
a user account created for them. This is intended only for
testing, and to make the installation go a bit smoother.
You should remove them before moving into a production
environment.

Remove anonymous users? (Press y|Y for Yes, any other key for No) : Y
Success.


Normally, root should only be allowed to connect from
'localhost'. This ensures that someone cannot guess at
the root password from the network.

Disallow root login remotely? (Press y|Y for Yes, any other key for No) : Y
Success.

By default, MySQL comes with a database named 'test' that
anyone can access. This is also intended only for testing,
and should be removed before moving into a production
environment.


Remove test database and access to it? (Press y|Y for Yes, any other key for No) : Y
 - Dropping test database...
Success.

 - Removing privileges on test database...
Success.

Reloading the privilege tables will ensure that all changes
made so far will take effect immediately.

Reload privilege tables now? (Press y|Y for Yes, any other key for No) : Y
Success.

All done! 
--------------------------------


# Create Dedicated User & Database for Cloud Data:
--------------------------------
sudo mysql -u root -p
[sudo] password for biohpcadmin: 
Enter password: 

  # DMZ pass for both

CREATE DATABASE biohpc_cloud;

SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| biohpc_cloud       |
| mysql              |
| performance_schema |
| sys                |
+--------------------+

CREATE USER 'cloud_overseer'@'localhost' IDENTIFIED by 'Cl0udW0rk3er~';

GRANT ALL PRIVILEGES ON biohpc_cloud.* to 'cloud_overseer'@'localhost';

 SELECT User, Host FROM mysql.user;
+----------------+-----------+
| User           | Host      |
+----------------+-----------+
| cloud_overseer | localhost |
| mysql.session  | localhost |
| mysql.sys      | localhost |
| root           | localhost |
+----------------+-----------+

 mysql -u cloud_overseer -p

SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| biohpc_cloud       |
+--------------------+

--------------------------------

```

\
\

### Copy Production Server's Data

```
# previous iptables ruleset on production:
--------------------------------
Chain INPUT (policy ACCEPT)
target     prot opt source               destination         
ACCEPT     all  --  anywhere             anywhere            state RELATED,ESTABLISHED 
ACCEPT     icmp --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     tcp  --  198.215.54.21        anywhere            state NEW tcp dpt:ssh 
ACCEPT     tcp  --  swlxnexpose01.swmed.org  anywhere            state NEW tcp dpt:ssh 
ACCEPT     tcp  --  198.241.19.201       anywhere            state NEW tcp dpt:ssh 
ACCEPT     tcp  --  198.241.19.202       anywhere            state NEW tcp dpt:ssh 
ACCEPT     tcp  --  198.215.54.65        anywhere            state NEW tcp dpt:ssh 
ACCEPT     tcp  --  198.215.54.12        anywhere            state NEW tcp dpt:ssh 
ACCEPT     tcp  --  198.215.54.58        anywhere            state NEW tcp dpt:ssh 
ACCEPT     tcp  --  anywhere             anywhere            state NEW tcp dpt:http 
ACCEPT     tcp  --  anywhere             anywhere            state NEW tcp dpt:https 
ACCEPT     tcp  --  anywhere             anywhere            state NEW tcp dpt:8089 
REJECT     all  --  anywhere             anywhere            reject-with icmp-host-prohibited 

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination         
REJECT     all  --  anywhere             anywhere            reject-with icmp-host-prohibited 

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination   
--------------------------------

# in config file:
--------------------------------
*filter
:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [212664404:882870786188]
-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
-A INPUT -p icmp -j ACCEPT
-A INPUT -i lo -j ACCEPT
# biohpcadmin.swmed.org
-A INPUT -p tcp -m state --state NEW -s 198.215.54.21 -m tcp --dport 22 -j ACCEPT
# Nexpose Scanning Hosts
-A INPUT -p tcp -m state --state NEW -s 199.165.152.154 -m tcp --dport 22 -j ACCEPT
-A INPUT -p tcp -m state --state NEW -s 198.241.19.201 -m tcp --dport 22 -j ACCEPT
-A INPUT -p tcp -m state --state NEW -s 198.241.19.202 -m tcp --dport 22 -j ACCEPT
#-A INPUT -p tcp -m state --state NEW -s 208.118.237.20 -m tcp --dport 22 -j ACCEPT
# backup001 nucleusS004.biohpc.swmed.edu
-A INPUT -p tcp -m state --state NEW -s 198.215.54.65 -m tcp --dport 22 -j ACCEPT
-A INPUT -p tcp -m state --state NEW -s 198.215.54.12 -m tcp --dport 22 -j ACCEPT
# Allow login from nucleus006, so dont have to go to bihpcadmin every time
-A INPUT -p tcp -m state --state NEW -s 198.215.54.58 -m tcp --dport 22 -j ACCEPT
# Allow Zeng's WS
# -A INPUT -p tcp -m state --state NEW -s 198.215.56.37 -m tcp --dport 22 -j ACCEPT
# http and https
-A INPUT -p tcp -m state --state NEW -m tcp --dport 80 -j ACCEPT
-A INPUT -p tcp -m state --state NEW -m tcp --dport 443 -j ACCEPT
# Splunk
-A INPUT -p tcp -m state --state NEW -m tcp --dport 8089 -j ACCEPT
-A INPUT -j REJECT --reject-with icmp-host-prohibited
-A FORWARD -j REJECT --reject-with icmp-host-prohibited
COMMIT
--------------------------------

# added line:
--------------------------------
# Allow port 22 from Test Cloud Server! 
-A INPUT -p tcp -m state --state NEW -s 129.112.9.42 -m tcp --dport 22 -j ACCEPT
--------------------------------

# reloaded iptables
--------------------------------
sudo service iptables reload
iptables: Trying to reload firewall rules:                 [  OK  ]

# IR/Ntwk Services Temp. Named Test Server 'mulan.swmed.edu'?

sudo iptables -L

ACCEPT     tcp  --  mulan.swmed.edu      anywhere            state NEW tcp dpt:ssh 

ping mulan.swmed.edu

64 bytes from mulan.swmed.edu (129.112.9.42): icmp_seq=1 ttl=64 time=0.172 ms
--------------------------------

# change SSHD rules to allow root copy from Test Cloud
--------------------------------

# lines added:

PermitRootLogin yes

AllowUsers ... root@129.112.9.42

# reload

sudo service sshd reload
Reloading sshd:                                            [  OK  ]
--------------------------------

# test SSH access

  # ssh from Nucleus006 --> access denied, good

  # ssh from Test Cloud
[biohpcadmin@cloud7 ~]$ ssh root@129.112.9.92


# Transfer User Data

sudo chmod g+w /home/condensation/
sudo chgrp biohpcadmin /home/condensation/

  # exclude old log files and sql dumps in production data directory:

nohup sudo rsync -a --exclude='*.log' --exclude='*.sql' root@129.112.9.92:/var/www/owncloud/data/data /cloud_data > /home/condensation/data_transfer_04052020.out 2>&1 
--------------------------------
^Z
[1]+  Stopped                 nohup sudo rsync -a --exclude='*.log' --exclude='*.sql' root@129.112.9.92:/var/www/owncloud/data/data /cloud_data > /home/condensation/data_transfer_04052020.out 2>&1

 bg
[1]+ nohup sudo rsync -a --exclude='*.log' --exclude='*.sql' root@129.112.9.92:/var/www/owncloud/data/data /cloud_data > /home/condensation/data_transfer_04052020.out 2>&1 &

 ps -C rsync -o uid,user,%cpu,%mem,etime
  UID USER     %CPU %MEM     ELAPSED
    0 root      0.0  0.0       03:42
    0 root     21.6  0.0       03:29

df -Th | grep -i cloud
/dev/mapper/cloudData-cloud_data xfs       4.0T   18G  4.0T   1% /cloud_data

df -Th | grep -i cloud
/dev/mapper/cloudData-cloud_data xfs       4.0T   87G  4.0T   3% /cloud_data
--------------------------------

#
#
#

# Copy Nextcloud Application from Production

--------------------------------
 nohup sudo rsync -a root@129.112.9.92:/var/www/owncloud/html /home/condensation > /home/condensation/nc_transfer_04052020.out 2>&1 

pgrep rsync
13120
13122
[1]+  Done                    nohup sudo rsync -a root@129.112.9.92:/var/www/owncloud/html /home/condensation > /home/condensation/nc_transfer_04052020.out 2>&1


 du -sh /home/condensation/html/
207M	/home/condensation/html/

cd /home/condensation/html/

sudo find . -type f | wc -l
13522

sudo find . -type d | wc -l
2278

  # same find commands ran on production's NC directory, the numbers matched


# ran rsync once more next day with verbose and extra exclude handle

nohup sudo rsync -av --exclude='*.log' --exclude='*.log.1' --exclude='*.sql' root@129.112.9.92:/var/www/owncloud/data/data /cloud_data > /home/condensation/data_transfer_04072020.out 2>&1


# next day did rsync with `--delete` option

nohup sudo rsync -av --delete --exclude='*.log' --exclude='*.log.1' --exclude='*.sql' root@129.112.9.92:/var/www/owncloud/data/data /cloud_data > /home/condensation/data_transfer_04162020.out 2>&1


# at the END of the migration: change permissions of app and data

sudo chown -R apache:apache /cloud_data/data/

--------------------------------
```

\
\

### Configure Nextcloud

```
# info

129.112.9.42
cloud7.biohpc.swmed.edu


# change config.php


```

\
\

### Import Database

```
sudo rsync -av root@129.112.9.92:/var/www/owncloud/DB_backup/db_20200416033003.sql /home/condensation/sql_imports/


```
 


### HTTPD + PHP + MySQL Tuning

```
# configure PHP per script memory limit, upload size, & total size per round of upload(s) 
-------------------------------------------
  # try not to change these values via web portal as admin

# /etc/opt/rh/rh-php73/php.ini

post_max_size = 20G
upload_max_filesize = 20G
memory_limit = 512M
#max_execution_time = 10800

-------------------------------------------

# MySQL Configurations
-------------------------------------------
sudo systemctl stop httpd

sudo systemctl stop mysqld

sudo vim /etc/my.cnf
---------------------------------
innodb_buffer_pool_size = 8G
innodb_buffer_pool_chunk_size = 256M
innodb_buffer_pool_instances = 16
---------------------------------

  # default `innodb_buffer_pool_instances`(8) & 
`innodb_buffer_pool_chunk_size`(128MB)   

-------------------------------------------


# apcu
-------------------------------------------
sudo vim /etc/opt/rh/rh-php73/php.d/40-apcu.ini

apc.enable_cli=1
-------------------------------------------

# opcache
-------------------------------------------
sudo vim /etc/opt/rh/rh-php73/php.d/10-opcache.ini

opcache.enable=1
opcache.enable_cli=1
opcache.memory_consumption=128
opcache.interned_strings_buffer=8
opcache.max_accelerated_files=12000
opcache.validate_timestamps=1
opcache.revalidate_freq=1
opcache.save_comments=1

-------------------------------------------




# check loaded HTTPD modules

--------------
httpd -M
--------------

# check installed PHP modules

--------------
php -m
--------------
```

\
\

### IPTables + SSHD Rules

```
# IPTABLES

sudo yum install iptables-services.x86_64

sudo vim /etc/sysconfig/iptables

-------------------------------------------
*filter
:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
-A INPUT -p icmp -j ACCEPT
-A INPUT -i lo -j ACCEPT
# Admin SSH
-A INPUT -p tcp -m state --state NEW -s 198.215.54.21 -m tcp --dport 22 -j ACCEPT
-A INPUT -p tcp -m state --state NEW -s 198.215.56.37 -m tcp --dport 22 -j ACCEPT
-A INPUT -p tcp -m state --state NEW -s 198.215.54.58 -m tcp --dport 22 -j ACCEPT
-A INPUT -p tcp -m state --state NEW -s 129.112.9.92  -m tcp --dport 22 -j ACCEPT
# SSH for UTSW Nexpose Scanning Hosts
-A INPUT -p tcp -m state --state NEW -s 199.165.152.154 -m tcp --dport 22 -j ACCEPT
-A INPUT -p tcp -m state --state NEW -s 198.241.19.201 -m tcp --dport 22 -j ACCEPT
-A INPUT -p tcp -m state --state NEW -s 198.241.19.202 -m tcp --dport 22 -j ACCEPT
# SSH for backup001 server
-A INPUT -p tcp -m state --state NEW -s 198.215.54.65 -m tcp --dport 22 -j ACCEPT
-A INPUT -p tcp -m state --state NEW -s 198.215.54.12 -m tcp --dport 22 -j ACCEPT
# HTTP & HTTPS
-A INPUT -p tcp -m state --state NEW -m tcp --dport 80 -j ACCEPT
-A INPUT -p tcp -m state --state NEW -m tcp --dport 443 -j ACCEPT
#
-A INPUT -j REJECT --reject-with icmp-host-prohibited
-A FORWARD -j REJECT --reject-with icmp-host-prohibited
COMMIT
-------------------------------------------


# SSHD Rules
-------------------------------------------
AllowUsers biohpcadmin swcwscan backup_services

Match User swcwscan,backup_services
  PasswordAuthentication no
-------------------------------------------

```


### ICING ON TOP

```
# add occ to $PATH!

# implement `backup_services` cron jobs

# implement `orphan_sweep` cron jobs

```

\
\

```
# configure posix for emailing
-------------------------------------------
sudo vim /etc/postfix/main.cf

relayhost = 199.165.152.166
inet_interfaces = all
-------------------------------------------

```

\
\

#### THEMING

```
### in /var/html/nextcloud/core/css/guest.css
-------------------------------------------
#submit-wrapper,
#reset-password-wrapper {
        display: flex;
        align-items: center;
        justify-content: center;
        position: relative; /* Make the wrapper the containing block of its
                                                   absolutely positioned descendant icons */
        padding: 5px;      //BioHPC Changes
}


#submit-wrapper .submit-icon {
        position: absolute;
        top: 25px;         //BioHPC Changes
        right: 24px;
        transition: right 100ms ease-in-out;
        pointer-events: none; /* The submit icon is positioned on the submit button.
                                                         From the user point of view the icon is part of the
                                                         button, so the clicks on the icon have to be
                                                         applied to the button instead. */
}


a.button {
        font-size: 18px;    // BiohHPC Changes
        margin: 5px;
        padding: 11px 10px 9px;
        outline: none;
        border-radius: 3px; /* --border-radius */
        -webkit-appearance: none;
}

-------------------------------------------

```


\
\
\

### Create Nexpose User & Install Splunkd

```
### Create nextpose scanning user

cd /tmp; wget https://www.utsouthwestern.net/intranet/administration/information-resources/info-sec/vulnerability-management/swcwscan-publickey.txt

sudo useradd swcwscan

sudo su swcwscan

mkdir /home/swcwscan/.ssh; sudo chmod 700 /home/swcwscan/.ssh

cat /tmp/swcwscan-publickey.txt >> /home/swcwscan/.ssh/authorized_keys

```

\
\

### Enable NTPD

```
# couldn't enable due to following error:
------------------------------------------------------------
ntpdate[17286]: no server suitable for synchronization found
------------------------------------------------------------

# instead used `timedatectl set-time 'HH:MM:SS'` to sync with NucleusS004

```

\
\

### Xymon

```

sudo scp /project/biohpcadmin/shared/rpms/xymon/rhel7/xymon-client-4.3.28-1.el7.x86_64.rpm biohpcadmin@129.112.9.92:/home/condensation/images/

sudo yum install xymon-client-4.3.28-1.el7.x86_64.rpm

# copy config to server
cat /project/biohpcadmin/shared/rpms/xymon/rhel7/xymonclient.cfg 

# allow SSH from xymon host via both iptables & sshd_config
```

\
\

### Splunkd v8.0.1

```
cd /home/condensation

wget http://linux.swmed.edu/linux/misc/splunk/8.0.1/splunk-8.0.1-6db836e2fb9e-linux-2.6-x86_64.rpm

sudo yum install splunk-8.0.1-6db836e2fb9e-linux-2.6-x86_64.rpm

sudo /opt/splunk/bin/splunk enable boot-start --accept-license

 # here you will be prompted to set up an admin user: `biohpcadmin` + old root pass

sudo /opt/splunk/bin/splunk set deploy-poll splunkdeploy.swmed.net:8089

sudo /opt/splunk/bin/splunk restart

# this is no longer needed
sudo systemctl restart splunk

# Verify:
sudo /opt/splunk/bin/splunk status

 # sample output:
-------------------------------------
splunkd is running (PID: 2030).
splunk helpers are running (PIDs: 2032 2050 2175 2236).
-------------------------------------

```

  - [splunk client deploy documentation](https://docs.splunk.com/Documentation/Splunk/8.0.3/Updating/Configuredeploymentclients)


\
\

### Backup setup

```
# crete user backup_services 

# assign appropriate UID & GID 

# add backup001's pubkey to .ssh/authorized_keys

# add to biohpc_admin group, add backup_services to it, add to 'apache' group

sudo chmod g+s /var/www/html/db_bak/

sudo chown -R biohpcadmin:apache /var/www/html/db_bak/

```

\
\

### APACHE Security Enforcements


- redirect to HTTPS

 - in httpd.conf

```
    # redirect HTTP connections to HTTPS
    RewriteEngine On
    RewriteCond %{HTTPS} off
    RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI}

```

- use TLS 1.2 & 1.3 protocol

 - in ssl.conf

```
SSLProtocol -all +TLSv1.2 +TLSv1.3

```

\
\

### Additional Touch-ups

- UID-GID mismatch

```
sudo /opt/splunk/bin/splunk stop
sudo usermod -u 1003 splunk
sudo usermod -u 1002 swcwscan
sudo /opt/splunk/bin/splunk restart
```

- LDAP Remnant User Cleanup

```
sudo vim /var/www/html/nextcloud/config/config.php

'ldapUserCleanupInterval' => 10,

sudo -u apache php occ ldap:show-remnants

+--------------------------------------+--------------------+----------+------------------------------------------------------------------+-------------------+----------------+-----+--------+
| Nextcloud name                       | Display Name       | LDAP UID | LDAP DN                                                          | Last Login        | Detected on    | Dir | Sharer |
+--------------------------------------+--------------------+----------+------------------------------------------------------------------+-------------------+----------------+-----+--------+
| 0bc2f2d2-4fd9-4992-8070-aaaa0ecc9a34 | Hackathon User 114 | hack114  | uid=hack114,ou=hackathon,ou=users,dc=biohpc,dc=swmed,dc=edu      | -                 | April 24, 2020 |     | N      |
| 124e8121-883e-44ad-930f-c5ef99a3fbf0 | Pranish Kantak     | s182514  | uid=s182514,ou=tibir,ou=users,dc=biohpc,dc=swmed,dc=edu          | August 6, 2018    | April 24, 2020 |     | N      |
| 15629011-f7fd-49c7-b5a6-3f4107f8cd19 | Hackathon User 118 | hack118  | uid=hack118,ou=hackathon,ou=users,dc=biohpc,dc=swmed,dc=edu      | -                 | April 24, 2020 |     | N      |
| 206c67ad-ab99-44b9-89f0-bd8f005f138b | Hackathon User 110 | hack110  | uid=hack110,ou=hackathon,ou=users,dc=biohpc,dc=swmed,dc=edu      | -                 | April 23, 2020 |     | N      |
| 211a25cf-2e59-4c07-8a80-009c35a4472f | Hackathon User 106 | hack106  | uid=hack106,ou=hackathon,ou=users,dc=biohpc,dc=swmed,dc=edu      | -                 | April 24, 2020 |     | N      |
| 25998dbb-24f4-47fe-a84f-18a8f5ca46f9 | Hackathon User 115 | hack115  | uid=hack115,ou=hackathon,ou=users,dc=biohpc,dc=swmed,dc=edu      | -                 | April 24, 2020 |     | N      |
| 36a2f947-f02b-4e60-b2bb-52492bcff1a7 | Hackathon User 113 | hack113  | uid=hack113,ou=hackathon,ou=users,dc=biohpc,dc=swmed,dc=edu      | -                 | April 24, 2020 |     | N      |
| 38721cba-97c8-414b-aa3b-92f6b3bcb452 | Hackathon User 112 | hack112  | uid=hack112,ou=hackathon,ou=users,dc=biohpc,dc=swmed,dc=edu      | -                 | April 24, 2020 |     | N      |
| 3b3d49c3-0585-484f-be64-63aea1f0058b | Hackathon User 103 | hack103  | uid=hack103,ou=hackathon,ou=users,dc=biohpc,dc=swmed,dc=edu      | -                 | April 24, 2020 |     | N      |
| 56b1a878-cf91-481d-aa9a-0f4a2c66182c | Anand Kadumberi    | s167746  | uid=s167746,ou=bioinformatics,ou=users,dc=biohpc,dc=swmed,dc=edu | December 22, 2017 | April 23, 2020 |     | N      |
| 58a9b2df-49fe-49e3-a1d4-2a8404876991 | Bogdan Bordieanu   | s178964  | uid=s178964,ou=cri,ou=users,dc=biohpc,dc=swmed,dc=edu            | November 19, 2019 | April 23, 2020 |     | Y      |
| 5bb25cf2-b738-4570-ac64-d2ab7a93208d | Luciana Oliveira   | s169185  | uid=s169185,ou=biophysics,ou=users,dc=biohpc,dc=swmed,dc=edu     | August 12, 2019   | April 18, 2020 |     | Y      |
| 5bc802b6-722e-4151-b209-b5fd68c38187 | Hackathon User 119 | hack119  | uid=hack119,ou=hackathon,ou=users,dc=biohpc,dc=swmed,dc=edu      | -                 | April 24, 2020 |     | N      |
| 5c81096f-e1d1-41d9-90f4-8d48b869167e | Hackathon User 109 | hack109  | uid=hack109,ou=hackathon,ou=users,dc=biohpc,dc=swmed,dc=edu      | -                 | April 24, 2020 |     | N      |
| 5cb9e153-035e-4490-81c5-e4e44349ed5a | XIN LIU            | s160704  | uid=s160704,ou=cri,ou=users,dc=biohpc,dc=swmed,dc=edu            | December 9, 2019  | April 17, 2020 |     | N      |
| 6153c0b5-4ab9-471d-be02-37e4a4e54d47 | Hackathon User 107 | hack107  | uid=hack107,ou=hackathon,ou=users,dc=biohpc,dc=swmed,dc=edu      | -                 | April 24, 2020 |     | N      |
| a9c70fde-ac14-41a6-99cb-482852b9a41f | Andrew Schober     | aschober | uid=aschober,ou=greencenter,ou=users,dc=biohpc,dc=swmed,dc=edu   | February 13, 2018 | April 23, 2020 |     | N      |
| d5a4c86b-f9a4-47c5-a18f-a6db2ac9831b | Hackathon User 000 | hack000  | uid=hack000,ou=hackathon,ou=users,dc=biohpc,dc=swmed,dc=edu      | -                 | April 24, 2020 |     | N      |
| db37a01a-f1c1-4295-a9ea-8236b11b50ba | Hackathon User 108 | hack108  | uid=hack108,ou=hackathon,ou=users,dc=biohpc,dc=swmed,dc=edu      | -                 | April 24, 2020 |     | N      |
| e543e903-78a2-4090-9713-792f636be609 | Michael Porter     | s421953  | uid=s421953,ou=sccc,ou=users,dc=biohpc,dc=swmed,dc=edu           | January 17, 2019  | April 24, 2020 |     | N      |
| ee3b58f3-0a55-48c7-a354-785f3224d252 | Hackathon User 105 | hack105  | uid=hack105,ou=hackathon,ou=users,dc=biohpc,dc=swmed,dc=edu      | -                 | April 24, 2020 |     | N      |
| f8ad9fa0-c074-416e-b834-8cc0146a7276 | Hackathon User 104 | hack104  | uid=hack104,ou=hackathon,ou=users,dc=biohpc,dc=swmed,dc=edu      | -                 | April 24, 2020 |     | N      |
+--------------------------------------+--------------------+----------+------------------------------------------------------------------+-------------------+----------------+-----+--------+
```

\
\

- **SSL Cert Replacement**

``` 
# in /etc/httpd/conf.d/ssl.conf

SSLCertificateFile /etc/ssl/certs/cloud_biohpc_swmed_edu_cert.cer

SSLCertificateKeyFile /etc/ssl/certs/cloud_biohpc_swmed_edu.key

SSLCertificateChainFile /etc/ssl/certs/cloud_biohpc_swmed_edu_interm.cer


# upload new files to /tmp/directory of server

  # first copied to /home2/cloud_ssl/

Nucleus006 ~]$ scp -r cloud_ssl/ biohpcadmin@cloud7:/tmp/

# on cloud server, change permission of cert files

cloud7 ~]$ chmod 644 /tmp/cloud_ssl/* 

# overwrite existing cert files with `-i` confirmation option

sudo cp -i /tmp/cloud_ssl/cloud_biohpc_swmed_edu_cert.cer /etc/ssl/certs/cloud_biohpc_swmed_edu_cert.cer

sudo cp -i /tmp/cloud_ssl/cloud_biohpc_swmed_edu.key /etc/ssl/certs/cloud_biohpc_swmed_edu.key

sudo cp -i /tmp/cloud_ssl/cloud_biohpc_swmed_edu_interm.cer /etc/ssl/certs/cloud_biohpc_swmed_edu_interm.cer 


# Restart httpd server --> NEW CERTS WILL NOT GO INTO EFFECT UNTIL Server Reloads!

sudo systemctl restart httpd
```
- [simple instructions for adding ssl cert](https://www.linuxhelp.com/install-ssl-certificate-in-rhelcentos)

















































































### BUGS Noticed On Production

```
# `backup_services` squeezing a relatively large amount of SQL dumps on a weekly basis without cleaning them
--------------------------------
sudo ls -lt /var/www/owncloud/DB_backup/
total 18574548
-rw-r--r-- 1 backup_services biohpc_admin 940329725 Apr  5 03:30 db_20200405033001.sql
-rw-r--r-- 1 backup_services biohpc_admin 952497703 Apr  4 03:30 db_20200404033002.sql
-rw-r--r-- 1 backup_services biohpc_admin 915015149 Apr  3 03:30 db_20200403033003.sql
-rw-r--r-- 1 backup_services biohpc_admin 903524079 Apr  2 03:30 db_20200402033003.sql
-rw-r--r-- 1 backup_services biohpc_admin 903206628 Apr  1 03:30 db_20200401033003.sql
-rw-r--r-- 1 backup_services biohpc_admin 894983544 Mar 30 03:30 db_20200330033002.sql
-rw-r--r-- 1 backup_services biohpc_admin 894819188 Mar 29 03:30 db_20200329033003.sql
-rw-r--r-- 1 backup_services biohpc_admin 894638553 Mar 28 03:30 db_20200328033002.sql
-rw-r--r-- 1 backup_services biohpc_admin 894148706 Mar 27 03:30 db_20200327033003.sql
-rw-r--r-- 1 backup_services biohpc_admin 889870726 Mar 26 03:30 db_20200326033003.sql
-rw-r--r-- 1 backup_services biohpc_admin 887428323 Mar 25 03:30 db_20200325033003.sql
-rw-r--r-- 1 backup_services biohpc_admin 886614936 Mar 24 03:30 db_20200324033002.sql
-rw-r--r-- 1 backup_services biohpc_admin 885536121 Mar 23 03:30 db_20200323033003.sql
-rw-r--r-- 1 backup_services biohpc_admin 884792498 Mar 21 03:30 db_20200321033003.sql
-rw-r--r-- 1 backup_services biohpc_admin 883404542 Mar 20 03:30 db_20200320033001.sql
-rw-r--r-- 1 backup_services biohpc_admin 918666424 Mar 19 03:30 db_20200319033003.sql
-rw-r--r-- 1 backup_services biohpc_admin 920250213 Mar 18 16:30 db_20200318163015.sql
-rw-r--r-- 1 backup_services biohpc_admin 919927544 Mar 18 03:30 db_20200318033003.sql
-rw-r--r-- 1 backup_services biohpc_admin 919598040 Mar 17 03:30 db_20200317033001.sql
-rw-r--r-- 1 backup_services biohpc_admin 915589381 Mar 16 03:30 db_20200316033002.sql
-rw-r--r-- 1 backup_services biohpc_admin 915371316 Mar 15 03:30 db_20200315033001.sql

sudo du -sh /var/www/owncloud/DB_backup/
18G	/var/www/owncloud/DB_backup/

sudo rm  /var/www/owncloud/DB_backup/db_2020031*

sudo du -sh /var/www/owncloud/DB_backup/
13G	/var/www/owncloud/DB_backup/

# changed `backup_services` crontab to delete SQL dumps OLDER than 14 DAYS every Sunday @3P.M.

  # original:
-------------
crontab -l
0 5 * * 3,6 cp /var/www/owncloud/data/data/nextcloud.log /home/backup_services/cloud_log/nextcloud.log."`date +%m%d%Y`"

# remove `.sql` dumps older than 21 days: every Sun@3:00P.M.

0 15 * * sun /bin/find /var/www/owncloud/DB_backup -mtime +21 -exec rm {} \;  
-------------

  # modified:
-------------
crontab -l
0 5 * * 3,6 cp /var/www/owncloud/data/data/nextcloud.log /home/backup_services/cloud_log/nextcloud.log."`date +%m%d%Y`"

# remove `.sql` dumps older than 21 days: every Sun@3:00P.M.

0 15 * * sun /bin/find /var/www/owncloud/DB_backup -mtime +14 -exec rm {} \; 
-------------
--------------------------------


```

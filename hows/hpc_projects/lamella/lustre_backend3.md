## Setting Up Lustre Parallel FS Backend-Lamella Test Deployment-Part III 

	Creating Lustre Object Storage Servers & Lustre Clients (on 2 new nodes)


### Steps for base Lustre Setup: Simplified 

```
1. Add zfs repo 

2. Install `e2fsprogs` distribution of Lustre

`sudo yum --nogpgcheck --disablerepo=* --enablerepo=e2fsprogs install e2fsprogs`

3. install Lustre-patched kernel packages (enable only the Lustre repo!)

4. hostgenID? 

5. reboot--check and see if  then install lustre software

`sudo yum --nogpgcheck --enablerepo=lustre-server install kmod-lustre-osd-ldiskfs lustre-osd-ldiskfs-mount lustre-osd-zfs-mount lustre lustre-resource-agents lustre-dkms zfs` 

# should result in the installation of 7 packages & 40+ dependencies (assuming you have a blank install)

6. set up LNET

sudo vim /etc/modprobe.d/lnet.conf

--------------------------------------------------
options lnet networks=tcp0(eth1)
--------------------------------------------------

sudo modprobe lnet 

[estrella@data2 ~]$ sudo modprobe -v lnet
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/net/libcfs.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/net/lnet.ko networks=tcp0(eth1)

lsmod | grep lnet 

sudo vim /etc/sysconfig/modules/lnet.modules

--------------------------------------------------
#!/bin/sh

if [ ! -c /dev/lnet ] ; then 
    exec /sbin/modprobe lnet >/dev/null 2>&1 
fi
--------------------------------------------------

sudo chmod 775 /etc/sysconfig/modules/lnet.modules  ### do this or the script won't run upon reboot!


7. Load zfs & lustre as kernel modules

`sudo modprobe -v zfs` 
`sudo modprobe -v lustre` 
`sudo lustre_rmmod`		#remove the lustre module (test/clients only)

 check zfs & lnet & lustre via `lsmod`

### Details ### 

[estrella@data2 ~]$ sudo modprobe -v zfs
[sudo] password for estrella: 
[estrella@data2 ~]$ 
[estrella@data2 ~]$ 
[estrella@data2 ~]$ 
[estrella@data2 ~]$ sudo modprobe -v lustre
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/net/libcfs.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/net/lnet.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/obdclass.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/ptlrpc.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/fld.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/fid.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/lov.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/osc.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/mdc.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/lmv.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/lustre.ko 
[estrella@data2 ~]$ 
[estrella@data2 ~]$ 
[estrella@data2 ~]$ lsmod | grep zfs
zfs                  3564425  0 
zunicode              331170  1 zfs
zavl                   15236  1 zfs
icp                   270148  1 zfs
zcommon                73440  1 zfs
znvpair                89131  2 zfs,zcommon
spl                   102412  4 icp,zfs,zcommon,znvpair
[estrella@data2 ~]$ 
[estrella@data2 ~]$ 
[estrella@data2 ~]$ 
[estrella@data2 ~]$ lsmod | grep lustre
lustre                758679  0 
lmv                   177987  1 lustre
mdc                   232938  1 lustre
lov                   314581  1 lustre
ptlrpc               2264705  7 fid,fld,lmv,mdc,lov,osc,lustre
obdclass             1962422  8 fid,fld,lmv,mdc,lov,osc,lustre,ptlrpc
lnet                  595941  6 lmv,osc,lustre,obdclass,ptlrpc,ksocklnd  # ABNORMAL 
libcfs                421295  11 fid,fld,lmv,mdc,lov,osc,lnet,lustre,obdclass,ptlrpc,ksocklnd

[estrella@data2 ~]$ sudo modprobe -v lnet
[estrella@data2 ~]$ 
[estrella@data2 ~]$ lsmod | grep lnet     # pre restarting lnet
lnet                  595941  6 lmv,osc,lustre,obdclass,ptlrpc,ksocklnd  # ABNORMAL
libcfs                421295  11 fid,fld,lmv,mdc,lov,osc,lnet,lustre,obdclass,ptlrpc,ksocklnd

# for the 'ABNORMAL' LNET statuses highlighted above, make sure your 'lnet.conf' & 'lnet.modules' files are configured properly, then load the module---the stdout one should get for `lsmod | grep lnet` should be:

lsmod | grep lnet
lnet                  595941  0 
libcfs                421295  1 lnet


##################Create Template (clone) at this Point######################

``` 
nbsp;
nbsp;
nbsp;

### Setting up the OSS (Object Storage Service)

	essentially the I/O services/servers 

#### rundown of protocol

- essentially identical to that for the MDS (meta data service)

  1. make a block device/partition/virtual device/logical volume (in this deployment, a ZFS zpool, which is similar to a vg from LVM)
 
  2. make a lustre file system on the virtual volume via `mkfs.lustre`

    - must-have command options:

      - `--mgsnode` specify the network address of your management service node (may be serveral nodes, just repeat the option entry) 

      - `--fsname` name of an overall/global lustre filesystem (what will be mounted by the client) that this particular file system will join 
   
      - `--index` specify a particular OST or MDT index that is joining the overall lustre file system (in this case, `silustre`). Required for all targets other than the MGS, and must be unique for all targets in the same filesystem. Typically sequential values starting from 0 are used. The index parameter may either be a decimal number, or a hexadecimal number starting with '0x'  

  3. mount the newly made filesystem to start the lustre-oss 

### Details 

- primary reference sources:

[lustre official doc on setting up OSS](http://wiki.lustre.org/Creating_Lustre_Object_Storage_Services_(OSS)) 

[lustre introduction page](http://wiki.lustre.org/Introduction_to_Lustre) 

[zpool updated manpage](https://fossies.org/linux/zfs/man/man8/zpool.8)

```
# pristine virtual disks w/o any partitions 

[estrella@oss1 ~]$ lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0   12G  0 disk 
├─sda1            8:1    0    1G  0 part /boot
└─sda2            8:2    0   11G  0 part 
  ├─centos-root 253:0    0  9.8G  0 lvm  /
  └─centos-swap 253:1    0  1.2G  0 lvm  [SWAP]
sr0              11:0    1 1024M  0 rom  
nvme0n1         259:0    0   45G  0 disk 
nvme0n2         259:1    0   45G  0 disk 
nvme0n3         259:2    0   45G  0 disk 
nvme0n4         259:3    0   45G  0 disk 


# no zpools yet 

[estrella@oss1 ~]$ zpool list
no pools available

# error while generating zpool

[estrella@oss1 ~]$ sudo zpool create -O canmount=off -o multihost=on -o cachefile=/etc/zfs/zpool.cache oss1pool mirror nvme0n1 nvme0n2 mirror nvme0n3 nvme0n4

cannot create 'oss1pool': requires a non-zero system hostid

# check hostid

[estrella@oss1 ~]$ hostid
640a03b4

# no `/etc/hostid` file though...STOP EXECUTING the generate host ID pipe lsited in the `lustre_backend1.md` file

# correct solution found here: "http://wiki.lustre.org/Protecting_File_System_Volumes_from_Concurrent_Access" 

which genhostid 

sudo genhostid   # re-generate a host id & place it in `/etc/hostid` with a hash 

[estrella@oss1 ~]$ od -An -tx /etc/hostid
 d3a5b4a7        # unhash and view the host ID 

[estrella@oss1 ~]$ hostid   # double-verify the host ID
d3a5b4a7

# remake the zpool (this time successful) & verify


[estrella@oss1 ~]$ sudo zpool create -O canmount=off -o multihost=on -o cachefile=/etc/zfs/zpool.cache oss1pool mirror nvme0n1 nvme0n2 mirror nvme0n3 nvme0n4
[estrella@oss1 ~]$ 
[estrella@oss1 ~]$ 
[estrella@oss1 ~]$ lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0   12G  0 disk 
├─sda1            8:1    0    1G  0 part /boot
└─sda2            8:2    0   11G  0 part 
  ├─centos-root 253:0    0  9.8G  0 lvm  /
  └─centos-swap 253:1    0  1.2G  0 lvm  [SWAP]
sr0              11:0    1 1024M  0 rom  
nvme0n1         259:0    0   45G  0 disk 
├─nvme0n1p1     259:4    0   45G  0 part 
└─nvme0n1p9     259:5    0    8M  0 part 
nvme0n2         259:1    0   45G  0 disk 
├─nvme0n2p1     259:6    0   45G  0 part 
└─nvme0n2p9     259:7    0    8M  0 part 
nvme0n3         259:2    0   45G  0 disk 
├─nvme0n3p1     259:8    0   45G  0 part 
└─nvme0n3p9     259:9    0    8M  0 part 
nvme0n4         259:3    0   45G  0 disk 
├─nvme0n4p1     259:10   0   45G  0 part 
└─nvme0n4p9     259:11   0    8M  0 part 
[estrella@oss1 ~]$ 
[estrella@oss1 ~]$ 
[estrella@oss1 ~]$ zpool list
NAME       SIZE  ALLOC   FREE  EXPANDSZ   FRAG    CAP  DEDUP  HEALTH  ALTROOT
oss1pool  89.5G   105K  89.5G         -     0%     0%  1.00x  ONLINE  -
[estrella@oss1 ~]$ 
[estrella@oss1 ~]$ 
[estrella@oss1 ~]$ zpool status
  pool: oss1pool
 state: ONLINE
  scan: none requested
config:

	NAME         STATE     READ WRITE CKSUM
	oss1pool     ONLINE       0     0     0
	  mirror-0   ONLINE       0     0     0
	    nvme0n1  ONLINE       0     0     0
	    nvme0n2  ONLINE       0     0     0
	  mirror-1   ONLINE       0     0     0
	    nvme0n3  ONLINE       0     0     0
	    nvme0n4  ONLINE       0     0     0

errors: No known data errors
[estrella@oss1 ~]$ 
[estrella@oss1 ~]$ 
[estrella@oss1 ~]$ zfs get all -s local
NAME      PROPERTY              VALUE                  SOURCE
oss1pool  canmount              off                    local


# create OST lustre fs 

[estrella@oss1 ~]$ sudo mkfs.lustre --ost --fsname=silustre --index=1 --mgsnode 10.100.180.2@tcp0 --backfstype=zfs oss1pool/ost1
[sudo] password for estrella: 

   Permanent disk data:
Target:     silustre:OST0001
Index:      1
Lustre FS:  silustre
Mount type: zfs
Flags:      0x62
              (OST first_time update )
Persistent mount opts: 
Parameters: mgsnode=10.100.180.2@tcp
checking for existing Lustre data: not found
mkfs_cmd = zfs create -o canmount=off  oss1pool/ost1
  xattr=sa
  dnodesize=auto
  recordsize=1M
Writing oss1pool/ost1 properties
  lustre:mgsnode=10.100.180.2@tcp
  lustre:version=1
  lustre:flags=98
  lustre:index=1
  lustre:fsname=silustre
  lustre:svname=silustre:OST0001


[estrella@oss1 ~]$ zfs get all -s local
NAME           PROPERTY              VALUE                  SOURCE
oss1pool       canmount              off                    local
oss1pool/ost1  recordsize            1M                     local
oss1pool/ost1  canmount              off                    local
oss1pool/ost1  xattr                 sa                     local
oss1pool/ost1  dnodesize             auto                   local
oss1pool/ost1  lustre:mgsnode        10.100.180.2@tcp       local
oss1pool/ost1  lustre:svname         silustre:OST0001       local
oss1pool/ost1  lustre:index          1                      local
oss1pool/ost1  lustre:flags          98                     local
oss1pool/ost1  lustre:fsname         silustre               local
oss1pool/ost1  lustre:version        1                      local


[estrella@oss1 ~]$ zfs list
NAME            USED  AVAIL  REFER  MOUNTPOINT
oss1pool        134K  86.7G    24K  /oss1pool
oss1pool/ost1    24K  86.7G    24K  /oss1pool/ost1


# Error Mounting
[estrella@oss1 ~]$ sudo mount.lustre oss1pool/ost1 /lustre/ost1
mount.lustre: mount oss1pool/ost1 at /lustre/ost1 failed: No such file or directory
Is the MGS specification correct?
Is the filesystem name correct?
If upgrading, is the copied client log valid? (see upgrade docs)


[estrella@oss1 ~]$ sudo lnetctl net show
net:
    - net type: lo
      local NI(s):
        - nid: 0@lo
          status: up
    - net type: tcp
      local NI(s):
        - nid: 10.100.180.3@tcp
          status: up
          interfaces:
              0: eth1


### TROUBLESHOOT Network Issue ### 


[estrella@mgds ~]$ sudo service firewalld stop       # stop firewalld
Redirecting to /bin/systemctl stop firewalld.service

[estrella@mgds ~]$ sudo iptables -L                  # iptable rules stopped
Chain INPUT (policy ACCEPT)
target     prot opt source               destination         

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination         

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination         
[estrella@mgds ~]$ 
[estrella@mgds ~]$ sudo setenforce 0               # disable SElink
[estrella@mgds ~]$ 
[estrella@mgds ~]$ sudo getenforce                   
Permissive                                         # verify 

# disable SElinux & Firewalld & the OST mounted successfully, indicating that it was a network issue that was preventing the OSS from mounting its filesystem on the OST & communicating to the MGS server


### Solution: Open TCP port 988 on MGS & Add OSS's IP to FirewallD Public zone

[estrella@mgds ~]$ sudo firewall-cmd --permanent --add-port=988/tcp
success
[estrella@mgds ~]$ 
[estrella@mgds ~]$ sudo firewall-cmd --permanent --zone=public --add-source=10.100.180.3
success
[estrella@mgds ~]$ sudo firewall-cmd --reload
success

# verify:

[estrella@mgds ~]$ sudo firewall-cmd --list-ports
988/tcp
[estrella@mgds ~]$ 
[estrella@mgds ~]$ 
[estrella@mgds ~]$ sudo cat /etc/firewalld/zones/public.xml
<?xml version="1.0" encoding="utf-8"?>
<zone>
  <short>Public</short>
  <description>For use in public areas. You do not trust the other computers on networks to not harm your computer. Only selected incoming connections are accepted.</description>
  <source address="10.100.180.3"/>
  <service name="ssh"/>
  <service name="dhcpv6-client"/>
  <port protocol="tcp" port="988"/>
</zone>


### NOTE: the `--add-source` firewall-cmd should be applied for additional OSSs

### Solution Sources 

[lustre wiki on SElinux & Firewalld](http://wiki.lustre.org/Operating_System_Configuration_Guidelines_For_Lustre)

[open allow specifc IP's & ports via firewall-cmd](https://serverfault.com/questions/684602/how-to-open-port-for-a-specific-ip-address-with-firewall-cmd-on-centos)

[details on firewall-cmd config](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-using-firewalld-on-centos-7)


# `mount.lustre` successful


[estrella@oss1 ~]$ sudo mount.lustre oss1pool/ost1 /lustre/ost1/

[estrella@oss1 ~]$ df -Th
Filesystem              Type      Size  Used Avail Use% Mounted on
/dev/mapper/centos-root xfs       9.8G  2.4G  7.4G  25% /
devtmpfs                devtmpfs  1.9G     0  1.9G   0% /dev
tmpfs                   tmpfs     1.9G     0  1.9G   0% /dev/shm
tmpfs                   tmpfs     1.9G  8.6M  1.9G   1% /run
tmpfs                   tmpfs     1.9G     0  1.9G   0% /sys/fs/cgroup
/dev/sda1               xfs      1014M  231M  784M  23% /boot
tmpfs                   tmpfs     379M     0  379M   0% /run/user/1000
oss1pool/ost1           lustre     87G  3.0M   87G   1% /lustre/ost1

[estrella@oss1 ~]$ sudo lctl dl
  0 UP osd-zfs silustre-OST0001-osd silustre-OST0001-osd_UUID 4
  1 UP mgc MGC10.100.180.2@tcp 4f2e9d75-85f9-6fcb-1cb4-7f3e3d30f9c8 4
  2 UP ost OSS OSS_uuid 2
  3 UP obdfilter silustre-OST0001 silustre-OST0001_UUID 4
  4 UP lwp silustre-MDT0000-lwp-OST0001 silustre-MDT0000-lwp-OST0001_UUID 4
```

### Lustre Client Setup (Finally)


#### Outline of steps

> Assuming that the lustre-patched kernel, lustre-dkms, zfs (& all of their dependencies) are installed 

- load the Lustre module & configure it to load automatically upon boot  

  - if you haven't done this for the LNET module, do it now

- make mount point & mount the lustre file system 

#### Details 

```

# If the client is a new machine or a cloned VM

- double check & correct the interface connecting to the host-only network 

  - assign unique IP

  - match MAC addr to the entry in ifcfg-config file 

  - ensure the interface name matches that listed in the config file (via `ip link set <old name> name <new name>`) 

  - restart the network 

- assign unique hostname to node & add hostname of lustre nodes

- regenerate a hostid: 

  - `which genhostid`   # if not available, then install it 

  - `hostid`   # check current hostID 

  - `sudo genhostid`   # generate a new one 

  - VERIFY: `hostid`, also execute `od -An -tx /etc/hostid` 

- Reload LNET & lustre kernel modules after initial setup!

- Add new hostname & IP to other lustre servers

# Add lustre module

[estrella@lamella_t1 ~]$ sudo modprobe -v lustre
[sudo] password for estrella: 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/obdclass.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/ptlrpc.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/fld.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/fid.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/lov.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/osc.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/mdc.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/lmv.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/lustre.ko 


[estrella@lamella_t1 ~]$ sudo vim /etc/sysconfig/modules/lustre.modules

----------------------------------------------------
#!/bin/sh

/sbin/lsmod | /bin/grep lustre 1>/dev/null 2>&1
if [ ! $? ] ; then
   /sbin/modprobe lustre >/dev/null 2>&1
fi
----------------------------------------------------

# now the lustre module will be loaded upon every reboot


# Mount `silustre`, the overall lustre filesystem 


[estrella@lamella_t1 ~]$ sudo mkdir /silustre


# NOTICE: the ***client mounts from the MGS node!*** 

  # before mounting, make sure your MGS,MDS,& OSS devices have started (in that respective sequence as well) 

[estrella@lamella_t1 ~]$ sudo mount.lustre 10.100.180.2@tcp0:/silustre /silustre/
[estrella@lamella_t1 ~]$ 
[estrella@lamella_t1 ~]$ df -Th
Filesystem                 Type      Size  Used Avail Use% Mounted on
/dev/mapper/centos-root    xfs       9.8G  2.4G  7.4G  25% /
devtmpfs                   devtmpfs  1.9G     0  1.9G   0% /dev
tmpfs                      tmpfs     1.9G     0  1.9G   0% /dev/shm
tmpfs                      tmpfs     1.9G  8.6M  1.9G   1% /run
tmpfs                      tmpfs     1.9G     0  1.9G   0% /sys/fs/cgroup
/dev/sda1                  xfs      1014M  231M  784M  23% /boot
tmpfs                      tmpfs     379M     0  379M   0% /run/user/1000
10.100.180.2@tcp:/silustre lustre     87G  3.0M   87G   1% /silustre

# Check Lustre filesystem 

[estrella@lamella_t1 ~]$ sudo lfs check servers
silustre-MDT0000-mdc-ffff9e89956ea800 active.
silustre-OST0001-osc-ffff9e89956ea800 active.

[estrella@lamella_t1 ~]$ sudo lfs df -h
UUID                       bytes        Used   Available Use% Mounted on
silustre-MDT0000_UUID        3.8G        3.0M        3.8G   0% /silustre[MDT:0]
silustre-OST0001_UUID       86.0G        3.0M       86.0G   0% /silustre[OST:1]

filesystem_summary:        86.0G        3.0M       86.0G   0% /silustre


[estrella@lamella_t1 silustre]$ sudo setfacl -m u:estrella:rwx /silustre/

[estrella@lamella_t1 silustre]$ touch anythinggoes.md

[estrella@lamella_t1 silustre]$ ls -lah
total 12K
drwxrwxr-x+  3 root     root     12K Jun 21 13:41 .
dr-xr-xr-x. 18 root     root     240 Jun 21 13:28 ..
-rw-rw-r--.  1 estrella estrella  33 Jun 21 13:41 anythinggoes.md


# add lustre mount to filesystem table 

[estrella@lamella_t1 silustre]$ sudo vim /etc/fstab 

# insert this line

10.100.180.2@tcp0:/silustre	/silustre	lustre	defaults,_netdev	0 0


# NOTE: when unmounting the lustre file share, make sure you are OUT of its shared directories!

[estrella@lamella_t1 silustre]$ sudo umount /silustre/
umount: /silustre: target is busy.
        (In some cases useful info about processes that use
         the device is found by lsof(8) or fuser(1))
[estrella@lamella_t1 silustre]$ cd 
[estrella@lamella_t1 ~]$ 
[estrella@lamella_t1 ~]$ sudo umount /silustre/   # unmount success!

```

- Check out the following troubleshooting/management commands 

```
lctl dl # see all connected lustre servers/devices (clients see all, lustre servers themselves see partial; what they are connected to) 

lctl list_nids  # see WHICH INTERFACE the CLIENT is using to connect to lustre servers

lfs df -h # see all storage targets (MDT & OST's)  VERY HELPFUL in Monitoring 

lfs check servers  # see connected servers (management servers will not show)

lfs mdts/osts/  # check specific MDTS or OSTS   VERY HELPFUL in Monitoring

lfs find .. (a faster look up?)

```

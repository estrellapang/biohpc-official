## BioHPC Mock Update Part II: Actual Update Procedures

### Pre-Steps

```
cd /var/www/html/nextcloud_15/

sudo -u apache php occ maintenance:mode --on
Maintenance mode enabled

sudo systemctl stop httpd
sudo systemctl stop mysqld
sudo systemctl disable mysqld

```
 
### System Update: CentOS 7_6 --> 7_7

```
sudo yum check-update

GeoIP.x86_64                                        1.5.0-14.el7                                   base   
NetworkManager.x86_64                               1:1.18.0-5.el7_7.1                             updates
NetworkManager-libnm.x86_64                         1:1.18.0-5.el7_7.1                             updates
NetworkManager-team.x86_64                          1:1.18.0-5.el7_7.1                             updates
NetworkManager-tui.x86_64                           1:1.18.0-5.el7_7.1                             updates
alsa-lib.x86_64                                     1.1.8-1.el7                                    base   
bash.x86_64                                         4.2.46-33.el7                                  base   
bind-libs-lite.x86_64                               32:9.11.4-9.P2.el7                             base   
bind-license.noarch                                 32:9.11.4-9.P2.el7                             base   
binutils.x86_64                                     2.27-41.base.el7_7.1                           updates
biosdevname.x86_64                                  0.7.3-2.el7                                    base   
ca-certificates.noarch                              2019.2.32-76.el7_7                             updates
centos-release.x86_64                               7-7.1908.0.el7.centos                          base   
coreutils.x86_64                                    8.22-24.el7                                    base   
cronie.x86_64                                       1.4.11-23.el7                                  base   
cronie-anacron.x86_64                               1.4.11-23.el7                                  base   
cryptsetup-libs.x86_64                              2.0.3-5.el7                                    base   
curl.x86_64                                         7.29.0-54.el7_7.1                              updates
dbus.x86_64                                         1:1.10.24-13.el7_6                             base   
dbus-libs.x86_64                                    1:1.10.24-13.el7_6                             base   
device-mapper.x86_64                                7:1.02.158-2.el7_7.2                           updates
device-mapper-event.x86_64                          7:1.02.158-2.el7_7.2                           updates
device-mapper-event-libs.x86_64                     7:1.02.158-2.el7_7.2                           updates
device-mapper-libs.x86_64                           7:1.02.158-2.el7_7.2                           updates
device-mapper-persistent-data.x86_64                0.8.5-1.el7                                    base   
dhclient.x86_64                                     12:4.2.5-77.el7.centos                         base   
dhcp-common.x86_64                                  12:4.2.5-77.el7.centos                         base   
dhcp-libs.x86_64                                    12:4.2.5-77.el7.centos                         base   
diffutils.x86_64                                    3.3-5.el7                                      base   
dmidecode.x86_64                                    1:3.2-3.el7                                    base   
dracut.x86_64                                       033-564.el7                                    base   
dracut-config-rescue.x86_64                         033-564.el7                                    base   
dracut-network.x86_64                               033-564.el7                                    base   
e2fsprogs.x86_64                                    1.42.9-16.el7                                  base   
e2fsprogs-libs.x86_64                               1.42.9-16.el7                                  base   
elfutils-default-yama-scope.noarch                  0.176-2.el7                                    base   
elfutils-libelf.x86_64                              0.176-2.el7                                    base   
elfutils-libs.x86_64                                0.176-2.el7                                    base   
epel-release.noarch                                 7-12                                           epel   
ethtool.x86_64                                      2:4.8-10.el7                                   base   
firewalld.noarch                                    0.6.3-2.el7_7.2                                updates
firewalld-filesystem.noarch                         0.6.3-2.el7_7.2                                updates
freetype.x86_64                                     2.8-14.el7                                     base   
glib2.x86_64                                        2.56.1-5.el7                                   base   
glibc.x86_64                                        2.17-292.el7                                   base   
glibc-common.x86_64                                 2.17-292.el7                                   base   
grub2.x86_64                                        1:2.02-0.80.el7.centos                         base   
grub2-common.noarch                                 1:2.02-0.80.el7.centos                         base   
grub2-pc.x86_64                                     1:2.02-0.80.el7.centos                         base   
grub2-pc-modules.noarch                             1:2.02-0.80.el7.centos                         base   
grub2-tools.x86_64                                  1:2.02-0.80.el7.centos                         base   
grub2-tools-extra.x86_64                            1:2.02-0.80.el7.centos                         base   
grub2-tools-minimal.x86_64                          1:2.02-0.80.el7.centos                         base   
grubby.x86_64                                       8.28-26.el7                                    base   
hostname.x86_64                                     3.13-3.el7_7.1                                 updates
hwdata.x86_64                                       0.252-9.3.el7                                  base   
initscripts.x86_64                                  9.49.47-1.el7                                  base   
iproute.x86_64                                      4.11.0-25.el7_7.2                              updates
iprutils.x86_64                                     2.4.17.1-2.el7                                 base   
ipset.x86_64                                        7.1-1.el7                                      base   
ipset-libs.x86_64                                   7.1-1.el7                                      base   
iptables.x86_64                                     1.4.21-33.el7                                  base   
irqbalance.x86_64                                   3:1.0.7-12.el7                                 base   
iwl100-firmware.noarch                              39.31.5.1-72.el7                               base   
iwl1000-firmware.noarch                             1:39.31.5.1-72.el7                             base   
iwl105-firmware.noarch                              18.168.6.1-72.el7                              base   
iwl135-firmware.noarch                              18.168.6.1-72.el7                              base   
iwl2000-firmware.noarch                             18.168.6.1-72.el7                              base   
iwl2030-firmware.noarch                             18.168.6.1-72.el7                              base   
iwl3160-firmware.noarch                             22.0.7.0-72.el7                                base   
iwl3945-firmware.noarch                             15.32.2.9-72.el7                               base   
iwl4965-firmware.noarch                             228.61.2.24-72.el7                             base   
iwl5000-firmware.noarch                             8.83.5.1_1-72.el7                              base   
iwl5150-firmware.noarch                             8.24.2.2-72.el7                                base   
iwl6000-firmware.noarch                             9.221.4.1-72.el7                               base   
iwl6000g2a-firmware.noarch                          17.168.5.3-72.el7                              base   
iwl6000g2b-firmware.noarch                          17.168.5.2-72.el7                              base   
iwl6050-firmware.noarch                             41.28.5.1-72.el7                               base   
iwl7260-firmware.noarch                             22.0.7.0-72.el7                                base   
iwl7265-firmware.noarch                             22.0.7.0-72.el7                                base   
kernel.x86_64                                       3.10.0-1062.9.1.el7                            updates
kernel-tools.x86_64                                 3.10.0-1062.9.1.el7                            updates
kernel-tools-libs.x86_64                            3.10.0-1062.9.1.el7                            updates
kexec-tools.x86_64                                  2.0.15-33.el7                                  base   
kmod.x86_64                                         20-25.el7                                      base   
kmod-libs.x86_64                                    20-25.el7                                      base   
kpartx.x86_64                                       0.4.9-127.el7                                  base   
krb5-libs.x86_64                                    1.15.1-37.el7_7.2                              updates
libblkid.x86_64                                     2.23.2-61.el7_7.1                              updates
libcap.x86_64                                       2.22-10.el7                                    base   
libcom_err.x86_64                                   1.42.9-16.el7                                  base   
libcurl.x86_64                                      7.29.0-54.el7_7.1                              updates
libdb.x86_64                                        5.3.21-25.el7                                  base   
libdb-utils.x86_64                                  5.3.21-25.el7                                  base   
libdrm.x86_64                                       2.4.97-2.el7                                   base   
libgcc.x86_64                                       4.8.5-39.el7                                   base   
libgomp.x86_64                                      4.8.5-39.el7                                   base   
libmount.x86_64                                     2.23.2-61.el7_7.1                              updates
libndp.x86_64                                       1.2-9.el7                                      base   
libsmartcols.x86_64                                 2.23.2-61.el7_7.1                              updates
libss.x86_64                                        1.42.9-16.el7                                  base   
libssh2.x86_64                                      1.8.0-3.el7                                    base   
libstdc++.x86_64                                    4.8.5-39.el7                                   base   
libteam.x86_64                                      1.27-9.el7                                     base   
libuuid.x86_64                                      2.23.2-61.el7_7.1                              updates
linux-firmware.noarch                               20190429-72.gitddde598.el7                     base   
lshw.x86_64                                         B.02.18-13.el7                                 base   
lvm2.x86_64                                         7:2.02.185-2.el7_7.2                           updates
lvm2-libs.x86_64                                    7:2.02.185-2.el7_7.2                           updates
lz4.x86_64                                          1.7.5-3.el7                                    base   
make.x86_64                                         1:3.82-24.el7                                  base   
microcode_ctl.x86_64                                2:2.1-53.7.el7_7                               updates
nspr.x86_64                                         4.21.0-1.el7                                   base   
nss.x86_64                                          3.44.0-7.el7_7                                 updates
nss-pem.x86_64                                      1.0.3-7.el7                                    base   
nss-softokn.x86_64                                  3.44.0-8.el7_7                                 updates
nss-softokn-freebl.x86_64                           3.44.0-8.el7_7                                 updates
nss-sysinit.x86_64                                  3.44.0-7.el7_7                                 updates
nss-tools.x86_64                                    3.44.0-7.el7_7                                 updates
nss-util.x86_64                                     3.44.0-4.el7_7                                 updates
numactl-libs.x86_64                                 2.0.12-3.el7_7.1                               updates
openldap.x86_64                                     2.4.44-21.el7_6                                base   
openssh.x86_64                                      7.4p1-21.el7                                   base   
openssh-clients.x86_64                              7.4p1-21.el7                                   base   
openssh-server.x86_64                               7.4p1-21.el7                                   base   
openssl.x86_64                                      1:1.0.2k-19.el7                                base   
openssl-libs.x86_64                                 1:1.0.2k-19.el7                                base   
parted.x86_64                                       3.1-31.el7                                     base   
passwd.x86_64                                       0.79-5.el7                                     base   
plymouth.x86_64                                     0.8.9-0.32.20140113.el7.centos                 base   
plymouth-core-libs.x86_64                           0.8.9-0.32.20140113.el7.centos                 base   
plymouth-scripts.x86_64                             0.8.9-0.32.20140113.el7.centos                 base   
polkit.x86_64                                       0.112-22.el7_7.1                               updates
procps-ng.x86_64                                    3.3.10-26.el7_7.1                              updates
python.x86_64                                       2.7.5-86.el7                                   base   
python-firewall.noarch                              0.6.3-2.el7_7.2                                updates
python-libs.x86_64                                  2.7.5-86.el7                                   base   
python-linux-procfs.noarch                          0.4.11-4.el7                                   base   
python-perf.x86_64                                  3.10.0-1062.9.1.el7                            updates
qemu-guest-agent.x86_64                             10:2.12.0-3.el7                                base   
readline.x86_64                                     6.2-11.el7                                     base   
rpm.x86_64                                          4.11.3-40.el7                                  base   
rpm-build-libs.x86_64                               4.11.3-40.el7                                  base   
rpm-libs.x86_64                                     4.11.3-40.el7                                  base   
rpm-python.x86_64                                   4.11.3-40.el7                                  base   
rsyslog.x86_64                                      8.24.0-41.el7_7.2                              updates
selinux-policy.noarch                               3.13.1-252.el7_7.6                             updates
selinux-policy-targeted.noarch                      3.13.1-252.el7_7.6                             updates
sg3_utils.x86_64                                    1.37-18.el7_7.1                                updates
sg3_utils-libs.x86_64                               1.37-18.el7_7.1                                updates
shadow-utils.x86_64                                 2:4.6-5.el7                                    base   
sudo.x86_64                                         1.8.23-4.el7_7.1                               updates
teamd.x86_64                                        1.27-9.el7                                     base   
tuned.noarch                                        2.11.0-5.el7_7.1                               updates
tzdata.noarch                                       2019c-1.el7                                    updates
util-linux.x86_64                                   2.23.2-61.el7_7.1                              updates
vim-minimal.x86_64                                  2:7.4.629-6.el7                                base   
xfsprogs.x86_64                                     4.5.0-20.el7                                   base   
yum.noarch                                          3.4.3-163.el7.centos                           base   
yum-plugin-fastestmirror.noarch                     1.1.31-52.el7                                  base   


### Perform OS Update

sudo yum update

sudo reboot

```

\
\
\

### Update PHP Packages

```
sudo yum remove php*

Removed:
  php71w-cli.x86_64 0:7.1.33-1.w7                    php71w-common.x86_64 0:7.1.33-1.w7                   
  php71w-gd.x86_64 0:7.1.33-1.w7                     php71w-intl.x86_64 0:7.1.33-1.w7                     
  php71w-ldap.x86_64 0:7.1.33-1.w7                   php71w-mbstring.x86_64 0:7.1.33-1.w7                 
  php71w-mysql.x86_64 0:7.1.33-1.w7                  php71w-opcache.x86_64 0:7.1.33-1.w7                  
  php71w-pdo.x86_64 0:7.1.33-1.w7                    php71w-pear.noarch 1:1.10.4-1.w7                     
  php71w-pecl-apcu.x86_64 0:5.1.9-1.w7               php71w-pecl-imagick.x86_64 0:3.4.3-1.w7              
  php71w-process.x86_64 0:7.1.33-1.w7                php71w-xml.x86_64 0:7.1.33-1.w7                      

sudo yum remove mod_php71w


sudo yum install php72w-cli php72w-common  php72w-gd  php72w-intl php72w-ldap php72w-mbstring php72w-mysql php72w-opcache php72w-pdo php72w-pear php72w-pecl-apcu php72w-pecl-imagick php72w-process php72w-xml mod_php72w

Installed:
  mod_php72w.x86_64 0:7.2.24-1.w7                        php72w-cli.x86_64 0:7.2.24-1.w7                  
  php72w-common.x86_64 0:7.2.24-1.w7                     php72w-gd.x86_64 0:7.2.24-1.w7                   
  php72w-intl.x86_64 0:7.2.24-1.w7                       php72w-ldap.x86_64 0:7.2.24-1.w7                 
  php72w-mbstring.x86_64 0:7.2.24-1.w7                   php72w-mysql.x86_64 0:7.2.24-1.w7                
  php72w-opcache.x86_64 0:7.2.24-1.w7                    php72w-pdo.x86_64 0:7.2.24-1.w7                  
  php72w-pear.noarch 1:1.10.4-1.w7                       php72w-pecl-apcu.x86_64 0:5.1.9-1.w7             
  php72w-pecl-imagick.x86_64 0:3.4.3-1.2.w7              php72w-process.x86_64 0:7.2.24-1.w7              
  php72w-xml.x86_64 0:7.2.24-1.w7                       

Dependency Installed:
  libargon2.x86_64 0:20161029-3.el7

php -v

PHP 7.2.24 (cli) (built: Oct 26 2019 12:28:19) ( NTS )
Copyright (c) 1997-2018 The PHP Group
Zend Engine v3.2.0, Copyright (c) 1998-2018 Zend Technologies
    with Zend OPcache v7.2.24, Copyright (c) 1999-2018, by Zend Technologies


sudo vim /etc/php.ini
  # change `memory_limit` to `1028M`

  # look up optimal settings

```

\
\
\

### Backups

- critical components:

  - [basic link](https://docs.nextcloud.com/server/16/admin_manual/maintenance/backup.html)

```
    The config folder
    The data folder
    The theme folder
    The database

```
- backup entire directory:  

```
sudo tar -cpzvf /backup/nc_15_backup /var/www/html/nextcloud_15/ 

  ## is the `-v` verbose option needed? We can Pass or `| tee -a` to a output file!

```

- rename old NC folder (later change httpd configs)

```
sudo mv /var/www/html/nextcloud_15/ /var/www/html/nextcloud_15_old/
```

\
\
\

### NC Update to 16

 - [NC Download Link for ALL Versions](https://nextcloud.com/changelog/)

```
### Download NC 16

yum install wget

cd /var/www/html/

wget https://download.nextcloud.com/server/releases/nextcloud-16.0.7.tar.bz2

ls -lah 
---------------------------------------------------
ls -lah
total 60M
drwxr-xr-x.  3 root   root     62 Jan  7 06:21 .
drwxr-xr-x.  4 root   root     33 Jan  6 11:50 ..
drwxr-xr-x. 14 apache apache 4.0K Dec 27 19:06 nextcloud_15_old
-rw-r--r--   1 root   root    60M Dec 19 02:36 nextcloud-16.0.7.tar.bz2
---------------------------------------------------

tar -xvjf /var/www/html/nextcloud-16.0.7.tar.bz2

chown -R apache:apache /var/www/html/nextcloud

sudo find /var/www/html/nextcloud/ -type d -exec chmod 750 {} \;

sudo find /var/www/html/nextcloud/ -type f -exec chmod 640 {} \;

cp -p /var/www/html/nextcloud_15_old/config/config.php /var/www/html/nextcloud/config/

/etc/httpd/conf/httpd.conf
DocumentRoot "/var/www/html/nextcloud"

sudo systemctl start mysqld

sudo systemctl start httpd

cd /var/www/html/nextcloud

sudo -u apache php occ upgrade

----------------------------------------------------
Set log level to debug
Updating database schema
Updated database
Disabled incompatible app: sharerenamer
Updating <accessibility> ...
Updated <accessibility> to 1.2.0
Updating <federatedfilesharing> ...
Updated <federatedfilesharing> to 1.6.0
Updating <files_pdfviewer> ...
Updated <files_pdfviewer> to 1.5.0
Updating <files_texteditor> ...
Updated <files_texteditor> to 2.8.0
Updating <files_videoplayer> ...
Updated <files_videoplayer> to 1.5.0
Updating <gallery> ...
Updated <gallery> to 18.3.0
Updating <logreader> ...
Updated <logreader> to 2.1.0
Updating <password_policy> ...
Updated <password_policy> to 1.6.0
Updating <provisioning_api> ...
Updated <provisioning_api> to 1.6.0
Updating <serverinfo> ...
Updated <serverinfo> to 1.6.0
Updating <survey_client> ...
Updated <survey_client> to 1.4.0
Updating <twofactor_backupcodes> ...
Updated <twofactor_backupcodes> to 1.5.0
Updating <lookup_server_connector> ...
Updated <lookup_server_connector> to 1.4.0
Updating <oauth2> ...
Updated <oauth2> to 1.4.2
Updating <user_ldap> ...
Updated <user_ldap> to 1.6.0
Updating <files> ...
Updated <files> to 1.11.0
Updating <activity> ...
Updated <activity> to 2.9.1
Updating <cloud_federation_api> ...
Updated <cloud_federation_api> to 0.2.0
Updating <dav> ...
Fix broken values of calendar objects

 Done
    0/0 [>---------------------------]   0%
Updated <dav> to 1.9.2
Updating <files_sharing> ...
Updated <files_sharing> to 1.8.0
Updating <files_versions> ...
Updated <files_versions> to 1.9.0
Updating <sharebymail> ...
Updated <sharebymail> to 1.6.0
Updating <workflowengine> ...
Updated <workflowengine> to 1.6.0
Updating <admin_audit> ...
Updated <admin_audit> to 1.6.0
Updating <comments> ...
Updated <comments> to 1.6.0
Updating <nextcloud_announcements> ...
Updated <nextcloud_announcements> to 1.5.0
Updating <systemtags> ...
Updated <systemtags> to 1.6.0
Updating <theming> ...
Updated <theming> to 1.7.0
Checking for update of app accessibility in appstore
Checked for update of app "accessibility" in appstore 
Checking for update of app activity in appstore
Checked for update of app "activity" in appstore 
Checking for update of app admin_audit in appstore
Checked for update of app "admin_audit" in appstore 
Checking for update of app cloud_federation_api in appstore
Checked for update of app "cloud_federation_api" in appstore 
Checking for update of app comments in appstore
Checked for update of app "comments" in appstore 
Checking for update of app dav in appstore
Checked for update of app "dav" in appstore 
Checking for update of app federatedfilesharing in appstore
Checked for update of app "federatedfilesharing" in appstore 
Checking for update of app files in appstore
Checked for update of app "files" in appstore 
Checking for update of app files_pdfviewer in appstore
Checked for update of app "files_pdfviewer" in appstore 
Checking for update of app files_sharing in appstore
Checked for update of app "files_sharing" in appstore 
Checking for update of app files_texteditor in appstore
Checked for update of app "files_texteditor" in appstore 
Checking for update of app files_versions in appstore
Checked for update of app "files_versions" in appstore 
Checking for update of app files_videoplayer in appstore
Checked for update of app "files_videoplayer" in appstore 
Checking for update of app gallery in appstore
Checked for update of app "gallery" in appstore 
Checking for update of app logreader in appstore
Checked for update of app "logreader" in appstore 
Checking for update of app lookup_server_connector in appstore
Checked for update of app "lookup_server_connector" in appstore 
Checking for update of app nextcloud_announcements in appstore
Checked for update of app "nextcloud_announcements" in appstore 
Checking for update of app oauth2 in appstore
Checked for update of app "oauth2" in appstore 
Checking for update of app password_policy in appstore
Checked for update of app "password_policy" in appstore 
Checking for update of app provisioning_api in appstore
Checked for update of app "provisioning_api" in appstore 
Checking for update of app serverinfo in appstore
Checked for update of app "serverinfo" in appstore 
Checking for update of app sharebymail in appstore
Checked for update of app "sharebymail" in appstore 
Checking for update of app support in appstore
Checked for update of app "support" in appstore 
Checking for update of app survey_client in appstore
Checked for update of app "survey_client" in appstore 
Checking for update of app systemtags in appstore
Checked for update of app "systemtags" in appstore 
Checking for update of app theming in appstore
Checked for update of app "theming" in appstore 
Checking for update of app twofactor_backupcodes in appstore
Checked for update of app "twofactor_backupcodes" in appstore 
Checking for update of app user_ldap in appstore
Checked for update of app "user_ldap" in appstore 
Checking for update of app workflowengine in appstore
Checked for update of app "workflowengine" in appstore 
Checking for update of app sharerenamer in appstore
Checked for update of app "sharerenamer" in appstore 
Starting code integrity check...
Finished code integrity check
Update successful
Maintenance mode is kept active
Reset log level
----------------------------------------------------

vim config/config.php   ### check if configs are correct

sudo -u apache php occ maintenance:mode --off
Maintenance mode disabled

```

### NC Update to 17

```
----------------------------------------------------
sudo -u apache php occ upgrade
Nextcloud or one of the apps require upgrade - only a limited number of commands are available
You may use your browser or the occ upgrade command to do the upgrade
Set log level to debug
Updating database schema
Updated database
Disabled incompatible app: files_texteditor
Updating <accessibility> ...
Updated <accessibility> to 1.3.0
Updating <federatedfilesharing> ...
Updated <federatedfilesharing> to 1.7.0
Updating <files_pdfviewer> ...
Updated <files_pdfviewer> to 1.6.0
Updating <files_rightclick> ...
Updated <files_rightclick> to 0.14.2
Updating <files_videoplayer> ...
Updated <files_videoplayer> to 1.6.0
Updating <gallery> ...
Updated <gallery> to 18.4.0
Updating <logreader> ...
Updated <logreader> to 2.2.0
Updating <password_policy> ...
Updated <password_policy> to 1.7.0
Updating <privacy> ...
Updated <privacy> to 1.1.0
Updating <provisioning_api> ...
Updated <provisioning_api> to 1.7.0
Updating <recommendations> ...
Updated <recommendations> to 0.5.0
Updating <serverinfo> ...
Updated <serverinfo> to 1.7.0
Updating <support> ...
Updated <support> to 1.0.1
Updating <survey_client> ...
Updated <survey_client> to 1.5.0
Updating <twofactor_backupcodes> ...
Updated <twofactor_backupcodes> to 1.6.0
Updating <lookup_server_connector> ...
Updated <lookup_server_connector> to 1.5.0
Updating <oauth2> ...
Updated <oauth2> to 1.5.0
Updating <user_ldap> ...
Updated <user_ldap> to 1.7.0
Updating <files> ...
Updated <files> to 1.12.0
Updating <activity> ...
Updated <activity> to 2.10.1
Updating <cloud_federation_api> ...
Updated <cloud_federation_api> to 1.0.0
Updating <dav> ...
Fix broken values of calendar objects

 Done
    0/0 [>---------------------------]   0%
Updated <dav> to 1.13.0
Updating <files_sharing> ...
Updated <files_sharing> to 1.9.0
Updating <files_versions> ...
Updated <files_versions> to 1.10.0
Updating <sharebymail> ...
Updated <sharebymail> to 1.7.0
Updating <workflowengine> ...
Updated <workflowengine> to 1.7.0
Updating <admin_audit> ...
Updated <admin_audit> to 1.7.0
Updating <comments> ...
Updated <comments> to 1.7.0
Updating <nextcloud_announcements> ...
Updated <nextcloud_announcements> to 1.6.0
Updating <systemtags> ...
Updated <systemtags> to 1.7.0
Updating <theming> ...
Updated <theming> to 1.8.0
Checking for update of app accessibility in appstore
Checked for update of app "accessibility" in appstore 
Checking for update of app activity in appstore
Checked for update of app "activity" in appstore 
Checking for update of app admin_audit in appstore
Checked for update of app "admin_audit" in appstore 
Checking for update of app cloud_federation_api in appstore
Checked for update of app "cloud_federation_api" in appstore 
Checking for update of app comments in appstore
Checked for update of app "comments" in appstore 
Checking for update of app dav in appstore
Checked for update of app "dav" in appstore 
Checking for update of app federatedfilesharing in appstore
Checked for update of app "federatedfilesharing" in appstore 
Checking for update of app files in appstore
Checked for update of app "files" in appstore 
Checking for update of app files_pdfviewer in appstore
Checked for update of app "files_pdfviewer" in appstore 
Checking for update of app files_rightclick in appstore
Checked for update of app "files_rightclick" in appstore 
Checking for update of app files_sharing in appstore
Checked for update of app "files_sharing" in appstore 
Checking for update of app files_versions in appstore
Checked for update of app "files_versions" in appstore 
Checking for update of app files_videoplayer in appstore
Checked for update of app "files_videoplayer" in appstore 
Checking for update of app gallery in appstore
Checked for update of app "gallery" in appstore 
Checking for update of app logreader in appstore
Checked for update of app "logreader" in appstore 
Checking for update of app lookup_server_connector in appstore
Checked for update of app "lookup_server_connector" in appstore 
Checking for update of app nextcloud_announcements in appstore
Checked for update of app "nextcloud_announcements" in appstore 
Checking for update of app oauth2 in appstore
Checked for update of app "oauth2" in appstore 
Checking for update of app password_policy in appstore
Checked for update of app "password_policy" in appstore 
Checking for update of app privacy in appstore
Checked for update of app "privacy" in appstore 
Checking for update of app provisioning_api in appstore
Checked for update of app "provisioning_api" in appstore 
Checking for update of app recommendations in appstore
Checked for update of app "recommendations" in appstore 
Checking for update of app serverinfo in appstore
Checked for update of app "serverinfo" in appstore 
Checking for update of app sharebymail in appstore
Checked for update of app "sharebymail" in appstore 
Checking for update of app support in appstore
Checked for update of app "support" in appstore 
Checking for update of app survey_client in appstore
Checked for update of app "survey_client" in appstore 
Checking for update of app systemtags in appstore
Checked for update of app "systemtags" in appstore 
Checking for update of app theming in appstore
Checked for update of app "theming" in appstore 
Checking for update of app twofactor_backupcodes in appstore
Checked for update of app "twofactor_backupcodes" in appstore 
Checking for update of app user_ldap in appstore
Checked for update of app "user_ldap" in appstore 
Checking for update of app viewer in appstore
Checked for update of app "viewer" in appstore 
Checking for update of app workflowengine in appstore
Checked for update of app "workflowengine" in appstore 
Checking for update of app files_texteditor in appstore
Checked for update of app "files_texteditor" in appstore 
Starting code integrity check...
Finished code integrity check
Update successful
Maintenance mode is kept active
Reset log level
----------------------------------------------------

```

- **Trial NOTES**:

```
# ONCE NC is Updated, the DATABASE WILL NOT BE BACKWARDS COMPATIBLE! 

 # We must STAY WITH INITIAL SQL DUMP and carry it all the way through!

 # Alternative: just rsync all data files, forget the database sync

 # ACTUALLY: one needs to drop previous database (old import), make a new database template, then import the changed database 

```

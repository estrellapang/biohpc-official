## Lamella Test Deployment Part IV: Samba-LDAP Authentication & CTDB 

- Machines Operated On: 

  - "lamella_t1.biohpc.swmed.edu" 

  - "lamella_t2.biohpc.swmed.edu" 

    - both of which are lustre clients  

- Networks: 

  - 10.100.180.0/24  (intra-cluster network) 

  - 192.168.56.20 & 21  (server-client network) 

### Samba Servers 

```

# Install basic Samba packages

[estrella@lamella_t1 ~]$ sudo yum install samba samba-client samba-common 
============================================================================================================================================
 Package                                 Arch                        Version                                Repository                 Size
============================================================================================================================================
Installing:
 samba                                   x86_64                      4.8.3-4.el7                            base                      680 k
 samba-client                            x86_64                      4.8.3-4.el7                            base                      618 k
Installing for dependencies:
 libarchive                              x86_64                      3.1.2-10.el7_2                         base                      318 k
 libsmbclient                            x86_64                      4.8.3-4.el7                            base                      134 k
 pytalloc                                x86_64                      2.1.13-1.el7                           base                       17 k
 samba-common-tools                      x86_64                      4.8.3-4.el7                            base                      448 k
 samba-libs                              x86_64                      4.8.3-4.el7                            base                      276 k

Transaction Summary
============================================================================================================================================
Install  2 Packages (+5 Dependent packages)

Installed:
  samba.x86_64 0:4.8.3-4.el7                                        samba-client.x86_64 0:4.8.3-4.el7                                       

Dependency Installed:
  libarchive.x86_64 0:3.1.2-10.el7_2                 libsmbclient.x86_64 0:4.8.3-4.el7            pytalloc.x86_64 0:2.1.13-1.el7           
  samba-common-tools.x86_64 0:4.8.3-4.el7            samba-libs.x86_64 0:4.8.3-4.el7             

Complete!


[estrella@lamella_t1 ~]$ sudo firewall-cmd --permanent --zone=public --add-service=samba
success
[estrella@lamella_t1 ~]$ 
[estrella@lamella_t1 ~]$ sudo firewall-cmd --reload
success


# Install Packages for Integrating with LDAP

sudo yum install smbldap-tools openldap-clients authconfig-gtk nss-pam-ldapd

Transaction Summary
============================================================================================================================================
Install  3 Packages (+99 Dependent packages)

Total download size: 15 M
Installed size: 49 M
Is this ok [y/d/N]:y 

Installed:                             # plus +100 dependencies
  authconfig-gtk.x86_64 0:6.2.8-30.el7         nss-pam-ldapd.x86_64 0:0.8.13-16.el7_6.1         smbldap-tools.noarch 0:0.9.11-6.el7

# TROUBLESHOOT: the openldap-client was not installed, the correct package name is

   # `openldap-clients`  --> provided by `yum search openldap` 


[estrella@lamella_t1 cacerts]$ sudo yum install openldap-clients

Dependencies Resolved

============================================================================================================================================
 Package                               Arch                        Version                               Repository                    Size
============================================================================================================================================
Installing:
 openldap-clients                      x86_64                      2.4.44-21.el7_6                       updates                      190 k

Transaction Summary
============================================================================================================================================
Install  1 Package

Total download size: 190 k
Installed size: 571 k
Is this ok [y/d/N]: y
Downloading packages:
openldap-clients-2.4.44-21.el7_6.x86_64.rpm                                                                          | 190 kB  00:00:00     
Running transaction check
Running transaction test
Transaction test succeeded
Running transaction
  Installing : openldap-clients-2.4.44-21.el7_6.x86_64                                                                                  1/1 
  Verifying  : openldap-clients-2.4.44-21.el7_6.x86_64                                                                                  1/1 

Installed:
  openldap-clients.x86_64 0:2.4.44-21.el7_6    

# secure copy certificate from LDAP server to the designated local directory:

    # first connected server to another host-only network (serving clients) - 192.168.56.0/24

    # assigned static IP to `lamella_t1`'s "enp0s8" interface (192.168.56.20)

    # added ldap.vms.train to `etc/hosts`


# Enable LDAP authentication via `authconfig` or `authconfig-tui`  ## GUI for authconfig 

`authconfig --enableldap --enableldapauth --ldapserver=ldap.vms.train --ldapbasedn="dc=ldap,dc=vms,dc=train" --enablemkhomedir --update` 

   # NOTE: `--enablemkhomedir` is not available on the GUI 

   # seems like `--enablemkhomedir` needs some sort of a package to work

   # Hmm, might need to install oddjob-mkhomedir 

# copy ldapserver's certificate over the local Lamella/samba server 

  # as root, because "estrella" does not exist on that server
 
[root@lamella_t1 ~]# scp -pr ldap.vms.train:/etc/openldap/certs/ldapserver1_cert.pem /etc/openldap/cacerts

The authenticity of host 'ldap.vms.train (192.168.56.6)' can't be established.
ECDSA key fingerprint is SHA256:6fhYiDUQunN8lls/0FVzOkVCiiZ6+O56Y9C6q44uMs0.
ECDSA key fingerprint is MD5:79:f9:e1:68:d8:5b:59:fe:f3:0c:ff:3b:bc:de:28:61.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'ldap.vms.train,192.168.56.6' (ECDSA) to the list of known hosts.
root@ldap.vms.train's password: 
ldapserver1_cert.pem                                                                                      100% 1155     1.8MB/s   00:00    


# Edit `/etc/samba/smb.conf` --> create a .bak file before doing this 

-----------------------------------------------------------------

# See smb.conf.example for a more detailed config file or
# read the smb.conf manpage.
# Run 'testparm' to verify the config is correct after
# you modified it.

[global]
        workgroup = WORKGROUP

        security = user

        netbios name = Lamella

        passdb backend = ldapsam:ldap://ldap.vms.train

        ldap suffix = dc=ldap,dc=vms,dc=train

        ldap admin dn = cn=rootadmin,dc=ldap,dc=vms,dc=train

        ldap passwd sync = Yes


[lustre]
        comment = file share mounted from lustre cluster

        path = /silustre

        valid users = @cloud_nine

        guest ok = No

        browsable = Yes

        writable = Yes

	ldap ssl = start tls

        create mask = 0750   # currently removed 

        directory mask = 0750   # currently removed 

        create mode = 0640   # currently removed 

        directory mode = 0750   # currently removed 

        inherit acls = Yes   # currently removed

-----------------------------------------------------------------

# In order for Samba server to access LDAP server's database, the latter's admin password has to be manually handed to the Samba server

[estrella@lamella_t1 cacerts]$ sudo smbpasswd -w 13579ZxP rootadmin
Setting stored password for "cn=rootadmin,dc=ldap,dc=vms,dc=train" in secrets.tdb

### Request LDAP server to assign Samba server an unique Samba Domain ID ("SID")

[estrella@lamella_t1 cacerts]$ sudo net getlocalsid
SID for domain LAMELLA is: S-1-5-21-439045346-4104710651-3900850888


### not sure if useful at all --> changing security context of shared directory? 

sudo chcon -t samba_share_t /silustre


### Misc. Note:

[estrella@lamella_t1 ~]$ sudo smbpasswd -a youngsensei
New SMB password:   13579ZxP  ??? Um if the user's NOT a Samba user then it's futile???
Retype new SMB password:   13579ZxP 

# Refer to /home/Practice on 192.168.56.6 (ldap server to see how users are Samba enabled!) 


#######  06/27/2019: Lamella LDAP-Client/Samba Server Config Cont'd #######

# Add ldap service to firewall-cmd

[estrella@lamella_t1 silustre]$ sudo firewall-cmd --add-service=ldap --permanent 
success

[estrella@lamella_t1 silustre]$ sudo firewall-cmd --reload
success

[estrella@lamella_t1 silustre]$ sudo firewall-cmd --list-services
ssh dhcpv6-client samba ldap


# LDAPTLS Setup

  ## Note: this would NOT work if the certificates have not been transferred to the client's "cacerts" directory, details listed above

sudo authconfig --enableldaptls --update

sudo ldapwhoami -H ldaps://ldap.vms.train -x   # output should be "anonymous"


# Create Groups for Samba Share

  ## all lamella users will temporarily fall under this GID = 2100 

  ## all Samba-LDAP users will be assigned this GID
  
[estrella@lamella_t1 silustre]$ sudo groupadd -g 2100 cloud_nine

[estrella@lamella_t1 silustre]$ sudo cat /etc/group | grep cloud_nine
cloud_nine:x:2100:


# Temp: Manually added Lambella's home directory & assigned `cloud_nine` as its group owner

sudo mkdir /silustre/lambella

sudo chown root:cloud_nine /silustre/

sudo chmod 770 /silustre/    # NOTE: should probably be 750


# Assign samba share parameters

refer to `smb.conf` example above

# Start Related Services

[estrella@lamella_t1 cacerts]$ sudo systemctl restart nslcd
[estrella@lamella_t1 ~]$ sudo systemctl restart smb[estrella@mgs001 ~]$ sudo authconfig --enableldap --enableldapauth --ldapserver=ldap.vms.train --ldapbasedn="dc=ldap,dc=vms,dc=train" --enablemkhomedir --enableldaptls --update
[sudo] password for estrella: 
authconfig: Authentication module /usr/lib64/security/pam_ldap.so is missing. Authentication process might not work correctly.
[estrella@lamella_t1 ~]$ sudo systemctl restart nmb

# Test Connections to LDAP

getent passwd youngsensei

ldapsearch x uid=youngsensei


##### On LDAP Server ######

# Create OrganizationalUnit called "clouders", this is not needed, but only done for organizational purposes 

vi lamella1.ldif   # LDAP script, watch out for "Ghost spaces" in file, as they cause errors

---------------------------------------------------

dn: ou=clouders,o=biohpc,dc=ldap,dc=vms,dc=train
objectClass: organizationalUnit
ou: clouders
description: group dedicated to lamella access

---------------------------------------------------

# add ou to the LDAP directory tree

sudo ldapadd -x -W -D "cn=rootadmin,dc=ldap,dc=vms,dc=train" -f lambella.ldif

# Create a lamella user "lambella," under this ou: 

vim lambella.ldif

---------------------------------------------------

dn: uid=lambella,ou=clouders,o=biohpc,dc=ldap,dc=vms,dc=train
objectClass: inetOrgPerson
objectClass: posixAccount
objectClass: shadowAccount
objectClass: sambaSamAccount
ou: clouders
o: biohpc
cn: lambella
sn: lambella
uid: lambella
uidNumber: 1200
givenName: Lamb of Lamella
homeDirectory: /silustre/lambella
loginShell: /bin/bash
shadowMax: 9120
shadowWarning: 30
userPassword: {crypt}x
shadowLastChange: 18075   # `date +%s` then divide by 86400
shadowMin: 0
gidNumber: 2100
sambaDomainName: Lamella
sambaSID: S-1-5-21-439045346-4104710651-3900850888-1200   ###CRITICAL: last 4 digits are manually set, which is the UID of the user. Everything that comesbefore it are a part of the main Samba Server SID. ### NOTE: once again, if the unique last 4 digits are missing for the user/object, the server will fail to pick it up!!! 
sambaKickoffTime: 1700000000		# seconds since unix Epoch when user will be deactivated 
sambaPwdLastSet: 1561682451   # `date +%s`	# ~ user passwd was set/reset 

---------------------------------------------------


# Assign user password: 

sudo ldappasswd -s 13579ZxP -W -D "cn=rootadmin,dc=ldap,dc=vms,dc=train" -x "uid=lambella,ou=clouders,o=biohpc,dc=ldap,dc=vms,dc=train"


### BACK on LAMELLA/SAMBA Server ###

# Give LDAP-Samba User the Samba Password 

sudo smbpasswd -a lambella   # you will be prompted to enter & confirm the password 

# Verify whether LDAP-Samba user created is properly put into the Samba Domain

[estrella@lamella_t1 ~]$ sudo pdbedit -L -v
sid S-1-5-21-3576214011-3692339728-313591563-1015 does not belong to our domain   # this user is from another SID hosted by another standalone Samba server

# this is our user
---------------
Unix username:        lambella
NT username:          lambella
Account Flags:        [U          ]
User SID:             S-1-5-21-439045346-4104710651-3900850888-1200
Primary Group SID:    S-1-5-21-439045346-4104710651-3900850888-513
Full Name:            lambella
Home Directory:       \\lamella\lambella
HomeDir Drive:        
Logon Script:         
Profile Path:         \\lamella\lambella\profile
Domain:               LAMELLA
Account desc:         
Workstations:         
Munged dial:          
Logon time:           0
Logoff time:          never
Kickoff time:         Tue, 14 Nov 2023 16:13:20 CST
Password last set:    Thu, 27 Jun 2019 19:42:58 CDT
Password can change:  Thu, 27 Jun 2019 19:42:58 CDT
Password must change: never
Last bad password   : 0
Bad password count  : 0
Logon hours         : FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF


### Configure `/etc/smbldap-tools/smbldap_bind.conf` 

-------------------------------------------------------------------------------


# $Id$
#
############################
# Credential Configuration #
############################
# Notes: you can specify two differents configuration if you use a
# master ldap for writing access and a slave ldap server for reading access
# By default, we will use the same DN (so it will work for standard Samba
# release)
SID="S-1-5-21-439045346-4104710651-3900850888"
slaveDN="ldap.vms.train"
slavePort="636"
masterDN="ldap.vms.train"
masterPort="636"
ldapTLS="1"


### reverted to non-TLS setttings later: ports 389 
-------------------------------------------------------------------------------


### On MGS & MDS Servers

# CRITICAL: CONFIGURE LUSTRE MGS & MDS AS A OPENLDAP CLIENTS ###
 

[estrella@mgs001 ~]$ sudo yum install openldap-clients authconfig-gtk
Installed:
  authconfig-gtk.x86_64 0:6.2.8-30.el7                               openldap-clients.x86_64 0:2.4.44-21.el7_6                              

Dependency Installed:
  atk.x86_64 0:2.28.1-1.el7                      cairo.x86_64 0:1.15.12-3.el7                   dejavu-fonts-common.noarch 0:2.33-6.el7    
  dejavu-sans-fonts.noarch 0:2.33-6.el7          fontconfig.x86_64 0:2.13.0-4.3.el7             fontpackages-filesystem.noarch 0:1.44-8.el7
  fribidi.x86_64 0:1.0.2-1.el7                   gdk-pixbuf2.x86_64 0:2.36.12-3.el7             graphite2.x86_64 0:1.3.10-1.el7_3          
  gtk-update-icon-cache.x86_64 0:3.22.30-3.el7   gtk2.x86_64 0:2.24.31-1.el7                    harfbuzz.x86_64 0:1.7.5-2.el7              
  hicolor-icon-theme.noarch 0:0.12-7.el7         jasper-libs.x86_64 0:1.900.1-33.el7            jbigkit-libs.x86_64 0:2.0-11.el7           
  libICE.x86_64 0:1.0.9-9.el7                    libSM.x86_64 0:1.2.2-2.el7                     libX11.x86_64 0:1.6.5-2.el7                
  libX11-common.noarch 0:1.6.5-2.el7             libXau.x86_64 0:1.0.8-2.1.el7                  libXcomposite.x86_64 0:0.4.4-4.1.el7       
  libXcursor.x86_64 0:1.1.15-1.el7               libXdamage.x86_64 0:1.1.4-4.1.el7              libXext.x86_64 0:1.3.3-3.el7               
  libXfixes.x86_64 0:5.0.3-1.el7                 libXft.x86_64 0:2.3.2-2.el7                    libXi.x86_64 0:1.7.9-1.el7                 
  libXinerama.x86_64 0:1.1.3-2.1.el7             libXrandr.x86_64 0:1.5.1-2.el7                 libXrender.x86_64 0:0.9.10-1.el7           
  libXxf86vm.x86_64 0:1.1.4-1.el7                libglade2.x86_64 0:2.6.4-11.el7                libglvnd.x86_64 1:1.0.1-0.8.git5baa1e5.el7 
  libglvnd-egl.x86_64 1:1.0.1-0.8.git5baa1e5.el7 libglvnd-glx.x86_64 1:1.0.1-0.8.git5baa1e5.el7 libthai.x86_64 0:0.1.14-9.el7              
  libtiff.x86_64 0:4.0.3-27.el7_3                libwayland-client.x86_64 0:1.15.0-1.el7        libwayland-server.x86_64 0:1.15.0-1.el7    
  libxcb.x86_64 0:1.13-1.el7                     libxshmfence.x86_64 0:1.2-1.el7                mesa-libEGL.x86_64 0:18.0.5-4.el7_6        
  mesa-libGL.x86_64 0:18.0.5-4.el7_6             mesa-libgbm.x86_64 0:18.0.5-4.el7_6            mesa-libglapi.x86_64 0:18.0.5-4.el7_6      
  pango.x86_64 0:1.42.4-2.el7_6                  pixman.x86_64 0:0.34.0-1.el7                   pycairo.x86_64 0:1.8.10-8.el7              
  pygtk2.x86_64 0:2.24.0-9.el7                   pygtk2-libglade.x86_64 0:2.24.0-9.el7          startup-notification.x86_64 0:0.12-8.el7   
  usermode.x86_64 0:1.111-5.el7                  usermode-gtk.x86_64 0:1.111-5.el7              xcb-util.x86_64 0:0.4.0-2.el7              

Complete!


# open ports on firewall for ldap service: 

[estrella@mgs001 ~]$ sudo firewall-cmd --add-service=ldap,samba --permanent
success
[estrella@mgs001 ~]$ 
[estrella@mgs001 ~]$ sudo firewall-cmd --reload
success
[estrella@mgs001 ~]$ sudo firewall-cmd --list-services
ssh dhcpv6-client ntp ldap samba

# add `ldap.vms.train` (192.168.56.6) to `/etc/hosts` 

  ## NOTE: to mimic realistic scenarios, we need to add MGS001 to the 192.168.56.6 network!

# enable ldap authentication


[estrella@mgs001 ~]$ sudo authconfig --enableldap --enableldapauth --ldapserver=ldap.vms.train --ldapbasedn="dc=ldap,dc=vms,dc=train" --enablemkhomedir --enableldaptls --update
[sudo] password for estrella: 

### ERROR: a specific package is needed: `nss-pam-ldapd`
authconfig: Authentication module /usr/lib64/security/pam_ldap.so is missing. Authentication process might not work correctly.

sudo yum install nss-pam-ldapd 

# Retry 

[estrella@mgs001 ~]$ sudo authconfig --enableldap --enableldapauth --ldapserver=ldap.vms.train --ldapbasedn="dc=ldap,dc=vms,dc=train" --enablemkhomedir --enableldaptls --update 

# the `--enableldaptls` option will create the `/etc/openldap/cacerts` directory, in which you will secure copy your LDAP server's certificate file

sudo scp -pr ldap.vms.train:/etc/openldap/certs/ldapserver1_cert.pem /etc/openldap/cacerts 

# you will be asked to authenticate as root on the LDAP server 

# switch to root & pass the "allow TLS certificates" option on in the following configuration files  

echo "TLS_REQCERT allow" >> /etc/openldap/ldap.conf

echo "tls_reqcert allow" >> /etc/nslcd.conf

# restart related service

sudo systemctl start nslcd

sudo systemctl status nslcd

getent passwd lambella   # VERIFY 

### Repeat the following steps for MDS  ###
-------------------------------------------------------------------------------


####### Cont'd July 1st, 2019 #########

## Tentative Solution: Create a LDAP Posix Group Visible to All Lustre Nodes ##

# first delete the old `cloud_nine` local to lamella (& its `/etc/group` files)

# Error: lambella's gid = cloud_nine

[estrella@lamella_t1 ~]$ sudo groupdel cloud_nine
groupdel: cannot remove the primary group of user 'lambella'

# Solution: change lambella's gid in Apache ActiveDirectory to "2006" 

[estrella@lamella_t1 ~]$ getent passwd lambella
lambella:x:1200:2006:lambella:/silustre/lambella:/bin/bash
[estrella@lamella_t1 ~]$ 
[estrella@lamella_t1 ~]$ sudo groupdel cloud_nine   # delete successful


# changed group owner of /silustre to 2006, removed ACL for cloud_nine (gid=2100) 

[estrella@lamella_t1 ~]$ sudo setfacl -Rb /silustre/

[estrella@lamella_t1 ~]$ getfacl /silustre/
getfacl: Removing leading '/' from absolute path names
# file: silustre/
# owner: root
# group: 2100
user::rwx
group::rwx
other::---

[estrella@lamella_t1 ~]$ sudo chown root:2006 /silustre/

[estrella@lamella_t1 ~]$ sudo chown lambella:2006 /silustre/lambella

# this change is also refelcted in /etc/samba/smb.conf


# Create PosixGroup "oasis" 

-------------------------------------------------------------------------------
dn: cn=groupa,ou=regusers,o=biohpc,dc=ldap,dc=vms,dc=train
objectClass: posixGroup
cn: groupa
gidNumber: 3007
description: subgroup under ou=regusers
-------------------------------------------------------------------------------

[zxp8244@ldap Practice]$ sudo ldapadd -x -W -D "cn=rootadmin,dc=ldap,dc=vms,dc=train" -f LDAP_entries/oasis.ldif

# Verify: 

[zxp8244@ldap Practice]$ sudo ldapsearch -x cn=oasis -b dc=ldap,dc=vms,dc=train
# extended LDIF
#
# LDAPv3
# base <dc=ldap,dc=vms,dc=train> with scope subtree
# filter: cn=oasis
# requesting: ALL
#

# oasis, clouders, biohpc, ldap.vms.train
dn: cn=oasis,ou=clouders,o=biohpc,dc=ldap,dc=vms,dc=train
objectClass: posixGroup
cn: oasis
gidNumber: 2006
description: subgroup under ou=clouders; intended for lamella users

# search result
search: 2
result: 0 Success

# numResponses: 2
# numEntries: 1



# Verify if user lambella now belongs to the group created 

[estrella@lamella_t1 ~]$ id lambella
uid=1200(lambella) gid=2006(oasis) groups=2006(oasis)

# remove user from secondary group

sudo usermod -G group user 



########### July 2nd, 2019: Solution Found ############# 

### Solution Overview 

1. force smbd to bind to the correct subnet (192.168.56.0/24) 

2. disable NAT network adapter on Windows Client 

3. change SElinux mode to permissive 


### Solutio Details

1. [on samba server] sudo vim /etc/samba/smb.conf   # Add the following line

--------------------------------------------------------

        interfaces = 192.168.56.20
 
	# adding "hosts allow" option is also a good idea 

	# hosts allow = 192.168.56.0/24

--------------------------------------------------------

  # smb has a tendency to bind to whatever network interface available, by manually bind it to specific interfaces ensures that samba daemon always exports the file shares to the same set of networks available to the server 

2. shut down Windows VM, go to machine specific settings, & disconnect NAT adapter

  - the Windows machine was previously attached to the host-only network #.#.56.0/24 & assigned a static IP of 192.168.56.2 

  # when the Windows machine had both the NAT & host-only adapter enabled, its network manager was always confused when it came to mapping network drives, because the same samba share would be available from TWO SEPARATE networks (NAT & Host only)

3. [on samba server] sudo vim /etc/selinux/config

  - change "SELINUX=enforcing" to "SELINUX=permissive"

  - write & save --> `sudo setenforce 0`  

  # SElinux mode needs to be set to "permissive" when "interfaces" & "hosts allow" options are listed in `smb.conf`

  # TRY Adding the correct SElinux security context to the `/silustre` fileshare w/o having to set SElinux to permissive mode 

  	# [tutorial 1](https://www.tecmint.com/setup-samba-file-sharing-for-linux-windows-clients/) 

	# [tutortial 2](https://www.lisenet.com/2016/samba-server-on-rhel-7/)


### Additional Configs for Samba-LDAP authentication to work on shared LustreFS

# install & configure OpenLDAP client on BOTH the MGS & MDS 

  # or else we can't even load the right permissions on filesystem without MDS & MGS connected to LDAP! 

- Permanantly allow the following services via firewall-cmd (MGS, MDS, & OSS!) 

  - ldap + samba 

  - `sudo firewall-cmd --add-service=ldap --permanent` 

  - `sudo firewall-cmd --add-service=samba --permanent  

  - `sudo firewall-cmd --reload` 

  - `sudo firewall-cmd --list-services`


# [on Samba server] added mount lustre entry in `etc/fstab` 

------------------------------------------------------------------------------------

10.100.180.2@tcp0:/silustre	/silustre	lustre	defaults,_netdev,flock	0 0 

------------------------------------------------------------------------------------

#  "ldap passwd sync" option enabled in `smb.conf` 

  # makes things easier; if set to "no" then a single LDAP user will need 2 SEPARATE passwords between Linux & Window clients!


### System Mods & Additional Commands/Tricks on Lamella Server

# Network Issue with enp0s8 reverting to the default device name of "enp0s9" 

  # added "NM_CONTROLLED=no" to the network script 


# verify your samba clients locally 

  # syntax: smbclient -L <samba host IP> -U <Domain Name>/<User Name> -W <Workgroup Name>


[estrella@lamella_t1 ~]$ smbclient -L 192.168.56.20 -U lambella
Enter WORKGROUP\lambella's password: 

	Sharename       Type      Comment
	---------       ----      -------
	silustre        Disk      file share mounted from lustre cluster
	IPC$            IPC       IPC Service (Samba 4.8.3)
Reconnecting with SMB1 for workgroup listing.

	Server               Comment
	---------            -------

	Workgroup            Master
	---------            -------
	WORKGROUP            LAMELLA
[estrella@lamella_t1 ~]$ 
[estrella@lamella_t1 ~]$ 
[estrella@lamella_t1 ~]$ smbclient -L localhost -U lambella
Enter WORKGROUP\lambella's password: 

	Sharename       Type      Comment
	---------       ----      -------
	silustre        Disk      file share mounted from lustre cluster
	IPC$            IPC       IPC Service (Samba 4.8.3)
Reconnecting with SMB1 for workgroup listing.

	Server               Comment
	---------            -------

	Workgroup            Master
	---------            -------
	WORKGROUP            LAMELLA



# changed permissions of /silustre to 755

[estrella@lamella_t1 ~]$ ls -lah / | grep silustre
drwxr-xr-x.   4 root oasis  11K Jul  1 14:48 silustre

[estrella@lamella_t1 ~]$ ls -lah /silustre
total 27K
drwxr-xr-x. 4 root     oasis 11K Jul  1 14:48 .
drwxr-xr-x. 2 lambella oasis 11K Jul  2 12:07 lambella
-rw-r--r--. 1 root     root   25 Jun 27 13:55 text1.md

# temporarily disabled start TLS in: 

  - all LDAP & Samba config files in Lamella server

    - `/etc/samba/smb.conf`

    - `/etc/smbldap-tools/smbldap.conf`

    - `/etc/smbldap-tools/smbldap_bind.conf` (using port 389 instead of 636) 

  - on BOTH MGS & MDS


# If any of the Lustre VM's boot with the zpools gone --> shut down machine --> remove (not delete) virtual harddrive attachments --> reattach & reboot VM

# If a VM freezes & cannot shut down --> `ps -aux` --> locate right PID & `kill -9 <PID>` 


##### LDAP-Samba Configurations Complete ##### 

# Clone lamellta_t1 & test samba share 

  # assign cluster address of 10.100.180.6 

  # ~ server-client address of 192.168.56.21 

# Refer to `lamella_setup_2.md` for CTDB & HA proxy settings 

```

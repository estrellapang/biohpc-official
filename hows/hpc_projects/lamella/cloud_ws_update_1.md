## Official BioHPC Cloud Update Proposal: NCv15 --> NCv17  

### Pre-Downtime Preparations

- New EXSI VM:

  - on Production: create file share links for testing purposes (on BOTH local & ldap accounts) 

  - create new VM on ESXI Server (start with RHEL release 7.6)

  - disable SElinux

  - copy the following files to test server:

```
/etc/hosts
/etc/resolv.conf
/etc/sysconfig/network
/etc/profile.d/proxy.sh
/etc/yum.conf/
/etc/yum.repos.d/
/etc/sysconfig/network-scripts/ifcfg-<>  ### assign new 198.215.54.0/24 I.P. for now (will need DMZ I.P. later) 
/etc/httpd/httpd.conf
/etc/my.cnf
/etc/php.d/opcache.ini
/etc/php.ini

```

\
\

- Install Apache:

  - include `mod_ssl`

  - copy `/etc/httpd/conf` & `/etc/httpd/conf.d/` from production

\
\

- **PHP Install**

  - check production PHP repo, download & install from exact same repo

  - production Cloud uses Webtactic Repo for PHP packages, which is long outdated since 2017

  - use IUS repo for PHP packages (**ABANDON!!!**)

```
################# VOID #####################

sudo yum install https://repo.ius.io/ius-release-el7.rpm

yum --disablerepo="*" --enablerepo="ius" search php

```

- identify all php packages on production server, ensure test side has the same set installed

  - `rpm -qa | grep -i php`

  - `rpm -qa | grep -i php | wc -l`


- disable Webtatic repo:

```
sudo vim /etc/yum.repos.d/webtatic.repo

--------------------
[webtatic]
...
enabled=0
--------------------

sudo yum repolist   #verify

################# END -of- VOID #####################
```

- remove current PHP packages (if needed)

```
sudo yum remove *php*

-----------------------------------------------------------------------
  mod_php71w.x86_64 0:7.1.33-1.w7                       php71w-cli.x86_64 0:7.1.33-1.w7                   
  php71w-common.x86_64 0:7.1.33-1.w7                    php71w-gd.x86_64 0:7.1.33-1.w7                    
  php71w-intl.x86_64 0:7.1.33-1.w7                      php71w-ldap.x86_64 0:7.1.33-1.w7                  
  php71w-mbstring.x86_64 0:7.1.33-1.w7                  php71w-mysql.x86_64 0:7.1.33-1.w7                 
  php71w-opcache.x86_64 0:7.1.33-1.w7                   php71w-pdo.x86_64 0:7.1.33-1.w7                   
  php71w-pear.noarch 1:1.10.4-1.w7                      php71w-pecl-apcu.x86_64 0:5.1.9-1.w7              
  php71w-pecl-imagick.x86_64 0:3.4.3-1.w7               php71w-process.x86_64 0:7.1.33-1.w7               
  php71w-xml.x86_64 0:7.1.33-1.w7   
-----------------------------------------------------------------------
```

- search IUS repo for same packages under PHP v7.2

```
################# VOID #####################

# use for loop to search through previously deleted packages

# declare string array
------------------------------------------------------------------------- 
phpSuite=(mod_php common intl mbstring opcache pear imagick xml cli gd ldap mysql pdo pecl-apcu process)
------------------------------------------------------------------------- 

# run for loop
------------------------------------------------------------------------- 
for i in "${phpSuite[@]}"; do sudo yum --disablerepo="*" --enablerepo="ius" search php | grep -i 72 | grep -i $i;done | awk '//{print $1}'
------------------------------------------------------------------------- 

  # follow up by piping into `| wc -l `  --> see what's extra & decide if they are needed

# missed out package: `pear1.noarch` --> in IUS this is package is not PHP version dependent


# install packages
------------------------------------------------------------------------- 
sudo yum install php72u-common.x86_64 php72u-intl.x86_64 php72u-mbstring.x86_64 php72u-opcache.x86_64 php72u-pecl-imagick.x86_64 php72u-pecl-imagick-devel.x86_64 php72u-xml.x86_64 php72u-xmlrpc.x86_64 php72u-cli.x86_64 php72u-pecl-smbclient.x86_64 php72u-gd.x86_64 php72u-ldap.x86_64 php72u-mysqlnd.x86_64 php72u-pdo.x86_64 php72u-pdo-dblib.x86_64 php72u-pecl-apcu.x86_64 php72u-pecl-apcu-devel.x86_64 php72u-pecl-apcu-panel.noarch php72u-fpm.x86_64 php72u-process.x86_64 pear1.noarch mod_php72u.x86_64
------------------------------------------------------------------------- 

# change PHP settings

sudo vim /etc/php.ini
  # change `memory_limit` to `1028M`

################# END -of- VOID #####################

```

\
\

- MySQL Server Base Setup

  - [fetch links for MYSQL repos based on distro and release here](https://dev.mysql.com/doc/refman/8.0/en/linux-installation-yum-repo.html#yum-repo-select-seriesm/doc/refman/8.0/en/linux-installation-yum-repo.html#yum-repo-select-series)

  - enable "[mysql5*-community]" in `/etc/yum.repos.d/mysql-community.repo` (also disable "[mysql80-community]")

  - install mysql server, start mysqld service

  - fetch temp mysql root password from `/var/log/mysqld.log` 

  - perform `mysql_secure_installation`

  - set up cloud database user

  - create empty database, grant cloud database user access via localhost and static I.P (as mysql root)
 
\
\

- NextCloud Web App Base 

  - copy NC app directory (as root)

  - temporarily allow root SSH access via sshd_config 

  - change OWNER & GROUP to `apache` on entire NextCloud directory, `/var/lib/php/session`, and `/var/lib/php/wsdlcache` 

  - modify `config.php` within NextCloud directory, adjust  "trusted_domains," "dbname," "dbuser," "dbpassword," 

  - modify `/etc/php.ini` -->  increase "memory_limit" (default 256M)
 

\
\

- Replicate Data & Database from Server

  - select "quiet" time window to copy data (as root, sshd configs need modding for root login on production server)

  - allow SSH access from test server to production server
 
  - squeeze SQL dump on production 

  - `sudo mysqldump --single-transaction --quick --lock-tables=false -u <user> -p <database> /tmp/inject-$(date +%F).sql`

  - copy .sql to test and import: `mysql -u <database user> -p <database name>  < /tmp/inject.sql` (if database currently holds data, drop and recreate it)
 
  - confirm successful database import: log in as cloud user, SELECT <database_name>, SHOW TABLES 

  - rsync data with `-a` and `--delete` (delete extra files on destination)

    - put TRAILING "/" in both SRC and DEST directories, e.g. `sudo rsync -a 198.215.56.120:/acloud/cloud_data_new/ /acloud/` 

    - if transfer interrupted, resume with `rsync -a --append` 

    - count files & directories `sudo find <nextcloud directory> -type f | wc -l` (also `-type -d`) 

- Query database for admin users in local db: `sudo -u apache php occ user:info admin` 

  - change admin password: `sudo -u apache php occ user:resetpassword admin`

\
\

- Allow Port 389 from test on ldap002

  - open ldap port 389 to server (reload iptables)

  - check on test server:  `nc -vz <ldap002 I.P.> 389`


- NextCloud WebApp LDAP Integration (log in via `admin` or `biohpcadmin`)

  - log in, sign in as `admin`(reset passwd might be needed)  --> configure LDAP integration
  
  - check ldap user: 1. by signing in; 2. by querying ldap database via occ command: `sudo -u apache php occ ldap:search <first name>`

  - VERIFY: log in as LDAP user and see if files and share links are intact.
\
\

- Test if Replica Server can maintain database changes on Production Server

  - Add, Delete, and Move files/directories on both local and ldap user accounts 

  - Squeeze new SQL dump from production

  - Prior to Importing latest database, clean file cache on test server: `sudo -u apache php occ files:cleanup` 

  - Import new SQL dump on test; first DROP & recreate inital databse

  - Sync data from production to test via `rsync -a --delete` options 

  - do file & directory counts on both servers to ensure success transfer

  - Perform file scan to update file cache: `sudo -u apache php occ files:scan --all` (this step needs **LDAP integration first!!!**)

  - VERIFY CHANGES: log in as admin --> reconfigure ldap integration --> log into local and ldap accounts --> check if changes from production is reflected on test server 

\
\

- **Additional Touch-ups**

   - fix app recommendations prior to updating: log in as admin, and go to "Settings" --> "Overview"

- PHP OPcache warning:

```
The PHP OPcache is not properly configured. For better performance it is recommended to use the following settings in the php.ini:

opcache.enable=1
opcache.enable_cli=1
opcache.interned_strings_buffer=8
opcache.max_accelerated_files=10000
opcache.memory_consumption=128
opcache.save_comments=1
opcache.revalidate_freq=1

### edit `/etc/php.d/opcache.ini` --> can `opcache.memory_consumption` be raised to 1028 or more?
```

- MySQL 4-byte character message:

  - [NC Fix Tutorial](https://docs.nextcloud.com/server/17/admin_manual/configuration_database/mysql_4byte_support.html#mariadb-10-2-or-earlier)

```
## Original Message:

MySQL is used as database but does not support 4-byte characters. To be able to handle 4-byte characters (like emojis) without issues in filenames or comments for example it is recommended to enable the 4-byte support in MySQL.

## Edit `/etc/my.cnf`, add following entries:
------------------------------------------ 
innodb_buffer_pool_size = 1024M
innodb_large_prefix = true
innodb_file_format=barracuda
innodb_file_per_table=1
------------------------------------------ 


## Login to MySQL Terminal
------------------------------------------ 
mysql -u cloud_overseer -p 

show variables like 'innodb_file_format';   # should return "Barracuda"

ALTER DATABASE biohpc_cloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

  # this changes the databases character set and collation
------------------------------------------ 


### use occ command to enable "utf8mb4" 
------------------------------------------ 
sudo -u apache php occ config:system:set mysql.utf8mb4 --type boolean --value="true"

# expected OUTPUT:
"System config value mysql.utf8mb4 set to boolean true"
------------------------------------------ 

### run occ maintenance:repair command, could take 5-10 mins
------------------------------------------ 
sudo -u apache php occ maintenance:repair 
------------------------------------------ 

### stop httpd, restart mysqld, then restart httpd

```

- PHP Perfomance Tuning (via opcache.ini, php.ini, and my.cnf) 

  - [link 1](https://www.saotn.org/optimize-php-opcache-configuration/)

  - [link 2](https://www.scalingphpbook.com/blog/2014/02/14/best-zend-opcache-settings.html)

  - [link 3](https://tideways.com/profiler/blog/fine-tune-your-opcache-configuration-to-avoid-caching-suprises)

  - [link 4](https://gist.github.com/rohankhudedev/1a9c0a3c7fb375f295f9fc11aeb116fe)

  - [link 5](https://stackoverflow.com/questions/10905226/mysql-my-cnf-performance-tuning-recommendations)

  - [link 6](https://www.prestashop.com/en/blog/php-ini-file)

  - [link 7](https://stackify.com/php-performance-tuning/)

  - [php upgrade guide](https://docs.nextcloud.com/server/17/admin_manual/installation/php_72_installation.html?highlight=php)

  - [file upload size guide](https://docs.nextcloud.com/server/17/admin_manual/configuration_files/big_file_upload_configuration.html?highlight=php)


\
\
\
\
\



### ACTUAL DOWNTIME: 

- **On Production Server**



```
# enable maintenance mode

cd /var/www/owncloud/html

sudo -u apache php occ maintenance:mode --on

# Stop httpd

sudo systemctl stop httpd

# Generate latest .sql dump

sudo mysqldump --single-transaction --quick --lock-tables=false -u <user> -p <database> /tmp/inject-$(date +%F).sql

# Edit `/etc/sysconfig/iptables`

 # New Line:
-----------------------------------------------------------------------------------
-A INPUT -p tcp -m state --state NEW -s <test_cloud IP> -m tcp --dport 22 -j ACCEPT
-----------------------------------------------------------------------------------

service iptables reload

```

\
\

- **On Test Server: Pre-Update Data Sync**

```
# Also enable maintenance mode
---------------------------------------------------------
cd /var/www/html/nextcloud_15

sudo -u apache php occ maintenance:mode --on

sudo systemctl stop httpd
---------------------------------------------------------

# drop current database and recreate new one via SQL Cloud user
--------------------------------
mysql -u cloud_overseer -p

SHOW DATABASES;

DROP DATABASE <database_name>;

CREATE DATABASE biohpc_cloud;

  # if error occurs login as root SQL user & repeat
--------------------------------

# alternative commands (as root)
--------------------------------
mysql -u root -p

SHOW DATABASES;

DROP DATABASE <database_name>;

CREATE DATABASE biohpc_cloud;

GRANT ALL PRIVILEGES ON cloud_import.* TO 'cloud_overseer'@'localhost';
--------------------------------

# clean current filecache
---------------------------------------------------------
sudo -u apache php occ files:cleanup
---------------------------------------------------------

# scp SQL dump to `/tmp` & import to database
---------------------------------------------------------
sudo scp root@<cloud_ip>:/tmp/inject-$(date +%F).sql /tmp

mysql -u cloud_overseer -p biohpc_cloud  < /tmp/inject.sql
---------------------------------------------------------

# rsync production data, do file scan
---------------------------------------------------------
sudo rsync -a --delete  <cloud I.P.>:/var/www/owncloud/data/data/ /cloud_data

  # verify with find, count # of directories and files on SRC and DEST

  # verify that group and owner of all data files is "apache"

cd /var/www/html/nextcloud_15

  # file scan (could take a while---perhaps increase PHP & Opcache mem limit ahead of time)

sudo -u apache php occ files:scan --all
---------------------------------------------------------

# Turn off maintenance mode 
---------------------------------------------------------
sudo -u apache php occ maintenance:mode --off
---------------------------------------------------------

# Reset admin password 
---------------------------------------------------------
sudo -u apache php occ user:info admin

sudo -u apache php occ user:resetpassword admin
---------------------------------------------------------

# Start Apache
---------------------------------------------------------
sudo systemctl start http
---------------------------------------------------------

# Re-enable LDAP integration 
---------------------------------------------------------
# log in as admin user configure LDAP integration:

- Under **Server** Tab:

 - Delete current configuration

 - Add new configuration

 - Server IP: `192.168.54.1`

 - Leave the following empty: "User DN", "Password" 

 - Base DN `ou=users,dc=biohpc,dc=swmed,dc=edu` 


- Under **Users** Tab:

 - "Only these object classes": `posixAccount`
 
 - resultant LDAP filter should be: `(&(|(objectclass=posixAccount)))`

 - check "Verify settings and count users" --> expected output: `>1000 users found` 


- Under **Login Attributes**

 - Check the following options: "LDAP/AD Username", "LDAP/AD Email Address", "Other Attributes" --> `displayName` 

 - generated LDAP filter should look like this:
----------------------------------------------
(&(&(|(objectclass=posixAccount)))
(|(uid=%uid)
(|(mailPrimaryAddress=%uid)
(mail=%uid))(|(displayName=%uid))))
----------------------------------------------

- Under **Groups**

  - Only these object classes `posixGroup`

  - resultant LDAP filter: `(&(|(objectclass=posixGroup)))` 

  - select "Verify settings and count the groups" --> "373 groups found" 

- Under **Advnaced**

- Connection Settings:

  - Check `Configuration Active`

  - "Cache Time-To-Live" --> "600"

- Directory Settings:

  - "Base User Tree" --> `ou=users,dc=biohpc,dc=swmed,dc=edu`

  - "Base Group Tree" --> `ou=users,dc=biohpc,dc=swmed,dc=edu`

  - Check "Nested Groups"
---------------------------------------------------------


# Check LDAP User 
---------------------------------------------------------

# from server side

sudo -u apache php ldap:search <user firstname> 

e.g.
 
sudo -u apache php occ ldap:search zengxing
Zengxing Pang (e57019bc-0d42-460c-a6ec-9b231a6dcb14)


# from web interface, log in as LDAP user

  ## see if share links active

  ## see if additional files/folders present

  ## see if deleted files/folders have been erased
---------------------------------------------------------


# Enable 4-byte characters to SQL database
---------------------------------------------------------
sudo systemctl stop httpd

mysql -u cloud_overseer -p

show variables like 'innodb_file_format';   # should return "Barracuda"

ALTER DATABASE biohpc_cloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

  # this changes the databases character set and collation
------------------------------------------

# use occ command to enable "utf8mb4"

sudo -u apache php occ config:system:set mysql.utf8mb4 --type boolean --value="true"

# expected OUTPUT:
"System config value mysql.utf8mb4 set to boolean true"
---------------------------------------------------------

# prepare for update: re-enable maintenance mode
---------------------------------------------------------
sudo -u apache php occ maintenance:mode --on

# ensure httpd is shutdown

sudo systemctl stop httpd
---------------------------------------------------------
```

\
\

### Upgrade to NC v16 (has to be incremental)
 
```
# make a backup of the current app:

sudo tar -cpzvf /cloud_backups/nc_15_backup /var/www/html/nextcloud/

# rename old Nextcloud folder: 

sudo mv /var/www/html/nextcloud/ /var/www/html/nextcloud_15_old/ 

# download & untar NC 16

cd /var/www/html/

wget https://download.nextcloud.com/server/releases/nextcloud-16.0.7.tar.bz2

tar -xvjf /var/www/html/nextcloud-16.0.7.tar.bz2

  # contents will be placed in `/var/www/html/nextcloud`

# change everything to correct ownership & permissions

chown -R apache:apache /var/www/html/nextcloud

find /var/www/html/nextcloud/ -type d -exec chmod 750 {} \;

find /var/www/html/nextcloud/ -type f -exec chmod 640 {} \;

systemctl start httpd (required by the official manual upgrade guide)

# copy config file from old NC directory

cp -p /var/www/html/nextcloud_15_old/config/config.php /var/www/html/nextcloud/config/

  # we'll copy theme in the next NC iteration

# perform upgrade command

cd /var/www/html/nextcloud

sudo -u apache php occ upgrade

# check if NC is operational

sudo -u apache php occ maintenance:mode --off

# log in & check `admin` & LDAP users

# check "Settings" --> "Overview" --> Fix any database related errors

  # e.g. add missing indices

sudo -u www-data php occ db:add-missing-indices

# prepare for updating to next version

sudo -u apache php occ maintenance:mode --on

systemctl stop httpd
 
```

\
\

### Upgrade to NC v17

```
sudo mv /var/www/html/nextcloud/ /var/www/html/nextcloud_16_old/ 

cd /var/www/html/

wget https://download.nextcloud.com/server/releases/nextcloud-17.0.3.tar.bz2 

# un-tar download

tar -xvjf /var/www/html/nextcloud-17.0.3.tar.bz2

chown -R apache:apache /var/www/html/nextcloud

find /var/www/html/nextcloud/ -type d -exec chmod 750 {} \;

find /var/www/html/nextcloud/ -type f -exec chmod 640 {} \;

systemctl start httpd

# copy config file & theme folder over

cp -p /var/www/html/nextcloud_16_old/config/config.php /var/www/html/nextcloud/config/
  
cp -rvp /var/www/html/nextcloud_15_old/themes/biohpc/* /var/www/html/nextcloud/config/themes

# upgrade again

cd /var/www/html/nextcloud

sudo -u apache php occ upgrade

sudo -u apache php occ maintenance:mode --off

# log in as admin & LDAP user to check for additional errors

 # may need to add missing-indices to SQL database again

```

\
\

### Wrapping Up

```
# bring down old production server
----------------------------------
systemctl stop mysqld

ifdown eth0 
----------------------------------

# let new server inherit the cloud I.P
----------------------------------
systemctl stop httpd

systemctl stop mysqld

vim /etc/sysconfig/network-scripts/ifcfg-eth0

-------------------
DEVICE=eth0
HWADDR=< fill in new MAC addr >
TYPE=Ethernet
ONBOOT=yes
NM_CONTROLLED=yes
BOOTPROTO=none
IPADDR=129.112.9.92
NETMASK=255.255.255.0
GATEWAY=129.112.9.254
IPV6INIT=no
USERCTL=no
-------------------
----------------------------------

# configure `config.php`
-------------------
'trusted_domains' => 
  array (
    0 => 'cloud.biohpc.swmed.edu',
    1 => '129.112.9.92',
  ),
-------------------

# change `/etc/hosts`
-------------------
129.112.9.92 cloud   cloud.biohpc.swmed.edu
-------------------

# bring up new server's NIC

ifdown eth0

ifup eth0

systemctl restart network

# test if Nextcloud is operational

systemctl start mysqld

systemctl start httpd

# login via `admin` and LDAP user, check share links and files

# alter log in footnote to "@ 2020 UTSouthwestern BioHPC" (formerly "@ 2016")

--------------------------------------------------
vim themes/biohpc/defaults.php

123         public function getShortFooter() {
124                 $footer = '© 2020 <a href="https://portal.biohpc.swmed.edu" target="_blank\">'.$this->getEntity().'</a>'.

134         public function getLongFooter() {
135                 $footer = '© 2020 <a href="https://portal.biohpc.swmed.edu" target="_blank\">'.$this->getEntity().'</a>'.
--------------------------------------------------
```


- change theme css line

- change NIC I.P. --> bring down older server's NIC -->  change /etc/hosts, config.php --> restore cron job to delete orphan users

- restore sshd config to deny Root login

```

### ESXI VM Specs

```
# host info:
------------------------------------
I.P. --> 129.112.9.36 (original host: ###.###.#.18) 

- CPU: 24-32 CPUs 

  - no reservation

  - unlimited

  - hardware virtualization: Disabled

  - performance counters: Disabled


- Memory: 51200 MB

  - no reservation

  - unlimited


- Storage: Create 3 Disks

  - Disk 1: 120GB  --> mounts to `/`

  - Disk 2 + 3: 2TBx2  --> make new VG, mounts to `/cloud_data`

 - thin provision for all disks (dynamically allocated)
 

- Network adpater (1) --> connects to "VM Network"


- Default settings for Video card, Adapaters, Controllers, and Input Devices
------------------------------------

```

### IMPORTANT Theme Configs

```
## Steps Needed to Implement Fully Customized Themes:

1. Disable Theming App in Web Interface

2. Change deprecated `style.css` file to `server.css`, or else the instructions within will not be compiled (full path: /var/www/html/nextcloud/themess/<your customized theme>/core/css/.)

3. Update image cache: `sudo -u www-data php occ maintenance:theme:update` 

TIP: when resizing: check web inspector to get estimate pixel dimensions

### CHANGE LOG to Previous .css file
------------------------------------
## deleted cloud image from wrapper

  # commented out `background-image:url('../img/cloud_revisited.png')` under `#body-login .wrapper .v-align {}`

  # 

  # do not add `top` option to logo field!

------------------------------------


## Resource:

[nextclou 15 Developer manual](https://docs.nextcloud.com/server/15/developer_manual/core/theming.html)


##### WORKING `server.css` file
--------------------------------------------------------------------------------------------------
/**
 * @author Jan-Christoph Borchardt, http://jancborchardt.net
 * @copyright Copyright (c) 2015, ownCloud, Inc.
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */


/* header color */
/* this is the main brand color */
#body-user #header,
#body-settings #header,
#body-public #header {
	background-color: #557891;
}

/* log in screen background color */
/* gradient of the header color and a brighter shade */
/* can also be a flat color or an image */
#body-login {

	background-image:url('../img/cloud_revisited.png'); //IMPACT: login background image
	height: 100%;
//	text-align: center;
//	background:#557891; /* Old browsers */
//	background: -moz-linear-gradient(left, #bad2dd 0%, #557891 100%); /* FF3.6+ */
//	background: -webkit-gradient(linear, left right, color-stop(0%,#bad2dd), color-stop(100%,#557891)); /* Chrome,Safari4+ */
//	background: -webkit-linear-gradient(left, #bad2dd 0%,#557891 100%); /* Chrome10+,Safari5.1+ */
//	background: -o-linear-gradient(left, #bad2dd 0%,#557891 100%); /* Opera11.10+ */
//	background: -ms-linear-gradient(left, #bad2dd 0%,#557891 100%); /* IE10+ */
//	background: linear-gradient(left, #bad2dd 0%,#557891 100%); /* W3C */
//	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#bad2dd', endColorstr='#557891',GradientType=0 ); /* IE6-9 */
}

#body-login .wrapper {
        width: 420px; //IMPACT: how centered logo box is
}

#body-login .wrapper .v-align {
        height: 275px;
// background-image:url('../img/cloud.png');
}

#body-login .wrapper .v-align #header {
        height: 160px; //IMPACT: how far down login box is
}

/* primary action button, use sparingly */
/* header color as border, brighter shade again, here as background */
.primary,
input[type="submit"].primary,
input[type="button"].primary,
button.primary,
.button.primary,
.primary:active,
input[type="submit"].primary:active,
input[type="button"].primary:active,
button.primary:active,
.button.primary:active {  //IMPACT: login button color, border, and thickness
	border-color: white;
	background-color: grey;
	height: 48px;
}
.primary:hover,
input[type="submit"].primary:hover,
input[type="button"].primary:hover,
button.primary:hover,
.button.primary:hover,
.primary:focus,
input[type="submit"].primary:focus,
input[type="button"].primary:focus,
button.primary:focus,
.button.primary:focus {
	background-color: #8b75e4;
}
.primary:active, input[type="submit"].primary:active, input[type="button"].primary:active, button.primary:active, .button.primary:active,
.primary:disabled, input[type="submit"].primary:disabled, input[type="button"].primary:disabled, button.primary:disabled, .button.primary:disabled,
.primary:disabled:hover, input[type="submit"].primary:disabled:hover, input[type="button"].primary:disabled:hover, button.primary:disabled:hover, .button.primary:disabled:hover,
.primary:disabled:focus, input[type="submit"].primary:disabled:focus, input[type="button"].primary:disabled:focus, button.primary:disabled:focus, .button.primary:disabled:focus {
	background-color: #557891;
}

/* use logos from theme */
#header .logo { //IMPACT: logo image location, size, and width
	background-image: url('../img/biohpc_logo_height_4_enhanced.png');
        background-size: 450px;
        width: 500px;
	position: relative;
}

#header .logo-icon {
	background-image: url('../img/logo-icon.svg');
	width: 62px;
	height: 34px;
}


/*

#header #logo-claim {
    display: block;
    position: center;
    font-size: 30px;
   // top: -10px;
    font-weight: bold;
    opacity: 1;
    color: #577a8f;
}

*/


/*

#nextcloud {
    width: 200px;
    background-image: url('../img/cloud.png');
    background-position: 40% 10%;
}

#nextcloud .logo-icon {
    width: 152px;
    background-image: url('../img/logo-wide.png');
}

#header .header-appname-container {
    left: 180px;
}

*/ 

#body-login p.info { //IMPACT: Footer options
    color: gray;
    width: 50em;

}

#body-login p.info a{ //IMPACT: "UTSouthwestern BioHPC" options"

    color: grey;
}


// #body-user #header {
//	background:#557891; /* Old browsers */
//	background: -moz-linear-gradient(left, #bad2dd 0%, #557891 100%); /* FF3.6+ */
//	background: -webkit-gradient(linear, left right, left bottom, color-stop(0%,#bad2dd), color-stop(100%,#557891)); /* Chrome,Safari4+ */
//	background: -webkit-linear-gradient(left, #bad2dd 0%,#557891 100%); /* Chrome10+,Safari5.1+ */
//	background: -o-linear-gradient(left, #bad2dd 0%,#557891 100%); /* Opera11.10+ */
//	background: -ms-linear-gradient(left, #bad2dd 0%,#557891 100%); /* IE10+ */
//	background: linear-gradient(left, #bad2dd 0%,#557891 100%); /* W3C */
//	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#bad2dd', endColorstr='#557891',GradientType=0 ); /* IE6-9 */
//}

//#body-settings #header {
//	background:#557891; /* Old browsers */
//	background: -moz-linear-gradient(left, #bad2dd 0%, #557891 100%); /* FF3.6+ */
//	background: -webkit-gradient(linear, left right, left bottom, color-stop(0%,#bad2dd), color-stop(100%,#557891)); /* Chrome,Safari4+ */
//	background: -webkit-linear-gradient(left, #bad2dd 0%,#557891 100%); /* Chrome10+,Safari5.1+ */
//	background: -o-linear-gradient(left, #bad2dd 0%,#557891 100%); /* Opera11.10+ */
//	background: -ms-linear-gradient(left, #bad2dd 0%,#557891 100%); /* IE10+ */
//	background: linear-gradient(left, #bad2dd 0%,#557891 100%); /* W3C */
//	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#bad2dd', endColorstr='#557891',GradientType=0 ); /* IE6-9 */
//}

--------------------------------------------------------------------------------------------------


```



### Extras:

 - repeat steps in previous section, only difference here is to insert theme files needed for BioHPC cloud into the `themes` subdirectory (we can also modify from the web interface as admin) 

 - verify update: 

   - check files and links of BOTH local and ldap users 

   - fix additional bugs reported by NextCloud in "Settings" 

- Upload procedures and notes to BioHPC Docs:

\
\

- Finishing:

 - theme application

 - copy ssl certificate from production to test server 

 - temporarily stop Production Server --> bring NIC down 

 - assign TEST SERVER the I.P. once belonging to Production 

 - change in `config.php` the domain name from `test-cloud.biohpc.swmed.edu` to `cloud.biohpc.swmed.edu`

 - check upload size


### Resources for ESXI Host:

- [PHP 7.2](https://docs.nextcloud.com/server/17/admin_manual/installation/php_72_installation.html#rhel-7-upgrade-to-php-7-2)

- [mem caching](https://docs.nextcloud.com/server/17/admin_manual/configuration_server/caching_configuration.html)

- [maximum upload size](https://docs.nextcloud.com/server/17/admin_manual/configuration_files/big_file_upload_configuration.html?highlight=maximum%20upload%20size)

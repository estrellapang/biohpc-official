## DMZ Nextckoud Server Update: March, 2021

### Major Componenets to Change

- kernel 3.10.0-1127 --> -1160

- httpd 2.4.6-93 --> -95

- python-2.7.5-88 --> -89

\
\

### Test-Env: Authentications

```
# I.P.

198.215.56.120

# root

~#######ZxP~

# estrella

BioP@ssw0rd1
```

### Peripheral Components

- implement `sudo package` update

- implement download upload size change -- refer to lamella changelog

- install additional scanning hosts -- refer to PK's Teams message

- **APC Cache not enabled**

- upload size config files??? --> check docs

\
\

### Major Steps

- stop httpd, mysqld

- install security upgrade

- reboot; start services, include security services

- update NC 17 - 18

  - tweak upload php download time

- install new IR scanning agent

\
\

### Server Rebuild

#### Base Setup

```
scp biohpcadmin@<cloud>:/home/condensation/images/nextcloud-17.0.5.tar.bz2 /home/deploy/

cp -v /home/deploy/nextcloud-17.0.5.tar.bz2 /var/www/html

sudo yum install bzip2

cd /var/www/html 

sudo tar -xjf /var/www/html/nextcloud-17.0.5.tar.bz2

sudo rm -i nextcloud-17.0.5.tar.bz2

sudo chown -R apache:apache /var/www/html/nextcloud/

sudo find /var/www/html/nextcloud/ -type d -exec chmod 750 {} \;

sudo find /var/www/html/nextcloud/ -type f -exec chmod 640 {} \;

# copy essential configuration files and SQL dump from production server

scp -r biohpcadmin@129.112.9.92:/tmp/nc_data/ /home/deploy/

scp -r biohpcadmin@129.112.9.92:/tmp/nc_themes /home/deploy/

scp -r biohpcadmin@129.112.9.92:/tmp/nc_config /home/deploy/

scp biohpcadmin@129.112.9.92:/tmp/cloud_db_bak-20210226023701.sql /home/deploy/

\

# change `cloud_admin` password

sudo mysql -u root -p

ALTER USER 'cloud_admin'@'localhost' IDENTIFIED BY 'BioP@ssw0rd1';

\

# copy over nextcloud config from production, and modify it

sudo cp -vi /home/deploy/nc_config/config.php /var/www/html/nextcloud/config/
sudo chmod 640 /var/www/html/nextcloud/config/config.php
sudo chown apache:apache /var/www/html/nextcloud/config/config.php
-----------------------------------------------------------------------
<?php
$CONFIG = array (
  'instanceid' => 'oc5512891707',
  'passwordsalt' => 'a0b08fcadf85b64eb589ba02d9da2b',
  'trusted_domains' =>
  array (
    0 => '198.215.56.120',
  ),
  'datadirectory' => '/cloud_data/data/',
  'dbtype' => 'mysql',
  'version' => '17.0.5.0',
  'dbname' => 'test_cloud',
  'dbhost' => 'localhost',
  'dbtableprefix' => 'oc_',
  'dbuser' => 'cloud_admin',
  'dbpassword' => 'BioP@ssw0rd1',
  'installed' => true,
  'updatechecker' => false,
  'has_internet_connection' => false,
  'log_rotate_size' => 26214400,
  'appstoreenabled' => false,
  'theme' => 'biohpc',
  'forcessl' => true,
  'enable_previews' => true,
  'ldapIgnoreNamingRules' => false,
  'knowledgebaseenabled' => false,
  #'knowledgebaseurl' => 'https://portal-dmz.biohpc.swmed.edu',
  #'mail_from_address' => 'biohpc-help',
  #'mail_smtpmode' => 'smtp',
  #'mail_domain' => 'utsouthwestern.edu',
  'loglevel' => 0,
  'secret' => '2358593fda0dfbd19ec8af844b286e6dd6c419da7f08e6ffec16d8f3ad1c885af61f254976a1bd700c52a61a346c8f13',
  'maintenance' => false,
  'trashbin_retention_obligation' => 'auto',
  'memcache.local' => '\\OC\\Memcache\\APCu',
  #'lost_password_link' => 'https://portal.biohpc.swmed.edu/accounts/password-reset/',
  'check_for_working_webdav' => true,
  'check_for_working_htaccess' => true,
  #'ldapUserCleanupInterval' => 10,
  'appstore.experimental.enabled' => true,
  'allow_user_to_change_display_name' => false,
  'remember_login_cookie_lifetime' => 172800,
  'session_lifetime' => 86400,
  'session_keepalive' => true,
  'logtimezone' => 'America/Chicago',
  'ldapProviderFactory' => '\\OCA\\User_LDAP\\LDAPProviderFactory',
  'overwrite.cli.url' => 'https://198.215.56.120',
 #'mail_sendmailmode' => 'smtp',
 # 'mail_smtphost' => 'smtp.swmed.edu',
 #'mail_smtpport' => '25',
  'mysql.utf8mb4' => true,
);
-----------------------------------------------------------------------

\

# create cloud data directory; copy critical data files

sudo mkdir -p /cloud_data/data/

sudo cp -r /home/deploy/nc_data/* /cloud_data/data/

sudo cp /home/deploy/nc_data/.* /cloud_data/data/

sudo chmod -R 770 /cloud_data/data/

sudo chown -R apache:apache /cloud_data/data/

sudo cp -r /home/deploy/nc_themes/biohpc /var/www/html/nextcloud/themes/

sudo chown -R apache:apache /var/www/html/nextcloud/themes/biohpc

sudo chmod -R 750 /var/www/html/nextcloud/themes/biohpc

\

# configure httpd config
-----------------------
DocumentRoot "/var/www/html/nextcloud"
-----------------------

\

# inject database into test database

sudo mysql -u cloud_admin -p test_cloud < /home/deploy/cloud_db_bak-20210226023701.sql

```

\
\

#### LDAP Integration

```
"+"  # add new config

"198.215.54.45"  --> "389"   # make sure to open up port 389 to VM's I.P. address

                             # no need to select "Detect Port" until all of the configurations have finished


"ou=users,dc=biohpc,dc=swmed,dc=edu" --> "Base DN"

        # do not select "Detect Base DN/Test Base DN" until all of the configurations have finished

# DO NOT CHECK "Manually enter LDAP filters"

\

### "Users" Fields

# under "Edit LDAP Query" --> "(&(|(objectclass=posixAccount)))" --> click "Edit LDAP Query" link once more

  # After this, the previously greyed out user search criteria should be made available

# "Only these object classes"  --> "posixAccount"

# DO NOT TOUCH "Only from these groups"

\

### "Login Attributes" Fiekds

# CHECK "LDAP/AD Username"

# "LDAP/AD Email ADdress"

# "Other Attributes" --> "displayName"

# the "LDAP Query" filter terms should automatially appear

\

### "Groups"

# select "posixGroup" --> click "Edit LDAP Query" so thast the field becomes static & not editable

\

### Verify:

under "Server" --> "Detect Port" & "Test Base DN"  ("Detect Base DN" will return an error)

under "User" --> "Verify settings and count users"

under "Groups" --> "Verify settings and count the groups"

```

\
\

### STOP: Take Snapshot and Make Clones


- changed `estrella` password to `BioP@ssw0rd1`

- shut down vm, and create snapshot:

```
# take snapshot
vboxmanage snapshot cloud_18 take "pre_update_NC_v_17" --description "Working NC_v_17; no update; registered to RHEL Satellite"
0%...10%...20%...30%...40%...50%...60%...70%...80%...90%...100%
Snapshot taken. UUID: 482782ca-d568-44ca-99d1-071ccc04300e`

# make first clone
vboxmanage clonevm cloud_18 --name="cloud_18_AA" --register --mode=all

   # 198.215.54.102 

# make second clone
vboxmanage clonevm cloud_18 --name="cloud_18_XL" --register --mode=all

   # 198.215.54.124

# start vm, change IP and register for RHEL Satellite

vboxmanage startvm --type=headless cloud_18_AA

  # verify: 

VBoxManage list runningvms
"cloud_18_AA" {e07fc0e2-21fb-4042-ba41-c5801c89a303}

  # modifications:
---------------------------------------------------
sudo vim /etc/sysconfig/network-scripts/ifcfg-enp0s8
sudo systemctl restart network  # will lose connectivity at this point
sudo hostnamectl set-hostname 'cloud-test-AA.biohpc.swmed.edu'
sudo yum remove rhn-org-trusted-ssl-cert
sudo vim /etc/resolv.conf
sudo rpm -iv https://rhn.swmed.edu/pub/rhn-org-trusted-ssl-cert.noarch.rpm
/usr/sbin/rhnreg_ks --force --profilename=$(hostname -s) --activationkey 1-93af58dc96cfd4e7a7ccc4b3e4889f65  --serverUrl https://rhn.swmed.edu/XMLRPC --sslCACert /usr/share/rhn/RHN-ORG-TRUSTED-SSL-CERT
vim /var/www/html/nextcloud/config/config.php
sudo chmod 755 /var/www/html/nextcloud
visudo   # give sudo user rights for all
cd /var/www/html/nextcloud/ 
sudo -u apache php occ user:resetpassword admin   # change password to be same as everything else
systemctl start httpd,mysqld   # verify LDAP settings
---------------------------------------------------
```

\
\

### RHEL System Update

```
# suspend all sessions on Nextcloud
sudo -u apache php occ maintenance:mode --on

  # go to web page to confirm

# backup database for backup purposes
sudo mysqldump --single-transaction --quick --lock-tables=false -u cloud_admin -p test_cloud > /home/deploy/nexcloud-database-$(date +%F).sql

systemctl stop httpd
systemctl stop mysqld

# performance OS update

sudo yum --security check-updates

 # watch out for php/mysql updates --> exclude them if listed

sudo yum --security update-minimal

  # kernel change: 3.10.0-1127.el7.x86_64 --> 3.10.0-1160.15.2.el7.x86_64 (RHEL 7.8)

  # check if nextcloud is still functional

systemctl restart mysqld
systemctl restart httpd
sudo -u apache php occ maintenance:mode --off

# log in as admin --> check settings for general alerts
# log in as user --> check if LDAP is working

\
\

### Nextcloud v17 --> Nextcloud v18
 
sudo -u apache php occ maintenance:mode --on
systemctl stop httpd
systemctl stop mysqld

cd /home/deploy/images

wget https://download.nextcloud.com/server/releases/nextcloud-18.0.14.tar.bz2

sudo cp /home/deploy/images/nextcloud-18.0.14.tar.bz2

sudo mv /var/www/html/nextcloud /var/www/html/nextcloud_old_17

cd /var/www/html/

sudo tar -xjf /var/www/html/nextcloud-18.0.14.tar.bz2

### copy all critical configs from old to new server

sudo cp -vi --preserve /var/www/html/nextcloud_old_17/config/config.php /var/www/html/nextcloud/config/
sudo cp -vi --preserve /var/www/html/nextcloud_old_17/config/.htaccess /var/www/html/nextcloud/config/
sudo cp -r --preserve /var/www/html/nextcloud_old_17/themes/biohpc /var/www/html/nextcloud/themes/

### ensure proper permissions for new nextcloud files

chown -R apache:apache /var/www/html/nextcloud

sudo find /var/www/html/nextcloud/ -type d -exec chmod 750 {} \;

sudo find /var/www/html/nextcloud/ -type f -exec chmod 640 {} \;

### run update script

cd /var/www/html/nextcloud

sudo -u apache php occ upgrade

sudo -u apache php occ maintenance:mode --off

sudo systemctl restart httpd

# log in as admin, verify LDAP and errors in settings

```

### Nextcloud 18 Theming Configurations:


#### fixes for tiled background

- copy guest.css from /var/www/html/nextcloud/core/css/guest.css to NEW app dir

  - modifications

```
min-height: 98%

# line 37

color: #a4a4a4;
```

- modifications to `/var/www/html/nextcloud/themes/biohpc/core/css/server.css`

```
# line 18:

background-image:url('../img/cloud_revisited_new.png');

# line 61:

background-color: #5fafe6d1;

# line 123:
color: #035b98;

# line 128:
color: #005c9b;

### Header gradient changes --> insert this under vi command mode, replace any setting with gradients
:%s/#557891/#3cabfad9/gc
```

- modifications to `/var/www/html/nextcloud/themes/biohpc/themes/biohpc/defaults.php`

```
# line 124:
2021

# line 135:
2021

```

- took snapshot for theme fix:

```
 vboxmanage snapshot cloud_18 take updated_NC_v_18 --description "Working NC_v_18; New Theme; RHEL_v7_8"
0%...10%...20%...30%...40%...50%...60%...70%...80%...90%...100%
Snapshot taken. UUID: 7a6edfb0-2450-4f76-b72a-08006da74090
```

\
\

### Exact Commands Used for Downtime

#### Upgrade to RHEL 7.8

```
# suspend all sessions on Nextcloud
cd /var/www/html/nextcloud/
sudo -u apache php occ maintenance:mode --on

# go to web page to confirm

# backup database for backup purposes
sudo mysqldump --single-transaction --quick --lock-tables=false -u cloud_overseer -p biohpc_cloud > /home/condensation/sql_imports/nexcloud-database-$(date +%F).sql

systemctl stop httpd
systemctl stop mysqld

# performance OS update
sudo yum --security check-updates

# watch out for php/mysql updates --> exclude them if listed
sudo yum --security update-minimal

  # kernel change: 3.10.0-1127.el7.x86_64 --> 3.10.0-1160.15.2.el7.x86_64 (RHEL 7.8)

# restart system into new kernel
sudo reboot

  # check kernel and OS version
  uname -a
  cat /etc/redhat-release

# check if nextcloud is still functional
systemctl restart mysqld
systemctl restart httpd

# release session suspension on Nextcloud
cd /var/www/html/nextcloud/
sudo -u apache php occ maintenance:mode --off

# log in as admin --> check settings for general alerts
# log in as user --> check if LDAP is working
```

\
\
\

#### Upgrade Nextcloud to Version 18 

```
# go maintenance mode once more
sudo -u apache php occ maintenance:mode --on

systemctl stop httpd
systemctl stop mysqld

cd /home/condensation/images

wget https://download.nextcloud.com/server/releases/nextcloud-18.0.14.tar.bz2

sudo cp /home/condensation/images/nextcloud-18.0.14.tar.bz2 /var/www/html/
du -sh /home/condensation/images/nextcloud-18.0.14.tar.bz2
du -sh /var/www/html/nextcloud-18.0.14.tar.bz2

sudo mv /var/www/html/nextcloud /var/www/html/nextcloud_old_17

cd /var/www/html/

sudo tar -xjf /var/www/html/nextcloud-18.0.14.tar.bz2


### copy all critical configs from old to new server

sudo cp -vi --preserve /var/www/html/nextcloud_old_17/config/config.php /var/www/html/nextcloud/config/

sudo cp -r --preserve /var/www/html/nextcloud_old_17/themes/biohpc /var/www/html/nextcloud/themes/

sudp cp /tmp/cloud_revisited_new.png /var/www/html/nextcloud/themes/biohpc/core/img/

sudo chown apache:apache /var/www/html/nextcloud/themes/biohpc/core/img/cloud_revisited_new.png

### ensure proper permissions for new nextcloud files

chown -R apache:apache /var/www/html/nextcloud

sudo find /var/www/html/nextcloud/ -type d -exec chmod 750 {} \;

sudo find /var/www/html/nextcloud/ -type f -exec chmod 640 {} \;

### run update script

cd /var/www/html/nextcloud
sudo -u apache php occ upgrade
sudo -u apache php occ maintenance:mode --off
sudo systemctl restart httpd

# log in as admin, verify LDAP and errors in settings
.
.
.
# FRESH PAINT:

sudo systemctl stop httpd

sudo cp -i /tmp/guest.css /var/www/html/nextcloud/core/css/guest.css
sudo chown apache:apache /var/www/html/nextcloud/core/css/guest.css

sudo cp -i /tmp/server.css /var/www/html/nextcloud/themes/biohpc/core/css/server.css
sudo chown apache:apache /var/www/html/nextcloud/themes/biohpc/core/css/server.css

sudo cp -i /tmp/defaults.php /var/www/html/nextcloud/themes/biohpc/defaults.php
sudo chown apache:apache /var/www/html/nextcloud/themes/biohpc/defaults.php

sudo systemctl restart httpd

```

\
\
\

### Lamella Rebuild: NextCloud External Storage

- install samba client and server

  - `sudo yum install samba`

  - installs the following packages

```
samba-common-4.10.16-9.el7_9.noarch
samba-client-libs-4.10.16-9.el7_9.x86_64
samba-common-tools-4.10.16-9.el7_9.x86_64
samba-common-libs-4.10.16-9.el7_9.x86_64
samba-client-4.10.16-9.el7_9.x86_64
samba-libs-4.10.16-9.el7_9.x86_64
samba-4.10.16-9.el7_9.x86_64
```

- configure samba server, test External Storage feature

- test CTDB



## LamellaSE Rebuild Notes

### Overview of Steps

- 10G network NICs 

- install nfs server, samba server, LDAP client

  - export project, home2, and work bia samba

- pre-configure ib0's ifcfg file


- **downtime**

  - swap 10G IP

  - change passthrough to new VM

  - install OFED, GPFS client & Lustre Client

### Pass Through Configurations

 - check `/etc/systctl`

 - add master root key for nucleus001 

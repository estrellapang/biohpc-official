## Setting Up Lustre Parallel File Servers 

### Basic VM info: 

- Distribution: CentOS 7 
- Admin user: estrella (13579ZxP) 
- root: ~13579ZxP~ 
- network: 10.100.180.1/24 

  - `enp0s3` --> NAT connection to VM host interface 

  - `eth1` --> intra-cluster network (pretend this is the infiniBand network) 

- server IPs: *.*.180.2 & *.*.180.3

  - hostnames:

    - `data1.biohpc.new`  --> MDS/MDT server 

    - `data2.biohpc.new`  --> OST/OSS server 
 

- where they are stored: 

  - `/project/biohpcadmin/zpang1/VirtualBox_VMs/data1`(and `/../../../../data2`)

### Essential Setups: 

- UTSW proxy added in `/etc/profile.d/proxy.sh` 

  - also added in `/etc/yum.conf` 

- EPEL repo added: `sudo yum install epel-release`  

  - verify: `sudo yum repolist` 

- basic net tools installed: `sudo yum whatprovides ifconfig` --> `sudo yum install net-tools` 

  - `sudo yum install nc` 

  - `sudo yum install telnet` 

- kernel updated to 3.10.0-957.21.2.el7: `sudo yum update kernel` 

- all-around updates: `sudo yum update` 

- **install RAID software**: `sudo yum install mdadm` --> # no longer needed b.c. of ZFS implementation


### Storage on MDS/MDT Server, `data1` 

> MDS storage is accessed in a database-like access pattern with many seeks and read-and-writes of small
amounts of data. Storage types that provide much lower seek times, such as SSD or NVMe is strongly
preferred for the MDT, and high-RPM SAS is acceptable.
For maximum performance, the MDT should be configured as RAID1 with an internal journal and two
disks from different controller  [page 25 of lustre deployment guide](http://doc.lustre.org/lustre_manual.pdf#%5B%7B%22num%22%3A390%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2C72%2C720%2Cnull%5D) 

- NVMe controller added from VirtualBox, and 4 6-GB virtual drives were added:

  - `sda` connects separately to the default SATA controller

```
[estrella@data1 ~]$ lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0   12G  0 disk 
├─sda1            8:1    0    1G  0 part /boot
└─sda2            8:2    0   11G  0 part 
  ├─centos-root 253:0    0  9.8G  0 lvm  /
  └─centos-swap 253:1    0  1.2G  0 lvm  [SWAP]
sr0              11:0    1 1024M  0 rom  
nvme0n1         259:0    0    6G  0 disk 
nvme0n2         259:1    0    6G  0 disk 
nvme0n3         259:2    0    6G  0 disk 
nvme0n4         259:3    0    6G  0 disk 

``` 

- **Downloading & Installing ZFS Kernel Module** 

  - [general xfs info 1](https://linuxhint.com/zfs_vs_xfs/) 

  - [general xfs info 2](https://www.datamation.com/data-center/the-zfs-story-clearing-up-the-confusion-1.html) 

  - [great documentation on installing & configuring xfs on RHEL7/CentOS7](https://linuxhint.com/install-zfs-centos7/) 

- check CentOS version: 

```
[estrella@data1 ~]$ cat /etc/redhat-release 
CentOS Linux release 7.6.1810 (Core) 

```

- add the appropriate zfs repo based on your OS version 

`sudo yum install http://download.zfsonlinux.org/epel/zfs-release.el7_6.noarch.rpm`

- disable the default dmks-zfs repository 

  - we wish to install the kABI-based ZFS kernel module, b.c. DKMS based ZFS module has to **rebuild** every time the kernel is updated

  - `sudo vi /etc/yum.repos.d/zfs.repo` --> change `enabled=1` to `enabled=0` under the `dkms` section 

  - do the reverse, change  `enable=0` to `enable=1` under the `kmod` section 

  - save --> VERIFY the updated repo

```
[estrella@data1 ~]$ yum repolist
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: centos.mirror.lstn.net
 * epel: fedora-epel.mirror.lstn.net
 * extras: centos.mirror.lstn.net
 * updates: centos.mirror.lstn.net
repo id                                             repo name                                                                         status
base/7/x86_64                                       CentOS-7 - Base                                                                   10,019
epel/x86_64                                         Extra Packages for Enterprise Linux 7 - x86_64                                    13,231
extras/7/x86_64                                     CentOS-7 - Extras                                                                    409
updates/7/x86_64                                    CentOS-7 - Updates                                                                 2,076
zfs-kmod/x86_64                                     ZFS on Linux for EL7 - kmod                                                           34
repolist: 25,769

```


- `sudo yum install zfs` --> `kmod-zfs`should be installed (as opposed to `dkms-zfs`) 

```
============================================================================================================================================
 Package                          Arch                    Version                                           Repository                 Size
============================================================================================================================================
Installing:
 zfs                              x86_64                  0.7.13-1.el7_6                                    zfs-kmod                  416 k
Installing for dependencies:
 kmod-spl                         x86_64                  0.7.13-1.el7_6                                    zfs-kmod                  114 k
 kmod-zfs                         x86_64                  0.7.13-1.el7_6                                    zfs-kmod                  896 k
 libnvpair1                       x86_64                  0.7.13-1.el7_6                                    zfs-kmod                   31 k
 libuutil1                        x86_64                  0.7.13-1.el7_6                                    zfs-kmod                   36 k
 libzfs2                          x86_64                  0.7.13-1.el7_6                                    zfs-kmod                  131 k
 libzpool2                        x86_64                  0.7.13-1.el7_6                                    zfs-kmod                  595 k
 lm_sensors-libs                  x86_64                  3.4.0-6.20160601gitf9185e5.el7                    base                       42 k
 spl                              x86_64                  0.7.13-1.el7_6                                    zfs-kmod                   29 k
 sysstat                          x86_64                  10.1.5-17.el7                                     base                      315 k

Transaction Summary
============================================================================================================================================
Install  1 Package (+9 Dependent packages)

```
 
.
.
.

- success! 

```
Installed:
  zfs.x86_64 0:0.7.13-1.el7_6                                                                                                               

Dependency Installed:
  kmod-spl.x86_64 0:0.7.13-1.el7_6                             kmod-zfs.x86_64 0:0.7.13-1.el7_6      libnvpair1.x86_64 0:0.7.13-1.el7_6     
  libuutil1.x86_64 0:0.7.13-1.el7_6                            libzfs2.x86_64 0:0.7.13-1.el7_6       libzpool2.x86_64 0:0.7.13-1.el7_6      
  lm_sensors-libs.x86_64 0:3.4.0-6.20160601gitf9185e5.el7      spl.x86_64 0:0.7.13-1.el7_6           sysstat.x86_64 0:10.1.5-17.el7         

Complete!
[estrella@data1 ~]$ 

```
### NOTE: THE kmod-zfs DOES NOT WORK WELL with Lustre! DO NOT change dkms-zfs to kmod-zfs as suggested from above!

- **reboot** & check zfs-kernel module 

```
lsmod | grep zfs

[estrella@data1 ~]$ lsmod | grep zfs
zfs                  3564425  3 
zunicode              331170  1 zfs
zavl                   15236  1 zfs
icp                   270148  1 zfs
zcommon                73440  1 zfs
znvpair                89131  2 zfs,zcommon
spl                   102412  4 icp,zfs,zcommon,znvpair

```

- **set up ZFS software RAID** 

  - ZFS can function analogously to LVM by creating "zpools" that span +1 drives 

  - ZFS does not HAVE to work with disk partitions

  - there is no need for RAID controllers to present disks as any sort of hardware RAID, ZFS itseld can enforce storage integrity just as well, if not better, than most physical RAID controller [read up on this linuxjournal article](https://www.linuxjournal.com/content/zfs-linux)  

  - let the server controller present disks in the JBOD configuration for ZFS for best RAID performance

  - [read up on the technical details of ZFS](https://www.linuxjournal.com/content/zfs-linux)

  - [an 8 chapter SysAdmin handbook on ZFS!](https://www.freebsd.org/doc/handbook/zfs.html) --> really helpful~ (e.g. answers fundamental questions such as "da fuq is a vdev?")  


- create a LVM-like RAID10 volume called a "zpool" 

	we are essentially creating a striped volume consisting of a pair of mirrored (RAID1) disks 

  - `which zpool` --> check available pools; at this point there should be none. 

- `sudo zpool create meta_tank mirror nvme0n1 nvme0n2 mirror nvme0n3 nvme0n4` --> should happen very fast for 4x6-G virtual drives

- VERIFY: 

```
[estrella@data1 ~]$ zpool list 

NAME        SIZE  ALLOC   FREE  EXPANDSZ   FRAG    CAP  DEDUP  HEALTH  ALTROOT
meta_tank  11.9G   274K  11.9G         -     0%     0%  1.00x  ONLINE  -

[estrella@data1 ~]$ df -Th
Filesystem              Type      Size  Used Avail Use% Mounted on
/dev/mapper/centos-root xfs       9.8G  2.2G  7.7G  22% /
devtmpfs                devtmpfs  1.9G     0  1.9G   0% /dev
tmpfs                   tmpfs     1.9G     0  1.9G   0% /dev/shm
tmpfs                   tmpfs     1.9G  8.6M  1.9G   1% /run
tmpfs                   tmpfs     1.9G     0  1.9G   0% /sys/fs/cgroup
/dev/sda1               xfs      1014M  188M  827M  19% /boot
tmpfs                   tmpfs     379M     0  379M   0% /run/user/1000
meta_tank               zfs        12G     0   12G   0% /meta_tank 

```  

- by default zpools are only writable by root; change its permissions:

  - `estrella@data1 ~]$ sudo chown -Rfv estrella:estrella /meta_tank` 

- more resources: 

  - [zpools & file systems 1](https://www.thegeekdiary.com/zfs-tutorials-creating-zfs-pools-and-file-systems/) 

  - [zpools & file systems 2](https://www.cyberciti.biz/faq/how-to-create-raid-10-striped-mirror-vdev-zpool-on-ubuntu-linux/) 

  - [zfs RAID levels explained](http://www.zfsbuild.com/2010/05/26/zfs-raid-levels/) 



### Steps to Install the Lustre Software

- [primary reference page](http://wiki.lustre.org/Installing_the_Lustre_Software) 

  - under "Lustre Servers with Both LDISKFS and ZFS OSD Support"

- **add the lustre repo**

```
sudo vi /etc/yum.repos.d/lustre.repo 

[lustre-server]
name=CentOS-$releasever - Lustre
baseurl=https://downloads.hpdd.intel.com/public/lustre/latest-feature-release/el7/server/
gpgcheck=0

[e2fsprogs]
name=CentOS-$releasever - Ldiskfs
baseurl=https://downloads.hpdd.intel.com/public/e2fsprogs/latest/el7
gpgcheck=0

[lustre-client]
name=CentOS-$releasever - Lustre
baseurl=https://downloads.hpdd.intel.com/public/lustre/latest-feature-release/el7/client/
gpgcheck=0

``` 

- install the lustre `e2fsprogs` distribution: 

```
sudo yum --nogpgcheck --disablerepo=* --enablerepo=e2fsprogs install e2fsprogs

Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
e2fsprogs                                                                                                            | 2.9 kB  00:00:00     
e2fsprogs/primary_db                                                                                                 | 7.5 kB  00:00:00     
Resolving Dependencies
--> Running transaction check
---> Package e2fsprogs.x86_64 0:1.42.9-13.el7 will be updated
---> Package e2fsprogs.x86_64 0:1.44.5.wc1-0.el7 will be an update
--> Processing Dependency: libss = 1.44.5.wc1-0.el7 for package: e2fsprogs-1.44.5.wc1-0.el7.x86_64
--> Processing Dependency: libcom_err(x86-64) = 1.44.5.wc1-0.el7 for package: e2fsprogs-1.44.5.wc1-0.el7.x86_64
--> Processing Dependency: e2fsprogs-libs(x86-64) = 1.44.5.wc1-0.el7 for package: e2fsprogs-1.44.5.wc1-0.el7.x86_64
--> Running transaction check
---> Package e2fsprogs-libs.x86_64 0:1.42.9-13.el7 will be updated
---> Package e2fsprogs-libs.x86_64 0:1.44.5.wc1-0.el7 will be an update
---> Package libcom_err.x86_64 0:1.42.9-13.el7 will be updated
---> Package libcom_err.x86_64 0:1.44.5.wc1-0.el7 will be an update
---> Package libss.x86_64 0:1.42.9-13.el7 will be updated
---> Package libss.x86_64 0:1.44.5.wc1-0.el7 will be an update
--> Finished Dependency Resolution

Dependencies Resolved

============================================================================================================================================
 Package                             Arch                        Version                               Repository                      Size
============================================================================================================================================
Updating:
 e2fsprogs                           x86_64                      1.44.5.wc1-0.el7                      e2fsprogs                      955 k
Updating for dependencies:
 e2fsprogs-libs                      x86_64                      1.44.5.wc1-0.el7                      e2fsprogs                      202 k
 libcom_err                          x86_64                      1.44.5.wc1-0.el7                      e2fsprogs                       42 k
 libss                               x86_64                      1.44.5.wc1-0.el7                      e2fsprogs                       47 k

Transaction Summary
============================================================================================================================================
Upgrade  1 Package (+3 Dependent packages)

Total download size: 1.2 M
Is this ok [y/d/N]: y
Downloading packages:
Delta RPMs disabled because /usr/bin/applydeltarpm not installed.
(1/4): e2fsprogs-libs-1.44.5.wc1-0.el7.x86_64.rpm                                                                    | 202 kB  00:00:00     
(2/4): libcom_err-1.44.5.wc1-0.el7.x86_64.rpm                                                                        |  42 kB  00:00:00     
(3/4): libss-1.44.5.wc1-0.el7.x86_64.rpm                                                                             |  47 kB  00:00:00     
(4/4): e2fsprogs-1.44.5.wc1-0.el7.x86_64.rpm                                                                         | 955 kB  00:00:01     
--------------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                       1.1 MB/s | 1.2 MB  00:00:01     
Running transaction check
Running transaction test
Transaction test succeeded
Running transaction
  Updating   : libcom_err-1.44.5.wc1-0.el7.x86_64                                                                                       1/8 
  Updating   : e2fsprogs-libs-1.44.5.wc1-0.el7.x86_64                                                                                   2/8 
  Updating   : libss-1.44.5.wc1-0.el7.x86_64                                                                                            3/8 
  Updating   : e2fsprogs-1.44.5.wc1-0.el7.x86_64                                                                                        4/8 
  Cleanup    : e2fsprogs-1.42.9-13.el7.x86_64                                                                                           5/8 
  Cleanup    : e2fsprogs-libs-1.42.9-13.el7.x86_64                                                                                      6/8 
  Cleanup    : libss-1.42.9-13.el7.x86_64                                                                                               7/8 
  Cleanup    : libcom_err-1.42.9-13.el7.x86_64                                                                                          8/8 
  Verifying  : e2fsprogs-libs-1.44.5.wc1-0.el7.x86_64                                                                                   1/8 
  Verifying  : e2fsprogs-1.44.5.wc1-0.el7.x86_64                                                                                        2/8 
  Verifying  : libcom_err-1.44.5.wc1-0.el7.x86_64                                                                                       3/8 
  Verifying  : libss-1.44.5.wc1-0.el7.x86_64                                                                                            4/8 
  Verifying  : libss-1.42.9-13.el7.x86_64                                                                                               5/8 
  Verifying  : libcom_err-1.42.9-13.el7.x86_64                                                                                          6/8 
  Verifying  : e2fsprogs-libs-1.42.9-13.el7.x86_64                                                                                      7/8 
  Verifying  : e2fsprogs-1.42.9-13.el7.x86_64                                                                                           8/8 

Updated:
  e2fsprogs.x86_64 0:1.44.5.wc1-0.el7                                                                                                       

Dependency Updated:
  e2fsprogs-libs.x86_64 0:1.44.5.wc1-0.el7          libcom_err.x86_64 0:1.44.5.wc1-0.el7          libss.x86_64 0:1.44.5.wc1-0.el7         

Complete!


``` 

- Install **lustre-patched** kernel packages; enable only the Lustre repo: 

```
sudo yum --nogpgcheck --disablerepo=base,extras,updates --enablerepo=lustre-server install kernel kernel-devel kernel-headers kernel-tools kernel-tools-libs kernel-tools-libs-devel
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * epel: fedora-epel.mirror.lstn.net
lustre-client                                                                                                        | 2.9 kB  00:00:00     
lustre-server                                                                                                        | 2.9 kB  00:00:00     
(1/2): lustre-client/primary_db                                                                                      |  55 kB  00:00:00     
(2/2): lustre-server/primary_db                                                                                      | 557 kB  00:00:00     
Package matching kernel-headers-3.10.0-957.el7_lustre.x86_64 already installed. Checking for update.
No package kernel-tools-libs-devel available.
Resolving Dependencies
--> Running transaction check
---> Package kernel.x86_64 0:3.10.0-957.el7_lustre will be installed
---> Package kernel-devel.x86_64 0:3.10.0-957.el7_lustre will be installed
--> Finished Dependency Resolution

Dependencies Resolved

============================================================================================================================================
 Package                         Arch                      Version                                   Repository                        Size
============================================================================================================================================
Installing:
 kernel                          x86_64                    3.10.0-957.el7_lustre                     lustre-server                     48 M
 kernel-devel                    x86_64                    3.10.0-957.el7_lustre                     lustre-server                     23 M

Transaction Summary
============================================================================================================================================
Install  2 Packages

Total download size: 72 M
Installed size: 141 M
Is this ok [y/d/N]: y
Downloading packages:
Delta RPMs disabled because /usr/bin/applydeltarpm not installed.
(1/2): kernel-devel-3.10.0-957.el7_lustre.x86_64.rpm                                                                 |  23 MB  00:00:09     
(2/2): kernel-3.10.0-957.el7_lustre.x86_64.rpm                                                                       |  48 MB  00:00:18     
--------------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                       3.9 MB/s |  72 MB  00:00:18     
Running transaction check
Running transaction test
Transaction test succeeded
Running transaction
  Installing : kernel-devel-3.10.0-957.el7_lustre.x86_64                                                                                1/2 
  Installing : kernel-3.10.0-957.el7_lustre.x86_64                                                                                      2/2 
  Verifying  : kernel-3.10.0-957.el7_lustre.x86_64                                                                                      1/2 
  Verifying  : kernel-devel-3.10.0-957.el7_lustre.x86_64                                                                                2/2 

Installed:
  kernel.x86_64 0:3.10.0-957.el7_lustre                             kernel-devel.x86_64 0:3.10.0-957.el7_lustre                            

Complete!

```


- Generate a persistent hostid on the machine, if one does not already exist. This is needed to help protect ZFS zpools against simultaneous imports on multiple servers 

```

which genhostid   # if not available, then install it

sudo genhostid 

# see if the two commands below return the same outputs

od -An -tx /etc/hostid 

hostid


``` 

- reboot node: `sudo reboot` 

  - UPON reboot, the previously created zpool was gone? --> how to make it persistent? 

```

[estrella@data1 ~]$ lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0   12G  0 disk 
├─sda1            8:1    0    1G  0 part /boot
└─sda2            8:2    0   11G  0 part 
  ├─centos-root 253:0    0  9.8G  0 lvm  /
  └─centos-swap 253:1    0  1.2G  0 lvm  [SWAP]
sr0              11:0    1 1024M  0 rom  
nvme0n1         259:0    0    6G  0 disk 
├─nvme0n1p1     259:1    0    6G  0 part 
└─nvme0n1p9     259:2    0    8M  0 part 
nvme0n2         259:3    0    6G  0 disk 
├─nvme0n2p1     259:4    0    6G  0 part 
└─nvme0n2p9     259:5    0    8M  0 part 
nvme0n3         259:6    0    6G  0 disk 
├─nvme0n3p1     259:7    0    6G  0 part 
└─nvme0n3p9     259:8    0    8M  0 part 
nvme0n4         259:9    0    6G  0 disk 
├─nvme0n4p1     259:10   0    6G  0 part 
└─nvme0n4p9     259:11   0    8M  0 part 

### nothing was mounted but the previous raw disks have already been partitioned

``` 



- install lustre software 

sudo yum --nogpgcheck --enablerepo=lustre-server install kmod-lustre-osd-ldiskfs lustre-osd-ldiskfs-mount lustre-osd-zfs-mount lustre lustre-resource-agents

```
============================================================================================================================================
 Package                                   Arch                    Version                             Repository                      Size
============================================================================================================================================
Installing:
 kmod-lustre-osd-ldiskfs                   x86_64                  2.12.0-1.el7                        lustre-server                  478 k
 lustre                                    x86_64                  2.12.0-1.el7                        lustre-server                  767 k
 lustre-osd-ldiskfs-mount                  x86_64                  2.12.0-1.el7                        lustre-server                   14 k
 lustre-osd-zfs-mount                      x86_64                  2.12.0-1.el7                        lustre-server                   12 k
 lustre-resource-agents                    x86_64                  2.12.0-1.el7                        lustre-server                  7.4 k
Installing for dependencies:
 bc                                        x86_64                  1.06.95-13.el7                      base                           115 k
 cifs-utils                                x86_64                  6.2-10.el7                          base                            85 k
 cups-libs                                 x86_64                  1:1.6.3-35.el7                      base                           357 k
 gssproxy                                  x86_64                  0.7.0-21.el7                        base                           109 k
 keyutils                                  x86_64                  1.5.8-3.el7                         base                            54 k
 kmod-lustre                               x86_64                  2.12.0-1.el7                        lustre-server                  4.0 M
 libbasicobjects                           x86_64                  0.1.1-32.el7                        base                            26 k
 libcollection                             x86_64                  0.7.0-32.el7                        base                            42 k
 libevent                                  x86_64                  2.0.21-4.el7                        base                           214 k
 libini_config                             x86_64                  1.3.1-32.el7                        base                            64 k
 libldb                                    x86_64                  1.3.4-1.el7                         base                           137 k
 libnfsidmap                               x86_64                  0.25-19.el7                         base                            50 k
 libpath_utils                             x86_64                  0.2.1-32.el7                        base                            28 k
 libref_array                              x86_64                  0.1.5-32.el7                        base                            27 k
 libtalloc                                 x86_64                  2.1.13-1.el7                        base                            32 k
 libtdb                                    x86_64                  1.3.15-1.el7                        base                            48 k
 libtevent                                 x86_64                  0.9.36-1.el7                        base                            36 k
 libtirpc                                  x86_64                  0.2.4-0.15.el7                      base                            89 k
 libverto-libevent                         x86_64                  0.2.5-4.el7                         base                           8.9 k
 libwbclient                               x86_64                  4.8.3-4.el7                         base                           109 k
 libyaml                                   x86_64                  0.1.4-11.el7_0                      base                            55 k
 nfs-utils                                 x86_64                  1:1.3.0-0.61.el7                    base                           410 k
 psmisc                                    x86_64                  22.20-15.el7                        base                           141 k
 quota                                     x86_64                  1:4.01-17.el7                       base                           179 k
 quota-nls                                 noarch                  1:4.01-17.el7                       base                            90 k
 resource-agents                           x86_64                  4.1.1-12.el7_6.8                    updates                        449 k
 rpcbind                                   x86_64                  0.2.0-47.el7                        base                            60 k
 samba-client-libs                         x86_64                  4.8.3-4.el7                         base                           4.8 M
 samba-common                              noarch                  4.8.3-4.el7                         base                           206 k
 samba-common-libs                         x86_64                  4.8.3-4.el7                         base                           164 k
 tcp_wrappers                              x86_64                  7.6-77.el7                          base                            78 k

Transaction Summary
============================================================================================================================================
Install  5 Packages (+31 Dependent packages)

Total download size: 13 M
Installed size: 53 M


```



- Verify: 

```

[estrella@data1 ~]$ sudo modprobe -v zfs
[sudo] password for estrella: 
modprobe: FATAL: Module zfs not found.
[estrella@data1 ~]$ 
[estrella@data1 ~]$ 
[estrella@data1 ~]$ 
[estrella@data1 ~]$ sudo modprobe -v lustre   ### lustre module is successfully configured
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/net/libcfs.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/net/lnet.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/obdclass.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/ptlrpc.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/fld.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/fid.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/lov.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/osc.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/mdc.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/lmv.ko 
insmod /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/lustre/fs/lustre.ko 


``` 



## BUG fixes for kmod-zfs incompatibility with the lustre kernel 

- I guess working with dkms-zfs is more straight-forward 

  - `sudo yum remove zfs` 

  - `sudo yum remove kmod-spl` 

  - go to `/etc/yum.repos.d/zfs.repo` --> disable kmod-zfs & enable the default dkms-zfs back! 

- re-install default dkms-zfs packages along with lustre kernel dependencies: 

```
[estrella@data1 ~]$ sudo yum --nogpgcheck --enablerepo=lustre-server install lustre-dkms zfs
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: centos.mirror.lstn.net
 * epel: fedora-epel.mirrors.tds.net
 * extras: centos.mirror.lstn.net
 * updates: centos.mirror.lstn.net
Resolving Dependencies
--> Running transaction check
---> Package lustre-zfs-dkms.noarch 0:2.12.0-1.el7 will be installed
--> Processing Dependency: zfs-dkms >= 0.6.5 for package: lustre-zfs-dkms-2.12.0-1.el7.noarch
--> Processing Dependency: spl-dkms >= 0.6.5 for package: lustre-zfs-dkms-2.12.0-1.el7.noarch
--> Processing Dependency: dkms >= 2.2.0.3-28.git.7c3e7c5 for package: lustre-zfs-dkms-2.12.0-1.el7.noarch
--> Processing Dependency: libyaml-devel for package: lustre-zfs-dkms-2.12.0-1.el7.noarch
--> Processing Dependency: /usr/bin/expect for package: lustre-zfs-dkms-2.12.0-1.el7.noarch
---> Package zfs.x86_64 0:0.7.13-1.el7_6 will be installed
--> Processing Dependency: spl = 0.7.13 for package: zfs-0.7.13-1.el7_6.x86_64
--> Running transaction check
---> Package dkms.noarch 0:2.7.1-1.el7 will be installed
--> Processing Dependency: elfutils-libelf-devel for package: dkms-2.7.1-1.el7.noarch
---> Package expect.x86_64 0:5.45-14.el7_1 will be installed
--> Processing Dependency: libtcl8.5.so()(64bit) for package: expect-5.45-14.el7_1.x86_64
---> Package libyaml-devel.x86_64 0:0.1.4-11.el7_0 will be installed
---> Package spl.x86_64 0:0.7.13-1.el7_6 will be installed
---> Package spl-dkms.noarch 0:0.7.13-1.el7_6 will be installed
---> Package zfs-dkms.noarch 0:0.7.13-1.el7_6 will be installed
--> Running transaction check
---> Package elfutils-libelf-devel.x86_64 0:0.172-2.el7 will be installed
--> Processing Dependency: pkgconfig(zlib) for package: elfutils-libelf-devel-0.172-2.el7.x86_64
---> Package tcl.x86_64 1:8.5.13-8.el7 will be installed
--> Running transaction check
---> Package zlib-devel.x86_64 0:1.2.7-18.el7 will be installed
--> Finished Dependency Resolution

Dependencies Resolved

============================================================================================================================================
 Package                                 Arch                     Version                             Repository                       Size
============================================================================================================================================
Installing:
 lustre-zfs-dkms                         noarch                   2.12.0-1.el7                        lustre-server                    12 M
 zfs                                     x86_64                   0.7.13-1.el7_6                      zfs                             416 k
Installing for dependencies:
 dkms                                    noarch                   2.7.1-1.el7                         epel                             75 k
 elfutils-libelf-devel                   x86_64                   0.172-2.el7                         base                             39 k
 expect                                  x86_64                   5.45-14.el7_1                       base                            262 k
 libyaml-devel                           x86_64                   0.1.4-11.el7_0                      base                             82 k
 spl                                     x86_64                   0.7.13-1.el7_6                      zfs                              29 k
 spl-dkms                                noarch                   0.7.13-1.el7_6                      zfs                             458 k
 tcl                                     x86_64                   1:8.5.13-8.el7                      base                            1.9 M
 zfs-dkms                                noarch                   0.7.13-1.el7_6                      zfs                             4.9 M
 zlib-devel                              x86_64                   1.2.7-18.el7                        base                             50 k

Transaction Summary
============================================================================================================================================
Install  2 Packages (+9 Dependent packages)

``` 
- Installation Complete, but the `zfs-mount.service` from the previous kmod-zfs is still here? 

```
[estrella@data1 ~]$ sudo systemctl status zfs-mount.service
[sudo] password for estrella: 
● zfs-mount.service - Mount ZFS filesystems
   Loaded: loaded (/usr/lib/systemd/system/zfs-mount.service; enabled; vendor preset: enabled)
   Active: failed (Result: exit-code) since Fri 2019-06-14 22:35:40 CDT; 53min ago
 Main PID: 3278 (code=exited, status=1/FAILURE)

Jun 14 22:35:30 data1.biohpc.new systemd[1]: Starting Mount ZFS filesystems...
Jun 14 22:35:40 data1.biohpc.new zfs[3278]: /dev/zfs and /proc/self/mounts are required.
Jun 14 22:35:40 data1.biohpc.new zfs[3278]: Try running 'udevadm trigger' and 'mount -t proc proc /proc' as root.
Jun 14 22:35:40 data1.biohpc.new systemd[1]: zfs-mount.service: main process exited, code=exited, status=1/FAILURE
Jun 14 22:35:40 data1.biohpc.new systemd[1]: Failed to start Mount ZFS filesystems.
Jun 14 22:35:40 data1.biohpc.new systemd[1]: Unit zfs-mount.service entered failed state.
Jun 14 22:35:40 data1.biohpc.new systemd[1]: zfs-mount.service failed.

```

- this time around `modprobe -v zfs` works 

```
[estrella@data1 ~]$ zpool list
no pools available
[estrella@data1 ~]$ 
[estrella@data1 ~]$ zpool status
no pools available
[estrella@data1 ~]$ 
[estrella@data1 ~]$ modinfo zfs
filename:       /lib/modules/3.10.0-957.el7_lustre.x86_64/extra/zfs.ko.xz
version:        0.7.13-1
license:        CDDL
author:         OpenZFS on Linux
description:    ZFS
alias:          devname:zfs
alias:          char-major-10-249
retpoline:      Y
rhelversion:    7.6
srcversion:     CD1489EAB5842683DC3584F

```


### Setting Up LNET 


```
sudo vim /etc/modprobe.d/lnet.conf

options lnet networks=tcp0(eth1)


modprobe lnet 

lsmod | grep lent



sudo vim /etc/sysconfig/modules/lnet.modules

#!/bin/sh

if [ ! -c /dev/lnet ] ; then
    exec /sbin/modprobe lnet >/dev/null 2>&1
fi


sudo chmod 775 /etc/sysconfig/modules/lnet.modules   ### or else the script won't be run!

```

&nbsp;
----------------A Lustre Template VM (Clone) was Created Here-------------------
&nbsp; 

### Creating MGS System with ZFS Backend

[official lustre documentation](http://wiki.lustre.org/Creating_the_Lustre_Management_Service_(MGS)) 

```
sudo zpool create -O canmount=off -o cachefile=none mgspool mirror nvme0n1 nvme0n2


[estrella@data1 ~]$ zpool list
NAME      SIZE  ALLOC   FREE  EXPANDSZ   FRAG    CAP  DEDUP  HEALTH  ALTROOT
mgspool  3.97G  89.5K  3.97G         -     0%     0%  1.00x  ONLINE  -

[estrella@data1 ~]$ zpool status
  pool: mgspool
 state: ONLINE
  scan: none requested
config:

	NAME         STATE     READ WRITE CKSUM
	mgspool      ONLINE       0     0     0
	  mirror-0   ONLINE       0     0     0
	    nvme0n1  ONLINE       0     0     0
	    nvme0n2  ONLINE       0     0     0

errors: No known data errors

# Verify Actions Taken On Disks:

[estrella@data1 ~]$ lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0   12G  0 disk 
├─sda1            8:1    0    1G  0 part /boot
└─sda2            8:2    0   11G  0 part 
  ├─centos-root 253:0    0  9.8G  0 lvm  /
  └─centos-swap 253:1    0  1.2G  0 lvm  [SWAP]
sr0              11:0    1 1024M  0 rom  
nvme0n1         259:0    0    4G  0 disk 
├─nvme0n1p1     259:4    0    4G  0 part 
└─nvme0n1p9     259:5    0    8M  0 part 
nvme0n2         259:1    0    4G  0 disk 
├─nvme0n2p1     259:6    0    4G  0 part 
└─nvme0n2p9     259:7    0    8M  0 part  # 2 disk-partitions created  
nvme0n3         259:2    0    4G  0 disk 
nvme0n4         259:3    0    4G  0 disk 


[estrella@data1 ~]$ sudo  mkfs.lustre --mgs --backfstype=zfs mgspool/mgt

   Permanent disk data:
Target:     MGS
Index:      unassigned
Lustre FS:  
Mount type: zfs
Flags:      0x64
              (MGS first_time update )
Persistent mount opts: 
Parameters:
checking for existing Lustre data: not found
mkfs_cmd = zfs create -o canmount=off  mgspool/mgt
  xattr=sa
  dnodesize=auto
Writing mgspool/mgt properties
  lustre:version=1
  lustre:flags=100
  lustre:index=65535
  lustre:svname=MGS



[estrella@data1 ~]$ zfs get all -s local
NAME         PROPERTY              VALUE                  SOURCE
mgspool      canmount              off                    local
mgspool/mgt  canmount              off                    local
mgspool/mgt  xattr                 sa                     local
mgspool/mgt  dnodesize             auto                   local
mgspool/mgt  lustre:index          65535                  local
mgspool/mgt  lustre:svname         MGS                    local
mgspool/mgt  lustre:flags          100                    local
mgspool/mgt  lustre:version        1                      local


[estrella@data1 /]$ zfs list
NAME          USED  AVAIL  REFER  MOUNTPOINT
mgspool       126K  3.84G    24K  /mgspool
mgspool/mgt    24K  3.84G    24K  /mgspool/mgt


sudo mkdir -p /lustre/mgt


sudo mount -t lustre mgspool/mgt /lustre/mgt/

### the above command is equivalent to `sudo mount.lustre ... ...`

[estrella@data1 /]$ df -Th
Filesystem              Type      Size  Used Avail Use% Mounted on
/dev/mapper/centos-root xfs       9.8G  2.4G  7.4G  25% /
devtmpfs                devtmpfs  1.9G     0  1.9G   0% /dev
tmpfs                   tmpfs     1.9G     0  1.9G   0% /dev/shm
tmpfs                   tmpfs     1.9G  8.7M  1.9G   1% /run
tmpfs                   tmpfs     1.9G     0  1.9G   0% /sys/fs/cgroup
/dev/sda1               xfs      1014M  232M  783M  23% /boot
tmpfs                   tmpfs     379M     0  379M   0% /run/user/1000
mgspool/mgt             lustre    3.9G  2.8M  3.9G   1% /lustre/mgt


### fetch Lustre device list:
[estrella@data1 /]$ sudo lctl dl
  0 UP osd-zfs MGS-osd MGS-osd_UUID 4
  1 UP mgs MGS MGS 4
  2 UP mgc MGC10.100.180.2@tcp c9faa979-4269-7500-cc86-86dd7876688d 4

```

#### Troubleshooting: ZFS pools disappearing after re-boot

[zfs canmount commands found here](https://docs.oracle.com/cd/E19253-01/819-5461/gdrcf/index.html) 

[more zfs mount related commands](https://www.cyberciti.biz/faq/freebsd-linux-unix-zfs-automatic-mount-points-command/) 

[zfs cache file regenerate](http://zeeshanali.com/big-data/zfs-cache-file-regenerate/) 

[zfs legacy mount via /etc/fstab option](https://superuser.com/questions/1248622/zfs-pool-disappears-after-reboot-on-debian-8) 

[lustre mount syntax](http://manpages.ubuntu.com/manpages/precise/man8/mount.lustre.8.html) 

[lustre mount syntax 2](http://wiki.lustre.org/Mounting_a_Lustre_File_System_on_Client_Nodes) 

```
# issue found with NAT-network connecting to VM Host 

sudo vim /etc/sysconfig/network-scripts/ifcfg-eth0 

# changed "DEVICE" & "NAME" entry both to "enp0s3" 

sudo systemctl restart network 

# change previously "canmount=off" to "canmount=on" 

[estrella@data1 ~]$ zfs get all -s local           # checking on previous status
NAME         PROPERTY              VALUE                  SOURCE
mgspool      canmount              off                    local
mgspool/mgt  canmount              off                    local
mgspool/mgt  xattr                 sa                     local
mgspool/mgt  dnodesize             auto                   local
mgspool/mgt  lustre:index          65535                  local
mgspool/mgt  lustre:svname         MGS                    local
mgspool/mgt  lustre:flags          36                     local
mgspool/mgt  lustre:version        1                      local


sudo zfs set canmount=on mgspool 

sudo zfs set canmount=on mgspool/mgt 


zfs get all -s local                               # verify the change 

[estrella@data1 ~]$ zfs get all -s local
NAME         PROPERTY              VALUE                  SOURCE
mgspool      canmount              on                     local
mgspool/mgt  canmount              on                     local
mgspool/mgt  xattr                 sa                     local
mgspool/mgt  dnodesize             auto                   local
mgspool/mgt  lustre:index          65535                  local
mgspool/mgt  lustre:svname         MGS                    local
mgspool/mgt  lustre:flags          36                     local
mgspool/mgt  lustre:version        1                      local


# check the mount status of the lustre-backed pools 

[estrella@data1 ~]$ zfs get mounted mgspool
NAME     PROPERTY  VALUE    SOURCE
mgspool  mounted   no       -                       # not mounted


trella@data1 ~]$ zfs get mounted mgspool/mgt
NAME         PROPERTY  VALUE    SOURCE
mgspool/mgt  mounted   no       -                   # not mounted 


# designate mountpoints for `mgspool` & `mgspool/mgt`

[estrella@data1 ~]$ sudo zfs set mountpoint=/mgspool mgspool

[estrella@data1 ~]$ sudo zfs set mountpoint=/mgspool/mgt mgspool/mgt 

# Verify

[estrella@data1 ~]$ zfs get all -s local
NAME         PROPERTY              VALUE                  SOURCE
mgspool      mountpoint            /mgspool               local
mgspool      canmount              on                     local
mgspool/mgt  mountpoint            /mgspool/mgt           local
mgspool/mgt  canmount              on                     local
mgspool/mgt  xattr                 sa                     local
mgspool/mgt  dnodesize             auto                   local
mgspool/mgt  lustre:index          65535                  local
mgspool/mgt  lustre:svname         MGS                    local
mgspool/mgt  lustre:flags          36                     local
mgspool/mgt  lustre:version        1                      local

[estrella@data1 ~]$ sudo zfs get mountpoint mgspool
NAME     PROPERTY    VALUE       SOURCE
mgspool  mountpoint  /mgspool    local
[estrella@data1 ~]$ 
[estrella@data1 ~]$ 
[estrella@data1 ~]$ sudo zfs get mountpoint mgspool/mgt
NAME         PROPERTY    VALUE         SOURCE
mgspool/mgt  mountpoint  /mgspool/mgt  local


# force created a zpool.cache file: 

[estrella@data1 lustre]$ sudo zpool set cachefile=/etc/zfs/zpool.cache mgspool

[estrella@data1 lustre]$ sudo systemctl restart zfs-import-cache
[estrella@data1 lustre]$ 
[estrella@data1 lustre]$ sudo systemctl status zfs-import-cache   # works now
● zfs-import-cache.service - Import ZFS pools by cache file
   Loaded: loaded (/usr/lib/systemd/system/zfs-import-cache.service; enabled; vendor preset: enabled)
   Active: active (exited) since Mon 2019-06-17 21:10:31 CDT; 4s ago
  Process: 16341 ExecStart=/sbin/zpool import -c /etc/zfs/zpool.cache -aN (code=exited, status=0/SUCCESS)
 Main PID: 16341 (code=exited, status=0/SUCCESS)

# Tried Mounting: `mgspool` created its local directory, but `mgspool/mgt` didn't because it's been mounted by the lustre 

[estrella@data1 ~]$ sudo zfs mount -a
filesystem 'mgspool/mgt' is already mounted
cannot mount 'mgspool/mgt': mountpoint or dataset is busy


# disabled mount for `mgspool/mgt` & left `mgspool` mounted

[estrella@data1 lustre]$ sudo zfs set canmount=off mgspool/mgt
[estrella@data1 lustre]$ 
[estrella@data1 lustre]$ zfs get all -s local
NAME         PROPERTY              VALUE                  SOURCE
mgspool      mountpoint            /mgspool               local
mgspool      canmount              on                     local
mgspool/mgt  mountpoint            /mgspool/mgt           local
mgspool/mgt  canmount              off                    local
mgspool/mgt  xattr                 sa                     local
mgspool/mgt  dnodesize             auto                   local
mgspool/mgt  lustre:index          65535                  local
mgspool/mgt  lustre:svname         MGS                    local
mgspool/mgt  lustre:flags          36                     local
mgspool/mgt  lustre:version        1                      local

# mgspool mounted 

[estrella@data1 by-uuid]$ zfs mount
mgspool                         /mgspool

[estrella@data1 ~]$ cd /mgspool/
[estrella@data1 mgspool]$ ls
mgt

[estrella@data1 by-uuid]$ df -Th

mgspool                 zfs       3.9G     0  3.9G   0% /mgspool

# /lustre/mgt not accessible by design?

[estrella@data1 mgspool]$ cd /lustre/
[estrella@data1 lustre]$ ls -lah
total 0
drwxr-xr-x.  3 root root  17 Jun 17 17:14 .
dr-xr-xr-x. 19 root root 253 Jun 17 20:57 ..
d---------.  1 root root   0 Jun 17 16:23 mgt
[estrella@data1 lustre]$ 
[estrella@data1 lustre]$ ls -lah mgt/
ls: cannot access mgt/: Not a directory

# add lustre mount to `/etc/fstab`  # COMMENT: don't try this


# /etc/fstab
# Created by anaconda on Thu Jun 13 13:53:11 2019
#
# Accessible filesystems, by reference, are maintained under '/dev/disk'
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info
#
/dev/mapper/centos-root /                       xfs     defaults        0 0
UUID=905d67a4-abcb-4697-bc8f-e239272603cd /boot                   xfs     defaults        0 0
/dev/mapper/centos-swap swap                    swap    defaults        0 0
/mgspool/mgt	/lustre/mgt	lustre	defaults	0 0


## IF THIS DOESN'T WORK AFTER REBOOT --> follow legacy mount for `mgspool` ##

`/dev/disks`---> useful directory for fetching the proper UUID's

### After Reboot `mgspool/mgt` remained, but not mounted on `/lustre/mgt` ###

```
# Good News: Pool Integrity Remains

[estrella@data1 ~]$ zfs get all -s local
NAME         PROPERTY              VALUE                  SOURCE
mgspool      mountpoint            /mgspool               local
mgspool      canmount              on                     local
mgspool/mgt  mountpoint            /mgspool/mgt           local
mgspool/mgt  canmount              off                    local
mgspool/mgt  xattr                 sa                     local
mgspool/mgt  dnodesize             auto                   local
mgspool/mgt  lustre:index          65535                  local
mgspool/mgt  lustre:svname         MGS                    local
mgspool/mgt  lustre:flags          36                     local
mgspool/mgt  lustre:version        1                      local
[estrella@data1 ~]$ 


# needed to manually mount 

sudo mount.lustre mgspool/mgt /lustre/mgt/ 

sudo lctl dl 
  0 UP osd-zfs MGS-osd MGS-osd_UUID 4
  1 UP mgs MGS MGS 4
  2 UP mgc MGC10.100.180.2@tcp 26179e89-19b4-5a13-2c15-23672d60e7f2 4
[estrella@data1 ~]$ 
[estrella@data1 ~]$ 
[estrella@data1 ~]$ 
[estrella@data1 ~]$ lsmod | grep lnet
lnet                  595941  5 mgc,mgs,obdclass,ptlrpc,ksocklnd
libcfs                421295  10 fid,fld,mgc,mgs,lnet,lquota,obdclass,ptlrpc,osd_zfs,ksocklnd
[estrella@data1 ~]$ 
[estrella@data1 ~]$ 
[estrella@data1 ~]$ 
[estrella@data1 ~]$ lsmod | grep zfs
osd_zfs               372864  1 
lquota                368975  1 osd_zfs
fid                    90014  1 osd_zfs
fld                    89399  2 fid,osd_zfs
obdclass             1962422  8 fid,fld,mgc,mgs,lquota,ptlrpc,osd_zfs
zfs                  3564425  5 osd_zfs
zunicode              331170  1 zfs
zavl                   15236  1 zfs
icp                   270148  1 zfs
zcommon                73440  2 zfs,osd_zfs
znvpair                89131  3 zfs,zcommon,osd_zfs
spl                   102412  5 icp,zfs,zcommon,znvpair,osd_zfs
libcfs                421295  10 fid,fld,mgc,mgs,lnet,lquota,obdclass,ptlrpc,osd_zfs,ksocklnd

[estrella@data1 by-label]$ lsmod | grep lustre     # this doesn't start by default!
lustre                758679  0 
lmv                   177987  1 lustre
mdc                   232938  1 lustre
lov                   314581  1 lustre
ptlrpc               2264705  10 fid,fld,lmv,mdc,lov,mgc,mgs,osc,lquota,lustre
obdclass             1962422  13 fid,fld,lmv,mdc,lov,mgc,mgs,osc,lquota,lustre,ptlrpc,osd_zfs
lnet                  595941  8 lmv,mgc,mgs,osc,lustre,obdclass,ptlrpc,ksocklnd
libcfs                421295  15 fid,fld,lmv,mdc,lov,mgc,mgs,osc,lnet,lquota,lustre,obdclass,ptlrpc,osd_zfs,ksocklnd

# check services:

sudo systemctl status zfs-import-cache
sudo systemctl status zfs.target
sudo systemctl status zfs-mount.service   # all seem to work

# Reconfigure `/ereated by anaconda on Thu Jun 13 13:53:11 2019
#
# Accessible filesystems, by reference, are maintained under '/dev/disk'
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info
#
/dev/mapper/centos-root /                       xfs     defaults        0 0
UUID=905d67a4-abcb-4697-bc8f-e239272603cd /boot                   xfs     defaults        0 0
/dev/mapper/centos-swap swap                    swap    defaults        0 0
MGC10.100.180.2@tcp:mgspool/mgt	/lustre/mgt	lustre	defaults,_netdev	0 0
[estrella@data1 by-label]$ 


sudo vi /etc/sysconfig/lustre

uncomment, and modify: 

ZPOOL_IMPORT_DIR="/dev/disk/by-label"   #COMMENT: DOESN'T WORK

# error message

[estrella@data1 by-label]$ sudo mount -a
mount.lustre: MGC10.100.180.2@tcp:mgspool/mgt has not been formatted with mkfs.lustre or the backend filesystem type is not supported by this tool


###### Tentative Solution: Startup Script Mounting ####### 


####################### RECAP ############################

# Once the zpool & its attached lustre database is established, set the zpool's `canmount` option to `on`. But for the lustre filesystem (or database) on the pool, i.e. `/<pool name>/<location of lustre fs>` (e.g. `/mgspool/mgt`), set the option to OFF! 

# Make sure to SET A PERSISTENT MOUNT POINT for the created zpool (via `sudo zfs mountpoint=<directory/sub-directory/..> <zpool name>`)

# Also make sure to ASSIGN A CACHE FILE to the newly created zpool (via `sudo zfs set cachefile=/etc/zfs/zpool.cache <zpool name>`) 

- avoid the `-o cachefile=none` option when creating the zpool as stated in the Lustre's official documentation... 

```

## Refer to `lustre_backend_2.md` for continuation of this test deployment 



#################### REVISION ##########################

- Re-configuring the MGS: Initial Setup (off of the "lustre-template" VM Snapshot)

```
# Setting up Guest Additions 

# Insert "Virtual Box Guest Additions" from the VirtualBox window

[estrella@lamella_t1 ~]$ sudo mkdir /media/cdrom

[estrella@lamella_t1 ~]$ sudo mount /dev/cdrom /media/cdrom/
mount: /dev/sr0 is write-protected, mounting read-only

[estrella@lamella_t1 ~]$ ls /media/cdrom/
32Bit  AUTORUN.INF  cert  runasroot.sh            VBoxSolarisAdditions.pkg        VBoxWindowsAdditions.exe
64Bit  autorun.sh   OS2   VBoxLinuxAdditions.run  VBoxWindowsAdditions-amd64.exe  VBoxWindowsAdditions-x86.exe

[root@lamella_t1 ~]# cd /media/cdrom/
[root@lamella_t1 cdrom]# ./VBoxLinuxAdditions.run
Verifying archive integrity... All good.
Uncompressing VirtualBox 5.1.22 Guest Additions for Linux...........
VirtualBox Guest Additions installer
Copying additional installer modules ...
Installing additional modules ...
vboxadd.sh: Starting the VirtualBox Guest Additions.

# Verify 

[root@lamella_t1 cdrom]# modinfo vboxguest
filename:       /lib/modules/3.10.0-957.el7_lustre.x86_64/misc/vboxguest.ko
version:        5.1.22 r115126
license:        GPL
description:    Oracle VM VirtualBox Guest Additions for Linux Module
author:         Oracle Corporation
retpoline:      Y
rhelversion:    7.6
srcversion:     E747E9F0126BD23943BF4B1
alias:          pci:v000080EEd0000CAFEsv00000000sd00000000bc*sc*i*
depends:        
vermagic:       3.10.0-957.el7_lustre.x86_64 SMP mod_unload modversions 

# deleted previous lustre mount entry in /etc/fstab, deleted /etc/sysconfig/modules/lustre.modules

# installing ntp daemon

[estrella@lamella_t1 ~]$ sudo yum install ntp
Installed:
  ntp.x86_64 0:4.2.6p5-28.el7.centos                                                                                                        

Dependency Installed:
  autogen-libopts.x86_64 0:5.18-5.el7                                 ntpdate.x86_64 0:4.2.6p5-28.el7.centos                                

Complete!

# Verify & Open UDP port 123 for NTP 

[estrella@lamella_t1 ~]$ which ntpdate
/usr/sbin/ntpdate

[estrella@lamella_t1 ~]$ sudo firewall-cmd --add-service=ntp --permanent
success

[estrella@lamella_t1 ~]$ sudo firewall-cmd --reload
success

[estrella@lamella_t1 ~]$ sudo firewall-cmd --list-services
ssh dhcpv6-client ntp


# enable & start ntpd & see time difference between NTP servers 

[estrella@lamella_t1 ~]$ sudo systemctl enable ntpd
Created symlink from /etc/systemd/system/multi-user.target.wants/ntpd.service to /usr/lib/systemd/system/ntpd.service.
[estrella@lamella_t1 ~]$ sudo systemctl start ntpd

[estrella@lamella_t1 ~]$ sudo systemctl status ntpd
● ntpd.service - Network Time Service
   Loaded: loaded (/usr/lib/systemd/system/ntpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2019-06-25 17:08:04 CDT; 11s ago
  Process: 8970 ExecStart=/usr/sbin/ntpd -u ntp:ntp $OPTIONS (code=exited, status=0/SUCCESS)
 Main PID: 8971 (ntpd)
   CGroup: /system.slice/ntpd.service
           └─8971 /usr/sbin/ntpd -u ntp:ntp -g

Jun 25 17:08:04 lamella_t1.biohpc.new ntpd[8971]: Listen normally on 4 eth1 10.100.180.2 UDP 123
Jun 25 17:08:04 lamella_t1.biohpc.new ntpd[8971]: Listen normally on 5 lo ::1 UDP 123
Jun 25 17:08:04 lamella_t1.biohpc.new ntpd[8971]: Listen normally on 6 enp0s3 fe80::fa4c:234c:6373:61cb UDP 123
Jun 25 17:08:04 lamella_t1.biohpc.new ntpd[8971]: Listening on routing socket on fd #23 for interface updates
Jun 25 17:08:05 lamella_t1.biohpc.new ntpd[8971]: 0.0.0.0 c016 06 restart
Jun 25 17:08:05 lamella_t1.biohpc.new ntpd[8971]: 0.0.0.0 c012 02 freq_set kernel 0.000 PPM
Jun 25 17:08:05 lamella_t1.biohpc.new ntpd[8971]: 0.0.0.0 c011 01 freq_not_set
Jun 25 17:08:11 lamella_t1.biohpc.new ntpd[8971]: 0.0.0.0 c61c 0c clock_step +1.001916 s
Jun 25 17:08:12 lamella_t1.biohpc.new ntpd[8971]: 0.0.0.0 c614 04 freq_mode
Jun 25 17:08:13 lamella_t1.biohpc.new ntpd[8971]: 0.0.0.0 c618 08 no_sys_peer

# Check local machine time 

[estrella@lamella_t1 ~]$ sudo date -R
Tue, 25 Jun 2019 17:09:32 -0500

# Offset with NTP servers
[estrella@lamella_t1 ~]$ sudo ntpq -p
     remote           refid      st t when poll reach   delay   offset  jitter
==============================================================================
+darwin.kenyonra 127.67.113.92    2 u   15   64    1   41.272    0.488   0.170
*ntp3.junkemailf 216.218.254.202  2 u   14   64    1   39.708   -3.852   0.359
+time.mclarkdev. 128.100.100.128  3 u   13   64    1   39.920   -1.444   0.252
-horp-bsd01.horp 152.2.133.54     2 u   44   64    1   27.752    3.126   0.030

###### Remember to Install NTPD on ALL OTHER SERVERS ######

```

### Re-configuring the MGS: ZFS pool & Lustre Filesystem 

```

# Create Zpool (a logical volume similar to VG/LV) 

  # NOTE: the `cachefile=/etc/zfs/zpool.cache` option is critical to the integrity of the zpool; if it is missing, upon system reboot, the zpool will disappear  (the cache file can be arbitrarily named, i.e. "mypool.cache")

  # `mirror <disklabel> <disklabel> mirror <disklabel> <disklabel>` generates a RAID10 array (striped mirror) 

  # to expand the size of the RAID10 poool, simply execute `sudo zpool add mirror <disklabel> <disklabel>`, which adds an additional mirrored pair to be striped with the current array

[estrella@lamella_t1 zfs]$ sudo zpool create -O canmount=off -o cachefile=/etc/zfs/zpool.cache mgs1pool mirror nvme0n1 nvme0n2 mirror nvme0n3 nvme0n4

# Verify

lsblk   # if pools were created from JBOD, then partitions will be formed on each disk 

[estrella@lamella_t1 zfs]$ lsblk

nvme0n1         259:0    0    3G  0 disk 
├─nvme0n1p1     259:4    0    3G  0 part 
└─nvme0n1p9     259:5    0    8M  0 part 
nvme0n2         259:1    0    3G  0 disk 
├─nvme0n2p1     259:6    0    3G  0 part 
└─nvme0n2p9     259:7    0    8M  0 part 
nvme0n3         259:2    0    3G  0 disk 
├─nvme0n3p1     259:8    0    3G  0 part 
└─nvme0n3p9     259:9    0    8M  0 part 
nvme0n4         259:3    0    3G  0 disk 
├─nvme0n4p1     259:10   0    3G  0 part 
└─nvme0n4p9     259:11   0    8M  0 part 

[estrella@lamella_t1 zfs]$ zpool list
NAME       SIZE  ALLOC   FREE  EXPANDSZ   FRAG    CAP  DEDUP  HEALTH  ALTROOT
mgs1pool  5.97G   120K  5.97G         -     0%     0%  1.00x  ONLINE  -
[estrella@lamella_t1 zfs]$ 
[estrella@lamella_t1 zfs]$ 
[estrella@lamella_t1 zfs]$ zpool status
  pool: mgs1pool
 state: ONLINE
  scan: none requested
config:

	NAME         STATE     READ WRITE CKSUM
	mgs1pool     ONLINE       0     0     0
	  mirror-0   ONLINE       0     0     0
	    nvme0n1  ONLINE       0     0     0
	    nvme0n2  ONLINE       0     0     0
	  mirror-1   ONLINE       0     0     0
	    nvme0n3  ONLINE       0     0     0
	    nvme0n4  ONLINE       0     0     0

# Trouble-shooting ZFS, check the following services: 

`sudo systemctl status zfs-import-cache`   # looks for cache file needed to import a zpool upon boot

`sudo systemctl status zfs-mount.service`

`sudo systemctl status zfs.target` 

# Make Lustre Filesystem on top of the `mgs1pool` 

[estrella@lamella_t1 ~]$ sudo mkfs.lustre --mgs --backfstype=zfs mgs1pool/mgt1
[sudo] password for estrella: 

   Permanent disk data:
Target:     MGS
Index:      unassigned
Lustre FS:  
Mount type: zfs
Flags:      0x64
              (MGS first_time update )
Persistent mount opts: 
Parameters:
checking for existing Lustre data: not found
mkfs_cmd = zfs create -o canmount=off  mgs1pool/mgt1  # a mgt target is created
  xattr=sa
  dnodesize=auto
Writing mgs1pool/mgt1 properties
  lustre:version=1
  lustre:flags=100
  lustre:index=65535
  lustre:svname=MGS

# Verify

[estrella@lamella_t1 ~]$ zfs get all -s local
NAME           PROPERTY              VALUE                  SOURCE
mgs1pool       canmount              off                    local
mgs1pool/mgt1  canmount              off                    local
mgs1pool/mgt1  xattr                 sa                     local
mgs1pool/mgt1  dnodesize             auto                   local  
mgs1pool/mgt1  lustre:flags          100                    local  #zfs recognizes lustre fs built on top of it
mgs1pool/mgt1  lustre:svname         MGS                    local
mgs1pool/mgt1  lustre:version        1                      local
mgs1pool/mgt1  lustre:index          65535                  local
[estrella@lamella_t1 ~]$ 
[estrella@lamella_t1 ~]$ 
[estrella@lamella_t1 ~]$ 
[estrella@lamella_t1 ~]$ zfs list
NAME            USED  AVAIL  REFER  MOUNTPOINT   # disregard mount point!
mgs1pool        134K  5.78G    24K  /mgs1pool
mgs1pool/mgt1    24K  5.78G    24K  /mgs1pool/mgt1

# NOTE: the pools themselves do not need to be mounted in order for the lustre mgt database (mgs1pool/mgt1) to be mounted

# mount MGT (mgs1pool/mgt1) to start MGS server/service 

[estrella@lamella_t1 ~]$ sudo mount.lustre mgs1pool/mgt1 /lustre/mgt1/ 

   # `mount -t lustre .. ..` is fine too

# Verify Lustre Mount

sudo lctl dl ---> lists local lustre devices

[estrella@lamella_t1 ~]$ sudo lctl dl
  0 UP osd-zfs MGS-osd MGS-osd_UUID 4
  1 UP mgs MGS MGS 4
  2 UP mgc MGC10.100.180.2@tcp b90633ba-4a58-0f11-83bd-b334ce7a6186 4 

# Open TCP port 988 for MGS to communicate with other lustre servers on via LNET protocol

[estrella@lamella_t1 ~]$ sudo firewall-cmd --permanent --add-port=988/tcp
[sudo] password for estrella: 
success

[estrella@lamella_t1 ~]$ sudo firewall-cmd --reload
success

# Verify

[estrella@lamella_t1 ~]$ sudo firewall-cmd --list-ports
988/tcp

-------------------------------------------------------------------------------

# End of MGS setup; refer to `lustre_backend_2.md` for MDT set up


















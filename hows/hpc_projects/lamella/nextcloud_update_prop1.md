## Nextcloud Update Plan: BioHPC Cloud

### Schedule Downtime

### Backups Prior to NC Update

- **User Data**

`rsync -avn` --> dry run first 

`rsync -av` 

- **Database**

```
# confirm the name of the database:

mysql -u <MySQL Root User> -p -e "show databases" 

# push a .sql dump as backup 

mysqldump --single-transaction -u <root user> -p <nextcloud database> > /work/.backup/backup_services/cloud/update-$(date +%F).sql

  # --single-transaction: temporarily issues a READ LOCK, which will be released once the "binary log coordinates" are released; essentially, prevent any changes to InnoDB tables during the SQL dump---this causes the dump to be a snapshot of the databases at the instant the dump started, regardless of how long the dump takes.

  # --quick: dump tables row by row---if system's memory is low or has large databases, this prevents memory from filling up as a result of storing entire tables in memory

  # --lock-tables= false: ensure the entire backup session does not lock tables

    # [link 1](https://stackoverflow.com/questions/41683158/mysqldump-single-transaction-option) 

    # [link 2: good fundamentals](https://www.linode.com/docs/databases/mysql/use-mysqldump-to-back-up-mysql-or-mariadb/)

    # [link 3: MariaDB's docs on `mysqldump`](https://mariadb.com/kb/en/library/mysqldump/)

```

- **NextCloud Config Directory**

```
cd 
sudo tar -cpzvf /work/.backup/backup_services/cloud/nc_config_bak/nc-config.tar.gz <path to NC's config directory>/

  # `-c` & `-f`: create archive & name the archive file name
  # `-p`: preserve permissions, ownership, etc.
  # `-z`: compress with gZip standard
 
    # [why tar & gzip file types are used together](https://superuser.com/questions/252065/gzip-without-tar-why-are-they-used-together) 

    # [general tarc command example](https://www.howtogeek.com/248780/how-to-compress-and-extract-files-using-the-tar-command-on-linux/)

- **Turn on "maintenance mode" 

`sudo -u www-data php occ maintenance:mode --on`

  # this locks the sessions of all logged-in users, including administrators, and displays a status screen warning that the server is in maintenance mode 

    # [NC 15 Docs](https://docs.nextcloud.com/server/15/admin_manual/configuration_server/occ_command.html#maintenance-commands)

```


- **Stop Services**

```
# Apache Server
systemctl stop httpd

# MySQL database
systemctl stop mysqld

```


- **Run Security Update Patches**

```
yum --exclude=kernel*,redhat-release* check-update

  ## do we want to update to a newer kernel?

yum disable httpd 

yum disable mysqld

```

- **Update PHP to v.7+**

```
# check PHP version:

php -v 

# NC 17 requires PHP 7.2+ [link here](https://docs.nextcloud.com/server/17/admin_manual/installation/system_requirements.html)

# Delete existing php packages

yum list installed | grep -i php*

# install newer packages

[if one loses track of needed PHP modules](https://docs.nextcloud.com/server/17/admin_manual/installation/source_installation.html)  

# REBOOT Machine

```


- **Finish NC Updates**

```

occ update command

```


\
\
\


### Useful Links for Future:

```
[maintence commands-if updates go wrong](https://docs.nextcloud.com/server/15/admin_manual/configuration_server/occ_command.html#maintenance-commands)

[PHP update docs](https://www.rootusers.com/upgrade-php-5-6-7-1-centos-7-linux/) 

[MySQL's doc on mysqldump command](https://dev.mysql.com/doc/refman/5.7/en/mysqldump.html)

[Backup & Restore SQL Dump Guide](https://mysqlserverteam.com/creating-and-restoring-database-backups-with-mysqldump-and-mysql-enterprise-backup-part-1-of-2/)

[deep forum on --single-transaction](https://dba.stackexchange.com/questions/71961/mysqldump-single-transaction-yet-update-queries-are-waiting-for-the-backup#72526)

[mysqldump cmd examples](https://www.configserverfirewall.com/mysqladmin/mysqldump-mysql-database-backup-examples/)

[occ db add missing indices](https://blog.whabash.com/posts/backup-upgrade-nextcloud-13-to-14.0.1-notes) 

[generic NC update 1](https://www.techrepublic.com/article/how-to-upgrade-to-the-nextcloud-10-cloud-server-in-seven-easy-steps/)

[bashcript for NC update?](https://www.techandme.se/nextcloud-update-is-now-fully-automated/)

[generic NC update 2](https://www.turnkeylinux.org/forum/support/tue-20171003-1200/how-update-nextcloud-lastest-version)

[generic NC update 3](https://www.linuxbabe.com/cloud-storage/nextcloud-11-upgrade-from-10-updater-app)

[NC doc on auto updater](https://docs.nextcloud.com/server/16/admin_manual/maintenance/update.html)

[NC manual update guide](https://docs.nextcloud.com/server/16/admin_manual/maintenance/manual_upgrade.html)

[NC how to update general](https://docs.nextcloud.com/server/16/admin_manual/maintenance/upgrade.html)



```

### New Update/Pointers According to Discussions with LW

- let data be mounted on new vm from WS host? 

  - nfs server & client mount 

- make new backup VM: 

  - put SQL dump from old server to new server, add nextcloud configs

  - perform update

- Integrity Check
 
- 4-bit character fix on database


### Production/ESXI Update Plan

- **Select Config Files to Copy Over**

  - SElinux?

  - iptables settings? 

  - /etc/hosts

  - /etc/passwd

  - /etc/shadow

  - /etc/sysconfig/network

  - /etc/resolv.conf

  - /etc/nslcd.conf

  - /etc/openldap/ldap.conf

- **PHP**

  - retain same version as production, check requirements for 16, can we go straight to PHP v. 7.3? 
 
  - check repository

- MySQL

  - sqldump on production 

  - create new database --> create new root sql user --> inject database
 
- **OS Release** 

  - new VM Template exact 6.9 build or directly bring to 7.7?

- **Copy NC Over & See if it runs on new template 

  - httpd conf, change domain name 

  - copy data as root & retain original permissions

    - create new block devices in VM

    - not enough space??? retire some VMs!

  - assign different IP

    - doc IP usage

\
\
\

- **IF UPDATE WORKS**

  - wait until downtime, rsync the data between production and template

  - perform NC updates 

  - look into SSL cert?

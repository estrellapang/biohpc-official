## Creating Test `cloud.biohpc.swmed.edu` 

### References

- How to import MySQL database

  - [link 1](https://stackoverflow.com/questions/4546778/how-can-i-import-a-database-with-mysql-from-terminal) 

  - [link 2](https://www.digitalocean.com/community/tutorials/how-to-import-and-export-databases-in-mysql-or-mariadb) 

  - [link 3](https://www.quackit.com/mysql/tutorial/mysql_import_data.cfm) 

- Copying via SCP - or - SFTP 

  - [link 1](https://www.cerberusftp.com/comparing-scp-vs-sftp-which-is-better/) 

  - [link 2](https://www.maketecheasier.com/scp-vs-sftp/) 


### Perequisites

- Migrate VMs to 10T drive 

 - exact replica of LDAP, LUSTRE CLUSTER, LAMELLA001, 002 --> save on 10T on `/pandorabox`

   - **LONGTERM** --> store all VMs locally; backup on cluster

- Install Apache, set up ssl (mod_ssl)

  - get TLS certificate from a free CA

- check out what version of PHP to use 

  - cloud is running lamella 15.0.7.0 (per /var/www/owncloud/html/config/config.php) 

- Set up MySQL server, create root user

- Set up LDAP client --> CONNECT to cluster!


### LIVE: Storage Config 


- make a full partition on a 10T HDD:

```
[root@biohpcwsc037 ~]# lsblk


# newly installed disk---already has a full partition

sdb                            8:16   0   9.1T  0 disk 
└─sdb1                         8:17   0   9.1T  0 part 
  └─datastorage-storage1     253:4    0   9.1T  0 lvm


# need to repartition the disk

- make a new disk label: 


[root@biohpcwsc037 ~]# parted /dev/sdb mklabel gpt
Warning: The existing disk label on /dev/sdb will be destroyed and all data on this disk will be lost. Do you want to continue?
Yes/No? Yes                                                               
Information: You may need to update /etc/fstab.

  # NOTE: this is the second time this command was executed--after the first time, the system needed to be rebooted


[root@biohpcwsc037 ~]# lsblk

sdb                            8:16   0   9.1T  0 disk 

   # previous partition erased


# [root@biohpcwsc037 ~]# parted -a opt /dev/sdb mkpart primary xfs 0% 100%
Information: You may need to update /etc/fstab.

  # ISSUE: even though it's a new partition LVM picked up on previous records

sdb                            8:16   0   9.1T  0 disk 
└─sdb1                         8:17   0   9.1T  0 part 
  └─datastorage-storage1     253:4    0   9.1T  0 lvm  

# check out old info

[root@biohpcwsc037 ~]# pvdisplay
  --- Physical volume ---
  PV Name               /dev/sda3
  VG Name               vg_biohpcws001
  PV Size               <930.83 GiB / not usable 3.69 MiB
  Allocatable           yes (but full)
  PE Size               4.00 MiB
  Total PE              238291
  Free PE               0
  Allocated PE          238291
  PV UUID               UJ6N1f-IpDz-S4s2-2HHb-Srf0-8FgV-oZLA9q
   
  --- Physical volume ---
  PV Name               /dev/sdb1
  VG Name               datastorage
  PV Size               <9.10 TiB / not usable 2.00 MiB
  Allocatable           yes (but full)
  PE Size               4.00 MiB
  Total PE              2384383
  Free PE               0
  Allocated PE          2384383
  PV UUID               J0k0Xd-HGcO-Tn9U-ARA6-NJWB-mz5G-3As2U0



[root@biohpcwsc037 ~]# vgdisplay
  --- Volume group ---
  VG Name               vg_biohpcws001
  System ID             
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  17
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                4
  Open LV               4
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               930.82 GiB
  PE Size               4.00 MiB
  Total PE              238291
  Alloc PE / Size       238291 / 930.82 GiB
  Free  PE / Size       0 / 0   
  VG UUID               Zi1B5T-2Vu4-ltXj-PMap-DkWO-2dKu-5ij8Gh
   
  --- Volume group ---
  VG Name               datastorage
  System ID             
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  2
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                1
  Open LV               0
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <9.10 TiB
  PE Size               4.00 MiB
  Total PE              2384383
  Alloc PE / Size       2384383 / <9.10 TiB
  Free  PE / Size       0 / 0   
  VG UUID               RQY0cn-1jV7-rjR4-XvHB-YBjp-cbG1-uOfztB


[root@biohpcwsc037 ~]# lvdisplay

.
.
.
  --- Logical volume ---
  LV Path                /dev/datastorage/storage1
  LV Name                storage1
  VG Name                datastorage
  LV UUID                KXayl5-Ad1f-EsbI-l3yB-X586-A8nX-QPxxgD
  LV Write Access        read/write
  LV Creation host, time biohpcwsc120.biohpc.swmed.edu, 2019-02-18 22:55:53 -0600
  LV Status              available
  # open                 0
  LV Size                <9.10 TiB
  Current LE             2384383
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:4


# Remove old VG:

[root@biohpcwsc037 ~]# vgremove datastorage
Do you really want to remove volume group "datastorage" containing 1 logical volumes? [y/n]: y
Do you really want to remove active logical volume datastorage/storage1? [y/n]: y
  Logical volume "storage1" successfully removed
  Volume group "datastorage" successfully removed

  # verify

[root@biohpcwsc037 ~]# vgs
  VG             #PV #LV #SN Attr   VSize   VFree
  vg_biohpcws001   1   4   0 wz--n- 930.82g    0 
[root@biohpcwsc037 ~]# 
[root@biohpcwsc037 ~]# lvs
  LV        VG             Attr       LSize    Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  lv_home   vg_biohpcws001 -wi-ao----    1.00g                                                    
  lv_root   vg_biohpcws001 -wi-ao---- <248.83g                                                    
  lv_shared vg_biohpcws001 -wi-ao---- <665.00g                                                    
  lv_swap   vg_biohpcws001 -wi-ao----   16.00g 


# Remove old PV:

[root@biohpcwsc037 ~]# pvremove /dev/sdb1
  Labels on physical volume "/dev/sdb1" successfully wiped.
[root@biohpcwsc037 ~]# 
[root@biohpcwsc037 ~]# pvs
  PV         VG             Fmt  Attr PSize   PFree
  /dev/sda3  vg_biohpcws001 lvm2 a--  930.82g    0 

```


- Manage the new space via LVM


```
[root@biohpcwsc037 ~]# pvcreate /dev/sdb1
  Physical volume "/dev/sdb1" successfully created.
[root@biohpcwsc037 ~]# 
[root@biohpcwsc037 ~]# 
[root@biohpcwsc037 ~]# pvs
  PV         VG             Fmt  Attr PSize   PFree 
  /dev/sda3  vg_biohpcws001 lvm2 a--  930.82g     0 
  /dev/sdb1                 lvm2 ---   <9.10t <9.10t

[root@biohpcwsc037 ~]# vgcreate Tesseract /dev/sdb1
  Volume group "Tesseract" successfully created
[root@biohpcwsc037 ~]# 
[root@biohpcwsc037 ~]# 
[root@biohpcwsc037 ~]# 
[root@biohpcwsc037 ~]# vgs
  VG             #PV #LV #SN Attr   VSize   VFree 
  Tesseract        1   0   0 wz--n-  <9.10t <9.10t
  vg_biohpcws001   1   4   0 wz--n- 930.82g     0 
[root@biohpcwsc037 ~]# 


# create new VG & LV 

[root@biohpcwsc037 ~]# vgcreate Tesseract /dev/sdb1
  Volume group "Tesseract" successfully created


[root@biohpcwsc037 ~]# lvcreate -l 100%FREE -n Pandora Tesseract
WARNING: ext4 signature detected on /dev/Tesseract/Pandora at offset 1080. Wipe it? [y/n]: y
  Wiping ext4 signature on /dev/Tesseract/Pandora.
  Logical volume "Pandora" created.


[root@biohpcwsc037 ~]# lsblk
NAME                         MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda                            8:0    0 953.9G  0 disk 
├─sda1                         8:1    0   200M  0 part /boot/efi
├─sda2                         8:2    0   500M  0 part /boot
└─sda3                         8:3    0 930.8G  0 part 
  ├─vg_biohpcws001-lv_root   253:0    0 248.8G  0 lvm  /
  ├─vg_biohpcws001-lv_swap   253:1    0    16G  0 lvm  [SWAP]
  ├─vg_biohpcws001-lv_home   253:2    0     1G  0 lvm  /home
  └─vg_biohpcws001-lv_shared 253:3    0   665G  0 lvm  /shared
sdb                            8:16   0   9.1T  0 disk 
└─sdb1                         8:17   0   9.1T  0 part 
  └─Tesseract-Pandora        253:4    0   9.1T  0 lvm  
[root@biohpcwsc037 ~]# 


# finalize

[root@biohpcwsc037 ~]# mkfs -t xfs /dev/Tesseract/Pandora
meta-data=/dev/Tesseract/Pandora isize=512    agcount=10, agsize=268435455 blks
         =                       sectsz=4096  attr=2, projid32bit=1
         =                       crc=1        finobt=0, sparse=0
data     =                       bsize=4096   blocks=2441608192, imaxpct=5
         =                       sunit=0      swidth=0 blks
naming   =version 2              bsize=4096   ascii-ci=0 ftype=1
log      =internal log           bsize=4096   blocks=521728, version=2
         =                       sectsz=4096  sunit=1 blks, lazy-count=1
realtime =none                   extsz=4096   blocks=0, rtextents=0


[root@biohpcwsc037 ~]# mkdir /mnt/box

[root@biohpcwsc037 ~]# mount /dev/Tesseract/Pandora /mnt/box/

[root@biohpcwsc037 ~]# df -Th
...
/dev/mapper/Tesseract-Pandora                xfs       9.1T   33M  9.1T   1% /mnt/box

chown root:biohpc_admin /mnt/box 

[root@biohpcwsc037 ~]# chmod 770 /mnt/box/


# add new mount to `/etc/fstab`

[root@biohpcwsc037 ~]# cat /etc/fstab
...
/dev/mapper/Tesseract-Pandora	/mnt/box/	xfs	defaults	0 0


[zpang1@biohpcwsc037 box]$ mkdir test_environments
[zpang1@biohpcwsc037 box]$ ll
total 0
drwxr-xr-x. 2 zpang1 biohpc_admin 6 Sep 16 23:14 test_environments
[zpang1@biohpcwsc037 box]$ 

```

- **transfer my VMs** 

  - `scp`'ed `database_1` VM from work to local `/mnt/box/test_environments` --> renamed VM `test_cloud` 

  - due to both the original and the copy having identical UIDs (CORRECT method ought to have been: Clone (reinitialized all MAC addressed) --> Copy Clone to local directory --> Add Clone as Additional VM), the original VM was REMOVED so `test_cloud` could be added a new machine

  - added 2x 1.5T virtual drives in VM, adjusted its memory cap, and added the drives into the LVM system

```
[estrella@data1 ~]$ lsblk

sdb               8:16   0  1.5T  0 disk 
sdc               8:32   0  1.5T  0 disk 

[estrella@data1 ~]$ sudo parted /dev/sdb mklabel gpt

[estrella@data1 ~]$ sudo parted /dev/sdc mklabel gpt

[estrella@data1 ~]$ sudo parted -a opt /dev/sdc mkpart primary xfs 0% 100%
Information: You may need to update /etc/fstab.

[estrella@data1 ~]$ sudo parted -a opt /dev/sdb mkpart primary xfs 0% 100%
Information: You may need to update /etc/fstab.

[estrella@data1 ~]$ lsblk

sdb               8:16   0  1.5T  0 disk 
└─sdb1            8:17   0  1.5T  0 part 
sdc               8:32   0  1.5T  0 disk 
└─sdc1            8:33   0  1.5T  0 part 
sr0              11:0    1 1024M  0 rom  
[estrella@data1 ~]$ 


[estrella@data1 ~]$ sudo pvcreate /dev/sdb1
  Physical volume "/dev/sdb1" successfully created.

[estrella@data1 ~]$ sudo pvcreate /dev/sdc1
  Physical volume "/dev/sdc1" successfully created.

[estrella@data1 ~]$ sudo vgcreate condensation /dev/sdb1

[estrella@data1 ~]$ sudo vgextend condensation /dev/sdc1
  Volume group "condensation" successfully extended

[estrella@data1 ~]$ lvcreate -l 100%FREE -n sky_1 condensation

[estrella@data1 ~]$ sudo lvdisplay

  --- Logical volume ---
  LV Path                /dev/condensation/sky_1
  LV Name                sky_1
  VG Name                condensation
  LV UUID                059DIN-ghe5-AWot-XnB8-ZCJ4-c7jB-zn6Rom
  LV Write Access        read/write
  LV Creation host, time data1.biohpc.new, 2019-09-17 00:28:37 -0500
  LV Status              available
  # open                 0
  LV Size                <3.00 TiB
  Current LE             786430
  Segments               2
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:2
   

[estrella@data1 ~]$ mkfs -t xfs /dev/condensation/sky_1

[estrella@data1 ~]$ sudo mkdir /acloud

[estrella@data1 ~]$ sudo mount /dev/condensation/sky_1 /acloud/

[estrella@data1 ~]$ sudo vim /etc/fstab

/dev/mapper/condensation-sky_1	/acloud		xfs	defaults	0 0

[estrella@data1 ~]$ sudo mount /dev/condensation/sky_1 /acloud/

```


### 10-9-2019 Copying `cloud.biohpc.swmed.edu` to local directory `/mnt/box/`


- log-in to cloud --> allowed workstation I.P. to iptables INPUT chain

  - `/etc/sysconfig/iptables` 

  - `-A INPUT -p tcp -m state --state NEW -s 198.215.56.37 -m tcp --dport 22 -j ACCEPT` 

  - `service iptables restart`


- modify `/etc/ssh/sshd_config`

  - comment out `PermitRootLogin`

  - add `root` in field `AllowUsers`

  - `service sshd restart` 

- on WS, add cloud I.P. to `/etc/hosts.allow` 

- LIVE: Datacopy 


```

## quiet,recursive,fastest compression algorithm secure copy

# copy existing user data 

scp -qrc arcfour root@129.112.9.92:/var/www/owncloud/data/data /mnt/box/gets/

# copy nextcloud app 

scp -qrc arcfour root@129.112.9.92:/var/www/owncloud/html /mnt/box/gets/model_cloud

# copy recently dumped SQL database

scp -c arcfour biohpcadmin@129.112.9.92:/var/www/owncloud/DB_backup/db_20191009033002.sql model_cloud/

# copy old operation scripts--> not really needed 

scp -rc arcfour root@129.112.9.92:/var/www/owncloud/scripts/ model_cloud/

```


- **copy `cloud.biohpc.swmed.edu`** to local directory 

### Links

- **Storage Config** 

  - [Redhat Docs LVM Guide: HELPFUL!](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/4/html/Cluster_Logical_Volume_Manager/PV_remove.html) 

  - [parted remove partition](https://www.2daygeek.com/how-to-manage-disk-partitions-using-parted-command/) 



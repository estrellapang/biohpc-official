## NextCloud Setup & Installation: Standalone Database Server 

### New VM Dedicated MySQL Server

- **basic info** 

  - I.P.:10.100.180.8 

  - hostname: data1.biohpc.new 

- **essential configurations** 

  - `sudo yum install net-tools` 

  - `sudo yum install epel-release`

  - `sudo yum update kernel`

    - updated to kernel version `kernel.x86_64 0:3.10.0-957.21.3.el7`
  
  - `sudo yum install vim` 

  - permanently change SElinux to permissive: `sudo vim /etc/selinux/config` 
 
    - `SELINUX=permissive`

    - [tutorial link](https://www.rootusers.com/how-to-enable-or-disable-selinux-in-centos-rhel-7/)

  - `sudo yum install nc telnet`
  
  - check firewall-cmd's services & ports `sudo firewall-cmd --list-all`

  - `sudo yum update`

  - updated packages

```
Installed:
  kernel-devel.x86_64 0:3.10.0-957.21.3.el7                                                                                                 

Updated:
  NetworkManager.x86_64 1:1.12.0-10.el7_6                            NetworkManager-libnm.x86_64 1:1.12.0-10.el7_6                         
  NetworkManager-team.x86_64 1:1.12.0-10.el7_6                       NetworkManager-tui.x86_64 1:1.12.0-10.el7_6                           
  bind-libs-lite.x86_64 32:9.9.4-74.el7_6.1                          bind-license.noarch 32:9.9.4-74.el7_6.1                               
  cpp.x86_64 0:4.8.5-36.el7_6.2                                      cronie.x86_64 0:1.4.11-20.el7_6                                       
  cronie-anacron.x86_64 0:1.4.11-20.el7_6                            dbus.x86_64 1:1.10.24-13.el7_6                                        
  dbus-libs.x86_64 1:1.10.24-13.el7_6                                device-mapper.x86_64 7:1.02.149-10.el7_6.8                            
  device-mapper-event.x86_64 7:1.02.149-10.el7_6.8                   device-mapper-event-libs.x86_64 7:1.02.149-10.el7_6.8                 
  device-mapper-libs.x86_64 7:1.02.149-10.el7_6.8                    efivar-libs.x86_64 0:36-11.el7_6.1                                    
  freetype.x86_64 0:2.8-12.el7_6.1                                   gcc.x86_64 0:4.8.5-36.el7_6.2                                         
  gcc-c++.x86_64 0:4.8.5-36.el7_6.2                                  gcc-gfortran.x86_64 0:4.8.5-36.el7_6.2                                
  git.x86_64 0:1.8.3.1-20.el7                                        glib2.x86_64 0:2.56.1-4.el7_6                                         
  glibc.x86_64 0:2.17-260.el7_6.6                                    glibc-common.x86_64 0:2.17-260.el7_6.6                                
  glibc-devel.x86_64 0:2.17-260.el7_6.6                              glibc-headers.x86_64 0:2.17-260.el7_6.6                               
  gnutls.x86_64 0:3.3.29-9.el7_6                                     grub2.x86_64 1:2.02-0.76.el7.centos.1                                 
  grub2-common.noarch 1:2.02-0.76.el7.centos.1                       grub2-pc.x86_64 1:2.02-0.76.el7.centos.1                              
  grub2-pc-modules.noarch 1:2.02-0.76.el7.centos.1                   grub2-tools.x86_64 1:2.02-0.76.el7.centos.1                           
  grub2-tools-extra.x86_64 1:2.02-0.76.el7.centos.1                  grub2-tools-minimal.x86_64 1:2.02-0.76.el7.centos.1                   
  iproute.x86_64 0:4.11.0-14.el7_6.2                                 ipset.x86_64 0:6.38-3.el7_6                                           
  ipset-libs.x86_64 0:6.38-3.el7_6                                   kernel-headers.x86_64 0:3.10.0-957.21.3.el7                           
  kernel-tools.x86_64 0:3.10.0-957.21.3.el7                          kernel-tools-libs.x86_64 0:3.10.0-957.21.3.el7                        
  kexec-tools.x86_64 0:2.0.15-21.el7_6.3                             krb5-libs.x86_64 0:1.15.1-37.el7_6                                    
  libblkid.x86_64 0:2.23.2-59.el7_6.1                                libgcc.x86_64 0:4.8.5-36.el7_6.2                                      
  libgfortran.x86_64 0:4.8.5-36.el7_6.2                              libgomp.x86_64 0:4.8.5-36.el7_6.2                                     
  libmount.x86_64 0:2.23.2-59.el7_6.1                                libquadmath.x86_64 0:4.8.5-36.el7_6.2                                 
  libquadmath-devel.x86_64 0:4.8.5-36.el7_6.2                        libsmartcols.x86_64 0:2.23.2-59.el7_6.1                               
  libssh2.x86_64 0:1.4.3-12.el7_6.2                                  libstdc++.x86_64 0:4.8.5-36.el7_6.2                                   
  libstdc++-devel.x86_64 0:4.8.5-36.el7_6.2                          libteam.x86_64 0:1.27-6.el7_6.1                                       
  libuuid.x86_64 0:2.23.2-59.el7_6.1                                 lvm2.x86_64 7:2.02.180-10.el7_6.8                                     
  lvm2-libs.x86_64 7:2.02.180-10.el7_6.8                             microcode_ctl.x86_64 2:2.1-47.5.el7_6                                 
  mokutil.x86_64 0:15-2.el7.centos                                   nss.x86_64 0:3.36.0-7.1.el7_6                                         
  nss-pem.x86_64 0:1.0.3-5.el7_6.1                                   nss-sysinit.x86_64 0:3.36.0-7.1.el7_6                                 
  nss-tools.x86_64 0:3.36.0-7.1.el7_6                                nss-util.x86_64 0:3.36.0-1.1.el7_6                                    
  openldap.x86_64 0:2.4.44-21.el7_6                                  openssl.x86_64 1:1.0.2k-16.el7_6.1                                    
  openssl-libs.x86_64 1:1.0.2k-16.el7_6.1                            perl.x86_64 4:5.16.3-294.el7_6                                        
  perl-Git.noarch 0:1.8.3.1-20.el7                                   perl-Pod-Escapes.noarch 1:1.04-294.el7_6                              
  perl-libs.x86_64 4:5.16.3-294.el7_6                                perl-macros.x86_64 4:5.16.3-294.el7_6                                 
  policycoreutils.x86_64 0:2.5-29.el7_6.1                            polkit.x86_64 0:0.112-18.el7_6.1                                      
  python.x86_64 0:2.7.5-80.el7_6                                     python-libs.x86_64 0:2.7.5-80.el7_6                                   
  python-perf.x86_64 0:3.10.0-957.21.3.el7                           rsync.x86_64 0:3.1.2-6.el7_6.1                                        
  selinux-policy.noarch 0:3.13.1-229.el7_6.12                        selinux-policy-targeted.noarch 0:3.13.1-229.el7_6.12                  
  shadow-utils.x86_64 2:4.1.5.1-25.el7_6.1                           systemd.x86_64 0:219-62.el7_6.7                                       
  systemd-libs.x86_64 0:219-62.el7_6.7                               systemd-sysv.x86_64 0:219-62.el7_6.7                                  
  teamd.x86_64 0:1.27-6.el7_6.1                                      tuned.noarch 0:2.10.0-6.el7_6.3                                       
  tzdata.noarch 0:2019b-1.el7                                        util-linux.x86_64 0:2.23.2-59.el7_6.1                                 
  vim-minimal.x86_64 2:7.4.160-6.el7_6                               xfsprogs.x86_64 0:4.5.0-19.el7_6                                      

```

### Steps Outlined 

- [official NextCloud Install Guidelines](https://docs.nextcloud.com//server/16/admin_manual/installation/source_installation.html)

  - doc for NextCloud version 14 is pretty good too 

0. Install PHP version 7.3 

- check if EPEL repo is installed 

```
[estrella@data1 ~]$ yum repolist | grep -i 'epel'
 * epel: csc.mcs.sdsmt.edu
!epel/x86_64          Extra Packages for Enterprise Linux 7 - x86_64      13,328

```

sudo yum install https://mirror.webtatic.com/yum/el7/webtatic-release.rpm

Installed:
  webtatic-release.noarch 0:7-3   

yum repolist

repo id                                             repo name                                                                         status
base/7/x86_64                                       CentOS-7 - Base                                                                   10,019
epel/x86_64                                         Extra Packages for Enterprise Linux 7 - x86_64                                    13,336
extras/7/x86_64                                     CentOS-7 - Extras                                                                    419
updates/7/x86_64                                    CentOS-7 - Updates                                                                 2,500
webtatic/x86_64                                     Webtatic Repository EL7 - x86_64                                                     459
repolist: 26,733


- **install relevant PHP packages**

```
sudo yum install mod_php73w php73w-common php73w-opcache php73w-dom php73w-mbstring php73w-gd php73w-json php73w-xml php73w-zip php73w-curl php73w-pear php73w-intl php73w-mysql php73w-ldap php73w-imagick php73w-apcu php73w-ldap php73w-posix php73w-mcrypt php73w-smbclient bzip2 setroubleshoot-server php73w php73w-pdo

# PHP 7.3 packages not yet available on `webtactic` repo? 

No package mod_php73w available.
No package php73w-common available.
No package php73w-opcache available.
No package php73w-dom available.
No package php73w-mbstring available.
No package php73w-gd available.
No package php73w-json available.
No package php73w-xml available.
No package php73w-zip available.
No package php73w-curl available.
No package php73w-pear available.
No package php73w-intl available.
No package php73w-mysql available.
No package php73w-ldap available.
No package php73w-imagick available.
No package php73w-apcu available.
No package php73w-ldap available.
No package php73w-posix available.
No package php73w-mcrypt available.
No package php73w-smbclient available.
Package bzip2-1.0.6-13.el7.x86_64 already installed and latest version
No package php73w available.
No package php73w-pdo available.


# these packages are not available:

No package php72w-imagick available.
No package php72w-apcu available.
No package php72w-mcrypt available.   ### NO lONGER SUPPORTED in PHP 7.2 in for Webtatic [reference](https://webtatic.com/packages/php72/)
No package php72w-smbclient available.
Package bzip2-1.0.6-13.el7.x86_64 already installed and latest version
No package php732-pdo available.  ## Typo Here 


# fix  

php72w-pdo
php72w-pecl-apcu 
php72w-pecl-imagick
php72w-pecl 

sudo yum install mod_php72w php72w-common php72w-opcache php72w-dom php72w-mbstring php72w-gd php72w-json php72w-xml php72w-zip php72w-curl php72w-pear php72w-intl php72w-mysql php72w-ldap php72w-pecl-imagick php72w-pecl-apcu  php72w-posix php72w-pecl bzip2 setroubleshoot-server php72w php72w-pdo bzip2


# I'm confused about the php72w-pecl packages: 

No package php72w-pecl available.

Installed:
  mod_php72w.x86_64 0:7.2.19-1.w7            php72w-common.x86_64 0:7.2.19-1.w7          php72w-gd.x86_64 0:7.2.19-1.w7                    
  php72w-intl.x86_64 0:7.2.19-1.w7           php72w-ldap.x86_64 0:7.2.19-1.w7            php72w-mbstring.x86_64 0:7.2.19-1.w7              
  php72w-mysql.x86_64 0:7.2.19-1.w7          php72w-opcache.x86_64 0:7.2.19-1.w7         php72w-pdo.x86_64 0:7.2.19-1.w7                   
  php72w-pear.noarch 1:1.10.4-1.w7           php72w-pecl-apcu.x86_64 0:5.1.9-1.w7        php72w-pecl-imagick.x86_64 0:3.4.3-1.2.w7         
  php72w-process.x86_64 0:7.2.19-1.w7        php72w-xml.x86_64 0:7.2.19-1.w7             setroubleshoot-server.x86_64 0:3.2.30-3.el7       

Dependency Installed:
  ImageMagick.x86_64 0:6.7.8.9-16.el7_6                                OpenEXR-libs.x86_64 0:1.7.1-7.el7                                   
  audit-libs-python.x86_64 0:2.8.4-4.el7                               cairo.x86_64 0:1.15.12-3.el7                                        
  checkpolicy.x86_64 0:2.5-8.el7                                       cups-libs.x86_64 1:1.6.3-35.el7                                     
  dejavu-fonts-common.noarch 0:2.33-6.el7                              dejavu-sans-fonts.noarch 0:2.33-6.el7                               
  fontconfig.x86_64 0:2.13.0-4.3.el7                                   fontpackages-filesystem.noarch 0:1.44-8.el7                         
  fribidi.x86_64 0:1.0.2-1.el7                                         gdk-pixbuf2.x86_64 0:2.36.12-3.el7                                  
  ghostscript.x86_64 0:9.07-31.el7_6.11                                ghostscript-fonts.noarch 0:5.50-32.el7                              
  graphite2.x86_64 0:1.3.10-1.el7_3                                    harfbuzz.x86_64 0:1.7.5-2.el7                                       
  ilmbase.x86_64 0:1.0.3-7.el7                                         jasper-libs.x86_64 0:1.900.1-33.el7                                 
  jbigkit-libs.x86_64 0:2.0-11.el7                                     lcms2.x86_64 0:2.6-3.el7                                            
  libICE.x86_64 0:1.0.9-9.el7                                          libSM.x86_64 0:1.2.2-2.el7                                          
  libX11.x86_64 0:1.6.5-2.el7                                          libX11-common.noarch 0:1.6.5-2.el7                                  
  libXau.x86_64 0:1.0.8-2.1.el7                                        libXdamage.x86_64 0:1.1.4-4.1.el7                                   
  libXext.x86_64 0:1.3.3-3.el7                                         libXfixes.x86_64 0:5.0.3-1.el7                                      
  libXft.x86_64 0:2.3.2-2.el7                                          libXpm.x86_64 0:3.5.12-1.el7                                        
  libXrender.x86_64 0:0.9.10-1.el7                                     libXt.x86_64 0:1.1.5-3.el7                                          
  libXxf86vm.x86_64 0:1.1.4-1.el7                                      libargon2.x86_64 0:20161029-3.el7                                   
  libcgroup.x86_64 0:0.41-20.el7                                       libfontenc.x86_64 0:1.1.3-3.el7                                     
  libglvnd.x86_64 1:1.0.1-0.8.git5baa1e5.el7                           libglvnd-egl.x86_64 1:1.0.1-0.8.git5baa1e5.el7                      
  libglvnd-glx.x86_64 1:1.0.1-0.8.git5baa1e5.el7                       libicu.x86_64 0:50.1.2-17.el7                                       
  libjpeg-turbo.x86_64 0:1.2.90-6.el7                                  librsvg2.x86_64 0:2.40.20-1.el7                                     
  libsemanage-python.x86_64 0:2.5-14.el7                               libthai.x86_64 0:0.1.14-9.el7                                       
  libtiff.x86_64 0:4.0.3-27.el7_3                                      libtool-ltdl.x86_64 0:2.4.2-22.el7_3                                
  libwayland-client.x86_64 0:1.15.0-1.el7                              libwayland-server.x86_64 0:1.15.0-1.el7                             
  libwmf-lite.x86_64 0:0.2.8.4-41.el7_1                                libxcb.x86_64 0:1.13-1.el7                                          
  libxml2-python.x86_64 0:2.9.1-6.el7_2.3                              libxshmfence.x86_64 0:1.2-1.el7                                     
  libxslt.x86_64 0:1.1.28-5.el7                                        mesa-libEGL.x86_64 0:18.0.5-4.el7_6                                 
  mesa-libGL.x86_64 0:18.0.5-4.el7_6                                   mesa-libgbm.x86_64 0:18.0.5-4.el7_6                                 
  mesa-libglapi.x86_64 0:18.0.5-4.el7_6                                pango.x86_64 0:1.42.4-2.el7_6                                       
  php72w-cli.x86_64 0:7.2.19-1.w7                                      pixman.x86_64 0:0.34.0-1.el7                                        
  policycoreutils-python.x86_64 0:2.5-29.el7_6.1                       poppler-data.noarch 0:0.4.6-3.el7                                   
  pygobject2.x86_64 0:2.28.6-11.el7                                    python-IPy.noarch 0:0.75-6.el7                                      
  setools-libs.x86_64 0:3.3.8-4.el7                                    setroubleshoot-plugins.noarch 0:3.0.67-3.el7                        
  systemd-python.x86_64 0:219-62.el7_6.9                               urw-fonts.noarch 0:2.4-16.el7                                       
  xorg-x11-font-utils.x86_64 1:7.5-21.el7                             

Dependency Updated:
  systemd.x86_64 0:219-62.el7_6.9            systemd-libs.x86_64 0:219-62.el7_6.9            systemd-sysv.x86_64 0:219-62.el7_6.9           

Complete!

### check PHP version

php --version
PHP 7.2.19 (cli) (built: Jun  2 2019 09:49:05) ( NTS )
Copyright (c) 1997-2018 The PHP Group
Zend Engine v3.2.0, Copyright (c) 1998-2018 Zend Technologies
    with Zend OPcache v7.2.19, Copyright (c) 1999-2018, by Zend Technologies


####### THE PHP CMDs ABOVE ARE  NOT NECESSARY FOR THE Stand-Alone MySQL Server #########

```

1. Set up **Standalone MYSQL Server** 

  - [link for fresh mysql installation](https://dev.mysql.com/doc/mysql-yum-repo-quick-guide/en/) 

  - [link for MySQL repo selection](https://dev.mysql.com/doc/refman/8.0/en/linux-installation-yum-repo.html#yum-repo-select-seriesm/doc/refman/8.0/en/linux-installation-yum-repo.html#yum-repo-select-series) 

  - [mysql_secure_installation_program](https://dev.mysql.com/doc/refman/8.0/en/mysql-secure-installation.html) 

  - [another mysql tutorial](https://linode.com/docs/databases/mysql/how-to-install-mysql-on-centos-7/) 

```

### LIVE ### 

which wget 

cat /etc/redhat-release   # get your distro release version

sudo yum install wget

# download & add repo:

cd /temp 

wget https://dev.mysql.com/get/mysql80-community-release-el7-3.noarch.rpm

# OR #

wget https://repo.mysql.com/mysql80-community-release-el7-3.noarch.rpm

sudo yum localinstall mysql80-community-release-el7-3.noarch.rpm

# verify: 

cat /etc/yum.repos.d/mysql-community.repo
# Enable to use MySQL 5.5
[mysql55-community]
name=MySQL 5.5 Community Server
baseurl=http://repo.mysql.com/yum/mysql-5.5-community/el/7/$basearch/
enabled=0
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-mysql

# Enable to use MySQL 5.6
[mysql56-community]
name=MySQL 5.6 Community Server
baseurl=http://repo.mysql.com/yum/mysql-5.6-community/el/7/$basearch/
enabled=0
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-mysql

# Enable to use MySQL 5.7
[mysql57-community]
name=MySQL 5.7 Community Server
baseurl=http://repo.mysql.com/yum/mysql-5.7-community/el/7/$basearch/
enabled=0
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-mysql

[mysql80-community]
name=MySQL 8.0 Community Server
baseurl=http://repo.mysql.com/yum/mysql-8.0-community/el/7/$basearch/
enabled=1
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-mysql

[mysql-connectors-community]
name=MySQL Connectors Community
baseurl=http://repo.mysql.com/yum/mysql-connectors-community/el/7/$basearch/
enabled=1
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-mysql

[mysql-tools-community]
name=MySQL Tools Community
baseurl=http://repo.mysql.com/yum/mysql-tools-community/el/7/$basearch/
enabled=1
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-mysql

[mysql-tools-preview]
name=MySQL Tools Preview
baseurl=http://repo.mysql.com/yum/mysql-tools-preview/el/7/$basearch/
enabled=0
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-mysql

[mysql-cluster-7.5-community]
name=MySQL Cluster 7.5 Community
baseurl=http://repo.mysql.com/yum/mysql-cluster-7.5-community/el/7/$basearch/
enabled=0
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-mysql

[mysql-cluster-7.6-community]
name=MySQL Cluster 7.6 Community
baseurl=http://repo.mysql.com/yum/mysql-cluster-7.6-community/el/7/$basearch/
enabled=0
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-mysql

[mysql-cluster-8.0-community]
name=MySQL Cluster 8.0 Community
baseurl=http://repo.mysql.com/yum/mysql-cluster-8.0-community/el/7/$basearch/
enabled=0
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-mysql

   # mysql80-community is enabled

# install MySQL server

sudo yum install mysql-community-server

Running transaction
  Installing : mysql-community-common-8.0.17-1.el7.x86_64                                                                               1/6 
  Installing : mysql-community-libs-8.0.17-1.el7.x86_64                                                                                 2/6 
  Installing : mysql-community-client-8.0.17-1.el7.x86_64                                                                               3/6 
  Installing : mysql-community-server-8.0.17-1.el7.x86_64                                                                               4/6 
  Installing : mysql-community-libs-compat-8.0.17-1.el7.x86_64                                                                          5/6 
  Erasing    : 1:mariadb-libs-5.5.60-1.el7_5.x86_64                                                                                     6/6 
  Verifying  : mysql-community-libs-8.0.17-1.el7.x86_64                                                                                 1/6 
  Verifying  : mysql-community-server-8.0.17-1.el7.x86_64                                                                               2/6 
  Verifying  : mysql-community-common-8.0.17-1.el7.x86_64                                                                               3/6 
  Verifying  : mysql-community-client-8.0.17-1.el7.x86_64                                                                               4/6 
  Verifying  : mysql-community-libs-compat-8.0.17-1.el7.x86_64                                                                          5/6 
  Verifying  : 1:mariadb-libs-5.5.60-1.el7_5.x86_64                                                                                     6/6 

Installed:
  mysql-community-libs.x86_64 0:8.0.17-1.el7 mysql-community-libs-compat.x86_64 0:8.0.17-1.el7 mysql-community-server.x86_64 0:8.0.17-1.el7

Dependency Installed:
  mysql-community-client.x86_64 0:8.0.17-1.el7                         mysql-community-common.x86_64 0:8.0.17-1.el7                        

Replaced:
  mariadb-libs.x86_64 1:5.5.60-1.el7_5       


### IMPORTANT CONFIGS ### 

- let MySQL bind to the NIC rather than the localhost via `/etc/my.cnf` 

  - refer to links referenced above 


### Harden MySQL Server via `mysql_secure_installation` 

- sudo vim /var/log/mysqld.log --> grab temporary password 

# Use your temporary password 

mysql_secure_installation 

Securing the MySQL server deployment.

Enter password for user root: 
Error: Access denied for user 'root'@'localhost' (using password: YES)
[estrella@data1 tmp]$ 
[estrella@data1 tmp]$ 
[estrella@data1 tmp]$ mysql_secure_installation

Securing the MySQL server deployment.

Enter password for user root: 

The existing password for the user account root has expired. Please set a new password.

New password: 

Re-enter new password: 
 ... Failed! Error: Your password does not satisfy the current policy requirements

New password: 

Re-enter new password: 
The 'validate_password' component is installed on the server.
The subsequent steps will run with the existing configuration
of the component.
Using existing password for root.

Estimated strength of the password: 100 
Change the password for root ? ((Press y|Y for Yes, any other key for No) : y

New password: 

Re-enter new password: 

Estimated strength of the password: 100 
Do you wish to continue with the password provided?(Press y|Y for Yes, any other key for No) : Y    
By default, a MySQL installation has an anonymous user,
allowing anyone to log into MySQL without having to have
a user account created for them. This is intended only for
testing, and to make the installation go a bit smoother.
You should remove them before moving into a production
environment.

Remove anonymous users? (Press y|Y for Yes, any other key for No) : Y
Success.


Normally, root should only be allowed to connect from
'localhost'. This ensures that someone cannot guess at
the root password from the network.

Disallow root login remotely? (Press y|Y for Yes, any other key for No) : Y
Success.

By default, MySQL comes with a database named 'test' that
anyone can access. This is also intended only for testing,
and should be removed before moving into a production
environment.


Remove test database and access to it? (Press y|Y for Yes, any other key for No) : Y
 - Dropping test database...
Success.

 - Removing privileges on test database...
Success.

Reloading the privilege tables will ensure that all changes
made so far will take effect immediately.

Reload privilege tables now? (Press y|Y for Yes, any other key for No) : Y
Success.

All done! 


- [link 1](https://planet.mysql.com/entry/?id=5991189) 

- [link 2](https://tecadmin.net/install-mysql-8-on-centos/) 

- [link 3](https://docs.oracle.com/cd/E17952_01/mysql-8.0-en/linux-installation-yum-repo.html) 

- [link 4](https://www.itzgeek.com/how-tos/linux/centos-how-tos/how-to-install-mysql-5-78-0-on-centos-76-rhel-76-fedora-272625.html)


### Configure MySQL server to listen on dedicated network? 

- open port 3306 on firewall-cmd    # test if one can connect first 

  - alternative let mysqld bind to  0.0.0.0 and use firewalld to control incoming traffic 

### Create Database & Users 


[estrella@data1 ~]$ mysql -u root -p

----------------------------------------------------------------------------------------
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 10
Server version: 8.0.17 MySQL Community Server - GPL

Copyright (c) 2000, 2019, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> CREATE DATABASE lamella_bank;
Query OK, 1 row affected (0.02 sec) 

mysql> CREATE USER 'lamella_squad'@'localhost' IDENTIFIED BY '13579ZxP';
ERROR 1819 (HY000): Your password does not satisfy the current policy requirements 

mysql> CREATE USER 'lamella_squad'@'localhost' IDENTIFIED BY '~13579ZxP~';
Query OK, 0 rows affected (0.00 sec)

mysql> CREATE USER 'lamella_squad'@'10.100.180.%' IDENTIFIED BY '~13579ZxP~';
Query OK, 0 rows affected (0.02 sec) 

mysql> GRANT ALL PRIVILEGES ON lamella_bank.* TO 'lamella_squad'@'10.100.180.%';
Query OK, 0 rows affected (0.01 sec)

mysql> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.00 sec)

mysql> ^DBye   # CTRL+D to quit
----------------------------------------------------------------------------------------


# Verify: 

[estrella@data1 ~]$ mysql -u root -p 

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| lamella_bank       |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
5 rows in set (0.00 sec) 

mysql>  SELECT User, Host FROM mysql.user;
+------------------+--------------+
| User             | Host         |
+------------------+--------------+
| lamella_squad    | 10.100.180.% |
| lamella_squad    | localhost    |
| mysql.infoschema | localhost    |
| mysql.session    | localhost    |
| mysql.sys        | localhost    |
| root             | localhost    |
+------------------+--------------+
6 rows in set (0.00 sec) 

[link](https://www.cyberciti.biz/faq/mysql-command-to-show-list-of-databases-on-server/)

[link](https://alvinalexander.com/blog/post/mysql/show-users-i-ve-created-in-mysql-database)

```


- **install mysql-client on lamella servers** 

```
### NOTE on Lamella Servers! eth1 (10.100.180.###) conflicts with eth2 (192.168.56.###)

  ## BOTH `yum` & `sshd` get glitchy when eth2 is enabled! 

  ## Example of `yum` Errors: 

----------------------------------------------------------------------------------------
[estrella@lamella_t1 ~]$  sudo yum install https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
Loaded plugins: fastestmirror
Cannot open: https://mirror.webtatic.com/yum/el7/webtatic-release.rpm. Skipping.
Error: Nothing to do


  ## Tried enabling http & https via firewalld but to no avail...


[estrella@lamella_t1 ~]$ sudo firewall-cmd --permanent --add-service=http
success
[estrella@lamella_t1 ~]$ sudo firewall-cmd --permanent --add-service=https
success
[estrella@lamella_t1 ~]$ 
[estrella@lamella_t1 ~]$ sudo firewall-cmd --reload
success
[estrella@lamella_t1 ~]$ 
[estrella@lamella_t1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: eth2 enp0s3
  sources: 
  services: ssh dhcpv6-client samba ldap http https
  ports: 4379/tcp
  protocols: 
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 


### TEMP FIX ### If Installing or working remotely while CTDB is down ---> `sudo ifdown eth2` 

  # if CTDB needs to be up, then disable NAT to solve the SSH lag (but won't resolve the `yum` issue)
----------------------------------------------------------------------------------------


# enable webtatic repo 

[estrella@lamella_t1 ~]$ sudo yum install https://mirror.webtatic.com/yum/el7/webtatic-release.rpm

============================================================================================================================================
 Package                               Arch                        Version                     Repository                              Size
============================================================================================================================================
Installing:
 webtatic-release                      noarch                      7-3                         /webtatic-release                       22 k

Transaction Summary
============================================================================================================================================
Install  1 Package

Total size: 22 k
Installed size: 22 k

# verify:

[estrella@lamella_t1 ~]$ yum repolist

repo id                                             repo name                                                                         status
base/7/x86_64                                       CentOS-7 - Base                                                                   10,019
e2fsprogs                                           CentOS-7 - Ldiskfs                                                                    10
epel/x86_64                                         Extra Packages for Enterprise Linux 7 - x86_64                                    13,348
extras/7/x86_64                                     CentOS-7 - Extras                                                                    435
lustre-client                                       CentOS-7 - Lustre                                                                      9
lustre-server                                       CentOS-7 - Lustre                                                                     40
updates/7/x86_64                                    CentOS-7 - Updates                                                                 2,500
webtatic/x86_64                                     Webtatic Repository EL7 - x86_64                                                     523
zfs/x86_64                                          ZFS on Linux for EL7 - dkms                                                           26
repolist: 26,910

  # also look into `/etc/yum.repos.d/webtatic.repo` 



# install PHP

[estrella@lamella_t1 ~]$ sudo yum install mod_php72w php72w-common php72w-opcache php72w-dom php72w-mbstring php72w-gd php72w-json php72w-xml php72w-zip php72w-curl php72w-pear php72w-intl php72w-mysql php72w-ldap php72w-pecl-imagick php72w-pecl-apcu  php72w-posix php72w-pecl bzip2 setroubleshoot-server php72w php72w-pdo bzip2

----------------------------------------------------------------------------------------
Installed:
  mod_php72w.x86_64 0:7.2.21-1.w7            php72w-common.x86_64 0:7.2.21-1.w7          php72w-gd.x86_64 0:7.2.21-1.w7                    
  php72w-intl.x86_64 0:7.2.21-1.w7           php72w-ldap.x86_64 0:7.2.21-1.w7            php72w-mbstring.x86_64 0:7.2.21-1.w7              
  php72w-mysql.x86_64 0:7.2.21-1.w7          php72w-opcache.x86_64 0:7.2.21-1.w7         php72w-pdo.x86_64 0:7.2.21-1.w7                   
  php72w-pear.noarch 1:1.10.4-1.w7           php72w-pecl-apcu.x86_64 0:5.1.9-1.w7        php72w-pecl-imagick.x86_64 0:3.4.3-1.2.w7         
  php72w-process.x86_64 0:7.2.21-1.w7        php72w-xml.x86_64 0:7.2.21-1.w7             setroubleshoot-server.x86_64 0:3.2.30-3.el7       

Dependency Installed:
  ImageMagick.x86_64 0:6.7.8.9-16.el7_6    OpenEXR-libs.x86_64 0:1.7.1-7.el7                audit-libs-python.x86_64 0:2.8.4-4.el7        
  checkpolicy.x86_64 0:2.5-8.el7           ghostscript.x86_64 0:9.07-31.el7_6.11            ghostscript-fonts.noarch 0:5.50-32.el7        
  ilmbase.x86_64 0:1.0.3-7.el7             lcms2.x86_64 0:2.6-3.el7                         libXpm.x86_64 0:3.5.12-1.el7                  
  libXt.x86_64 0:1.1.5-3.el7               libargon2.x86_64 0:20161029-3.el7                libcgroup.x86_64 0:0.41-20.el7                
  libfontenc.x86_64 0:1.1.3-3.el7          libicu.x86_64 0:50.1.2-17.el7                    librsvg2.x86_64 0:2.40.20-1.el7               
  libsemanage-python.x86_64 0:2.5-14.el7   libtool-ltdl.x86_64 0:2.4.2-22.el7_3             libwmf-lite.x86_64 0:0.2.8.4-41.el7_1         
  php72w-cli.x86_64 0:7.2.21-1.w7          policycoreutils-python.x86_64 0:2.5-29.el7_6.1   poppler-data.noarch 0:0.4.6-3.el7             
  python-IPy.noarch 0:0.75-6.el7           setools-libs.x86_64 0:3.3.8-4.el7                setroubleshoot-plugins.noarch 0:3.0.67-3.el7  
  urw-fonts.noarch 0:2.4-16.el7            xorg-x11-font-utils.x86_64 1:7.5-21.el7      
----------------------------------------------------------------------------------------

# verify:

[estrella@lamella_t1 ~]$ php --version
PHP 7.2.21 (cli) (built: Aug  4 2019 08:42:27) ( NTS )    ### PHP 7.2 updated since my inital try
Copyright (c) 1997-2018 The PHP Group
Zend Engine v3.2.0, Copyright (c) 1998-2018 Zend Technologies
    with Zend OPcache v7.2.21, Copyright (c) 1999-2018, by Zend Technologies
[estrella@lamella_t1 ~]$ 
   

# enabled MySQL180 community repo and install client: 

[estrella@lamella_t1 ~]$ cd /tmp/ 

[estrella@lamella_t1 tmp]$ wget https://dev.mysql.com/get/mysql80-community-release-el7-3.noarch.rpm

[estrella@lamella_t1 tmp]$ ls
mysql80-community-release-el7-3.noarch.rpm

[estrella@lamella_t1 tmp]$ sudo yum localinstall mysql80-community-release-el7-3.noarch.rpm

============================================================================================================================================
 Package                                Arch                Version              Repository                                            Size
============================================================================================================================================
Installing:
 mysql80-community-release              noarch              el7-3                /mysql80-community-release-el7-3.noarch               31 k

Transaction Summary
============================================================================================================================================
Install  1 Package


# verify:

[estrella@lamella_t1 tmp]$ yum repolist

repo id                                                     repo name                                                                 status
base/7/x86_64                                               CentOS-7 - Base                                                           10,019
e2fsprogs                                                   CentOS-7 - Ldiskfs                                                            10
epel/x86_64                                                 Extra Packages for Enterprise Linux 7 - x86_64                            13,348
extras/7/x86_64                                             CentOS-7 - Extras                                                            435
lustre-client                                               CentOS-7 - Lustre                                                              9
lustre-server                                               CentOS-7 - Lustre                                                             40
mysql-connectors-community/x86_64                           MySQL Connectors Community                                                   118
mysql-tools-community/x86_64                                MySQL Tools Community                                                         95
mysql80-community/x86_64                                    MySQL 8.0 Community Server                                                   129
updates/7/x86_64                                            CentOS-7 - Updates                                                         2,500
webtatic/x86_64                                             Webtatic Repository EL7 - x86_64                                             523
zfs/x86_64                                                  ZFS on Linux for EL7 - dkms                                                   26
repolist: 27,252

# search & install mysql-client

[estrella@lamella_t1 tmp]$ yum search mysql-community-client
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: centos.mirror.lstn.net
 * epel: csc.mcs.sdsmt.edu
 * extras: centos.mirror.lstn.net
 * updates: centos.mirror.lstn.net
 * webtatic: uk.repo.webtatic.com
=================================================== N/S matched: mysql-community-client ====================================================
mysql-community-client.i686 : MySQL database client applications and tools
mysql-community-client.x86_64 : MySQL database client applications and tools


[estrella@lamella_t1 tmp]$ sudo yum install mysql-community-client.x86_64

============================================================================================================================================
 Package                                     Arch                   Version                         Repository                         Size
============================================================================================================================================
Installing:
 mysql-community-client                      x86_64                 8.0.17-1.el7                    mysql80-community                  32 M
 mysql-community-libs                        x86_64                 8.0.17-1.el7                    mysql80-community                 3.0 M
     replacing  mariadb-libs.x86_64 1:5.5.60-1.el7_5
 mysql-community-libs-compat                 x86_64                 8.0.17-1.el7                    mysql80-community                 2.1 M
     replacing  mariadb-libs.x86_64 1:5.5.60-1.el7_5
Installing for dependencies:
 mysql-community-common                      x86_64                 8.0.17-1.el7                    mysql80-community                 589 k

Transaction Summary
============================================================================================================================================
Install  3 Packages (+1 Dependent package)

Installed:
  mysql-community-client.x86_64 0:8.0.17-1.el7 mysql-community-libs.x86_64 0:8.0.17-1.el7 mysql-community-libs-compat.x86_64 0:8.0.17-1.el7

Dependency Installed:
  mysql-community-common.x86_64 0:8.0.17-1.el7                                                                                              

Replaced:
  mariadb-libs.x86_64 1:5.5.60-1.el7_5 


# VERIFY:

[estrella@lamella_t1 tmp]$ mysql -h 10.100.180.8 -u lamella_squad -p
Enter password: 
ERROR 2003 (HY000): Can't connect to MySQL server on '10.100.180.8' (113) 

 # firewall rules not allowing mysql connections from BOTH sides??? 

 # CORRECT # --> `sudo firewall-cmd --permanent --add-service=mysql` --> `sudo firewall-cmd --reload`

   # on BOTH client & server side


 # verify again: 

----------------------------------------------------------------------------------------
 [estrella@lamella_t1 tmp]$ mysql -h 10.100.180.8 -u lamella_squad -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 14
Server version: 8.0.17 MySQL Community Server - GPL

Copyright (c) 2000, 2019, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>   # good
mysql>
mysql>
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| lamella_bank       |
+--------------------+
2 rows in set (0.00 sec)

----------------------------------------------------------------------------------------

# install vsftpd, httpd, mod_ssl 

[estrella@lamella_t1 tmp]$ sudo yum install mod_ssl httpd vsftpd

Installed:
  httpd.x86_64 0:2.4.6-89.el7.centos.1            mod_ssl.x86_64 1:2.4.6-89.el7.centos.1            vsftpd.x86_64 0:3.0.2-25.el7           

Dependency Installed:
  httpd-tools.x86_64 0:2.4.6-89.el7.centos.1

[estrella@lamella_t1 tmp]$ yum info httpd.x86_64 0:2.4.6-89.el7.centos.1
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: centos.mirror.lstn.net
 * epel: csc.mcs.sdsmt.edu
 * extras: centos.mirror.lstn.net
 * updates: centos.mirror.lstn.net
 * webtatic: uk.repo.webtatic.com
Installed Packages
Name        : httpd
Arch        : x86_64
Version     : 2.4.6
Release     : 89.el7.centos.1
Size        : 9.4 M
Repo        : installed
From repo   : updates
Summary     : Apache HTTP Server
URL         : http://httpd.apache.org/
License     : ASL 2.0
Description : The Apache HTTP Server is a powerful, efficient, and extensible
            : web server.


[estrella@lamella_t1 tmp]$ yum info mod_ssl.x86_64 1:2.4.6-89.el7.centos.1
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: centos.mirror.lstn.net
 * epel: d2lzkl7pfhq30w.cloudfront.net
 * extras: centos.mirror.lstn.net
 * updates: centos.mirror.lstn.net
 * webtatic: uk.repo.webtatic.com
Installed Packages
Name        : mod_ssl
Arch        : x86_64
Epoch       : 1
Version     : 2.4.6
Release     : 89.el7.centos.1
Size        : 224 k
Repo        : installed
From repo   : updates
Summary     : SSL/TLS module for the Apache HTTP Server
URL         : http://httpd.apache.org/
License     : ASL 2.0
Description : The mod_ssl module provides strong cryptography for the Apache Web
            : server via the Secure Sockets Layer (SSL) and Transport Layer
            : Security (TLS) protocols.


[estrella@lamella_t1 tmp]$ yum info vsftpd.x86_64 0:3.0.2-25.el7
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: centos.mirror.lstn.net
 * epel: csc.mcs.sdsmt.edu
 * extras: centos.mirror.lstn.net
 * updates: centos.mirror.lstn.net
 * webtatic: uk.repo.webtatic.com
Installed Packages
Name        : vsftpd
Arch        : x86_64
Version     : 3.0.2
Release     : 25.el7
Size        : 353 k
Repo        : installed
From repo   : base
Summary     : Very Secure Ftp Daemon
URL         : https://security.appspot.com/vsftpd.html
License     : GPLv2 with exceptions
Description : vsftpd is a Very Secure FTP daemon. It was written completely from
            : scratch.

# Download and install NextCloud


[estrella@lamella_t1 html]$ cd /var/www/html/

[root@lamella_t1 html]$ su   # you MUST become root to do wget!

[root@lamella_t1 html]# wget https://download.nextcloud.com/server/releases/nextcloud-16.0.3.tar.bz2

[root@lamella_t1 html]# wget https://download.nextcloud.com/server/releases/nextcloud-16.0.3.tar.bz2
--2019-08-19 16:34:18--  https://download.nextcloud.com/server/releases/nextcloud-16.0.3.tar.bz2
Resolving proxy.swmed.edu (proxy.swmed.edu)... 129.112.115.42, 129.112.115.40, 129.112.115.53, ...
Connecting to proxy.swmed.edu (proxy.swmed.edu)|129.112.115.42|:3128... connected.
Proxy request sent, awaiting response... 200 OK
Length: 61612508 (59M) [application/x-bzip2]
Saving to: ‘nextcloud-16.0.3.tar.bz2’

100%[==================================================================================================>] 61,612,508  1003KB/s   in 61s    

2019-08-19 16:35:19 (985 KB/s) - ‘nextcloud-16.0.3.tar.bz2’ saved [61612508/61612508]

[root@lamella_t1 html]# 
[root@lamella_t1 html]# 
[root@lamella_t1 html]# 
[root@lamella_t1 html]# 
[root@lamella_t1 html]# ll
total 60172
-rw-r--r--. 1 root root 61612508 Jul  9 07:05 nextcloud-16.0.3.tar.bz2


# Extract files from `.bz2` file: 

[root@lamella_t1 html]# tar -xjvf nextcloud-16.0.3.tar.bz2   # the FLAG MUST END WITH `-f` !!! Order of preceding options are arbitrary

[root@lamella_t1 html]# ll
total 60176
drwxr-xr-x. 14 nobody nfsnobody     4096 Jul  9 07:03 nextcloud
-rw-r--r--.  1 root   root      61612508 Jul  9 07:05 nextcloud-16.0.3.tar.bz2


[root@lamella_t1 html]# du -sh nextcloud
227M	nextcloud                                    # the .bz2 file was ~45MB 

 
# make `data` folder

[root@lamella_t1 html]# mkdir nextcloud/data 

[root@lamella_t1 html]# ls nextcloud | grep -i data
data

# change OWNER of `nextcloud` to http server's unix user

[root@lamella_t1 html]# id apache
uid=48(apache) gid=48(apache) groups=48(apache)

[root@lamella_t1 html]# chown -R apache:apache nextcloud

[root@lamella_t1 html]# ls -l nextcloud
total 108
drwxr-xr-x. 32 apache apache  4096 Jul  9 07:03 3rdparty
drwxr-xr-x. 42 apache apache  4096 Jul  9 07:00 apps
-rw-r--r--.  1 apache apache 12063 Jul  9 06:59 AUTHORS
drwxr-xr-x.  2 apache apache    67 Jul  9 06:59 config
-rw-r--r--.  1 apache apache  3805 Jul  9 06:59 console.php
-rw-r--r--.  1 apache apache 34520 Jul  9 06:59 COPYING
drwxr-xr-x. 23 apache apache  4096 Jul  9 07:03 core
-rw-r--r--.  1 apache apache  4986 Jul  9 06:59 cron.php
drwxr-xr-x.  2 apache apache     6 Aug 20 10:13 data
-rw-r--r--.  1 apache apache   156 Jul  9 06:59 index.html
-rw-r--r--.  1 apache apache  3172 Jul  9 06:59 index.php
drwxr-xr-x.  6 apache apache   125 Jul  9 06:59 lib
-rw-r--r--.  1 apache apache   283 Jul  9 06:59 occ
drwxr-xr-x.  2 apache apache    23 Jul  9 06:59 ocm-provider
drwxr-xr-x.  2 apache apache    55 Jul  9 06:59 ocs
drwxr-xr-x.  2 apache apache    23 Jul  9 06:59 ocs-provider
-rw-r--r--.  1 apache apache  2951 Jul  9 06:59 public.php
-rw-r--r--.  1 apache apache  5139 Jul  9 06:59 remote.php
drwxr-xr-x.  4 apache apache   107 Jul  9 06:59 resources
-rw-r--r--.  1 apache apache    26 Jul  9 06:59 robots.txt
drwxr-xr-x. 12 apache apache   248 Jul  9 07:03 settings
-rw-r--r--.  1 apache apache  2232 Jul  9 06:59 status.php
drwxr-xr-x.  3 apache apache    35 Jul  9 06:59 themes
drwxr-xr-x.  2 apache apache    43 Jul  9 07:00 updater
-rw-r--r--.  1 apache apache   362 Jul  9 07:03 version.php


# create httpd config file


[estrella@lamella_t1 ~]$ sudo vim /etc/httpd/conf.d/nextcloud.conf
-------------------------------------------------------------------------------

Alias /nextcloud "/var/www/html/nextcloud/"
   
<Directory /var/www/html/nextcloud/>
   Options +FollowSymlinks
   AllowOverride All
     

 <IfModule mod_dav.c>
   Dav off
 </IfModule>


 SetEnv HOME /var/www/html/nextcloud
 SetEnv HTTP_HOME /var/www/html/nextcloud

</Directory>

-------------------------------------------------------------------------------

# check/change firewall 


  # be sure to add the following services to BOTH the NextCloud servers & MySQL databases:

	mysql, http, https


# allow nextcloud with SElinux

-------------------------------------------------------------------------------

[root@lamella_t1 nextcloud]# semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/nextcloud/config(/.*)?'
[root@lamella_t1 nextcloud]# semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/nextcloud/apps(/.*)?'
[root@lamella_t1 nextcloud]# semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/nextcloud/data(/.*)?'
[root@lamella_t1 nextcloud]# semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/nextcloud/.htaccess'
[root@lamella_t1 nextcloud]# semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/nextcloud/.user.ini'
[root@lamella_t1 nextcloud]# semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/nextcloud/3rdparty/aws/aws-sdk-php/src/data/logs(/.*)?'

[root@lamella_t1 nextcloud]# restorecon -R '/var/www/html/nextcloud/'

[root@lamella_t1 nextcloud]# setsebool -P httpd_can_network_connect on

[root@lamella_t1 nextcloud]# setsebool -P httpd_can_network_connect_db on   # for MySQl database access
-------------------------------------------------------------------------------

# raise php memory limit

[root@lamella_t1 nextcloud]# vim /etc/php.ini 

  # change `memory_limit` to `512M`

# enable opache

-------------------------------------------------------------------------------
[root@lamella_t1 nextcloud]# rpm -qa | grep -i opcache
php72w-opcache-7.2.21-1.w7.x86_64

[root@lamella_t1 nextcloud]# vim /etc/php.d/opcache.ini

# the following entries were/modified and enabled: 

zend_extension=opcache.so
opcache.enable=1
opcache.enable_cli=1
opcache.memory_consumption=128
opcache.interned_strings_buffer=8
opcache.max_accelerated_files=10000
opcache.validate_timestamps=1
opcache.revalidate_freq=2
opcache.save_comments=1
-------------------------------------------------------------------------------


# installation from the command line (configuring database mostly) via `occ`
 
  # check  if all things are in the right place

[root@lamella_t1 nextcloud]# sudo -u apache php /var/www/html/nextcloud/occ 
Nextcloud is not installed - only a limited number of commands are available
Nextcloud 16.0.3

Usage:
  command [options] [arguments]

Options:
  -h, --help            Display this help message
  -q, --quiet           Do not output any message
  -V, --version         Display this application version
      --ansi            Force ANSI output
      --no-ansi         Disable ANSI output
  -n, --no-interaction  Do not ask any interactive question
      --no-warnings     Skip global warnings, show command output only
  -v|vv|vvv, --verbose  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug

Available commands:
  check                 check dependencies of the server environment
  help                  Displays help for a command
  list                  Lists commands
  status                show some status information
 app
  app:check-code        check code to be compliant
 integrity
  integrity:check-app   Check integrity of an app using a signature.
  integrity:check-core  Check integrity of core code using a signature.
  integrity:sign-app    Signs an app using a private key.
  integrity:sign-core   Sign core using a private key.
 l10n
  l10n:createjs         Create javascript translation files for a given app
 maintenance
  maintenance:install   install Nextcloud


### Installation Command ###

[root@lamella_t1 nextcloud]# sudo -u apache php occ maintenance:install --database "mysql" --database-name "lamella_bank" --database-host "10.100.180.8" --database-user "lamella_squad" --database-pass "~13579ZxP" --admin-user "admin" --admin-pass "~13579ZxP~"


#ERROR# 

Error while trying to create admin user: Failed to connect to the database: An exception occurred in driver: SQLSTATE[HY000] [2059] Authentication plugin 'caching_sha2_password' cannot be loaded: /usr/lib64/mysql/plugin/caching_sha2_password.so: cannot open shared object file: No such file or directory

# debug attempt 1 --> configured mysql server to listen to all available networks (via `bind-address=0.0.0.0` in `/etc/my.cnf`) 

# debug attemp 2 --> log in to MySQL server --> change authentication method for `root` & `lamella_squad`

--------------------------------------------------------------------------------------

[estrella@data1 ~]$ mysql -u root -p

Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 11
Server version: 8.0.17 MySQL Community Server - GPL

Copyright (c) 2000, 2019, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '~13579ZxP~';
Query OK, 0 rows affected (0.00 sec)

mysql> ALTER USER 'lamella_squad'@'10.100.180.%' IDENTIFIED WITH mysql_native_password BY '~13579ZxP~';
Query OK, 0 rows affected (0.00 sec)

mysql> ALTER USER 'lamella_squad'@'localhost' IDENTIFIED WITH mysql_native_password BY '~13579ZxP~';
Query OK, 0 rows affected (0.01 sec)
 
mysql> ^DBye

--------------------------------------------------------------------------------------

[change to mysql_native_password link](https://stackoverflow.com/questions/50169576/mysql-8-0-11-error-connect-to-caching-sha2-password-the-specified-module-could-n)

### SUCCESS! ### 

[root@lamella_t1 nextcloud]# sudo -u apache php occ maintenance:install --database "mysql" --database-name "lamella_bank" --database-host "data1" --database-user "lamella_squad" --database-pass "~13579ZxP~" --admin-user "lamella_admin" --admin-pass "~13579ZxP~"
Nextcloud was successfully installed



### VERIFY ### 

# change firefox settings to `.pac URL` 

# 10.100.180.5/nextcloud --> `Access through untrusted domain` error 

  # SOLUTION: add route to array of "trusted_domains" in `config.php`


# issue: admin cannot log-in 

  # troubleshoot: use occ command to list avail users


## solution: use occ-command to reset password for user

  # get all occ-commands:

--------------------------------------------------------------------------------------
[estrella@lamella_t1 ~]$ sudo -u apache php /var/www/html/nextcloud/occ list
[sudo] password for estrella: 
Nextcloud 16.0.3

Usage:
  command [options] [arguments]

Options:
  -h, --help            Display this help message
  -q, --quiet           Do not output any message
  -V, --version         Display this application version
      --ansi            Force ANSI output
      --no-ansi         Disable ANSI output
  -n, --no-interaction  Do not ask any interactive question
      --no-warnings     Skip global warnings, show command output only
  -v|vv|vvv, --verbose  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug

Available commands:
  check                               check dependencies of the server environment
  help                                Displays help for a command
  list                                Lists commands
  status                              show some status information
  upgrade                             run upgrade routines after installation of a new release. The release has to be installed before.
 activity
  activity:send-mails                 Sends the activity notification mails
 app
  app:check-code                      check code to be compliant
  app:disable                         disable an app
  app:enable                          enable an app
  app:getpath                         Get an absolute path to the app directory
  app:install                         install an app
  app:list                            List all available apps
  app:remove                          remove an app
  app:update                          update an app or all apps
 background
  background:ajax                     Use ajax to run background jobs
  background:cron                     Use cron to run background jobs
  background:webcron                  Use webcron to run background jobs
 config
  config:app:delete                   Delete an app config value
  config:app:get                      Get an app config value
  config:app:set                      Set an app config value
  config:import                       Import a list of configs
  config:list                         List all configs
  config:system:delete                Delete a system config value
  config:system:get                   Get a system config value
  config:system:set                   Set a system config value
 dav
  dav:create-addressbook              Create a dav addressbook
  dav:create-calendar                 Create a dav calendar
  dav:list-calendars                  List all calendars of a user
  dav:move-calendar                   Move a calendar from an user to another
  dav:remove-invalid-shares           Remove invalid dav shares
  dav:sync-birthday-calendar          Synchronizes the birthday calendar
  dav:sync-system-addressbook         Synchronizes users to the system addressbook
 db
  db:add-missing-indices              Add missing indices to the database tables
  db:convert-filecache-bigint         Convert the ID columns of the filecache to BigInt
  db:convert-mysql-charset            Convert charset of MySQL/MariaDB to use utf8mb4
  db:convert-type                     Convert the Nextcloud database to the newly configured one
 encryption
  encryption:change-key-storage-root  Change key storage root
  encryption:decrypt-all              Disable server-side encryption and decrypt all files
  encryption:disable                  Disable encryption
  encryption:enable                   Enable encryption
  encryption:encrypt-all              Encrypt all files for all users
  encryption:list-modules             List all available encryption modules
  encryption:set-default-module       Set the encryption default module
  encryption:show-key-storage-root    Show current key storage root
  encryption:status                   Lists the current status of encryption
 federation
  federation:sync-addressbooks        Synchronizes addressbooks of all federated clouds
 files
  files:cleanup                       cleanup filecache
  files:recommendations:recommend     
  files:scan                          rescan filesystem
  files:scan-app-data                 rescan the AppData folder
  files:transfer-ownership            All files and folders are moved to another user - shares are moved as well.
 group
  group:add                           Add a group
  group:adduser                       add a user to a group
  group:delete                        Remove a group
  group:list                          list configured groups
  group:removeuser                    remove a user from a group
 integrity
  integrity:check-app                 Check integrity of an app using a signature.
  integrity:check-core                Check integrity of core code using a signature.
  integrity:sign-app                  Signs an app using a private key.
  integrity:sign-core                 Sign core using a private key.
 l10n
  l10n:createjs                       Create javascript translation files for a given app
 log
  log:file                            manipulate logging backend
  log:manage                          manage logging configuration
  log:tail                            Tail the nextcloud logfile
  log:watch                           Watch the nextcloud logfile
 maintenance
  maintenance:data-fingerprint        update the systems data-fingerprint after a backup is restored
  maintenance:mimetype:update-db      Update database mimetypes and update filecache
  maintenance:mimetype:update-js      Update mimetypelist.js
  maintenance:mode                    set maintenance mode
  maintenance:repair                  repair this installation
  maintenance:theme:update            Apply custom theme changes
  maintenance:update:htaccess         Updates the .htaccess file
 migrations
  migrations:execute                  Execute a single migration version manually.
  migrations:generate                 
  migrations:generate-from-schema     
  migrations:migrate                  Execute a migration to a specified version or the latest available version.
  migrations:status                   View the status of a set of migrations.
 notification
  notification:generate               Generate a notification for the given user
 security
  security:certificates               list trusted certificates
  security:certificates:import        import trusted certificate
  security:certificates:remove        remove trusted certificate
 sharing
  sharing:cleanup-remote-storages     Cleanup shared storage entries that have no matching entry in the shares_external table
 trashbin
  trashbin:cleanup                    Remove deleted files
  trashbin:expire                     Expires the users trashbin
 twofactorauth
  twofactorauth:cleanup               Clean up the two-factor user-provider association of an uninstalled/removed provider
  twofactorauth:disable               Disable two-factor authentication for a user
  twofactorauth:enable                Enable two-factor authentication for a user
  twofactorauth:enforce               Enabled/disable enforced two-factor authentication
  twofactorauth:state                 Get the two-factor authentication (2FA) state of a user
 update
  update:check                        Check for server and app updates
 user
  user:add                            adds a user
  user:delete                         deletes the specified user
  user:disable                        disables the specified user
  user:enable                         enables the specified user
  user:info                           show user info
  user:lastseen                       shows when the user was logged in last time
  user:list                           list configured users
  user:report                         shows how many users have access
  user:resetpassword                  Resets the password of the named user
  user:setting                        Read and modify user settings
 versions
  versions:cleanup                    Delete versions
  versions:expire                     Expires the users file versions

--------------------------------------------------------------------------------------

# list all resetpassword options

--------------------------------------------------------------------------------------

[estrella@lamella_t1 ~]$ sudo -u apache php /var/www/html/nextcloud/occ user:resetpassword -h
Description:
  Resets the password of the named user

Usage:
  user:resetpassword [options] [--] <user>

Arguments:
  user                     Username to reset password

Options:
      --password-from-env  read password from environment variable OC_PASS
  -h, --help               Display this help message
  -q, --quiet              Do not output any message
  -V, --version            Display this application version
      --ansi               Force ANSI output
      --no-ansi            Disable ANSI output
  -n, --no-interaction     Do not ask any interactive question
      --no-warnings        Skip global warnings, show command output only
  -v|vv|vvv, --verbose     Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug

--------------------------------------------------------------------------------------

# reset password

--------------------------------------------------------------------------------------
[estrella@lamella_t1 ~]$ sudo -u apache php /var/www/html/nextcloud/occ user:resetpassword lamella_admin
Enter a new password: 
Confirm the new password: 
Successfully reset password for lamella_admin
--------------------------------------------------------------------------------------

# this did not work..! 

# enable SSL for your http service --> check `mod_ssl` 

```

### LIVE links:

[occ command link 1](https://doc.owncloud.org/server/10.0/admin_manual/configuration/server/occ_command.html#user-commands)

[trusted domain link](https://docs.nextcloud.com/server/16/admin_manual/installation/installation_wizard.html#trusted-domains)

[occ NC doc](https://docs.nextcloud.com/server/16/admin_manual/configuration_server/occ_command.html#command-line-installation-label) 

[NC download page](https://nextcloud.com/install/#instructions-server) 

[NC deploy official](https://docs.nextcloud.com/server/16/admin_manual/installation/source_installation.html)

[NC cmd install 1](https://docs.nextcloud.com/server/16/admin_manual/installation/command_line_installation.html) 

[NC cmd install 2](https://docs.nextcloud.com/server/16/admin_manual/configuration_server/occ_command.html#command-line-installation-label) 

[mysql change pass 1](https://stackoverflow.com/questions/49194719/authentication-plugin-caching-sha2-password-cannot-be-loaded) 

[mysql change pass 2](https://stackoverflow.com/questions/50169576/mysql-8-0-11-error-connect-to-caching-sha2-password-the-specified-module-could-n) 

[centos SIG repo?](https://wiki.centos.org/AdditionalResources/Repositories/SCL)

@
@
@
@
@
@
@
@
@
@
===========================PRE-LIVE DOCUMENTATIONS=============================

sudo rpm -ivh http://repo.mysql.com/mysql-community-release-el7-6.noarch.rpm

# install release-specific version of MySQL server based on added repo



# e.g. for Centos Release 7.6: 

sudo yum install mysql80-community-release-el7-6.noarch.rpm

sudo yum install mysql-community-server 

# start mysql server 

systemctl start mysqld.service 

  # you can enable later

# configure root password  

mysql_secure_installation 

# enter database 

mysql -u root -p   # enters mysql prompt 

# select "Create User"

'lamella'@'localhost' --> identified by '<password here>'

'lamella'@'<10.100.180.%>' --> identified by '<same password here>'  # create a cloud user that will be available across the entire internal subnet

# create database, select "Create Database" 'cloud_9' 

# grant privilege to user `lamella` to access database

  # select "Grant All Privileges On" --> 'nextcloud.*' to 'lamella'@'localhost'; 

  # repeat, enter 'lamella'@'10.100.180.5' (or "%") 

# select "Flush Privileges" --> ???

# "CTRL-D" --> quit mysql prompt 

```

1a. Install MySQL Client on Lamella Servers 


```
sudo yum install mysql80-community-release-el7-6.noarch.rpm

sudo yum install mysql-community-client

# try connecting as user `lamella` from lamella servers to the standalone database from step "1." 

mysql -h 10.100.180.<database IP> -u lamella -p  

[link for testing connnection to remote database](https://support.rackspace.com/how-to/mysql-connect-to-your-database-remotely/)

```

2. Install **Apache Server & PHP** Packages

  - EPEL, & webtatic repositories must be installed

```
yum install epel-release
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm

```

  - `yum repolist` to verify
 
  - install the newest PHP version (7.3) 

```

# this includes apache:

yum install php72w php72w-dom php72w-mbstring php72w-gd php72w-pdo php72w-json php72w-xml php72w-zip php72w-curl php72w-pear php72w-intl setroubleshoot-server bzip2 php73w-opache php73w-mysql httpd nextcloud-httpd(?) mod_ssl

  ## replace all php package names with should start with `php73w`

- [nextcloud 16 installation docs](https://docs.nextcloud.com/server/16/admin_manual/installation/source_installation.html)

# check against the following package list from the official NextCloud documentation: 

rh-php72 rh-php72-php rh-php72-php-gd rh-php72-php-mbstring \
rh-php72-php-intl rh-php72-php-pecl-apcu rh-php72-php-mysqlnd rh-php72-php-pecl-redis \
rh-php72-php-opcache rh-php72-php-imagick
 
  ## more info on `opcache` (https://www.sitepoint.com/understanding-opcache/)

  ## `opcache` features (https://stackoverflow.com/questions/17224798/how-to-use-php-opcache) 

  ## info on enabling `mod_ssl` for Apache server (https://stackoverflow.com/questions/5257974/how-to-install-mod-ssl-for-apache-httpd)

  ## [what is setroubleshoot-server](https://centos.pkgs.org/7/centos-x86_64/setroubleshoot-server-3.2.30-3.el7.x86_64.rpm.html)

  ## [[what is setroubleshoot-server 2](https://danwalsh.livejournal.com/20931.html) 

  ## [php-smbclient package](https://centos.pkgs.org/7/epel-x86_64/php-smbclient-0.9.0-1.el7.x86_64.rpm.html) 

``` 

3. Install & Configure NextCloud 

- 

- mod `/var/www/nextcloud` to use the database user created 

- configure `opcache` (/etc/php.d/opcache.ini)

  - follow Wei's Lamella update documents, marksei docs, & Lamell7 installations 

- look into Redis server, a " a fast in-memory key-value store that speeds up everything in NextCloud "


#. Install & Configure opcache, a PHP-built in extension/caching engine that stores pre-compiled script byte-code in shared memory--this removes the need for  PHP to load & parse script for each request, thus eliminating the need to reprocess the same file per request

  - references: 

    - [tutorial 1](https://www.tecmint.com/install-opcache-in-centos-7/) 

    - [explanation 1](https://hoststud.com/resources/what-is-opcache-and-steps-to-enable-opcache-in-cpanel.435/) 



**last step**. Set up SSL certificate for NextCloud servers --> ensure that your comms are encrypted

- Resources: 

  - [go to SSL section in here](https://bayton.org/docs/nextcloud/installing-nextcloud-on-ubuntu-16-04-lts-with-redis-apcu-ssl-apache/)

  - [apache SSL 1](https://geekflare.com/apache-setup-ssl-certificate/)

  - [apache SSL 2](https://www.digitalocean.com/community/tutorials/how-to-create-an-ssl-certificate-on-apache-for-centos-7)

  - [apache SSL 3](https://www.thegeekdiary.com/centos-rhel-how-to-enable-ssl-for-apache/)

  - [in depth on caching](https://stackoverflow.com/questions/10137857/is-redis-just-a-cache) 

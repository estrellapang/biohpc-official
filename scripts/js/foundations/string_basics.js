/* String Syntax Rules & Tools in Javascript */ 

//
//
//

/* Placing Literal Quotation Marks in Strings */
//
// also known as "escaping a quote" 
//
// CHARACTER: backslash, "\" 
//
// SYNTAX, DOUBLE QUOTE:        var = "string \"string quote\".";   
//
// SYNTAX, SINGLE QUOTE:        var = 'string \'string quote\'.';
//
// NOTE the position of \ at the beginning and end of the literal quote is different with respect to the "" marks

var legible = "Francis Drake once famously stated, \"Parvis Magna,\" which means Greatness from small Beginnings"; 

console.log(legible);

//
//
//

/*	Using single quotes & double quotes within eachother	*/ 

// PURPOSE: good alternative to using escape characters, which can really make the script look confusing at times

// e.g. no escape character: 
// var myStr = '<a href="http://www.example.com" target="_blank">Link</a>';

// e.g. with escape character:
// var myStr = "<a href=\"http://www.example.com\" target=\"_blank\">Link</a>";
 
// NOTE: The outermost quote characters MUST ONLY BE USED at the BEGINNING & END of the sstring!!! 

// e.g. CORRECT METHOD:  using outer "" marks:

var string1 = "This Sunday's Gospel Hymn was titled 'Let All Mortal Flesh Be Silenct', which carried a message more relevant than ever in today's society";

console.log(string1); 

  // e.g. WRONG METHOD:   single quotes outside:

/*

var string1 = 'This Sunday's Gospel Hymn was titled "Let All Mortal Flesh Be Silenct", wh    ich carried a message more relevant than ever in today's society';

  ### see how the apostraphe in "Sunday's" tells the interpreter to end the string? 

*/

//
//
//

/*	Additional Escape Characters/Sequences in Strings	*/

// In Addition to escaping quotes inside JS strings, additional characters that can be escaped are:

/*

\'	single quote
\"	double quote
\\	backslash
\n	newline
\r	carriage return
\t	tab
\b	word boundary
\f	form feed

*/

// e.g. of a string using multiple escape characters

console.log('the following string was created without any space, but only via escape characters'); 

console.log(escapeString="FirstLine\n\t\\SecondLine\nThirdLine");

//
//
//

/*	CONCATENATE "+" OPERATOR in JS	     */

// PURPOSE: combine strings/variables of the same type together

// SYNTAX: no spaces are added in concatenating strings together---add your own spaces 

console.log(HeSaid = "I am the Alpha" + " and the Omega," + " the beginning and the end.");

console.log("Testing if declared\n", HeSaid);

// COMMON MISTAKE: using the declare function `var` in `console.log()`:

/* This would result in ERROR:

console.log(var HeSaid = "I am the Alpha" + " and the Omega," + " the beginning a    nd the end.");

  ## `var` is not needed in front of `HeSaid`

*/

//
//
//



/*	Concatenating via "+="		*/

//PURPOSE: concatenate string to existing variable

/* USES: 

1. shorthand for stringVar = stringVar + "<string>"; 

2. ?						*/

// e.g. 1: Append String to String Variables

var johnConner = "No Fate";

console.log(johnConner += " But What We Make");

console.log("testing concat string to variable result,\n",johnConner);

// e.g. 2: Append string variables to string variables

var onTop = "Sky Above";

var onBottom = ", Ground Below";

console.log(startPack = onTop + onBottom);

//
//
//

/*	Concatenate strings with string variables	*/

var insertString = "Christmas Spirit";

console.log(holidayGreetings = "Oh what a wonderful Holiday this year! It must be the " + insertString + " that has filled the air with Joy!");


//NOTE: COMMON MISTAKE: Re-Declaring while concatenate-appending:

/*

var myStr = "This is the first sentence";

var myStr += "This is the second sentence."; 

  ## "var" is not NECESSARY on the second line!
 
*/





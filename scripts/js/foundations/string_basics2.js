/* JAVASCRIPT STRING SYNTAX and TOOLs --- Part II */

//
//
//

/*	The `.length` property		*/ 

// PURPOSE: determine length of strings

// RULES: begins count at "0" 

// e.g. 1: determine length of a string and assign to variable

var aNumber= 0; 

console.log(aNumber);

var templateString = "No Where But Here"; 

console.log(aNumber = templateString.length);

//
//
//

/* 	Bracket Notataion "[]"  	*/

//PURPOSE: pinpoint to a SPECIFIC index within strings 

/* RULES: In JS, once a STRING variable is declared, INDIVIDUAL INDEXES CANNOT BE CHANGED---the only way is to RE-ASSIGN NEW STRING to variable */

//e.g. 1. Find Nth character in string and assign it as variable

var myName = "Heres"; 

var theRlettersIndex = myName[2];   // notice how it's 2 and not 3?

console.log(theRlettersIndex); 


// e.g. 2 Find the Nth from LAST character in strings

// SYNTAX: COMBINE Bracket Notation with `.length` property

// NthtoLastCharacter = stringVar[stringVar.length - N];

var sweet = "Dessert";

var lastE = sweet[sweet.length - 3]; 

console.log(lastE);


/* e.g. stitching string vars with undeclared strings together */ 

// PRACTICE: notice the behavior of `console.log` --> ASSIGNMENTS & DECLARATIONS are executed for real and not simply for displaying "what/if" outputs

console.log("Below is FCoCamp's 'Mad Libs' exercise");

var myNoun = "dog";
var myAdjective = "big";
var myVerb = "ran";
var myAdverb = "quickly";

var wordBlanks = ""; // Only change this line;

console.log(wordBlanks = "a " + myAdjective + " " + myNoun + " " + myVerb + " rather " + myAdverb + " into our lives");

console.log(wordBlanks);

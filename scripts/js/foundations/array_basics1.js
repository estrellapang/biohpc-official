/*	ARRAYS: Rules + Syntax	   */

/* TOPICS: declare syntax, accessing array elements via indexes, `.push()` function, `.pop()` function, `.shift()` & `.unshift()` functions */

/*	DECLARATION SYNTAXES	 */

// Square Brackets are used " [ ] " to DECLARE ARRAYS 

// ELEMENTS are separated by COMMAS, "," 

// e.g. `var new_language= ["Trial and Error", 2019, 2020]; 

	// an array containing 1 string & 2 number elements 

//
//
//

/*	DIFFERENCES Between STRING Vars & ARRAYS	 */

// SIMILARITIES

	// INDEXES can be used to specify values in variables

// DIFFERENCES

	// values within ARRAYS ARE MUTABLE (not in STRING VARS)

//
//
//


/*	Nested Arrays	   */

// "Multi-dimensional Arrays"

// e.g. 

var threeTier = [[[3, 3, 3,], ["one", "one", "one"], [4, 4, 4,]]];

console.log(threeTier);

//
//
//

/*	Access Array Data with Indexes   	*/

// Array elements can be pointed out via their indexes

// e.g. assign 1st element of array to new variable 

// oneArray = [ 1, 2, 3];

// var = newVar;

// console.log (newVar = oneArray[0]); 

  // `console.log()` CANNOT BE USED to DECLARE variables, therefore `newVar` has to be first declared! 

 // SYNTAX: for clarity--> do `array[]` and not `array []`---> SPACES can be confusing

//
//
//


/*	Accessing Elements Inside Nested Arrays   	*/


/*
var arr = [
  [1,2,3],
  [4,5,6],
  [7,8,9],
  [[10,11,12], 13, 14]
];
arr[3]; // equals [[10,11,12], 13, 14]
arr[3][0]; // equals [10,11,12]
arr[3][0][1]; // equals 11

  // SYNTAX: No spaces between the brackets when specifying indexes;

*/ 

//
//
//


/*	`push()` function---APPEND to END of Arrays	*/

/* 
var arr = [1,2,3];
arr.push(4);
// arr is now [1,2,3,4]
*/

// e.g. push an array into a NESTED ARRAY

var theDread = [["can", "only", "be"], ["overcome", "with", "a"]]; 

console.log(theDread); 

// Method 1: Push A DECLARED  VARIABLE 
theFix = ["sense", "of", "humor"];

theDread.push(theFix); 

console.log(theDread);

/* Method 2: Push UNDECLARED ARRAY

theDread.push(["sense", "of", "humor"]);

*/ 

/* WHAT DOESN'T WORK

DECLARE & PUSH SIMULTANEOUSLY (this should WORK??!)
var theFix = theDread.push(["sense", "of", "humor"]);

	// however, this will work for `.pop(),`.shift()`, & `.unshift()`

OR doing the same INSIDE `console.log()`

console.log(theFix = theDread.push(["sense", "of", "humor"]));

*/

//
//
//

/*	.pop() function: REMOVE From END of Arrays	*/


/* e.g. 1  

var threeArr = [1, 4, 6];
var oneDown = threeArr.pop();
console.log(oneDown); // Returns 6
console.log(threeArr); // Returns [1, 4]  */

//e.g. 2 --> REMOVE an array from a NESTED array

var myArray = [["John", 23], ["cat", 2]]; 

var removedFromMyArray = myArray.pop(1);

  // notice how "removedFromMyArray" did not need to be declared separately?

  // same syntax doesn't work for `.(push)`

console.log(removedFromMyArray);
console.log(myArray);


//
//
//

/*	.shift() function: REMOVE From BEGINNING of Arrays	*/ 

/* e.g. 1

var ourArray = ["Stimpson", "J", ["cat"]];
var removedFromOurArray = ourArray.shift();
// removedFromOurArray now equals "Stimpson" and ourArray now equals ["J", ["cat"]]

*/


//e.g. 2 --> REMOVE an array at the BEGINNING of a NESTED array 


var thisFriday = [["is", 12, "13th"],
["but", "I feel", 10]]; 

var ridDate = thisFriday.shift(0); 

  // alternative: `var ridDate = thisFriday.shift();`
console.log(thisFriday);

//
//
//

/* 	.unshift() function: APPEND From BEGINNING of Arrays	*/

//PURPOSE: opposite of `.push()`, to add to BEGINNING of arrays


/* e.g. 1

var ourArray = ["Stimpson", "J", "cat"];
ourArray.shift(); // ourArray now equals ["J", "cat"]
ourArray.unshift("Happy");
// ourArray now equals ["Happy", "J", "cat"]

*/


//e.g. 2 --> REMOVE THEN APPEND FROM BEGINNING of NESTED ARRAY


var xmasPresents = [

[2, "3D Prints"],
[3, "Store Bought"],
[5, "Greeting Cards"] 

];

console.log(xmasPresents); 

var lazyHands = xmasPresents.shift(1);
  // '(1)' doesn't take out the 2nd array, only the 1st
  // these functions seem to not work with indexes
console.log(lazyHands);


var selfImprove = xmasPresents.unshift([10, "3D Prints!"]);
console.log(xmasPresents);



// practice example: "Sunday's Shopping List"

console.log("This is what Lily & I need this Sunday!");
var myList = [
    ["Chlrox Wipes",3],
    ["Men's Pomade",1],
    ["Tooth Brushes",2],
    ["Hair Curler",1],
    ["Bully Stick",2]
]; 

console.log(myList);


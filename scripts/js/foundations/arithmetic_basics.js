// Using Basic Operators to Perform Simple Arithmetic Tasks in Javascript 


// Addition 

var sum = 15 + 6; 

console.log(sum)

// Subtraction --> "-" 

// Division --> "/"

// Multiplication --> "*" 

// The universal syntax, essentially---one can perform these operations with NUMBERS & FLOATING POINT/DECIMALS without declaring variables:

// E.G. use `console.log()` to output string and floating point multiplication to console

console.log ('product of 5.0 & 5.5 is', 5.0 * 5.5);

//
//
//


/* Incrementing Numbers */ 

// "++" operator !!! 

// "++" = "i = i +1"

i = 8;
i = i + 1;
console.log(i);

newNum =6; 
newNum++;  // no "=" needed for the incremental operator! 

console.log(newNum);


/* Decrement Numbers */ 

// "--" operator !!! --> same rules as increment operator

//
//
//


/* Calculating the Remainder */

// "%" operator  --> SYNTAX: <dividend> % <divisor> 

// APPLICATION: the remainder from DIVIDING BY 2 is used to dictate whether numbers are odd or even
// Remainder of "1" indicates an odd number where as "0" an even number 

console.log('the remainder of 9/2 is ', 9 % 2,',which means 9 is an odd number');
console.log('the remainder of 8/2 is ', 8 % 2,',which means 8 is an odd number');

//
//
//

/* Augmented Addition Operator */

// a short hand for performing 1. Mathematical operation & 2. Assignment 
// " += " operator

// SYNTAX:
// var = var + 1    <-->   var += 1; 

var x = 1;
x += 2;		// x = x + 2;

console.log('x is added by value of 1 and its new value is:');
console.log(x); 


//
//
//

/* Augemented Subtraction */

// var = var - 1   <--> var -= 1; 

var y = 10;
y -= 1;		// y = y - 1;  

console.log('y is subtracted by value of 1 and its new value is:');
console.log(y);

/* Augemented Multiplication */ 

// var = var * real number   <--> var *= real number;

var w = 12; 

w *= 5;		// w = w * 5; 

console.log('w is augment multiplied by 5'); 
console.log(w); 

//
//
//


/* Augemented Division */

// var = var / real number   <--> var /= real number;

console.log(" z's initial value is ", z=9, "z is augment divided by 3 and now equals", z /= 3);

//
//
//



/*	RESOURCES	*/

/* [console.log() print methods]( https://www.juniordevelopercentral.com/javascript-print-to-the-console/)

*/

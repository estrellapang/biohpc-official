// Storing Values within Variables 

/* 
" = " assignment operator does the bulk of the work; 

SYNTAX: 

"everything to the RIGHT of the operator is resolved" 

<variable/place holder> = <value/**resolvable** variable to be assigned> 

e.g.

-------------------------------------------------
myVar = 5;
myNum = myVar;

# This assigns 5 to myVar and then resolves myVar to 5 again and assigns it to myNum
-------------------------------------------------

*/ 


// Practice

/*
Example of ERRONEOUS variable assignment below---value of "3" is assigned to variable "atlas," but the value "broken" to be resolved in order to br assigned to "atlas"---there is no value that "broken" can resolve to

One would get an error message e.g.

-------------------------------------------------
var atlas = broken; 
            ^
ReferenceError: broken is not defined
-------------------------------------------------
 
*/ 

var atlas = 1;

/* WRONG WAY: */

//`var atlas = broken;`

console.log(atlas);

//console.log(broken); 


/* CORRECT WAY: */

var broken = atlas; 

console.log(broken);

// Sic Parvis Magna 

/* 
# 7 Data Types in JAVASCRIPT 
 
undefined, null, boolean, string, symbol, number, and object

null --> nothing
boolean --> true or false
symbol --> immutable primitive value that's unique (?)
object --> store key value pairs (?)

# What are Variables?

"Allows computers to store and manipulate data in a dynamic fashion...Labels that point to the data" 

# SYNTAX: similar to Java/C++, each statement conclides with a SEMICOLON ";"

# SYNTAX: Variable names can be made up of numbers, letters, and $ or _, but may not contain **spaces** or **start with a number**

*/ 



/*
# declare via `var` 
# var declares are **GLOBAL**, throughout the program 

# NOTE that STRING values need quotations
# SYNTAX: similar to Java/C++, each statement conclides with a SEMICOLON
*/ 
var myName;

// `console.log()` returns state of variables at specific points in the script to the console  
console.log(myName)	// NO VALUE ASSIGNED: should return "null" or "undefined"

var myName  = "man seizes control"; 

//NOTE: a string can also be declared with apostraphes '' 


/* 
# declare via `let` 
# let values only hold true  within the function in which it is declared 

*/ 

let myAge = "Always";


/*
# declare via `const`
# can be declared ONLY ONCE and whose VALUE CANNOT change 

*/

const Trinity = "Eternal";

console.log(myName)
console.log(myAge)
console.log(Trinity)

//
//
//
//
//


/* Best Practice for Case Sensitivity: 

- **camelCase** --- multi-workd variables being with lower case letters and only has the first letter of every subqsequent character capitalized 


e.g. 
---------------------------
heresEstrella 
vermisMeridiem
--------------------------- 

//	Functios in JS: Part I, The Basics

/*	SYNTAX & CONCEPT

# Functions are reusable code that programmers create so that the same code does not need to be written over and over again, e.g. `console.log`, `.length()`, `.shift()`, etc.

# SYNTAX

```
function functionName() {

<commands/functions>;
<commands/functions>;
.
.
. 
 
}

```	*/


/*	"NIN HAO" Function	*/

function reusableFunction() {

console.log("Hi World");

}

reusableFunction(); 
// here I'm calling the function, note that if I use this call as input into "console.log()", an ERROR would be outputted from the function "not being defined"


/* 	PARAMETERS IN FUNCTIONS		 */

/*

### Parameters & Functions:

- Parameters are variables that act as placeholders for the values that are to be input to a function when it is called; The actual values that are input (or "passed") into a function when it is called are known as arguments.

### e.g. Sample Function with 2 Parameters
----------------------------------------------
# function:

function testFun(param1, param2) {
  console.log(param1, param2);
}

# call command:

testFun("Amor","Fati"); --> output would be "Amor, Fati"
----------------------------------------------

*/


/*	subtraction function & sum function	*/

function ourFunctionWithArgs(a, b) {
  console.log(a - b);
}
console.log("Subtracting 5 from 10 via 'ourFunctionWithArgs' function");
ourFunctionWithArgs(10, 5);

function functionWithArgs(x, y) {
console.log(x + y)
}
console.log("Finding the sum of integers '3' & '9' by calling 'functionWithArgs' function with these numbers as input arguments: ")
functionWithArgs(3,9); 





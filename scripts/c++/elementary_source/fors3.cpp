// Example of Range based for loop: Example w/h No String Declaration! 
// 
//
// to compile code that contains this feature, one may have to include the `-std=c++11` option in the compiling command
#include <cstdio>
using namespace std; 

int main ()	
{

for (char rg : "Hallehluyah")	{	// remember to specify variable type (e.g. `int` or `char`) 

if (rg == 0) break;   // no need for brackets if the statement and the conditionals will be on one line

printf ("%c\n", rg);  

} 

return 0; 
} 

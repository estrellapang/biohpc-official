// Examples of Range based for loops 
//
// Range based for loops are a new feature since C++11: Eliminating the Extra Space Caused by 0 Appended to String 
//
// to compile code that contains this feature, one may have to include the `-std=c++11` option in the compiling command
#include <cstdio>
using namespace std; 

int main ()	
{

char prim_string[] = "Hallehluyah";

for (char rg : prim_string)	{	 

   if (rg == 0) { break;}  

printf ("%c\n", rg);  

} 

return 0; 
} 

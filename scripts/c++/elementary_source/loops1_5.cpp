/* Basic While Loop 1.5: reading out elements of an array--using "continue" statement to skip specific elements in the arry*/ 

#include <cstdio>
using namespace std;

int main () 
{ 

char loop1[] = "Unrelenting";  int i = 0;

while (loop1[i] != 0 && i < 11)	{  

	if (i==8 || i==9 || i==10)	{ 

	i++; 
	continue; //the loop will not finish if this was executed first! 

	} 
	
	printf("element %d holds the value %c\n", i, loop1[i]); 
	i++; 

} 

return 0; 

} 

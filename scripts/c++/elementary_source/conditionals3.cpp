// Ternary Operators
//
//
#include <cstdio> 
using namespace std; 

int main () 

{ 

	int a = 1;
	int b = 2;

	printf ("the larger value is %d\n", a > b ? a : b); 

/* The above condition is called a ternary conditional operator because it contains THREE OPERANDs, 1: "a > b"  2: "a" 3: "b". Essentially, the second operator is returned if the condition is true, and the thir operator is returned if the condition is false. */ 

return 0; 

} 

// Example of for-loop reading out a primitive string via incrementing a connected pointer "*kuai" 
//
#include <cstdio>
using namespace std; 

int main ()	
{

char prim_string[] = "Hallehluyah";  //remember to declare with "char" type!


for (char *kuai = prim_string; *kuai; kuai++)   /* remember to declare pointer with "char" type, again! Conditions inside the loop: 1. pointer variable "*kuai" equals to the value at the beginning of "prim_string" 2. while *kuai is pointing to a TRUE value (not zero) 3. "*kuai" is incremented at the end of each loop */ 

{

printf ("%c\n", *kuai);  

} 

return 0; 
} 

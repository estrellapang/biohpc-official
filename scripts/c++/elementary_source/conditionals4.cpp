// Multiway conditional using the `switch` statements
// which are good for matching variables to specific criteria, i.e. integer values (in this example)
// 
#include <cstdio> 
#include <iostream> 
using namespace std; 

int main () 

{ 

// declare & assign values to categorical labels

	const int father = 1;   // notice these are CONSTANT Variables! The switch statements NEED THEM to WORK!
	const int son = 2;
	const int hlysprt = 3; 

	int belief;	// this variable will store value for user input
	cout << "Greetings, traveler. Please input an integer of great importance to you:\n";
	cin >> belief; 
	cin.ignore ();  // ignore blank inputs 
	cin.get(); 

// start the switch statement 

	switch (belief)   {

	case father: puts ("you believe in the Father"); /* if this case matches, then all of the statements below will also execute unless a 'break' statement is placed underneath this */ 

	case son: puts ("you believe in the Son"); 

	case hlysprt: puts ("you believe in the Holy Spirit"); 
	
	break;   // this is a branching statement that exits the switch statement block 

	default: puts ("perhaps you should work hard on your Faith and Effort, my fellow travelor"); /* the default statement will be executed if no cases match the variable being matched ("belief")---however, this is not mandatory */   

	} 

return 0; 

} 

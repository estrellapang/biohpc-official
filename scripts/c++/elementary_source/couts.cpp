// introduction to cout 
//
// 
#include <iostream>
using namespace std; 

int main () 

{

cout << "This is the first line" << endl;   //`endl` terminates the line 
cout << "Here is my second string, which includes arithmetics: " << 3*4 << " see? that was result of 3*4!" << endl; 

/* "<<" is called a "bitwise left shift operator," which `cout` overloads with a stream of characters to the standard output stream */

return 0; 

} 

// This script lays out properties and syntaxical rules for primitive arrays
//
// Part I: Integer Arrays: Using Pointers to Access and Assign their values 
//
//
#include <cstdio> 
using namespace std; 

int main () 

{

	int arrayone[5];   // declare integer array of 5 elements 
	
	int *pointer = arrayone; // declare "*pointer" to utilize arrayone's address for integer values

 /* note that arrays can be accessed as if they were pointers! the statements below  assign values to each element in the array */  

	*pointer = 1;	// assigns integer value of 1 to the first array element
	++pointer; *pointer = 2;   //advances the pointer one element down the array, and assign value of 2 to second array element
	++pointer; *pointer = 3;
	*(++pointer) = 4;   //advances the pointer and assigns the value of the corresponding array element at the same time 
	*(++pointer) = 5;


   for (int i = 0; i != 5; ++i)	{

	printf ("this is element %d, ",i);
	printf ("and my value is %d\n", arrayone[i]);

	} 

return 0; 

} 



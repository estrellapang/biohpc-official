// This script lays out properties and syntaxical rules for primitive arrays
//
// Part II: Character Arrays (Primitive Strings) --- More approaches to Declaring Strings 
//
//
#include <cstdio> 
using namespace std; 

int main () 

{

	char letters[] = "AmorFati";  // declare and assign a primitive string, which is essentially a character array 

	printf("here is a character string: %s\n",letters);  // this cannot be done with integer arrays!

	// declaration in accordance to the definition of primitive strings below: 
	
	char scatter[] = {'i','n','r','i',0};	// primitive strings terminate with a "0"
	
	for (int i=0; scatter[i] != 0; i++)	{

	printf("%c\n", scatter[i]);	// use %c placeholder to print the string like an array  

	}  

	printf("printing out the primitive string directly\n%s\n",scatter);   // %s string holder can be used to print 

	int numbers[] = {1,3,5};  // an easier approach to assign array values compared it Part I

	// below is what's called a range-based for loop, which is a short-cut for printing out integer arrays
	
	for (int roller : numbers) { 

	// `if ( roller == 0) break;` --> if the array was a primitive string, this line would be needed to terminate
	
	printf("using range-based for loop: this element is: %d\n",roller); 



	}


	 
 
return 0; 

} 



//example showing the different properties of pointers & references in C++ 
//
//
#include <cstdio>
using namespace std;

int main () 

{

   int a = 3; 
   int *b = &a;  // b points to the memory location of a for its own value
   
   printf("value of a is %d\n", a);
   printf("the memory location for value of b (really, a) %d\n", b); 
   printf("the integer value of pointer variable *b is %d\n", *b); 

return 0; 

} 



// Basic If, Else If, and Else Command 
//
//
#include <cstdio> 
using namespace std; 

int main () 

{ 

	int a = 12;
	int b = 10;

	if (a == b)	{	// Watch out with the EQUAL operator: if "=" was here, then the condition would be True!

	puts("the if condition is True.");   } 

	else if (b < 10)   {

	puts("the else if condition is True.");   } 

	else if (a >= 9)   { 

	puts("the 2nd else if condition is True.");   } 

	else	{ 

	puts("the statement is False.");   } 

return 0; 

} 

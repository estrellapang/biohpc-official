#include<cstdio>

//"Hello World via functions `printf` & `put`
//
//The header file <cstdio> is equivalent with <stdio.h> 

int main()

{ 

 printf("Hello World! I'm `printf`!\n");
 
 puts("Hello World too! I'm `put`!");

 return 0;

}


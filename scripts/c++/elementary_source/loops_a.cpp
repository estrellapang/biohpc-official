// Basic While Loop 1_a: reading out elements of an array: using a "do-while" loop

#include <cstdio>
using namespace std;

int main () 
{ 

char loop1[] = "Unrelenting"; 

int i = 0;

// notice that with the do-while loop, the condition is at the end of the bracketed statements
do {  
	
	printf("element %d holds the value %c\n", i, loop1[i]); 
	i++; 

} while(loop1[i] != 0); // DON'T forget the semi-colon here! 


return 0; 

} 

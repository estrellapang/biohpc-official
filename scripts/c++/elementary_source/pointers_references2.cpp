//example showing the different properties of pointers & references in C++ 
//
//in the 2nd part,references are added to show their effects 

#include <cstdio>
using namespace std;

int main () 

{

   int a = 3; 
   int *b = &a;  // *b points to the memory location of a for its own value
   int c = 9;  // *b is referenced to the value of c

   b = &c;   /*unless the referenced variable is BEING declared (e.g. `int &c = ...`) for the first time,  the referencing notation has to be on the RIGHT side of expressions!*/ 

   printf("value of a is %d\n", a);
   printf("the value of pointer variable *b is %d\n", *b); 
   printf("value of c is %d\n", c);
    

return 0; 

} 



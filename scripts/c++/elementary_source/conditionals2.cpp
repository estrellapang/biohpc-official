// Basic If, Else If, and Else Command: Integer value of True & False 
//
//
#include <cstdio> 
using namespace std; 

int main () 

{ 

	int a = 1;
	int b = 2;

	printf ("the condition value for a > b is %d\n", a > b); 
	printf ("the condition value for a < b is %d\n", a < b); 

return 0; 

} 

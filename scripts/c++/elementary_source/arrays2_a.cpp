// This script lays out properties and syntaxical rules for primitive arrays
//
// Part II.5: Character Arrays (Primitive Strings): Illustrate How Primitive Strings Terminate with a 0
//
//
#include <cstdio> 
using namespace std; 

int main () 

{


	char faith[] = "INRI";   // this declaration form = "primitive string" 
	
	for (int i=0; faith[i]; i++)   /* faith[non-zero integer]=TRUE; faith[0]=FALSE!  As "i" increments past "3", the statement "faith[i] would equal to faith[0], which returns a FALSE in our while condition */ 

	{

	printf("%c\n", faith[i]); 

	}  
	 
 
return 0; 

} 



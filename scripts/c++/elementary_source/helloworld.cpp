#include<stdio.h>

//Not my first rodeo, but here goes "Hello World" again---except that this time, We Go One We Go All!
//
//Look at the headerfile "<stdio.h>," which is the standard C library---while C++ can use this, it does not contain
//everything exclusively needed for C++  
//
//We are using <stdio.h> to utilize the `printf()` function, otherwise, with <iostream> (for C++) we can only use `cout()`
//
//Good Practice: utilize both libraries :) 

int main(void)

{ 

 printf("Oh Hey~, Back into the Fray again?\n");
 return 0;

}

//For some reason, the version compiled on Linux cannot be run on the MAC, on wh//ich the source code had to be re-compiled in order to run "helloworld1"

// Basic While Loop I: reading out elements of an array

#include <cstdio>
using namespace std;

int main () 
{ 

char loop1[] = "Unrelenting";  /*no need for brackets in this assignment style---e.g. {"Unrelenting"}---however, they are needed when the individual elements are assigned one at a time. Refer to script `arrays2.cpp` */
int i = 0;

while (loop1[i] != 0)
/* "loop1[]" is an primitive string, which is automatically assigned a "0" in C++ */ 

{  
	
	printf("element %d holds the value %c\n", i, loop1[i]); 
// note the syntax for calling the value of +1 variables in printf!!! 
	i++; 

} 

return 0; 

} 

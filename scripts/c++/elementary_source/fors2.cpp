// Examples of Range based for loops 
//
// Range based for loops are a new feature since C++11 
//
// to compile code that contains this feature, one may have to include the `-std=c++11` option in the compiling command
#include <cstdio>
#include <iostream>   //this is really not needed for this script 
using namespace std; 

int main ()	
{

char prim_string[] = "Hallehluyah";

for (char rg : prim_string)	{	// remember to specify variable type (e.g. `int` or `char`), NOTICE how "rg" is "char" 


printf ("%c\n", rg);  /* there will be an aditional space at the end of the output; this is as result of the "0" automatically assigned at the end of the primitive string---this would not occur if the variable being operated on is an integer array (or if the string was read using traditional for-loop controls */ 


} 

return 0; 
} 

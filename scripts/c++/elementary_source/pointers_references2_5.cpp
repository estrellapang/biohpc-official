//example showing the different properties of pointers & references in C++ 
//
//in the 2.5 part,references are declared differently to show their effects 

#include <cstdio>
using namespace std;

int main () 

{

   int a = 3; 
   int *b = &a;  // *b points to the memory location of a for its own value
   int &c = *b;  // variable c is declared as a reference to pointer variable *b
   c = 9;

   printf("value of a is %d\n", a);
   printf("the value of pointer variable *b is %d\n", *b); 
   printf("value of c is %d\n", c);
    

return 0; 

} 

// output: a = 9, b = 9, c = 9 
//
/* even though pointer variable "*b" looks to the address of "a" for its value at first, it is later referenced to the value of "c." And because references cannot be changed once it is declared (unless an additional reference overwrites it in sequential code), the statement in lines 12 & 13 has to follow suit, in order to keep the reference true. Therefore, a has to be changed to 9 in order to satisfy statements in both line 13 and 14 follow suit, in order to keep the reference true. Therefore, a has to be changed to 9 in order to satisfy statements in both line 13 and 14. Look in part 2 to see a case when the reference definition is NOT enfoced */ 

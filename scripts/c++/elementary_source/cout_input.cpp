#include <iostream> 

using namespace std; 

int main() // `main()` is okay in C++ too, as the `int` return type is set as default

 { 
   
   string eloquency; 
   cout<<"Well hello! Who has executed this humble script may I ask?\n";
   cin>> eloquency;
   cin.ignore();
   cout<<"Hmm! I sure am pleased by your choice of me, "<< eloquency <<"!\n";
   cin.get();

 return 0; 

//in C++, main() returns 0 by defualt

 }

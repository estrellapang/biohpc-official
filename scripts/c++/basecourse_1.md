## C++ Fundamentals, Part 1 
 
> The languate that has direct access to system memory! Do things from the Ground Up!! 

### Overview 

- developed by Bjarne Stroustrup in 1970's

- an extension of the C language

- object oriented; reads sequentially

- utilizes at multiple machine layers:

  - Application 

  - Operating System 

  - Firmware

- Used to write other languages i.e. Java, PHP, Python, & Pearl 

- Major Componenets:

  - the Original C

  - C preprocessor 

  - C++ classes & objects 

  - C++ templates (for generic programming)

  - STL (C++ standard template library) 




### Basic C++ SYNTAX

- **Comments**: 

`//` --> single-line comments 


```  --> mutiple-line comments

/*

...

*/


```

- **Header Files**: 

`<cstdio>` or `<stdio.h>` ---> C standard I/O library 

`<iostream>` ---> C++ library

- do not use with open-bracket functios i.e. `int main ()`



- **Functions**: 

`int main()` --> function called upon by the OS as it is the main entry point into the program

`int main (int argc, char ** argv)` or `int main (int argc, char * argv[])` --> these two parameters are "count of arguments in the array," and "an array of character pointers," both of which are rarely used today except in the cases of command line programs 



- **Statements** (an unit of execution) 

  - Expressions: any line that returns a value---often statements as well

    - allows inclusion of operators and parentheses 

`;` --> Always used to terminate statements!

- Blank Spaces **ARE IGNORED!**: 

  - one can knit lines of code as tightly as possible

  - all the indents and spaces are really just for human-readability 


- Good Statement **Example 1**  


```
int =x;   # intialize x as an integer variable
x = 42;   # expression/statement assigning value of "42" to variable x 
printf("x is %d\n", x);   # `%d` this is a placeholder for ***integer values*** 

# `%c` is the placeholder for ***character values*** 

# alternatives for the statement above:  

printd("x is %d/n", x=42); # this is still valid, letting a single statement assign a value and return the value of "x" 

# note you still have to `int` "x" as an variable


# alternative with operator containing expressions

x = 42 + (3 * 5); 

``` 


- **Identifiers/Naming Conventions** 

  - components: 1. alphabetical letters 2. 0-9 numerals 3. underscore

  - **cannot conflict w/h reserved words**

  - **cannot begin w/h **numerals**

  - Generally, it is a good idea to begin **Class** and **Type** names with **Capital Letters** 

    - and everything else with **lower case letters**

![alt-text](https://www.oreilly.com/library/view/c-primer-plus/9780132781145/graphics/app-b-tab01.jpg) 
> list of reserved words in C++ 

  - keep **length under 31 characters** 

  - variables can begin with `_`. however, just **not two underscores**, as they are used for **private system identifiers**!

    - one underscore makes an identifier "private" 


- **Variables**

> in C/C++, a variable is a **typed (characterized)** and **named** location in memory---when a variable is created, it is assigned a **location** in memory an **value** associated with the variable is copied into the location
  
e.g. 

```
int x=3;
int *conduit = &x;  # this statement assigns the value stored in the **address if "x" to the pointer variable "*conduit"**, so that "*conduit" only holds an address in its memory rather than a defined value---this saves memory when practiced on bigger proportions

```

- declare multiple variables at once: 


`int [name1], [name2], [name3],...;` 


- Tokens: define a variable's value and type

    - `int` --> integer variable

  - Variables are NOT INITIALZED until their values have been assigned 

    - `int x = 7;` --> **good practice: define type & assign value simultaneously!** 

- Qualifiers: modify the nature of the variable types 

    - e.g. `const int y = 8;` --> instructs the compiler that the value of the variable cannot be changed once it has intialized; hence the variable becomes a **"read-only"** variable---if the line `y = 22;` is ran after this, the compiler would be prevented from assigning a new value to "y" 

- Pointers: a powerful & common data type

GENERAL SYNTAX: `*[variable name] = &[variable defined and assigned a value]` # `*` in C++ is called a pointer deference operator; '&' means "at the address of", which is another operator

**NOTE**: once a pointer variable is declared, i.e. `*x`, then by declaring another variable via `int x` would conflict with its initial variable type! ALWAYS refer to pointer variables with the ASTERISK INCLUDED in their variable name!
 

- References: an exclusive feature of C++; and compared to pointers, it is resistant to additional changes down the lines of code; the reference link overpowers that of the pointers. REMEMBER; references are NOT variables like pointer variables! 
 
  - for syntax-related rules on references & pointers, refer to `pointer_references*.cpp` inside `elementary_source/` sub directory


- **Primitive Types** -- what composes classes & structures 


- Primitive/Integer Arrays: 

> fixed size container for elements with a common type, e.g. an array of integers 

e.g.

```
int myarray[3]; # declare an array of fixed size: 3 elements 

myarray[0] = 1;   # first entry is element 0 

```

**range-based for loops**: much easier than the traditional way of printing out arrays; however, if your compiler's standard C++ version is not C++11 or higher; e.g. C++98, add in the following option: `-std=c++11` (because the range-based for loops is an C++11 extension) 

e.g. 

code: 

```

        int numbers[] = {1,3,5};

        for (int roller : numbers) {

        printf("here is an integer string: %d\n",roller);

	} 

```

- Primitive String / Character Arrays

  - essentially, strings can also be declared as character arrays 

  - however, string arrays whose value is assigned in the following fashion `char <arrayname>[] = "<a string>";` is automatically appended with a 0 at the end of it 

  - refer to `arrays.cpp & arrays2.cpp` 


- **Conditionals** [operators in C/C++](https://www.geeksforgeeks.org/operators-c-c/) 

  - True is represented by "1" or any integer that is NOT zero, and False if represented by "0"

  - check out `.cpp` files named `loop*` and `conditional*`

  - `switch()` conditionals require attributes being checked declared as constant variables (see "Qualifiers")

- For Loops: 

  - Contains **3** Different types of controls: `for ([pre-loop initialization)]; [while conditional]; [post-loop incrementation])` 

  - range-basedl loops: need option `-std=c++11` 

    - can escape the need for declaring strings/arrays in order to print them

e.g. of traditional for-loop structure 

`for (int i=0; i != 13 || i >= 0 || i++)`

`for (char *i = arrayname; *i; *i++)` 

e.g. of range-based loop structure

`for (int ranger : array1) ` 

`for (char texas : "I am a string!") 
- check out examples in for*.cpp


- `cout` class:

  - a different way other than `puts` or `printf` to deliver output into standard output stream

  - belongs to the `<iostream>` library 

  - tends to take up more memory when run and space when compiled (as compared to scripts using only `printf` and `puts`)

### Basic Commands for Compiliers

gcc's compiling command:

- when using new C++ features like range-based arrays, specify the C++ version/standard to the compiler

e.g. 

`g++ -std=c++11 arrays2.cpp -o array2` 





- non-global variable: temp. space in memory 

  - once a variable is defined, and if it has relationships with other variables: 

    - its dependent variables will have to be defined **after** 

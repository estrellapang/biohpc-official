// Hello World: modified from duplicate file
//
// This code does not yet work on workstations!
#include <stdio.h>
#include <mpi.h>


int main(int argc, char *argv[]) {
  
using namespace std;
 
  int chulo, numprocs, rank;

  char processor_name[MPI_MAX_PROCESSOR_NAME];

  chulo = MPI_Init(&argc, &argv);
  chulo = MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
  chulo = MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  printf("Hello from rank %d, out of %s processes!\n", rank, numprocs);

  chulo = MPI_Finalize();

}


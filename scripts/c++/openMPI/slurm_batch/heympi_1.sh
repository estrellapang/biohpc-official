#!/bin/bash

#SBATCH --job-name="first MPI"
#SBATCH --partition=normal
#SBATCH --nodes=3 
#SBATCH --ntasks=12
#SBATCH -o mpihey.out
#SBATCH -e mpihey.err

module load gnu   # these next three lines apply to the virtual cluster enviroment

module load openmpi

mpirun ~/test_scripts/greetmpi


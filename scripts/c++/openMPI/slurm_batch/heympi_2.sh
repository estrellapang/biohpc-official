#!/bin/bash

#SBATCH --job-name="2nd MPI"
#SBATCH --partition=normal
#SBATCH --nodes=3   # notice how this option is different: refer to `mpi_1.md`
#SBATCH --ntasks-per-node=4   
#SBATCH -o heympi.out
#SBATCH -e heympi.err

module load gnu

module load openmpi

mpirun ~/test_scripts/heympi


## Get In There & Set Things Up! 


### Get yourself the GCC Compiler (Development Tools Group Package) 

`[zxp8244@head ~]$ yum group list`

```
Loaded plugins: fastestmirror
There is no installed groups file.
Maybe run: yum groups mark convert (see man yum)
Loading mirror speeds from cached hostfile
 * base: centos.mirror.lstn.net
 * epel: fedora-epel.mirror.lstn.net
 * extras: centos.mirror.lstn.net
 * updates: centos.mirror.lstn.net
Available Environment Groups:
   Minimal Install
   Compute Node
   Infrastructure Server
   File and Print Server
   Cinnamon Desktop
   MATE Desktop
   Basic Web Server
   Virtualization Host
   Server with GUI
   GNOME Desktop
   KDE Plasma Workspaces
   Development and Creative Workstation
Available Groups:
   Cinnamon
   Compatibility Libraries
   Console Internet Tools
   Development Tools
   Educational Software
   Electronic Lab
   Fedora Packager
   General Purpose Desktop
   Graphical Administration Tools
   Haskell
   Legacy UNIX Compatibility
   MATE
   Milkymist
   Scientific Support
   Security Tools
   Smart Card Support
   System Administration Tools
   System Management
   TurboGears application framework
   Xfce
   ohpc-autotools
   ohpc-base
   ohpc-base-compute
   ohpc-ganglia
   ohpc-io-libs-gnu
   ohpc-io-libs-intel
   ohpc-nagios
   ohpc-parallel-libs-gnu
   ohpc-parallel-libs-gnu-mpich
   ohpc-parallel-libs-gnu-mvapich2
   ohpc-parallel-libs-gnu-openmpi
   ohpc-parallel-libs-intel-impi
   ohpc-parallel-libs-intel-mpich
   ohpc-parallel-libs-intel-mvapich2
   ohpc-parallel-libs-intel-openmpi
   ohpc-perf-tools-gnu
   ohpc-perf-tools-intel
   ohpc-python-libs-gnu
   ohpc-python-libs-intel
   ohpc-runtimes-gnu
   ohpc-runtimes-intel
   ohpc-serial-libs-gnu
   ohpc-serial-libs-intel
   ohpc-slurm-client
   ohpc-slurm-server
   ohpc-warewulf
Done
```

> sudo yum **groupinstall** "[Group Packages]"  # `sudo yum install` --> will not work!

e.g. 

``` 
zxp8244@head ~]$ sudo yum groupinstall "Development Tools"
Loaded plugins: fastestmirror
There is no installed groups file.
Maybe run: yum groups mark convert (see man yum)
Loading mirror speeds from cached hostfile
 * base: centos-distro.cavecreek.net
 * epel: fedora-epel.mirror.lstn.net
 * extras: mirror.cc.columbia.edu
 * updates: centos.mirror.lstn.net
Resolving Dependencies
--> Running transaction check
---> Package autoconf.noarch 0:2.69-11.el7 will be installed
--> Processing Dependency: m4 >= 1.4.14 for package: autoconf-2.69-11.el7.noarch
---> Package automake.noarch 0:1.13.4-3.el7 will be installed
--> Processing Dependency: perl(Thread::Queue) for package: automake-1.13.4-3.el7.noarch
---> Package bison.x86_64 0:3.0.4-2.el7 will be installed
---> Package byacc.x86_64 0:1.9.20130304-3.el7 will be installed
---> Package cscope.x86_64 0:15.8-10.el7 will be installed
---> Package ctags.x86_64 0:5.8-13.el7 will be installed
---> Package diffstat.x86_64 0:1.57-4.el7 will be installed
---> Package doxygen.x86_64 1:1.8.5-3.el7 will be installed
---> Package elfutils.x86_64 0:0.172-2.el7 will be installed
---> Package flex.x86_64 0:2.5.37-6.el7 will be installed
---> Package gcc.x86_64 0:4.8.5-36.el7_6.1 will be installed
--> Processing Dependency: libgomp = 4.8.5-36.el7_6.1 for package: gcc-4.8.5-36.el7_6.1.x86_64
--> Processing Dependency: cpp = 4.8.5-36.el7_6.1 for package: gcc-4.8.5-36.el7_6.1.x86_64
--> Processing Dependency: libgcc >= 4.8.5-36.el7_6.1 for package: gcc-4.8.5-36.el7_6.1.x86_64
--> Processing Dependency: libmpfr.so.4()(64bit) for package: gcc-4.8.5-36.el7_6.1.x86_64
--> Processing Dependency: libmpc.so.3()(64bit) for package: gcc-4.8.5-36.el7_6.1.x86_64
---> Package gcc-c++.x86_64 0:4.8.5-36.el7_6.1 will be installed
--> Processing Dependency: libstdc++-devel = 4.8.5-36.el7_6.1 for package: gcc-c++-4.8.5-36.el7_6.1.x86_64
--> Processing Dependency: libstdc++ = 4.8.5-36.el7_6.1 for package: gcc-c++-4.8.5-36.el7_6.1.x86_64
---> Package gcc-gfortran.x86_64 0:4.8.5-36.el7_6.1 will be installed
--> Processing Dependency: libquadmath-devel = 4.8.5-36.el7_6.1 for package: gcc-gfortran-4.8.5-36.el7_6.1.x86_64
--> Processing Dependency: libquadmath = 4.8.5-36.el7_6.1 for package: gcc-gfortran-4.8.5-36.el7_6.1.x86_64
--> Processing Dependency: libgfortran = 4.8.5-36.el7_6.1 for package: gcc-gfortran-4.8.5-36.el7_6.1.x86_64
--> Processing Dependency: libgfortran.so.3()(64bit) for package: gcc-gfortran-4.8.5-36.el7_6.1.x86_64
---> Package git.x86_64 0:1.8.3.1-20.el7 will be installed
--> Processing Dependency: perl-Git = 1.8.3.1-20.el7 for package: git-1.8.3.1-20.el7.x86_64
--> Processing Dependency: rsync for package: git-1.8.3.1-20.el7.x86_64
--> Processing Dependency: perl(Term::ReadKey) for package: git-1.8.3.1-20.el7.x86_64
--> Processing Dependency: perl(Git) for package: git-1.8.3.1-20.el7.x86_64
--> Processing Dependency: perl(Error) for package: git-1.8.3.1-20.el7.x86_64
---> Package indent.x86_64 0:2.2.11-13.el7 will be installed
---> Package intltool.noarch 0:0.50.2-7.el7 will be installed
--> Processing Dependency: gettext-devel for package: intltool-0.50.2-7.el7.noarch
---> Package libtool.x86_64 0:2.4.2-22.el7_3 will be installed
---> Package patch.x86_64 0:2.7.1-10.el7_5 will be installed
---> Package patchutils.x86_64 0:0.3.3-4.el7 will be installed
---> Package rcs.x86_64 0:5.9.0-5.el7 will be installed
---> Package redhat-rpm-config.noarch 0:9.1.0-87.el7.centos will be installed
--> Processing Dependency: dwz >= 0.4 for package: redhat-rpm-config-9.1.0-87.el7.centos.noarch
--> Processing Dependency: zip for package: redhat-rpm-config-9.1.0-87.el7.centos.noarch
--> Processing Dependency: perl-srpm-macros for package: redhat-rpm-config-9.1.0-87.el7.centos.noarch
---> Package rpm-build.x86_64 0:4.11.3-35.el7 will be installed
--> Processing Dependency: unzip for package: rpm-build-4.11.3-35.el7.x86_64
--> Processing Dependency: bzip2 for package: rpm-build-4.11.3-35.el7.x86_64
---> Package rpm-sign.x86_64 0:4.11.3-35.el7 will be installed
---> Package subversion.x86_64 0:1.7.14-14.el7 will be installed
--> Processing Dependency: subversion-libs(x86-64) = 1.7.14-14.el7 for package: subversion-1.7.14-14.el7.x86_64
--> Processing Dependency: libsvn_wc-1.so.0()(64bit) for package: subversion-1.7.14-14.el7.x86_64
--> Processing Dependency: libsvn_subr-1.so.0()(64bit) for package: subversion-1.7.14-14.el7.x86_64
--> Processing Dependency: libsvn_repos-1.so.0()(64bit) for package: subversion-1.7.14-14.el7.x86_64
--> Processing Dependency: libsvn_ra_svn-1.so.0()(64bit) for package: subversion-1.7.14-14.el7.x86_64
--> Processing Dependency: libsvn_ra_neon-1.so.0()(64bit) for package: subversion-1.7.14-14.el7.x86_64
--> Processing Dependency: libsvn_ra_local-1.so.0()(64bit) for package: subversion-1.7.14-14.el7.x86_64
--> Processing Dependency: libsvn_ra-1.so.0()(64bit) for package: subversion-1.7.14-14.el7.x86_64
--> Processing Dependency: libsvn_fs_util-1.so.0()(64bit) for package: subversion-1.7.14-14.el7.x86_64
--> Processing Dependency: libsvn_fs_fs-1.so.0()(64bit) for package: subversion-1.7.14-14.el7.x86_64
--> Processing Dependency: libsvn_fs_base-1.so.0()(64bit) for package: subversion-1.7.14-14.el7.x86_64
--> Processing Dependency: libsvn_fs-1.so.0()(64bit) for package: subversion-1.7.14-14.el7.x86_64
--> Processing Dependency: libsvn_diff-1.so.0()(64bit) for package: subversion-1.7.14-14.el7.x86_64
--> Processing Dependency: libsvn_delta-1.so.0()(64bit) for package: subversion-1.7.14-14.el7.x86_64
--> Processing Dependency: libsvn_client-1.so.0()(64bit) for package: subversion-1.7.14-14.el7.x86_64
--> Processing Dependency: libneon.so.27()(64bit) for package: subversion-1.7.14-14.el7.x86_64
---> Package swig.x86_64 0:2.0.10-5.el7 will be installed
---> Package systemtap.x86_64 0:3.3-3.el7 will be installed
--> Processing Dependency: systemtap-devel = 3.3-3.el7 for package: systemtap-3.3-3.el7.x86_64
--> Processing Dependency: systemtap-client = 3.3-3.el7 for package: systemtap-3.3-3.el7.x86_64
--> Running transaction check
---> Package bzip2.x86_64 0:1.0.6-13.el7 will be installed
---> Package cpp.x86_64 0:4.8.5-36.el7_6.1 will be installed
---> Package dwz.x86_64 0:0.11-3.el7 will be installed
---> Package gettext-devel.x86_64 0:0.19.8.1-2.el7 will be installed
--> Processing Dependency: gettext-common-devel = 0.19.8.1-2.el7 for package: gettext-devel-0.19.8.1-2.el7.x86_64
---> Package libgcc.x86_64 0:4.8.5-36.el7 will be updated
---> Package libgcc.x86_64 0:4.8.5-36.el7_6.1 will be an update
---> Package libgfortran.x86_64 0:4.8.5-36.el7_6.1 will be installed
---> Package libgomp.x86_64 0:4.8.5-36.el7 will be updated
---> Package libgomp.x86_64 0:4.8.5-36.el7_6.1 will be an update
---> Package libmpc.x86_64 0:1.0.1-3.el7 will be installed
---> Package libquadmath.x86_64 0:4.8.5-36.el7_6.1 will be installed
---> Package libquadmath-devel.x86_64 0:4.8.5-36.el7_6.1 will be installed
---> Package libstdc++.x86_64 0:4.8.5-36.el7 will be updated
---> Package libstdc++.x86_64 0:4.8.5-36.el7_6.1 will be an update
---> Package libstdc++-devel.x86_64 0:4.8.5-36.el7 will be updated
---> Package libstdc++-devel.x86_64 0:4.8.5-36.el7_6.1 will be an update
---> Package m4.x86_64 0:1.4.16-10.el7 will be installed
---> Package mpfr.x86_64 0:3.1.1-4.el7 will be installed
---> Package neon.x86_64 0:0.30.0-3.el7 will be installed
--> Processing Dependency: libproxy.so.1()(64bit) for package: neon-0.30.0-3.el7.x86_64
--> Processing Dependency: libpakchois.so.0()(64bit) for package: neon-0.30.0-3.el7.x86_64
---> Package perl-Error.noarch 1:0.17020-2.el7 will be installed
---> Package perl-Git.noarch 0:1.8.3.1-20.el7 will be installed
---> Package perl-TermReadKey.x86_64 0:2.30-20.el7 will be installed
---> Package perl-Thread-Queue.noarch 0:3.02-2.el7 will be installed
---> Package perl-srpm-macros.noarch 0:1-8.el7 will be installed
---> Package rsync.x86_64 0:3.1.2-4.el7 will be installed
---> Package subversion-libs.x86_64 0:1.7.14-14.el7 will be installed
---> Package systemtap-client.x86_64 0:3.3-3.el7 will be installed
--> Processing Dependency: systemtap-runtime = 3.3-3.el7 for package: systemtap-client-3.3-3.el7.x86_64
--> Processing Dependency: mokutil for package: systemtap-client-3.3-3.el7.x86_64
--> Processing Dependency: libavahi-common.so.3()(64bit) for package: systemtap-client-3.3-3.el7.x86_64
--> Processing Dependency: libavahi-client.so.3()(64bit) for package: systemtap-client-3.3-3.el7.x86_64
---> Package systemtap-devel.x86_64 0:3.3-3.el7 will be installed
--> Processing Dependency: kernel-devel-uname-r for package: systemtap-devel-3.3-3.el7.x86_64
---> Package unzip.x86_64 0:6.0-19.el7 will be installed
---> Package zip.x86_64 0:3.0-11.el7 will be installed
--> Running transaction check
---> Package avahi-libs.x86_64 0:0.6.31-19.el7 will be installed
---> Package gettext-common-devel.noarch 0:0.19.8.1-2.el7 will be installed
---> Package kernel-debug-devel.x86_64 0:3.10.0-957.10.1.el7 will be installed
---> Package libproxy.x86_64 0:0.4.11-11.el7 will be installed
--> Processing Dependency: libmodman.so.1()(64bit) for package: libproxy-0.4.11-11.el7.x86_64
---> Package mokutil.x86_64 0:15-2.el7.centos will be installed
--> Processing Dependency: libefivar.so.1(libefivar.so.0)(64bit) for package: mokutil-15-2.el7.centos.x86_64
--> Processing Dependency: libefivar.so.1(LIBEFIVAR_0.24)(64bit) for package: mokutil-15-2.el7.centos.x86_64
--> Processing Dependency: libefivar.so.1()(64bit) for package: mokutil-15-2.el7.centos.x86_64
---> Package pakchois.x86_64 0:0.4-10.el7 will be installed
---> Package systemtap-runtime.x86_64 0:3.3-3.el7 will be installed
--> Processing Dependency: libsymtabAPI.so.9.3()(64bit) for package: systemtap-runtime-3.3-3.el7.x86_64
--> Processing Dependency: libdyninstAPI.so.9.3()(64bit) for package: systemtap-runtime-3.3-3.el7.x86_64
--> Running transaction check
---> Package dyninst.x86_64 0:9.3.1-2.el7 will be installed
--> Processing Dependency: libdwarf.so.0()(64bit) for package: dyninst-9.3.1-2.el7.x86_64
--> Processing Dependency: libboost_thread-mt.so.1.53.0()(64bit) for package: dyninst-9.3.1-2.el7.x86_64
--> Processing Dependency: libboost_system-mt.so.1.53.0()(64bit) for package: dyninst-9.3.1-2.el7.x86_64
--> Processing Dependency: libboost_date_time-mt.so.1.53.0()(64bit) for package: dyninst-9.3.1-2.el7.x86_64
---> Package efivar-libs.x86_64 0:36-11.el7_6.1 will be installed
---> Package libmodman.x86_64 0:2.0.1-8.el7 will be installed
--> Running transaction check
---> Package boost-date-time.x86_64 0:1.53.0-27.el7 will be installed
---> Package boost-system.x86_64 0:1.53.0-27.el7 will be installed
---> Package boost-thread.x86_64 0:1.53.0-27.el7 will be installed
---> Package libdwarf.x86_64 0:20130207-4.el7 will be installed
--> Finished Dependency Resolution

Dependencies Resolved

============================================================================================================================================
 Package                                 Arch                      Version                                 Repository                  Size
============================================================================================================================================
Installing for group install "Development Tools":
 autoconf                                noarch                    2.69-11.el7                             base                       701 k
 automake                                noarch                    1.13.4-3.el7                            base                       679 k
 bison                                   x86_64                    3.0.4-2.el7                             base                       674 k
 byacc                                   x86_64                    1.9.20130304-3.el7                      base                        65 k
 cscope                                  x86_64                    15.8-10.el7                             base                       203 k
 ctags                                   x86_64                    5.8-13.el7                              base                       155 k
 diffstat                                x86_64                    1.57-4.el7                              base                        35 k
 doxygen                                 x86_64                    1:1.8.5-3.el7                           base                       3.6 M
 elfutils                                x86_64                    0.172-2.el7                             base                       299 k
 flex                                    x86_64                    2.5.37-6.el7                            base                       293 k
 gcc                                     x86_64                    4.8.5-36.el7_6.1                        updates                     16 M
 gcc-c++                                 x86_64                    4.8.5-36.el7_6.1                        updates                    7.2 M
 gcc-gfortran                            x86_64                    4.8.5-36.el7_6.1                        updates                    6.7 M
 git                                     x86_64                    1.8.3.1-20.el7                          updates                    4.4 M
 indent                                  x86_64                    2.2.11-13.el7                           base                       150 k
 intltool                                noarch                    0.50.2-7.el7                            base                        59 k
 libtool                                 x86_64                    2.4.2-22.el7_3                          base                       588 k
 patch                                   x86_64                    2.7.1-10.el7_5                          base                       110 k
 patchutils                              x86_64                    0.3.3-4.el7                             base                       104 k
 rcs                                     x86_64                    5.9.0-5.el7                             base                       230 k
 redhat-rpm-config                       noarch                    9.1.0-87.el7.centos                     base                        81 k
 rpm-build                               x86_64                    4.11.3-35.el7                           base                       147 k
 rpm-sign                                x86_64                    4.11.3-35.el7                           base                        48 k
 subversion                              x86_64                    1.7.14-14.el7                           base                       1.0 M
 swig                                    x86_64                    2.0.10-5.el7                            base                       1.3 M
 systemtap                               x86_64                    3.3-3.el7                               base                       148 k
Installing for dependencies:
 avahi-libs                              x86_64                    0.6.31-19.el7                           base                        61 k
 boost-date-time                         x86_64                    1.53.0-27.el7                           base                        52 k
 boost-system                            x86_64                    1.53.0-27.el7                           base                        40 k
 boost-thread                            x86_64                    1.53.0-27.el7                           base                        57 k
 bzip2                                   x86_64                    1.0.6-13.el7                            base                        52 k
 cpp                                     x86_64                    4.8.5-36.el7_6.1                        updates                    5.9 M
 dwz                                     x86_64                    0.11-3.el7                              base                        99 k
 dyninst                                 x86_64                    9.3.1-2.el7                             base                       3.5 M
 efivar-libs                             x86_64                    36-11.el7_6.1                           updates                     89 k
 gettext-common-devel                    noarch                    0.19.8.1-2.el7                          base                       410 k
 gettext-devel                           x86_64                    0.19.8.1-2.el7                          base                       320 k
 kernel-debug-devel                      x86_64                    3.10.0-957.10.1.el7                     updates                     17 M
 libdwarf                                x86_64                    20130207-4.el7                          base                       109 k
 libgfortran                             x86_64                    4.8.5-36.el7_6.1                        updates                    300 k
 libmodman                               x86_64                    2.0.1-8.el7                             base                        28 k
 libmpc                                  x86_64                    1.0.1-3.el7                             base                        51 k
 libproxy                                x86_64                    0.4.11-11.el7                           base                        64 k
 libquadmath                             x86_64                    4.8.5-36.el7_6.1                        updates                    189 k
 libquadmath-devel                       x86_64                    4.8.5-36.el7_6.1                        updates                     52 k
 m4                                      x86_64                    1.4.16-10.el7                           base                       256 k
 mokutil                                 x86_64                    15-2.el7.centos                         updates                     42 k
 mpfr                                    x86_64                    3.1.1-4.el7                             base                       203 k
 neon                                    x86_64                    0.30.0-3.el7                            base                       165 k
 pakchois                                x86_64                    0.4-10.el7                              base                        14 k
 perl-Error                              noarch                    1:0.17020-2.el7                         base                        32 k
 perl-Git                                noarch                    1.8.3.1-20.el7                          updates                     55 k
 perl-TermReadKey                        x86_64                    2.30-20.el7                             base                        31 k
 perl-Thread-Queue                       noarch                    3.02-2.el7                              base                        17 k
 perl-srpm-macros                        noarch                    1-8.el7                                 base                       4.6 k
 rsync                                   x86_64                    3.1.2-4.el7                             base                       403 k
 subversion-libs                         x86_64                    1.7.14-14.el7                           base                       922 k
 systemtap-client                        x86_64                    3.3-3.el7                               base                       3.5 M
 systemtap-devel                         x86_64                    3.3-3.el7                               base                       2.1 M
 systemtap-runtime                       x86_64                    3.3-3.el7                               base                       438 k
 unzip                                   x86_64                    6.0-19.el7                              base                       170 k
 zip                                     x86_64                    3.0-11.el7                              base                       260 k
Updating for dependencies:
 libgcc                                  x86_64                    4.8.5-36.el7_6.1                        updates                    102 k
 libgomp                                 x86_64                    4.8.5-36.el7_6.1                        updates                    157 k
 libstdc++                               x86_64                    4.8.5-36.el7_6.1                        updates                    305 k
 libstdc++-devel                         x86_64                    4.8.5-36.el7_6.1                        updates                    1.5 M

Transaction Summary
============================================================================================================================================
Install  26 Packages (+36 Dependent packages)
Upgrade              (  4 Dependent packages)

Total download size: 84 M
Is this ok [y/d/N]:y 

...
...
...

Installed:
  autoconf.noarch 0:2.69-11.el7                 automake.noarch 0:1.13.4-3.el7         bison.x86_64 0:3.0.4-2.el7                           
  byacc.x86_64 0:1.9.20130304-3.el7             cscope.x86_64 0:15.8-10.el7            ctags.x86_64 0:5.8-13.el7                            
  diffstat.x86_64 0:1.57-4.el7                  doxygen.x86_64 1:1.8.5-3.el7           elfutils.x86_64 0:0.172-2.el7                        
  flex.x86_64 0:2.5.37-6.el7                    gcc.x86_64 0:4.8.5-36.el7_6.1          gcc-c++.x86_64 0:4.8.5-36.el7_6.1                    
  gcc-gfortran.x86_64 0:4.8.5-36.el7_6.1        git.x86_64 0:1.8.3.1-20.el7            indent.x86_64 0:2.2.11-13.el7                        
  intltool.noarch 0:0.50.2-7.el7                libtool.x86_64 0:2.4.2-22.el7_3        patch.x86_64 0:2.7.1-10.el7_5                        
  patchutils.x86_64 0:0.3.3-4.el7               rcs.x86_64 0:5.9.0-5.el7               redhat-rpm-config.noarch 0:9.1.0-87.el7.centos       
  rpm-build.x86_64 0:4.11.3-35.el7              rpm-sign.x86_64 0:4.11.3-35.el7        subversion.x86_64 0:1.7.14-14.el7                    
  swig.x86_64 0:2.0.10-5.el7                    systemtap.x86_64 0:3.3-3.el7          

Dependency Installed:
  avahi-libs.x86_64 0:0.6.31-19.el7              boost-date-time.x86_64 0:1.53.0-27.el7   boost-system.x86_64 0:1.53.0-27.el7              
  boost-thread.x86_64 0:1.53.0-27.el7            bzip2.x86_64 0:1.0.6-13.el7              cpp.x86_64 0:4.8.5-36.el7_6.1                    
  dwz.x86_64 0:0.11-3.el7                        dyninst.x86_64 0:9.3.1-2.el7             efivar-libs.x86_64 0:36-11.el7_6.1               
  gettext-common-devel.noarch 0:0.19.8.1-2.el7   gettext-devel.x86_64 0:0.19.8.1-2.el7    kernel-debug-devel.x86_64 0:3.10.0-957.10.1.el7  
  libdwarf.x86_64 0:20130207-4.el7               libgfortran.x86_64 0:4.8.5-36.el7_6.1    libmodman.x86_64 0:2.0.1-8.el7                   
  libmpc.x86_64 0:1.0.1-3.el7                    libproxy.x86_64 0:0.4.11-11.el7          libquadmath.x86_64 0:4.8.5-36.el7_6.1            
  libquadmath-devel.x86_64 0:4.8.5-36.el7_6.1    m4.x86_64 0:1.4.16-10.el7                mokutil.x86_64 0:15-2.el7.centos                 
  mpfr.x86_64 0:3.1.1-4.el7                      neon.x86_64 0:0.30.0-3.el7               pakchois.x86_64 0:0.4-10.el7                     
  perl-Error.noarch 1:0.17020-2.el7              perl-Git.noarch 0:1.8.3.1-20.el7         perl-TermReadKey.x86_64 0:2.30-20.el7            
  perl-Thread-Queue.noarch 0:3.02-2.el7          perl-srpm-macros.noarch 0:1-8.el7        rsync.x86_64 0:3.1.2-4.el7                       
  subversion-libs.x86_64 0:1.7.14-14.el7         systemtap-client.x86_64 0:3.3-3.el7      systemtap-devel.x86_64 0:3.3-3.el7               
  systemtap-runtime.x86_64 0:3.3-3.el7           unzip.x86_64 0:6.0-19.el7                zip.x86_64 0:3.0-11.el7                          

Dependency Updated:
  libgcc.x86_64 0:4.8.5-36.el7_6.1                   libgomp.x86_64 0:4.8.5-36.el7_6.1          libstdc++.x86_64 0:4.8.5-36.el7_6.1         
  libstdc++-devel.x86_64 0:4.8.5-36.el7_6.1         

Complete!   # Look at the packages, one will notice that many useful packages are installed along with the "Development Tools" group
```

**VERIFY** 

```
which gcc 

which g++ 

whereis gcc

whereis g++
gcc --version 

# output from last line

gcc (GCC) 4.8.5 20150623 (Red Hat 4.8.5-36)
Copyright (C) 2015 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

```

### Get Yourself Some Colors! 

`which vim` --> if not available --> `sudo yum install vim` 

  - the newer, linux-based versions of VIM should have syntax coloring on by default---however, if this is not the case, then use input the command `:syn on` to turn off syntax coloring

> Honestly, who would NOT want some SMART LOOKING fonts while Coding??? 
>
> vim is standard on BioHPC cluster (vi is aliased as vim), but on most machines, 
> vi has fewer features than vim! 


``` 
[zxp8244@head test_scripts]$ sudo yum install vim
[sudo] password for zxp8244: 
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: centos-distro.cavecreek.net
 * epel: fedora-epel.mirror.lstn.net
 * extras: mirror.cc.columbia.edu
 * updates: centos.mirror.lstn.net
Resolving Dependencies
--> Running transaction check
---> Package vim-enhanced.x86_64 2:7.4.160-5.el7 will be installed
--> Processing Dependency: vim-common = 2:7.4.160-5.el7 for package: 2:vim-enhanced-7.4.160-5.el7.x86_64
--> Running transaction check
---> Package vim-common.x86_64 2:7.4.160-5.el7 will be installed
--> Processing Dependency: vim-filesystem for package: 2:vim-common-7.4.160-5.el7.x86_64
--> Running transaction check
---> Package vim-filesystem.x86_64 2:7.4.160-5.el7 will be installed
--> Finished Dependency Resolution

Dependencies Resolved

============================================================================================================================================
 Package                              Arch                         Version                                 Repository                  Size
============================================================================================================================================
Installing:
 vim-enhanced                         x86_64                       2:7.4.160-5.el7                         base                       1.0 M
Installing for dependencies:
 vim-common                           x86_64                       2:7.4.160-5.el7                         base                       5.9 M
 vim-filesystem                       x86_64                       2:7.4.160-5.el7                         base                        10 k

Transaction Summary
============================================================================================================================================
Install  1 Package (+2 Dependent packages)

Total download size: 7.0 M
Installed size: 23 M
Is this ok [y/d/N]: y
Downloading packages:
(1/3): vim-enhanced-7.4.160-5.el7.x86_64.rpm                                                                         | 1.0 MB  00:00:00     
(2/3): vim-filesystem-7.4.160-5.el7.x86_64.rpm                                                                       |  10 kB  00:00:00     
(3/3): vim-common-7.4.160-5.el7.x86_64.rpm                                                                           | 5.9 MB  00:00:08     
--------------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                       817 kB/s | 7.0 MB  00:00:08     
Running transaction check
Running transaction test
Transaction test succeeded
Running transaction
  Installing : 2:vim-filesystem-7.4.160-5.el7.x86_64                                                                                    1/3 
  Installing : 2:vim-common-7.4.160-5.el7.x86_64                                                                                        2/3 
  Installing : 2:vim-enhanced-7.4.160-5.el7.x86_64                                                                                      3/3 
  Verifying  : 2:vim-enhanced-7.4.160-5.el7.x86_64                                                                                      1/3 
  Verifying  : 2:vim-common-7.4.160-5.el7.x86_64                                                                                        2/3 
  Verifying  : 2:vim-filesystem-7.4.160-5.el7.x86_64                                                                                    3/3 

Installed:
  vim-enhanced.x86_64 2:7.4.160-5.el7                                                                                                       

Dependency Installed:
  vim-common.x86_64 2:7.4.160-5.el7                                  vim-filesystem.x86_64 2:7.4.160-5.el7                                 

Complete!

```




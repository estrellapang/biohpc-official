# 9th bash_script: practicing with "time"  & "printf" 
#!/bin/bash
printf "\n"
#inserts a new line

echo 'simply inputting "date" returns unformatted time:' 
date 

echo 'using the " - " specifier in the "date" command:'
date +"%d-%m-%Y"

echo 'using "date" to return hours, minutes, and seconds:' 
date +"%H:%M:%S"

# The lines below are "printf" commands that allow specific formatting: 
printf "\n"

printf "Name:\t%s\nID:\t%06d\n" "Zing" "149453" 

# "\t" indicates an indent; "\n" returns a new line "%s" specifies the character# following "Name:" to be a string, and "%06" speicies a numerical input with 
# 6 zeros   

printf "\n" 
now=$(date +"%d-%m-%Y") 
verynow=$(date +"%H:%M:%S") 
printf "User:\t%s\nDate:\t%s\nTime:\t%s\n" $USER $now $verynow

#using printf to output date and time all at once 

# This script illustrates the if operations and the use of Regular Expression Operator "=~" 

Test_String="1.have you even seen the rain?" 

if [[ $Test_String =~ [A-Z]+ ]];

 then

	echo "$Test_String Yes I've seen capital!"
else

	echo "$Test_String No I have not seen any capital rain!"  

else [[ $Test_String =~ [0-9]+ ]];

 then

	echo "$Test_String yes I've seen one! And its numbers were immense!" 

else 
	echo "$Test_String I haven't see a thing.." 

fi 

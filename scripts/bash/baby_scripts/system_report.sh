#This script outputs a series of information on the local system: 
#!/bin/bash 

GrnTxt=$(tput setaf 2)
BlkTxt=$(tput setaf 0)
BOLD=$(tput bold)  
default=$(tput sgr0) 
LogDate=$(date +"%m%d%y") 
DiskUse=$(df -h|grep "root"|awk '{print$4}')  
  

echo -e $BlkTxt"the system info for "$BOLD$HOSTNAME$default" is listed below\n"$GrnTxt > "$LogDate"_report.log 

  
printf "System type:\t%s\n" $MACHTYPE >> "$LogDate"_report.log
printf "Bash version:\t%s\n" $BASH_VERSION >> "$LogDate"_report.log
printf "Free Space in root:\t%s"$DiskUse >> "$LogDate"_report.log
printf "\n" >> "$LogDate"_report.log 

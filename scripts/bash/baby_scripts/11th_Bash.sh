# This is a bash script showing examples of how ">" ">>" and "echo" in reading and writing text files:
#!/bin/bash
echo 'Woah!' > 1st_text.txt 

# the above command redirects the output "Woah!" and writes it into the file 1st_text.txt 
# NOTE: if there were contents in the file, this would overwrite everything in it 
cat 1st_text.txt

> 1st_text.txt 

# ALL contents are now erased from the file 

# Append new text to the file. ">>" append by starting on a new line 
echo 'Start anew' > 1st_text.txt
echo 'An end brings a beginning' >> 1st_text.txt 

cat 1st_text.txt 

# Using a text file as the INPUT for a WHILE LOOP! 
# Use the redirection operation "<" 

printf "\n" 

j=1
while read substrate; do 
	echo "Line $j: $substrate" 
	((j++))
done < 1st_text.txt  

# Here the while loop read into a variable called substrate, and in effect the contents of the 1st_text.txt
# are listed out line by line??? 

# Example of "HERE" DOCUMENT: 

printf "\n" 

cat <<FinishIt 
This is a input
Which is considered
A String 
FinishIt 

cat <<-LockIt

	Yo I got dis
	Studyin' stuff
	on 100 %
LockIt  

# The additional dash by the end of "-" lets it ignore any indents
# somehow this is not working as a script; but normally in the commandline, the << assigns a term that 
# you would want to terminate the here document.   

#!/bin/bash

LIMIT=60

while read line; do

PERCENT=$(echo $line | awk '{print $2}')
USAGE=${PERCENT%?}
DIRECTORY=$(echo $line | awk '{print $1}')

if [ $USAGE -gt $LIMIT ]; then

        echo "The space available in $DIRECTORY is low. Current usage: $PERCENT" | mail -s "CAUTION: Storage Space is Low in the "$DIRECTORY" directory" zengxing.pang@utsouthwestern.edu # Note on the pipe here: the echoed content from the first pipe will actually become the content of the second mail pipe, where its first argument is the title

fi #fi terminates the if loop

done < <(df -Th --total | awk 'NR==2 {print}' | awk '{print $7 " " $6}') 

du -sh ~/Desktop/Practice | awk '{print $1}' | while read output; do

VARUSE=$output

echo "The amount of space occupying ~/Desktop/Practice is $VARUSE"

done
 
du -sh ~/Desktop/Notes | awk '{print $1}' | while read output; do

TMPUSE=$output

echo "The amount of space occupying ~Desktop/Notes is $TMPUSE"

done

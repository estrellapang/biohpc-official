# second simple script: illustrate effect of single/double/no quotation marks around inputs for echo command 

#!/bin/bash/

greeting="hello" 
#here a "greeting" is defined as a variable 

# echo $greeting  world (planet)! 
# "$[variable] --> read/interpret  variable; without quotations, echo will try to  interpret everything, so this# line would stop the whole script due error 

echo $greeting world \(planet\)! 
# using "\" to escape special characters such as "()" and "!" 
 
echo '$greeting world (planet)!' 
# single quotation marks means to print everything LITERALLY, no interpretation

echo "$greeting world (planet)!" 
# double quotation allows interpretation of listed variables only 

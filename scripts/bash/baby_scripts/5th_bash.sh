# This is the 5th demo bash script, with arithmetic operations 
#!/bin/bash/

num=3 
((num++)) 
echo $num 

echo $((num+3)) 
echo $num 
#NOTE we need to place "$" in "echo []((num+3))" b.c. echo needs to call on the variable to retrieve its value
 
echo $((num+6)) 

((num--))
echo $num  

((num+6))
echo $num 
#NOTE that echo $(()) nor (()) changes the value of a variable, only ++ -- !!! 

((num+=4))
echo $num 
#NOTE "+=" OR "-=" changes the value of the variable 

((num/=7)) 
echo $num 
#NOTE the "/=" or "*=" also implement the changes 

# REMEMBER the (()) does NOT OUTPUT anything, and echo $(()) would require a varialbe to be called upon to work 
# DON'T DO Pipelines WITHIN the (()), it ONLY ACCEPTS ARITHMETIC OPERATORS!
# Pipeline into bc to output decimals
# echo itself has ability to conduct arithmetic without the use of (()), see below: 

decimal=$(echo 1/4 | bc -l) 
echo $decimal

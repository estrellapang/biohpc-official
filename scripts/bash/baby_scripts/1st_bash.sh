#!/bin/bash
# this is a simple bash script listing the # of files in the current directory 
files=$(ls -1 | wc -l) 
echo "the number of files in this folder is $files"

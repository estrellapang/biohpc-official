#10th bash script: topic is the "declare" variable & assigning indexed arrays:

#make a variable that's an indexed array: 

Fruits=("orange" "banana" "apples")  
printf "\n"
# Call the first index value in Fruits:
echo ${Fruits[1]} 

# Add a new value to the index "5," if there is no 5th index, the addition will be appended to the 4th item 

Fruits[4]="watermelon" 

printf "\n" 
echo ${Fruits[@]}

# Another way to sequentially add an item (sequentially) in an array: 

Fruits+=("pears") 
echo ${Fruits[@]}
# The "@" lists all of the items in the array

# Return the last two items in the array 
echo ${Fruits[@]: -2}  

#
#
# using the "declare" command to make variables with more options: "-A" allows values to be assigned not 
# to numerical indexes, but to associative terms as indexes: 

declare -A FunArray 

FunArray[nature]=Educational 
FunArray[location]=TheHeart 

echo ${FunArray[nature]} starts from the ${FunArray[location]} 
# NOTE: if any index's name or value is MORE THAN ONE TERM, we need to use "" quotation marks around
# the input: see below 
 
FunArray["A Question"]="but is it true in the modern world?" 
echo ${FunArray[nature]} starts from the ${FunArray[location]}, ${FunArray[A Question]} 

echo ${FunArray[@]}

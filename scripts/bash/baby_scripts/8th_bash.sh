# This is Zengxing's 8th script, with focus on styling and coloring script text: 

# SUBSTITUTE style & color commands into simpler VARIABLES: 

Bking_Grn_Blk=$(tput blink; tput setaf 2; tput setab 0) 
Default=$(tput sgr0) 


# User the "escape" option/flag of echo to assign styling: 
# IF We DON'T let echo call the Default variable command to restore default, then the entire command line will 
# be changed!  (" ls " will actually restore everything to default)  

echo -e $Bking_Grn_Blk"An Old-Fashioned, BASIC-like line" $Default 


# This is the 6th practice script from Zengxing, the topic will be testing brackets, logical & comparison ops
#!/bin/bash 

# " [[ ]] " denotes comparing or test strings

echo 'Does "Hello == "hello?"' 
[[ "Hello" == "hello" ]] 
echo $? 
# " $? " contains the status of the previous test run, "0" for success (yes), "1" for failure (no) 

echo 'Does "Hello = "hello?"' 
[[ "Hello" = "hello" ]] 
echo $? 

echo 'Is 3 > 1?' 
[[ 3 > 1 ]] 
echo $? 

#Logical ops and null values: 

ego="" 
#ego is defined as being empty 

honor="infinite" 
echo 'is ego null AND honor not null?' 
[[ -z $ego && -n $honor ]] 
echo $?

covet="crave"

[[ $ego != $honor || ! $covet ]] 
echo 'is ego NOT EQUAL honor OR NOT covet?' 
echo $? 


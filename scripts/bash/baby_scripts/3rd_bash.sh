# Another demo bash script, with variable attributes and built-in variables
#!/bin/bash

Lily="Beautiful Pooh" 
Age=25
State=Vibrant 

echo $Lily
echo $Age
echo $State 

echo 'your system name:'
echo $HOME
echo 'your OS system info:'
echo $MACHTYPE 
echo 'amount of time this session:'
echo $SECONDS 

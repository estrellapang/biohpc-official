# This is the Zengxing's 7th bash script: the subject will be concatenation,bracket expansion in "echo," etc. 
#!/bin/bash

one="How"
two="dy!"
three="$one$two" 
#the variable three is the concatenation of the variables "one" and "two" 

echo $three  

# " echo ${#variable} " gives the length of any variable or string? 

echo ${#three} 

# " ${variable:3} " outputs partial strings,with its content specified by ":"
echo ${three:2}  

echo ${three:2:2} 
# the third colon specifies how many characters after the 2nd character will be included, here two letters

echo ${three: -6} 
# negative numbers allows counting backwards 

# STRING MODIFIERS: 

river="fish fish turtle bird raccoon" 

echo ${river/fish/algae} 
#replace the first instance of "fish" in var. river with "algae" 

echo ${river//fish/algae}
#replace all instances ~ 

echo ${river/#turtle/algae} 
# the "/# " specifies that the term term " turtle " can only be replaced if it's at the beginning of string

echo ${river/#fish/algae} 

echo ${river/%raccoon/algae} 
# "/%" replaces only if target at end of string 

echo ${river/t*/algae} 
# use of wild card "*"----1st term starting with "f" will be replaced with "algae"--SCRIPT ENDS after! 

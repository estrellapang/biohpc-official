#!/bin/bash
#
# Ask User for group name/gid

echo "Greetings! Please enter lab group name(case case sensitive)/gid number!"

read GID

echo "/archive usage for $GID :"
sudo /usr/lpp/mmfs/bin/mmlsquota -g $GID --block-size=auto | sed -n '4p;6p' | awk '//{print $4, $5, $6;}'

# link on sed selecting specific lines from StdOUT
# https://stackoverflow.com/questions/12682810/select-multiple-lines-using-the-linux-command-sed

# parse group members into space separated array:
peopleList=( $(getent group $GID | sed 's/[^,*]*://g' | sed 's/*//g' | sed 's/,/ /g') )

# output number of members within inputted group:
echo "the group $GID has ${#peopleList[@]} members!"

# fetch each user's work usage:
for i in "${peopleList[@]}"

do

# output uid & the parsed LDAP name search onto the same line (`echo -n`)
echo -n "/work usage for $i, or "; sudo ldapsearch -x uid=$i | grep -i "displayname" | awk '//{print $2, $3;}'

# GPFS command sorted to output only storage usage
sudo /usr/lpp/mmfs/bin/mmlsquota -u $i --block-size=auto | sed -n '2,3p;' | awk '//{print $4, $5, $6;}'

done

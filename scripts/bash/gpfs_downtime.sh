## GPFS Downtime Script 20200604

> check if old GPFS is mounted; umount; remove old symlinks; generating directory and symlinks for new GPFS filessytem

### Actual Script

- `/home/tools/bin/.`

```
#!/bin/bash

dev=`ibv_devinfo | head -1 | awk '{print $2}'`
name=`/bin/hostname`

#if ls -al / | grep -q endosome; then 
if [ -d /endosome/work/biohpcadmin ]; then 
   echo "$name: /endosome already mounted, Please check this node, EXITING"
   exit 1
fi


# Before downtime, it is better to check the script can define all nodes as active or not
if /usr/lpp/mmfs/bin/mmgetstate |grep "$name" | grep -q "active"; then 
   echo "$name: GPFS daemon is Active, Cleaning"
   /usr/lpp/mmfs/bin/mmshutdown
   sleep 12
   if /usr/lpp/mmfs/bin/mmgetstate |grep "$name" | grep -q "active"; then
      echo "$name: GPFS daemon is STILL Active, Exiting the script"
      exit 1
   fi
fi

if [ -f /work/.workgpfs ]; then 
   echo "$name: CUH GPFS is still mounted, EXITING"
   exit 1
fi 


if [ -L /archive ]; then
   echo "$name: unlinking old /archive"
   unlink /archive 
fi

if [ -L /work ]; then
   echo "$name: unlinking old /work"
   unlink /work
fi


if [ -d /work ]; then
   echo "$name: lazy umounting /work "
   umount -l /work
   echo "$name: /work directory empty still exists, moving to /tmp/."
   mv /work /tmp/
fi
   

if ls -la / | grep -q work; then 
   echo "$name: /work cannot be moved, EXITING"
   exit 1
fi

if ls -la / | grep -q archive; then 
   echo "$name: /archive cannot be unlinked, EXITING"
   exit 1
fi

if [ ! -d /endosome ]; then
   mkdir -p /endosome
fi

echo "$name ALL CHECKS PASSED, generating symlinks"
ln -s /endosome/work /work
ln -s /endosome/archive /archive

```

### Reference Material

- [bash check presence of various file types](https://stackoverflow.com/questions/638975/how-do-i-tell-if-a-regular-file-does-not-exist-in-bash#638980)

- [exit status of process/command](https://www.cyberciti.biz/faq/shell-how-to-determine-the-exit-status-of-linux-and-unix-command/)

- [if,else if, else](https://linuxhint.com/bash_if_else_examples/)

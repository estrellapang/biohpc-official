#!/bin/bash

# STORE user first names, emails as separate arrays
userName=( $(cat astrocyte_top_usage.raw | awk -F. '{print $1}' | awk '{print $2}') )
userMail=( $(cat astrocyte_top_usage.raw | awk '{print $2}') )

# STORE Astrocyte usage into different array vars
userTotal=( $(cat astrocyte_top_usage.raw | awk '{print $3}') )
userRuns=( $(cat astrocyte_top_usage.raw | awk '{print $4}') )
userIncome=( $(cat astrocyte_top_usage.raw | awk '{print $5}') )
userOut=( $(cat astrocyte_top_usage.raw | awk '{print $6}') )

# loop through an email template, and with each iteration, substitute in values from variables above into the template --> send out email to corresponding user and BioHPC-Help

for ((i=0; i<${#userName[@]}; ++i)); do cat astrocyte_data_email.template | sed "s|FIRST|${userName[$i]}|g; s|TOTAL|${userTotal[$i]}|g; s|RUNS|${userRuns[i]}|g; s|INCOMING|${userIncome[i]}|g; s|OUTGOING|${userOut[i]}|g" > email_out.txt; mail -s 'Request to Offload Astrocyte Data' -r biohpc-help@utsouthwestern.edu ${userMail[i]},biohpc-help@utsouthwestern.edu < email_out.txt; done

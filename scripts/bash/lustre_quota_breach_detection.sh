#!/bin/bash
# 06-02-2021 Zengxing Pang

set -u
#set -e --> this terminates the script prematurely when `lfs quota` hits a user who is no longer in database

# Generate raw quota report with usage by all GIDs

echo "Lustre Alert on $(date +%F)"

sudo ldapsearch -x cn=* | grep -i gidNumber | sort -V | sort -u | awk -F: '{ print $2 }' | xargs -n1 -I% sh -c 'echo "GID=%";sudo lfs quota -g % -h /project' > /project/biohpcadmin/shared/scripts/lustre_quota_current.txt &&

# point to raw report file
rawQuota='/project/biohpcadmin/shared/scripts/lustre_quota_current.txt'

# IF there ARE NOT any quota breaches, exit without error
if [ ! "$(grep '*' $rawQuota)" ]; then

  echo 'There are no quota breaches at this time :)'
  exit 0

fi

# filter out groups having exceeded quota limit into array
breachGroups=( $(grep '*' $rawQuota | awk '{print $2}' | awk -F* '{print $1}' | xargs -n1 -I% grep -B3 % $rawQuota | grep GID | awk -F '=' '{print $2}' | xargs getent group | awk -F: '{print $1}') )

echo''

# output quota for each group in question
echo '#####################################################'
echo 'The following groups have breached their quota limit'
echo '#####################################################'

for i in ${breachGroups[@]}; do echo "=======$i======="; sudo lfs quota -g $i -h /project; done | grep -v 'Disk' | awk '{print $1,$2,$3,$6}' | sed 's|Filesystem||g;s|/project||g' | sed 's/ //1' | sed 's/used/| used |/g; s/quota/limit |/g; s/files/file_count |/g'

echo ''
echo ''

# outout quota info for each user in the groups
echo '#####################################################'
echo 'Per-user usage in each group'
echo '#####################################################'

for i in ${breachGroups[@]}; do echo "=======$i======="; getent group $i | cut -f 1,2,3 -d ':' --complement | tr ',' '\n' | xargs -n1 -I% sh -c 'getent passwd % | cut -f 5 -d : | cut -f 1 -d , && lfs quota -hu % /project 2> /dev/null'; done |  grep -v Disk | awk '{print $1,$2,$6}' | sed 's|Filesystem||g;s|/project||g' | sed 's/ //1' | sed 's/used/| storage |/g; s/files/file_count |/g'

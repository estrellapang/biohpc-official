# PURPOSE: find all files belonging to root under a large directory
#!/bin/bash

ls /endosome/archive/bioinformatics/Danuser_lab/melanoma/raw/ | xargs -n1 -I% -P5 find /endosome/archive/bioinformatics/Danuser_lab/melanoma/raw/% -user root &

ls /endosome/archive/bioinformatics/Danuser_lab/melanoma/analysis/ | xargs -n1 -I% -P3 find /endosome/archive/bioinformatics/Danuser_lab/melanoma/analysis/% -user root &

#!/bin/bash
# 06-21-2021 Zengxing Pang

set -u
#set -e --> this terminates the script prematurely when `lfs quota` hits a user who is no longer in database

# point to raw report file, generated every Monday and Thursday of each week
rawQuota='/project/biohpcadmin/shared/scripts/lustre_quota_current.txt'

# parse out top 21 groups' GIDs, store as array
topGroups=( $(cat $rawQuota | awk '{ print $2 }' | tr -s '\n' | grep T | sort -rV | head -n 21 | xargs -n1 -I% grep -B3 % $rawQuota | grep GID | awk -F '=' '{print $2}' | xargs getent group | awk -F: '{print $3}') )

# print out final result

echo '===================================='
echo 'Lustre Filesystem Usage: Top 21 Labs'
echo '===================================='
echo '-----------------------------------'

for i in ${topGroups[@]}; do getent group $i | awk -F: '{print $3, $1}'; grep -wA3 "$i" $rawQuota | grep -wA2 'used' | grep -v GID | awk '{print $1,$2,$3,$4}' | sed 's\ \ | \'g; echo '-----------------------------------'; done

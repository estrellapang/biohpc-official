#!/bin/bash
#
# Ask User for group name/gid
echo "Greetings! Please enter lab group/gid number!"

read GID

# output group member & parse into nice array

# declare -a PPLLIST=( $(getent group $GID | sed 's/[^,*]*://g' | sed 's/*//g' | sed 's/,/ /g') )

# `declare as an array is not needed`

# IF WE DO NOT USE PARENTHESES OUTSIDE OF `$()`, THE VARIALBE/ARRAY WILL ONLY CONTAIN **ONE** ELEMENT!!!

PPLLIST=( $(getent group $GID | sed 's/[^,*]*://g' | sed 's/*//g' | sed 's/,/ /g') )

# determine array length
echo "the group $GID has ${#PPLLIST[@]} members!"

# for loop to fetch each user's display name and return their project quota 
# future idea --> also return their work & home2 quota

for i in "${PPLLIST[@]}"

do

echo "$i corresponds to the user:"

ldapsearch -x uid=$i | grep -i "displayname" | awk '//{print $2, $3;}' 

echo "whose /project usage is:"

lfs quota -u $i -h /project | awk '//{print $2;}' | awk 'NR==3 {print $0}'

done

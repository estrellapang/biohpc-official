# DESCRIPTION:
# ----------------------------------------------
# 1. use of `xargs` to parallelize --> the sub-directories
# listed in the source and destination dirs need to EXIST!
#
# 2. `&&` to run lines one at a time, to run simultaneously
# replace with `&`
#
# 3. email notify upon completion (ONLY works with &&)
#
# 4. `2>&1` directs both errors and output to console or log
# 
# 5. example of escaping special characters in source and destination
# ----------------------------------------------

#!/bin/bash

echo "$HOSTNAME check..."

ls /project/bioinformatics/Danuser_lab/melanoma/analysis/Vasanth/ | xargs -n1 -P7 -I% rsync -Aavh /project/bioinformatics/Danuser_lab/melanoma/analysis/Vasanth/% /endosome/archive/bioinformatics/Danuser_lab/melanoma/analysis/Vasanth/% 2>&1 &&

rsync -Aavh /project/bioinformatics/Danuser_lab/melanoma/raw/randomNonMelanoma/ /endosome/archive/bioinformatics/Danuser_lab/melanoma/raw/randomNonMelanoma/ 2>&1 &&

rsync -Aavh /project/bioinformatics/Danuser_lab/melanoma/raw/RandomImages/ /endosome/archive/bioinformatics/Danuser_lab/melanoma/raw/RandomImages/ 2>&1 &&

rsync -Aavh /project/bioinformatics/Danuser_lab/melanoma/raw/SpinningDisk/ /endosome/archive/bioinformatics/Danuser_lab/melanoma/raw/SpinningDisk/ 2>&1 &&

rsync -Aavhs /project/bioinformatics/Danuser_lab/melanoma/raw/hiRes3D\(cheap\ knockoff\)/ /endosome/archive/bioinformatics/Danuser_lab/melanoma/raw/hiRes3D\(cheap\ knockoff\)/ 2>&1 &&

rsync -Aavh /project/bioinformatics/Danuser_lab/melanoma/raw/3DCollagenSpinningDisk/ /endosome/archive/bioinformatics/Danuser_lab/melanoma/raw/3DCollagenSpinningDisk/ 2>&1 &&

rsync -Aavh /project/bioinformatics/Danuser_lab/melanoma/raw/3DCollagenLowResWidefield/ /endosome/archive/bioinformatics/Danuser_lab/melanoma/raw/3DCollagenLowResWidefield/ 2>&1 &&

rsync -Aavh /project/bioinformatics/Danuser_lab/melanoma/raw/3DViability/ /endosome/archive/bioinformatics/Danuser_lab/melanoma/raw/3DViability/ 2>&1 &&

rsync -Aavh /project/bioinformatics/Danuser_lab/melanoma/raw/FieldSynethesis/ /endosome/archive/bioinformatics/Danuser_lab/melanoma/raw/FieldSynethesis/ 2>&1 &&

echo "NucleusA070 Rsync Check Completed" | mail -s "Job Completion: NucleusA070" zengxing.pang@utsouthwestern.edu

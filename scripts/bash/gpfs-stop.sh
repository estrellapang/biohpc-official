#!/bin/bash 
# PURPOSE - launched with xargs ignore errors and ssh + timeout option to unmount a large number of GPFS clients simultaneously

# check if gpfs active --> force unmount then shutdown daemon 
 
if sudo /usr/lpp/mmfs/bin/mmgetstate | grep -qi "active"; then 

   echo "$HOSTNAME GPFS active, unmounting" 
   timeout 90 /usr/lpp/mmfs/bin/mmshutdown  
   #sleep 30
fi 

# check unmount status --> if still active, then lazy mount  

if [ ! -d /endosome/work/biohpcadmin ]; then 
   echo "$HOSTNAME GPFS shutdown successful" 
   exit 0 
 
fi 

if  mount | grep -qi bassclient || /usr/lpp/mmfs/bin/mmgetstate | grep -qi active; then 
   echo "$HOSTNAME GPFS still active, lazy unmounting..."
   timeout 17 umount -l /endosome 
   #timeout 9 umount /endosome 
   timeout 60 /usr/lpp/mmfs/bin/mmshutdown

   if mount | grep -qi bassclient || /usr/lpp/mmfs/bin/mmgetstate | grep -qi 'active\|unknown\|arbitrating'; then 
      echo "$HOSTNAME GPFS hanging" 
      echo "$HOSTNAME" >> /home/tools/logs/problem_nodes02.txt 
      exit 0 

   fi 

fi

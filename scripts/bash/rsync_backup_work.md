#!/bin/sh

# Changes
# =======
#
# 2017-03-20 DCT
# Improve safety, use set -u -e and quote paths. Avoids any issue where
# we could end up with a space in a directory name.


# Safety - no unset vars, stop on first error
set -u
set -e

PARALLEL="/cm/shared/apps/parallel/20150122/bin/parallel"
# DIRS_CMD="ls /work | grep -v 'archive' | grep -v '.backup'"

# 04242019 WG
# add excludes directories and **user directories** containing 2.5M files.
myexcludes=$PWD/myexcludes.txt

DIRS_CMD="find /work -mindepth 2 -maxdepth 2 -type d | egrep -v -f $myexcludes | sed 's#/work/##'"

CONCURRENCY=8
RSYNC_COMMAND="rsync -avx -A --delete"
DATESTAMP=$(date +%Y_%m_%d_%H%M)
LOG_DIR="/project/backup_work/.logs"
JOB_LOG="$LOG_DIR/${DATESTAMP}.log"

eval $DIRS_CMD | $PARALLEL -t --results $LOG_DIR -j $CONCURRENCY --joblog $JOB_LOG $RSYNC_COMMAND /work/{}/ /project/backup_work/{}/

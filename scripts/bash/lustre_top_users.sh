#!/bin/bash
# 07-14-2021 Zengxing Pang
# PURPOSE: run this script to output TOP 45 Lustre USERS 

set -u
#set -e --> this terminates the script prematurely when `lfs quota` hits a user who is no longer in database

# point to raw report file, generated every Monday and Thursday of each week
rawQuota='/project/biohpcadmin/shared/scripts/lustre_user_report_current.txt'


# parse out top numbers, arrange high to low
rawStats=( $(cat $rawQuota | awk '{ print $2 }' | tr -s '\n' | grep T | sort -rV | head -n 45) )

# from $rawStats, search and match usage with user name from raw quota file, store as array
userCredents=( $(for i in ${rawStats[@]}; do grep -B2 $i $rawQuota | grep 'usr' | xargs getent passwd | awk -F: '{print $1}' ; done) )

  # NOTE: $userCredents is populated by sequentially looping through $rawStats---the user list is also arranged high to low in terms of storage occupation

# print out FINAL RESULT

echo '===================================='
echo 'Lustre Filesystem Usage: Top 45 Users'
echo '===================================='

# loop through all elements in $rawStats, and for each element, echo out its corresponding user
for ((i=0; i<${#rawStats[@]}; i++)); do echo '----------------------------' ; getent passwd ${userCredents[i]} | awk -F: '{print $5}' | awk -F, '{print $1}' ; echo -n "(${userCredents[i]}) - " ; echo ${rawStats[i]}; done

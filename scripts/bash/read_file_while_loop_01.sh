# DESCRIPTION:
# read text file with absolute paths to files, and perform chgrp on files one by one

#!/bin/bash

while read -r line; do chgrp -hv Danuser_lab $line; done < live_cell_files_Danuser_lab.raw

# `-r` option for read cmd prevents ignoring of special characters such as '\'

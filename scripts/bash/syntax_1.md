## Bash Scripting Syntax and Know-hows

> `.sh` extension is standard for script files, but it's not essential 

> make things executable by assigning +x permission (only when ready) 

\
\

### Logical Operators

- [awesome bash operator guide - opensource.com](https://opensource.com/article/19/10/programming-bash-logical-operators-shell-expansions) 

- [stackoverflow 1](https://stackoverflow.com/questions/18668556/how-to-compare-numbers-in-bash)

- [askubuntu 1](https://askubuntu.com/questions/1042659/how-to-check-if-a-value-is-greater-than-or-equal-to-another)

#### file operators


#### string operators


#### arithmetic operators


\
\

### Basic Syntax

- **General Syntax**

- line break: `\`

  - e.g.

```
sudo yum install vim nmap-ncat tree \
telnet net-tools \
sysstat

```

\

- command break: `;`

  - e.g.

```
hostname; cat /etc/redhat-release; uname -a

```

\

- WHY start each bash script with `#!/bin/bash`? 

  - points to where the interpreter is for your script, thus `/bin/bash` is the path of our bash interpreter! 

  - `#!/bin/sh` among numerous other shell interpreters are also options! Python scripts could be initialized the same way 

\

#### Process & Job Control

- run two commands SIMULTANEOUSLY

  - `cmd & cmd`

  - in scripts

```
# run series of commands simultaneously

1st series of commands &
2nd series of commands &
.
.
.
```

- WAIT for 1st command to finish SUCCESSFULLY, then START second command

  - `cmd && cmd`

- WAIT to run 2nd command, even if 1st command FAILS

  - `cmd ; cmd`

- [additional examples using wait and while conditionals](https://unix.stackexchange.com/questions/487955/how-to-plan-a-task-to-run-after-another-already-running-task-in-bash)

- [run commands simultaneously](https://stackoverflow.com/questions/3004811/how-do-you-run-multiple-programs-in-parallel-from-a-bash-script)

\

#### Output Redirection!

- `<cmd> > /dev/null` --> ONLY print stdErr to console/terminal

- `<cmd> > output_file &>`  --> redirects BOTH stdout and stderr

  - independent of choice of shells: `<cmd> > output_file 2>&1` 

  - [good reference for correct redirection syntax](https://www.cyberciti.biz/faq/redirecting-stderr-to-stdout/)

- `<cmd> > /dev/null 2>&1` --> redirect BOTH stdOut and stdErr to the "pseudo-devices special file" called `/dev/null` which special function is to accept and discard all input (and therefore produces no output)
  
- `2> /dev/null` --> omit/do not output errors

- `1> /dev/null` --> omit stdout

- `2>&1 >/dev/null` --> omit stdout, ONLY pipe stderror as input into next command

  - [reference here](https://stackoverflow.com/questions/2342826/how-can-i-pipe-stderr-and-not-stdout)

  - e.g. count invalid users using `id` cmd, which outputs invalid users as an error out

```
ls <parent_dir> | head -n 500 | xargs -n1 -I% id % 2>&1 >/dev/null | wc -l

```

- QUESTIONS:

  - how to direct stderr and stdout to separate files

  - how to direct only stderr to output file

  - how to direct stdout of more than one command

- [stack overflow output redirection](https://stackoverflow.com/questions/637827/redirect-stderr-and-stdout-in-bash)

- [stackoverflow DETAILED explanation](https://stackoverflow.com/questions/10508843/what-is-dev-null-21)

- [stackoverflow error redirect](https://stackoverflow.com/questions/32379855/how-to-suppress-error-message-of-a-command)

\

#### Exit Codes

- `echo $?` to get the exit code of last command

- **scripting approaches**

  - we may use exit codes as arguments for if-statements

  - we may set out own exit codes while scripting

  - [setting exit codes](https://www.cyberciti.biz/faq/bash-get-exit-code-of-command/)



\
\

### Running Bash Scripts

- **ways to execute**

  1. enter full path of script file

  2. `bash <path to script>` 

  3. cd into directory, and ` ./<script> ` 

- **Debug Mode** 

  - add `-x` to `#!/bin/bash` --> `#!/bin/bash -x` 

  - this prints out the commands being executed in the script before printing output to the screen! (helpful because we can avoid much echoing and manual explaining--not ot mention debugging for clarity)  

  - ON TERMINAL, `bash -x <script/command>` will output exactly what the script is doing ~

- **standard location of scripts** (though, they could be anywhere)

  - `/home/username/bin` --> per-user access

  - `/usr/local/bin` --> avail to all users

- **Dafuq is $PATH?**

  - an "shell/environment variable" --> info stored in memory for shell to quickly access

  - view `echo $PATH` --> default location of programs & scripts

  - [add directory to $PATH](https://linuxize.com/post/how-to-add-directory-to-path-in-linux/) 

\

  - **adding directories to $PATH**

```

# OPTION ONE: Most Distributions are automatically scripted to add user specific bins to $PATH upon login

> generally configured in `.bash_profile` 

# e.g.
---------------------------------------------------
PATH=$PATH:$HOME/.local/bin:$HOME/bin
---------------------------------------------------

  ## simply write scripts under `/home/<user>/.local/bin` && ASSIGN `+x` to the script (or else it will NOT appear when tabbing to auto-complete)


# OPTION TWO: add directory of your desire to PATH 
---------------------------------------------------
export PATH="<directory>:$PATH"
---------------------------------------------------

  ## make this a permanent setup by adding to `.bashrc` (per user) -OR- to `/etc/profile` so shell looks into the directory for ALL users


# OPTION NOT $PATH related:

# add script under `/usr/local/bin`  --> this will make script executable by all users 

# create scrpit in any location, and add an alias under `.bashrc` to execute; no need to add to $PATH, once again

```

\

- **start-up/log in scripts**

  - [difference between /etc/profile and .bashrc](https://askubuntu.com/questions/247738/why-is-etc-profile-not-invoked-for-non-login-shells)

  - [other login startup script options](https://unix.stackexchange.com/questions/266780/how-to-make-a-simple-script-run-at-login-everytime)

\
\

### Special Calls

#### Triggering EOF

- [good explanation on stackexchange](https://askubuntu.com/questions/724990/what-is-eof-and-how-to-trigger-it)

\
\

### Data, Variable, Array Operations/Interactions

#### Save single command into variable

- [reference 1](https://unix.stackexchange.com/questions/261664/how-to-read-a-file-into-a-shell-script-as-a-variable)

\

#### Save command pipes into a variable

- `var=$(cmd | cmd | cmd)`

- **HOW TO PRESERVE OUTPUT FORMAT WHILE SAVING IT TO VARIABLE**

  - it is saved by default--to return origina formatting simply use `echo` w/h QUOTATION marks

  - e.g. `echo "$var"` --> original format  V.S. `echo $var` --> everything in a single string withoutdelimiters and only space characters (not good for further parsing).

- [ref 1](https://stackoverflow.com/questions/41728184/how-to-pipe-wc-l-output-to-echo-output)

- [ref 2](https://unix.stackexchange.com/questions/147185/preserve-formatting-when-command-output-is-sent-to-a-variable)

\

- **HOW TO FEED VARIABLES as STDIN to COMMANDS i.e. grep, sort, awk, etc.**

```
# "Here String" -or- "Here Document"
-------------------------------------
grep '<search_term>' <<<"$<variable>" 
-------------------------------------
```

- [AWESOME grep reference](https://www.putorius.net/grep-string-from-a-variable.html)

\

#### Array Elements with Spaces

- [how to enter elements with space characters](https://unix.stackexchange.com/questions/181507/bash-script-array-elements-containing-space-character)

\

#### Read Text File in Array

- [how to read file context as an array](https://peniwize.wordpress.com/2011/04/09/how-to-read-all-lines-of-a-file-into-a-bash-array/)

- [read lines into array 2](https://unix.stackexchange.com/questions/485221/read-lines-into-array-one-element-per-line-using-bash)

- [read lines into array 3](https://stackoverflow.com/questions/19864658/how-to-write-an-array-ignoring-space-characters-in-shell-scripting)

- [read lines into array 4](https://stackoverflow.com/questions/30988586/creating-an-array-from-a-text-file-in-bash)

- [read text files line by line 1](https://ccm.net/faq/1757-how-to-read-a-linux-file-line-by-line)

- 

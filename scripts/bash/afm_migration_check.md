### Corresponding Cron Job

```
MAILTO="paniz.karbasi@utsouthwestern.edu,zengxing.pang@utsouthwestern.edu"

0 */5 * * *  /root/check_afm_status.sh; /bin/find /root -maxdepth 1 -mmin 1 -type f -name "afm_check_*" -exec cat {} \;
```

### Script

```
#!/bin/bash

ext=`date +%F-%H-%M`
touch afm_check_$ext.txt

/usr/lpp/mmfs/bin/mmafmctl bassdata getstate > afm_check_$ext.txt
echo "==========================================================================" >> afm_check_$ext.txt
/usr/lpp/mmfs/bin/mmdsh -N afm_bass "ps -ef | grep tspcache" >> afm_check_$ext.txt
echo "==========================================================================" >> afm_check_$ext.txt
/usr/bin/df -H | grep -i endosome >> afm_check_$ext.txt
/usr/bin/df -H | grep -i work >> afm_check_$ext.txt

```

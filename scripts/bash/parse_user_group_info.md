## Pipe Snippets for Extracting User Info

#### Store all user in GROUP as an array

`DanUserLab=( $(getent group Danuser_lab | cut -f 4- -d ':' | sed 's|,| |g') )`


#### Parse out only ACTIVE users from array ABOVE

`for i in ${DanUserLab[@]}; do id $i; echo ''; done 2> /dev/null | awk '{print $1}' | awk -F'(' '{print$2}' | sed 's/)//g' | tr -s '\n'`


#### Query LDAP database and save their emails into an array

  # first declare pipe output from above as array var

`for i in ${ActiveDanuser[@]}; do sudo ldapsearch -x uid=$i | grep mail | awk -F: '{print $2}' | sed 's/ //g'; done`


#### Forward Out Group Emails to List Above

for i in ${BICFemails[@]}; do mail -s 'Request to Offload /project Data' -r biohpc-help@utsouthwestern.edu $i,biohpc-help@utsouthwestern.edu < data_offload_request.txt ; done

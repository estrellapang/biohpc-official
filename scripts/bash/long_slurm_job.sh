#!/bin/bash

# AUTHOR: Zengxing Pang, Murat Atis
# PURPOSE: List out SLURM jobs that have been running longer than 15 days, and that are using significant CPU cycles (>100%)

# query ldap database and output long running jobs

#!/bin/bash
# 07-12-2021 Zengxing Pang
# PURPOSE: script to quickly output /project usage for all UIDs in a single .txt file
# NOTE: this script DOES NOT include service users like `astrocyte`, `apache`, `galaxy`, `portal`, `thunder_ftp`, etc.
# NOTE: could take 1-2 minutes to run -- querying lustre metadata for 1200+ users' quota stats

# Requirements:
# - LDAP connection
# - /project needs to be mounted
# - run from a node where lustre root squash is disabled (Nuc001,Nuc003,Nuc006,Lamella nodes,etc.)
# - run by user with sudo rights/root

set -u
set -e

echo "saving user usage report the following directory --> $PWD/."

getent passwd | grep 'project\|home2' | awk -F: '{print $1}' | xargs -n1 -I% sudo lfs quota -uh % /project > lustre_user_report_$(date +%F).txt &&

echo "done"

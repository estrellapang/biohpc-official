## Quick & Dirty Tips for Bashscripting on Command Line Prompt! 

### FOR LOOPS!!!

> also called definite loops

- **resources**

  - [good link on arrays](https://www.tutorialkart.com/bash-shell-scripting/bash-array/)

  - [seq command syntax:sequential reads](https://www.cyberciti.biz/tips/how-to-generating-print-range-sequence-of-numbers.html)

  - [$@ variable](https://stackoverflow.com/questions/12711786/convert-command-line-arguments-into-an-array-in-bash)

  - [seq command examples](https://www.geeksforgeeks.org/seq-command-in-linux-with-examples/)

  - [remote SSH commands](https://www.linuxtechi.com/execute-linux-commands-remote-system-over-ssh/)
 
\

#### Define Arrays

- `<arrayName> = ( n1, n2, ... n_N)`

  - [if array elements have spaces](https://stackoverflow.com/questions/9084257/bash-array-with-spaces-in-elements)

- set output of command as array

  - `<arrayName> = ( $(command) )`

\


#### Through SEQUENTIAL Arrays

```
 e.g. 1 --> use SSH to output servers' hostnames 

  ## the same template can be used to **mount network storage, change passwords, get node availability,**etc. 

### E.g. Sequential: ssh into nodes & execute commands (`hostname`)
-------------------------------------------------------------------------------
 for in in $(seq -w 046 057);do ssh ###.###.###.$i hostname&; done
-------------------------------------------------------------------------------

### E.g. Sequential: drain nodes
-------------------------------------------------------------------------------
for i in $(seq -w 043 052); do scontrol update nodeName=NucleusC$i state=drain reason="test";done
-------------------------------------------------------------------------------

  ## `-w` in `seq` command is called the "padding with zeroes" option, which lets numbers be lead by `0's`---NOTE: user still has to SPECIFY the 0's in the sequence of numbers!!!
```

\

#### Loop Through MULTIPLE arrays

  - [tutorial 1](https://stackoverflow.com/questions/17403498/iterate-over-two-arrays-simultaneously-in-bash) 

```
### Loop Through NON-SEQUENTIAL Arrays

  # define the arrays

workBakGrpsDir=(BICF CAND CRI GCRB HCTOR InternalMedicine MCHGD MRL Neuroinformatics_Core PCDC SBL SCCC TDC TIBIR biohpcadmin bioinformatics biophysics cellbiology gdanuser greencenter immunology kjaqaman pathology psychiatry radiology urology)

workBakGrps=(BICF CAND CRI GCRB HCTOR InternalMedicine MCHGD MRL Neuroinformatics_Core PCDC SBL SCCC TDC TIBIR biohpc_admin Bioinformatics Biophysics CellBiology Danuser_lab GCSB Immunology kjaqaman Pathology Psychiatry Radiology Urology)

  # loop "i" through both arrays, where "#" will detect all vars starting with the SAME PREFIX

     # the "#<variableName_PREFIX>' works best if the common variable prefix consists of +1 word e.g. "workBak" rather than just "work"

for ((i=0; i<${#workBakGrps[@]}; ++i)); do mkdir /project1/backup_work/${workBakGrpsDir[i]}; chown root:${workBakGrps[i]} /project1/backup_work/${workBakGrpsDir[i]}; chmod 755 /project1/backup_work/${workBakGrpsDir[i]}; done
```

- e.g. create email notifications for new users and notify their PIs

- email template:

```
Dear Dr.PI,

This is Zeng from BioHPC---we are emailing to confirm whether FIRSTNAME is indeed working within your lab and is approved to use BioHPC resources.

FIRSTNAME, we will activate your account as soon as Dr.PI approves of this.

Thank you for working with us!


Respectfully,
```

- create arrays of PI names Users

```
newPIs=( Cobanoglu Woodruff Yan Huang Yan Ly Reese Henning )
newUsers=(Nicholas Pragya Jianjun Wei-Hsiang Kun Rashmi Amritha Jiaen)
```

- verify that the arrays have been properly created; echo them

```
for ((i=0; i<${#newPIs[@]}; ++i)); do echo ${newPIs[i]}; echo ${newUsers[i]}; done

Cobanoglu
Nicholas
Woodruff
Pragya
Yan
Jianjun
Huang
Wei-Hsiang
Yan
Kun
Ly
Rashmi
Reese
Amritha
Henning
Jiaen
```

- Finally,substitue the placeholders in template with names in the array:

```
for ((i=0; i<${#newPIs[@]}; ++i)); do cat confirmation_email_template.txt | sed "s/PI/${newPIs[i]}/g;s/FIRSTNAME/${newUsers[i]}/g" >> confirmation_emails.txt; echo "" >> confirmation_emails.txt; done
```

\

- Put all of the lab members in a group, and parse group associations

```
# Fill Array 

XiaoPPL=$( getent group Xiao_lab | cut -f 4- -d ':' | sed 's|,| |g' )

# Search and Match

for i in ${XiaoPPL[@]}; do id $i | grep -i bioinformatics; done 2> /dev/null | grep -w uid | awk '{print $1}'
uid=178588(s178588)
uid=421955(s421955)
uid=171162(s171162)
uid=413935(swan15)
uid=177373(s177373)
uid=85324(gxiao)
uid=118730(qzhou)
uid=124777(twang6)

```
\
\
\

### Through NON-SEQUENTIAL Arrays

```
### BASICS
-------------------------------------------------------------------------------
# DECLARE ARRAY Variable

testArray=(32 37 35)

  ## declarations are only viable for the session!

# See array length

echo ${#testArray[@]}
3


# See array elements

for i in "${testArray[@]}"; do echo $i; done
32
37
35


# e.g. see weekly backup services:
  
bkupServices=(graphite answer xymon lamella docs ticket)

  # these are for Monday Servcices

bkupServices=(portal galaxy cloud git astrocyte)

  # these are Sunday Services 

for i in "${bkupServices[@]}"; do echo "weekly backup for $i";sudo ls -l /work/.backup/backup_services/$i;done

-------------------------------------------------------------------------------

/
/
/

### Combine with admin commands:

------------------------------------------------------------------
 for i in "${testArray[@]}"; do ping -c 3 198.215.56.$i; done
-----------------------------------------------------------------

\
\
\

## Do Declarations & Looping Commands Together

------------------------------------------------------------------
## from example above

testArray=(32 37 35);for i in "${testArray[@]}";do ping -c 3 198.215.56.$i;done

  ## ping specifically 3 packets
------------------------------------------------------------------


\
\
\

## String Array;loop to execute MULTIPLE remote SSH comands for EACH node listed in array

---------------------------------------------------------------------
lustreClutz=(mds00 mds01 oss00 oss01 oss02 oss03)
for i in "${lustreClutz[@]}"; do ssh $i "hostname;iptables -L | grep -w '198.215.51.3'"; done
---------------------------------------------------------------------


```

\
\

### Loops Used for Specialized Operations:

```
### IB CARD FIRMWARE UPDATE:

newGPUS=(002 003 004 005 006 007 008 009 010 011 012 013 014 015 016 017 030 031 032 033 034 035 036 037 038 039 047 250)

# count

echo ${#newGPUS[@]}
28

# double verify listing:

for i in ${newGPUS[@]}; do echo "this is NucleusC$i" ; done

# check firmware and card PID for each node

for i in ${newGPUS[@]}; do echo "HCA FW info for NucleusC$i";sudo ssh NucleusC$i "ibv_devinfo | grep -i 'board_id\|fw_ver' ";done

# get an unzipped copy of firmware bin file, then copy to /tmp directory of all nodes:

 for i in ${newGPUS[@]}; do echo "copying to NucleusC$i"; scp /tmp/fw-ConnectX5-rel-16_26_4012-872725-B21_Ax-UEFI-14.19.17-FlexBoot-3.5.805.signed.bin NucleusC$i:/tmp; done

  # verify:

 for i in ${newGPUS[@]}; do ssh NucleusC$i "ls -l /tmp | grep -i fw" ; done

# check IB card bus address of al nodes:

for i in ${newGPUs[@]}; do sudo ssh NucleusC$i "lspci | grep -i Mell"; done

  # sample output `af:00.0 Infiniband controller: Mellanox Technologies MT27800 Family [ConnectX-5]`  --> we want "af:00.0" as address!

# do a test round before applying to all nodes:

testGPUs=(002 003 004)

 for i in ${testGPUs[@]}; do echo "Updating HCA FW for NucleusC$i"; sudo ssh NucleusC$i "mstflint -d af:00.0 -i /tmp/fw-ConnectX5-rel-16_26_4012-872725-B21_Ax-UEFI-14.19.17-FlexBoot-3.5.805.signed.bin burn"; done

------------------------------------------------------------
e.g. of successful output:

Updating HCA FW for NucleusC002

    Current FW version on flash:  16.21.2808
    New FW version:               16.26.4012

Initializing image partition -   OK          
Writing Boot image component -   OK          
-I- To load new FW run mlxfwreset or reboot machine.
Updating HCA FW for NucleusC003

    Current FW version on flash:  16.21.2808
    New FW version:               16.26.4012

Initializing image partition -   OK          
Writing Boot image component -   OK          
-I- To load new FW run mlxfwreset or reboot machine.
Updating HCA FW for NucleusC004

    Current FW version on flash:  16.21.2808
    New FW version:               16.26.4012

Initializing image partition -   OK          
Writing Boot image component -   OK          
-I- To load new FW run mlxfwreset or reboot machine.

------------------------------------------------------------

# if test works; apply to rest

```

\
\

### If Statements

#### check if PID is alive based on exit code

- `if [ "$(ps -p <PID>)" ]; then echo "yes"; else echo "no"; fi`

  - useful for setting up crons to periodically check PID

  - the `[ "$()" ]` is for checking the EXIT code, True if code ran successfully, False if returns error or "1" 

\
\

#### use command output as if condition argument

- e.g. perform action if file owner if 'root'

  - `if [ $(ls -l <some_directory> | awk '{print $3}' == root ]; then <action_cmd> ; fi`

\
\

### WHILE LOOPS!!!

> also called "indefinite loops"

\

##### Read files line by line

- [here is how](https://www.howtogeek.com/709838/how-to-process-a-file-line-by-line-in-a-linux-bash-script/)

- e.g. loop through file names in a file and change their permission

```
while read line; do chgrp -hv Danuser_lab $line; done < live_cell_files_Danuser_lab.raw
```

\

#### Check if files/directories are there

- [good example](https://stackoverflow.com/questions/638975/how-do-i-tell-if-a-regular-file-does-not-exist-in-bash#638980)

\

#### Find SUM of all elements in array

- [link_here](https://stackoverflow.com/questions/13635293/how-can-i-find-the-sum-of-the-elements-of-an-array-in-bash)

\
\

### Techniques with User Input

- [interest script](https://unix.stackexchange.com/questions/378101/shell-scripting-modify-files-with-user-input)

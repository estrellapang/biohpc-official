## A Bash Script for Monitoring Disk Usage 

- REMINDER: give your script execute permissions: `sudo chmod +x [your file name].sh` 
- REMINDER: install the mail utility by `sudo yum install mailx` 

``` 
#!/bin/bash

LIMIT=60

while read line; do

PERCENT=$(echo $line | awk '{print $2}') 
USAGE=${PERCENT%?} 
DIRECTORY=$(echo $line | awk '{print $1}') 

if [ $USAGE -gt $LIMIT ]; then 

	echo "The space available in $DIRECTORY is low. Current usage: $PERCENT" | mail -s "CAUTION: Storage Space Dangerously Low in $DIRECTORY" [someadmin]@[somedomain] # mail -s --> "send"
 
fi 

done < <(df -Th --total | awk 'NR==2 {print}' | awk '{print $7 " " $6}')

# here, there is a SPACE between the two "<" signs 

# print usage for specific directories:

du -sh /var/log | awk '{print $1}' | while read output; do 

VARUSE=$output 

echo "The amount of space occupying /var/log is $VARUSE" 

done 

du -sh /tmp | awk '{print $1}' | while read output; do 

TMPUSE=$output 

echo "The amount of space occupying /tmp is $TMPUSE" 

done  

```


*** Notes on `grep` 

`-v` --> invert match option: whatever that is matched will be excluded 

*** Notes on `du` 

`-s` --> outputs a single, summarized sum of the file/directory size 

`-c` --> still lets the command read out the size of every subdirectory, but will add a "total" entry at the end


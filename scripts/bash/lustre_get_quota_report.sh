#!/bin/bash
# 07-12-2021 Zengxing Pang
# PURPOSE: script to quickly query /project usage for all GIDs under BioHPC

set -u

echo "Saving report the following directory --> $PWD/."

sudo ldapsearch -x cn=* | grep -i gidNumber | sort -V | sort -u | awk -F: '{ print $2 }' | xargs -n1 -I% sh -c 'echo "GID=%";sudo lfs quota -g % -h /project' > lustre_quota_report_$(date +%F).txt && 

echo "done"

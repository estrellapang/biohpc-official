#!/bin/bash


file_ext="_files.txt"
rand_files="rand_"
rand_files_bass="bass_rand_"
md5_bass="md5_bass_"
md5_cuh="md5_cuh_"

ext=`date +%F-%H-%M`
touch random_file_check_$ext.txt

random_date=$(shuf -n1 -i$(date -d '2017-01-01' '+%s')-$(date -d '2020-04-20' '+%s') | xargs -I{} date -d '@{}' '+%m/%d/%Y')

#echo "Finding files generated on and after $random_date ..."

for d in /Work/* ; do
    if [ "$d" != "/Work/archive" ]
    then
    	echo "Generating files for $d" 
        filename="$(echo "$d" | cut -c7-)" 
        #echo $filename$file_ext     
        timeout 3s find -O3 -L $d -type f -newermt $random_date > $filename$file_ext
        #num_files=$(wc -l $filename$file_ext | awk '{print $1}')
        #echo "Scanning $num_files files ..."
        cat $filename$file_ext | shuf -n 2000 > $rand_files$filename$file_ext
        cp $rand_files$filename$file_ext $rand_files_bass$filename$file_ext
        sed -i 's/Work/endosome\/work/' $rand_files_bass$filename$file_ext
        
        #compare md5sums
        cat $rand_files$filename$file_ext | xargs md5sum | awk '{print $1}' > $md5_cuh$filename$file_ext
        cat $rand_files_bass$filename$file_ext | xargs md5sum | awk '{print $1}' > $md5_bass$filename$file_ext
        echo "Comparing md5sums ..."
        output_diff=$(diff -u $md5_cuh$filename$file_ext $md5_bass$filename$file_ext)
        if [[ -z "$output_diff" ]]
    	then
        	# output is empty 
        	echo "No diff" 
    	else
        	# output is non-empty 
        	echo "Possible mismatch $md5_cuh$filename$file_ext and $md5_bass$filename$file_ext" >> random_file_check_$ext.txt
    	fi

    fi
done

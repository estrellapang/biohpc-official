## Bash Pipes, Lego Pieces for Future Automation

### Clean Old User Data 

#### Astrocyte

- **list old incoming/outgoing**

```
# 03-23-2021

cd /project/apps/astrocyte/astrocyte_incoming/

ls | xargs -n1 -I% id % 2>&1 >/dev/null | awk -F: '{print $2}' | tr -d "[:blank:]" | sudo xargs -n1 -I% du -sh /project/apps/astrocyte/astrocyte_incoming/% | grep 'T\|G'
4.0K    /project/apps/astrocyte/astrocyte_incoming/WXING
30G     /project/apps/astrocyte/astrocyte_incoming/bchen4
26G     /project/apps/astrocyte/astrocyte_incoming/cheple
6.0G    /project/apps/astrocyte/astrocyte_incoming/cpak1
166G    /project/apps/astrocyte/astrocyte_incoming/dngo1
44G     /project/apps/astrocyte/astrocyte_incoming/niqbal
83G     /project/apps/astrocyte/astrocyte_incoming/s167891
21G     /project/apps/astrocyte/astrocyte_incoming/s170636
5.1G    /project/apps/astrocyte/astrocyte_incoming/s175603
21G     /project/apps/astrocyte/astrocyte_incoming/s178337
11G     /project/apps/astrocyte/astrocyte_incoming/s178791
78G     /project/apps/astrocyte/astrocyte_incoming/s185797
101G    /project/apps/astrocyte/astrocyte_incoming/s418665
18G     /project/apps/astrocyte/astrocyte_incoming/xluo4
6.6G    /project/apps/astrocyte/astrocyte_incoming/xsun2
4.5G    /project/apps/astrocyte/astrocyte_incoming/zhuang

cd /project/apps/astrocyte/astrocyte_outgoing/

ls | xargs -n1 -I% id % 2>&1 >/dev/null | awk -F: '{print $2}' | tr -d "[:blank:]" | sudo xargs -n1 -I% du -sh /project/apps/astrocyte/astrocyte_outgoing/%
| grep 'T\|G'

4.0K    /project/apps/astrocyte/astrocyte_outgoing/WXING
49G     /project/apps/astrocyte/astrocyte_outgoing/bchen4
490G    /project/apps/astrocyte/astrocyte_outgoing/dtrudgian
579G    /project/apps/astrocyte/astrocyte_outgoing/mkim8
47G     /project/apps/astrocyte/astrocyte_outgoing/niqbal
191G    /project/apps/astrocyte/astrocyte_outgoing/s175603
27G     /project/apps/astrocyte/astrocyte_outgoing/s178791
2.2T    /project/apps/astrocyte/astrocyte_outgoing/s185797
2.0T    /project/apps/astrocyte/astrocyte_outgoing/xluo4
```

\

- **CONCISE METHOD**

```
# output all invalid users directories, and measure folder size; outputs to file

ls /project/apps/astrocyte/astrocyte_<incoming/outgoing> | xargs -n1 -I% id % 2>&1 >/dev/null | awk -F: '{print $2}' | tr -d "[:blank:]" | sudo xargs -n1 -I% du -sh /project/apps/astrocyte/astrocyte_<incoming/outgoing>/% > LIST.txt 

# parse out file for largest subdir sizes

cat LIST.txt | sort -V | grep 'M\|G\|T'
```

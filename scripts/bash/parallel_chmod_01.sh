#!/bin/bash

ls /endosome/archive/bioinformatics/Danuser_lab/melanoma/raw | xargs -n1 -I% -P5 chmod -Rv 750 /endosome/archive/bioinformatics/Danuser_lab/melanoma/raw/% 2>&1 &

ls /endosome/archive/bioinformatics/Danuser_lab/melanoma/analysis | xargs -n1 -I% -P5 chmod -Rv 750 /endosome/archive/bioinformatics/Danuser_lab/melanoma/analysis/% 2>&1 &

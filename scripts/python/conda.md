## Conda Tips and Tricks

### Install Mini-conda

```
mkdir miniconda3

wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda3/miniconda.sh

bash miniconda3/miniconda.sh -b -u -p miniconda3/

./miniconda3/bin/conda init bash

./miniconda3/bin/conda config --set auto_activate_base false

conda update conda

```

- [tutorial 1](https://waylonwalker.com/install-miniconda)

- [tutorial 2](https://educe-ubc.github.io/conda.html)

\
\

### Create Environments 

```
# by path

conda create -p <full path>

# by name --> NOTE: this places your conda env in your default directory!

conda create -n <environment_name> 

# load your environments

conda <activate/deactivate> <envir_name>

```

\
\

### Packages Installation

#### General Knowhows

```
# Search for packages, list all versions and builds
conda search <package>

  # specify unique channels: `conda search -c <conda-forge/biobuilds/bioconda> <package>`

# Default install
conda install <package> <package> .. <package>

# specify version
conda install package=<version>

# specify a range of versions
conda install package'>=LOW_VERSION, <=HIGH_VERSION'

# specify version and build
conda install <package>=<version>=<build>

# specify channels to install packages from
conda install -c <channel> <package>

  # specify multiple channels `-c <channel_1> -c <channel_2> -c <channel_3>`

  # e.g. to install RMATs gene analysis tool, we needed to do the following
-------------------------------------------------------------------------------
conda install -c defaults -c conda-forge -c bioconda rmats=4.1.1=py27h9c845e1_1
-------------------------------------------------------------------------------

  # the channel `biobuilds` is another good repo of bioinformatics tools

```

\

#### Delete or Revert Group Installs

```
# remove a package in an active environment

conda remove <package>


# remove a package, specify environment and package name

conda remove -n <env> <package>


# to REVERT Group Installs, much like `yum undo <install_history_ID>`

  # list revision history

conda list --revisions

  # roll back to previous install

conda install --revision <#>
```

#### references

- [conda use multiple channels](https://stackoverflow.com/questions/58658964/how-can-an-anaconda-package-require-two-channels-to-install)

- [conda specify package build](https://stackoverflow.com/questions/48128029/installing-specific-build-of-an-anaconda-package)

- [conda specify lower and higher package versions](https://stackoverflow.com/questions/38411942/anaconda-conda-install-a-specific-package-version)


\
\

#### Prevent Auto-Activation of Conda Environment

```
If you'd prefer that conda's base environment not be activated on startup,
   set the auto_activate_base parameter to false:

conda config --set auto_activate_base false
```

\

#### Remove an environment

```
# Remove Conda Environments by NAME

conda env remove -n ENV_NAME


# Remove Conda Environments by PATH/Location

conda env remove -p <directory>

```

\
\

### OpenCV 

- first create empty envir, then activate and install openCV

- [special instructions here](https://sushant4191.medium.com/installing-opencv-on-windows-through-anaconda-how-to-fix-solving-environment-error-message-6760a1b07ba5)


\
\


### Use `mamba` package manager

> nice, fast alternative to conda

```
# first create a conda env installed with mamba (python 3.9 required as of July 2021)
conda create -n mamba python=3.9

  # -OR-, create the env in a specific path:
  conda create -p <path> python=3.9

conda activate mamba
conda install -c conda-forge mamba

# activate env installed with mamba, and use its CLI utility to efficiently create other envs

  # e.g. use mamba to install `snakemake`

mamba create -n snakemake -c conda-forge -c bioconda snakemake
```

- [snakemake-mamba installation guide](https://snakemake.readthedocs.io/en/stable/getting_started/installation.html)

\
\

### Debug

#### 2-26-2021: Debug with Xinxin Wang

- User cannot load his python kernels in jupyter/jupyter lab

  - issue: cannot create conda environments, no conda environments seen in .conda

- Created a test conda for him

```
# created empty environment

"mytestenv"

conda activate mytestenv

conda install python=3.6

conda install ipykernel

ipython kernel install --user --name mytestenv --display-name "py3.6"

### what fixed issues with ALL kernels
pip3 uninstall tornado
pip3 install tornado==5.1.1

# user manually updated conda, and brough tornado to vers=6.0, which broke the jupyter functionalities
```

- [troubleshooting links here](https://github.com/jupyter/notebook/issues/2664)

- [how to switch Jupyter notebook environments](https://stackoverflow.com/questions/47191297/how-to-switch-environment-on-jupyter-notebook-for-new-notebook)

\
\

#### 03-25-2021: Ticket#2021032510000149

- install single cell

```
mkdir /work/biohpcadmin/zpang1/conda/envs/
module load python/3.6.1-2-anaconda
conda create -p /work/biohpcadmin/zpang1/conda/envs/py_3_5 python=3.5

# enable `conda activate` for python/3.6.1-2-anaconda
echo ". /cm/shared/apps/python/3.6.1-2-anaconda/etc/profile.d/conda.sh" >> ~/.bashrc
source ~/.bashrc

# activate conda environment
conda activate /work/biohpcadmin/zpang1/conda/envs/py_3_5
module load samtools/1.4.1
module load star/2.5.2b
module unload python/2.7.x-anaconda
module unload intel/compiler/64/2017/current

pip install --upgrade pip

# go to or make desired folder to download Singlecell
  # download files

git clone https://github.com/flo-compbio/singlecell.git

cd singlecell/
pip install -e .

# check if `single cell` had been installed into your conda environment

conda list singlecell
```

- To setup environment for future use---was this a good idea?

```
module load python/3.6.1-2-anaconda
module load samtools/1.4.1
module load star/2.5.2b
conda activate /work/biohpcadmin/zpang1/conda/envs/py_3_5
module unload python/2.7.x-anaconda
module unload intel/compiler/64/2017/current
```

\

- better, alternate conda-exclusive install

```
module load python/3.7.x-anaconda
conda create -n singlecell-py35
conda activate singlecell-py35
conda install -c defaults -c bioconda python=3.5 htslib=1.4.1 samtools=1.4.1 star=2.5.3a
pip install --upgrade pip
cd ~/.conda/envs/singlecell-py35/
git clone https://github.com/flo-compbio/singlecell.git
pip install -e .
conda list singlecell
```

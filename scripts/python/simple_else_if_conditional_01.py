## LESSION: ELSEIF Conditional: Where +1 conditions could be considered TRUE within each IF conditional
#
# RESERVED WORD: `elif`
#
## LOGIC FLOW: if a SINGLE condition is met, the `if-elseif` conditional executes the corresponding action, and exits to the next code block. The interpreter will move from `if` to `elif` and onto the next one, and so on, if all preceeding conditions are FALSE.

usrIn = input('What is your favorite, common household pet?\n',)

#

if usrIn == 'Dog' or usrIn == 'dog' or usrIn == 'DOG':

   print('Hey you are like me, a Dog Person!')

elif usrIn == 'Cat' or usrIn == 'cat' or usrIn == 'CAT':

   print('I guess you are a Kitty Guy or Gal')

else:

   print("That ain't no HOUSEHOLD pet!")

#

print("\nOkay, questionarie over~") 

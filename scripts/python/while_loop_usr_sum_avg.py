# Use While Loop to Collect User Integer Input into An Array, and Return the Sum of All Elements in The Array

  # in Python, arrays are defined as "[]" or the "List" datatype

aList=[]
aTotal=int(0)

while True:

  i = input("Enter integer values, one at a time, and press ENTER after each value (enter 'done' to finalize your array)\n")

  if i == 'done' :   # user's way to end the while loop and stop appending to the array
    break

  try:   # use `isinstance()` function to check if the input can be converted into an integer value, if not, the 'except' statement will invoke the 'continue' statement to return to top of while loop, to start another iteration

    isinstance(int(i),int)

  except:

    print("your input cannot be converted into an integer, please try again")
    continue

  i = int(i)         # if all former checks have passed, convert input into 'int' value and append to existing list
  aList.append(i)
  print('Array Elements Thus Far:\n',aList)

for k in aList :   # seek sum of all integer elements in array
  
  aTotal = k + aTotal

print('The total sum of all positive integers in your array equals to:\n')
print(aTotal)

print('The average of all integers equals to:\n')
print(aTotal/len(aList))

# REFERENCES
# [seek sum of all elements in array](https://www.geeksforgeeks.org/python-program-to-find-sum-of-elements-in-list/)
# ['isinstance()' function to check if var is int](https://pythonguides.com/python-check-if-the-variable-is-an-integer/)
# [gather user input to append to array](https://stackoverflow.com/questions/64781691/how-to-add-user-input-integers-to-array)

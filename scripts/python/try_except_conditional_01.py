# Ask user to input an number, and avoid letting the program terminate to string to int conversion error

usrInput = input('Greetings! Please enter a number, and see its integer neighbor!')

try:
    cnrtInput = float(usrInput)
       #NOTE: int() cannot convert strings that contain "decimals"; we have to use float() instead
    cnrtInput = round(cnrtInput)
    print('your number, rounded to the nearest integer, ', cnrtInput)

except:
    cnrtInput = 0 
    print ('your number is invalid! please enter postive or negative numerical value!')

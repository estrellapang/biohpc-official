# Example of a WHILE loop that terminates upon matching of keywords

print('Good morning, or afternoon, or evening here! This program repeats your input and prints it to the screen! \n')
print('To exit program type "Exit", "EXIT", OR "exit"') 

while True :

  usrIn = input('Enter your input > ')

  if usrIn == '':
    print("you have to type AT LEAST a letter or a character :O \n")
    continue

  if usrIn == "Exit" or usrIn == "EXIT" or usrIn == "exit" :
    break

  print("Printing ... \n")
  print(usrIn)
  print("\n") 

print ('Thank you for this test, goodbye and hope to see you again :)\n')

## Realistic `try-except` block intended to DETECT ERROR and OUTPUT FEEDBACK from USERs' INPUT

usrIn = input('Please enter a number (no strings please)\n')

# convery attempt for integer values
try :
    cvtIn = int(usrIn)
    print('\nThe number you have entered is\n',cvtIn)

except :
    print('\nIt appears that you have entered a non-integer value\n')
    cvtIn = 'placeholder'
	# assign "cvtIn" as a string so that the next block can begin testing if the input is a floating point value

# if-conditional "type" check if a variable is of a certain data class
if type(cvtIn) != int:  

  # [reference 01](https://stackoverflow.com/questions/14113187/how-do-you-set-a-conditional-in-python-based-on-datatypes)

# convert attempt for floating point values
  try :
    cvtIn = float(usrIn)
    print('The number you have entered is\n',cvtIn)

# output negative feedback "-1" if user input is neither an integer or a floating point integer value e.g. a string!
  except :
    cvtIn = -1

# print result to indicate invalid input has been entered
if cvtIn < 0 :
    print('Hey, try an integer or a decimal, please~?\n')
    print('Program terminating...Re-run script to see if input value is valid\n')

print ('Ciao!')

# create file handle via `open()` function
# print file content by running FOR LOOP through a file handle object

  # READ and CAT text file ONE LINE AT A TIME as a SEQUENCE

# use of `os` module to list current working directory

import os


print('Current working directory:\n',os.getcwd(),'\n')

fhand_poem = open('./test_files/Learned_Astronomers_Whitman.poem','r')

print('Generating file handle...\n')

fhand_attr = print(fhand_poem)
print(type(fhand_attr))

  # NOTE: a TYPE error occurs when trying to assign the printed out info of filehandle to to a new variable, resulting in an empty variable of the type "<class 'NoneType'>" 

  # NOTE: basically DON'T print(<filehandle>) and expect to easily process the output as a string, do the below instead!

# Get NAME OF OPENED FILE, using the `.name` built in function

poemName = fhand_poem.name

# count number of lines as well
ticker = 0

print('\nReading File:',"'" + poemName + "'",'\n')

print('Processing...only print lines starting with "When"\n')

print('-------------BEGIN FILE--------------\n')

for i in fhand_poem :
  ticker += 1
  i = i.rstrip() # `.rstrip()` eliminates the "\n" characters in the raw string, thus deleting the extra lines inbetween each string in the total sequence 
  if not i.startswith('When') : # `IF NOT` to skip lines that do not match a criterion
    continue
  print(i)

print('\n-------------END of FILE--------------')
print(poemName,'has',ticker,'of lines\n')


# OBSERVATION: ODD Behavior -- if we run a second FOR LOOP through the file handle, nothing seems to happen -- why?

#print('Eliminating Extra Spaces in the Text')

#for j in fhand_poem :
#  j = j.rstrip()      # `.rstrip()` eliminates the "\n" characters in the raw string, thus deleting the extra lines inbetween each string in the total sequence
#  print(j)
#
#print('\nAbove output was accomplished using the .rstrip() function of strings')

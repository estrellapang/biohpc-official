# utilizes string indexing and .find() function to parse out/extract first and last name from user input

rawEmail = input('Enter your email in the following format: "<FIRST>.<LAST>@institution.state.edu"\n')

while True :

  if '@' not in rawEmail or 'edu' not in rawEmail or '.' not in rawEmail :
    print('INCORRECT format! Please try again!') 
    rawEmail = input('Enter your email in the following format: "<FIRST>.<LAST>@institution.state.edu"\n')
    continue

  else :
    break

print("This is Your entry:",rawEmail)

# extract any extra empty spaces at the beginning and the end of the string
rawEmail = rawEmail.strip()

# use .find() to locate index positions of first and last name 

period = rawEmail.find('.')
atSign = rawEmail.find('@')

firstName = rawEmail[0:period]   # note that the second index position, 'period' -- its value is not read -- python string index rules ...
lastName = rawEmail[period+1:atSign]   # if we start at 'period' index position, the '.' character will be included

print('Thank you,',firstName + '!', 'We are sure you will make the',lastName,'family proud being a part of us!') 

  ## "firstName + '!'" is so that the str variable can be printed without an auto space char between it and the "!"

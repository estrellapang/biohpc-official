## YAMLs in Python

### Resources

- [link 1](https://stackoverflow.com/questions/14262433/large-data-workflows-using-pandas?rq=1)

- [link 2](https://www.softwaretestinghelp.com/yaml-tutorial/)

- [link 3](https://stackabuse.com/reading-and-writing-yaml-to-a-file-in-python/)

- [link 4](https://towardsdatascience.com/from-novice-to-expert-how-to-write-a-configuration-file-in-python-273e171a8eb3)

- [link 5](https://www.tutorialspoint.com/yaml/yaml_introduction.htm)

- [link 6](https://www.softwaretestinghelp.com/yaml-tutorial/)

- [link 7](https://geekflare.com/python-yaml-intro/)

- [link8_ansible_example](https://blog.stackpath.com/yaml/)

- [link_9_kubernetes_example](https://dzone.com/articles/yaml-and-its-usage-in-kubernetes)

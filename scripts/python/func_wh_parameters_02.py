## define function ---

  # "usrType" acts as a placeholder for when arguments are passed and stored into the function

def lingo(usrType) :
  if usrType == "EN" or usrType == "en" or usrType == "En" :
    return("Greetin's!")
  elif usrType == "SN" or usrType == "sn" or usrType == "Sn" :
    return("Bueno Diaz~")
  elif usrType == "CN" or usrType == "cn" or usrType == "Cn" :
    return("Nin Hao,") 
  else :
    return("Sorry, we do not have your native language uploaded to memory yet,")

## end of function definition ---

## ask user to enter input, which is stored into the parameter "usrType"
usrType = input("Ey man! Choose a Language (EN,ES,CN,JP,GM,RS,AB,HD)\n")
userName = input ("Ey, don't forget to enter your name!\n") 

## call the lingo(function) with values stored inside the parameter
print(lingo(usrType),userName)

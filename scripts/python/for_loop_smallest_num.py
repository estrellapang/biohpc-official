# Using for loop to iterate through an array of values, and output the smallest one

anArray = [ 1.2, 4.6, 5.7, 12.6, 8.8, 0.7 ]
contestant = 'null' 

for i in anArray :

  print ('comparing ',contestant,'to ',i)

  if contestant == 'null' or float(contestant) > float(i) :
    
    contestant = i
    print ('the smaller number is now:', contestant)

print ('Evaluation Complete, the smallest number in argument array is...\n',contestant)

# ALTERNATIVE Approach, use `min()` function
#
# `min(anArray)`

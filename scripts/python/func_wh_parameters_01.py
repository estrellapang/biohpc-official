# define function

  # "usrType" acts as a placeholder for when arguments are passed and stored into the function

def lingo(usrType) :
  if usrType == "EN" or usrType == "en" or usrType == "En" :
    print("You've choosen English, pretty popular choice")

# ask user to enter input, which is stored into the parameter "usrType"
usrType = input("Ey man! Choose a Language (EN,ES,CN,JP,GM,RS,AB,HD)")

# call the lingo(function) with values stored inside the parameter
lingo(usrType)

# While-loop code snippet on how to loop through string variables

# TASK: output the value of each indexed character in a string

# NOTE: An iteration variable is required

runString = 'Will You Come, New Earth?'
runner = 0   # iteration variable

print('Original String\n')
print('###############################')
print(runString)
print('###############################\n')

print('This string has',len(runString),'characters')

while len(runString) > runner:

  print(runner,runString[runner])
  runner += 1

print('The given string has',runner,'# of characters')

# conditional TYPE: NESTED CONDITIONALS with TWO-WAY, ELSE Decisions

rawInput=input('Enter an integer between 0 and 10: ')
usrInput=int(rawInput)

if usrInput >= 0 and usrInput <= 10:
  print('the integer you have entered is',usrInput)

  if usrInput%2 == 0:
    print('you have entered an even number!')
  else:
    print('your integer is considered odd')
  print('we good for now, bye')
else:
  print('invalid, YO!')

# For-loop code snippet on how to loop through string variables

# TASK: output the value of each indexed character in a string

runString = 'Will You Come, New Earth?'

print('Original String\n')
print('###############################')
print(runString)
print('###############################\n')

for runner in runString:

  print(runner) 

print('The given string has',len(runString),'# of characters')

# LESSON: 'if-elif' conditionals DO NOT need to be closed by an 'else' conditional!

usrIn = input('Do you cast thou net to the Left or Right? (enter "L/R")')

if usrIn == 'l' or usrIn == 'L' :
    print("You've chosen the Left-Hand Path")
elif usrIn == 'r' :
    print("You've chosen the Righteous Path")
elif usrIn == 'R' :
    print("You've chosen the Righteous Path")

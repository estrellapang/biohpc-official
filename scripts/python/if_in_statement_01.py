# if-loop combined with `in` operator to detect specific characters/patterns

inPut=input('Type a sentence, and we will spin the wheel to see if you have won the jackpot!\n')

if 'truth' in inPut or 'Truth' in inPut or 'TRUTH' in inPut : 
  print('Oh...my..! You are on to the TRUTH! The ULTIMATE JACKPOT!')

### alternative to consecutive 'or' operators --> elif's ###

#elif 'TRUTH' in inPut :
  #print('Oh...my..! You are on to the TRUTH! The ULTIMATE JACKPOT!')

#elif 'Truth' in inPut :
  #print('Oh...my..! You are on to the TRUTH! The ULTIMATE JACKPOT!')

### END OF ALTERNATIVE BLOCK ###

else :

  print('Better try again next time :)')

# Using for loop to iterate through an array of values, and output the Largest one

anArray = [ 1.2, 4.6, 5.7, 12.6, 8.8, 0.7 ]
contestant = 0

for i in anArray :

  print ('comparing ',contestant,'to ',i)

  if float(contestant) < float(i) :
    
    contestant = i
    print ('the largest number is now:', contestant)

print ('Evaluation Complete, the largest number in argument array is...\n',contestant)

# SHORTCUT to code above, use built-in function for list datatype
#
# `max(anArray)`

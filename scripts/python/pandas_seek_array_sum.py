# import Pandas and Numpy into working path
import pandas as pd
import numpy as np
testDf = pd.read_csv('HOLDER', header=None, names=["sizes"])
print('# of rows in dataframe:', len(testDf.index))
#
# SEEK SUM of all elements in array
kSum = testDf['sizes'].sum()
print('the sum of all file sizes', kSum)
print('file sizes in TB', kSum / 1024**4)

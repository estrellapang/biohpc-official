# LESSON: Indentations and De-indentations signify the beginning and the closing of conditional blocks!

# conditional TYPE in this example: BLOCKED Conditionals/Decisions, each conditions are independent of each other

rawInput=input ('please enter a number(decimals welcomed) between 0 and 90:')
usrInput=float(rawInput)
# without TYPE conversion we cannot perform a numerical conditional operation on our variable, which is initially inputed as a TYPE str

if usrInput >=0 and usrInput <=30:
  print('look at that!',usrInput,',you must be a symbolic thinker')
  print("okay, now we know a little more about you :)")

if usrInput > 30 and usrInput <= 79:
  print('looky here,',usrInput,",we've got a numbers person!")   
  print("okay, now we know a little more about you :)")
if usrInput >=80 and usrInput <= 90:
  print('well well,',usrInput,",it appears that you like to push the envelope~")
  print("okay, now we know a little more about you :)")

if usrInput < 0 or usrInput > 90:
  print("invalid number entered!")

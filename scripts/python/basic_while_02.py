# basic while loop with user input
i  = input('Greetings! Enter an integer ranging larger than 3 and smaller than 12:\n')

# convert str to floating point variable
i = round(float(i))

  # `i = i + 1` does not bode well with floats, imagine n_1 = 3.01, n_1 = 3.02 ...
print("the number you've entered is or has been rounded off to,\n", i)

if i <= 3 or i >= 12 :
    print("value not in design range!")
    exit()

print ("WHILE LOOP HAS STARTED!")
while True :
    if i == 12 :
            break
    print(i) 
    i = i + 1

print('The while loop has finished!')

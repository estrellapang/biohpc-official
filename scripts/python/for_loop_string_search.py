# use of while loop to collect user input for string value

# subsequent use of for loop to match specific phrases, and count how many times they appear in the input string 

strTemplate=''

while True:

  i = input("Enter your sentence, press ENTER to enter next sentence (EXIT to finish)\n")

  if i == 'EXIT' :   # user's way to end the while loop and stop appending to the array
    break

  strTemplate = strTemplate + ' ' + i

  print('Your input thus far', strTemplate)

# use for loop to match and count specific characters/patterns

sentences = 0

for j in strTemplate:

  if j == '.' :

    sentences += 1

print('There are',sentences,'sentences in your paragraph!')

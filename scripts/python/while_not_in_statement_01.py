# while-loop combined with `in` operator to detect the specific characters/patterns

inPut=''

while 'lies' not in inPut :

  if inPut == 'exit' or inPut == 'Exit' or inPut == 'EXIT' :
    print('goodbye -')
    exit()

  if 'Lies' in inPut:
    break
  if 'LIES' in inPut:
    break

  print('your words are sane')
  inPut=input('Type words and sentences, press ENTER to check sanity(EXIT to quit)\n')
 
print('YOU HAVE LIED!')

## define function ---

  # "usrType" acts as a placeholder for when arguments are passed and stored into the function

def lingo(usrType, usrName) :

  # below is an example of a rookie practice: using 'return()' as one would use 'print()'
  # LESSON: return values are to be stored, not explicitly printed out---think "store for further operations"
  if usrType == "EN" or usrType == "en" or usrType == "En" :
    return("Greetin's!",usrName)
  elif usrType == "SN" or usrType == "sn" or usrType == "Sn" :
    return("Bueno Diaz~")
  elif usrType == "CN" or usrType == "cn" or usrType == "Cn" :
    return("Nin Hao,")
  else :
    return("Sorry, we do not have your native language uploaded to memory yet,", usrName)

## end of function definition ---

## ask user to enter input, which is stored into the parameter "usrType"
usrLan = input("Ey man! Choose a Language (EN,SN,CN,JP,GM,RS,AB,HD)\n")
usrLab = input ("Ey, don't forget to enter your name!\n")

## call the lingo(function) with values stored inside the parameter
print(lingo(usrLan, usrLab))

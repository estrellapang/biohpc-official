# Example: Use for-loop to gather user input to make an integer array

# NOTE: this code has NOT been reviewed! Copied from https://xiith.com/python-program-to-create-an-array-by-user-input/

import array as arr

a = arr.array('i', [])

k = int(input("Enter size of array:"))
for i in range(0, k):
    num = int(input("Enter %d array element:" % (i + 1)))
    a.append(num)

print("All array elements are:", end="")
for i in a:
    print(i, end=" ") 

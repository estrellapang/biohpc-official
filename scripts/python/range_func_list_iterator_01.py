# how range() and len() funcs can be used in tandem to iterate through lists

interestingList = ['Father', 'Son', 'Holy Spirit']

for i in range(len(interestingList)) :

   print('element',i, ',' ,'value = ',interestingList[i])

inputNum = float(input('please enter a number (decimals are welcomed)'))
# convert default string input into a floating type variable
# this is needed to later convert to interger type
#
# here alternatively we could have done float() in the next sequential step
# e.g. `inputNum = float(inputNum)`

print ("the number you have entered is ",inputNum)
convertInt = int(round(inputNum))
print ("and it rounds off to ",convertInt)

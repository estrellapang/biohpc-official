## Python Concepts and Syntax and Examples

- [all topics covered are neatly condensed and elegantly explained here](https://www.freecodecamp.org/news/the-python-handbook/#dictionariesinpython)

### Variables

- **all has to start with underscore or letter**

- WATCH OUT not to use python's reserved words:

```
# to retrieve words from python3 console

python3 --> help() --> keywords

----------------------------------------------------------------
False               class               from                or
None                continue            global              pass
True                def                 if                  raise
and                 del                 import              return
as                  elif                in                  try
assert              else                is                  while
async               except              lambda              with
await               finally             nonlocal            yield
break               for                 not
----------------------------------------------------------------
```

\
\

#### Variable Basics and TYPES

- **Variable Assignment RULE**

```
<declare variable> = <definition: function/value>

```

- variables can be assigned via expressions

  - e.g. `x = 12/4 + 5**2`


- **determine data/variable TYPE**

- using function `type()`

  - e.g.

```
aString = 'hello ' + 'bright ' + and 'beautiful'
type(aString)

# OUTPUT:

>> <class'str'> 

# for integers the output would be `<class'int'>`

# for floating point, `<class'float'>'`

```

- **depending on the TYPE of variable**, operators may perform different actions

  - [use if-conditionals based on data class](https://stackoverflow.com/questions/14113187/how-do-you-set-a-conditional-in-python-based-on-datatypes)


\
\


#### STRINGS: they can represent numbers and characters

- ARRAY-like in backend

  - each character in a STRING/ARRAY variable has AN INDEX, starting from '0'

  - output value of each index, enter `<string_var>[index #]` e.g. `aString[1]`

  - INDEX VALUES can be COMPUTED, e.g. `aChar = aString[3 -1]` -->  this assigns `aChar = aString[2]`

  - INDEX VALUES can be expressed in A RANGE, e.g. `aString[3:7]` --> this outputs the values from index #3 --> #6

    - [example script illustrating above phenomenon](string_parse_extract_01.py)

    - `sString[:]` --> this outputs the ENTIRE string

    - **note**: the continuous selection via the colon operator DOES NOT include the index value of the second number

\

- String Concatenation

```
intty = 9 + 10
print(intty)

>> 19

stringgy = 'couch' + 'learner'
# here the '+" operator does concatenation
print(stringy)

>> couchlearner
```

\

- **Looping Through Strings**

- [via while loops](while_loop_through_string.py)

- [via for loops](for_loop_through_string.py)

  - Notice how for-loops do not require explicit iteration of a counter variable to loop through a string, this is advantageous for simplicity

\

- **Booleans for Strings**

  - `<` and `>` --> compares strings based on Unicode value (all strings are Unicode in Python 3+)

  - if two strings being compared are not equal in length, the comparison only acts up until the end of the shorter string

  - use function `ord()` --> outputs the unicode value of characters

  - [good tutorial for boolean operators on strings](https://www.thecoderpedia.com/blog/python-string-comparison/)

\


- **GENERAL for STRINGS**

> not exclusively to strings; some are extremely useful for arrays of integers and floats as well

- `len():` : outputs the total character count for strings and arrays

  - e.g. `strLength=len(aString)`

- `min('string')` and `max('string')` --> find smallest or largest value numerical or alphabetical in a string

\

- **BUILT-IN FUNCTIONS for STRINGS**

- included in the string library, and each string has these functions PRE-BUILT into it

  - we are only calling/invoking them

  - they DO NOT MODIFY the original string, and instead OUTPUTS NEW string, which can be stored as variable

- APPLICATION for WORD/STREAMING PROCESSING:

  - combined with indexing and boolean operators, the string built-in functions can do a lot, nearly comparable to BASH's efficiency in the same topic --> [example script parsing out first and last names from email addresses](string_parse_extract_01.py)

- use function `dir()` to list built-in functions for variables

  - e.g.

```
>>> dir(aString)
['__add__', '__class__', '__contains__', '__delattr__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__getitem__', '__getnewargs__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__iter__', '__le__', '__len__', '__lt__', '__mod__', '__mul__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__rmod__', '__rmul__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', 'capitalize', 'casefold', 'center', 'count', 'encode', 'endswith', 'expandtabs', 'find', 'format', 'format_map', 'index', 'isalnum', 'isalpha', 'isascii', 'isdecimal', 'isdigit', 'isidentifier', 'islower', 'isnumeric', 'isprintable', 'isspace', 'istitle', 'isupper', 'join', 'ljust', 'lower', 'lstrip', 'maketrans', 'partition', 'replace', 'rfind', 'rindex', 'rjust', 'rpartition', 'rsplit', 'rstrip', 'split', 'splitlines', 'startswith', 'strip', 'swapcase', 'title', 'translate', 'upper', 'zfill']
```

- SYNTAX: `string.<strFunc>()` 

  - e.g.

```
>>> aLow = 'cottage cheese'
>>> aLow.upper()
'COTTAGE CHEESE'
```

\

- **COMMON BUILT-IN FUNCs FOR STRs**

- `.find()` --> finds the FIRST OCCURENCE of substrings/characters in the string

  - returns the INDEX POSITION(remember, they start at '0') when match is found

  - returns '-1' when no matches are found

- e.g.

```
>>> aString = 'Ohana'
>>> aString.find('an')
2
```

- `.replace()` --> WORD PROCESSOR! (think `awk`, `sed`, `tr` in BASH terms)

  - Syntax: `someString.replace('<search term>','<replacement term>')

  - "search and replace"

  - ALL INSTANCES of search string are replaced by the replacement string

- e.g.

```
>>> aString = 'Ohana'
>>> aString.replace('a','e')
'Ohene'

```

- `.upper()` AND `.lower()` --> WORD PROCESSOR!

  - turn everything into upper and lowercase

- `.strip()`, `.lstrip()`, `.rstrip()` --> WORD PROCESSOR!

  - remove all whitespace, from the beginning, or from the end of the string, respectively

  - e.g.

```
String[:]
' Ohana '

>>> aString.strip()
'Ohana'

>>> aString.lstrip()
'Ohana '

>>> aString.rstrip()
' Ohana'
```

\

- `.split()` --> splits a string into individual components as specified

  - default behavior: identifies space characters as SINGLE delimiter

  - can specify delimiters

  - OUTPUTS a list datatype! Converts strings to lists!!!

e.g.

```
kindList = 'Protect The Innocent'

kinderList = kindList.split()

type(kinderList)
<class 'list'>

print(kinderList)
['Protect', 'The', 'Innocent']

### choose SPACE characters delimiters 

  # useful for counting the number of spaces

kindList = 'Protect   The    Innocent   '

kindList.split(' ')
['Protect', '', '', 'The', '', '', '', 'Innocent', '', '', '']

  # note that each match returns a non-value character
```

\


- **NOTE**: functions can be embedded into other functions like `print()`

  - e.g. `print(aString.lower()` --> print the string but transformed to all-lower case

\
\

#### floating point-integer conversions

- when BOTH floats & ints are used in an operation/expression, the int is converted to float and the OUTPUT will be float

  - BEHAVIOR: numerical division operations using integers produce floating point results

- use functions `int()` and `float()` to convert between the two numerical variable types

  - NOTE: a string variable cannot be converted into an integer---doing so will throw an error

\

#### Arrays/"Lists"

  - "Lists" in Python datatype

- syntax:

  - for integers and floats: `arrayVar = [ <n_1>, <n_2>, ... <n_n> ]`

  - for strings `arrayVar = [ '<n_1>', '<n_2>', ... '<n_n>' ]`

  - slicing lists `arrayVar[ <index>:<index> ]`

    - RULE: first/single index INCLUDES or BEGINS, `arrayVar[:3]` --> returns elements 0 to 3

    - RULE: second index indicates UP TO BUT NOT INCLUDING: e.g. `arrayVar[0:3]` --> returns elements 0 to 2!

\

- **PROPERTIES**

- one may declare an **EMPTY ARRAY** at beginning of script for appending later if needed

- **each element can be ANY PYTHON OBJECT**

  - e.g. an element can contain multiple values, don't think one-dimensionally

```
>>> interestingList = [3, 6, 9, [1,4,4]]

>>> for i in interestingList :
...   print(i)
...

3
6
9
[1, 4, 4]

```

\

- **Lists are MUTABLE**

  - whereas STRINGS ARE NOT, e.g. `aString[0] = <new value>` will trigger a traceback

  - `aList[3] = <new value>` --> will execute

\

- **Lists can be concatenated**

  - `aList = aList + bList` --> totally works

\

- **Relevant Functions**

- `len()` returns # of elements in a list as it does strings

```
>>> len(interestingList)
4
```

\

- `.sort()` --> sorts elements of list alphabetically or numerically

  - can specify function for sorting

  - [more info here](https://www.w3schools.com/python/ref_list_sort.asp)

\

- `range()` --> creates a range of values all in integers

  - assigns its own class/datatype to variables, called "range" (not list)

  - only works with integers

  - syntax: `range(<int>)` --> 0 to int ; `range(<start_int>,<end_int>)` --> some # to some #

  - USEFUL for iterating through lists and indexing each element, [code snippet here](range_func_list_iterator_01.py)

  - e.g. CONVERT EACH ELEMENT from STR to INT --> and sort them in ascending order, then reverse order

```
>>> print(totalList)
[2, '3', 9, 4, '5', 7]

>>> for i in newList : type(i)
...
<class 'int'>
<class 'str'>
<class 'int'>
<class 'int'>
<class 'str'>
<class 'int'>


  # CONVERT element-wise, STR to INT (`sort.()` function cannot sort both INT and STR elements in a single list)

>>> for i in range(len(newList)) : newList[i] = int(newList[i])

>>> for i in newList : type(i)
...
<class 'int'>
<class 'int'>
<class 'int'>
<class 'int'>
<class 'int'>
<class 'int'>

print(newList)
[2, 3, 9, 4, 5, 7]

  # NOTE: this function operates and changes the list completely
newList.sort() 

print(newList)
[2, 3, 4, 5, 7, 9]

  # NOTE: sort again in reverse fashion

>>> newList.sort(reverse=True)

>>> print(newList)
[9, 7, 5, 4, 3, 2]

```

\

- `.append()` --> append to the end of the list

```
>>> kindList=[ 'one' ]
>>> kindList.append('two')
>>> kindList.append('3')
>>> kindList.append(4)

>>> print(kindList)
['one', 'two', '3', 4]
```

\

- `.insert(<index>,<value>)` --> prepend to the start of the list, or ANYWHERE

```
>>> kindList.insert(0,'beginning')
>>> print(kindList)
['beginning', 'one', 'two', '3', 4]

>>> kindList.insert(5,'end')
>>>
>>> print(kindList)
['beginning', 'one', 'two', '3', 4, 'end']
```

\

- `min()` & `max()` & `sum()` 

  - could save time from coding for-loops


\

- `.split()` --> turn strings into lists

  - BEHAVIOR: by default, translates spaces characters into a SINGLE delimiter

  - we can .split() twice or more, depending on the complexity of text body

  - alternative approach: declare empty list, then set equal to string var:

```
kindList = 'Protect The Innocent'
kinderList[:] = kindList
print(kinderList)
['P', 'r', 'o', 't', 'e', 'c', 't', ' ', 'T', 'h', 'e', ' ', 'I', 'n', 'n', 'o', 'c', 'e', 'n', 't']

  # notice how EVERY LETTER is turned into an index? using .split() function may be a little better

```

\
\

### Data TYPE: Dictionaries

> Also called:
> "Associative Arrays" in Perl/PHP
> "Properties/Maps/HashMap" in Java
> "Property Bag" - in C#/.Net

\

#### Data Structure

- Similar to LISTS

  - array-like, but entries have no apparent numerical order AMONG ELEMENTS -- e.g. element "Bridge" does not have a numerical precedence to "River" in data logic, regardless of which was inserted into the dictionary/collection first

  - each element has a/multiple "tag" or "key", with associated values, commonly numerical e.g. {'Trinity': 3} 

\

#### Why is it SPECIAL?

- **"key-value" pair association**

  - more context and easier to understand than lists, which uses "index(or position)-value" based association  

  - establishes the basis of database-like data structures, allows for fast data querying operations

\

#### Syntax

- **declare dictionary var via dict() function**

- e.g. `NaturalLaw = dict()` --> declares var "NaturalLaw" as an empty dictionary

    - v.s. `NaturalLaw=[]` --> declares as empty list

    - insert key and value into dictionary `NaturalLaw['Marriage'] = 2`

    - `print(NaturalLaw['Marriage'])` --> output the value of a tag/key

    - `print(NaturalLaw)` --> output all key-value pairs in dictionary

\

- **dictionary elements are mutable**

  - `NaturalLaw['Self']` = `NaturalLaw['Self'] + 1`

\

- **declare dictionary via '{}'**

  - these curly braces are called "dictionary literals" 

  - `dictionaryVar = {}` --> declare an empty dictionary

- syntax:

```
dictionaryVar = { 'key1' : <value_1> , 'key2' : <value_2>, 'key3' : <value_3> }
```

\

- tutorials

```
https://www.softwaretestinghelp.com/working-with-python-dictionary/
https://www.jquery-az.com/python-dictionary-explained-with-11-examples/
https://realpython.com/python-dicts/
https://www.freecodecamp.org/news/the-python-handbook/#dictionariesinpython
```

\
\
\

## Conditional/Comparison Operators

- [Great Reference Sheet from W3SCHOOL](https://www.w3schools.com/python/python_operators.asp)

\

#### Boolean expressions

- uses comparison operators to ask a question and to yield a TRUE or FALSE output

- comparison operators DO NOT CHANGE the variables! Only compares.

- Python will automatically perform type conversions when comparing data types e.g. int against floats `0 == 0.0` will return `True`

```
# LESS THAN

   < 


# LESS THAN OR EQUAL TO

   <=

# EQUAL TO

   ==

## GREATER THAN OR EQUAL TO

   >=


## GREATER THAN

   >


## NOT EQUA:

   !=

```

\

#### Logical Operators

> used for combining conditional statements
> NOTE: these are still read and evalutated sequentially

- `and`

- `or`

- `not` --> returns False signal if the conditionals are met

\

#### Unique Operators

- `is`

  - simiar to `==` but requires that the two variables are of the SAME type!

  - returns "True" or "False" as output

  - e.g. `0 is 0.0` --> False

\
  
- `is not`

  - e.g. `0 is not 0.0` --> True

\

- `in`

  - used to identify if specific characters/strings are WITHIN other strings

  - returns "True" or "False", commonly used in `if` statements

  - e.g. `'kind' in 'humankind'` --> `True` [if statement exampple](if_in_statement_01.py)

  - can be combined with `not` to make `not in` [while_loop_example](while_not_in_statement_01.py)


\
\
\

### Conditionals

\

#### if statements

- **syntax**

```
# CONVENTIONAL:

if <condition>:
   action
   action
   action

<new code block> 
.
.
.

# SHORTHAND:

if <condition>: <action>
<new code block>

e.g.
--------------------
# use IF statement to break out of WHILE loops

while True:
  userInput = input('Enter a value')
  if userInput == 'done' : break
--------------------
```

- **Identation**

- each if-block iss defined by the code contained within the indented code underneathe it

  - if "yes" --> runs code in if-block

  - if "no" --> skips all code in if-block and moves onto next code block


- **Nested If-blocks**

- INDENTATION MATTERS A LOT!

- e.g. the following if-blocks are different!

```
# example 1

if x == 0:
if y == 1:
  print("on point!") 

# example 2

if x== 0:
  if y== 1:
    print("on point!")

# COMMENT: in the 2nd example, the nested "if y == 1" conditional will not be evaluated unless the preceding "x == 0" statement had been evaluated to be TRUE... if x = 0 AND y = 1 --> print "on point!" onto screen. In the first example, the y if-block will be evaluated indepedent of the first x if-block
```

\

#### if-else conditionals

- **PURPOSE**: if TRUE --> perform an action; if FALSE --> perform a different action

  - ONLY 1 of the 2 action is performed

  - note that a simple if-conditional only performs an action when TRUE

  - if-else conditional is intended for programming "two-way decisions"


- **syntax**

```
# A single if-else block: note that "else" doesn't need to be indented within its corresponding if-statement
if <condition>:

  action

else:

  action

```

\

#### if-elseif-else conditionals

- **PURPOSE**: multiple TRUE conditions are allowed

  - **Logic Flow**: if one condition is TRUE, then the block is exited---if there many possible TRUE conditions, there is still only ONE condition that can be true 

  - `if .. elif` is interchangeable with `if <condition> or <2nd condition>` 

- **syntax**

```
if <>:

elif <>:

else <>

  ## END OF BLOCK
```

- **NOTE**: the block does not need to END with an `else` statement, but this would mean that there's no negative feed-back---if no input is TRUE, then the code could return no-feedback (may or may not be desired, depending on the situation).

\

#### try-except 

- **PURPOSE**: an unique conditional that serves to prevent the erroneous parts of the script from sending out "traceback" calls and stop the entire program

  - A mechanism for error compensation (explained in better detail below)

  - EXECELLENT conditional for outputting tangible feedback as error detection

  - surround a risky line or block of code with an try-except block, and the program will proceed to fail back on the fail-safe, "except" statement for execution.

  - NOTE: best-practice, avoid stuffing more than a few statements inside the "try" block, for the program will IMMEDIATELY JUMP to the "except" clause upon the first error---this causes the other lines to never get a chance to be executed and tested forerror---NOT PRO PRACTICE

- **syntax**

```

try:

  statement
  statement

except:

  fall-back statement

```

\
\

### LOOPS

#### While, or "Indefinite" Loops

> They will keep repeating until a FALSE condition appears
> Could result in infinite loops if one is not careful with setting the appropriate FALSE conditions

\

- **SYNTAX**

```
### CONVENTIONAL
--------------------------------------------------------
statement

while <condition> and <condition> .. : 

  statement
  statement
  ..         # once at end of loop, returns to top of while to evaluate if conditions are TRUE for another round

statement    # this will be executed independently, once the while loops finishes or is bypassed due to FALSE conditions
--------------------------------------------------------

\

### MORE FLEXIBLE 'while True:' code blocks 

  # LEVERAGE MULTIPLE TRUE CONDITIONS via "while-if" combination
--------------------------------------------------------
while True:

  statement  # declare relevant variables and their values
  statement

    if conditional
      break

    if conditional
      continue

   statement

statement
--------------------------------------------------------

```

\

- **COMPONENTS**

  - **iteration variables**: variables that change each time the loop is repeated e.g. `n = n + 1` [basic while loop using iteration variables](basic_while_02.py)

  - **break** statement: an "exit()" call to end the current loop and move onto the next code block; serves as a negative feedback, and often used with IF statements inside WHILE loops [example script here](while_break_01.py)

  - **continue** statement: ends the current iteration of the while loop and returns to the top of the loop, to start another iteration

    - [example 1](while_break_01.py)

    - [example_2](while_continue_01.py)

\
\

#### For, or "Definite" Loops

> These loops are repeatedly executed for an exact number of times
> The iteration variables are typically arrays of numbers and strings
> For each iterated loop, the current value of the iterated variable is inserted as input/argument to the functions and statements within the for loop. The iterated variable moves onto the next value in the array once the current iteration finishes


- **SYNTAX**

```
# iterate through explicit array
--------------------------------------------------------
for i in [0, 1, 2, 3, 4, 5, 6, 7] :

  statement(i)
  statement(i)
  .
  .
--------------------------------------------------------

\

# iterate through array variable
--------------------------------------------------------
oneSet = [0, 1, 2, 3]

for i in oneSet :

  statement(oneSet(i))
  statement(oneSet(i))
  .
  .
--------------------------------------------------------

```

\
\
\

### Assignment Operators

#### Basic Arithmetic Opertors

- `+=`

  - `x += 3` == `x = x + 3`

- similarly, we also have: `-=`, `*=`, `/=`, `%=`


#### Special Arithmetic Operators

- `**` --> exponent/power

- `%` --> remainder

```
a = 21
b = a % 5
print(b)

>> 1
```

- numeric operators work within certain functions, e.g. `print()`


```
print(3**2)
>> 9

print(var + 3)
```

- **MUST-HAVE REFERENCES**

- [full list ALL python operators: comparative, boolean, and arithmetic!](https://www.w3schools.com/python/python_operators.asp)


\
\
\

### Functions

#### Terminology

  - "function" --> stored scripts for easy reuse

  - "argument" --> real INPUT into a function

  - "paramter" --> a variable defined INSIDE a function for it to act upon the INPUT ARGUMENT and perform logical operations on it. Parameters store values for input arguments

  - **'def' keyword**: defines and stores a function

  - **'return' keyword**: outputs a value from the input given; most built-in functions use the 'return' keyword, i.e. print(),min(),max(),int(),type()


#### Exmaples

- convert string variable to list varialbe, character-wise

```
def stringConvert(stringy): 
    list1=[] 
    list1[:0]=stringy
    return list1   # prints out/outputs value of converted list
```


\
\
\

#### BEST PRACTICES

  - define your funcs at the BEGINNING of your script (if you can) 

#### syntax

```
## Most simplistic form
---------------------------------------------------
# define and store function:

def <func_name>():
  line
  line
  line

# call/invoke function later in code:

<func_name>(<argument/input>):
---------------------------------------------------

\

## Define function with a parameter:
---------------------------------------------------
def <fx_name>(<parameter_var_name>):

## ~ with multiple parameters:   NOTE: the function will throw traceback errors if not enough parameters were entered, given that they've already been defined in their functions

def <fx>(parmA, parmB, parmC):
---------------------------------------------------
```

- e.g.

```
# max() is a built-function that outputs the largest character within the argument
---------------------------------------------------
max('Tetelestai')
't'

max('17')
'7'

  # NOTE: use '' around your argument or you will get an TypeError---max() only operates on strings
---------------------------------------------------

# min() like max(), but servers the opposite purpose
---------------------------------------------------
min('1993')
'1'
---------------------------------------------------

# more refined example
---------------------------------------------------
  # in console mode, do remember to manually ident after starting code blocks!

>>> myVar="abba"
>>> if max(myVar) >= "e":
...   print("your input has great potential!")  # had to manually indent
... else:
...   print("eek...")  # had to manually indent
...
eek...
---------------------------------------------------
```

\
\
\

### Accessing Data using Python

- `file handle` -- a VARIABLE used by Python to operate on files

  - through a file handle, the actual file may be opened

  - similar to `File --> Open`

  - DO NOT contain the ACTUAL data --> SITS IN BETWEEN input and actual file

  - open, read, write, close operations ALL PASS to the HANDLE

  - a declared handle contains file metadata such as permissions, filename, and encoding commonly "UTF-8"



- `open()` -- the FUNCTION THAT CREATES FILE HANDLES!

  - Syntax: `open(filename,mode)`

    - mode: `r` (read) or `w` (writable) 


- `print(<file_handle_object>)` --> get file attributes, CANNOT PASS OUTPUT to STR variable

  - instead, use built-in funcs like `.name` --> e.g. `aString = fileHandle.name`

  - [output open file name w/o full path](https://stackoverflow.com/questions/323515/how-to-get-the-name-of-an-open-file)

  - [see example script here](open_files_read_sequence.py)

  - [if we absolutely wanted to save stdout to a variable/file](https://www.askpython.com/python/python-stdin-stdout-stderr)

\

#### Ways of Scanning Through Text Files

- **Using For-Loop**

  - NOTE: it's interesting that we can run a for loop through a file handle, an object rather than a conventionally, through string variables

  - most efficient, each LINE is declared as a STRING VARIABLE in AN SEQUENCE of strings that makes up the text file

  - in output, each line is followed by an EMPTY LINE (we may need to remove that)

  - [example script](open_files_read_sequence.py)

\

- **Store all content into SINGLE STRING**

  - coding this way may be timesaving, but this affects read performance if file is large

  - no extra spaces inserted here

  - e.g.

```
>>> WW = open('Learned_Astronomers_Whitman.poem','r')
>>>
>>> oneLiner = WW.read()
>>>
>>> print(oneLiner[:])
When I heard the learn’d astronomer,
When the proofs, the figures, were ranged in columns before me,
When I was shown the charts and diagrams, to add, divide, and measure them,
When I sitting heard the astronomer where he lectured with much applause in the lecture-room,
How soon unaccountable I became tired and sick,
Till rising and gliding out I wander’d off by myself,
In the mystical moist night-air, and from time to time,
Look’d up in perfect silence at the stars.

#
#
# if we need to visualize the '\n' empty space characters in output, use `repr()` function:

>>> print(repr(oneLiner))
'When I heard the learn’d astronomer,\nWhen the proofs, the figures, were ranged in columns before me,\nWhen I was shown the charts and diagrams, to add, divide, and measure them,\nWhen I sitting heard the astronomer where he lectured with much applause in the lecture-room,\nHow soon unaccountable I became tired and sick,\nTill rising and gliding out I wander’d off by myself,\nIn the mystical moist night-air, and from time to time,\nLook’d up in perfect silence at the stars.\n'

```

\

- **SEARCH through opened files**


- via `IF-IN/IF NOT IN` Statements (and `.find()` for positioning)

```
### via for-looping through filehandle

  # NOTE: the iteration variable e.g. "i" in each cycle is actually READ AS A LINE when looping through file handles --- 
  # so if specific terms or characters were to be sought out and matched, then we may use `in` and/or `.find()` to return matches for strings that are contained within each line i.e. `if <searchTerm> in i :`

-----------------------------------------
for lines in fileHandle :

  if searchTerm in lines :
     indexLocate = lines.find(searchTerm)
     print(lines[0:indexLocate])
-----------------------------------------     
```




- [e.g. match words that begin with certain sentences](open_files_search_start_terms.py)

- [e.g. match any word in an opened file](open_files_search_any_term.py)

\

- **additional scanning methods and references**

  - [read files line by line](https://www.geeksforgeeks.org/read-a-file-line-by-line-in-python/?ref=leftbar-rightbar)


\

#### "os" Module

- import and `os.getcwd()` --> state CURRENT DIRECTORY

- `os.listdir()` --> list current files & dirs in CWD

- [more common os module functions](https://www.geeksforgeeks.org/os-module-python-examples/)


\
\
\

## BIG CONCEPTS IN PYTHON

### What are Modules?

> similar to a code library or header files in C/C++
> includes functions, ojects, and classes for functionalities

- [import module methods](https://www.geeksforgeeks.org/import-module-python/?ref=rp)

- [template for creating one's own module BASIC](https://www.w3schools.com/python/python_modules.asp)

\
\

### Objects and Classes?

- REMEMBER, EVERYTHING IN PYTHON, IS AN OBJECT

  - hence the object oriented programming?

- [good explanation on YT](https://www.youtube.com/watch?v=8yjkWGRlUmY&t=0s)

- [another explanation](https://www.w3schools.com/python/python_classes.asp)

- [more explanations on classes](https://www.digitalocean.com/community/tutorials/how-to-construct-classes-and-define-objects-in-python-3)

\
\
\

### Python Terminal Tips

#### Modules

- [how to list all modules](http://xahlee.info/python/standard_modules.html)

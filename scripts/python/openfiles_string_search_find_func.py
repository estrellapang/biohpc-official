# TASK: Search and match ANY term inside an opened text file
# MECHANICS: FOR & IF IN logic statements, string.find() function, string index positions

# generate file handle object


# Protect script from propagating errors if file does not exist trying `try-except` block!

try :

  fhand_poem = open('./test_files/Learned_Astronomers_Whitman.poem','r')

except :

  print ('File cannot be opened/does not exist...')
  quit()  

# User input and store search term
searchTerm = input('Please enter your search term(the search is case sensitive!)\n')

lineCount = 0
searchCount = 0

for lines in fhand_poem :

  lineCount += 1

  if searchTerm in lines :
   
    searchCount += 1  # counts match instances
    indexLocate = lines.find(searchTerm)
    print("Line #",lineCount,":",lines[indexLocate:indexLocate+12]+'...')  # print search term in-text for context

print('There were',searchCount,"matches in this document for the term:",searchTerm)

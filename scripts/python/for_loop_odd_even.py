# for loop for classifying array integers to be even or odd

# NOTE: mathematical misconception made: '0' is an EVEN number, and not neither even or odd as this script suggests --- the mistake was made to leverage the effectiveness of nest if statements

# NEXT: gather user input for an array of integers

aList=[]

while True:

  i = input("Enter integer values one at a time - enter 'done' to finalize your array)\n")

  if i == 'done' :
    break

  try :
    isinstance(int(i),int)

  except:

    print("not an integer value!")
    continue

  i = int(i)
  aList.append(i)

# END of user input while loop #
#
# NEXT: Evaluate array for even and odd elements:

e = 0
o = 0

for i in aList : 

  if i % 2 == 0 :
    if i == 0 :
      print(i, 'is neither even or odd')
      continue
    print(i,'is an even number')
    e = e + 1

  if i % 2 != 0 :
    print(i,'is an odd number')
    o = o + 1

print('we have',e,'even numbers AND ',o,'odd numbers in our array')

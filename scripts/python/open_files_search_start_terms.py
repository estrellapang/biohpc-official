# TASK: search if lines begin with a search term in a file
# STEPS: Create file handle, and use for-if loop statement combination to search for terms within files


fhand_poem = open('./test_files/Learned_Astronomers_Whitman.poem','r')

searchTerm = input('Please enter your search term\n')

#searchTerm = searchTerm.lower()

lineCount = 0
searchCount = 0

for i in fhand_poem :

  lineCount += 1
  if i.startswith(searchTerm) :
    searchCount += 1
    print('Line #',lineCount,i[0:len(searchTerm)+12]+'...')  # print out line number on which the search term was found

print('There were',searchCount,'sentences that began with the word',searchTerm,"in Walt Whitman's poem: Lear'd Astronomers")

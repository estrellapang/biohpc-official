# for-loop example, use counter variable to count the # of elements in an array, and calculate the average of all elements in an array 

ticker = 0
listSum = 0
numSet = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]

for i in numSet :

  ticker = ticker + 1
  listSum = listSum + i   # seek sum of array elements
  
  print('Element #',ticker,', its value = array element value: ',i)

listAvg = listSum / ticker

print('This array has',ticker,'elements', 'and their values average out to be ',listAvg)

# ALTERNATIVE: use `sum()` and `leng()` functions for average
#
# `sum(numSet)/len(numSet)`

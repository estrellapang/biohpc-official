# example of while loop using the 'continue' statement

print('Enter input to print to screen,\nEnter EXIT to quit program,\nAnd prepend # to input to not print to screen')

while True:

  usrIn = input('> ')

  if usrIn == 'exit' or usrIn == 'Exit' or usrIn == 'EXIT' : 
    break 

  if usrIn == '' or usrIn[0] == '#' :   # if 0th character of string variable is '#' OR if empty input is entered, do not print input
    continue

  print('\n',usrIn)

print('The program has been terminated.')

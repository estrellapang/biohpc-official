# displaying the differences between '==' and 'is' operators

print("Output of '0 = 0.0'\n", 0 == 0.0)

print("Output of '0 is 0.0'\n", 0 is 0.0)

# for-loop example where a "counter variable" that starts at 0, that will serve to count how many iterations it takes to finish this for loop

ticker = 0
numSet = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]

for i in numSet :

  ticker = ticker + 1
  
  print('Loop #',ticker,' ,array element value: ',i)

print('We went through ',ticker,' iterations to finish this for-loop!')

## Networking: TCP/IP Protocols 

- TCP & IP protocl is a suite of protocol 
- TCP: Transmission Control Protocl 
- IP: Internet Protocol 

### IP Protocol

- Controls routing of info 

	- Topics: I.P. Addresses, Subnet Masks, Default Gateways, DNS, etc.

- I.P. is a routable protocol

  - can divide networks into multiple subnetworks (does not require every computer to connect to each other) 

  - routers act as main connections between sub-networks 

  - on OSI model: IP is on the #3 layer: network layer 

  - in general, its role is for devices to locate one another 

  - I.P. number is read as a series of 4 octets; each octet represents 8 bits (either 1 or 0), the number associated with each bit goes from1 to 128 (from right to left) and the actual number within each octet is the sum of all the 1 (true) bits--for each octet, the sum of all bits equal 255 

- Subnet Mask 

  - Prevent all devices from talking to each other

- Default Gateway 

  - router for sub-network: when another device cannot be found on the local network, the host goes to the Default Gateway, which via its routing table, redirects it to the external destinatio 

  - what connects your sub-networks to other networks, e.g. modems,routers

- DNS (domain name server) 

  - map domain names to I.P. addresses 

  - sequence of events: 

  1. user enters domain name 
  2. device sends it to  DNS server(s), which returns an I.P. address
  3. device receives I.P. --> begin looking within local subnet for it 
  4. when DNS returned I.P. not found locally --> device goes to router/default gateway
  5. default gateway connects device to I.P. found externally 

- DHCP (Dynamic Host Control Protocol) 

  - device connects to network --> talks to DHCP server, which gives the device its IP info (& keeps tabs on what has been givenl; avoid duplicates) 

  - device has to configure a range/scope of I.P. addresses that DHCP is allowed to return 

  - a lease time is assigned with each I.P. that has been given out by DHCP--every half-life,down to the end of the lease time, the device needs to contact DHCP servert to renew/keep the I.P. address  

- Static I.P. --> manually set-up I.P., submask, DNS server, & Default Gateway

  - if two devices have the same I.P. then their receipt of packages will be disorganized   


- NAT (Network Address Translation) 

  - removes need to have globally unique I.P.s 

  - NAT routes EXTERNAL, default gateway I.P.s to the local, non-unique I.P.s 

  - MORE DETAILS

### IP: Sub-net Masks 


- tells what part of I.P. is network identifier, and what part is for device or host identifier---these masks are used to create subnets
 
    - in other words, the subnet mask imparts two IDs  onto a single I.P. address: a Network ID & a Host ID   

      - the network identifier is something that does not vary throughout the device IPs in the network and subnetworks; the "Net" ID is what gets used for routing IP packets --> goes into sub/private networks & --> Node/Subnet addresses are used to locate devices within  

- two devices cannot talk if they belong to different subnets; EVEN if they are physically plugged into the same switch (how TCP/IP works), a router is absolutely needed for the subnets to communicate 

- IP Classes were divided by their left-most bytes(each octet has EIGHT bits, which altogether form a BYTE), with varying network/host I.D. allocations

	- A(0-127): network.host.host.host

	- B(128-191): network.network.host.host

	- C(192-255):  network.network.network.host 

  - NOTE that NETWORK IDs do NOT change! Here, 'host' is equally understood as 'subnets' and 'nodes' (nodes fall within subnets) 

nbsp;

- CALCULATE # of Subnets & Possible # of Hosts!!!

1. look at given network I.P., determine its class

2. from network I.P. class, differentiate which octet can be dedicated to making subnets 

3. pick a general mask form

	class A. 255.###.###.###
	
	class B. 255.255.###.###

	class C. 255.255.255.###

4. determine # of subnet  bits (1)  & # of hostID bits (2) 

- subnet bits = "1" bits on host octets where the mask bits overlap with network IP bits 

- Host ID bits = "0" bits on host octets both where mask bits overlap with subnet bits and where they do not


  - (2^n - 2) = number of possible hosts; n = number of bits utilized for hosts(0s) 

  - 2^n = number of possible subnets; n = # of bits used for subnets (1s) 

> Sub-netting at the byte boundary (most common) 
> e.g. given network address: 29.0.0.0 
>
>      given subnet Masks: 255.255.0.0 & 255.255.255.0 
>
> 1. given network I.P. is class A --> 2. network.host.host.host (the first octe> t is a fixed for public network routing) --> 3. Class B & C masks given 
>
> 4. subnet ID & host ID bits: 
>
> network IP bits: 00011101.00000000.00000000.00000000 
>
> class B mask:    11111111.11111111.00000000.00000000 
>
> class C mask:    11111111.11111111.11111111.00000000 
>
> results from cls.B mask: 8 subnet bits, & 16 host ID bits --> 2^8 = 256 subnet> s,and 64000 - 2 = 6398 hosts/subnet  
>
> cls.C mask: 16 subnetbits & 8 host ID bits; 2^16 subnets & 2^8 = 256-2 = 254 h> osts/subnet
 
 - why subtract 2 from # of hosts? B.C. 1st I.P. is used for the sub-network, & last (#.#.#.255) entry is used for the broadcast, which does not get assigned to a single host

> Sub-netting at NON-BYTE BOUNDARIES (typically used with class C network addresses with specific subnet & host numbers) 
> 
> Given network address: 195.1.1.0 ---Need: 5 subnets/Max 10 hosts each 
>
> - from left-most byte, the address is class C, so --> netowork.network.network.host  
> 
>  - only the last octet can be varied, meaning we are going to create our subnets not at the " . " byte boundaries, but instead in the middle of the right most byte 
>
> Steps: 
>
> 1. determine how many subnet bits we will borrow (from the right-most octet/byte)  
>
>   a. borrow from left to right --> 1 1 _ _ _ _ _ _ --> in this example, two subnet bits are borrowed, translating to 128 + 64 = 192 for th> e byte 
> 
>   --> for 5 subnets, a minimum of 3 subnet bits are needed --> 2^3 = 8 
>
> 2. determine how many host bits will be needed
>
>   --> for a maximum of 10 hosts, we actually need to retrict the # of host bits down to no larger than 4 -> 2^4 -2 = 14
> 
> 3. list possible subnetmasks:  
> 
>   --> we can have either 3 or 4 subnet bits and 4 host bits, yielding 
>   
>   `255.255.255.224` ( 3 subnet bit & 4 host bits)  -OR- `255.255.255.240` (4 & 4) 
>
> *** So in this problem, for the case the mask borrowing 3 subnet bits, we have 
>	
>	11111111.11111111.11111111.11100000

- Notice how the number of subnet bits dictate the number for the subnet byte   

[More Specific Subnetting Examples Here](http://www.steves-internet-guide.com/subnetting-subnet-masks-explained/) 

- Classless Inter-Domain Routing (CIDR)

    - Abbreviated labeling for the subnet-masks

    - General notation: "/##"

    - Indexing varies with the number of subnet bits in the subnet bytes

        - e.g. 255.255.255.0 = 24 subnet bits --> " /24 "

        - e.g. 255.255.192.0 = 18 subnet bits --> " /18 "

    - Applied e.g. 192.168.20.1, /24 = I.P. address + 255.255.255.0 as subnet mask

##### How routers split IP addresses into host & network 

- IP routers --> takes incoming packets with an IP address & subnet mask --> matches (in binary) everything in its routing table, from the longest IPs to the shortest (more specific destinations are matched first; host IDs are covered by the mask & not matched at this point) --> goes to specific subnet --> then specific host

- Actual IP splitting operation: 

1. translate both I.P. address & subnet into binary/ octet bits
2. align IP binary with mask binary
3. perform a logical AND operation to obtain IP identifier
4. perform  a logical OR op in the mask's 0 bit (or mask) region to obtain host identifier'

> e.g. 10.13.216.41 --> subnet mask = 255.255.192.0
>
>
> IP bits: 00001010.00001101.11011000.00101001
> netbits: 11111111.11111111.11000000.00000000
> host ID: 00000000.00000000.00011000.00101001 = 0.0.24.41
> net  ID: 00001010.00001101.11000000.00000000 = 10.13.192.0   




### TCP Protocol 

- Once devices locate each other, how do they communicate

  - #4 layer in OSI model: transport layer 

- Windowing: the process by which client exchanges info with servers 

  - exponentially growing array of packets: 2,4,16...this continue on as long as an acknowledgement is sent back
 
  - if in the middle, an break/corruption occurs, then the exchange restarts with receiver senting the last few received packets to the sender, then the exchange is gradually allowed to grow again  



## Protocols within the Samba suite: 

	- Samba comprises of a series of services & daemons that runs (typically) on Unix/Linux based platforms, and it allows the Unix/Linux host to communicate via the CIFS/SMB protocol in order for it to communicate with Windows hosts and share resources i.e. printers and filesystems

### Services & Daemons 

1. **smbd** --> controlled by the smb.service, it is responsible for granting the Linux system the ability to communicate over CIFS/SMB protocol, enabling file & printer sharing to Windows clients. Additional roles: user authentication (lets the Linux machine act as the Windows domain controllor, which responds to security authentications within Windows, and authenticate/validate user access on the network), resource locking (TCP ports 139 & 445) 

  - TCP port 139: for NetBIOS session (Windows) 
  - TCP port 445: for CIFS/SMB (Windows)

2. **nmbd** --> also controlled by the smb.service, this daemon interpretsand responds to NetBIOSname service requests, which are requests based on CIFS/SMB from Windows systems. (UDP port 137) 

  - UDP port 137: port for NetBIOS names (Windows)

3. **winbind** --> controlled by the windbind.services, it resolves user & group info from servers running on Windows NT, 2000, or 2003, for these systems utilize broadcasts that do not route. As such, the winbind daemon translates NetBIOS names from these types of hosts into IP addresses.


### Nomenclature 

- CIFS(common internet file system is an earlier dialect of the SMB (Server Message Block) protocol (application layer)  

- NetBIOS (Network Basic Input/Output System), or  File Sharing & Name Resolution protocol: used in file sharing in Windows hosts (NetBIOS is an API: Application Programming Interface)
 
  - not a networking protocol, and solely for communication in a local area network.

  - provides 3 services: 1. Name service, or WINS in Windows (add, find, delete names) 2. Datagram (basic transfer unit)  distribution service(send to specific NetBIOS name, send broadcast, receive), and 3. Session service (establishes connection between two machines, call listen, send, receive)

- API--> lets programs speak in an unified language to the system & other softwares. 

  - e.g. API talking to graphics cards 
	  

[good info on Samba ports and images](https://wiki.samba.org/index.php/Samba_AD_DC_Port_Usage)

## Network File System (NFS) & Samba filesharing: 

	**both comprises of services and protocols used in NAS (Networking Access  Storage) storage devices** 

### What is NAS? (NFS & Samba/CIFS file sharing protocols)

General Topology of NAS Storage

![alt text](https://www.bitrecover.com/blog/wp-content/uploads/2015/12/1-1024x619.jpg)  

- provides File-level Access

  - file access client(application) is on the **storage side** of the network  

  - typically involves one body of storage array, inside of which are loaded with RAID disk arrays

  - the storage devices themselves have to manage authentication, privileges, file-locking (waiting for one user to finish reading/writing before another user can read/write on it), and file-access requests    

- utilizes only Ethernet connections between clients & storage arrays

  - one access point (LAN switch) to the storage array from the file servers/directory services/clients 

  
- Strengths

  - easily accessible by different types of machines (Windows, Linux, MAC) 

- Weaknesses (scalability? traffic handling? performance?) 

  - could be loaded with lots of protocols & software in order to provide general access---slower execution in access; more processor utilization for data access (from both server & host) 

    - more I/O traffic on LAN network b.c. server/cient interactions 

  - single point of failue 

  - LAN bandwidth, which is already partially reduced by data transfers and back ups, is a bottle neck  


- Data Transmission Protocols

  - NFS,CIFS/SMB,TCP/IP

&nbsp;

### What is SAN (Storage Area Network)? 

General Architecture: 

![alt text](https://www.what-is-my-computer.com/images/storage-area-network.jpg)

&nbsp;

- provides Block-level Access

difference between file-level & block-level access: 
![alt text](http://itbundle.net/wp-content/uploads/2016/07/storaglevel-access.png) 

  - client already has filesystem, and knows which blocks are associated with each file---client sends block requests e.g. 5000-7000 to the servers, which then returns all the pieces 

  - how to block requests work?  

- utilizes interposing SAN connections between servers (Copper or  Fiber Optic cabling, and SAN switches) 

  - via utilization of "direct attached shared storage", clients are inter-connected to several file server/storage, which themselves are also interconnected. 

  - sometimes built with redundant links in SAN switches

  - every storage device within disk arrays, even partitions within, have a **Logical Unit Number** (LUN) --- which allows the unique identification, and therefore precise route of access to portions of storage within the SAN.

    - the LUNs allow the SAN to implement specific access controls, i.e. an admin can give isolated portions of storage to different groups or individual servers, which are identified and authenticated by their LUNs

    - LUN masking, similar to subnet masking? 

  - another way to control storage access, is called "zoning" 

- Protocols

  - Infiniband & iSCSI (bridged into fibre channel SAN), SCSI, FCOE (fiber channel over Ethernet)

    - thus SANS require unique networking devices i.e. HBA (host bus adapters that fits in PCI slots) & SAN supported switches [see how the SAN HBA adapters and fiber channels work](https://en.wikipedia.org/wiki/Storage_area_network) 

  - within the switched SAN, the fiber channel switched fabric protocol "FC-SW-6"(which is below the SCSI protocol) is used, where every HBA has an unique World Wide Name (WWN), which is registered in the SAN switch name server. Sometimes even the SAN storage devices will have Worldwide Port Names (WWPN).

- Strengths

  - better Fault Tolerance, high I/O performance (I/O), Latency  

  - not accessible via LAN, therefore reduces chance of interferring LAN by using its own dedicated SAN network   

- Weaknesses

  - more expensive & comeplex than NAS

  - needs filesystems that are cluster aware for simultaneous access [follow trail here](https://www.idkrtm.com/what-is-the-difference-between-a-storage-area-network-san-network-attached-storage-nas/) 



- Additional Illustration: SAN vs. NAS

![alt text](http://computer-room-design.com/wp-content/uploads/sites/21/2015/06/CompellentStorage_01.gif) 

![alt text](https://cdn.ttgtmedia.com/rms/onlineImages/storage-pfs_panasas_desktop.jpg)

#### Diagrams on Object-level Access File Systems 

![alt text](https://cdn.ttgtmedia.com/rms/onlineImages/storage-pfs_lustre_desktop.jpg) 

 

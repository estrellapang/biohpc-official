## What is the OSI (Open System Interconnection)  Model?

[Fakipedia has an  Elaborate Overview Here](https://en.wikipedia.org/wiki/Transport_layer) 

### A Logical Model for Network Communication 

- 7 layers, which layer is a series of protocols

#### 7th (topmost) Layer 

- Application layer
 
 - User interaction protocols (HTTP, HTTPS, FTP, TELNET, DHCP, NFS, SMTP(emails),etc.) 

#### 6th Layer

- Presentation layer 

 - Operating system works on this layer

  - 3 MAJOR ROLES: 

	1. **Conversion**:  data from app. layer to binary 

	2. **Compression**: reducing the number of bits via data compression (two main types: Lossy & Lossless?) 

	3. **Encrption**: data is encrypted/decrypted via SSL (Secure Sockets Layer) protocol (additional protocols include XDR, TLS, & MIME,etc)

   - Additional Components: device drivers, security protocols

#### 5th Layer 

 - ROLES:  Authentication (credential & passwords),  Authorization (permissions) & Session Management (which applications the received packets go to)

 - Deals with communication that creates a session b/w two computers
 
 - APIs: application programming interfaces (e.g. NETBIOS), let applications from diff. hosts to communicate 

   - PHP & APACHE config files for web servers
 

#### 4th Layer (this & all above considered local system component layers) 

- Transport layer

- ROLES: Data Segmentation, Flow Control (devices communicate rate of transfer from client to host), Error control (automatic repeat request when transfer process is interrupted) 

  - protocols: **Transmission Control Protocol(TCP)**, which relies critically on sequential order to work; UDP(does not need sequencing in packets;transported over several ports with a reference map for packet-reassebly),DCCP,etc. 

	- UDP is faster b.c. no requirements for feedback (activities involving streaming & video games) --> considered "connection-less transmission"   

	- TCP is slower, but more reliable (full data delivery is crucial; www., email,FTP,etc) --> "connection transmission" 

  - Windowing (Segentation) how large a block a piece of info is sent; how long a computer waits for confirmaion

WHAT ACTUALLY DOES THIS (segmentation process?) 


#### 3rd Layer (3rd layer & below are together called the Network Communication Layer) 

- Network layer

- ROLES:  Logical Addressing, Routing (how subnets & hosts within are located), & Path Determination (which route is the faster way to connect)

- Concepts:  DNS, Default Gateways (routers), subnet masks

 - Devices:Routers (default gateways)
  
** firewalls operate at the 3rd & 4th layers, and the essence of its task boils down to blocking TCP/UDP ports or IP addresses

#### 2nd Layer

 - Data Link layer receives data packets from the network layer (contains sender & receiver address)  

 - Where MAC (media access control) --> allows point-to-point connection between hosts within a local network where numerous hosts are connected together 

	 - when an adapter/NIC receives a packet, it'll try to compare the packet's destination MAC addr. to its own MAC addr., if they match, then the packet is processed; if not, then packet --> discarded   

	- **MAC addresses' role in device discovery** (broadcast request via ARP, if no response locally, the gateway router will respond)

 - protocols i.e. 802.11 & SS7, IEEE 802.3 (protocol for MAC addresses)

 - ARP (address resolution protocol) --> how switches/hubs obtains MAC addresses 
    
 - Devices: local physical connections--> could be switches or wifi router 

 - when outward bound, this layer appends source & destination MAC addresses to the packet sent from the Network layer, making the initial packet a "frame" 

[details  2nd & 3rd layer work together](https://superuser.com/questions/623511/what-is-the-exact-use-of-a-mac-address) 

[more details  2nd & 3rd layer work together](https://www.quora.com/How-does-a-computer-know-the-MAC-address-of-its-receiver) 

[even more details](https://www.howtogeek.com/169540/what-exactly-is-a-mac-address-used-for/) 


#### 1st Layer

 - Physical layer

 - signal trickled down from layers above (binary bits) will be carried over in local media i.e. copper & fiber optic cables, and radio signals

[details on which protocol resides in which layer](https://www.techopedia.com/definition/24961/osi-protocols) 

### What does this Model provide? 

- Helps in breaking down problem categories in troubleshooting 

 e.g. server issues: look & see if the back blnkers are lit 



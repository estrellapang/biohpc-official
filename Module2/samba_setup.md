## Install Samba Server on your Linux host! 

	- when your Linux machine wants to mount a shared filesystem a Windows host


### On Linux Server: 

1. Samba installation & Firewall bypass: 

`sudo yum install samba samba-client samba-common` 

`sudo firewall-cmd --permanent --zone=public --add-service=samba` 

`firewall-cmd --reload` 

2. Appoint user on Linux server whom the Windows client can authenicate as:

- this can be an existing or new user

`smbpasswd -a [user]` 

e.g. `smbpasswd -a estrella` 

--> you will be prompted to put in a password 
 
- add this user to the group associated with the shared directory 

`usermod -g sharers estrella` 
 

3. Modify samba .conf file: 

- good practice to make a copy of the original .conf file before editing it: 

`cp /etc/samba/smb.conf /etc/samba/smb.conf.bak`

- `.bak` as in backup file 

- add modify the following lines to the smb.conf file: 

```bash 

[global] 

workgroup = WORKGROUP (be sure to check on your Windows machine) 

  - to check Windows client workgroup, go to "This PC" --> *right click "Advanced System Settings" --> under tab "Computer Name", both the host/machine name and its workgroup will be displayed 

netbios name = [arbitrary; equivalent to a hostname for your Linux machine that will appear on the windows client]

security = user

[Secure] --> this entry is arbitrary as well 

comment = fileshare with password security

path = [directory of shared files] e.g. path = /home2 

valid users = @[group associated with the shared directory; your smbpasswd user should be in here] 

e.g. valid users= @sharers 

guest ok = no 

writable = yes 

browsable = yes 

``` 

####NOTE: one may need to change the SELinux security context for the SAMBA shared directory: 

`chcon -t samba_share_t /home2` 

- check the security context of files/directories via `ls -Z` 


4. Test smb.conf file paramters

`testparm` 

- enable & restart SAMBA

`sudo systemctl enable smb.service` --> controls the smbd (daemon), which faciliates the SMB/CIFS protocols 

`sudo systemctl restart smb.service` 

`sudo systemctl enable nmb.service`

`sudo systemctl restart nmb.service` -->  listens for requests for its own NetBIOS name; responds with its own IP address  

5. Access shared Linux directory from Windows Client: 

- Enable network discovery/file sharing option (on the host-only adapter)

go to "Network Settings" --> "Ethernet" --> click the correct adapter, and slide on the network discovery option

- The Windows Client may take up to a few minutes to be able to fully connect to the Linux host:

  - "Network" --> check if the Linux server is detected (its name will be identical to the `netbios name` entry within the `smb.conf` file

  - Click into the Linux server --> authenticate via username specified previously in the `smb.conf` file


## Samba Account Management: 

`sudo pdbedit -L -v` -- list all samba users (`-L` -- list users; `-v` verbose)



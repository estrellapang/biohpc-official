## Network File System: 

- Allows work stations (or thin clients--ones that don't have much of an internal harddrive but that coming from the NFS server) to store info in a server file system' 

- Advantages: 1. Less local disk space used 2. Shared "/home" directories 3. removeable media i.e. CD/DVD, flashdrives, & floppies can be accessed via other machines connected to the NFS

## NFS Host Setup 

- check if you have firewall daemon running: if not, install & start it

```bash 

which firewalld

systemctl status firewalld 

sudo yum -y install firewalld 

sudo systemctl enable firewalld 

systemctl start firewalld 

systemctl status firewalld 

``` 

1. Install nfs-utils 

`sudo yum -y install nfs-utils` 

`sudo systemctl enable nfs-server.service` 

`sudo systemctl start nfs-server.service`

- check the status, ENABLE, and start the following services: `rpcbind, nfs-lock, nfs-idmap`  

2. Check your client user UIDs & GIDs 

- in order for your client user to have the appropriate permissions, you need to 

	a. make sure your client has the same uid & gid on both server & client machines

	`id [username]`	--> see uid & gid; compare the output to that in the client device

	`/etc/passwd` --> edit the info on this page if uid & gid are different from nfs host to client 

 
	b. set your shared directory with desired group or ACL permissions for the user   

		- good questions to ask are: --> "do newly created files within the shared directory belong to the group that can access it, or do they belong to the primiary groups of the individual users within that access group(the latter scenario is not desirable)?"

					     --> "should I apply a GID or a default ACL on this directory?" 

3. Make a shared directory 

- modify permissions

`sudo chmod 2770 /home2` --> give owner & group all permissions, plus a setgid (allows new files/directories created to inherit the groupname of the group that has access to the directory) 
 
- change the directory group to "sharers" `chown root:sharers /home2` 

- a default ACL MIGHT be needed: log in as a member of "sharers," create a file within the shared directory; then `ls -l` to see the newly created file permissions: the new file SHOULD: 

1. have its group being "sharers" 

2. have read write execute for "sharers"

To apply a default ACL: 

```bash 

sudo setfacl -dm g:[access group name]:rwx [shared directory]

e.g. 

sudo setfacl -dm g:sharers:rwx /home2

sudo getfacl /home2 --> verify

```  

4. Edit the export file `/etc/exports` 

- Typical Options for exports: 

  -  rw (read & write permissions), ro (read-only), sync(reply only after disk write;it's default for nfs versions > 1), root_squash (take away root privileges from the client), (anonuid & anongid & all_squash)--> makes all users from the client become mapped with one predefined uid & gid; only use when no security requirements exist), no_all_squash (no need to specify)'

**NO SPACES b/w host IP & export option parentheses!!!** 

i.e. `/home2 client.innercircle(rw,sync,root_squash)` 

i.e. `/home2 192.168.56.4(rw,sync,root_squash)` 

`:wq` 

**be wary**, as a NFS-client, you only have the rights to:

a. change owner & group ownership of your OWN files & subdirectories
 
b. make a user locally, and add him/her/it to the ownergroup of the sharedfile directory (that is, if you have the same gid as the one that's on the remote server)

5. Add NFS service override in CentOS 7 firewall-cmd

```bash

firewall-cmd --permanent --zone=public --add-service=ssh
firewall-cmd --permanent --zone=public --add-service=nfs
firewall-cmd --reload

```

6. restart nfs-server: 

`systemctl restart nfs-server`  --> MUST-DO! 

-OR- `exportfs -a` --> should also make changes effective


## NFS Client: 

1. Install nfs-utils 

`sudo yum install nfs-utils` 

--> check & see if it is running

`sudo service nfs-server enable` 

`sudo service nfs-server start` 

`sudo service nfs-server status` 

2. Create mount point for NFS directory 

`mkdir -p /mnt/nfs/home2` --> we can put it anywhere, really

`mkdir /home2` 

3. Mount the the shared NFS directory 

`mount -t nfs [IP of NFS server]:[shared directory] [mount point]`

e.g. `sudo mount -t nfs nfs.innercircle:/homeshare /home2`

4. Check the mount via `df -Th` 

5. Put the mount in the /etc/fstab 

`[NSF server IP]:[shared directory] [mount point] [fs type] defaults 0 0` 

`nfs.innercircle:/homeshare /home2 nfs defaults 0 0`

-OR- `I.P. of remote]:/[directory] /[local mount pt.] nfs _netdev 0 0`  

**NOTE: the file system type has to be specified as `nfs` on the CLIENT!** 

    ---> on the host fstab file, the shared filesystem does not have to be nfs! (b.c. it is local storage) 
 
--> "_netdev" = a filesystem option: system waits for network to connect before mounting nfsshare

6. Verify Mount

`umount [mount point]` 

`mount -a` 

`df -Th` 



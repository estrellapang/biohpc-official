## NFS-autofs 

> using autofs as alternative to fstab 

[ ] --> why? 


**packages** 

`sudo yum install authfs` 

**configurations**

- edit the `auto.master` 

entry: 

`[local mountpt] /etc/[name of mapping file]` 

e.g. `/nfs /etc/sharedhome.map` --> autofs service will make the `/nfs` directory as mount point for various shared file directories to be imported (e.g. within /nfs, we can have multiple subdirectories within from other file servers) 

- go to the mapping file, and place the following entry(ies) 
entry:

`[the mount pt directory] -fstype=[nfs,nfs3,nfs4 or auto] -rw [NFS location]:[exported directory]` 

e.g. `sharedhome -fstype=nfs4 -rw sambanfs.vms.train:/home2`  

  - in the line above, `sharedhome` will be the mount pt subdirectory for /home2 from the file server 

- enable the autofs service: 

`sudo systemctl enable autofs`

`sudo systemctl start autofs`

`sudo systemctl status autofs`

**two ways we can mount the import**

1. navigate directly: 

`cd /nfs` --> `cd ./sharedhome` --> autofs should mount `/home2` onto `/sharedhome`, you should be able to access its contents like you would if it was mounted via `/etc/fstab` 

`cd /nfs/sharedhome` --> a shortcut to the method above 

2. log in as a LDAP user: 

- the LDAP user's home directory should be pre-made on the shared directory 

  - the ldap user should also have the adequate group permissions to tranverse into the directory
 

## Creating a RAID6 device in RHEL/CentOS Linux

### Add additional virtual hardisks to your VM: 

1. select VM --> "Settings" --> "Storage" 

2. click "add hardisk" icon within the "Controller:SATA" tab (the icons will appear once selected)  

3. choose "create new disk" --> "VDI" --> "Dynamically allocated"

4. allocate name and size for your new hardisk --> confirm "Ok"  


### Partition your New Hard Drives: 

- you can let your partition take up a portion or span through the whole disk 

- assign a partition table to the hard drive: 

`sudo parted [device name, i.e. /dev/sdc] mklabel gpt` --> in the last field, as opposed `gpt`,  you can also enter `msdos` which is the old MBR (master boot record format) 

- to create a single partition spanning the entire disk (via parted tool; you can also use fdisk or gdisk)

`sudo parted -a opt /dev/sda mkpart primary [filesystem type: xfs, ext4, etc] 0% 100%` 

- user doesn't necessarily need to specify the filesytem type, which can be created after the partition has been created

`lsblk` --> a new partition should be listed under your disk name 

e.g. 


```bash

sudo parted /dev/sdb mklabel gpt 

sudo parted -a opt /dev/sdb mkpart primary 0% 100%

``` 

- repeat the same two commands for all hard drives you wish to add into your RAID device


--> should also practice the same task with gdisk & fdisk 

&nbsp; 

### Make your RAID storage with `mdadm` 

> NOTE: be prepared to sudo a lot

1. check if `mdadm`(multiple devices admin) tool is installed on your system:

`which mdadm` --> if not there `yum install mdadm` 

2. check if your disks already have RAID created: 

`mdadm -E /dev/sd[c-j]1` this runs the command through several disk partitions from /dev/sdc1 to /dev/sdj1 

- "-E" --> the examine option

- if output displays " no md superblock detected, " then your disks are free to go

3. create RAID device:

`mdadm --create /dev/[device name, i.e. md0] --level=[] --raid-devices=[# of disks] [disk drive names]`

e.g. `mdadm --create /dev/md0 --level=6 --raid-devices=8 /dev/sd{c..j}1`   

4. confirm the creation by examining the file `/proc/mdstat`

`cat /proc/mdstat`

- you can also check raid process in real time: `watch -n1 cat /proc/mdstat` 

  - this option gives you a live update every 1 second

5. run command from step 2. to verify the status of your RAID device

- verify further via the `--detail` option 

`mdadm --detail /dev/[device]`

e.g. `mdadm --detail /dev/md0` 

`lsblk` --> you should see your disk partitions being a part of a raid6 device 

&nbsp;

### Make your RAID device a part of the LVM system:
  
1. make your RAID device a PV (physical volume) 

`pvcreate /dev/[device name]` 

`pvs` --> verify 

e.g `sudo pvcreate /dev/md/firstraid` -OR- `sudo pvcreate /dev/md0` 
 

2. add RAID's PV into an existing/new VG (volume group)' 

`vgextend [VG name] [path to PV]` 

e.g. `vgextend centos2 /dev/md0`

2a. add RAID's PV to a NEW VG 

`vgcreate [new VG name] [associated path to PV]` 

e.g. `sudo vgcreate raidone /dev/md0` 

`vgs` --> verify 

3. make a logical volume from RAID-containning VG

`lvcreate --name [] --size [] [Your VG]` 

e.g. `lvcreate --name sharing1 --size [30G] centos2` 

                   -OR- 

`lvcreate -n [name] -l [percent of free space]%FREE [VG name]` 

--> `-l` used to indicate percent free space 

--> `-L` option for assigning fixed amount of space

e.g. `lvcreate -n nfslv -l 100%FREE raidone` 

`lvs` or `lvdisplay` --> remember the path to your LV  

&nbsp; 

### Make filesystem on your RAID device based LV  & Mount it

- establish filestystem

`mkfs -t xfs [path to LV]` 

- make a mount point 

`mkdir /home2` 

- mount LV onto /home2 

`mount [path to LV] /home2`

`df -Th` --verify mount 

- make mount permanent: 

`vi /etc/fstab` 

ADD `[path to LV] [mt. pt.] xfs [defaults] 0 0` 

- verify mount: `umount /home2` --> `mount -a` --> `df -Th`  


